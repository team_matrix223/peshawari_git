﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="recipe.aspx.cs" Inherits="recipe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cnMaster" Runat="Server">
<div class="row">
            
            
            <div class="col-md-12">
  					
              <h2 style="border-bottom: 1px dashed silver; border-top: 1px dashed silver; 
                  color: gray; padding: 7px; font-size: 20px; margin-top: 10px;
                   background: none repeat scroll 0% 0% rgb(238, 238, 238); 
                   font-family: serif;"><a href="recipies.aspx">PESHAWARI RECIPIES</a>  >  <span style="font-size:14px" ><asp:Literal ID="ltTitle" runat="server"></asp:Literal></span></h2> 
  					</div>
            
        </div>

  <div class="row">
  <div class="col-md-12">
 
   

<br />

  <asp:Literal ID="ltProcess" runat="server"></asp:Literal>
  <br />
  <h5><b>Choose Ingredients to Buy</b></h5>
  <br />

  <table>
  <asp:Literal ID="ltRecipeIngredients" runat="server"></asp:Literal>
  </table>
  <br />
  </div>
  
  </div>

</asp:Content>

