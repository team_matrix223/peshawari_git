﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="performance.aspx.cs" Inherits="performance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <script src="js/jquery.min.js" type="text/javascript"></script>
   
   <script language="javascript" type="text/javascript">
       $(document).ready(
       function () {


           $("#dvAdd").click(
       function () {
           var qty = $("#txtQty").val();
           qty = Number(qty) + Number(1);
           $("#txtQty").val(qty);

           $.ajax({
               type: "POST",
               data: '{}',
               url: "performance.aspx/AddUpdate",
               contentType: "application/json",
               dataType: "json",
               async:false,
               success: function (msg) {
                   var obj = jQuery.parseJSON(msg.d);
                   $("#txtQty").val(msg.d);
          
                   

               }, error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function (msg) {

               }

           });
       }
       );
       }
       );
       
   </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="dvCart">
    
 
   <div id="dvMinus"  style="float:left;background:gray;color:White;width:20px;cursor:pointer">-</div>
 
   <input type="text" value="1" id="txtQty" style="float:left"/>
   <div id="dvAdd" style="float:left;background:gray;color:White;width:20px;cursor:pointer">+</div>
    </div>
   
    </div>
    </form>
</body>
</html>
