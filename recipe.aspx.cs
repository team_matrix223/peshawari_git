﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class recipe : System.Web.UI.Page
{
    public int RecipeId { get { return Request.QueryString["id"] != null ? Convert.ToInt32(Request.QueryString["id"]) : 0; } }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            BindRecipeDetail();
        }

    }


    void BindRecipeDetail()
    {
        Recipe objRecipe = new Recipe();
        objRecipe.RecipeId = RecipeId;
        new RecipeBLL().GetById(objRecipe);

        
        ltProcess.Text = objRecipe.Process;
        ltTitle.Text = objRecipe.Title;
        ltRecipeIngredients.Text = new RecipeBLL().GetRecipeIngredientHtml(RecipeId);
       
    }
}