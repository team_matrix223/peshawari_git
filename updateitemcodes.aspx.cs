﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    [WebMethod]
    public static string BindCat2()
    {

        string Cat2 = new CategoryBLL().GetLevel2Options();
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Category2 = Cat2,
        };
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string GetData(int PageNumber, int PageSize, int Cat2)
    {
        int TotalRows = 0;
        string html = new ProductsBLL().GetProductPaging(PageNumber, PageSize, Cat2, out TotalRows);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            HTML = html,
            TR=TotalRows
        };
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string GetProductDetail(string ItemCode,int VariationId)
    {
        int VId = 0;
        string HtmlLocal = "";
        int OStatus = 0;
        int LStatus = 0;
        string html = new ProductsBLL().GetProductDetail(ItemCode, VariationId, out VId, out HtmlLocal, out OStatus, out LStatus);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            HL=HtmlLocal,
            HTML = html,
            VariationId=VId,
            OStatus=OStatus ,
            LStatus = LStatus 
        };
        return ser.Serialize(JsonData);
    }
    
    [WebMethod]
    public static string UpdateVariationDetail(string ItemCode, decimal Price, decimal Mrp, string VariationId)
    {

        int  status = new ProductsDAL().UpdateVariationDetail(ItemCode,Price,Mrp,VariationId );
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Status = status,
           
        };
        return ser.Serialize(JsonData);
    }
}