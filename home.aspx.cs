﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class m_home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        List<Category> lst = new CategoryBLL().GetByParentId(0);
        StringBuilder str = new StringBuilder();


        foreach (var item in lst)
        {
            str.Append("<div style='width:100%;margin-bottom:2px'><a href='search.aspx?c="+item.CategoryId+"'><img src='CategoryImages/"+item.PhotoUrl+"' style='width:100%'/></a></div>");
        
        }
        ltImages.Text = str.ToString();
    }
}