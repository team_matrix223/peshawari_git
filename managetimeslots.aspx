﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="managetimeslots.aspx.cs" Inherits="backoffice_managetimeslots" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
  <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">

    var m_Id = 0;

    function ResetControls() {
        m_Id = 0;
        $("#btnAdd").css({ "display": "block" });


//        $("#btnUpdate,#btnReset").css({ "display": "none" });
        $("#chkIsActive").prop("checked", false);
        $("#hdnId").val("0");
        validateForm("detach");
    }



    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {
        if (!validateForm("frmTimeSlot")) {
            return;
        }
        var Id = m_Id;
        var StartTime = $("#<%=ddlStartTime.ClientID %>").val();
        if ($.trim(StartTime) == "") {
            $("#<%=ddlStartTime.ClientID %>").focus();

            return;
        }
        var IsActive = "";

        if ($("#chkIsActive").prop("checked") == true) {
            IsActive = "True";
        }
        else {
            IsActive = "False";
        }
        var EndTime = $("#<%=ddlEndTime.ClientID %>").val();
        if ($.trim(EndTime) == "") {
            $("#<%=ddlEndTime.ClientID %>").focus();

            return;
        }
        $.ajax({
            type: "POST",
            data: '{"Id":"' + Id + '", "StartTime": "' + StartTime + '","EndTime": "' + EndTime + '","IsActive": "' + IsActive + '"}',
            url: "managetimeslots.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == 0) {
                   
                    alert("Insertion Failed.slot already exists.");
                    return;
                }

                if (Id == "0") {
                    BindGrid();
                    alert("Slot is added successfully.");
                }
               

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }

    $(document).ready(
    function () {

        BindGrid();
        $("#btnAdd").click(
        function () {
           
            m_Id = 0;
            InsertUpdate();
        }
        );


      

        $("#btnReset").click(
        function () {
           
            ResetControls();

        }
        );


    }
    );

</script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
<div id="content" style="padding:6px">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update TimeSlot</span>
                     
                        <br />
                    </h3>
				   <div class="youhave" style="padding-left:30px">
                   <table cellpadding="0" cellspacing="0" border="0" id="frmTimeSlot">
                     
                   
                     <tr> 
                     <td>Start Time:</td>
                    <td> <asp:DropDownList ID="ddlStartTime"  class="validate ddlrequired" runat="server"   Width="195px"  >
                                                         <asp:ListItem Value ="01:00" Text ="01:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="02:00" Text="02:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="03:00" Text= "03:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="04:00" Text = "04:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="05:00" Text = "05:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="06:00" Text = "06:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="07:00" Text= "07:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="08:00" Text = "08:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="09:00" Text = "09:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="10:00" Text = "10:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="11:00" Text = "11:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="12:00" Text = "12:00 PM" ></asp:ListItem>
                                                        <asp:ListItem Value ="13:00" Text = "01:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="14:00" Text = "02:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="15:00" Text = "03:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="16:00" Text = "04:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="17:00" Text = "05:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="18:00" Text = "6:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="19:00" Text = "07:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="20:00" Text = "08:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="21:00" Text = "09:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="22:00" Text = "10:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="23:00" Text = "11:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="24:00" Text = "12:00 AM"></asp:ListItem>
                                                        </asp:DropDownList>
                     
                    </td>
                     
                  </tr>
                                                    
                      <tr> 
                     <td>End Time:</td>
                    <td> <asp:DropDownList ID="ddlEndTime"  class="validate ddlrequired" runat="server"   Width="195px">
                                                        <asp:ListItem Value ="01:00" Text ="01:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="02:00" Text="02:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="03:00" Text= "03:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="04:00" Text = "04:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="05:00" Text = "05:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="06:00" Text = "06:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="07:00" Text= "07:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="08:00" Text = "08:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="09:00" Text = "09:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="10:00" Text = "10:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="11:00" Text = "11:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value ="12:00" Text = "12:00 PM" ></asp:ListItem>
                                                        <asp:ListItem Value ="13:00" Text = "01:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="14:00" Text = "02:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="15:00" Text = "03:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="16:00" Text = "04:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="17:00" Text = "05:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="18:00" Text = "6:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="19:00" Text = "07:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="20:00" Text = "08:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="21:00" Text = "09:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="22:00" Text = "10:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="23:00" Text = "11:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value ="24:00" Text = "12:00 AM"></asp:ListItem>
                                                       
                                                        </asp:DropDownList>
                     
                    </td>
                     
                  </tr>
                                                     
                      <tr>
                     <td><label><input type="checkbox" name = "CheckBox" value= "Order" id="chkIsActive">IsActive</label></td> 
                      </tr>
                   
                                            <tr>
                                             
                                            <td colspan="100%" style="padding-left:45px" >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add Slot</div></td>
                                          <%-- <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" >Update Slot</div></td>--%>
                                            <td><div id="btnReset"  class="btn btn-primary btn-small" style="display:none;width:60px;" >Cancel</div></td>
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>
                    			 
       
                    </div>
			  </div>
               


               <div id="Div1" style="margin-top:10px">
                    <h3 class="reallynow">
                        <span>Manage TimeSlots </span>
                      
                        <br />
                    </h3>
				    <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>
			  </div>

            </div>
</form>

 <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageTimeSlots.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Id', 'Slot StartTime', 'Slot EndTime','AdminId','StartTime','EndTime','IsActive'],
                        colModel: [
                                    { name: 'Id', key: true, index: 'Id', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'SlotStartTime', index: 'SlotStartTime', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'SlotEndTime', index: 'SlotEndTime', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'AdminId', index: 'AdminId', width: 200, stype: 'text', sortable: true, editable: true,hidden: true, editrules: { required: true } },
                                     { name: 'StartTime', index: 'StartTime', width: 200, stype: 'text', sortable: true, editable: true,hidden: true, editrules: { required: true } },
                                      { name: 'EndTime', index: 'EndTime', width: 200, stype: 'text', sortable: true, editable: true,hidden: true, editrules: { required: true } },
                                       { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Id',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Time Slots",

                        editurl: 'handlers/ManageTimeSlots.ashx',



                    });




                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
//                    m_Id = 0;
//                    validateForm("detach");
//                   
//                    m_Id = $('#jQGridDemo').jqGrid('getCell', rowid, 'Id');

//                    var Start = $('#jQGridDemo').jqGrid('getCell', rowid, 'StartTime');
//                    var End =  $('#jQGridDemo').jqGrid('getCell', rowid, 'EndTime');
//                  
//                   
//                    $("#<%=ddlStartTime.ClientID %>").val(Start);
//                     $("#<%=ddlEndTime.ClientID %>").val(End);


//                     if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
//                        $('#chkIsActive').prop('checked', true);
//                    }
//                    else {
//                        $('#chkIsActive').prop('checked', false);

//                    }
//                 
////                   $("#<%=ddlStartTime.ClientID %>").val($('#jQGridDemo').jqGrid('getCell', rowid, 'StartTime'));
////                   $("#<%=ddlEndTime.ClientID %>").val($('#jQGridDemo').jqGrid('getCell', rowid, 'EndTime'));
//                   
//                    $("#btnAdd").css({ "display": "none" });
//                    $("#btnUpdate").css({ "display": "block" });
//                    $("#btnReset").css({ "display": "block" });
//                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>

</asp:Content>

