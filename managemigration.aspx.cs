﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Data;

public partial class backoffice_managemigration : System.Web.UI.Page
{

    protected string m_sMessage = "";
    protected string m_sSubMessage = "";
    public DataTable dtFinal = new DataTable();
    protected bool m_bShow = false;
    protected string ErrorMessage = "";
    protected string htmldata = "";
    public int m_CurrentPage = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {

                if (ViewState["currentPage"] == null)
                {
                    ViewState["currentPage"] = 0;

                }


                if (Session["dttProducts"] == null)
                {
                    dtFinal.Rows.Clear();
                    dtFinal.Columns.Clear();
                    dtFinal.Columns.Add("Id", typeof(int));
                    dtFinal.Columns.Add("Unit", typeof(string));
                    dtFinal.Columns.Add("Qty", typeof(decimal));
                    dtFinal.Columns.Add("Price", typeof(decimal));
                    dtFinal.Columns.Add("Mrp", typeof(decimal));
                    dtFinal.Columns.Add("Description", typeof(string));
                    dtFinal.Columns.Add("type", typeof(string));
                    dtFinal.Columns.Add("PhotoUrl", typeof(string));
                    dtFinal.Columns.Add("ItemCode", typeof(string));
                    dtFinal.Columns[0].AutoIncrement = true;
                    dtFinal.Columns[0].AutoIncrementSeed = 1;
                    dtFinal.Columns[0].AutoIncrementStep = 1;
                    Session["dttProducts"] = dtFinal;
                }
                catLevel3.Visible = false;
                //ProductcatLevel3.Visible = false;
                BindCategories();
               // btnUpdate.Visible = false;

            }
        }
        catch (Exception ex)
        {
        }
    }


    public void BindCategories()
    {
        try
        {
            ddlCategory.DataSource = new CategoryBLL().GetByLevel(1);
            ddlCategory.DataValueField = "CategoryId";
            ddlCategory.DataTextField = "Title";
            ddlCategory.DataBind();
            ListItem li = new ListItem();
            li.Text = "--Category Level 1--";
            li.Value = "0";
            ddlCategory.Items.Insert(0, li);

            ddlnewCategory.DataSource = new CategoryBLL().GetByLevel(1);
            ddlnewCategory.DataValueField = "CategoryId";
            ddlnewCategory.DataTextField = "Title";
            ddlnewCategory.DataBind();
            ListItem li1 = new ListItem();
            li1.Text = "--Category Level 1--";
            li1.Value = "0";
            ddlnewCategory.Items.Insert(0, li1);

          


        }
        finally { }

    }
    public void BindSubCategories(int ParentId, DropDownList ddlParent, DropDownList ddl)
    {
        try
        {
            if (ddlParent.SelectedIndex != 0)
            {
                
                m_bShow = true;
                ddl.DataSource = new CategoryBLL().GetByParentId(ParentId);
                ddl.DataTextField = "Title";
                ddl.DataValueField = "CategoryId";
                ddl.DataBind();
                ListItem li = new ListItem();
                li.Text = "--Category Level 2--";
                li.Value = "0";
                ddl.Items.Insert(0, li);
            }
            else
            {
                m_sMessage = "Please Select  Category Level 1";
            }
        }
        finally
        {
        }

    }

    public void BindCategoryLevel3(int ParentId, DropDownList ddl)
    {
        try
        {
            if (ParentId != 0)
            {
              
                m_bShow = true;
                ddl.DataSource = new CategoryBLL().GetByParentId(ParentId);
                ddl.DataTextField = "Title";
                ddl.DataValueField = "CategoryId";
                ddl.DataBind();
                ListItem li = new ListItem();
                li.Text = "--Category Level 3--";
                li.Value = "0";
                ddl.Items.Insert(0, li);
            }
            else
            {
                m_sMessage = "Please Select  Category";
                return;
            }
        }
        finally
        {
        }

    }

    public void Reset()
    {
       

      

        dtFinal = (DataTable)Session["dttProducts"];
        dtFinal.Rows.Clear();
        Session["dttProducts"] = dtFinal;
        htmldata = "";
      
    }





    public void FillDataListByPageId(int SubCategoryId)
    {



        repProducts.DataSource = new ProductsBLL().GetBySubCategoryId(SubCategoryId);
        repProducts.DataBind();

        updProducts.Update();


    }

    public void FillDataListByCategoryLevel3(int SubCategoryId)
    {



        repProducts.DataSource = new ProductsBLL().GetByCategoryLevel3(SubCategoryId);
        repProducts.DataBind();

        updProducts.Update();


    }
    public void FillDataListByCategoryLevel1(int Categoryid)
    {

        repProducts.DataSource = new ProductsBLL().GetByCategoryLevel1(Categoryid);
        repProducts.DataBind();
        updProducts.Update();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        InsertUpdate();
    }
    [WebMethod]
    public static string EditTempById(int Id)
    {
        DataTable dttt = new DataTable();
        dttt.Clear();
        dttt = (DataTable)HttpContext.Current.Session["dttProducts"];
        var desc = "";
        var icode = "";
        var unit = "";
        var qty = "";
        var type = "";
        var price = "";
        var mrp = "";
        var hdVId = "";

        desc = dttt.Rows[Id]["Description"].ToString();
        icode = dttt.Rows[Id]["ItemCode"].ToString();
        unit = dttt.Rows[Id]["Unit"].ToString();
        qty = dttt.Rows[Id]["Qty"].ToString();
        type = dttt.Rows[Id]["Type"].ToString();
        price = dttt.Rows[Id]["Price"].ToString();
        mrp = dttt.Rows[Id]["Mrp"].ToString();
        hdVId = Id.ToString();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            Status = 1,
            Desc = desc,
            ICode = icode,
            Unit = unit,
            Qty = qty,
            Type = type,
            Price = price,
            Mrp = mrp,
            HdVid = Id
        };
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string DeleteTempById(int Id)
    {
        string data = "";
        DataTable dttt = new DataTable();
        dttt.Clear();
        dttt = (DataTable)HttpContext.Current.Session["dttProducts"];
        string path = HttpContext.Current.Request.MapPath("../ProductImages/T_" + dttt.Rows[Id]["PhotoUrl"].ToString());
        if (System.IO.File.Exists(path) == true)
        {
            System.IO.File.Delete(path);
        }
        string path1 = HttpContext.Current.Request.MapPath("../ProductImages/" + dttt.Rows[Id]["PhotoUrl"].ToString());
        if (System.IO.File.Exists(path1) == true)
        {
            System.IO.File.Delete(path1);
        }
        dttt.Rows.RemoveAt(Id);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        for (int i = 0; i < dttt.Rows.Count; i++)
        {
            data = data + "<tr><td><img style='height:30px;width:30px' src='../ProductImages/" + dttt.Rows[i]["PhotoUrl"] + "' /></td><td>" + dttt.Rows[i]["ItemCode"] + "</td><td>" + dttt.Rows[i]["Unit"] + "</td><td>" + dttt.Rows[i]["type"] + "</td>" +
                "<td>" + dttt.Rows[i]["Qty"] + "</td><td>" + dttt.Rows[i]["Price"] + "</td><td>" + dttt.Rows[i]["Mrp"] + "</td><td>" + dttt.Rows[i]["Description"] + "</td><td> <div class='btn btn-primary btn-small' name='btnEdit' id='btnEdit_" + dttt.Rows[i]["Id"] + "'>Edit</div></td><td> <div class='btn btn-primary btn-small' name='btnDelete' id='btn_" + dttt.Rows[i]["Id"] + "'>Delete</div></td></tr>";
        }
        HttpContext.Current.Session["dttProducts"] = dttt;
        var JsonData = new
        {
            Status = 1,
            DetailData = data
        };


        return ser.Serialize(JsonData);

    }
    protected void btnAddToTemp_Click(object sender, EventArgs e)
    {
        DatatableInsert();
    }

    public void DatatableInsert()
    {
        ////string url = "";
        ////dtFinal = (DataTable)Session["dttProducts"];
        ////if (hdVariationId.Value == "-1")
        ////{
        ////    Int32 count = dtFinal.Rows.Count;
        ////    if (FileUpload1.HasFile)
        ////    {
        ////        url = CommonFunctions.UploadImage(FileUpload1, "~/ProductImages/", true, 150, 150, false, 0, 0);
        ////    }
        ////    else
        ////    {
        ////        if (count == 0)
        ////        {
        ////            Response.Write("<script>alert('Choose Image');</script>");
        ////            return;
        ////        }
        ////    }
        ////    dtFinal.Rows.Add();
        ////    dtFinal.Rows[dtFinal.Rows.Count - 1]["Unit"] = ddlUnits.Text;
        ////    dtFinal.Rows[dtFinal.Rows.Count - 1]["Qty"] = Convert.ToDecimal(txtQty.Text);
        ////    dtFinal.Rows[dtFinal.Rows.Count - 1]["Price"] = Convert.ToDecimal(txtPrice.Text);
        ////    dtFinal.Rows[dtFinal.Rows.Count - 1]["Mrp"] = Convert.ToDecimal(txtMRP.Text);
        ////    dtFinal.Rows[dtFinal.Rows.Count - 1]["Description"] = txtDescr.Text;
        ////    dtFinal.Rows[dtFinal.Rows.Count - 1]["type"] = ddltype.Text;
        ////    dtFinal.Rows[dtFinal.Rows.Count - 1]["PhotoUrl"] = url;
        ////    dtFinal.Rows[dtFinal.Rows.Count - 1]["ItemCode"] = txtItemCode.Text;
        //}
        //else
        //{
        //    if (FileUpload1.HasFile)
        //    {
        //        url = CommonFunctions.UploadImage(FileUpload1, "~/ProductImages/", true, 150, 150, false, 0, 0);
        //    }
        //    dtFinal.Rows[Convert.ToInt32(hdVariationId.Value)]["Unit"] = ddlUnits.Text;
        //    dtFinal.Rows[Convert.ToInt32(hdVariationId.Value)]["Qty"] = Convert.ToDecimal(txtQty.Text);
        //    dtFinal.Rows[Convert.ToInt32(hdVariationId.Value)]["Price"] = Convert.ToDecimal(txtPrice.Text);
        //    dtFinal.Rows[Convert.ToInt32(hdVariationId.Value)]["Mrp"] = Convert.ToDecimal(txtMRP.Text);
        //    dtFinal.Rows[Convert.ToInt32(hdVariationId.Value)]["Description"] = txtDescr.Text;
        //    dtFinal.Rows[Convert.ToInt32(hdVariationId.Value)]["type"] = ddltype.Text;
        //    if (url.ToString().Trim() != "")
        //    {
        //        dtFinal.Rows[Convert.ToInt32(hdVariationId.Value)]["PhotoUrl"] = url;
        //    }
        //    dtFinal.Rows[Convert.ToInt32(hdVariationId.Value)]["ItemCode"] = txtItemCode.Text;
        //}
        //int id = Convert.ToInt32(dtFinal.Rows[dtFinal.Rows.Count - 1]["Id"]);
        //htmldata = "";
        //for (int i = 0; i < dtFinal.Rows.Count; i++)
        //{
        //    if (i != 0 && dtFinal.Rows[i]["PhotoUrl"].ToString().Trim() == "")
        //    {
        //        dtFinal.Rows[i]["PhotoUrl"] = dtFinal.Rows[0]["PhotoUrl"].ToString();
        //    }
        //    htmldata = htmldata + "<tr><td><img style='height:30px;width:30px' src='../ProductImages/" + dtFinal.Rows[i]["PhotoUrl"] + "' /></td><td>" + dtFinal.Rows[i]["ItemCode"] + "</td><td>" + dtFinal.Rows[i]["Unit"] + "</td><td>" + dtFinal.Rows[i]["type"] + "</td>" +
        //        "<td>" + dtFinal.Rows[i]["Qty"] + "</td><td>" + dtFinal.Rows[i]["Price"] + "</td><td>" + dtFinal.Rows[i]["Mrp"] + "</td><td>" + dtFinal.Rows[i]["Description"] + "</td><td> <div class='btn btn-primary btn-small' name='btnEdit' id='btnEdit_" + id + "'>Edit</div></td><td> <div class='btn btn-primary btn-small' name='btnDelete' id='btn_" + id + "'>Delete</div></td></tr>";
        //}

        //btnCancelPrice_Click(null, null);
        //Session["dttProducts"] = dtFinal;
    }
    public void InsertUpdate()
    {
    //    if (ddlProductCategory3.Items.Count > 1 && ddlProductCategory3.SelectedIndex == 0)
    //    {
    //        ErrorMessage = "Please select Level 3 Category.";
    //        dtFinal = (DataTable)Session["dttProducts"];
    //        htmldata = "";
    //        for (int i = 0; i < dtFinal.Rows.Count; i++)
    //        {
    //            if (i != 0 && dtFinal.Rows[i]["PhotoUrl"].ToString().Trim() == "")
    //            {
    //                dtFinal.Rows[i]["PhotoUrl"] = dtFinal.Rows[0]["PhotoUrl"].ToString();
    //            }
    //            htmldata = htmldata + "<tr><td><img style='height:30px;width:30px' src='../ProductImages/" + dtFinal.Rows[i]["PhotoUrl"] + "' /></td><td>" + dtFinal.Rows[i]["ItemCode"] + "</td><td>" + dtFinal.Rows[i]["Unit"] + "</td><td>" + dtFinal.Rows[i]["type"] + "</td>" +
    //                "<td>" + dtFinal.Rows[i]["Qty"] + "</td><td>" + dtFinal.Rows[i]["Price"] + "</td><td>" + dtFinal.Rows[i]["Mrp"] + "</td><td>" + dtFinal.Rows[i]["Description"] + "</td><td> <div class='btn btn-primary btn-small' name='btnEdit' id='btnEdit_" + dtFinal.Rows[i]["Id"] + "'>Edit</div></td><td> <div class='btn btn-primary btn-small' name='btnDelete' id='btn_" + dtFinal.Rows[i]["Id"] + "'>Delete</div></td></tr>";
    //        }
    //        ddlProductCategory2.Focus();
    //        return;
    //    }
    //    if (ddlProductCategory2.SelectedValue == "--Select--")
    //    {
    //        ErrorMessage = "Please Choose Sub Category.";
    //        ddlProductCategory2.Focus();
    //        return;
    //    }
    //    dtFinal = (DataTable)Session["dttProducts"];
    //    Int32 count = dtFinal.Rows.Count;
    //    if (count == 0)
    //    {
    //        Response.Write("<script>alert('Choose atleast one Price List');</script>");
    //        //ddlUnits.Focus();
    //        return;
    //    }
    //    Products objProducts = new Products()
    //       {
    //           ProductId = Convert.ToInt32(hdnProductId.Value),
    //           CategoryId = Convert.ToInt32(ddlProductCategory1.SelectedValue),
    //           SubCategoryId = Convert.ToInt32(ddlProductCategory2.SelectedValue),
    //           CategoryLevel3 = Convert.ToInt32(ddlProductCategory3.SelectedValue),
             
    //       };
    //    try
    //    {
    //        int Status = new ProductsBLL().InsertUpdate(objProducts, dtFinal);
    //        if (Status == 0)
    //        {
    //            ErrorMessage = "Operation Failed.Please try again.";
    //        }
    //        else
    //        {
    //            if (Convert.ToInt32(hdnProductId.Value) == 0)
    //            {
    //                ErrorMessage = "Product  Added  Successfully!";
    //            }
    //            else
    //            {
    //                ErrorMessage = "Product  Updated  Successfully!";
    //            }

    //            if (ddlCategoryLevel3.SelectedIndex > 0)
    //            {
    //                FillDataListByCategoryLevel3(Convert.ToInt32(ddlCategoryLevel3.SelectedValue));
    //            }
    //            else if (ddlSubCategories.SelectedIndex > 0)
    //            {
    //                FillDataListByPageId(Convert.ToInt32(ddlSubCategories.SelectedValue));
    //            }
    //            else if (ddlCategory.SelectedIndex > 0)
    //            {
    //                FillDataListByCategoryLevel1(Convert.ToInt32(ddlCategory.SelectedValue));
    //            }
    //            Reset();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Response.Write("<script>alert('" + ex.Message + "')</script>");
    //        ErrorMessage = ex.Message;
    //    }

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {

        InsertUpdate();

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Reset();
        
        ViewState["currentPage"] = 0;

        ddlSubCategories.Items.Clear();
        ListItem li = new ListItem();
        li.Text = "--Category Level 2--";
        li.Value = "0";
        ddlSubCategories.Items.Insert(0, li);

        ddlCategoryLevel3.Items.Clear();
        ListItem li2 = new ListItem();
        li2.Text = "--Category Level 3--";
        li2.Value = "0";
        ddlCategoryLevel3.Items.Insert(0, li2);
        if (ddlCategory.SelectedIndex > 0)
        {
            BindSubCategories(Convert.ToInt32(ddlCategory.SelectedValue), ddlCategory, ddlSubCategories);
            FillDataListByCategoryLevel1(Convert.ToInt32(ddlCategory.SelectedValue));
        }
        else
        {
            catLevel3.Visible = false;
            repProducts.DataSource = null;
            repProducts.DataBind();

           
        }
    }
    protected void ddlSubCategories_SelectedIndexChanged(object sender, EventArgs e)
    {
        Reset();
        ViewState["currentPage"] = 0;
        updProducts.Update();
        if (ddlSubCategories.SelectedIndex > 0)
        {
            BindCategoryLevel3(Convert.ToInt32(ddlSubCategories.SelectedValue), ddlCategoryLevel3);
            repProducts.DataSource = null;
            repProducts.DataBind();
            if (ddlCategoryLevel3.Items.Count == 1)
            {
                catLevel3.Visible = false;
            }
            else
            {
                catLevel3.Visible = true;
            }
            FillDataListByPageId(Convert.ToInt32(ddlSubCategories.SelectedValue));
        }
        else
        {
            ddlCategoryLevel3.Items.Clear();
            ListItem li = new ListItem();
            li.Text = "--Category Level 3--";
            li.Value = "0";
            ddlCategoryLevel3.Items.Insert(0, li);
            repProducts.DataSource = null;
            repProducts.DataBind();
            FillDataListByCategoryLevel1(Convert.ToInt32(ddlCategory.SelectedValue));
        }
    }
    protected void ddlCategoryLevel3_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["currentPage"] = 0;

        if (ddlCategoryLevel3.SelectedIndex > 0)
        {
            FillDataListByCategoryLevel3(Convert.ToInt32(ddlCategoryLevel3.SelectedValue));
        }
        else
        {
            FillDataListByPageId(Convert.ToInt32(ddlSubCategories.SelectedValue));

        }
    }
    protected void repProducts_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
       if (e.CommandName == "edit")
      {
    //        int ProductId = Convert.ToInt32(((HiddenField)e.Item.FindControl("hdnRepProductId")).Value);
    //        hdnProductId.Value = ProductId.ToString();
    //        SqlDataReader dr = new ProductsDAL().GetByProductId(ProductId);
    //        if (dr.HasRows == true)
    //        {
    //            if (dr.Read())
    //            {
                   
    //                ddlProductCategory1.SelectedValue = dr["CategoryId"].ToString();
    //                BindSubCategories(Convert.ToInt32(ddlProductCategory1.SelectedValue), ddlProductCategory1, ddlProductCategory2);
    //                ddlProductCategory2.SelectedValue = dr["SubCategoryId"].ToString();
    //                BindCategoryLevel3(Convert.ToInt32(ddlProductCategory2.SelectedValue), ddlProductCategory3);
    //                ddlProductCategory3.SelectedValue = dr["CategoryLevel3"].ToString();
    //                if (ddlProductCategory3.SelectedValue == "")
    //                {
    //                    ProductcatLevel3.Visible = false;
    //                }
    //                else
    //                {
    //                    ProductcatLevel3.Visible = true;
    //                }
                   
    //                btnUpdate.Visible = true;
    //                btnCancel.Visible = true;
    //                btnAdd.Visible = false;
    //            }
    //        }
    //        dr.NextResult();
    //        StringBuilder str = new StringBuilder();
    //        dtFinal = (DataTable)Session["dttProducts"];
    //        dtFinal.Rows.Clear();
    //        if (dr.HasRows)
    //        {
    //            while (dr.Read())
    //            {
    //                dtFinal.Rows.Add();
    //                dtFinal.Rows[dtFinal.Rows.Count - 1]["PhotoUrl"] = dr["PhotoUrl"].ToString();
    //                dtFinal.Rows[dtFinal.Rows.Count - 1]["Unit"] = dr["Unit"].ToString();
    //                dtFinal.Rows[dtFinal.Rows.Count - 1]["type"] = dr["type"].ToString();
    //                dtFinal.Rows[dtFinal.Rows.Count - 1]["Qty"] = dr["Qty"].ToString();
    //                dtFinal.Rows[dtFinal.Rows.Count - 1]["Price"] = dr["Price"].ToString();
    //                dtFinal.Rows[dtFinal.Rows.Count - 1]["Mrp"] = dr["Mrp"].ToString();
    //                dtFinal.Rows[dtFinal.Rows.Count - 1]["Description"] = dr["Description"].ToString();
    //                dtFinal.Rows[dtFinal.Rows.Count - 1]["ItemCode"] = dr["ItemCode"].ToString();
    //                str.Append("<tr><td><img style='height:30px;width:30px' src='../ProductImages/" + dr["PhotoUrl"].ToString() + "' /></td><td>" + dr["ItemCode"].ToString() + "</td><td>" + dr["Unit"].ToString() + "</td><td>" + dr["type"].ToString() + "</td><td>" + dr["Qty"].ToString() + "</td><td>" + dr["Price"].ToString() + "</td><td>" + dr["Mrp"].ToString() + "</td><td>" + dr["Description"].ToString() + "</td><td> <div class='btn btn-primary btn-small' name='btnEdit' id='btnEdit_" + dr["variationid"].ToString() + "'>Edit</div></td><td> <div class='btn btn-primary btn-small' name='btnDelete' id='btn_" + dr["variationid"].ToString() + "'>Delete</div></td></tr>");
    //            }
    //        }
    //        htmldata = str.ToString();
     }
    //    Session["dttProducts"] = dtFinal;
    }
    protected void repProducts_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
    //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //    {

    //        PostBackTrigger trigger = new PostBackTrigger();

    //        LinkButton firstButton = (LinkButton)e.Item.FindControl("Lnkedit");

    //        trigger.ControlID = firstButton.UniqueID;

    //        updProducts.Triggers.Add(trigger);
    //        Scrpt1.RegisterPostBackControl(firstButton);
    //    }
    }
    //protected void ddlProductCategory1_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    ddlProductCategory2.Items.Clear();
    //    ListItem li22 = new ListItem();
    //    li22.Text = "--Category Level 2--";
    //    li22.Value = "0";
    //    ddlProductCategory2.Items.Insert(0, li22);

    //    ddlProductCategory3.Items.Clear();
    //    ListItem li2 = new ListItem();
    //    li2.Text = "--Category Level 3--";
    //    li2.Value = "0";
    //    ddlProductCategory3.Items.Insert(0, li2);
    //    if (ddlProductCategory1.SelectedIndex > 0)
    //    {
    //        BindSubCategories(Convert.ToInt32(ddlProductCategory1.SelectedValue), ddlProductCategory1, ddlProductCategory2);
    //    }
    //    else
    //    {
    //        ProductcatLevel3.Visible = false;
    //    }
    //    dtFinal = (DataTable)Session["dttProducts"];
    //    htmldata = "";
    //    for (int i = 0; i < dtFinal.Rows.Count; i++)
    //    {
    //        if (i != 0 && dtFinal.Rows[i]["PhotoUrl"].ToString().Trim() == "")
    //        {
    //            dtFinal.Rows[i]["PhotoUrl"] = dtFinal.Rows[0]["PhotoUrl"].ToString();
    //        }
    //        htmldata = htmldata + "<tr><td><img style='height:30px;width:30px' src='../ProductImages/" + dtFinal.Rows[i]["PhotoUrl"] + "' /></td><td>" + dtFinal.Rows[i]["ItemCode"] + "</td><td>" + dtFinal.Rows[i]["Unit"] + "</td><td>" + dtFinal.Rows[i]["type"] + "</td>" +
    //            "<td>" + dtFinal.Rows[i]["Qty"] + "</td><td>" + dtFinal.Rows[i]["Price"] + "</td><td>" + dtFinal.Rows[i]["Mrp"] + "</td><td>" + dtFinal.Rows[i]["Description"] + "</td><td> <div class='btn btn-primary btn-small' name='btnEdit' id='btnEdit_" + dtFinal.Rows[i]["Id"] + "'>Edit</div></td><td> <div class='btn btn-primary btn-small' name='btnDelete' id='btn_" + dtFinal.Rows[i]["Id"] + "'>Delete</div></td></tr>";
    //    }
    //}
    //protected void ddlProductCategory2_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    updProducts.Update();
    //    if (ddlProductCategory2.SelectedIndex > 0)
    //    {
    //        BindCategoryLevel3(Convert.ToInt32(ddlProductCategory2.SelectedValue), ddlProductCategory3);
    //        if (ddlProductCategory3.Items.Count == 1)
    //        {
    //            ProductcatLevel3.Visible = false;
    //        }
    //        else
    //        {
    //            ProductcatLevel3.Visible = true;
    //        }
    //    }
    //    else
    //    {
    //        ddlProductCategory3.Items.Clear();
    //        ListItem li = new ListItem();
    //        li.Text = "--Category Level 3--";
    //        li.Value = "0";
    //        ddlProductCategory3.Items.Insert(0, li);
    //    }
    //    htmldata = "";
    //    dtFinal = (DataTable)Session["dttProducts"];
    //    for (int i = 0; i < dtFinal.Rows.Count; i++)
    //    {
    //        if (i != 0 && dtFinal.Rows[i]["PhotoUrl"].ToString().Trim() == "")
    //        {
    //            dtFinal.Rows[i]["PhotoUrl"] = dtFinal.Rows[0]["PhotoUrl"].ToString();
    //        }
    //        htmldata = htmldata + "<tr><td><img style='height:30px;width:30px' src='../ProductImages/" + dtFinal.Rows[i]["PhotoUrl"] + "' /></td><td>" + dtFinal.Rows[i]["ItemCode"] + "</td><td>" + dtFinal.Rows[i]["Unit"] + "</td><td>" + dtFinal.Rows[i]["type"] + "</td>" +
    //            "<td>" + dtFinal.Rows[i]["Qty"] + "</td><td>" + dtFinal.Rows[i]["Price"] + "</td><td>" + dtFinal.Rows[i]["Mrp"] + "</td><td>" + dtFinal.Rows[i]["Description"] + "</td><td> <div class='btn btn-primary btn-small' name='btnEdit' id='btnEdit_" + dtFinal.Rows[i]["Id"] + "'>Edit</div></td><td> <div class='btn btn-primary btn-small' name='btnDelete' id='btn_" + dtFinal.Rows[i]["Id"] + "'>Delete</div></td></tr>";
    //    }
    //}

    protected void btnCancelPrice_Click(object sender, EventArgs e)
    {
       
        hdVariationId.Value = "-1";
     
        dtFinal = (DataTable)Session["dttProducts"];
        htmldata = "";
        for (int i = 0; i < dtFinal.Rows.Count; i++)
        {
            if (i != 0 && dtFinal.Rows[i]["PhotoUrl"].ToString().Trim() == "")
            {
                dtFinal.Rows[i]["PhotoUrl"] = dtFinal.Rows[0]["PhotoUrl"].ToString();
            }
            htmldata = htmldata + "<tr><td><img style='height:30px;width:30px' src='../ProductImages/" + dtFinal.Rows[i]["PhotoUrl"] + "' /></td><td>" + dtFinal.Rows[i]["ItemCode"] + "</td><td>" + dtFinal.Rows[i]["Unit"] + "</td><td>" + dtFinal.Rows[i]["type"] + "</td>" +
                "<td>" + dtFinal.Rows[i]["Qty"] + "</td><td>" + dtFinal.Rows[i]["Price"] + "</td><td>" + dtFinal.Rows[i]["Mrp"] + "</td><td>" + dtFinal.Rows[i]["Description"] + "</td><td> <div class='btn btn-primary btn-small' name='btnEdit' id='btnEdit_" + dtFinal.Rows[i]["Id"] + "'>Edit</div></td><td> <div class='btn btn-primary btn-small' name='btnDelete' id='btn_" + dtFinal.Rows[i]["Id"] + "'>Delete</div></td></tr>";
        }
    }
    protected void ddlnewCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Reset();
       

        ViewState["currentPage"] = 0;

        ddlnewSubCategories.Items.Clear();
        ListItem li = new ListItem();
        li.Text = "--Category Level 2--";
        li.Value = "0";
        ddlnewSubCategories.Items.Insert(0, li);

        ddlnewCategoryLevel3.Items.Clear();
        ListItem li2 = new ListItem();
        li2.Text = "--Category Level 3--";
        li2.Value = "0";
        ddlnewCategoryLevel3.Items.Insert(0, li2);
        if (ddlnewCategory.SelectedIndex > 0)
        {
            BindSubCategories(Convert.ToInt32(ddlnewCategory.SelectedValue), ddlnewCategory, ddlnewSubCategories);
            //FillDataListByCategoryLevel1(Convert.ToInt32(ddlCategory.SelectedValue));
        }
        else
        {
            catLevel3.Visible = false;
            repProducts.DataSource = null;
            repProducts.DataBind();

        }



    }
    protected void ddlnewSubCategories_SelectedIndexChanged(object sender, EventArgs e)
    {
        Reset();
        ViewState["currentPage"] = 0;
        updProducts.Update();
        if (ddlnewSubCategories.SelectedIndex > 0)
        {
            BindCategoryLevel3(Convert.ToInt32(ddlnewSubCategories.SelectedValue), ddlnewCategoryLevel3);
           
            if (ddlnewCategoryLevel3.Items.Count == 1)
            {
                Tr1.Visible = false;
            }
            else
            {
                Tr1.Visible = true;
            }
           // FillDataListByPageId(Convert.ToInt32(ddlnewSubCategories.SelectedValue));
        }
        else
        {
            ddlnewCategoryLevel3.Items.Clear();
            ListItem li = new ListItem();
            li.Text = "--Category Level 3--";
            li.Value = "0";
            ddlnewCategoryLevel3.Items.Insert(0, li);
           
            //FillDataListByCategoryLevel1(Convert.ToInt32(ddlCategory.SelectedValue));
        }


    }
    protected void ddlnewCategoryLevel3_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["currentPage"] = 0;
    }




    [WebMethod]
    public static string Insert(int oldCategory, int oldSubCategory, int oldCategory3, int Category, int SubCategory, int Category3, string status1, string arrProduct)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("ProductId");
        if (arrProduct != "")
        {
            string[] ProductData = arrProduct.Split(',');



            

            for (int i = 0; i < ProductData.Length; i++)
            {
                DataRow dr = dt.NewRow();
                dr["ProductId"] = Convert.ToInt16(ProductData[i]);
                dt.Rows.Add(dr);
            }
        }

        int Status = new MigrationDAL().UpdateMigrateProducts(oldCategory, oldSubCategory, oldCategory3, Category, SubCategory, Category3, status1, dt);

        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Status = Status,
       

        };
        return ser.Serialize(JsonData);
    }






    //protected void btnmigrate_Click(object sender, EventArgs e)
    //{
    //    if (repProducts.Items.Count == 0)
    //    {
    //        Response.Write("<script>alert('Choose Products to Migrate');</script>");
    //        return;
    //    }
    //    if (ddlnewCategoryLevel3.Items.Count > 1 && ddlnewCategoryLevel3.SelectedIndex == 0)
    //    {
    //        Response.Write("<script>alert('Choose Category Level 3');</script>");
    //        ddlCategoryLevel3.Focus();
    //        return;
    //    }

    //      string status ="";
    //      DataTable dt = new DataTable();
    //      dt.Columns.Add("ProductId");

    //      for (int i = 0; i < repProducts.Items.Count; i++)
    //      {

    //          dt.Rows.Add();
    //          HiddenField hdProductid = (HiddenField)repProducts.Items[i].FindControl("hdnRepProductId");
    //          CheckBox chk = (CheckBox)repProducts.Items[i].FindControl("chkactive_" + hdProductid);
    //          if (chk.Checked)
    //          {
    //              dt.Rows[i]["ProductId"] = hdProductid.Value;
    //          }
             
    //      }

    //      int Status = new MigrationDAL().UpdateMigrateProducts(Convert.ToInt32(ddlnewCategory.SelectedValue),Convert.ToInt32( ddlnewSubCategories.SelectedValue),Convert.ToInt32(ddlnewCategoryLevel3.SelectedValue), status,dt);
           
    //}
}