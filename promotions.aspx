﻿<%@ Page Title="" Language="C#" MasterPageFile="main.master" AutoEventWireup="true" CodeFile="promotions.aspx.cs" Inherits="promotions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cnMaster" Runat="Server">
 <%--<script src="jquery-1.4.4.min.js" type="text/javascript"></script>--%>
    <script src="js/jquery.min.js" type="text/javascript"></script>
     <script src="js/jquery.uilocksearch.js" type="text/javascript"></script>



 
  <link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="css/jquery.notifyBar.css">
   <script src="jquery.notifyBar.js"></script>



   <script language="javascript" type="text/javascript">
       var m_MinPrice = -1;
       var m_MaxPrice = -1;
       var selValues = "";
     


     



      



       $(document).on("click", "div[name='decr']", function (event) {
           var data = $(this).attr("id");


           var arrData = data.split('_');
           var vid = arrData[1];
           $("#cqty_" + vid).css("height", "20px");

           //        $("#cqty_" + vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");


           var tempQty = $("#cqty_" + vid).html();
           var tempQ = Number(tempQty) - 1;
           if (tempQ <= 1) {
               tempQ = 1;
           }
           $("#cqty_" + vid).html(tempQ);


           var qty = 1;
           var st = "m";
           var type = "Product";
           ATC(vid, qty, st, type);
       });


       $(document).on("click", "div[name='incr']", function (event) {


           var data = $(this).attr("id");
           var arrData = data.split('_');
           var vid = arrData[1];
           var qty = 1;
           var st = "p";
           var type = "Product";
           $("#cqty_" + vid).css("height", "20px");

           //        $("#cqty_" + vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");



           var tempQty = $("#cqty_" + vid).html();
           var tempQ = Number(tempQty) + 1;

           $("#cqty_" + vid).html(tempQ);

           ATC(vid, qty, st, type);



       });





       function ATC(vid, qty, st, type) {
           $.ajax({
               type: "POST",
               data: '{"vid":"' + vid + '","qty":"' + qty + '","st":"' + st + '","type":"' + type + '"}',
               url: "promotions.aspx/FirstTimeATC",
               async: true,
               contentType: "application/json",
               dataType: "json",
               success: function (msg) {


                   var obj = jQuery.parseJSON(msg.d);

                   if (obj.error == 1) {

                       alert("An Error occured during transaction. Please refresh the page and try again Later.");
                       return;
                   }


                   if (type == "Combo") {

                       $("#cartcontainer").html(obj.cartHTML);

                       $("#cbox_" + obj.pid).html(obj.cartHTML);


                       if (obj.qty <= 0) {

                           $("#cbox_" + obj.pid).css("background", "white")

                       }
                       else {

                           $("#cbox_" + obj.pid).css("background", "#D8EDC0")

                       }



                   }
                   else {


                       //$("#cartcontainer").html(obj.cartHTML);

                       $("#schemeQty").html(obj.SchemeQty);
                       $("#variation_" + vid).html(obj.productHTML);


                       //                    if (obj.qty <= 0) {

                       //                        $("#box_" + obj.pid).css("background", "white")

                       //                    }
                       //                    else {

                       //                        $("#box_" + obj.pid).css("background", "#D8EDC0")

                       //                    }

                   }


                   //$("#cqty_" + obj.pid).html(totalQ);


                   BindCart();
               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function (msg) {


               }

           });

       }

       $(document).on("click", "button[name='btnAddToCart']", function (event) {

           var data = $(this).attr("id");
           var arrData = data.split('_');
           var vid = arrData[1];
           var qty = $("#q_" + arrData[1]).val();
           var st = "p";
           var type = "Product";

           $("#a_" + vid).css("width", "55px");
           $("#a_" + vid).html("<img src='images/loaderadd.gif' style='margin-top:2px'   alt='.....'/>");



           ATC(vid, qty, st, type);
           $.notifyBar({ cssClass: "success", html: "Product Added Succcessfully!" });

       });


       $(document).on("click", "div[name='cartminus']", function (event) {

           var data = $(this).attr("id");
           var arrData = data.split('_');
           var vid = arrData[1];
           var qty = 1;
           var st = "m";
           var type = "Product";
           $("#cq_" + vid).css("height", "20px");

           $("#cq_" + vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");

           ATC(vid, qty, st, type);

       });

       $(document).on("click", "div[name='cartadd']", function (event) {



           var data = $(this).attr("id");
           var arrData = data.split('_');
           var vid = arrData[1];
           var qty = 1;
           var st = "p";
           var type = "Product";
           $("#cq_" + vid).css("height", "20px");

           $("#cq_" + vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");

           ATC(vid, qty, st, type);


       });



      
     
  

 

  


 


</script>




<div class="row" style="margin-top:15px;">
<div class="col-md-5"   >
 <table>
 
 <tr><td style="font-size:28px;color:#4D4D4D"><asp:Literal ID="ltSchemeTitle" runat="server"></asp:Literal></td></tr>
 <tr><td>
 
 <asp:Literal ID="ltSchemeDesc" runat="server"></asp:Literal></td></tr>
 <tr><td style="font-size:10px;color:green;padding:10px 0px 10px 0px">
 
 * Promotional Scheme valid till  <asp:Literal ID="ltValidUpTo" runat="server"></asp:Literal></td></tr>
 
 
  

    <tr><td>
    <div style="padding:1px;border:solid 1px silver;width:260px;height:101px;float:left;text-align:center;border-radius:5px;background:#B3DD84">
  <div style="width: 100px; font-size:68px;padding:0px;margin:0px;float:left;background:#E2F2D0" id="schemeQty">
  
    <asp:Literal ID="ltTotal" runat="server">0</asp:Literal>  
  </div>
  <div style="float:left;width:150px;font-size:30px">
  ITEMS IN CART
  </div>
  
    </div>
     

    
    </td></tr>

 </table>
</div>

<div class="col-md-7"   >

 
<asp:Literal ID="ltSchemeProducts" runat="server" ></asp:Literal>
</div>
</div>



<div class="row" style="margin-top:15px;">
<div class="col-md-12" style="font-weight:bold;font-size:24px;color:#CCC7BD">
PESHAWARI PROMOTIONAL OFFERS
<hr />
</div>
</div>



<asp:ListView ID="lstOffers" GroupItemCount="3" runat="server">
<LayoutTemplate>


<asp:PlaceHolder ID="groupPlaceHolder" runat="server"></asp:PlaceHolder>

</LayoutTemplate>

<ItemTemplate>
<div class="col-md-4">
 <div id="promotion" style="text-align:center;margin:2px">
 <a href="promotions.aspx?sc=<%#Eval("SchemeId") %>"><img src='SchemeImages/<%#Eval("PhotoUrl") %>' class="img-responsive" alt=""></a>
 

 </div>
 </div>
</ItemTemplate>

<GroupTemplate>

<div class="row" style="">


<asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>


 


 </div>

</GroupTemplate>

</asp:ListView>




 


 
</asp:Content>

