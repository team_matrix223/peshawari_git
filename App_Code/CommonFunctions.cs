﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Drawing;

using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

/// <summary>
/// Summary description for CommonFunctions
/// </summary>
public class CommonFunctions
{




    public static int ValidateQuantity(string qty)
    {

        int n;
        bool isNumeric = int.TryParse(qty, out n);

        if (!isNumeric)
        {
            n = 1;
        }
        else if (n > 5)
        {
            n = 5;
        }
        return n;
    }
    public static Int64 IsNumeric(string value)
    {

        Int64 n;
        bool isNumeric = Int64.TryParse(value, out n);

        if (!isNumeric)
        {
            n = 0;
        }
         
        return n;
    }
    public static bool IsValidProductId(string pid)
    {

        int n;
        bool isNumeric = int.TryParse(pid, out n);
        return isNumeric;
        
    }
    public static string ReplaceWhiteSpaces(string Name)
    {
        return Name.Replace(' ', '-');
    }
    public static string ReplaceHyphen(string Name)
    {
        return Name.Replace('-', ' ');
    }
    public static Int32 GetImageWidth(string strPicName)
    {
        int iWidth = -1;
        if (File.Exists(strPicName))
        {
            Bitmap bitMap = new Bitmap(strPicName);
            iWidth = bitMap.Width;

            if (iWidth > 500)
            {
                iWidth = 500;
            }
          
        }
        return iWidth;
    }
    public static Int32 GetImageHeight(string strPicName)
    {

        int iHeight = -1;
        if (File.Exists(strPicName))
        {
            Bitmap bitMap = new Bitmap(strPicName);
            iHeight = bitMap.Height;
            if (iHeight > 500)
            {
                iHeight = 500;
            }
        }
        return iHeight;
    }
    public static string ChangeDateFormat(object Date)
    {
        DateTime dt = Convert.ToDateTime(Date);
        return dt.ToString("f");
    }
    public static string ChangeDateFormatShortDate(object Date)
    {
        DateTime dt = Convert.ToDateTime(Date);
        return dt.ToString("D");
    }

    public static string GenerateRandomUserCode(int codeCount)
    {
        string allChar = "0,1,2,3,4,5,6,7,8,9";
        string[] allCharArray = allChar.Split(',');
        string randomCode = "";
        int temp = -1;

        Random rand = new Random();
        for (int i = 0; i < codeCount; i++)
        {
            if (temp != -1)
            {
                rand = new Random(i * temp * ((int)DateTime.Now.Ticks));
            }
            int t = rand.Next(10);
            if (temp != -1 && temp == t)
            {
                return GenerateRandomUserCode(codeCount);
            }
            temp = t;
            randomCode += allCharArray[t];
        }
        return randomCode;
    }
    public static string GenerateRandomPassword(int codeCount)
    {
        string allChar = "0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
        string[] allCharArray = allChar.Split(',');
        string randomCode = "";
        int temp = -1;

        Random rand = new Random();
        for (int i = 0; i < codeCount; i++)
        {
            if (temp != -1)
            {
                rand = new Random(i * temp * ((int)DateTime.Now.Ticks));
            }
            int t = rand.Next(36);
            if (temp != -1 && temp == t)
            {
                return GenerateRandomPassword(codeCount);
            }
            temp = t;
            randomCode += allCharArray[t];
        }
        return randomCode;
    }


    public static string UploadImage(FileUpload ctl, string Path, bool CreateThumbNail1, int width, int height,bool CreateThumbNail2, int width2, int height2)
    {


        string sReturn = "";

       if (ctl.HasFile)
        {
            try
            {

                string sFileName = ctl.FileName;
                if (sFileName != "" || sFileName != null)
                {
                    string sPicType = Convert.ToString(ctl.PostedFile.ContentType);
                    if (sPicType == "image/png" || sPicType == "image/pjpeg" || sPicType == "image/jpeg" || sPicType == "image/gif" || sPicType == "image/png" || sPicType == "image/bmp")
                    {
                        sFileName = DateTime.Now.ToString() + DateTime.Now.Millisecond.ToString() + sFileName;
                        sFileName = sFileName.Replace(' ', '0').Replace(':', '1').Replace('/', '0').Replace('-', '0');
                        ctl.SaveAs(HttpContext.Current.Server.MapPath(Path + sFileName));


                        sReturn = sFileName;



                        Bitmap photo;
                        string photoName = sFileName;
                        string photoPath = HttpContext.Current.Server.MapPath(Path + photoName);
                        string PhotoPath2 = HttpContext.Current.Server.MapPath(Path);

                        try
                        {
                            photo = new Bitmap(photoPath);
                        }
                        catch (ArgumentException)
                        {
                            throw new HttpException(404, "Photo not found.");
                        }

                        if (CreateThumbNail1 == true)
                        {

                           


                            Bitmap target = new Bitmap(width, height);
                            using (Graphics graphics = Graphics.FromImage(target))
                            {
                                graphics.CompositingQuality = CompositingQuality.HighSpeed;
                                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                graphics.CompositingMode = CompositingMode.SourceCopy;
                                graphics.DrawImage(photo, 0, 0, width, height);


                                string newStrFileName = PhotoPath2 + "T_" + photoName;


                               target.Save(newStrFileName, ImageFormat.Jpeg);

                            }
                        }

                        if (CreateThumbNail2 == true)
                        {



                            Bitmap target2 = new Bitmap(width2, height2);
                            using (Graphics graphics2 = Graphics.FromImage(target2))
                            {
                                graphics2.CompositingQuality = CompositingQuality.HighSpeed;
                                graphics2.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                graphics2.CompositingMode = CompositingMode.SourceCopy;
                                graphics2.DrawImage(photo, 0, 0, width2, height2);


                                string newStrFileName2 = PhotoPath2 + "P_" + photoName;


                              target2.Save(newStrFileName2, ImageFormat.Jpeg);

                            }

                        }





                    }
                    else
                    {
                        throw new Exception("Only .jpeg,.jpg,.gif and .png images are allowed");
                    }
                }
            }
            finally { }
        } 
        return sReturn;
    }
  
    
    public static string UploadImage(FileUpload ctl, string Path, bool CreateThumbNail1, int width, int height, bool CreateThumbNail2, int width2, int height2,string Name)
    {


        string sReturn = "";

        if (ctl.HasFile)
        {
            try
            {

                string sFileName = ctl.FileName;
                if (sFileName != "" || sFileName != null)
                {
                    string sPicType = Convert.ToString(ctl.PostedFile.ContentType);
                    if (sPicType == "image/png" || sPicType == "image/pjpeg" || sPicType == "image/jpeg" || sPicType == "image/gif" || sPicType == "image/png" || sPicType == "image/bmp")
                    {
                        sFileName = Name; //DateTime.Now.ToString() + DateTime.Now.Millisecond.ToString() + sFileName;
                        //sFileName = sFileName.Replace(' ', '0').Replace(':', '1').Replace('/', '0').Replace('-', '0');
                        ctl.SaveAs(HttpContext.Current.Server.MapPath(Path + sFileName));


                        sReturn = sFileName;



                        Bitmap photo;
                        string photoName = sFileName;
                        string photoPath = HttpContext.Current.Server.MapPath(Path + photoName);
                        string PhotoPath2 = HttpContext.Current.Server.MapPath(Path);

                        try
                        {
                            photo = new Bitmap(photoPath);
                        }
                        catch (ArgumentException)
                        {
                            throw new HttpException(404, "Photo not found.");
                        }

                        if (CreateThumbNail1 == true)
                        {




                            Bitmap target = new Bitmap(width, height);
                            using (Graphics graphics = Graphics.FromImage(target))
                            {
                                graphics.CompositingQuality = CompositingQuality.HighSpeed;
                                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                graphics.CompositingMode = CompositingMode.SourceCopy;
                                graphics.DrawImage(photo, 0, 0, width, height);


                                string newStrFileName = PhotoPath2 + "T_" + photoName;


                                target.Save(newStrFileName, ImageFormat.Jpeg);

                            }
                        }

                        if (CreateThumbNail2 == true)
                        {



                            Bitmap target2 = new Bitmap(width2, height2);
                            using (Graphics graphics2 = Graphics.FromImage(target2))
                            {
                                graphics2.CompositingQuality = CompositingQuality.HighSpeed;
                                graphics2.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                graphics2.CompositingMode = CompositingMode.SourceCopy;
                                graphics2.DrawImage(photo, 0, 0, width2, height2);


                                string newStrFileName2 = PhotoPath2 + "P_" + photoName;


                                target2.Save(newStrFileName2, ImageFormat.Jpeg);

                            }

                        }





                    }
                    else
                    {
                        throw new Exception("Only .jpeg,.jpg,.gif and .png images are allowed");
                    }
                }
            }
            finally { }
        }
        return sReturn;
    }



    public static string UploadImage(HttpPostedFile ctl, string Path, bool CreateThumbNail1, int width, int height, bool CreateThumbNail2, int width2, int height2)
    {


        string sReturn = "";

        if (ctl.ContentLength>0)
        {
            try
            {

                string sFileName = ctl.FileName;
                if (sFileName != "" || sFileName != null)
                {
                    string sPicType = Convert.ToString(ctl.ContentType);
                    if (sPicType == "image/png" || sPicType == "image/pjpeg" || sPicType == "image/jpeg" || sPicType == "image/gif" || sPicType == "image/png" || sPicType == "image/bmp")
                    {
                        sFileName = DateTime.Now.ToString() + DateTime.Now.Millisecond.ToString() + sFileName;
                        sFileName = sFileName.Replace(' ', '0').Replace(':', '1').Replace('/', '0').Replace('-', '0');
                        ctl.SaveAs(HttpContext.Current.Server.MapPath(Path + sFileName));


                        sReturn = sFileName;



                        Bitmap photo;
                        string photoName = sFileName;
                        string photoPath = HttpContext.Current.Server.MapPath(Path + photoName);
                        string PhotoPath2 = HttpContext.Current.Server.MapPath(Path);

                        try
                        {
                            photo = new Bitmap(photoPath);
                        }
                        catch (ArgumentException)
                        {
                            throw new HttpException(404, "Photo not found.");
                        }

                        if (CreateThumbNail1 == true)
                        {




                            Bitmap target = new Bitmap(width, height);
                            using (Graphics graphics = Graphics.FromImage(target))
                            {
                                graphics.CompositingQuality = CompositingQuality.HighSpeed;
                                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                graphics.CompositingMode = CompositingMode.SourceCopy;
                                graphics.DrawImage(photo, 0, 0, width, height);


                                string newStrFileName = PhotoPath2 + "T_" + photoName;


                                target.Save(newStrFileName, ImageFormat.Jpeg);

                            }
                        }

                        if (CreateThumbNail2 == true)
                        {



                            Bitmap target2 = new Bitmap(width2, height2);
                            using (Graphics graphics2 = Graphics.FromImage(target2))
                            {
                                graphics2.CompositingQuality = CompositingQuality.HighSpeed;
                                graphics2.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                graphics2.CompositingMode = CompositingMode.SourceCopy;
                                graphics2.DrawImage(photo, 0, 0, width2, height2);


                                string newStrFileName2 = PhotoPath2 + "P_" + photoName;


                                target2.Save(newStrFileName2, ImageFormat.Jpeg);

                            }

                        }





                    }
                    else
                    {
                        throw new Exception("Only .jpeg,.jpg,.gif and .png images are allowed");
                    }
                }
            }
            finally { }
        }
        return sReturn;
    }

    public static string UploadPDF(FileUpload ctl, string Path, bool CreateThumbNail1, int width, int height, bool CreateThumbNail2, int width2, int height2)
    {


        string sReturn = "";

        if (ctl.HasFile)
        {
            try
            {

                string sFileName = ctl.FileName;
                if (sFileName != "" || sFileName != null)
                {
                    string sPicType = Convert.ToString(ctl.PostedFile.ContentType);

                    sFileName = DateTime.Now.ToString() + DateTime.Now.Millisecond.ToString() + sFileName;
                    sFileName = sFileName.Replace(' ', '0').Replace(':', '1').Replace('/', '0').Replace('-','0');
                    ctl.SaveAs(HttpContext.Current.Server.MapPath(Path + sFileName));


                    sReturn = sFileName;



                   







                }
            }
            finally { }
        }
        return sReturn;
    }




}
