﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using System.Text;
using System.Drawing;
using System.IO;




[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class UploadPicture : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public String UploadImage(String image)
    {



       // byte[] image_byte = Encoding.Unicode.GetBytes(image);
        Image convertedImage = Base64ToImage(image);
        try
        {
            convertedImage.Save(Server.MapPath("~/PictureOrder/ABC.jpg"), System.Drawing.Imaging.ImageFormat.Jpeg);
        }
        catch (Exception e)
        {
            JavaScriptSerializer ser2 = new JavaScriptSerializer();

            var JsonData2 = new
            {

                Status = e.Message,
            };
            return ser2.Serialize(JsonData2);

             
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Status = image,
        };
        return ser.Serialize(JsonData);

    }


    public Image ConvertToImage(byte[] image)
    {
        MemoryStream ms = new MemoryStream(image);
        Image returnImage = Image.FromStream(ms);
        return returnImage;
    }

    private Image Base64ToImage(string base64String)
    {
        // Convert Base64 String to byte[]
        byte[] imageBytes = Convert.FromBase64String(base64String);
        using (var ms = new MemoryStream(imageBytes, 0,
                                         imageBytes.Length))
        {
            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }
    }

}
