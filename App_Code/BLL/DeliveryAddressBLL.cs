﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Text;


public class DeliveryAddressBLL
{
    public string KeywordSearch(string Keyword)
    {
        System.Data.DataSet ds = new DeliveryAddressDAL().KeywordSearch(Keyword);
        StringBuilder str = new StringBuilder();

        if (ds.Tables[0].Rows.Count > 0)
        {

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {


                str.Append("<tr><td><div name='dvpincode' style='cursor:pointer' id='dv_" + ds.Tables[0].Rows[i]["PinCode"].ToString() + "' ><span style='font-size:12px;padding-right:30px'>" + ds.Tables[0].Rows[i]["PinCode"] + "</span><span style='font-size:12px;padding-right:30px'>" + ds.Tables[0].Rows[i]["Sector"] + "</span></div></td></tr>");

            }


        }
        return str.ToString();
    }
    public int InsertUpdate(DeliveryAddress objDelAdd)
    {
        return new DeliveryAddressDAL().InsertUpdate(objDelAdd);
    }
    public int DeleteById(DeliveryAddress objDelAdd)
    {
        return new DeliveryAddressDAL().DeleteById(objDelAdd);
    }



    public string GetHtmlByUserId(Int64 UserId,out Int64 DeliveryAddressId)
    {
        DeliveryAddressId = 0;


        StringBuilder strBuilder = new StringBuilder();
        SqlDataReader dr = new DeliveryAddressDAL().GetAllByUserId(UserId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["IsPrimary"]))
                    {
                        DeliveryAddressId = Convert.ToInt64(dr["DeliveryAddressId"]);
                    }

                    string chk=Convert.ToBoolean(dr["IsPrimary"]) ? "checked=checked" :"";
                    string color = Convert.ToBoolean(dr["IsPrimary"]) ? "#E6E8B5" : "white"; 
                    strBuilder.Append(
"<div class='box5' id='d_" + dr["DeliveryAddressId"].ToString() + "' style='background:" + color + "'><table><tr><td>Name:</td><td>" + dr["RecipientFirstName"].ToString() + " " + dr["RecipientLastName"].ToString() + "</td></tr><tr><td>City:</td><td>" + dr["CityName"].ToString() + "</td></tr>" +
"<tr><td>Area:</td><td>" + dr["Area"].ToString() + "</td></tr><tr><td>Street:</td><td>" + dr["Street"].ToString() + "</td></tr><tr><td>Address:</td><td>" + dr["Address"].ToString() + ", " + dr["PinCode"] + "</td></tr>" +
"<tr><td>Phone:</td><td>" + dr["MobileNo"] + "," + dr["MobileNo"] + "</td></tr><tr><td></td><td align='center'><input type='radio'  value='" + dr["DeliveryAddressId"].ToString() + "'  id='chk_" + dr["DeliveryAddressId"].ToString() + "' name='delivery'  " + chk + "/></td></tr></table></div>"

                        );



                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return strBuilder.ToString();



    }

    public List<DeliveryAddress> GetAllByUserId(Int64 UserId)
    {
        List<DeliveryAddress> DelAddList = new List<DeliveryAddress>();
        SqlDataReader dr = new DeliveryAddressDAL().GetAllByUserId(UserId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    DeliveryAddress objDelivery = new DeliveryAddress()
                    {
                        PinCodeId = Convert.ToInt32(dr["PinCodeId"].ToString()),
                        DeliveryAddressId = Convert.ToInt64(dr["DeliveryAddressId"].ToString()),
                        RecipientFirstName = dr["RecipientFirstName"].ToString(),
                        RecipientLastName = dr["RecipientLastName"].ToString(),
                        MobileNo = Convert.ToString(dr["MobileNo"]),
                        Telephone = Convert.ToString(dr["Telephone"]),
                        CityId = Convert.ToInt32(dr["CityId"].ToString()),
                        CityName = dr["CityName"].ToString(),
                        Area = Convert.ToString(dr["Area"]),
                        Street = Convert.ToString(dr["Street"]),
                        Address = Convert.ToString(dr["Address"]),
                        PinCode = Convert.ToString(dr["PinCode"]),
                        IsPrimary = Convert.ToBoolean(dr["IsPrimary"]),
                    };
                    DelAddList.Add(objDelivery);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return DelAddList;

    }
}