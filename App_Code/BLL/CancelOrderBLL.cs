﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for CancelOrderBLL
/// </summary>
public class CancelOrderBLL
{
    public Int32 CancelOrder(CancelOrders objCancelOrders)
    {
        return new CancelOrderDAL().CancelOrder(objCancelOrders);
    }


    public List<CancelOrders> CancelOrdersGetByDate(DateTime FromDate, DateTime ToDate)
    {
        List<CancelOrders> OrderList = new List<CancelOrders>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new CancelOrderDAL().CancelOrdersGetByDate(FromDate, ToDate);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    CancelOrders objOrder = new CancelOrders()
                    {
                        OrderId = Convert.ToInt32(dr["OrderId"].ToString()),

                        OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString()),
                        CustomerName = dr["CustomerName"].ToString(),
                        RecipientFirstName = dr["RecipientFirstName"].ToString(),
                        RecipientLastName = Convert.ToString(dr["RecipientLastName"]),
                        RecipientMobile = Convert.ToString(dr["RecipientMobile"]),
                        RecipientPhone = Convert.ToString(dr["RecipientPhone"].ToString()),
                        Recipient = dr["Recipient"].ToString(),
                        CompleteAddress = dr["CompleteAddress"].ToString(),
                        Area = Convert.ToString(dr["Area"]),
                        Street = Convert.ToString(dr["Street"]),
                        Address = Convert.ToString(dr["Address"]),
                        Pincode = Convert.ToString(dr["PinCode"]),
                        BillValue = Convert.ToDecimal(dr["BillValue"]),
                        DisAmt = Convert.ToDecimal(dr["DisAmt"]),
                        NetAmount = Convert.ToDecimal(dr["NetAmount"]),
                        ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]),
                        VatAmt = Convert.ToDecimal(dr["VatAmt"]),
                        canceldate = Convert.ToDateTime(dr["canceldate"]),
                        DeliverySlot = dr["DeliverySlot"].ToString(),
                        PaymentMode = dr["PaymentMode"].ToString(),
                    };
                    OrderList.Add(objOrder);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return OrderList;

    }


    public void TempInsert(OrderDetail objOrder)
    {
        new CancelOrderDAL().TempInsert(objOrder);
    }
}