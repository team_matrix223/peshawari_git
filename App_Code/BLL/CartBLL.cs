﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;

/// <summary>
/// Summary description for CartBLL
/// </summary>
public class CartBLL
{
    public string UserCartListInsertUpdate(UserCart objUserCart, string Mode, Calc objCalc, out string ProductHtml, Int32 ListId, bool KeepExisting)
    {
        ProductHtml = "";
        StringBuilder strProducts = new StringBuilder();
        StringBuilder str = new StringBuilder();
        DataSet ds = new CartDAL().UserCartListInsertUpdate(objUserCart, Mode, ListId, KeepExisting);
        try
        {

        }
        finally
        {

        }
        return str.ToString();
    }

    public string RemoveFromUserCart(UserCart objUserCart, Calc objCalc, out string ProductHtml)
    {
        StringBuilder strProducts = new StringBuilder();
        StringBuilder str = new StringBuilder();
        DataSet ds = new CartDAL().RemoveFromUserCart(objUserCart);
        try
        {


            objUserCart.ProductId = Convert.ToInt32(ds.Tables[0].Rows[0]["ProductId"]);
            objUserCart.Qty = Convert.ToInt32(ds.Tables[0].Rows[0]["CartCount"]);
            objUserCart.VariationId = Convert.ToInt32(ds.Tables[0].Rows[0]["VariationId"]);

            objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            objCalc.Price = Convert.ToInt32(ds.Tables[0].Rows[0]["Price"]);
            objCalc.Qty = Convert.ToInt32(ds.Tables[0].Rows[0]["Qty"]);
            objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            objCalc.ProductSubTotal = Convert.ToInt32(ds.Tables[0].Rows[0]["Price"]) * Convert.ToInt32(ds.Tables[0].Rows[0]["Qty"]);
            objCalc.SubTotal = Convert.ToInt32(ds.Tables[0].Rows[0]["SubTotal"]);
            objCalc.VariationId = ds.Tables[0].Rows[0]["VariationId"].ToString();

            decimal Price = Convert.ToDecimal(ds.Tables[0].Rows[0]["Price"]);
            decimal Mrp = Convert.ToDecimal(ds.Tables[0].Rows[0]["Mrp"]);

            int CartCount = Convert.ToInt16(ds.Tables[0].Rows[0]["CartCount"]);
            string VariationList = ds.Tables[0].Rows[0]["VariationList"].ToString();
            string bgColor = "white";
            if (CartCount > 0)
            {
                bgColor = "#D8EDC0";
            }

            string imgName1 = ds.Tables[0].Rows[0]["PhotoUrl"].ToString() != "" ? ds.Tables[0].Rows[0]["PhotoUrl"].ToString() : "noimage.jpg";
            //<div style='position:absolute;top:10px;left:25px'><img src='11.png'/></div>
            strProducts.Append("<div class='box1'  id='box_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' style='background:" + bgColor + "'><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[0]["ProductId"].ToString() + "&v=" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'><img  src='ProductImages/T_" + imgName1 + "'  class='boximage' alt=''/></a>");
            strProducts.Append("<span style='font-size:10px'>" + ds.Tables[0].Rows[0]["BrandName"] + "</span><span><b>" + ds.Tables[0].Rows[0]["Name"] + "</b></span>");

            int VariationCount = Convert.ToInt16(ds.Tables[0].Rows[0]["VCount"]);
            if (VariationCount > 1)
            {

                strProducts.Append("<select name='variation' selvar='" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' id='d_" + ds.Tables[0].Rows[0]["ProductId"].ToString() + "' class='atcSelect'>");

                strProducts.Append(VariationList);


                strProducts.Append("</select>");
            }
            else
            {


                strProducts.Append("<span>" + VariationList + "</span>");

            }

            strProducts.Append("<table class='tbAtc'><tr><td>");
            if (Mrp > Price)
            {
                strProducts.Append("<h2 style='text-decoration:line-through' >र " + Mrp + "</h2>");
            }
            strProducts.Append("</td><td><h2>र " + Price + "</h2></td></tr></table><div id='dvChangable" + ds.Tables[0].Rows[0]["ProductId"].ToString() + "' class='dvChangable'>");

            if (CartCount == 0)
            {

                strProducts.Append("<button name='btnAddToCart'  style='outline:none'  type='button' id='a_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'  class='btn btn-success atc'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</button>" +
                "<div class='qty'> <div align='center'><div class='atcVar'>" +
                "Qty<select name='qty'  id='q_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' style='margin:1px 0px 1px 0px; padding:3px;border:0px'>" +
                "<option value='1' style='font-size:12px;'>1</option>" +
                "<option value='2' style='font-size:12px;'>2</option>" +
                "<option value='3' style='font-size:12px;'>3</option><option value='4' style='font-size:12px;'>4</option><option value='5' style='font-size:12px;'>5</option></select></div></div></div>");
            }
            else
            {
                strProducts.Append(
                    "<div style='margin:14px  0px 0px 20px'><div class='priceDiv'>" +
     "<div name='decr' class='decr' id='decr_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' >-</div><div style='width:80px;text-align:center;float:left;background:white;padding:0px 0 0 3px;' id='cqty_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[0]["CartCount"].ToString() + "</div>" +
     "<div name='incr' class='incr' id='incr_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' >+</div></div></div>"

                    );
            }


            strProducts.Append("<br/><br/></div></div>");

            ProductHtml = strProducts.ToString();
            //if (ds.Tables[1].Rows.Count>0)
            // {
            //     int m_sTotal = 0;
            //     for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
            //     {
            //         m_sTotal = m_sTotal + (Convert.ToInt32(ds.Tables[1].Rows[i]["Price"]) * Convert.ToInt16(ds.Tables[1].Rows[i]["Qty"]));
            //         string imgName = ds.Tables[1].Rows[i]["PhotoUrl"].ToString() != "" ? ds.Tables[1].Rows[i]["PhotoUrl"].ToString() : "noimage.jpg";

            //         if (ds.Tables[1].Rows[i]["type"].ToString() == "C")
            //         {
            //             str.Append(

            //                 "<table style='border-bottom:solid 2px #d4d4d4;' id='tb_" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "' ><tr>" +
            //                 "<td style='vertical-align:top;padding-top:8px'><img src='ComboImages/T_" + imgName + "' style='width:45px;height:46px;margin:5px' alt='' /></td>" +
            //                 "<td style='padding-top:10px'><table>" +
            //                 "<tr><td><h4><b>" + ds.Tables[1].Rows[i]["Name"] + "</b></h4></td></tr><tr><td><h5>" + ds.Tables[1].Rows[i]["ProductDesc"] + "</h5></td></tr>" +
            //                 "<tr><td style='padding-top:10px;padding-bottom:10px'>" +
            //                 "<div class='button'>" +
            //                 "<div style='width:55px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;'>" +
            //                 "<div  style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' v='" + ds.Tables[1].Rows[i]["VariationId"] + "' name='cartcombominus' id='cm_" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "'>-</div>" +
            //                 "<div style='width:23px;float:left;background:white;padding:0px 0 0 3px;'  id='ccq_" + ds.Tables[1].Rows[i]["ProductId"].ToString() + ds.Tables[1].Rows[i]["VariationId"] + "'>" + ds.Tables[1].Rows[i]["Qty"] + "</div>" +
            //                 "<div style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' v='" + ds.Tables[1].Rows[i]["VariationId"] + "'   name='cartcomboadd' id='cp_" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "'>+</div>" +
            //                 "</div><span style='color:#f40f00;margin-left:10px'>Rs " + ds.Tables[1].Rows[i]["Price"] + "</span>" +
            //                 "</td></tr></table> </td></tr></table>"

            //                 );
            //         }
            //         else
            //         {
            //             str.Append(

            //                 "<table style='border-bottom:solid 2px #d4d4d4;' id='tb_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' ><tr>" +
            //                 "<td style='vertical-align:top;padding-top:8px'><img src='ProductImages/T_" + imgName + "' style='width:45px;height:46px;margin:5px' alt='' /></td>" +
            //                 "<td style='padding-top:10px'><table>" +
            //                 "<tr><td><h4><b>" + ds.Tables[1].Rows[i]["ProductName"] + "</b></h4></td></tr><tr><td><h5>" + ds.Tables[1].Rows[i]["ProductDesc"] + "</h5></td></tr>" +
            //                 "<tr><td style='padding-top:10px;padding-bottom:10px'>" +
            //                 "<div class='button'>" +
            //                 "<div style='width:55px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;'>" +
            //                 "<div  style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' v='" + ds.Tables[1].Rows[i]["VariationId"] + "' name='cartminus' id='cm_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "'>-</div>" +
            //                 "<div style='width:23px;float:left;background:white;padding:0px 0 0 3px;'  id='cq_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + ds.Tables[1].Rows[i]["VariationId"] + "'>" + ds.Tables[1].Rows[i]["Qty"] + "</div>" +
            //                 "<div style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' v='" + ds.Tables[1].Rows[i]["VariationId"] + "'   name='cartadd' id='cp_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "'>+</div>" +
            //                 "</div><span style='color:#f40f00;margin-left:10px'>Rs " + ds.Tables[1].Rows[i]["Price"] + "</span>" +
            //                 "</td></tr></table> </td></tr></table>"

            //                 );
            //         }

            //     }

            // }
            // else
            // {
            //     objUserCart.Qty = Convert.ToInt32(0);

            // }
            return str.ToString();
        }
        finally
        {


        }

    }
    public Int32 GetTotalItems(string SessionID)
    {
        return new CartDAL().GetTotalItems(SessionID);
    }
    public string MobileUserCartInsertUpdate(UserCart objUserCart, string Mode, Calc objCalc, out string ProductHtml)
    {
        StringBuilder strProducts = new StringBuilder();
        StringBuilder str = new StringBuilder();
        DataSet ds = new CartDAL().UserCartInsertUpdate(objUserCart, Mode);
        try
        {


            objUserCart.ProductId = Convert.ToInt32(ds.Tables[0].Rows[0]["ProductId"]);
            objUserCart.Qty = Convert.ToInt32(ds.Tables[0].Rows[0]["CartCount"]);
            objUserCart.VariationId = Convert.ToInt32(ds.Tables[0].Rows[0]["VariationId"]);

            objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            objCalc.Price = Convert.ToInt32(ds.Tables[0].Rows[0]["Price"]);
            objCalc.Qty = Convert.ToInt32(ds.Tables[0].Rows[0]["Qty"]);
            objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            objCalc.ProductSubTotal = Convert.ToInt32(ds.Tables[0].Rows[0]["Price"]) * Convert.ToInt32(ds.Tables[0].Rows[0]["Qty"]);
            objCalc.SubTotal = Convert.ToInt32(ds.Tables[0].Rows[0]["SubTotal"]);
            objCalc.VariationId = ds.Tables[0].Rows[0]["VariationId"].ToString();

            decimal Price = Convert.ToDecimal(ds.Tables[0].Rows[0]["Price"]);
            decimal Mrp = Convert.ToDecimal(ds.Tables[0].Rows[0]["Mrp"]);

            int CartCount = Convert.ToInt16(ds.Tables[0].Rows[0]["CartCount"]);
            string VariationList = ds.Tables[0].Rows[0]["VariationList"].ToString();
            string bgColor = "white";
            if (CartCount > 0)
            {
                bgColor = "#D8EDC0";
            }

            string imgName1 = ds.Tables[0].Rows[0]["PhotoUrl"].ToString() != "" ? ds.Tables[0].Rows[0]["PhotoUrl"].ToString() : "noimage.jpg";
            string OfferHtml = "";
            if (Mrp > Price)
            {
                OfferHtml = "<div class='mobileOfferBg'>";
                OfferHtml += "<div class='mobileOffer'><b style='font-size:20px;'>" + Math.Round(Mrp - Price, 0) + "</b>Rs</div></div>";
            }



            strProducts.Append("<div class='box1'      style='background:" + bgColor + ";height:auto;width:100%;padding-bottom:5px;max-width:98%'><table><tr><td valign='top'>" + OfferHtml + "<a href='productdetail.aspx?p=" + ds.Tables[0].Rows[0]["ProductId"].ToString() + "&v=" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'><img  src='../ProductImages/T_" + imgName1 + "'  class='mobileBoximage' alt=''  /></a></td><td><table><tr><td style='min-width:150px;width:150px'>");
            strProducts.Append("<span style='font-size:10px'>" + ds.Tables[0].Rows[0]["BrandName"] + "</span><span><b>" + ds.Tables[0].Rows[0]["Name"] + "</b></span>");

            int VariationCount = Convert.ToInt16(ds.Tables[0].Rows[0]["VCount"]);

            string strVList = "";
            if (VariationCount > 1)
            {

                strVList += ("<select name='variation' selvar='" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' id='d_" + ds.Tables[0].Rows[0]["ProductId"].ToString() + "' class='atcSelect'>");

                strVList += (VariationList);


                strVList += ("</select>");
            }
            else
            {


                strVList += ("<span>" + VariationList + "</span>");

            }

            strProducts.Append("<table class='tbAtc'><tr><td>");
            if (Mrp > Price)
            {
                strProducts.Append("<h2 style='text-decoration:line-through' >र " + Mrp + "</h2>");
            }
            // str.Append("<h2 style='' >" + dt.Rows[i]["ItemCode"].ToString() + "</h2>");
            strProducts.Append("</td><td><h2>र " + Price + "</h2></td></tr></table></td><td><div id='dvChangable" + ds.Tables[0].Rows[0]["ProductId"].ToString() + "' class='dvChangable'>");

            strProducts.Append(strVList);
            if (CartCount == 0)
            {

                strProducts.Append("<button name='btnAddToCart'  style='outline:none;width:100px'  type='button' id='a_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'  class='btn btn-success atc'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</button>");
            }
            else
            {

                strProducts.Append(
                    "<table width='100px' style='background:#d2cfcf'><tr><td style='width:35px'><div name='decr' class='decr' id='decr_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' >-</div></td><td style='background:white;width:25px'><div  id='cqty_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[0]["CartCount"].ToString() + "</div></td><td style='width:35px'><div name='incr' class='incr' id='incr_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' >+</div></td></tr></table>" 
                    
                    );

         
            }


            strProducts.Append(" </div></td></tr></table></td></tr></table></div>");

         
            
            
     //       strProducts.Append("<div class='box1'  id='box_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' style='background:" + bgColor + "'><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[0]["ProductId"].ToString() + "&v=" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'><img  src='ProductImages/T_" + imgName1 + "'  class='boximage' alt=''/></a>");
     //       strProducts.Append("<span style='font-size:10px'>" + ds.Tables[0].Rows[0]["BrandName"] + "</span><span><b>" + ds.Tables[0].Rows[0]["Name"] + "</b></span>");

     //       int VariationCount = Convert.ToInt16(ds.Tables[0].Rows[0]["VCount"]);
     //       if (VariationCount > 1)
     //       {

     //           strProducts.Append("<select name='variation' selvar='" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' id='d_" + ds.Tables[0].Rows[0]["ProductId"].ToString() + "' class='atcSelect'>");

     //           strProducts.Append(VariationList);


     //           strProducts.Append("</select>");
     //       }
     //       else
     //       {


     //           strProducts.Append("<span>" + VariationList + "</span>");

     //       }

     //       strProducts.Append("<table class='tbAtc'><tr><td>");
     //       if (Mrp > Price)
     //       {
     //           strProducts.Append("<h2 style='text-decoration:line-through' >र " + Mrp + "</h2>");
     //       }
     //       strProducts.Append("</td><td><h2>र " + Price + "</h2></td></tr></table><div id='dvChangable" + ds.Tables[0].Rows[0]["ProductId"].ToString() + "' class='dvChangable'>");

     //       if (CartCount == 0)
     //       {

     //           strProducts.Append("<button name='btnAddToCart'  style='outline:none'  type='button' id='a_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'  class='btn btn-success atc'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</button>" +
     //           "<div class='qty'> <div align='center'><div class='atcVar'>" +
     //           "Qty<select name='qty'  id='q_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' style='margin:1px 0px 1px 0px; padding:3px;border:0px'>" +
     //           "<option value='1' style='font-size:12px;'>1</option>" +
     //           "<option value='2' style='font-size:12px;'>2</option>" +
     //           "<option value='3' style='font-size:12px;'>3</option><option value='4' style='font-size:12px;'>4</option><option value='5' style='font-size:12px;'>5</option></select></div></div></div>");
     //       }
     //       else
     //       {
     //           strProducts.Append(
     //               "<div style='margin:14px  0px 0px 20px'><div class='priceDiv'>" +
     //"<div name='decr' class='decr' id='decr_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' >-</div><div style='width:80px;text-align:center;float:left;background:white;padding:0px 0 0 3px;' id='cqty_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[0]["CartCount"].ToString() + "</div>" +
     //"<div name='incr' class='incr' id='incr_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' >+</div></div></div>"

     //               );
     //       }


     //       strProducts.Append("<br/><br/></div></div>");

            ProductHtml = strProducts.ToString();
            
            return str.ToString();
        }
        finally
        {


        }

    }



    public Int16 InsertComments(string Comments, string SessionId)
    {
        return new CartDAL().InsertComments(Comments, SessionId);
    }
    public Int16 ValidateCheckout(string SessionID)
    {
        return new CartDAL().ValidateCheckout(SessionID);
    }

    public void UserCartDelete(String SessionId)
    {
        new CartDAL().UserCartDelete(SessionId);
    }
    public string GetCartHTML(string SessionId, Calc objCalc)
    {

        DataSet ds = new CartDAL().GetCartBySessionId(SessionId);
        StringBuilder str = new StringBuilder();
        if (ds.Tables[0].Rows.Count > 0)
        {
            objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            objCalc.TotalItems = ds.Tables[0].Rows.Count;
            objCalc.MinimumCheckOutAmt = Convert.ToInt32(ds.Tables[0].Rows[0]["MinimumCheckOutAmt"]);
            objCalc.FreeDeliveryAmt = Convert.ToInt32(ds.Tables[0].Rows[0]["FreeDeliveryAmt"]);
        }



        int m_sTotal = 0;
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            string ImageUrl = ds.Tables[0].Rows[i]["PhotoUrl"].ToString() != "" ? ds.Tables[0].Rows[i]["PhotoUrl"].ToString() : "noimage.jpg";
             
            if (ds.Tables[0].Rows[i]["Type"].ToString() == "Combo")
            {
                str.Append(

                    "<table style='border-bottom:solid 2px #d4d4d4;width:100%' id='tb_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' ><tr>" +
                    "<td style='vertical-align:top;padding-top:8px'><img src='ComboImages/T_" + ImageUrl + "' style='width:45px;height:46px;margin:5px' alt='' /></td>" +
                    "<td style='padding-top:10px'><table>" +
                    "<tr><td><h4><b>" + ds.Tables[0].Rows[i]["Name"] + "</b></h4></td><td><div id= '" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' name='dvClose' ><img src='images/cancel3.png' style='width:20px;cursor:pointer'/></div></td></tr><tr><td><h5>" + ds.Tables[0].Rows[i]["ProductDesc"] + "</h5></td></tr>" +
                    "<tr><td style='padding-top:10px;padding-bottom:10px'>" +
                    "<div class='button'>" +
                    "<div style='width:55px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;'>" +
                    "<div  style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' v='" + ds.Tables[0].Rows[i]["VariationId"] + "' name='cartcombominus' id='cm_" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "'>-</div>" +
                    "<div style='width:23px;float:left;background:white;padding:0px 0 0 3px;'  id='ccq_" + ds.Tables[0].Rows[i]["ProductId"].ToString() + ds.Tables[0].Rows[i]["VariationId"] + "'>" + ds.Tables[0].Rows[i]["Qty"] + "</div>" +
                    "<div style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' v='" + ds.Tables[0].Rows[i]["VariationId"] + "'   name='cartcomboadd' id='cp_" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "'>+</div>" +
                    "</div><span style='color:#f40f00;margin-left:10px'>Rs " + ds.Tables[0].Rows[i]["Price"] + "</span>" +
                    "</td></tr></table> </td></tr></table>"

                    );
            }
            else
            {
                str.Append(

                    "<table style='border-bottom:solid 2px #d4d4d4;width:100%' id='tb_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' ><tr>" +
                    "<td style='vertical-align:top;padding-top:8px'><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "&v=" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'><img src='ProductImages/T_" + ImageUrl + "' style='width:45px;height:46px;margin:5px' alt='' /></a></td>" +
                    "<td style='padding-top:10px'><table style='width:100%'>" +
                    "<tr><td><h4><b>" + ds.Tables[0].Rows[i]["ProductName"] + "</b></h4></td><td><div id= '" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' name='dvClose' ><img src='images/cancel3.png' style='width:20px;cursor:pointer'/></div></td></tr><tr><td><h5>" + ds.Tables[0].Rows[i]["ProductDesc"] + "</h5></td></tr>" +
                    "<tr><td style='padding-top:10px;padding-bottom:10px'>" +
                    "<div class='button'>");

                if (Convert.ToInt32(ds.Tables[0].Rows[i]["SchemeId"]) == 0)
                {
                    m_sTotal = m_sTotal + (Convert.ToInt32(ds.Tables[0].Rows[i]["Price"]) * Convert.ToInt16(ds.Tables[0].Rows[i]["Qty"]));

                    str.Append("<div style='width:55px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;'>" +
                     "<div  style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' v='" + ds.Tables[0].Rows[i]["VariationId"] + "' name='cartminus' id='cm_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>-</div>" +
                     "<div style='width:23px;float:left;background:white;padding:0px 0 0 3px;'  id='cq_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[i]["Qty"] + "</div>" +
                     "<div style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' v='" + ds.Tables[0].Rows[i]["VariationId"] + "'   name='cartadd' id='cp_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>+</div>" +
                     "</div><span style='color:#f40f00;margin-left:10px'>Rs " + ds.Tables[0].Rows[i]["Price"] + "</span>");
                }
                else
                {
                    str.Append("<div style='width:25px;float:left;border:solid 1px silver;'>" +
                  "<div style='width:20px;float:left;background:white;padding:0px 0 0 3px;' >" + ds.Tables[0].Rows[i]["Qty"] + "</div>" +
                 "</div><span style='color:green;margin-left:10px'><img src='img/free.gif'/></span>");




                }


                str.Append("</td></tr></table> </td></tr></table>");
            }

        }

        objCalc.SubTotal = m_sTotal;

        return str.ToString();
    }


    public string GetOrderSummary(string SessionId, Calc objCalc)
    {

        DataSet ds = new CartDAL().GetCartBySessionId(SessionId);
        StringBuilder str = new StringBuilder();
        if (ds.Tables[0].Rows.Count > 0)
        {
            objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            objCalc.TotalItems = ds.Tables[0].Rows.Count;
            objCalc.MinimumCheckOutAmt = Convert.ToInt32(ds.Tables[0].Rows[0]["MinimumCheckOutAmt"]);
            objCalc.FreeDeliveryAmt = Convert.ToInt32(ds.Tables[0].Rows[0]["FreeDeliveryAmt"]);
            objCalc.DisAmt = Convert.ToInt32(ds.Tables[0].Rows[0]["DisAmt"]);
        }



        int m_sTotal = 0;
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {

          
            m_sTotal = m_sTotal + (Convert.ToInt32(ds.Tables[0].Rows[i]["Price"]) * Convert.ToInt16(ds.Tables[0].Rows[i]["Qty"]));

            if (ds.Tables[0].Rows[i]["Type"].ToString() == "Combo")
            {
                str.Append(

                    "<table style='border-bottom:solid 2px #d4d4d4;' id='tb_" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "' ><tr>" +
                    "<td style='vertical-align:top;padding-top:8px'><img src='ComboImages/T_" + ds.Tables[0].Rows[i]["PhotoUrl"] + "' style='width:45px;height:46px;margin:5px' alt='' /></td>" +
                    "<td style='padding-top:10px'><table>" +
                    "<tr><td><h4><b>" + ds.Tables[0].Rows[i]["Name"] + "</b></h4></td></tr><tr><td><h5>" + ds.Tables[0].Rows[i]["ProductDesc"] + "</h5></td></tr>" +
                    "<tr><td style='padding-top:10px;padding-bottom:10px'>" +
                    "<div class='button'>" +
                    "<div style='width:55px;float:left;border:solid 1px silver;text-align:center;width:35px'>" +

                    "<div style='width:23px;float:left;background:white;padding:0px 0 0 3px;' >" + ds.Tables[0].Rows[i]["Qty"] + "</div>" +
                    "</div><span style='color:#f40f00;margin-left:10px'>Rs " + ds.Tables[0].Rows[i]["Price"] + "</span>" +
                    "</td></tr></table> </td></tr></table>"

                      );
            }
            else
            {
                string imgName = ds.Tables[0].Rows[i]["PhotoUrl"].ToString() != "" ? ds.Tables[0].Rows[i]["PhotoUrl"].ToString() : "noimage.jpg";

                str.Append(

      "<table style='border-bottom:solid 2px #d4d4d4;' id='tb_" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "' ><tr>" +
      "<td style='vertical-align:top;padding-top:8px'><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "&v=" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'><img src='ProductImages/T_" + imgName + "' style='width:45px;height:46px;margin:5px' alt='' /></a></td>" +
     "<td style='padding-top:10px'><table>" +
     "<tr><td><h4><b>" + ds.Tables[0].Rows[i]["BrandName"] + "</b></h4></td></tr><tr><td><h5>" + ds.Tables[0].Rows[i]["ProductDesc"] + "</h5></td></tr>" +
     "<tr><td style='padding-top:10px;padding-bottom:10px'>" +
     "<div class='button'>" +
     "<div style='width:55px;float:left;border:solid 1px silver;text-align:center;width:35px'>" +

     "<div style='width:23px;float:left;background:white;padding:0px 0 0 3px;' >" + ds.Tables[0].Rows[i]["Qty"] + "</div>" +
      "</div><span style='color:#f40f00;margin-left:10px'>Rs " + ds.Tables[0].Rows[i]["Price"] + "</span>" +
     "</td></tr></table> </td></tr></table>"

                    );
            }
        }

        objCalc.SubTotal = m_sTotal;

        return str.ToString();
    }



    //public string GetBasketHTML(string SessionId, Calc objCalc, int Status = 1)
    //{


    //    DataSet ds = new CartDAL().GetCartBySessionId(SessionId);
    //    StringBuilder str = new StringBuilder();
    //    if (ds.Tables[0].Rows.Count > 0)
    //    {
    //        objCalc.TotalItems = ds.Tables[0].Rows.Count;
    //        objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
    //        objCalc.MinimumCheckOutAmt = Convert.ToInt32(ds.Tables[0].Rows[0]["MinimumCheckOutAmt"]);
    //        objCalc.FreeDeliveryAmt = Convert.ToInt32(ds.Tables[0].Rows[0]["FreeDeliveryAmt"]);
    //        objCalc.Comments = ds.Tables[0].Rows[0]["Comments"].ToString();
    //        objCalc.DisAmt = Convert.ToDecimal(ds.Tables[0].Rows[0]["DisAmt"].ToString());
    //    }



    //    int m_sTotal = 0;
    //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
    //    {
    //        string ImageUrl = ds.Tables[0].Rows[i]["PhotoUrl"].ToString() != "" ? ds.Tables[0].Rows[i]["PhotoUrl"].ToString() : "noimage.jpg";
         
    //        if (Convert.ToInt32(ds.Tables[0].Rows[i]["SchemeId"]) == 0)
    //        {
    //            m_sTotal = m_sTotal + (Convert.ToInt32(ds.Tables[0].Rows[i]["Price"]) * Convert.ToInt16(ds.Tables[0].Rows[i]["Qty"]));

    //            str.Append("<tr style='border-bottom:dotted 1px silver' id='tp_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'><td><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "&v=" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'><img src='ProductImages/T_" + ImageUrl + "' style='width:45px;height:46px;margin:5px' alt='' /></a></td><td><b>" + ds.Tables[0].Rows[i]["ProductName"] + "</b><br/>" + ds.Tables[0].Rows[i]["ProductDesc"] + "</td><td>Rs " + ds.Tables[0].Rows[i]["Price"] + "</td>");
    //            str.Append("<td><div class='button'>");

    //            if (Status == 1)
    //            {


    //                str.Append("<div style='width:55px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;'>" +
    //                "<div  style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer'   name='cartminus' id='cm_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>-</div>" +
    //                "<div style='width:23px;height:20px;float:left;background:white;padding:0px 0 0 3px;'  id='cq_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[i]["Qty"] + "</div>" +
    //                "<div style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer'   name='cartadd' id='cp_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>+</div>");
    //            }
    //            else
    //            {
    //                str.Append("<div style='width:23px;height:20px;float:left;background:white;padding:0px 0 0 3px;'  id='cq_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[i]["Qty"] + "</div>");

    //            }

    //            str.Append("</div></td>");
    //            str.Append("<td>Rs. <span id='span" + ds.Tables[0].Rows[i]["ProductId"].ToString() + ds.Tables[0].Rows[i]["VariationId"] + "' >" + Convert.ToInt64(ds.Tables[0].Rows[i]["Qty"]) * Convert.ToInt64(ds.Tables[0].Rows[i]["Price"]) + "</span></td></tr>");

    //        }
    //        else
    //        {

    //            str.Append("<tr style='border-bottom:dotted 1px silver' ><td><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "&v=" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'><img src='ProductImages/T_" + ImageUrl + "' style='width:45px;height:46px;margin:5px' alt='' /></a></td><td><b>" + ds.Tables[0].Rows[i]["ProductName"] + "</b><br/>" + ds.Tables[0].Rows[i]["ProductDesc"] + "</td><td>Rs " + ds.Tables[0].Rows[i]["Price"] + "</td>");

    //            str.Append("<td><div style='width:25px;float:left;margin-left:15px'>" +
    //             "<div style='width:20px;float:left;background:white;padding:0px 0 0 3px;' >" + ds.Tables[0].Rows[i]["Qty"] + "</div>" +
    //            "</div></td>");


    //            str.Append("<td><img src='img/free.gif'/></td></tr>");


    //        }

    //    }

    //    objCalc.SubTotal = m_sTotal;

    //    return str.ToString();
    //}


    public string GetBasketHTML(string SessionId, Calc objCalc, int Status = 1)
    {


        DataSet ds = new CartDAL().GetCartBySessionId(SessionId);
        StringBuilder str = new StringBuilder();
        if (ds.Tables[0].Rows.Count > 0)
        {
            objCalc.TotalItems = ds.Tables[0].Rows.Count;
            objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            objCalc.MinimumCheckOutAmt = Convert.ToInt32(ds.Tables[0].Rows[0]["MinimumCheckOutAmt"]);
            objCalc.FreeDeliveryAmt = Convert.ToInt32(ds.Tables[0].Rows[0]["FreeDeliveryAmt"]);
            objCalc.Comments = ds.Tables[0].Rows[0]["Comments"].ToString();
            objCalc.DisAmt = Convert.ToDecimal(ds.Tables[0].Rows[0]["DisAmt"].ToString());
        }



        int m_sTotal = 0;
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            string ImageUrl = ds.Tables[0].Rows[i]["PhotoUrl"].ToString() != "" ? ds.Tables[0].Rows[i]["PhotoUrl"].ToString() : "noimage.jpg";

            if (Convert.ToInt32(ds.Tables[0].Rows[i]["SchemeId"]) == 0)
            {
                m_sTotal = m_sTotal + (Convert.ToInt32(ds.Tables[0].Rows[i]["Price"]) * Convert.ToInt16(ds.Tables[0].Rows[i]["Qty"]));

                str.Append("<tr style='border-bottom:dotted 1px silver' id='tp_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'><td><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "&v=" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'><img src='ProductImages/T_" + ImageUrl + "' style='width:45px;height:46px;margin:5px' alt='' /></a></td>");



                str.Append("<td><b>" + ds.Tables[0].Rows[i]["ProductName"] + "</b><br/>" + ds.Tables[0].Rows[i]["ProductDesc"] + "</td> ");

                str.Append("<td style='font-weight:bold'>Rs " + ds.Tables[0].Rows[i]["Price"] + "");

                str.Append(" <div class='button'  >");

                if (Status == 1)
                {


                    str.Append("<div style='margin:2px;width:55px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;'>" +
                    "<div  style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer'   name='cartminus' id='cm_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>-</div>" +
                    "<div style='width:23px;height:20px;float:left;background:white;padding:0px 0 0 3px;'  id='cq_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[i]["Qty"] + "</div>" +
                    "<div style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer'   name='cartadd' id='cp_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>+</div>");
                }
                else
                {
                    str.Append("<div style='width:23px;height:20px;float:left;background:white;padding:0px 0 0 3px;'  id='cq_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[i]["Qty"] + "</div>");

                }

                str.Append("</div> ");



                str.Append("</td><td style='color:red'>Rs. <span id='span" + ds.Tables[0].Rows[i]["ProductId"].ToString() + ds.Tables[0].Rows[i]["VariationId"] + "' >" + Convert.ToInt64(ds.Tables[0].Rows[i]["Qty"]) * Convert.ToInt64(ds.Tables[0].Rows[i]["Price"]) + "</span></td>");

                if (Status == 1)
                {
                    str.Append("<td><div id= '" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' name='dvClose' ><img src='images/cancel3.png' style='width:30px;cursor:pointer'/></div></td>");
                }
                str.Append("</tr>");

            }
            else
            {

                str.Append("<tr style='border-bottom:dotted 1px silver' ><td><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "&v=" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'><img src='ProductImages/T_" + ImageUrl + "' style='width:45px;height:46px;margin:5px' alt='' /></a></td>");



                str.Append("<td><b>" + ds.Tables[0].Rows[i]["ProductName"] + "</b><br/>" + ds.Tables[0].Rows[i]["ProductDesc"] + "</td><td><table><tr><td style='font-weight:bold'>Rs " + ds.Tables[0].Rows[i]["Price"] + "</td></tr><tr><td>");


                str.Append("<div style='width:55px;background:silver;text-align:center;padding:0px 0 0 3px;' >" + ds.Tables[0].Rows[i]["Qty"] + "</div>");


                str.Append("</td></tr></table></td><td><img src='img/free.gif'/></td></tr>");


            }

        }

        objCalc.SubTotal = m_sTotal;

        return str.ToString();
    }

    public DataSet GetCartBySessionId(string SessionId)
    {
        return new CartDAL().GetCartBySessionId(SessionId);
    }
    public string InsertMaster(Cart objCart)
    {
        return new CartDAL().InsertMaster(objCart);
    }
    public string InsertDetail(Cart objCart)
    {
        return new CartDAL().InsertDetail(objCart);
    }

    public string UserCartInsertUpdate(UserCart objUserCart, string Mode, Calc objCalc, out string ProductHtml)
    {
        StringBuilder strProducts = new StringBuilder();
        StringBuilder str = new StringBuilder();
        DataSet ds = new CartDAL().UserCartInsertUpdate(objUserCart, Mode);
        try
        {


            objUserCart.ProductId = Convert.ToInt32(ds.Tables[0].Rows[0]["ProductId"]);
            objUserCart.Qty = Convert.ToInt32(ds.Tables[0].Rows[0]["CartCount"]);
            objUserCart.VariationId = Convert.ToInt32(ds.Tables[0].Rows[0]["VariationId"]);

            objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            objCalc.Price = Convert.ToInt32(ds.Tables[0].Rows[0]["Price"]);
            objCalc.Qty = Convert.ToInt32(ds.Tables[0].Rows[0]["Qty"]);
            objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            objCalc.ProductSubTotal = Convert.ToInt32(ds.Tables[0].Rows[0]["Price"]) * Convert.ToInt32(ds.Tables[0].Rows[0]["Qty"]);
            objCalc.SubTotal = Convert.ToInt32(ds.Tables[0].Rows[0]["SubTotal"]);
            objCalc.VariationId = ds.Tables[0].Rows[0]["VariationId"].ToString();

            decimal Price = Convert.ToDecimal(ds.Tables[0].Rows[0]["Price"]);
            decimal Mrp = Convert.ToDecimal(ds.Tables[0].Rows[0]["Mrp"]);

            int CartCount = Convert.ToInt16(ds.Tables[0].Rows[0]["CartCount"]);
            string VariationList = ds.Tables[0].Rows[0]["VariationList"].ToString();
            string bgColor = "white";
            if (CartCount > 0)
            {
                bgColor = "#D8EDC0";
            }

            string imgName1 = ds.Tables[0].Rows[0]["PhotoUrl"].ToString() != "" ? ds.Tables[0].Rows[0]["PhotoUrl"].ToString() : "noimage.jpg";
            //<div style='position:absolute;top:10px;left:25px'><img src='11.png'/></div>
            strProducts.Append("<div class='box1'  id='box_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' style='background:" + bgColor + "'><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[0]["ProductId"].ToString() + "&v=" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'><img  src='ProductImages/T_" + imgName1 + "'  class='boximage' alt=''/></a>");
            strProducts.Append("<span style='font-size:10px'>" + ds.Tables[0].Rows[0]["BrandName"] + "</span><span><b>" + ds.Tables[0].Rows[0]["Name"] + "</b></span>");

            int VariationCount = Convert.ToInt16(ds.Tables[0].Rows[0]["VCount"]);
            if (VariationCount > 1)
            {

                strProducts.Append("<select name='variation' selvar='" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' id='d_" + ds.Tables[0].Rows[0]["ProductId"].ToString() + "' class='atcSelect'>");

                strProducts.Append(VariationList);


                strProducts.Append("</select>");
            }
            else
            {


                strProducts.Append("<span>" + VariationList + "</span>");

            }

            strProducts.Append("<table class='tbAtc'><tr><td>");
            if (Mrp > Price)
            {
                strProducts.Append("<h2 style='text-decoration:line-through' >र " + Mrp + "</h2>");
            }
            strProducts.Append("</td><td><h2>र " + Price + "</h2></td></tr></table><div id='dvChangable" + ds.Tables[0].Rows[0]["ProductId"].ToString() + "' class='dvChangable'>");

            if (CartCount == 0)
            {

                strProducts.Append("<button name='btnAddToCart'  style='outline:none'  type='button' id='a_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'  class='btn btn-success atc'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</button>" +
                "<div class='qty'> <div align='center'><div class='atcVar'>" +
                "Qty<select name='qty'  id='q_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' style='margin:1px 0px 1px 0px; padding:3px;border:0px'>" +
                "<option value='1' style='font-size:12px;'>1</option>" +
                "<option value='2' style='font-size:12px;'>2</option>" +
                "<option value='3' style='font-size:12px;'>3</option><option value='4' style='font-size:12px;'>4</option><option value='5' style='font-size:12px;'>5</option></select></div></div></div>");
            }
            else
            {
                strProducts.Append(
                    "<div style='margin:14px  0px 0px 20px'><div class='priceDiv'>" +
     "<div name='decr' class='decr' id='decr_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' >-</div><div style='width:80px;text-align:center;float:left;background:white;padding:0px 0 0 3px;' id='cqty_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[0]["CartCount"].ToString() + "</div>" +
     "<div name='incr' class='incr' id='incr_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' >+</div></div></div>"

                    );
            }


            strProducts.Append("<br/><br/></div></div>");

            ProductHtml = strProducts.ToString();
            //if (ds.Tables[1].Rows.Count>0)
            // {
            //     int m_sTotal = 0;
            //     for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
            //     {
            //         m_sTotal = m_sTotal + (Convert.ToInt32(ds.Tables[1].Rows[i]["Price"]) * Convert.ToInt16(ds.Tables[1].Rows[i]["Qty"]));
            //         string imgName = ds.Tables[1].Rows[i]["PhotoUrl"].ToString() != "" ? ds.Tables[1].Rows[i]["PhotoUrl"].ToString() : "noimage.jpg";

            //         if (ds.Tables[1].Rows[i]["type"].ToString() == "C")
            //         {
            //             str.Append(

            //                 "<table style='border-bottom:solid 2px #d4d4d4;' id='tb_" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "' ><tr>" +
            //                 "<td style='vertical-align:top;padding-top:8px'><img src='ComboImages/T_" + imgName + "' style='width:45px;height:46px;margin:5px' alt='' /></td>" +
            //                 "<td style='padding-top:10px'><table>" +
            //                 "<tr><td><h4><b>" + ds.Tables[1].Rows[i]["Name"] + "</b></h4></td></tr><tr><td><h5>" + ds.Tables[1].Rows[i]["ProductDesc"] + "</h5></td></tr>" +
            //                 "<tr><td style='padding-top:10px;padding-bottom:10px'>" +
            //                 "<div class='button'>" +
            //                 "<div style='width:55px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;'>" +
            //                 "<div  style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' v='" + ds.Tables[1].Rows[i]["VariationId"] + "' name='cartcombominus' id='cm_" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "'>-</div>" +
            //                 "<div style='width:23px;float:left;background:white;padding:0px 0 0 3px;'  id='ccq_" + ds.Tables[1].Rows[i]["ProductId"].ToString() + ds.Tables[1].Rows[i]["VariationId"] + "'>" + ds.Tables[1].Rows[i]["Qty"] + "</div>" +
            //                 "<div style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' v='" + ds.Tables[1].Rows[i]["VariationId"] + "'   name='cartcomboadd' id='cp_" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "'>+</div>" +
            //                 "</div><span style='color:#f40f00;margin-left:10px'>Rs " + ds.Tables[1].Rows[i]["Price"] + "</span>" +
            //                 "</td></tr></table> </td></tr></table>"

            //                 );
            //         }
            //         else
            //         {
            //             str.Append(

            //                 "<table style='border-bottom:solid 2px #d4d4d4;' id='tb_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' ><tr>" +
            //                 "<td style='vertical-align:top;padding-top:8px'><img src='ProductImages/T_" + imgName + "' style='width:45px;height:46px;margin:5px' alt='' /></td>" +
            //                 "<td style='padding-top:10px'><table>" +
            //                 "<tr><td><h4><b>" + ds.Tables[1].Rows[i]["ProductName"] + "</b></h4></td></tr><tr><td><h5>" + ds.Tables[1].Rows[i]["ProductDesc"] + "</h5></td></tr>" +
            //                 "<tr><td style='padding-top:10px;padding-bottom:10px'>" +
            //                 "<div class='button'>" +
            //                 "<div style='width:55px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;'>" +
            //                 "<div  style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' v='" + ds.Tables[1].Rows[i]["VariationId"] + "' name='cartminus' id='cm_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "'>-</div>" +
            //                 "<div style='width:23px;float:left;background:white;padding:0px 0 0 3px;'  id='cq_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + ds.Tables[1].Rows[i]["VariationId"] + "'>" + ds.Tables[1].Rows[i]["Qty"] + "</div>" +
            //                 "<div style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' v='" + ds.Tables[1].Rows[i]["VariationId"] + "'   name='cartadd' id='cp_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "'>+</div>" +
            //                 "</div><span style='color:#f40f00;margin-left:10px'>Rs " + ds.Tables[1].Rows[i]["Price"] + "</span>" +
            //                 "</td></tr></table> </td></tr></table>"

            //                 );
            //         }

            //     }

            // }
            // else
            // {
            //     objUserCart.Qty = Convert.ToInt32(0);

            // }
            return str.ToString();
        }
        finally
        {


        }

    }

    public string SchemeProductsInsertUpdate(UserCart objUserCart, string Mode, Calc objCalc, out string ProductHtml,out int SchemeQty)
    {
        StringBuilder strProducts = new StringBuilder();
        StringBuilder str = new StringBuilder();
        DataSet ds = new CartDAL().SchemeProductsInsertUpdate(objUserCart, Mode);
        SchemeQty = 0;
        try
        {


            objUserCart.ProductId = Convert.ToInt32(ds.Tables[0].Rows[0]["ProductId"]);
            objUserCart.Qty = Convert.ToInt32(ds.Tables[0].Rows[0]["CartCount"]);
            objUserCart.VariationId = Convert.ToInt32(ds.Tables[0].Rows[0]["VariationId"]);

            objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            objCalc.Price = Convert.ToInt32(ds.Tables[0].Rows[0]["Price"]);
            objCalc.Qty = Convert.ToInt32(ds.Tables[0].Rows[0]["Qty"]);
            objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            objCalc.ProductSubTotal = Convert.ToInt32(ds.Tables[0].Rows[0]["Price"]) * Convert.ToInt32(ds.Tables[0].Rows[0]["Qty"]);
            objCalc.SubTotal = Convert.ToInt32(ds.Tables[0].Rows[0]["SubTotal"]);
            objCalc.VariationId = ds.Tables[0].Rows[0]["VariationId"].ToString();
            if (ds.Tables[1].Rows.Count > 0)
            {
                SchemeQty = Convert.ToInt16(ds.Tables[1].Rows[0]["SchemeQty"]);
            }
             
                decimal Price = Convert.ToDecimal(ds.Tables[0].Rows[0]["Price"]);
            decimal Mrp = Convert.ToDecimal(ds.Tables[0].Rows[0]["Mrp"]);

            int CartCount = Convert.ToInt16(ds.Tables[0].Rows[0]["CartCount"]);
            string VariationList = ds.Tables[0].Rows[0]["VariationList"].ToString();
            string bgColor = "white";
            if (CartCount > 0)
            {
                bgColor = "#D8EDC0";
            }

            string imgName1 = ds.Tables[0].Rows[0]["PhotoUrl"].ToString() != "" ? ds.Tables[0].Rows[0]["PhotoUrl"].ToString() : "noimage.jpg";
            //<div style='position:absolute;top:10px;left:25px'><img src='11.png'/></div>
            strProducts.Append("<div class='box1'  id='box_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' style='background:" + bgColor + "'><img  src='ProductImages/T_" + imgName1 + "'  class='boximage' alt=''>");
            strProducts.Append("<span style='font-size:10px'>" + ds.Tables[0].Rows[0]["BrandName"] + "</span><span><b>" + ds.Tables[0].Rows[0]["Name"] + "</b></span>");
            strProducts.Append("<span>" + VariationList + "</span>");
 

            strProducts.Append("<table class='tbAtc'><tr><td>");
            if (Mrp != Price)
            {
                strProducts.Append("<h2 style='text-decoration:line-through' >र " + Mrp + "</h2>");
            }
            strProducts.Append("</td><td><h2>र " + Price + "</h2></td></tr></table><div id='dvChangable" + ds.Tables[0].Rows[0]["ProductId"].ToString() + "' class='dvChangable'>");

            if (CartCount == 0)
            {

                strProducts.Append("<button name='btnAddToCart'  style='outline:none'  type='button' id='a_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'  class='btn btn-success atc'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</button>" +
                "<div class='qty'> <div align='center'><div class='atcVar'>" +
                "Qty<select name='qty'  id='q_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' style='margin:1px 0px 1px 0px; padding:3px;border:0px'>" +
                "<option value='1' style='font-size:12px;'>1</option>" +
                "<option value='2' style='font-size:12px;'>2</option>" +
                "<option value='3' style='font-size:12px;'>3</option><option value='4' style='font-size:12px;'>4</option><option value='5' style='font-size:12px;'>5</option></select></div></div></div>");
            }
            else
            {
                strProducts.Append(
                    "<div style='margin:14px  0px 0px 20px'><div class='priceDiv'>" +
     "<div name='decr' class='decr' id='decr_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' >-</div><div style='width:80px;text-align:center;float:left;background:white;padding:0px 0 0 3px;' id='cqty_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[0]["CartCount"].ToString() + "</div>" +
     "<div name='incr' class='incr' id='incr_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "' >+</div></div></div>"

                    );
            }


            strProducts.Append("<br/><br/></div></div>");

            ProductHtml = strProducts.ToString();
           
            return str.ToString();
        }
        finally
        {


        }

    }


    public string UserCartIncrDecr(UserCart objUserCart, string Mode, Calc objCalc)
    {
        try
        {

            StringBuilder str = new StringBuilder();
            DataSet ds = new CartDAL().UserCartInsertUpdate(objUserCart, Mode);


            //       if (ds.Tables[0].Rows.Count > 0)
            //       {
            //           objUserCart.ProductId = Convert.ToInt32(ds.Tables[0].Rows[0]["ProductId"]);
            //           objUserCart.Qty = Convert.ToInt32(ds.Tables[0].Rows[0]["CartCount"]);
            //           objUserCart.VariationId = Convert.ToInt32(ds.Tables[0].Rows[0]["VariationId"]);

            //           objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            //           objCalc.Price = Convert.ToInt32(ds.Tables[0].Rows[0]["Price"]);
            //           objCalc.Qty = Convert.ToInt32(ds.Tables[0].Rows[0]["CartCount"]);


            //           objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            //           objCalc.ProductSubTotal = Convert.ToInt32(ds.Tables[0].Rows[0]["Price"]) * Convert.ToInt32(ds.Tables[0].Rows[0]["Qty"]);
            //           objCalc.SubTotal = Convert.ToInt32(ds.Tables[0].Rows[0]["SubTotal"]);
            //           objCalc.VariationId = ds.Tables[0].Rows[0]["VariationId"].ToString();

            //           decimal Price = Convert.ToDecimal(ds.Tables[0].Rows[0]["Price"]);
            //           decimal Mrp = Convert.ToDecimal(ds.Tables[0].Rows[0]["Mrp"]);

            //           int CartCount = Convert.ToInt16(ds.Tables[0].Rows[0]["CartCount"]);


            //           str.Append("<td><img src='ProductImages/T_" + ds.Tables[0].Rows[0]["PhotoUrl"] + "' style='width:45px;height:46px;margin:5px' alt='' /></td><td><b>" + ds.Tables[0].Rows[0]["Name"] + "</b><br/>" + ds.Tables[0].Rows[0]["ProductDesc"] + "</td><td>Rs " + ds.Tables[0].Rows[0]["Price"] + "</td>");
            //           str.Append("<td><div class='button'>" +
            //"<div style='width:55px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;'>" +
            //"<div  style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer'   name='cartminus' id='cm_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'>-</div>" +
            //"<div style='width:23px;height:20px;float:left;background:white;padding:0px 0 0 3px;'  id='cq_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[0]["CartCount"] + "</div>" +
            //"<div style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer'   name='cartadd' id='cp_" + ds.Tables[0].Rows[0]["VariationId"].ToString() + "'>+</div>" +
            //"</div></td>");
            //           str.Append("<td>Rs. <span id='span" + ds.Tables[0].Rows[0]["ProductId"].ToString() + ds.Tables[0].Rows[0]["VariationId"] + "' >" + Convert.ToInt64(ds.Tables[0].Rows[0]["CartCount"]) * Convert.ToInt64(ds.Tables[0].Rows[0]["Price"]) + "</span></td>");





            //       }
            //       else
            //       {
            //           objUserCart.Qty = Convert.ToInt32(0);

            //       }
            //       return str.ToString();

            return "";
        }
        finally
        {


        }

    }





    public int DeleteCartDetailByproductId(string SessionId, int Customerid, int ProductId)
    {
        return new CartDAL().DeleteCartDetailByproductId(SessionId, Customerid, ProductId);
    }
    public int UpdateCartDetailQtyByProductId(string SessionId, int Customerid, int ProductId, string Sign)
    {
        return new CartDAL().UpdateCartDetailQtyByProductId(SessionId, Customerid, ProductId, Sign);
    }
    public List<Cart> GetCartDetailByID(string SessionId, int Customerid)
    {
        SqlDataReader dr = new CartDAL().GetCartDetailByID(SessionId, Customerid);

        List<Cart> lstCartDetail = new List<Cart>();
        try
        {


            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Cart objCart = new Cart()
                {
                    DProductId = Convert.ToInt32(dr["Productid"]),
                    DProductName = dr["ProductName"].ToString(),
                    DProductQty = Convert.ToDecimal(dr["ProductQty"]),
                    DPrice = Convert.ToDecimal(dr["Price"]),
                    PhotoUrl = Convert.ToString(dr["PhotoUrl"]),
                };

                    lstCartDetail.Add(objCart);



                }

            }


        }



        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return lstCartDetail;


    }
}