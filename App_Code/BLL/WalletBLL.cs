﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for WalletBLL
/// </summary>
public class WalletBLL
{
    public class WalletConsumption
    {
        public int WalletConsumptionId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string PhotoUrl { get; set; }
        public string GiftName { get; set; }
        public decimal Value { get; set; }
        public int OrderId { get; set; }
        public int BillId { get; set; }
        public bool IsDispatched { get; set; }
        public DateTime DOC { get; set; }
        public string strDOC { get { return DOC.ToString("d"); } }
    }
    public void WalletUpdateDispatchStatus(int WalletConsumptionId)
    {
        new WalletDAL().WalletUpdateDispatchStatus(WalletConsumptionId);
    }
    public List<WalletConsumption> GetAllWalletGifts()
    {
        List<WalletConsumption> listWalllet = new List<WalletConsumption>();
        SqlDataReader dr = null;

        try
        {
            dr = new WalletDAL().GetAllWalletGifts();



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    WalletConsumption objWallet = new WalletConsumption()
                    {

                        WalletConsumptionId = Convert.ToInt32(dr["WalletConsumptionId"].ToString()),
                        UserId = Convert.ToInt32(dr["UserId"].ToString()),
                        UserName = dr["UserName"].ToString(),
                        GiftName = dr["GiftName"].ToString(),
                        PhotoUrl = dr["PhotoUrl"].ToString(),
                        Value = Convert.ToDecimal(dr["Value"].ToString()),
                        OrderId = Convert.ToInt32(dr["OrderId"].ToString()),
                        BillId = Convert.ToInt32(dr["BillId"].ToString()),
                        IsDispatched = Convert.ToBoolean(dr["IsDispatched"].ToString()),
                        DOC = Convert.ToDateTime(dr["DOC"].ToString()),
                    };
                    listWalllet.Add(objWallet);
                }


            }
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }
        return listWalllet;

    }
    public void GetBalanceByUser(Wallet objWallet)
    {

        SqlDataReader dr = null;

        try
        {
            dr = new WalletDAL().GetWalletBalanceByUserId(objWallet.UserId);



            if (dr.HasRows)
            {
                dr.Read();



                objWallet.UserId = Convert.ToInt32(dr["UserId"].ToString());
           
                objWallet.Total = Convert.ToDecimal(dr["Total"].ToString());
                objWallet.Used = Convert.ToDecimal(dr["Used"].ToString());

              


            }
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }


    public int InsertUpdate(Wallet objWallet)
    {

        return new WalletDAL().InsertUpdate(objWallet);
    }


    public void GetWalletStatusCart(Wallet objWallet, string SessionId)
    {

        SqlDataReader dr = null;

        try
        {
            dr = new WalletDAL().GetWalletStatusCart(objWallet.UserId, SessionId);



            if (dr.HasRows)
            {
                dr.Read();



                objWallet.UserId = Convert.ToInt32(dr["UserId"].ToString());

                objWallet.Total = Convert.ToDecimal(dr["Total"].ToString());
                objWallet.Used = Convert.ToDecimal(dr["Used"].ToString());
                objWallet.Status = dr["Status"].ToString();
                objWallet.DisAmt = Convert.ToDecimal(dr["DisVal"].ToString());
                objWallet.Points = Convert.ToInt32(dr["DisAmt"].ToString());




            }
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }

}