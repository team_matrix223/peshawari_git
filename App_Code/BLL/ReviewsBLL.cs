﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;

/// <summary>
/// Summary description for ReviewsBLL
/// </summary>
public class ReviewsBLL
{
    public List<Reviews> GetAll()
    {
        List<Reviews> ReviewsList = new List<Reviews>();

        SqlDataReader dr = new ReviewDAL().GetAll();
        try
        {



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Reviews objReviews = new Reviews()
                    {
                        ReviewId = Convert.ToInt32(dr["ReviewId"]),
                        UserId = Convert.ToInt32(dr["UserId"]),
                        UserName = dr["UserName"].ToString(),
                        Title = dr["Title"].ToString(),
                        Description = dr["Description"].ToString(),
                        IsApproved = Convert.ToBoolean(dr["IsApproved"]),
                        ReviewDate = Convert.ToDateTime(dr["ReviewDate"]),
                        Rating = dr["Rating"].ToString(),
                    };
                    ReviewsList.Add(objReviews);
                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return ReviewsList;

    }

    public int InsertUpdate(Reviews objReviews)
    {

        return new ReviewDAL().InsertUpdate(objReviews);
    }

    public Int32 InsertReviewsApproval(DataTable dt)
    {

        return new ReviewDAL().InsertApproval(dt);
    }


    public Int32 InsertReviewsDisApproval(DataTable dt)
    {

        return new ReviewDAL().InsertDisApproval(dt);
    }



    public string GetReviewsHTML()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new ReviewDAL().GetAll();
            if (dr.HasRows)
            {

                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<div class='col-sm-6'><a style='text-decoration:none'><div  style='height:139px;width:100%;background:lemon'><h2 style='border: 1px solid silver; color: Brown; padding: 7px; font-size: 20px; margin-top: 10px;'>{0}</h2>{1}<h4>{2}</h4></div></div></a></div>", dr["Title"].ToString(), dr["Description"].ToString(), dr["Rating"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }
}