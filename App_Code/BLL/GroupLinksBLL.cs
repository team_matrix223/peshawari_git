﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using System.Data;

/// <summary>
/// Summary description for GroupLinksBLL
/// </summary>
public class GroupLinksBLL
{
    public List<GroupLinks> GetByGroupId(int GroupId)
    {
        List<GroupLinks> catList = new List<GroupLinks>();

        SqlDataReader dr = null;
        try
        {
            dr = new GroupLinksDAL().GetByGroupId(GroupId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    GroupLinks objCategories = new GroupLinks()
                    {
                        SubGroupId = Convert.ToInt16(dr["SubGroupId"].ToString()),
                        SubGroupTitle = dr["SubGroupTitle"].ToString(),
                        GroupId = Convert.ToInt32(dr["GroupId"]),
                        PhotoUrl = dr["PhotoUrl"].ToString(),
                        AdminId = Convert.ToInt32(dr["AdminId"]),
                        CatgoryId = Convert.ToInt16(dr["CatgoryId"]),
                        LinkUrl = dr["LinkUrl"].ToString(),
                      
                    };
                    catList.Add(objCategories);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }


     
        return catList;

    }



    public Int16 InsertUpdate(GroupLinks objGroupLinks)
    {

        return new GroupLinksDAL().InsertUpdate(objGroupLinks);
    }



    public void GetbySubGroupId(GroupLinks objGroupLinks)
    {

        SqlDataReader dr = null;

        try
        {
            dr = new GroupLinksDAL().GetBySubGroupId(objGroupLinks.SubGroupId);



            if (dr.HasRows)
            {
                dr.Read();

                objGroupLinks.CategoryLevel1 = dr["CategoryLevel1"].ToString();

                objGroupLinks.CategoryLevel2 = dr["CategoryLevel2"].ToString();
                objGroupLinks.CategoryLevel3 = dr["CategoryLevel3"].ToString();

            }
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }

  

    public Int32 InsertSubGroupProducts(GroupLinks objGroupLinks, DataTable dt)
    {

        return new GroupLinksDAL().InsertProducts(objGroupLinks, dt);
    }

    public string GetSubGroupHtml(int GroupId, out string GroupImage, out string CategoriesHTML)
    {
        StringBuilder strBuilder = new StringBuilder();
        StringBuilder strCategories = new StringBuilder();
        SqlDataReader dr = null;
        GroupImage = "";
        CategoriesHTML = "";
        try
        {
            dr = new GroupLinksDAL().GetByGroupId(GroupId);
            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    string Url = dr["LevelNo"].ToString() == "1" ? "c=" : dr["LevelNo"].ToString() == "2" ? "s=" : "sc=";
                    string image = dr["PhotoUrl"].ToString() == "" ? "images/fruitbox.png" : "SubGroupImages/" + dr["PhotoUrl"].ToString();
                    strBuilder.Append(string.Format("<div class='col-sm-3'><a style='text-decoration:none' href='list.aspx?" + Url + "{2}'><div class='fruitbox'><img src='" + image + "' alt='' class='img-responsive' style='height:211px;'/><h2>{1}</h2><div class='explorebutton'><h3>Explore</h3></div></div></a></div>", dr["PhotoUrl"].ToString(), dr["SubGroupTitle"].ToString(), dr["CatgoryId"].ToString()));

                }
            }

            dr.NextResult();
            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    string Color = "black";
                    if (Convert.ToBoolean(dr["IsActiveGroup"]))
                    {
                        GroupImage = dr["PhotoUrl2"].ToString();
                        Color = "red";
                    }
                    strCategories.Append(string.Format("<div style='float:left;background:" + Color + ";color:white;padding:5px;margin-right:1px'><a style='text-decoration:none;color:white' href='groups.aspx?id={0}'>{1}</a></div>", dr["Id"].ToString(), dr["Title"].ToString()));

                }
            }
            CategoriesHTML = strCategories.ToString();


        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }
}