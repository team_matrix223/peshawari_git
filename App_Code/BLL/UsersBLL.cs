﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Data;


public class UsersBLL
{
    public void GetById(Users objUsers, DeliveryAddress objDeliveryAddress)
    {
        List<Users> objList = new List<Users>();
        SqlDataReader dr = null;
        try
        {
            dr = new UsersDAL().GetById(objUsers.UserId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    objUsers.UserId = Convert.ToInt32(dr["UserId"]);
                    objUsers.FirstName = dr["FirstName"].ToString();
                    objUsers.LastName = dr["LastName"].ToString();
                    objUsers.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    objUsers.MobileNo = dr["MobileNo"].ToString();
                    objUsers.EmailId = dr["EmailId"].ToString();
                    objUsers.Code = dr["Code"].ToString();
                    objUsers.Code=dr["Code"].ToString();
                }
            }
            dr.NextResult();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    objDeliveryAddress.DeliveryAddressId = Convert.ToInt64(dr["DeliveryAddressId"]);
                    objDeliveryAddress.UserId = Convert.ToInt64(dr["UserId"]);
                    objDeliveryAddress.RecipientFirstName = Convert.ToString(dr["RecipientFirstName"]);
                    objDeliveryAddress.RecipientLastName = Convert.ToString(dr["RecipientLastName"]);
                    objDeliveryAddress.MobileNo = Convert.ToString(dr["MobileNo"]);
                    objDeliveryAddress.Telephone = Convert.ToString(dr["Telephone"]);
                    objDeliveryAddress.CityId = Convert.ToInt32(dr["CityId"]);
                    objDeliveryAddress.Area = Convert.ToString(dr["Area"]);
                    objDeliveryAddress.Street = Convert.ToString(dr["Street"]);
                    objDeliveryAddress.CityId = Convert.ToInt32(dr["CityId"]);
                    objDeliveryAddress.Address = Convert.ToString(dr["Address"]);

                    objDeliveryAddress.PinCode = Convert.ToString(dr["PinCode"]);

                    objDeliveryAddress.IsPrimary = Convert.ToBoolean(dr["IsPrimary"]);

                    objDeliveryAddress.DOC = Convert.ToDateTime(dr["DOC"]);
                    objDeliveryAddress.PinCodeId = Convert.ToInt32(dr["PinCodeId"]);
                }
            }

        }
        finally
        {

            dr.Close();
            dr.Dispose();
        }

    }



    public List<DeliveryAddress> GetByMobileNo(Users objUsers)
    {
        List<DeliveryAddress> objList = new List<DeliveryAddress>();
        SqlDataReader dr = null;
        try
        {
            dr = new UsersDAL().GetUserByMobileNo(objUsers);
            if (dr.HasRows)
            {
                dr.Read();

                objUsers.UserId = Convert.ToInt32(dr["UserId"]);
                objUsers.FirstName = dr["FirstName"].ToString();
                objUsers.LastName = dr["LastName"].ToString();
                objUsers.EmailId = dr["EmailId"].ToString();
                objUsers.DOC = Convert.ToDateTime(dr["DOC"]);
                objUsers.NewsLetter = Convert.ToBoolean(dr["NewsLetter"]);
            }

            dr.NextResult();
            if (dr.HasRows)
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        DeliveryAddress objDeliveryAddress = new DeliveryAddress();

                        objDeliveryAddress.DeliveryAddressId = Convert.ToInt64(dr["DeliveryAddressId"]);
                        objDeliveryAddress.UserId = Convert.ToInt64(dr["UserId"]);
                        objDeliveryAddress.RecipientFirstName = Convert.ToString(dr["RecipientFirstName"]);
                        objDeliveryAddress.RecipientLastName = Convert.ToString(dr["RecipientLastName"]);
                        objDeliveryAddress.MobileNo = Convert.ToString(dr["MobileNo"]);
                        objDeliveryAddress.Telephone = Convert.ToString(dr["Telephone"]);
                        objDeliveryAddress.CityId = Convert.ToInt32(dr["CityId"]);
                        objDeliveryAddress.Area = Convert.ToString(dr["Area"]);
                        objDeliveryAddress.Street = Convert.ToString(dr["Street"]);
                        objDeliveryAddress.CityName = Convert.ToString(dr["CityName"]);
                        objDeliveryAddress.Address = Convert.ToString(dr["Address"]);

                        objDeliveryAddress.PinCode = Convert.ToString(dr["PinCode"]);

                        objDeliveryAddress.IsPrimary = Convert.ToBoolean(dr["IsPrimary"]);

                        objDeliveryAddress.DOC = Convert.ToDateTime(dr["DOC"]);
                        objList.Add(objDeliveryAddress);

                    }

                }

            }
        }
        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return objList;


    }




    public List<Users> GetAll()
    {
        List<Users> headingList = new List<Users>();

        SqlDataReader dr = new UsersDAL().GetAll();
        try
        {



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Users objCities = new Users()
                    {
                       UserId = Convert.ToInt32(dr["UserId"]),
                    FirstName = dr["FirstName"].ToString(),
                    LastName = dr["LastName"].ToString(),
                    EmailId = dr["EmailId"].ToString(),
                    DOC = Convert.ToDateTime(dr["DOC"]),
                    NewsLetter = Convert.ToBoolean(dr["NewsLetter"]),
                    IsActive = Convert.ToBoolean(dr["IsActive"]),
                    MobileNo = dr["MobileNo"].ToString(),
                    };
                    headingList.Add(objCities);
                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return headingList;

    }

   

    public void ResetPassword(string UserName, string Password)
    {
        new UsersDAL().ResetPassword(UserName, Password);
    }
    public string UserLoginCheck(Users objUser,string SessionId)
    {
        return new UsersDAL().userLoginCheck(objUser,SessionId);
    }
    public string ChangePassword(Users objUser)
    {
        return new UsersDAL().ChangePassword(objUser);

    }
    public Int32 InsertUpdate(Users objUser)
    {
        return new UsersDAL().InsertUpdate(objUser);
    }


    public Int32 UpdateNewlettersubscription(Users objUser)
    {
        return new UsersDAL().Updatenewlettersubscription(objUser);
    }

    public Int32 UpdateUserActive(Users objUser)
    {
        return new UsersDAL().UpdateUserActive(objUser);
    }






  


    public void GetDetailByUserId(Users objUsers)
    {

        SqlDataReader dr = null;

        try
        {
            dr = new UsersDAL().GetById(objUsers.UserId);



            if (dr.HasRows)
            {
                dr.Read();


                objUsers.UserId = Convert.ToInt32(dr["UserId"]);
                objUsers.FirstName = dr["FirstName"].ToString();
                objUsers.LastName = dr["LastName"].ToString();
                objUsers.IsActive = Convert.ToBoolean(dr["IsActive"]);
                objUsers.MobileNo = dr["MobileNo"].ToString();
                objUsers.EmailId = dr["EmailId"].ToString();
                objUsers.NewsLetter = Convert.ToBoolean(dr["NewsLetter"]);


            }
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }

}