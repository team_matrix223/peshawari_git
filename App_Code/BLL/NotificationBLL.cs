﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for NotificationBLL
/// </summary>
public class NotificationBLL
{
    public string GetNotificationDatewise()
    {
        StringBuilder strb = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new NotificationDAL().GetNotification();
            if (dr.HasRows)
            {

                string m_Date = "";
                strb.Append("<table id='tbNotification'>");
                while (dr.Read())
                {

                    if (m_Date != dr["Date"].ToString())
                    {
                        strb.Append(string.Format("<tr><td style='color:Green'><h2>{0}</h2></td></tr>", dr["Date"].ToString()));
                        m_Date = dr["Date"].ToString();
                    }
                    strb.Append(string.Format("<tr><td><table><tr><td>{1}</td><td><span style='font-weight:bold;font-size:12px'> <a style='text-decoration:none' href=" + dr["Url"].ToString() + ">{2}</a></span> </td><td style='font-style:italic;font-size:11px'>{3}</td></tr></table></td></tr>", dr["Date"].ToString(), dr["Img"], dr["Title"].ToString(), dr["TT"].ToString()));
                    strb.Append("<tr><td colspan='100%'><hr style='border:dashed 1px silver;margin:2px'/></td></tr>");

                }
                strb.Append("</table>");
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }



        return strb.ToString();

    }

    public Int16 UpdateNotification(Notification objnotify)
    {
        return new NotificationDAL().UpdateNotification(objnotify);
    }

    public List<Notification> GetNotificationAccToType(string type)
    {
        List<Notification> bomList = new List<Notification>();
        SqlDataReader dr = new NotificationDAL().GetNotificationAccToType(type);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    Notification objissue = new Notification()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Url = dr["Url"].ToString(),
                        Img = dr["Img"].ToString(),
                        Title = dr["Title"].ToString(),
                        Code = Convert.ToInt32(dr["Code"]),
                        Mobile = Convert.ToString(dr["Mobile"]),
                        Time = Convert.ToString(dr["Time"]),

                        Type = Convert.ToString(dr["Type"]),
                    };
                    bomList.Add(objissue);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return bomList;

    }

    public Int16 CountUnreadNotification()
    {

        return new NotificationDAL().CountUnreadNotification();
    }

    public Int16 CountUnreadNotificationBirthDay()
    {

        return new NotificationDAL().CountUnreadNotificationBirthday();
    }

    public List<Notification> GetNotificationOrders()
    {
        List<Notification> bomList = new List<Notification>();
        SqlDataReader dr = new NotificationDAL().GetNotificationOrders();


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    Notification objissue = new Notification()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Url = dr["Url"].ToString(),
                        Img = dr["Img"].ToString(),
                        Title = dr["Title"].ToString(),
                        Code = Convert.ToInt32(dr["Code"]),
                        Mobile = Convert.ToString(dr["Mobile"]),
                        Time = Convert.ToString(dr["Time"]),
                        TT = Convert.ToString(dr["TT"]),
                        Type = Convert.ToString(dr["Type"]),
                    };
                    bomList.Add(objissue);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return bomList;

    }


    public List<Notification> GetNotificationBirthdayAnniversary()
    {
        List<Notification> bomList = new List<Notification>();
        SqlDataReader dr = new NotificationDAL().GetNotificationBirthdayAnniversary();


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    Notification objissue = new Notification()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Url = dr["Url"].ToString(),
                        Img = dr["Img"].ToString(),
                        Title = dr["Title"].ToString(),
                        Code = Convert.ToInt32(dr["Code"]),
                        Mobile = Convert.ToString(dr["Mobile"]),
                        Time = Convert.ToString(dr["Time"]),
                        TT = Convert.ToString(dr["TT"]),
                        Type = Convert.ToString(dr["Type"]),
                    };
                    bomList.Add(objissue);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return bomList;

    }

    public List<Notification> GetNotification()
    {
        List<Notification> bomList = new List<Notification>();
        SqlDataReader dr = new NotificationDAL().GetNotification();


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    Notification objissue = new Notification()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Url = dr["Url"].ToString(),
                        Img = dr["Img"].ToString(),
                        Title = dr["Title"].ToString(),
                        Code = Convert.ToInt32(dr["Code"]),
                        Mobile = Convert.ToString(dr["Mobile"]),
                        Time = Convert.ToString(dr["Time"]),
                        TT = Convert.ToString(dr["TT"]),
                        Type = Convert.ToString(dr["Type"]),
                    };
                    bomList.Add(objissue);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return bomList;

    }
}