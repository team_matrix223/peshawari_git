﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for BrandBLL
/// </summary>
public class BrandBLL
{
    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new BrandDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["BrandId"].ToString(), dr["Title"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }
    public List<Brand> GetAll()
    {
        List<Brand> headingList = new List<Brand>();

        SqlDataReader dr = new BrandDAL().GetAll();
        try
        {



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Brand objBrand = new Brand()
                    {
                        BrandId = Convert.ToInt32(dr["BrandId"]),
                        Title = dr["Title"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                    };
                    headingList.Add(objBrand);
                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return headingList;

    }

    public int InsertUpdate(Brand objBrand)
    {

        return new BrandDAL().InsertUpdate(objBrand);
    }
}