﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;




public class OrderBLL
{
    public MessageCredentials GetMobileNos(Int64 OrderId)
    {
        MessageCredentials objMsg = new MessageCredentials();
        SqlDataReader dr = new OrderDAL().GetMobileNos(OrderId);
        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    objMsg.AdminMobileNo = dr["AdminMobileNo"].ToString();
                    objMsg.AdminMessageText = dr["AdminMessageText"].ToString();
                    objMsg.CustomerMessageText = dr["CustomerMessageText"].ToString();
                    objMsg.CustomerName = dr["CustomerName"].ToString();
                    objMsg.CustomerMobileNo = dr["CustomerMobileNo"].ToString();
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return objMsg;

    }
    public void GetShippingAddress(Int64 OrderId, out string ShippingAddress)
    {
        ShippingAddress = " ";
        SqlDataReader dr = new OrderDAL().GetShippingAddress(OrderId);
        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    ShippingAddress = ShippingAddress + "<table><tr><td style='padding-left:15px'>" + dr["RecipientFirstName"].ToString() + " " + Convert.ToString(dr["RecipientLastName"]) + "</td></tr><tr><td style='padding-left:15px'>" + "  " + Convert.ToString(dr["RecipientMobile"]) +
                    "</td></tr><tr><td style='padding-left:15px'> " + Convert.ToString(dr["Address"]) + "</td></tr><tr><td style='padding-left:15px'>" + Convert.ToString(dr["PinCode"]) + "</td></tr></table>";


                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }

    }
    public List<Order> GetOrderByUserId(Int64 UserId, DateTime FromDate, DateTime ToDate, string Status)
    {
        List<Order> OrderList = new List<Order>();
        SqlDataReader dr = new OrderDAL().GetOrdersByUserId(UserId,FromDate,ToDate,Status );
        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    Order objOrder = new Order ()
                    {
                        OrderId = Convert.ToInt32(dr["OrderId"].ToString()),
                        OrderDate =Convert.ToDateTime( dr["OrderDate"].ToString()),
                        RecipientFirstName = dr["RecipientFirstName"].ToString(),
                        RecipientLastName = Convert.ToString(dr["RecipientLastName"]),
                        RecipientMobile = Convert.ToString(dr["RecipientMobile"]),
                        RecipientPhone = Convert.ToString(dr["RecipientPhone"].ToString()),
                        Area = Convert.ToString(dr["Area"]),
                        Street = Convert.ToString(dr["Street"]),
                        Address = Convert.ToString(dr["Address"]),
                        Pincode = Convert.ToString(dr["PinCode"]),
                        BillValue = Convert.ToDecimal(dr["BillValue"]),
                        DisAmt = Convert.ToDecimal(dr["DisAmt"]),
                        NetAmount = Convert.ToDecimal(dr["NetAmount"]),
                        ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]),
                        VatAmt = Convert.ToDecimal(dr["VatAmt"]),
                        DeliverySlot = dr["DeliverySlot"].ToString(),
                        PaymentMode = dr["PaymentMode"].ToString(),
                        DeliveryCharges = Convert.ToDecimal(dr["Deliverycharges"]),
                        FreeDeliveryAmt = Convert.ToDecimal(dr["FreeDeliveryAmt"]),
                        Comment = dr["Comments"].ToString(),
                        Status = dr["Status"].ToString(),
                        DeliveryDate = Convert.ToDateTime(dr["DeliveryDate"].ToString()),
                       //Recipient = dr["Recipient"].ToString(),
                    };
                    OrderList.Add(objOrder);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return OrderList;

    }
    public List<OrderDetail> GetOrderDetailById(Int64 OrderId)
    {
        List<OrderDetail> OrderList = new List<OrderDetail>();
        SqlDataReader dr = new OrderDAL().GetOrderDetailById(OrderId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    OrderDetail objOrder = new OrderDetail()
                    {
                        OrderDetailId = Convert.ToInt32(dr["OrderDetailId"].ToString()),
                        OrderId = Convert.ToInt32(dr["OrderId"].ToString()),
                        ProductId = Convert.ToInt32(dr["ProductId"].ToString()),
                        ProductDescription = Convert.ToString(dr["ProductDesc"].ToString()),
                        Price  = Convert.ToDecimal(dr["Price"]),
                        ProductName = Convert.ToString(dr["ProductName"]),
                        Qty = Convert.ToInt32(dr["Qty"].ToString()),
                        
                    };
                    OrderList.Add(objOrder);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return OrderList;

    }



    public List<OrderDetail> GetOrderDetailByOrderId(OrderDetail objOrder)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = new OrderDAL().GetOrderDetailByOrderId(objOrder.OrderId);

        List<OrderDetail> lstOrderDetail = new List<OrderDetail>();
        try
        {
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    OrderDetail objOrderDetail = new OrderDetail();
                    objOrderDetail.ProductId = Convert.ToInt32(dr["ProductId"]);
                    objOrderDetail.ProductName = dr["ProductName"].ToString();
                    objOrderDetail.ProductDescription = Convert.ToString(dr["ProductDescription"]);
                    objOrderDetail.Units = dr["Units"].ToString();
                    objOrderDetail.Qty = Convert.ToInt32(dr["Qty"]);
                    objOrderDetail.Price = Convert.ToDecimal(dr["Price"]);
                    objOrderDetail.DisPer = Convert.ToDecimal(dr["DisPer"]);
                    objOrderDetail.DisAmt = Convert.ToDecimal(dr["DisAmt"]);
                    objOrderDetail.VatPer = Convert.ToDecimal(dr["VatPer"]);
                    objOrderDetail.VatAmt = Convert.ToDecimal(dr["VatAmt"]);
                    objOrderDetail.ServiceTaxPer = Convert.ToDecimal(dr["ServiceTaxPer"]);
                    objOrderDetail.ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]);
                    objOrderDetail.ServiceChargePer = Convert.ToDecimal(dr["ServiceChargePer"]);
                    objOrderDetail.ServiceChargeAmt = Convert.ToDecimal(dr["ServiceChargeAmt"]);
                    objOrderDetail.Amount = Convert.ToDecimal(dr["Amount"]);

                    lstOrderDetail.Add(objOrderDetail);



                }

            }


        }



        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return lstOrderDetail;


    }

    public List<OrderDetail> GetOrdersTemp(OrderDetail objOrder)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = new OrderDAL().GetOrdersTemp(objOrder.OrderId);

        List<OrderDetail> lstOrderDetail = new List<OrderDetail>();
        try
        {
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    OrderDetail objOrderDetail = new OrderDetail();
                    objOrderDetail.ProductId = Convert.ToInt32(dr["ProductId"]);
                    objOrderDetail.ProductName = dr["ProductName"].ToString();
                    objOrderDetail.ProductDescription = Convert.ToString(dr["ProductDescription"]);
                    objOrderDetail.VariationId = Convert.ToInt32(dr["VariationId"].ToString());
                    objOrderDetail.Qty = Convert.ToInt32(dr["Qty"]);
                    objOrderDetail.Price = Convert.ToDecimal(dr["Price"]);
                    objOrderDetail.SchemeId = Convert.ToInt32(dr["SchemeId"]);
                    // objOrderDetail.DisPer = Convert.ToDecimal(dr["DisPer"]);
                    objOrderDetail.DisAmt = Convert.ToDecimal(dr["DisAmt"]);
                    // objOrderDetail.VatPer = Convert.ToDecimal(dr["VatPer"]);
                    // objOrderDetail.VatAmt = Convert.ToDecimal(dr["VatAmt"]);
                    //  objOrderDetail.ServiceTaxPer = Convert.ToDecimal(dr["ServiceTaxPer"]);
                    //  objOrderDetail.ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]);
                    //  objOrderDetail.ServiceChargePer = Convert.ToDecimal(dr["ServiceChargePer"]);
                    //  objOrderDetail.ServiceChargeAmt = Convert.ToDecimal(dr["ServiceChargeAmt"]);
                    objOrderDetail.Amount = Convert.ToDecimal(dr["Amount"]);
                    objOrderDetail.PhotoUrl = Convert.ToString(dr["PhotoUrl"]);
                    objOrderDetail.DeliveryCharges = Convert.ToDecimal(dr["DeliveryCharges"]);
                    objOrderDetail.BrandName = dr["BrandName"].ToString();
                    lstOrderDetail.Add(objOrderDetail);



                }

            }


        }



        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return lstOrderDetail;


    }


    public List<Order> GetOrderSummaryByUserId(Int64 UserId)
    {
        List<Order> OrderList = new List<Order>();
        SqlDataReader dr = new OrderDAL().GetOrdersSummaryByUserId(UserId);
        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    Order objOrder = new Order()
                    {
                        OrderId = Convert.ToInt32(dr["OrderId"].ToString()),
                        OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString()),
                        RecipientFirstName = dr["RecipientFirstName"].ToString(),
                        RecipientLastName = Convert.ToString(dr["RecipientLastName"]),
                        RecipientMobile = Convert.ToString(dr["RecipientMobile"]),
                        RecipientPhone = Convert.ToString(dr["RecipientPhone"].ToString()),
                        Area = Convert.ToString(dr["Area"]),
                        Street = Convert.ToString(dr["Street"]),
                        Address = Convert.ToString(dr["Address"]),
                        Pincode = Convert.ToString(dr["PinCode"]),
                        BillValue = Convert.ToDecimal(dr["BillValue"]),
                        DisAmt = Convert.ToDecimal(dr["DisAmt"]),
                        NetAmount = Convert.ToDecimal(dr["NetAmount"]),
                        ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]),
                        VatAmt = Convert.ToDecimal(dr["VatAmt"]),
                        Status = dr["Status"].ToString(),
                        Recipient = dr["Recipient"].ToString(),
                        DeliverySlot = dr["DeliverySlot"].ToString(),
                        PaymentMode = dr["PaymentMode"].ToString(),
                        DeliveryCharges = Convert.ToDecimal(dr["Deliverycharges"]),
                        FreeDeliveryAmt = Convert.ToDecimal(dr["FreeDeliveryAmt"]),
                        Comment = dr["Comments"].ToString(),
                        DeliveryDate = Convert.ToDateTime(dr["DeliveryDate"].ToString()),
                     
                        ResponseMessage = dr["ResponseMessage"].ToString(),
                        PaymentStatus = dr["PaymentStatus"].ToString(),
                        OrderType = dr["OrderType"].ToString(),
                    };
                    OrderList.Add(objOrder);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return OrderList;

    }


    public List<OrderDetail> GetOrderDetailSummaryById(Int64 OrderId)
    {
        List<OrderDetail> OrderList = new List<OrderDetail>();
        SqlDataReader dr = new OrderDAL().GetOrderDetailSummaryById(OrderId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    OrderDetail objOrder = new OrderDetail()
                    {
                        OrderDetailId = Convert.ToInt32(dr["OrderDetailId"].ToString()),
                        OrderId = Convert.ToInt32(dr["OrderId"].ToString()),
                        ProductId = Convert.ToInt32(dr["ProductId"].ToString()),
                        ProductDescription = Convert.ToString(dr["ProductDescription"].ToString()),
                        Price = Convert.ToDecimal(dr["Price"]),
                        ProductName = Convert.ToString(dr["ProductName"]),
                        ProductQuantityIn = dr["ProductQuantityIn"].ToString(),
                        Rate = Convert.ToDecimal(dr["Rate"]),
                        Amount = Convert.ToDecimal(dr["Amount"]),
                        Qty = Convert.ToInt32(dr["Qty"]),
                      
                    };
                    OrderList.Add(objOrder);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return OrderList;

    }


    public List<Order> GetByDate(DateTime FromDate, DateTime ToDate)
    {
        List<Order> OrderList = new List<Order>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new OrderDAL().GetByDate(FromDate, ToDate);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Order objOrder = new Order()
                    {
                        OrderId = Convert.ToInt32(dr["OrderId"].ToString()),

                        OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString()),
                        RecipientFirstName = dr["RecipientFirstName"].ToString(),
                        RecipientLastName = Convert.ToString(dr["RecipientLastName"]),
                        RecipientMobile = Convert.ToString(dr["RecipientMobile"]),
                        RecipientPhone = Convert.ToString(dr["RecipientPhone"].ToString()),
                        Area = Convert.ToString(dr["Area"]),
                        Street = Convert.ToString(dr["Street"]),
                        Address = Convert.ToString(dr["Address"]),
                        Pincode = Convert.ToString(dr["PinCode"]),
                        BillValue = Convert.ToDecimal(dr["BillValue"]),
                        DisAmt = Convert.ToDecimal(dr["DisAmt"]),
                        NetAmount = Convert.ToDecimal(dr["NetAmount"]),
                        ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]),
                        VatAmt = Convert.ToDecimal(dr["VatAmt"]),
                        Status = dr["Status"].ToString(),
                        Recipient = dr["Recipient"].ToString(),
                        CompleteAddress = dr["CompleteAddress"].ToString(),
                        CustomerName = dr["CustomerName"].ToString(),
                        DeliverySlot = dr["DeliverySlot"].ToString(),
                        PaymentMode = dr["PaymentMode"].ToString(),
                        DeliveryCharges = Convert.ToDecimal(dr["Deliverycharges"]),
                        FreeDeliveryAmt = Convert.ToDecimal(dr["FreeDeliveryAmt"]),
                        Comment = dr["Comments"].ToString(),
                        CouponNo = dr["CouponNo"].ToString(),
                        DeliveryDate = Convert.ToDateTime(dr["DeliveryDate"].ToString()),
                        GiftId = Convert.ToInt32(dr["GiftId"].ToString()),
                        GiftName = dr["GiftName"].ToString(),
                        ResponseMessage = dr["ResponseMessage"].ToString(),
                        PaymentStatus = dr["PaymentStatus"].ToString(),
                        OrderType = dr["OrderType"].ToString(),
                    };
                    OrderList.Add(objOrder);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return OrderList;

    }



    public void GetOrderbyOrderId(Order objOrder)
    {
      
        SqlDataReader dr = null;

        try
        {
            dr = new OrderDAL().GetByOrderId(objOrder.OrderId);



            if (dr.HasRows)
            {
                dr.Read();



                        objOrder.OrderId = Convert.ToInt32(dr["OrderId"].ToString());

                        objOrder. OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString());
                        objOrder. RecipientFirstName = dr["RecipientFirstName"].ToString();
                        objOrder.RecipientLastName = Convert.ToString(dr["RecipientLastName"]);
                        objOrder. RecipientMobile = Convert.ToString(dr["RecipientMobile"]);
                        objOrder.  RecipientPhone = Convert.ToString(dr["RecipientPhone"].ToString());
                        objOrder. Area = Convert.ToString(dr["Area"]);
                        objOrder. Street = Convert.ToString(dr["Street"]);
                        objOrder. Address = Convert.ToString(dr["Address"]);
                        objOrder. Pincode = Convert.ToString(dr["PinCode"]);
                        objOrder. BillValue = Convert.ToDecimal(dr["BillValue"]);
                        objOrder. DisAmt = Convert.ToDecimal(dr["DisAmt"]);
                        objOrder. NetAmount = Convert.ToDecimal(dr["NetAmount"]);
                        objOrder. ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]);
                        objOrder. VatAmt = Convert.ToDecimal(dr["VatAmt"]);
                        objOrder. Status = dr["Status"].ToString();
                        objOrder. Recipient = dr["Recipient"].ToString();
                        objOrder. CompleteAddress = dr["CompleteAddress"].ToString();
                        objOrder. CustomerName = dr["CustomerName"].ToString();
                        objOrder.  DeliverySlot = dr["DeliverySlot"].ToString();
                        objOrder.PaymentMode = dr["PaymentMode"].ToString();
                        objOrder.DeliveryCharges = Convert.ToDecimal(dr["Deliverycharges"]);
                        objOrder.FreeDeliveryAmt = Convert.ToDecimal(dr["FreeDeliveryAmt"]);
                        objOrder.DeliveryDate = Convert.ToDateTime(dr["DeliveryDate"].ToString());
                       objOrder.Comment = dr["Comments"].ToString();
                        objOrder. ResponseMessage = dr["ResponseMessage"].ToString();
                        objOrder.PaymentStatus = dr["PaymentStatus"].ToString();
                        objOrder.OrderType = dr["OrderType"].ToString();

            }
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }

    //public List<Order> GetOrderbyOrderId(Int32 OrderId)
    //{
    //    List<Order> OrderList = new List<Order>();
    //    SqlDataReader dr = null;
    //    SqlParameter[] ObjParam = new SqlParameter[0];

    //    try
    //    {
    //        dr = new OrderDAL().GetByOrderId(OrderId);
    //        if (dr.HasRows)
    //        {
    //            while (dr.Read())
    //            {
    //                Order objOrder = new Order()
    //                {
    //                    OrderId = Convert.ToInt32(dr["OrderId"].ToString()),

    //                    OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString()),
    //                    RecipientFirstName = dr["RecipientFirstName"].ToString(),
    //                    RecipientLastName = Convert.ToString(dr["RecipientLastName"]),
    //                    RecipientMobile = Convert.ToString(dr["RecipientMobile"]),
    //                    RecipientPhone = Convert.ToString(dr["RecipientPhone"].ToString()),
    //                    Area = Convert.ToString(dr["Area"]),
    //                    Street = Convert.ToString(dr["Street"]),
    //                    Address = Convert.ToString(dr["Address"]),
    //                    Pincode = Convert.ToString(dr["PinCode"]),
    //                    BillValue = Convert.ToDecimal(dr["BillValue"]),
    //                    DisAmt = Convert.ToDecimal(dr["DisAmt"]),
    //                    NetAmount = Convert.ToDecimal(dr["NetAmount"]),
    //                    ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]),
    //                    VatAmt = Convert.ToDecimal(dr["VatAmt"]),
    //                    Status = dr["Status"].ToString(),
    //                    Recipient = dr["Recipient"].ToString(),
    //                    CompleteAddress = dr["CompleteAddress"].ToString(),
    //                    CustomerName = dr["CustomerName"].ToString(),
    //                    DeliverySlot = dr["DeliverySlot"].ToString(),
    //                    PaymentMode = dr["PaymentMode"].ToString(),
    //                };
    //                OrderList.Add(objOrder);
    //            }
    //        }
    //    }
    //    finally
    //    {
    //        dr.Dispose();
    //        dr.Close();
    //        ObjParam = null;
    //    }
    //    return OrderList;

    //}


    public List<Order> InProcessOrdersGetByDate(DateTime FromDate, DateTime ToDate)
    {
        List<Order> OrderList = new List<Order>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new OrderDAL().InProcessOrdersGetByDate(FromDate, ToDate);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Order objOrder = new Order()
                    {
                        OrderId = Convert.ToInt32(dr["OrderId"].ToString()),

                        OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString()),
                        RecipientFirstName = dr["RecipientFirstName"].ToString(),
                        RecipientLastName = Convert.ToString(dr["RecipientLastName"]),
                        RecipientMobile = Convert.ToString(dr["RecipientMobile"]),
                        RecipientPhone = Convert.ToString(dr["RecipientPhone"].ToString()),
                        Area = Convert.ToString(dr["Area"]),
                        Street = Convert.ToString(dr["Street"]),
                        Address = Convert.ToString(dr["Address"]),
                        Pincode = Convert.ToString(dr["PinCode"]),
                        BillValue = Convert.ToDecimal(dr["BillValue"]),
                        DisAmt = Convert.ToDecimal(dr["DisAmt"]),
                        NetAmount = Convert.ToDecimal(dr["NetAmount"]),
                        ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]),
                        VatAmt = Convert.ToDecimal(dr["VatAmt"]),
                        Status = dr["Status"].ToString(),
                        Recipient = dr["Recipient"].ToString(),
                        CompleteAddress = dr["CompleteAddress"].ToString(),
                        CustomerName = dr["CustomerName"].ToString(),
                        DeliverySlot = dr["DeliverySlot"].ToString(),
                        PaymentMode = dr["PaymentMode"].ToString(),
                        DeliveryCharges = Convert.ToDecimal(dr["Deliverycharges"]),
                        FreeDeliveryAmt = Convert.ToDecimal(dr["FreeDeliveryAmt"]),
                        Comment = dr["Comments"].ToString(),
                        CouponNo = dr["CouponNo"].ToString(),
                        DeliveryDate = Convert.ToDateTime(dr["DeliveryDate"].ToString()),
                        ResponseMessage = dr["ResponseMessage"].ToString(),
                        PaymentStatus = dr["PaymentStatus"].ToString(),
                        OrderType = dr["OrderType"].ToString(),
                    };
                    OrderList.Add(objOrder);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return OrderList;

    }


    public void InsertTemp(OrderDetail objOrder, DataTable dt)
    {
        new OrderDAL().InsertUpdate(objOrder, dt);
    }


    public int InsertOrder(string SessionId, Int32 CustomerId, string Remarks,string PaymentMode)
    {
        int retval = new OrderDAL().OrderInsertUpdate(SessionId, CustomerId, Remarks,PaymentMode);
        return retval;
    }


    public void DeleteTempOrders(Int32 OrderId, Int32 ProductId, Int32  VariationId)
    {
        new OrderDAL().DeleteTempOrder(OrderId, ProductId, VariationId);
    }

    //public void InProcessOrders(Int32 OrderId)
    //{
    //    new OrderDAL().InProcessOrders(OrderId);
    //}
    public void BilledOrders(Int32 OrderId)
    {
        new OrderDAL().BilledOrders(OrderId);
    }
    
    public void UpdateStatusDeliver(Int32 OrderId)
    {
        new OrderDAL().UpdateStatusDeliver(OrderId);
    }
    public void InsertTemp(OrderDetail objOrder)
    {
        new OrderDAL().InsertUpdate(objOrder);
    }

    public int UpdateOrderDetailQtyByProductId(int OrderId, int ProductId, Int32  VariationId, int Qty, decimal Price, string Mode)
    {
        return new OrderDAL().UpdatetempOrderDetailQtyByProductId(OrderId, ProductId, VariationId, Qty, Price, Mode);
    }

    public List<OrderDetail> GetCancelOrderDetailById(Int64 OrderId)
    {
        List<OrderDetail> OrderList = new List<OrderDetail>();
        SqlDataReader dr = new OrderDAL().GetCancelOrderDetailById(OrderId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    OrderDetail objOrder = new OrderDetail()
                    {

                        OrderId = Convert.ToInt32(dr["OrderId"].ToString()),
                        ProductId = Convert.ToInt32(dr["ProductId"].ToString()),
                        ProductDescription = Convert.ToString(dr["ProductDesc"].ToString()),
                        Price = Convert.ToDecimal(dr["Price"]),
                        ProductName = Convert.ToString(dr["ProductName"]),
                        Qty = Convert.ToInt32(dr["Qty"].ToString()),

                    };
                    OrderList.Add(objOrder);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return OrderList;

    }




    public string GetOrderHTML(int OrderId, Calc objCalc, int Status = 1)
    {


        DataSet ds = new OrderDAL().GetCartOrderByOrderId(OrderId);
        StringBuilder str = new StringBuilder();
        if (ds.Tables[0].Rows.Count > 0)
        {
            objCalc.TotalItems = ds.Tables[0].Rows.Count;
            objCalc.DeliveryCharges = Convert.ToInt32(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            objCalc.MinimumCheckOutAmt = Convert.ToInt32(ds.Tables[0].Rows[0]["MinimumCheckOutAmt"]);
            objCalc.FreeDeliveryAmt = Convert.ToInt32(ds.Tables[0].Rows[0]["FreeDeliveryAmt"]);
          //  objCalc.Comments = ds.Tables[0].Rows[0]["Comments"].ToString();
            objCalc.DisAmt = Convert.ToDecimal(ds.Tables[0].Rows[0]["DisAmt"].ToString());
        }



        int m_sTotal = 0;
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            string ImageUrl = ds.Tables[0].Rows[i]["PhotoUrl"].ToString() != "" ? ds.Tables[0].Rows[i]["PhotoUrl"].ToString() : "noimage.jpg";

            if (Convert.ToInt32(ds.Tables[0].Rows[i]["SchemeId"]) == 0)
            {
                m_sTotal = m_sTotal + (Convert.ToInt32(ds.Tables[0].Rows[i]["Price"]) * Convert.ToInt16(ds.Tables[0].Rows[i]["Qty"]));

                str.Append("<tr style='border-bottom:dotted 1px silver' id='tp_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'><td><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "&v=" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'><img src='ProductImages/T_" + ImageUrl + "' style='width:45px;height:46px;margin:5px' alt='' /></a></td>");



                str.Append("<td><b>" + ds.Tables[0].Rows[i]["ProductName"] + "</b><br/>" + ds.Tables[0].Rows[i]["ProductDesc"] + "</td> ");

                str.Append("<td style='font-weight:bold'>Rs " + ds.Tables[0].Rows[i]["Price"] + "");

                str.Append(" <div class='button'  >");

                if (Status == 1)
                {


                    str.Append("<div style='margin:2px;width:55px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;'>" +
                    "<div  style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer'   name='cartminus' id='cm_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>-</div>" +
                    "<div style='width:23px;height:20px;float:left;background:white;padding:0px 0 0 3px;'  id='cq_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[i]["Qty"] + "</div>" +
                    "<div style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer'   name='cartadd' id='cp_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>+</div>");
                }
                else
                {
                    str.Append("<div style='width:23px;height:20px;float:left;background:white;padding:0px 0 0 3px;'  id='cq_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[i]["Qty"] + "</div>");

                }

                str.Append("</div> ");



                str.Append("</td><td style='color:red'>Rs. <span id='span" + ds.Tables[0].Rows[i]["ProductId"].ToString() + ds.Tables[0].Rows[i]["VariationId"] + "' >" + Convert.ToInt64(ds.Tables[0].Rows[i]["Qty"]) * Convert.ToInt64(ds.Tables[0].Rows[i]["Price"]) + "</span></td>");

                if (Status == 1)
                {
                    str.Append("<td><div id= '" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' name='dvClose' ><img src='images/cancel3.png' style='width:30px;cursor:pointer'/></div></td>");
                }
                str.Append("</tr>");

            }
            else
            {

                str.Append("<tr style='border-bottom:dotted 1px silver' ><td><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "&v=" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'><img src='ProductImages/T_" + ImageUrl + "' style='width:45px;height:46px;margin:5px' alt='' /></a></td>");



                str.Append("<td><b>" + ds.Tables[0].Rows[i]["ProductName"] + "</b><br/>" + ds.Tables[0].Rows[i]["ProductDesc"] + "</td><td><table><tr><td style='font-weight:bold'>Rs " + ds.Tables[0].Rows[i]["Price"] + "</td></tr><tr><td>");


                str.Append("<div style='width:55px;background:silver;text-align:center;padding:0px 0 0 3px;' >" + ds.Tables[0].Rows[i]["Qty"] + "</div>");


                str.Append("</td></tr></table></td><td><img src='img/free.gif'/></td></tr>");


            }

        }

        objCalc.SubTotal = m_sTotal;

        return str.ToString();
    }


}