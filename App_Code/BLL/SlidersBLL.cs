﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for SlidersBLL
/// </summary>
public class SlidersBLL
{



    public List<Sliders> GetAll()
    {
        List<Sliders> sliderList = new List<Sliders>();
        SqlDataReader dr = new SlidersDAL().GetAll();


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    Sliders objSlider = new Sliders()
                    {
                        SliderId = Convert.ToInt16(dr["SliderId"]),
                        ImageUrl = dr["ImageUrl"].ToString(),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        AdminId = Convert.ToInt32(dr["AdminId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        UrlName = dr["UrlName"].ToString(),

                    };
                    sliderList.Add(objSlider);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return sliderList;

    }

   
    public Int16 InsertUpdate(Sliders objSliders)
    {

        return new SlidersDAL().InsertUpdate(objSliders);
    }
}