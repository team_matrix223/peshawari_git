﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using System.Data;

public class OfferStatus
{
    public string Title { get; set; }
    public string Description { get; set; }
    public string CouponNo { get; set; }
    public int Status { get; set; }
    public string Message { get; set; }
    public string DiscountType { get; set; }
    public string GiftName { get; set; }
}
public class OffersBLL
{
    public List<Gifts> GetGiftsByOfferId(int OfferId)
    {
        List<Gifts> objList = new List<Gifts>();

        SqlDataReader dr = new OffersDAL().GetGiftsByOfferId(OfferId);
        try
        {
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Gifts objGift = new Gifts()
                    {
                        GiftId = Convert.ToInt32(dr["GiftId"].ToString()),
                        GiftName = dr["GiftName"].ToString(),
                        GiftDesc = dr["GiftDesc"].ToString(),
                        GiftPhotoUrl = dr["GiftPhotoUrl"].ToString()
                    };
                    objList.Add(objGift);
                }
            }
        }
        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return objList;
    }
    public int RemoveOffer(string CouponNo, string SessionId)
    {
        OfferStatus obj = new OfferStatus();


        StringBuilder strBuilder = new StringBuilder();
        int Status = new OffersDAL().RemoveOffer(CouponNo, SessionId);


        return Status;
    }



    public int RemovePointOffer(string SessionId)
    {
        OfferStatus obj = new OfferStatus();


        StringBuilder strBuilder = new StringBuilder();
        int Status = new OffersDAL().RemovePointOffer(SessionId);


        return Status;
    }


    public int ApplyOfferForPoints(string SessionId, Int64 UserId, decimal DisAmt)
    {
        OfferStatus obj = new OfferStatus();


        StringBuilder strBuilder = new StringBuilder();
        int Status = new OffersDAL().ApplyOfferForPoints(SessionId, UserId, DisAmt);


        return Status;
    }


    public string GetGiftsHTMLByCouponNo(string CouponNo, out string DisType)
    {
        DisType = "";
        StringBuilder strBuilder = new StringBuilder();

        DataSet ds = new OffersDAL().GetGiftsByCouponNo(CouponNo);
        try
        {

            string check = "";

            strBuilder.Append("");
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                DisType = ds.Tables[0].Rows[i]["DiscountType"].ToString();
                check = "";
                if (i == 0)
                {
                    check = "checked='checked'";
                }

                strBuilder.Append("<div style='border:1px solid #d6d3d3;border-radius:5px;float:left;max-width:250px;height:130px;margin:5px'><table style='width: 100%'><tr><td style='width: 100%;padding:5px 10px 10px 20px'><img src='OfferImages/T_" + ds.Tables[0].Rows[i]["GiftPhotoUrl"].ToString().Trim() + "' style='width:50px;height:40px;border:0 solid #d6d3d3;margin-top:5px' /></td></tr><tr><td style='font-weight:bold;text-align:center;height:44px'>" + ds.Tables[0].Rows[i]["GiftName"].ToString().Trim() + "</td></tr>");

                strBuilder.Append("<tr><td style='padding-top:5px;text-align:center'><input type='radio' name='rbChooseGift' id='rbChooseGift_" + ds.Tables[0].Rows[i]["GiftId"].ToString().Trim() + "' value='" + ds.Tables[0].Rows[i]["GiftId"].ToString().Trim() + "' " + check + "/></td></tr></table></div>");
            }

        }



        finally
        {
        }
        return strBuilder.ToString();

    }

    public OfferStatus ApplyOffer(string CouponNo, string SessionId, Int64 CustomerId, Int32 Giftid)
    {
        OfferStatus obj = new OfferStatus();


        StringBuilder strBuilder = new StringBuilder();
        SqlDataReader dr = new OffersDAL().ApplyOffer(CouponNo, SessionId, CustomerId, Giftid);
        try
        {

            if (dr.HasRows)
            {
                dr.Read();
                obj.Message = dr["Message"].ToString();
                obj.Status = Convert.ToInt16(dr["Status"]);
                obj.Title = dr["Title"].ToString();
                obj.Description = dr["Description"].ToString();
                obj.CouponNo = dr["CouponNo"].ToString();
                obj.DiscountType = dr["DiscountType"].ToString();
                obj.GiftName = dr["GiftName"].ToString();
            }
        }
        finally
        {
        }

        return obj;
    }
    public OfferStatus GetActiveOfferForUser(string SessionId, Int64 CustomerId)
    {
        OfferStatus obj = new OfferStatus();


        StringBuilder strBuilder = new StringBuilder();
        SqlDataReader dr = new OffersDAL().GetActiveOfferForUser(SessionId, CustomerId);
        try
        {

            if (dr.HasRows)
            {
                dr.Read();
                obj.Message = dr["Message"].ToString();
                obj.Status = Convert.ToInt16(dr["Status"]);
                obj.Title = dr["Title"].ToString();
                obj.Description = dr["Description"].ToString();
                obj.CouponNo = dr["CouponNo"].ToString();


            }
        }
        finally
        {
        }

        return obj;
    }

    public string GetActiveOffersHTML()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = new OffersDAL().GetActiveOffers();
        try
        {



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    string[] ArrGiftIds = dr["GiftId"].ToString().Trim().Split(',');
                    string[] ArrGiftName = dr["GiftName"].ToString().Trim().Split(',');
                    string[] ArrGiftPhotoUrl = dr["PhotoUrl"].ToString().Trim().Split(',');
                    strBuilder.Append("<div class='row' style='border-bottom:1px solid silver;margin:0px'>"

                       + "<div class='col-md-3'>"

                       + "<div style='border:dashed 1px silver;padding:24px;width:225px;margin:5px'"
                       + "<b style='font-size:20px'>e-Coupon Code</b><br /><br /><span style='font-weight:bold;font-size:24px'>" + dr["CouponNo"].ToString().Trim() + "</span></div>"

                       + "</div><div class='col-md-8'><p style='font-weight:bold;color:green;font-size: 20px; '>" + dr["Title"].ToString().Trim() + "</p>" +

                        "<p style='font-size:20px;'>" + dr["Description"].ToString().Trim() + " </p><hr style='margin:5px'/>").ToString();

                    if (ArrGiftName.Length > 0)
                    {
                        if (ArrGiftName[0].ToString().Trim() != string.Empty)
                        {
                            string check = "";
                            strBuilder.Append("<div class='row' ><div class='col-md-12'>").ToString();
                            for (int i = 0; i <= ArrGiftName.Length - 1; i++)
                            {
                                check = "";
                                if (i == 0)
                                {
                                    check = "checked='checked'";
                                }

                                strBuilder.Append("<div style='border:1px solid #d6d3d3;border-radius:5px;float:left;max-width:250px;height:130px;margin:5px'><table style='width: 100%'><tr><td style='width: 100%;padding:5px 10px 10px 20px'><img src='OfferImages/T_" + ArrGiftPhotoUrl[i].ToString().Trim() + "' style='width:50px;height:40px;border:0 solid #d6d3d3;margin-top:5px' /></td></tr><tr><td style='font-weight:bold;text-align:center;height:44px'>" + ArrGiftName[i].ToString().Trim() + "</td></tr>");

                                strBuilder.Append("<tr><td style='padding-top:5px;text-align:center'><input type='radio' name='" + dr["CouponNo"].ToString().Trim() + "' id='rbGift_" + ArrGiftIds[i].ToString().Trim() + "' value='" + ArrGiftIds[i].ToString().Trim() + "' " + check + "/></td></tr></table></div>");
                            }
                            strBuilder.Append("</div></div><hr style='margin:5px'/>");
                        }
                    }

                    strBuilder.Append("<div class='row'><div class='col-md-9'><span style='color:RED;font-weight:bold'>Please Note:</span><span style='font-weight:bold'>This Coupon expires on " + Convert.ToDateTime(dr["ToDate"]).ToLongDateString() + "</span></div><div class='col-md-3'><div id='dvApplyVoucher'  onclick='ApplyVouher(\"" + dr["CouponNo"].ToString() + "\",2,\"" + dr["DisCountType"].ToString() + "\")'  class='btnVoucher' style='margin-top:0px'>Apply</div></div></div>" +

                         "</div></div>");

                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }
    public Int16 InsertUpdate(Offers objOffer, System.Data.DataTable dtGift)
    {

        return new OffersDAL().InsertUpdate(objOffer, dtGift);
    }
    public List<Offers> GetAll()
    {
        List<Offers> objList = new List<Offers>();

        SqlDataReader dr = new OffersDAL().GetAll();
        try
        {



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Offers objOffer = new Offers()
                    {
                        OfferId = Convert.ToInt32(dr["OfferId"]),
                        OfferType = dr["OfferType"].ToString(),
                        Title = dr["Title"].ToString(),
                        Description = dr["Description"].ToString(),
                        CouponNo = dr["CouponNo"].ToString(),
                        OrderAmount = Convert.ToDecimal(dr["OrderAmount"].ToString()),
                        DiscountType = dr["DiscountType"].ToString(),
                        Points = Convert.ToInt32(dr["Points"].ToString()),
                        DiscountMode = dr["DiscountMode"].ToString(),
                        Discount = Convert.ToDecimal(dr["Discount"].ToString()),
                        FromDate = Convert.ToDateTime(dr["FromDate"].ToString()),
                        ToDate = Convert.ToDateTime(dr["ToDate"].ToString()),
                        PaymentOption = dr["PaymentOption"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        OrdersPerCustomer = Convert.ToInt32(dr["OrdersPerCustomer"]),
                        MaxLimit = Convert.ToInt32(dr["MaxLimit"]),
                    };
                    objList.Add(objOffer);
                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return objList;

    }


    public List<Users> GetUsersInfoForNewsletter(int Id)
    {
        List<Users> objList = new List<Users>();

        SqlDataReader dr = new OffersDAL().GetUsersInfoForNewsletter(Id);
        try
        {



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Users objusers = new Users()
                    {
                        UserId = Convert.ToInt32(dr["UserId"]),
                        FirstName = dr["FirstName"].ToString(),
                        LastName = dr["LastName"].ToString(),
                        MobileNo = dr["MobileNo"].ToString(),
                        EmailId = dr["EmailId"].ToString(),
                     
                    };
                    objList.Add(objusers);
                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return objList;

    }
}