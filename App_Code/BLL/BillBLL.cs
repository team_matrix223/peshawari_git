﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for BillBLL
/// </summary>
public class BillBLL
{
    public Int32 InsertUpdate(Bills objBills)
    {
        return new BillDAL().InsertUpdate(objBills);
    }



    public Int32 BillUpdate(Bills objBills)
    {
        return new BillDAL().BillUpdate(objBills);
    }

    public List<Bills> GetByDate(DateTime FromDate, DateTime ToDate)
    {
        List<Bills> BillList = new List<Bills>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new BillDAL().GetByDate(FromDate, ToDate);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Bills objbills = new Bills()
                    {
                        BillId = Convert.ToInt32(dr["BillId"].ToString()),
                        OrderId = Convert.ToInt32(dr["OrderId"].ToString()),
                        BillDate = Convert.ToDateTime(dr["BillDate"].ToString()),
                        
                        CustomerId = Convert.ToInt32(dr["CustomerId"]),
                        RecipientFirstName = dr["RecipientFirstName"].ToString(),
                        RecipientLastName = Convert.ToString(dr["RecipientLastName"]),
                        RecipientMobile = Convert.ToString(dr["RecipientMobile"]),
                        RecipientPhone = Convert.ToString(dr["RecipientPhone"].ToString()),
                        City = dr["City"].ToString(),
                        Area = Convert.ToString(dr["Area"]),
                        Street = Convert.ToString(dr["Street"]),
                        Address = Convert.ToString(dr["Address"]),
                        Pincode = Convert.ToString(dr["PinCode"]),
                        BillValue = Convert.ToDecimal(dr["BillValue"]),
                        DisPer = Convert.ToDecimal(dr["DisPer"]),
                        DisAmt = Convert.ToDecimal(dr["DisAmt"]),
                        NetAmount = Convert.ToDecimal(dr["NetAmount"]),
                        ServiceTaxPer = Convert.ToDecimal(dr["ServiceTaxPer"]),
                        ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]),
                        ServiceChargePer = Convert.ToDecimal(dr["ServiceChargePer"]),
                        ServiceChargeAmt = Convert.ToDecimal(dr["ServiceChargeAmt"]),
                        VatPer = Convert.ToDecimal(dr["VatPer"]),
                        VatAmt = Convert.ToDecimal(dr["VatAmt"]),
                        Remarks = dr["Remarks"].ToString(),
                        ExecutiveId = Convert.ToInt32(dr["ExecutiveId"]),
                        IPAddress = dr["IPAddress"].ToString(),
                        Status = dr["Status"].ToString(),
                        Recipient = dr["Recipient"].ToString(),
                        CompleteAddress = dr["CompleteAddress"].ToString(),
                        CustomerName = dr["CustomerName"].ToString(),
                        DeliveryCharges = Convert.ToDecimal(dr["DeliveryCharges"]),
                        DeliverySlot = dr["DeliverySlot"].ToString(),
                        PaymentMode = dr["PaymentMode"].ToString(),
                        PaymentStatus = dr["PaymentStatus"].ToString(),
                        ResponseMessage = dr["ResponseMessage"].ToString(),
                        OrderType = dr["OrderType"].ToString(),
                    };
                    BillList.Add(objbills);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }

    public void GetBillbyBillId(Bills objBill)
    {

        SqlDataReader dr = null;

        try
        {
            dr = new BillDAL().GetByBillId(objBill.BillId);



            if (dr.HasRows)
            {
                dr.Read();



                      objBill. BillId = Convert.ToInt32(dr["BillId"].ToString());
                      objBill. OrderId = Convert.ToInt32(dr["OrderId"].ToString());
                      objBill. BillDate = Convert.ToDateTime(dr["BillDate"].ToString());                       
                      objBill. CustomerId = Convert.ToInt32(dr["CustomerId"]);
                      objBill. RecipientFirstName = dr["RecipientFirstName"].ToString();
                      objBill. RecipientLastName = Convert.ToString(dr["RecipientLastName"]);
                      objBill. RecipientMobile = Convert.ToString(dr["RecipientMobile"]);
                      objBill. RecipientPhone = Convert.ToString(dr["RecipientPhone"].ToString());
                      objBill. City = dr["City"].ToString();
                      objBill. Area = Convert.ToString(dr["Area"]);
                      objBill.Street = Convert.ToString(dr["Street"]);
                      objBill. Address = Convert.ToString(dr["Address"]);
                      objBill. Pincode = Convert.ToString(dr["PinCode"]);
                      objBill.  BillValue = Convert.ToDecimal(dr["BillValue"]);
                      objBill.  DisPer = Convert.ToDecimal(dr["DisPer"]);
                      objBill.  DisAmt = Convert.ToDecimal(dr["DisAmt"]);
                      objBill. NetAmount = Convert.ToDecimal(dr["NetAmount"]);
                      objBill.  ServiceTaxPer = Convert.ToDecimal(dr["ServiceTaxPer"]);
                      objBill.  ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]);
                      objBill.  ServiceChargePer = Convert.ToDecimal(dr["ServiceChargePer"]);
                      objBill. ServiceChargeAmt = Convert.ToDecimal(dr["ServiceChargeAmt"]);
                      objBill.  VatPer = Convert.ToDecimal(dr["VatPer"]);
                      objBill. VatAmt = Convert.ToDecimal(dr["VatAmt"]);
                      objBill.  Remarks = dr["Remarks"].ToString();
                      objBill.  ExecutiveId = Convert.ToInt32(dr["ExecutiveId"]);
                      objBill.   IPAddress = dr["IPAddress"].ToString();
                      objBill.Status = dr["Status"].ToString();
                      objBill.DeliverySlot = dr["DeliverySlot"].ToString();
                      objBill.OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString());
                      objBill. Recipient = dr["Recipient"].ToString();
                      objBill.  CompleteAddress = dr["CompleteAddress"].ToString();
                      objBill.   CustomerName = dr["CustomerName"].ToString();
                      objBill.  DeliveryCharges = Convert.ToDecimal(dr["DeliveryCharges"]);
                      objBill. PaymentMode = dr["PaymentMode"].ToString();
                       objBill.  PaymentStatus = dr["PaymentStatus"].ToString();
                       objBill.  ResponseMessage = dr["ResponseMessage"].ToString();
                       objBill.OrderType = dr["OrderType"].ToString();

            }
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }
    public List<Bills> DeliverBillsGetByDate(DateTime FromDate, DateTime ToDate)
    {
        List<Bills> BillList = new List<Bills>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new BillDAL().DeliverBillsGetByDate(FromDate, ToDate);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Bills objbills = new Bills()
                    {
                        BillId = Convert.ToInt32(dr["BillId"].ToString()),
                        OrderId = Convert.ToInt32(dr["OrderId"].ToString()),
                        BillDate = Convert.ToDateTime(dr["BillDate"].ToString()),

                        CustomerId = Convert.ToInt32(dr["CustomerId"]),
                        RecipientFirstName = dr["RecipientFirstName"].ToString(),
                        RecipientLastName = Convert.ToString(dr["RecipientLastName"]),
                        RecipientMobile = Convert.ToString(dr["RecipientMobile"]),
                        RecipientPhone = Convert.ToString(dr["RecipientPhone"].ToString()),
                        City = dr["City"].ToString(),
                        Area = Convert.ToString(dr["Area"]),
                        Street = Convert.ToString(dr["Street"]),
                        Address = Convert.ToString(dr["Address"]),
                        Pincode = Convert.ToString(dr["PinCode"]),
                        BillValue = Convert.ToDecimal(dr["BillValue"]),
                        DisPer = Convert.ToDecimal(dr["DisPer"]),
                        DisAmt = Convert.ToDecimal(dr["DisAmt"]),
                        NetAmount = Convert.ToDecimal(dr["NetAmount"]),
                        ServiceTaxPer = Convert.ToDecimal(dr["ServiceTaxPer"]),
                        ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]),
                        ServiceChargePer = Convert.ToDecimal(dr["ServiceChargePer"]),
                        ServiceChargeAmt = Convert.ToDecimal(dr["ServiceChargeAmt"]),
                        VatPer = Convert.ToDecimal(dr["VatPer"]),
                        VatAmt = Convert.ToDecimal(dr["VatAmt"]),
                        Remarks = dr["Remarks"].ToString(),
                        ExecutiveId = Convert.ToInt32(dr["ExecutiveId"]),
                        IPAddress = dr["IPAddress"].ToString(),

                        Recipient = dr["Recipient"].ToString(),
                        CompleteAddress = dr["CompleteAddress"].ToString(),
                        CustomerName = dr["CustomerName"].ToString(),
                        DeliveryCharges = Convert.ToDecimal(dr["DeliveryCharges"]),
                        PaymentMode = dr["PaymentMode"].ToString(),
                        PaymentStatus = dr["PaymentStatus"].ToString(),
                        ResponseMessage = dr["ResponseMessage"].ToString(),
                        OrderType = dr["OrderType"].ToString(),
                    };
                    BillList.Add(objbills);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }


    public void InsertTemp(Bills objBill)
    {
        new BillDAL().InsertUpdateTemp(objBill);
    }

    public List<BillDetail> GetBillsTemp(BillDetail objBill)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = new BillDAL().GetBillsTemp(objBill.BillId);

        List<BillDetail> lstBillDetail = new List<BillDetail>();
        try
        {
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    BillDetail objBillDetail = new BillDetail();
                    objBillDetail.ProductId = Convert.ToInt32(dr["ProductId"]);
                 // objBillDetail.SchemeId = Convert.ToInt32(dr["SchemeId"]);
                    objBillDetail.ProductDescription = dr["ProductDescription"].ToString();
                    objBillDetail.Qty = Convert.ToInt32(dr["Qty"]);
                    objBillDetail.Price = Convert.ToDecimal(dr["Price"]);

                    objBillDetail.SubTotal = Convert.ToDecimal(dr["SubTotal"]);
                    objBillDetail.VariationId =Convert.ToInt64( dr["VariationId"].ToString());
                    objBillDetail.PhotoUrl = dr["PhotoUrl"].ToString();
                    objBillDetail.BrandName = dr["BrandName"].ToString();
                    lstBillDetail.Add(objBillDetail);



                }

            }


        }



        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return lstBillDetail;


    }


    public void DeleteTempBills(Int32 BillId, Int32 ProductId, Int64  VariationId)
    {
        new BillDAL().DeleteTempBill(BillId, ProductId, VariationId);
    }


    public int UpdateBillDetailQtyByProductId(int BillId, int ProductId, Int64  VariationId, int Qty, decimal Price, string Mode)
    {
        return new BillDAL().UpdatetempBillDetailQtyByProductId(BillId, ProductId, VariationId, Qty, Price, Mode);
    }
}