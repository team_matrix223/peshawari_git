﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for QickListBLL
/// </summary>
public class QuickListBLL
{
    public Int32 InsertQuickList(QuickList objQuickList, DataTable dt, string OrderType = "Regular")
    {

        return new QuickListDAL().InsertQuickList(objQuickList, dt, OrderType);
    }


    public List<QuickList> GetByDate(DateTime FromDate, DateTime ToDate)
    {
        List<QuickList> OrderList = new List<QuickList>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new QuickListDAL().GetByDate(FromDate, ToDate);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    QuickList objQuickList = new QuickList()
                    {
                        ListId = Convert.ToInt32(dr["ListId"].ToString()),

                        UserId = Convert.ToInt32(dr["UserId"].ToString()),
                        UserName = dr["UserName"].ToString(),
                        Recipient = Convert.ToString(dr["Recipient"]),
                        CompleteAddress = Convert.ToString(dr["CompleteAddress"]),
                        DeliveryDate = Convert.ToDateTime(dr["DeliveryDate"].ToString()),
                        DeliveryAddressId = Convert.ToInt32(dr["DeliveryAddressId"].ToString()),
                        DeliverySlot = Convert.ToInt32(dr["DeliverySlot"].ToString()),
                        Slot = Convert.ToString(dr["Slot"]),
                        CreatedDate = Convert.ToDateTime(dr["CreatedDate"]),
                        Status = Convert.ToString(dr["CurrentStatus"]),
                    };
                    OrderList.Add(objQuickList);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return OrderList;

    }



    public List<QuickDetail> GetListProducts(QuickDetail objList)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = new QuickListDAL().GetListProducts(objList.ListId);

        List<QuickDetail> lstProductsDetail = new List<QuickDetail>();
        try
        {
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    QuickDetail objListDetail = new QuickDetail();
                    objListDetail.ProductName = dr["ProductName"].ToString();
                    objListDetail.Qty = Convert.ToDecimal(dr["Qty"]);
                    objListDetail.Unit = Convert.ToString(dr["Unit"]);
                    
                    lstProductsDetail.Add(objListDetail);



                }

            }


        }



        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return lstProductsDetail;


    }

    public Int32 InsertQuickOrder(Order objOrder, DataTable dt)
    {
        return new QuickListDAL().InsertUpdate(objOrder, dt);
    }

}