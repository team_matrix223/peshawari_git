﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System;

/// <summary>
/// Summary description for ProductsBLL
/// </summary>
/// 
public class VariationList
{
    public int ProductId { get; set; }
    public string ItemCode { get; set; }
    public string Name { get; set; }
    public string ShortName { get; set; }
    public string Description { get; set; }
    public bool IsActive { get; set; }
    public DateTime DOC { get; set; }
    public string BrandName { get; set; }
    public string Unit { get; set; }
    public decimal Qty { get; set; }
    public decimal Price { get; set; }
    public decimal Mrp { get; set; }
    public string Type { get; set; }
    public string Units { get; set; }
    public int VariationId { get; set; }
    public VariationList()
    {
        ProductId = 0;
        VariationId = 0;
        ItemCode = string.Empty;
        Name = string.Empty;
        ShortName = string.Empty;
        Description = string.Empty;
        IsActive = false;
        DOC = DateTime.Now;
        BrandName = string.Empty;
        Unit = string.Empty;
        Qty = 0;
        Price = 0;
        Mrp = 0;
        Type = string.Empty;
        Units = string.Empty;
    }
}
public class ProductsBLL
{
    public class ProductList
    {
        public int ProductId { get; set; }
        public int VariationId { get; set; }
        public string ItemCode { get; set; }
        public string Name { get; set; }
        public decimal OnlinePrice { get; set; }
        public decimal LocalPrice { get; set; }
        public string Unit { get; set; }
    }
    public string GetAllComboProductsHtml()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetAllComboProductsHtml();
            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    string str = "combos.aspx?p=" + dr["ProductId"].ToString() + "";
                    strBuilder.Append(string.Format("<div class='col-sm-3'><div class='fruitbox1'><a style='text-decoration:none' href='" + str + "'><img src='ProductImages/T_{0}'" +
                        " alt='' class='img-responsive' /></a><a style='text-decoration:none' href='" + str + "'><h2 >{2}</h2></a><h6 style='background-color: lightgray'>Rs.{3}</h6><table style='width:100%'><tr>" +
                "<td style='padding-left:2px'><div class='explorebutton1' name='dvAddToCart'   id='a_{1}'  style='background-color:darkorange'>Buy Now</div></td></tr></table></div></div>", dr["PhotoUrl"].ToString(), dr["VariationId"].ToString(), dr["Name"].ToString(), dr["Price"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }


    public string QuickListKeywordSearch(string Keyword)
    {
        DataSet ds = new ProductsDAL().QuickListKeywordSearch(Keyword);
        StringBuilder str = new StringBuilder();

        if (ds.Tables[0].Rows.Count > 0)
        {

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                string ImageUrl = ds.Tables[0].Rows[i]["PhotoUrl"].ToString() != "" ? ds.Tables[0].Rows[i]["PhotoUrl"].ToString() : "noimage.jpg";

                //<td style='width:70px;' align='center' valign='middle'><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[i]["ProductId"] + "&v=" + ds.Tables[0].Rows[i]["VariationId"] + "'><img src='ProductImages/T_" + ImageUrl + "' style='width:50px'/></a></td>

                str.Append("<tr ><td style='width:40px;' align='center' valign='middle'><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[i]["ProductId"] + "&v=" + ds.Tables[0].Rows[i]["VariationId"] + "'><img src='ProductImages/T_" + ImageUrl + "' style='width:40px'/></a></td>");
                str.Append("<td style='width:100px'><span style='font-size:11px;'>" + ds.Tables[0].Rows[i]["BrandName"] + "</span><br/><span  style='font-size:10px'>" + ds.Tables[0].Rows[i]["Name"] + "</span></td>");
                str.Append("<td style='width:60px;text-align:center'>" + ds.Tables[0].Rows[i]["Qty"] + ds.Tables[0].Rows[i]["Unit"] + "<br/>" + ds.Tables[0].Rows[i]["Price"] + "</td>");
                str.Append("<td style='padding-right:10px;width:20px'><a href='javascript:QLAdd(\"" + ds.Tables[0].Rows[i]["Name"] + "\"," + ds.Tables[0].Rows[i]["Qty"] + ",\"" + ds.Tables[0].Rows[i]["Unit"] + "\"," + ds.Tables[0].Rows[i]["VariationId"] + " )'>Add</a><input type='text' id='Qty" + ds.Tables[0].Rows[i]["VariationId"] + "' placeholder='Qty' style='width:40px' value='1'/></td></tr>");
                str.Append("<tr><td colspan='100%' style='border-bottom:dotted 1px silver'></td></tr>");
            }


        }
        return str.ToString();
    }



    public string GetComboProductsHtmlByProductId(int ProductId)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetComboProductsHtmlByProductId(ProductId);
            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    string name = "";
                    if (dr["ShortName"].ToString().Trim() == "")
                    {
                        name = dr["Name"].ToString();
                    }
                    else
                    {
                        name = dr["Name"].ToString() + "(" + dr["ShortName"].ToString() + ")";
                    }
                    strBuilder.Append(" <div class='col-md-4'><div style='border:1px solid silver'> <img src='ProductImages/" + dr["PhotoUrl"].ToString() + "' style='width:100%;'/></div></div><div class='col-sm-8'>");

                    strBuilder.Append(" <table ><tr><td style=''><h4 style='font-family: cambria;font-size: 20px;margin-top:10px'><b>" + name + "</b></h4></td></tr>");
                    strBuilder.Append("<tr><td style='padding:10px'><h5 style='font-family:cambria;Font-size:12px;'> " + dr["Description"].ToString() + "</h5></td></tr><br/><br/><tr><td style='padding:10px'><h3 style='font-family:cambria;Font-size:15px;color:gray'>Pay     Rs." + dr["Price"].ToString() + "</h3></td></tr>");
                    strBuilder.Append("<tr><td style='padding:0px'><div class='explorebutton1' name='btnAdd'   id='b_" + dr["VariationId"].ToString() + "'  style='background-color:darkorange;float:left'>Buy Now</div></td></tr></table></div>");
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }
    public string GetComboProductsFrontEndHtml(string SessionId)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetComboProductsFrontEndHtml(SessionId);
            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    string str = "combos.aspx?p=" + dr["ProductId"].ToString() + "";
                    strBuilder.Append(string.Format("<div class='col-sm-3'><div class='fruitbox1'><a style='text-decoration:none' href='" + str + "'><img src='ProductImages/T_{0}'" +
                        " alt='' class='img-responsive' /></a><a style='text-decoration:none' href='" + str + "'><h2 >{2}</h2></a><h6 style='background-color: lightgray'>Rs.{3}</h6><table style='width:100%'><tr><td ><div class='explorebutton1' name='dvAddToCart'   id='b_{1}'  style='background-color:darkorange'>Buy Now</div></td>" +
                "<td style='padding-left:2px'><a style='text-decoration:none' href='" + str + "'><div class='explorebutton1' name='btnView'  style='background-color:gray;'>View More</div></a></td></tr></table></div></div>", dr["PhotoUrl"].ToString(), dr["VariationId"].ToString(), dr["Name"].ToString(), dr["Price"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }
    public List<Products> GetAllComboProducts()
    {
        List<Products> headingList = new List<Products>();

        SqlDataReader dr = new ProductsDAL().GetAllComboProducts();
        try
        {



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Products objProducts = new Products()
                    {
                        ProductId = Convert.ToInt32(dr["ProductId"]),
                        SubCategoryId = Convert.ToInt16(dr["SubCategoryId"]),
                        CategoryId = Convert.ToInt16(dr["CategoryId"]),
                        Name = dr["Name"].ToString(),
                        ShortName = dr["ShortName"].ToString(),
                        Description = dr["Description"].ToString(),
                        PhotoUrl = dr["PhotoUrl"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        AdminId = Convert.ToInt16(dr["AdminId"]),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        CategoryLevel3 = Convert.ToInt32(dr["CategoryLevel3"]),
                        ShortDescription = dr["ShortDescription"].ToString(),
                        Unit = dr["Unit"].ToString(),
                        Price = Convert.ToDecimal(dr["Price"]),
                        MRP = Convert.ToDecimal(dr["Mrp"]),
                        Qty = Convert.ToInt32(dr["Qty"]),
                        descr = dr["VDescription"].ToString(),
                        ItemCode = dr["ItemCode"].ToString(),
                        Type = dr["type"].ToString(),
                        BrandId = Convert.ToInt32(dr["BrandId"]),

                    };
                    headingList.Add(objProducts);
                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return headingList;

    }

    public string MobileAdvancedSearch(string SessionId, string Brands, Int64 CategoryLevel1, Int64 CategoryLevel2, Int64 CategoryLevel3, out string BrandString, out string CategoryString, out string CatName, out string Desc, out int TotalRecords, int PageId, int MinPrice, int MaxPrice, out int lPrice, out int hPrice, Int64 BrandId = 0, Int64 GroupId = 0, Int64 SubGroupId = 0)
    {

        CatName = "";
        Desc = "";
        BrandString = "";
        CategoryString = "";
        DataSet ds = new ProductsDAL().AdvancedSearch(SessionId, Brands, CategoryLevel1, CategoryLevel2, CategoryLevel3, out TotalRecords, PageId, MinPrice, MaxPrice, BrandId, GroupId, SubGroupId,16);
        StringBuilder str = new StringBuilder();
        string BrandName = "";
        string CategoryName = "";
        int[] arrPid = new int[ds.Tables[1].Rows.Count];
        for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
        {
            if (CategoryName != ds.Tables[0].Rows[j]["Title"].ToString())
            {
                CategoryName = ds.Tables[0].Rows[j]["Title"].ToString();

                string Type = ds.Tables[0].Rows[j]["Type"].ToString();

                if (Type == "Level1")
                {
                    CategoryString += " <li class='list-group-item'><a href='javascript:ChangeCategory(" + ds.Tables[0].Rows[j]["CategoryId"].ToString() + ",0,0)' style='color:white'>" + CategoryName + " </a></li>";

                }

                else if (Type == "Level2")
                {
                    CategoryString += " <li class='list-group-item'><a href='javascript:ChangeCategory(0," + ds.Tables[0].Rows[j]["CategoryId"].ToString() + ",0)'  style='color:white'>" + CategoryName + " </a></li>";
                }
                else if (Type == "Level3")
                {
                    CategoryString += " <li class='list-group-item'><a href='javascript:ChangeCategory(0,0," + ds.Tables[0].Rows[j]["CategoryId"].ToString() + ")'  style='color:white'>" + CategoryName + " </a></li>";

                }
            }
        }

        int m_MinPrice = 1000000;
        int m_MaxPrice = 0;


        //  DataView myDataView = ds.Tables[1].DefaultView;
        // myDataView.Sort = "PhotoUrl DESC";
        //  DataTable dt = myDataView.ToTable();

        for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
        {




            CatName = ds.Tables[1].Rows[i]["BreadCrumb"].ToString();
            Desc = ds.Tables[1].Rows[i]["CategoryDescription"].ToString();


            decimal Price = Convert.ToDecimal(ds.Tables[1].Rows[i]["Price"]);
            decimal Mrp = Convert.ToDecimal(ds.Tables[1].Rows[i]["Mrp"]);
            int pr = (int)Price;
            m_MinPrice = pr < m_MinPrice ? pr : m_MinPrice;
            m_MaxPrice = pr > m_MaxPrice ? pr : m_MaxPrice;

            string OfferHtml = "";
            if (Mrp > Price)
            {
                OfferHtml = "<div class='mobileOfferBg'>";
                OfferHtml += "<div class='mobileOffer'><b style='font-size:20px;'>" + Math.Round(Mrp - Price, 0) + "</b>Rs</div></div>";
            }


            int CartCount = Convert.ToInt16(ds.Tables[1].Rows[i]["CartCount"]);
            string VariationList = ds.Tables[1].Rows[i]["VariationList"].ToString();
            string bgColor = "white";
            if (CartCount > 0)
            {
                bgColor = "#D8EDC0";
            }

            string visibleStatus = "block";

            if (arrPid.Contains(Convert.ToInt32(ds.Tables[1].Rows[i]["ProductId"])))
            {
                visibleStatus = "none";
            }
            arrPid[i] = Convert.ToInt32(ds.Tables[1].Rows[i]["ProductId"]);

            string imgName = ds.Tables[1].Rows[i]["PhotoUrl"].ToString() != "" ? ds.Tables[1].Rows[i]["PhotoUrl"].ToString() : "noimage.jpg";

            str.Append("<div class='col-md-12' id='variation_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' name='product_" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "' style='display:" + visibleStatus + "'><div class='box1'      style='background:" + bgColor + ";height:auto;width:100%;padding-bottom:5px;max-width:98%'><table><tr><td valign='top'>" + OfferHtml + "<a href='productdetail.aspx?p=" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "&v=" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "'><img  src='../ProductImages/T_" + imgName + "'  class='mobileBoximage' alt=''  /></a></td><td><table><tr><td style='min-width:150px;width:150px'>");
            str.Append("<span style='font-size:10px'>" + ds.Tables[1].Rows[i]["BrandName"] + "</span><span><b>" + ds.Tables[1].Rows[i]["Name"] + "</b></span>");

            int VariationCount = Convert.ToInt16(ds.Tables[1].Rows[i]["VCount"]);

            string strVList = "";
            if (VariationCount > 1)
            {

                strVList += ("<select name='variation' selvar='" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' id='d_" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "' class='atcSelect'>");

                strVList += (VariationList);


                strVList += ("</select>");
            }
            else
            {


                strVList += ("<span>" + VariationList + "</span>");

            }

            str.Append("<table class='tbAtc'><tr><td>");
            if (Mrp > Price)
            {
                str.Append("<h2 style='text-decoration:line-through' >र " + Mrp + "</h2>");
            }
            // str.Append("<h2 style='' >" + dt.Rows[i]["ItemCode"].ToString() + "</h2>");
            str.Append("</td><td><h2>र " + Price + "</h2></td></tr></table></td><td><div id='dvChangable" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "' class='dvChangable'>");

            str.Append(strVList);
            if (CartCount == 0)
            {

                str.Append("<button name='btnAddToCart'  style='outline:none;width:100px'  type='button' id='a_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "'  class='btn btn-success atc'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</button>");
            }
            else
            {
                str.Append(
                  "<table width='100px' style='background:#d2cfcf'><tr><td style='width:35px'><div name='decr' class='decr' id='decr_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' >-</div></td><td style='background:white;width:25px'><div  id='cqty_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "'>" + ds.Tables[1].Rows[i]["CartCount"].ToString() + "</div></td><td  style='width:35px'><div name='incr' class='incr' id='incr_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' >+</div></td></tr></table>"

                  );

            
            }


            str.Append(" </div></td></tr></table></td></tr></table></div></div>");




        }


        for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
        {
            if (BrandName != ds.Tables[2].Rows[i]["BrandName"].ToString())
            {

                BrandName = ds.Tables[2].Rows[i]["BrandName"].ToString();

                BrandString += " <li class='list-group-item'>   <input type='checkbox' name='brandfilter' id='chk" + ds.Tables[2].Rows[i]["BrandId"].ToString() + "' value='" + ds.Tables[2].Rows[i]["BrandId"].ToString() + "' /> <label for='chk" + ds.Tables[2].Rows[i]["BrandId"].ToString() + "'>" + BrandName + "</label> </li>";

            }


        }

        lPrice = m_MinPrice;
        hPrice = m_MaxPrice;
        return str.ToString();



    }



    public string GetProductDetail(string ItemCode, int VariationId, out int Vid, out string HtmlLocal, out int OStatus, out int LStatus)
    {
        Vid = 0;
        HtmlLocal = "";
        OStatus = 1;
        LStatus = 1;
        StringBuilder str = new StringBuilder();
        SqlDataReader dr = new ProductsDAL().GetProductDetail(ItemCode, VariationId);
        if (dr.HasRows)
        {
            str.Append(" <table><tr> <td colspan='100%' style='padding-bottom:10px'><h4 style='color:green'><b>Product Detail On Online Server</b></h4></td> </tr><tr> <td colspan='100%' style='padding-bottom:10px'><b style='color:red'>Entered ItemCode is already allotted to following Product</b></td> </tr>");

            while (dr.Read())
            {
                Vid = Convert.ToInt32(dr["VariationId"]);
                if (VariationId == Vid)
                {
                    OStatus = 1;
                }
                else
                {
                    OStatus = 0;
                }
                str.Append("<tr><td>ProductId:</td><td>" + dr["ProductId"] + " </td></tr><tr><td>VariationId:</td><td>" + dr["VariationId"] + " </td></tr>");
                str.Append("<tr><td>Name:</td><td>" + dr["Name"] + "</td></tr><tr><td>Unit:</td><td>" + dr["Qty"] + " " + dr["Unit"] + "</td><tr><td>Price:</td><td>" + dr["Price"] + "</td></tr>");


            }
            str.Append(" </table>");

        }
        dr.NextResult();
        HtmlLocal = " <table><tr>  <td colspan='100%' style='padding-bottom:10px'><h4 style='color:green'><b>Product Detail On Local Server</b></h4></td> </tr>";
        if (dr.HasRows)
        {
            //HtmlLocal = HtmlLocal + " <tr> <td colspan='100%' style='padding-bottom:10px'><b style='color:white'>Entered ItemCode ia  allotted to following Product</b></td> </tr>";

            while (dr.Read())
            {
                HtmlLocal = HtmlLocal + "<tr><td>MasterCode:</td><td>" + dr["Master_Code"] + " </td></tr><tr><td>ItemCode:</td><td>" + dr["Item_Code"] + " </td></tr>";
                HtmlLocal = HtmlLocal + "<tr><td>ItemName:</td><td>" + dr["Item_Name"] + "</td></tr>";
            }
            HtmlLocal = HtmlLocal + "</table></div></td></tr></table>";
            LStatus = 1;
        }
        else
        {
            LStatus = 0;
            HtmlLocal = "<tr><td colspan='100%'><b style='color:red'>Entered ItemCode doses not exist in Local Database.</b></td> </tr></table>";
        }
        return str.ToString();
    }
    public string GetProductPaging(int PageNumber, int PageSize, int Cat2, out int TotalRows)
    {
        TotalRows = 0;
        StringBuilder str = new StringBuilder();
        SqlDataReader dr = new ProductsDAL().GetProductPaging(PageNumber, PageSize, Cat2);
        if (dr.HasRows)
        {

            str.Append("   <tr><th>Sno</th><th style='display:none'>VariationId</th><th>Image</th><th>Name</th><th>Unit</th><th>Type</th><th>MRP</th><th>Price</th><th>ItemCode</th><th></th></tr><tr><td></td></tr>");
            while (dr.Read())
            {
                TotalRows = Convert.ToInt16(dr["TotalRows"]);
                str.Append(string.Format("<tr><td>{0}</td><td style='width:100px;display:none'>{1}</td><td><img style='height:50px;width:50px' src='../ProductImages/T_{8}'/></td><td style='width:350px' >{2}</td><td style='width:100px'>{3}</td><td style='width:100px'>{7}</td><td style='width:100px'><input type='text' name='pmrp' id='txtMrp_{1}' value='{4}' style='width:100px'/><td style='width:100px'><input type='text' name='pprice' id='txtPrice_{1}' value='{5}' style='width:100px'/></td><td style='width:150px'><input type='text' name='pitemcode' id='txtItemCode_{1}' value='{6}' style='width:120px'/></td><td style='width:80px'><div style='margin-bottom:5px'  class='btn btn-primary btn-small' onclick='javascript:CheckRecord({1})' name='dvUpdate'><b>Update</b></div></td></tr>", dr["Sno"], dr["VariationId"], dr["Name"], dr["Units"], dr["MRP"], dr["Price"], dr["ItemCode"], dr["Type"], dr["PhotoUrl"]));


            }


        }
        return str.ToString();
    }
    public List<ProductList> ComparePriceDifferences()
    {
        List<ProductList> ProductList = new List<ProductList>();
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = new ProductsDAL().ComparePriceDifferences();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ProductList objproduct = new ProductList()
                    {
                        ProductId = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]),
                        Name = ds.Tables[0].Rows[i]["Name"].ToString(),
                        Unit = ds.Tables[0].Rows[i]["Unit"].ToString(),
                        VariationId = Convert.ToInt32(ds.Tables[0].Rows[i]["VariationId"]),
                        ItemCode = ds.Tables[0].Rows[i]["ItemCode"].ToString(),
                        OnlinePrice = Convert.ToDecimal(ds.Tables[0].Rows[i]["OnlinePrice"]),
                        LocalPrice = Convert.ToDecimal(ds.Tables[0].Rows[i]["LocalPrice"]),
                    };
                    ProductList.Add(objproduct);
                }
            }
        }
        finally
        {
            objParam = null;

        }
        return ProductList;

    }
    public Int16 UpdatePrice(DataTable VariationIdsType)
    {
        return new ProductsDAL().UpdatePrice(VariationIdsType);
    }
 
    public List<Products> GetByStatus()
    {
        List<Products> ProductList = new List<Products>();
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = new ProductsDAL().GetByStatus();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Products objproduct = new Products()
                    {

                        ProductId = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]),
                        Name = ds.Tables[0].Rows[i]["Name"].ToString(),

                    };
                    ProductList.Add(objproduct);
                }
            }

        }

        finally
        {
            objParam = null;

        }
        return ProductList;

    }
    public Int16 UpdateStatus(DataTable ProductIdsType)
    {
        return new ProductsDAL().UpdateStatus(ProductIdsType);
    }

    public string AdvancedSearchDateFilter(string SessionId, string Brands, Int64 CategoryLevel1, Int64 CategoryLevel2, Int64 CategoryLevel3, out string BrandString, out string CategoryString, out string CatName, out string Desc, out int TotalRecords, int PageId, int MinPrice, int MaxPrice, out int lPrice, out int hPrice)
    {

        CatName = "";
        Desc = "";
        BrandString = "";
        CategoryString = "";
        DataSet ds = new ProductsDAL().AdvancedSearchDateFilter(SessionId, Brands, CategoryLevel1, CategoryLevel2, CategoryLevel3, out TotalRecords, PageId, MinPrice, MaxPrice);
        StringBuilder str = new StringBuilder();
        string BrandName = "";
        string CategoryName = "";
        int[] arrPid = new int[ds.Tables[1].Rows.Count];
        for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
        {
            if (CategoryName != ds.Tables[0].Rows[j]["Title"].ToString())
            {
                CategoryName = ds.Tables[0].Rows[j]["Title"].ToString();

                string Type = ds.Tables[0].Rows[j]["Type"].ToString();
                if (Type == "Level2")
                {
                    CategoryString += " <li class='list-group-item'><a href='list.aspx?s=" + ds.Tables[0].Rows[j]["CategoryId"].ToString() + "' style='color:black'>" + CategoryName + " </a></li>";
                }
                else if (Type == "Level3")
                {
                    CategoryString += " <li class='list-group-item'><a href='list.aspx?sc=" + ds.Tables[0].Rows[j]["CategoryId"].ToString() + "'' style='color:black'>" + CategoryName + " </a></li>";

                }
            }
        }

        int m_MinPrice = 1000000;
        int m_MaxPrice = 0;
        for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
        {




            CatName = ds.Tables[1].Rows[i]["BreadCrumb"].ToString();
            Desc = ds.Tables[1].Rows[i]["CategoryDescription"].ToString();


            decimal Price = Convert.ToDecimal(ds.Tables[1].Rows[i]["Price"]);
            decimal Mrp = Convert.ToDecimal(ds.Tables[1].Rows[i]["Mrp"]);
            int pr = (int)Price;
            m_MinPrice = pr < m_MinPrice ? pr : m_MinPrice;
            m_MaxPrice = pr > m_MaxPrice ? pr : m_MaxPrice;


            int CartCount = Convert.ToInt16(ds.Tables[1].Rows[i]["CartCount"]);
            string VariationList = ds.Tables[1].Rows[i]["VariationList"].ToString();
            string bgColor = "white";
            if (CartCount > 0)
            {
                bgColor = "#D8EDC0";
            }

            string visibleStatus = "block";

            if (arrPid.Contains(Convert.ToInt32(ds.Tables[1].Rows[i]["ProductId"])))
            {
                visibleStatus = "none";
            }
            arrPid[i] = Convert.ToInt32(ds.Tables[1].Rows[i]["ProductId"]);

            string imgName = ds.Tables[1].Rows[i]["PhotoUrl"].ToString() != "" ? ds.Tables[1].Rows[i]["PhotoUrl"].ToString() : "noimage.jpg";
            //<div style='position:absolute;top:10px;left:25px'><img src='11.png'/></div>
            str.Append("<div class='col-md-3' id='variation_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' name='product_" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "' style='display:" + visibleStatus + "'><div class='box1'      style='background:" + bgColor + ";'><a href='productdetail.aspx?p=" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "&v=" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "'><img  src='ProductImages/T_" + imgName + "'  class='boximage' alt=''/></a>");
            str.Append("<span style='font-size:10px'>" + ds.Tables[1].Rows[i]["BrandName"] + "</span><span><b>" + ds.Tables[1].Rows[i]["Name"] + "</b></span>");

            int VariationCount = Convert.ToInt16(ds.Tables[1].Rows[i]["VCount"]);
            if (VariationCount > 1)
            {

                str.Append("<select name='variation' selvar='" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' id='d_" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "' class='atcSelect'>");

                str.Append(VariationList);


                str.Append("</select>");
            }
            else
            {


                str.Append("<span>" + VariationList + "</span>");

            }

            str.Append("<table class='tbAtc'><tr><td>");
            if (Mrp > Price)
            {
                str.Append("<h2 style='text-decoration:line-through' >र " + Mrp + "</h2>");
            }
            str.Append("</td><td><h2>र " + Price + "</h2></td></tr></table><div id='dvChangable" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "' class='dvChangable'>");

            if (CartCount == 0)
            {

                str.Append("<button name='btnAddToCart'  style='outline:none'  type='button' id='a_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "'  class='btn btn-success atc'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</button>" +
                "<div class='qty'> <div align='center'><div class='atcVar'>" +
                "Qty<select name='qty'  id='q_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' style='margin:1px 0px 1px 0px; padding:3px;border:0px'>" +
                "<option value='1' style='font-size:12px;'>1</option>" +
                "<option value='2' style='font-size:12px;'>2</option>" +
                "<option value='3' style='font-size:12px;'>3</option><option value='4' style='font-size:12px;'>4</option><option value='5' style='font-size:12px;'>5</option></select></div></div></div>");
            }
            else
            {
                str.Append(
                    "<div style='margin:14px  0px 0px 20px'><div class='priceDiv'>" +
     "<div name='decr' class='decr' id='decr_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' >-</div><div style='width:80px;text-align:center;float:left;background:white;padding:0px 0 0 3px;' id='cqty_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "'>" + ds.Tables[1].Rows[i]["CartCount"].ToString() + "</div>" +
     "<div name='incr' class='incr' id='incr_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' >+</div></div></div>"

                    );
            }


            str.Append("<br/><br/></div></div></div>");




        }


        for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
        {
            if (BrandName != ds.Tables[2].Rows[i]["BrandName"].ToString())
            {

                BrandName = ds.Tables[2].Rows[i]["BrandName"].ToString();

                BrandString += " <li class='list-group-item'>   <input type='checkbox' name='brandfilter' value='" + ds.Tables[2].Rows[i]["BrandId"].ToString() + "' /> " + BrandName + " </li>";

            }


        }

        lPrice = m_MinPrice;
        hPrice = m_MaxPrice;
        return str.ToString();



    }

    public string GetProductHTMLByCategoryLevel2(int CategoryId)
    {

        StringBuilder strProducts = new StringBuilder();

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetBySubCategoryId(CategoryId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    strProducts.Append("<div class='product_main'><table><tr><td><asp:HiddenField ID='hdnRepProductId' runat='server' Value='" + dr["ProductId"].ToString() + "' />");
                    strProducts.Append("<asp:HiddenField ID='hdnalbumID' runat='server' Value ='" + dr["SubCategoryId"].ToString() + "' />");
                    strProducts.Append("<img src='../ProductImages/T_" + dr["PhotoUrl"].ToString() + "' height='125px' width='125px' style='border:solid 5px silver' />");
                    strProducts.Append("</td></tr><tr><td align='center'  >" + dr["Name"].ToString() + "");
                    strProducts.Append("<asp:HiddenField ID='hdnLangTitle' runat='server' Value ='" + dr["ShortName"].ToString() + "' />");
                    strProducts.Append(string.Format("</td></tr><tr><td align='center'><input type='checkbox' id ='chkactive_" + dr["ProductId"].ToString() + "' onchange='javascript:CheckboxChange(" + dr["ProductId"].ToString() + " ,\"{0}\")' /></td>", dr["Name"].ToString()));
                    strProducts.Append("</tr></table></div>");




                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return strProducts.ToString();


    }

    public string GetProductHTMLByCategoryLevel3(int CategoryId)
    {
        StringBuilder strProducts = new StringBuilder();

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetByCategoryLevel3(CategoryId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    strProducts.Append("<div style='width:150px;float:left;margin-right:5px;border:solid 1px silver;height:210px'><table><tr><td><asp:HiddenField ID='hdnRepProductId' runat='server' Value='" + dr["ProductId"].ToString() + "' />");
                    strProducts.Append("<asp:HiddenField ID='hdnalbumID' runat='server' Value ='" + dr["SubCategoryId"].ToString() + "' />");
                    strProducts.Append("<img src='../ProductImages/T_" + dr["PhotoUrl"].ToString() + "' height='125px' width='125px' style='border:solid 5px silver' />");
                    strProducts.Append("</td></tr><tr><td align='center'  >" + dr["Name"].ToString() + ""); 
                    strProducts.Append("<asp:HiddenField ID='hdnLangTitle' runat='server' Value ='" + dr["ShortName"].ToString() + "' />");
                    strProducts.Append(string.Format("</td></tr><tr><td align='center'><input type='checkbox' id ='chkactive_" + dr["ProductId"].ToString() + "' onchange='javascript:CheckboxChange(" + dr["ProductId"].ToString() + " ,\"{0}\")' /></td>", dr["Name"].ToString()));
                    strProducts.Append("</tr></table></div>");

                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return strProducts.ToString();
    }




    public string KeywordSearch(string Keyword)
    {
        DataSet ds = new ProductsDAL().KeywordSearch(Keyword);
        StringBuilder str = new StringBuilder();


        if (ds.Tables[1].Rows.Count > 0)
        {

            for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
            {
                string ImageUrl = ds.Tables[1].Rows[i]["ImageUrl"].ToString() != "" ? ds.Tables[1].Rows[i]["ImageUrl"].ToString() : "noimage.jpg";
                str.Append("<tr ><td style='width:70px;' align='center' valign='middle'><a href='recipe.aspx?id=" + ds.Tables[1].Rows[i]["RecipeId"]+ "'><img src='RecipeImages/" + ImageUrl + "' style='width:50px'/></a></td>");
                str.Append("<td colspan='3'><span style='font-size:12px'>" + ds.Tables[1].Rows[i]["Title"] + "</span></td><td style='padding-right:20px'><a href='recipe.aspx?id=" + ds.Tables[1].Rows[i]["RecipeId"] + "'><img style='margin:5px 0px 5px' src='images/view_button.png'/></a></td></tr>");
                str.Append("<tr><td colspan='100%' style='border-bottom:dotted 1px silver'></td></tr>");
            }


        }


        if (ds.Tables[0].Rows.Count > 0)
        {

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                string ImageUrl = ds.Tables[0].Rows[i]["PhotoUrl"].ToString() != "" ? ds.Tables[0].Rows[i]["PhotoUrl"].ToString() : "noimage.jpg";
                str.Append("<tr ><td style='width:70px;' align='center' valign='middle'><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[i]["ProductId"] + "&v=" + ds.Tables[0].Rows[i]["VariationId"] + "'><img src='ProductImages/T_" + ImageUrl + "' style='width:50px'/></a></td>");
                str.Append("<td><span style='font-size:12px'>" + ds.Tables[0].Rows[i]["BrandName"] + "</span><br/><span  style='font-size:10px'>" + ds.Tables[0].Rows[i]["Name"] + "</span></td><td style='width:100px;text-align:center'>" + ds.Tables[0].Rows[i]["Qty"] + ds.Tables[0].Rows[i]["Unit"] + "</td><td  style='width:100px;text-align:center'>Rs. " + ds.Tables[0].Rows[i]["Price"] + "</td><td style='padding-right:20px'><button class='btn btn-success atc' id='search_" + ds.Tables[0].Rows[i]["VariationId"] + "' type='button' style='outline:none' name='btnAddToCart'><img style='padding-right:5px;' alt='' src='images/addimg.png'>Add</button></td></tr>");
                str.Append("<tr><td colspan='100%' style='border-bottom:dotted 1px silver'></td></tr>");
            }


        }
        return str.ToString();
    }


    public string GetByCategoryLevel1forgroups(int CategoryLevel1)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetByCategoryLevel1forgroups(CategoryLevel1);
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["ProductId"].ToString(), dr["ProductName"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
        }
        return strBuilder.ToString();

    }



    public string GetBySubCategoryforgroups(int SubCategoryId)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetBySubCategoryId(SubCategoryId);
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["ProductId"].ToString(), dr["ProductName"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
        }
        return strBuilder.ToString();

    }


    public string GetByCategory3forgroups(int SubCategoryId)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetByCategoryLevel3(SubCategoryId);
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["ProductId"].ToString(), dr["ProductName"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
        }
        return strBuilder.ToString();

    }




     public List<Products> GetByCategoryLevel1(int CategoryLevel1,Boolean IsActive)
    {
        List<Products> AlbumList = new List<Products>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetByCategoryLevel1(CategoryLevel1,IsActive);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Products objAlbum = new Products()
                    {
                        ProductId = Convert.ToInt32(dr["ProductId"]),
                        SubCategoryId = Convert.ToInt16(dr["SubCategoryId"]),
                        CategoryId = Convert.ToInt16(dr["CategoryId"]),
                        Name = dr["Name"].ToString(),
                        ShortName = dr["ShortName"].ToString(),
                        Description = dr["Description"].ToString(),
                        PhotoUrl = dr["PhotoUrl"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        AdminId = Convert.ToInt16(dr["AdminId"]),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        CategoryLevel3 = Convert.ToInt32(dr["CategoryLevel3"]),
                        ShortDescription = dr["ShortDescription"].ToString(),
                    };
                    AlbumList.Add(objAlbum);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return AlbumList;

    }
    public void GetProductDetailByProductId(Products objProduct)
    {
        List<Products> headingList = new List<Products>();
        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetProductDetailByProductId(objProduct);
            if (dr.HasRows)
            {
                dr.Read();
                objProduct.ProductId = Convert.ToInt32(dr["ProductId"]);
                objProduct.SubCategoryId = Convert.ToInt16(dr["SubCategoryId"]);
                objProduct.CategoryId = Convert.ToInt16(dr["CategoryId"]);
                objProduct.Name = dr["Name"].ToString();
                objProduct.ShortName = dr["ShortName"].ToString();
                objProduct.Description = dr["Description"].ToString();
                objProduct.ShortDescription = dr["ShortDescription"].ToString();
                objProduct.PhotoUrl = dr["PhotoUrl"].ToString();
                objProduct.IsActive = Convert.ToBoolean(dr["IsActive"]);
                objProduct.AdminId = Convert.ToInt16(dr["AdminId"]);
                objProduct.DOC = Convert.ToDateTime(dr["DOC"]);
                objProduct.CategoryLevel3 = Convert.ToInt32(dr["CategoryLevel3"]);
            }
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }
    }
    public List<Products> GetAllVariations()
    {
        List<Products> ProductList = new List<Products>();
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = new ProductsDAL().GetAllVariations();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Products objproduct = new Products()
                    {
                        VariationId = Convert.ToInt32(ds.Tables[0].Rows[i]["VariationId"]),
                        ProductId = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]),
                        Name = ds.Tables[0].Rows[i]["Name"].ToString(),
                        PhotoUrl = ds.Tables[0].Rows[i]["PhotoUrl"].ToString(),
                        Price = Convert.ToDecimal(ds.Tables[0].Rows[i]["Price"]),
                    };
                    ProductList.Add(objproduct);
                }
            }

        }

        finally
        {
            objParam = null;

        }
        return ProductList;

    }

    public void GetProductDetailByProductIdZoom(string SessionId, int ProductId, int VariationId, out string PhotoUrl, out string Name, out int CartCount)
    {
        string Price = "";
        PhotoUrl = "";
        Name = "";
        CartCount = 0;
        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetProductDetailByProductIdZoom(SessionId, ProductId, VariationId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    PhotoUrl = "<a href='ProductImages/" + dr["PhotoUrl"].ToString() + "' class='minipic' title='" + dr["Name"].ToString() + "'><img src='ProductImages/T_" + dr["PhotoUrl"].ToString() + "'' title='" + dr["Name"].ToString() + "'/></a>";
                    //<img id='zoom_01' src='ProductImages/" + dr["PhotoUrl"].ToString() + "' data-zoom-image='ProductImages/" + dr["PhotoUrl"].ToString() + "'  style='width:390px;height:390px' />
                    Name = " <tr><td valign='top' colspan='100%' style='padding-bottom:40px;padding-left:10px'><span><h3><b>" + dr["Name"].ToString() + "</b></h3></span></td></tr>";
                }
            }
            dr.NextResult();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["Selected"].ToString()) == true)
                    {
                        Price = dr["Price"].ToString();
                        Name = Name + "<tr><td style='padding-left:10px'><input name='radioVariation' id='rd_" + ProductId + "_" + dr["VariationId"].ToString() + "' type='radio' checked='checked'  /></td>";
                        CartCount = Convert.ToInt32(dr["CartCount"].ToString());
                    }
                    else
                    {
                        Name = Name + "<tr><td style='padding-left:10px'><input name='radioVariation' id='rd_" + ProductId + "_" + dr["VariationId"].ToString() + "' type='radio'  /></td>";
                    }
                    Name = Name + "<td><label for='rd_" + ProductId + "_" + dr["VariationId"].ToString() + "' style='width:250px'>" + dr["Qty"].ToString() + dr["Unit"].ToString() + dr["Description"].ToString() + " Rs." + dr["Price"].ToString() + "</label></td></tr>";

                }
            }

            Name = Name + "<tr><td colspan='100%' style='padding-top:10px;padding-left:10px'>Rs. " + Price + "<td></tr>";
            //Name = Name + "<tr><td colspan='100%' style='padding-top:10px;padding-left:10px'><table><tr><td><div name='btnAddToCart' id='d_" + ProductId + "_" + dr["VariationId"].ToString() + "' class='btn btn-success atc' style='width:55px;height:28px'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</div>" +
            //        "</td><td style='padding-left:10px'>Qty<select name='ddlQty' id='ddlQty'  style='margin:1px 1px 1px 1px; padding:3px;border:thin 1px'>" +
            //        "<option value='1' style='font-size:12px;'>1</option>" +
            //        "<option value='2' style='font-size:12px;'>2</option>" +
            //        "<option value='3' style='font-size:12px;'>3</option><option value='4' style='font-size:12px;'>4</option><option value='5' style='font-size:12px;'>5</option></select></td></tr></table></td></tr>";
        }
        finally
        {
            dr.Close();
            dr.Dispose();
        }
    }

    public string AdvancedSearch(string SessionId, string Brands, Int64 CategoryLevel1, Int64 CategoryLevel2, Int64 CategoryLevel3, out string BrandString, out string CategoryString, out string CatName, out string Desc, out int TotalRecords, int PageId, int MinPrice, int MaxPrice, out int lPrice, out int hPrice, Int64 BrandId = 0, Int64 GroupId = 0, Int64 SubGroupId = 0)
    {

        CatName = "";
        Desc = "";
        BrandString = "";
        CategoryString = "";
        DataSet ds = new ProductsDAL().AdvancedSearch(SessionId, Brands, CategoryLevel1, CategoryLevel2, CategoryLevel3, out TotalRecords, PageId, MinPrice, MaxPrice, BrandId, GroupId, SubGroupId);
        StringBuilder str = new StringBuilder();
        string BrandName = "";
        string CategoryName = "";
        int[] arrPid = new int[ds.Tables[1].Rows.Count];
        for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
        {
            if (CategoryName != ds.Tables[0].Rows[j]["Title"].ToString())
            {
                CategoryName = ds.Tables[0].Rows[j]["Title"].ToString();

                string Type = ds.Tables[0].Rows[j]["Type"].ToString();

                if (Type == "Level1")
                {
                    CategoryString += " <li class='list-group-item'><a href='javascript:ChangeCategory(" + ds.Tables[0].Rows[j]["CategoryId"].ToString() + ",0,0)' style='color:black'>" + CategoryName + " </a></li>";

                }

                else if (Type == "Level2")
                {
                    CategoryString += " <li class='list-group-item'><a href='javascript:ChangeCategory(0," + ds.Tables[0].Rows[j]["CategoryId"].ToString() + ",0)'  style='color:black'>" + CategoryName + " </a></li>";
                }
                else if (Type == "Level3")
                {
                    CategoryString += " <li class='list-group-item'><a href='javascript:ChangeCategory(0,0," + ds.Tables[0].Rows[j]["CategoryId"].ToString() + ")'  style='color:black'>" + CategoryName + " </a></li>";

                }
            }
        }

        int m_MinPrice = 1000000;
        int m_MaxPrice = 0;


      //  DataView myDataView = ds.Tables[1].DefaultView;
       // myDataView.Sort = "PhotoUrl DESC";
      //  DataTable dt = myDataView.ToTable();

        for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
        {




            CatName = ds.Tables[1].Rows[i]["BreadCrumb"].ToString();
            Desc = ds.Tables[1].Rows[i]["CategoryDescription"].ToString();


            decimal Price = Convert.ToDecimal(ds.Tables[1].Rows[i]["Price"]);
            decimal Mrp = Convert.ToDecimal(ds.Tables[1].Rows[i]["Mrp"]);
            int pr = (int)Price;
            m_MinPrice = pr < m_MinPrice ? pr : m_MinPrice;
            m_MaxPrice = pr > m_MaxPrice ? pr : m_MaxPrice;

            string OfferHtml = "";
            if (Mrp > Price)
            {
                OfferHtml="<div style='position: absolute; height: 70px; width: 66px; background: url(&quot;images/offerbg.png&quot;) no-repeat scroll 0px 0px / 100% auto transparent;'>";
                OfferHtml += "<div style='color:white;margin-right:20px;font-weight:bold;font-size:10px;margin-top:-3px'><b style='font-size:20px;'>"+Math.Round(Mrp-Price,0)+"</b>Rs</div></div>";
            }


            int CartCount = Convert.ToInt16(ds.Tables[1].Rows[i]["CartCount"]);
            string VariationList = ds.Tables[1].Rows[i]["VariationList"].ToString();
            string bgColor = "white";
            if (CartCount > 0)
            {
                bgColor = "#D8EDC0";
            }

            string visibleStatus = "block";

            if (arrPid.Contains(Convert.ToInt32(ds.Tables[1].Rows[i]["ProductId"])))
            {
                visibleStatus = "none";
            }
            arrPid[i] = Convert.ToInt32(ds.Tables[1].Rows[i]["ProductId"]);

            string imgName = ds.Tables[1].Rows[i]["PhotoUrl"].ToString() != "" ? ds.Tables[1].Rows[i]["PhotoUrl"].ToString() : "noimage.jpg";
            //<div style='position:absolute;top:10px;left:25px'><img src='11.png'/></div>
            str.Append("<div class='col-md-3' id='variation_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' name='product_" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "' style='display:" + visibleStatus + "'><div class='box1'      style='background:" + bgColor + ";'>" + OfferHtml + "<a href='productdetail.aspx?p=" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "&v=" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "'><img  src='ProductImages/T_" + imgName + "'  class='boximage' alt=''/></a>");
            str.Append("<span style='font-size:10px'>" + ds.Tables[1].Rows[i]["BrandName"] + "</span><span><b>" + ds.Tables[1].Rows[i]["Name"] + "</b></span>");

            int VariationCount = Convert.ToInt16(ds.Tables[1].Rows[i]["VCount"]);
            if (VariationCount > 1)
            {

                str.Append("<select name='variation' selvar='" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' id='d_" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "' class='atcSelect'>");

                str.Append(VariationList);


                str.Append("</select>");
            }
            else
            {


                str.Append("<span>" + VariationList + "</span>");

            }

            str.Append("<table class='tbAtc'><tr><td>");
            if (Mrp > Price)
            {
                str.Append("<h2 style='text-decoration:line-through' >र " + Mrp + "</h2>");
            }
           // str.Append("<h2 style='' >" + dt.Rows[i]["ItemCode"].ToString() + "</h2>");
            str.Append("</td><td><h2>र " + Price + "</h2></td></tr></table><div id='dvChangable" + ds.Tables[1].Rows[i]["ProductId"].ToString() + "' class='dvChangable'>");

            if (CartCount == 0)
            {

                str.Append("<button name='btnAddToCart'  style='outline:none'  type='button' id='a_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "'  class='btn btn-success atc'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</button>" +
                "<div class='qty'> <div align='center'><div class='atcVar'>" +
                "Qty<select name='qty'  id='q_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' style='margin:1px 0px 1px 0px; padding:3px;border:0px'>" +
                "<option value='1' style='font-size:12px;'>1</option>" +
                "<option value='2' style='font-size:12px;'>2</option>" +
                "<option value='3' style='font-size:12px;'>3</option><option value='4' style='font-size:12px;'>4</option><option value='5' style='font-size:12px;'>5</option></select></div></div></div>");
            }
            else
            {
                str.Append(
                    "<div style='margin:14px  0px 0px 20px'><div class='priceDiv'>" +
     "<div name='decr' class='decr' id='decr_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' >-</div><div style='width:80px;text-align:center;float:left;background:white;padding:0px 0 0 3px;' id='cqty_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "'>" + ds.Tables[1].Rows[i]["CartCount"].ToString() + "</div>" +
     "<div name='incr' class='incr' id='incr_" + ds.Tables[1].Rows[i]["VariationId"].ToString() + "' >+</div></div></div>"

                    );
            }


            str.Append("<br/><br/></div></div></div>");




        }


        for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
        {
            if (BrandName != ds.Tables[2].Rows[i]["BrandName"].ToString())
            {

                BrandName = ds.Tables[2].Rows[i]["BrandName"].ToString();

                BrandString += " <li class='list-group-item'>   <input type='checkbox' name='brandfilter' value='" + ds.Tables[2].Rows[i]["BrandId"].ToString() + "' /> " + BrandName + " </li>";

            }


        }

        lPrice = m_MinPrice;
        hPrice = m_MaxPrice;
        return str.ToString();



    }


    public string GetFeaturedComboTypesAndCombos(string SessionId)
    {

        DataSet ds = new ProductsDAL().GetFeaturedComboTypesAndCombos(SessionId);
        StringBuilder str = new StringBuilder();
        string CategoryName = "";
        bool IsFirstTime = true;
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            if (CategoryName != ds.Tables[0].Rows[i]["Title"].ToString())
            {
                if (IsFirstTime == false)
                {
                    str.Append("</div>");

                }
                CategoryName = ds.Tables[0].Rows[i]["Title"].ToString();

                str.Append("<div class='vegetables'><div class='line veginner'><span class='vegspan'> " + CategoryName + " </span></div></div><div class='row boxbordre'>");
                IsFirstTime = false;
            }


            string ProductId = ds.Tables[0].Rows[i]["ComboId"].ToString();
            string bgColor = "white";
            int Variation1 = 0;
            if (Variation1 > 0)
            {
                bgColor = "#D8EDC0";//"#EFE1B0";
            }

            str.Append("<div class='col-md-3'><div class='box1' id='cbox_" + ds.Tables[0].Rows[i]["ComboId"].ToString() + "' style='background:" + bgColor + "'><img  src='ComboImages/T_" + ds.Tables[0].Rows[i]["PhotoUrl"] + "'  class='boximage' alt=''>");
            str.Append("<span><b>" + ds.Tables[0].Rows[i]["Name"] + "</b></span><div align='center'>");
            // str.Append("<div id='btnView' id='btnView_" + ds.Tables[0].Rows[i]["ComboId"].ToString() + "' style='width:100%;background:#5cb85c;border-color:#398439' class='btn btn-success'>View Detail</div>");
            str.Append("<div id='Combo_" + ds.Tables[0].Rows[i]["ComboId"].ToString() + "' name='btnView'  class='btn btn-primary btn-small' style='width:60%;height:30px;background:#5cb85c;border-color:#398439' >View Detail</div>");
            decimal price1 = Convert.ToDecimal(ds.Tables[0].Rows[i]["Price"]);

            str.Append("</div><table style='width:100%;margin-left:30px'><tr><td><h2 style='text-decoration:line-through' id='m_" + ds.Tables[0].Rows[i]["ComboId"].ToString() + "'></h2></td><td style='padding-left:22px'><h2 id='p_" + ds.Tables[0].Rows[i]["ComboId"].ToString() + "'>" + price1 + "</h2></td></tr></table><div id='dvChangable" + ds.Tables[0].Rows[i]["ComboId"].ToString() + "' style='margin:0px;padding:0px;float:left;'>");

            if (Variation1 == 0)
            {

                str.Append("<button name='btnAddToCombo' type='button' id='Combo_" + ds.Tables[0].Rows[i]["ComboId"].ToString() + "' style='padding:2px; margin-left:10px; margin-top:10px;' class='btn btn-success'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</button>" +
                "<div class='qty'> <div align='center'><div style='margin-left:10px;background-color:#f4f2f2;width:68px;border:solid 1px silver;border-radius:4px'>" +
                "Qty<select name='qty'  id='cq_" + ds.Tables[0].Rows[i]["ComboId"].ToString() + "' style='margin:1px 0px 1px 0px; padding:3px;border:0px'>" +
                "<option value='1' style='font-size:12px;'>1</option>" +
                "<option value='2' style='font-size:12px;'>2</option>" +
                "<option value='3' style='font-size:12px;'>3</option><option value='4' style='font-size:12px;'>4</option><option value='5' style='font-size:12px;'>5</option></select></div></div></div>");
            }
            else
            {
                str.Append(
                    "<div style='margin:14px  0px 0px 20px'><div style='width:115px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;'>" +
     "<div name='Combodecr' style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' id='Combodecr_" + ds.Tables[0].Rows[i]["ComboId"].ToString() + "' >-</div><div style='width:80px;text-align:center;float:left;background:white;padding:0px 0 0 3px;' id='ccqty_" + ds.Tables[0].Rows[i]["ComboId"].ToString() + "'>" + ds.Tables[0].Rows[i]["Variation1"].ToString() + "</div>" +
     "<div name='Comboincr' style='width:10px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' id='Comboincr_" + ds.Tables[0].Rows[i]["ComboId"].ToString() + "' >+</div></div></div>"

                    );
            }


            str.Append("<br/><br/></div></div></div>");
        }


        return str.ToString();



    }


    public string GetSchemeProducts(string SessionId, Int64 SchemeId,out string SchemeTitle,out string SchemeDesc,out string SchemeImage,out int SchemeCount,out string EndDate)
    {
         DataSet ds = new ProductsDAL().GetSchemeProducts(SessionId,SchemeId);
         int[] arrPid = new int[ds.Tables[0].Rows.Count];
         StringBuilder str = new StringBuilder();
         SchemeTitle = "";
         SchemeDesc = "";
         SchemeImage = "";
         SchemeCount = 0;
         EndDate = "";

       


        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
         {
          
           
             decimal Price = Convert.ToDecimal(ds.Tables[0].Rows[i]["Price"]);
             decimal Mrp = Convert.ToDecimal(ds.Tables[0].Rows[i]["Mrp"]);
             int pr = (int)Price;


             int CartCount = Convert.ToInt16(ds.Tables[0].Rows[i]["CartCount"]);
             string VariationList = ds.Tables[0].Rows[i]["VariationList"].ToString();
             string bgColor = "white";
             if (CartCount > 0)
             {
                 bgColor = "#D8EDC0";
             }

             string visibleStatus = "block";

             if (arrPid.Contains(Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"])))
             {
                 visibleStatus = "none";
             }
             arrPid[i] = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]);

             string imgName = ds.Tables[0].Rows[i]["PhotoUrl"].ToString() != "" ? ds.Tables[0].Rows[i]["PhotoUrl"].ToString() : "noimage.jpg";
             //<div style='position:absolute;top:10px;left:25px'><img src='11.png'/></div>
             str.Append("<div id='variation_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' name='product_" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "' style='display:" + visibleStatus + "'><div class='box1'      style='background:" + bgColor + ";'><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "&v=" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'><img  src='ProductImages/T_" + imgName + "'  class='boximage' alt=''/></a>");
             str.Append("<span style='font-size:10px'>" + ds.Tables[0].Rows[i]["BrandName"] + "</span><span><b>" + ds.Tables[0].Rows[i]["Name"] + "</b></span>");

             int VariationCount = Convert.ToInt16(ds.Tables[0].Rows[i]["VCount"]);
             if (VariationCount > 1)
             {

                 str.Append("<select name='variation' selvar='" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' id='d_" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "' class='atcSelect'>");

                 str.Append(VariationList);


                 str.Append("</select>");
             }
             else
             {


                 str.Append("<span>" + VariationList + "</span>");

             }

             str.Append("<table class='tbAtc'><tr><td>");
             if (Mrp> Price)
             {
                 str.Append("<h2 style='text-decoration:line-through' >र " + Mrp + "</h2>");
             }
             str.Append("</td><td><h2>र " + Price + "</h2></td></tr></table><div id='dvChangable" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "' class='dvChangable'>");

             if (CartCount == 0)
             {

                 str.Append("<button name='btnAddToCart'  style='outline:none'  type='button' id='a_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'  class='btn btn-success atc'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</button>" +
                 "<div class='qty'> <div align='center'><div class='atcVar'>" +
                 "Qty<select name='qty'  id='q_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' style='margin:1px 0px 1px 0px; padding:3px;border:0px'>" +
                 "<option value='1' style='font-size:12px;'>1</option>" +
                 "<option value='2' style='font-size:12px;'>2</option>" +
                 "<option value='3' style='font-size:12px;'>3</option><option value='4' style='font-size:12px;'>4</option><option value='5' style='font-size:12px;'>5</option></select></div></div></div>");
             }
             else
             {
                 str.Append(
                     "<div style='margin:14px  0px 0px 20px'><div class='priceDiv'>" +
      "<div name='decr' class='decr' id='decr_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' >-</div><div style='width:80px;text-align:center;float:left;background:white;padding:0px 0 0 3px;' id='cqty_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[i]["CartCount"].ToString() + "</div>" +
      "<div name='incr' class='incr' id='incr_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' >+</div></div></div>"

                     );
             }

             str.Append("<br/><br/></div></div></div>");
        }

        if (ds.Tables[1].Rows.Count > 0)
        {
            SchemeTitle = ds.Tables[1].Rows[0]["Title"].ToString();
            SchemeDesc = ds.Tables[1].Rows[0]["Description"].ToString();
            SchemeImage = ds.Tables[1].Rows[0]["PhotoUrl"].ToString();
            SchemeCount = Convert.ToInt16(ds.Tables[1].Rows[0]["SchemeCount"]);
            EndDate = Convert.ToDateTime(ds.Tables[1].Rows[0]["EndDate"]).ToLongDateString();

        }

        return str.ToString();



    }


    public string GetFeaturedCategoriesAndProductsList(string SessionId)
    {


        DataSet ds = new ProductsDAL().GetFeaturedCategoriesAndProducts(SessionId);
        StringBuilder str = new StringBuilder();
        int[] arrPid = new int[ds.Tables[0].Rows.Count];
        string CategoryName = "";
        bool IsFirstTime = true;
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            if (CategoryName != ds.Tables[0].Rows[i]["Title"].ToString())
            {
                if (IsFirstTime == false)
                {
                    str.Append("</div>");

                }
                CategoryName = ds.Tables[0].Rows[i]["Title"].ToString();

                str.Append("<div class='vegetables'><div class='line veginner'><span class='vegspan'> " + CategoryName + " </span></div></div><div class='row boxbordre'>");
                IsFirstTime = false;
            }


            decimal Price = Convert.ToDecimal(ds.Tables[0].Rows[i]["Price"]);
            decimal Mrp = Convert.ToDecimal(ds.Tables[0].Rows[i]["Mrp"]);

            int CartCount = Convert.ToInt16(ds.Tables[0].Rows[i]["CartCount"]);
            string VariationList = ds.Tables[0].Rows[i]["VariationList"].ToString();
            string bgColor = "white";
            if (CartCount > 0)
            {
                bgColor = "#D8EDC0";
            }

            string visibleStatus = "block";

            if (arrPid.Contains(Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"])))
            {
                visibleStatus = "none";
            }
            arrPid[i] = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]);

            //<div style='position:absolute;top:10px;left:25px'><img src='11.png'/></div>
            string imgName = ds.Tables[0].Rows[i]["PhotoUrl"].ToString() != "" ? ds.Tables[0].Rows[i]["PhotoUrl"].ToString() : "noimage.jpg";

            str.Append("<div class='col-md-3' name='product_" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "' id='variation_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' style='display:" + visibleStatus + "'><div class='box1'  id='box_" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "' style='background:" + bgColor + "'><a href='productdetail.aspx?p=" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "&v=" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'><img  src='ProductImages/T_" + imgName + "'  class='boximage' alt=''/></a>");



            str.Append("<span style='font-size:10px'>" + ds.Tables[0].Rows[i]["BrandName"] + "</span><span><b>" + ds.Tables[0].Rows[i]["Name"] + "</b></span>");

            int VariationCount = Convert.ToInt16(ds.Tables[0].Rows[i]["VCount"]);
            if (VariationCount > 1)
            {

                str.Append("<select name='variation' selvar='" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' id='d_" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "' class='atcSelect'>");

                str.Append(VariationList);


                str.Append("</select>");
            }
            else
            {


                str.Append("<span>" + VariationList + "</span>");

            }

            str.Append("<table class='tbAtc'><tr><td>");
            if (Mrp > Price)
            {
                str.Append("<h2 style='text-decoration:line-through' >र " + Mrp + "</h2>");
            }
            str.Append("</td><td><h2>र " + Price + "</h2></td></tr></table><div id='dvChangable" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "' class='dvChangable'>");

            if (CartCount == 0)
            {

                str.Append("<button name='btnAddToCart' style='outline:none' type='button' id='a_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'  class='btn btn-success atc'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</button>" +
                "<div class='qty'> <div align='center'><div class='atcVar'>" +
                "Qty<select name='qty'  id='q_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' style='margin:1px 0px 1px 0px; padding:3px;border:0px'>" +
                "<option value='1' style='font-size:12px;'>1</option>" +
                "<option value='2' style='font-size:12px;'>2</option>" +
                "<option value='3' style='font-size:12px;'>3</option><option value='4' style='font-size:12px;'>4</option><option value='5' style='font-size:12px;'>5</option></select></div></div></div>");
            }
            else
            {
                str.Append(
                    "<div style='margin:14px  0px 0px 20px'><div class='priceDiv'>" +
     "<div name='decr' class='decr' id='decr_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' >-</div><div style='width:80px;text-align:center;float:left;background:white;padding:0px 0 0 3px;' id='cqty_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "'>" + ds.Tables[0].Rows[i]["CartCount"].ToString() + "</div>" +
     "<div name='incr' class='incr' id='incr_" + ds.Tables[0].Rows[i]["VariationId"].ToString() + "' >+</div></div></div>"

                    );
            }


            str.Append("<br/><br/></div></div></div>");
        }


        return str.ToString();



    }



    public List<Products> GetByCategoryId(int CategoryId)
    {
        List<Products> ProductList = new List<Products>();
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = new ProductsDAL().GetByCategoryId(CategoryId);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Products objproduct = new Products()
                    {
                        VariationId = Convert.ToInt32(ds.Tables[0].Rows[i]["VariationId"]),
                        ProductId = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]),
                        Name = ds.Tables[0].Rows[i]["Name"].ToString(),
                        Description = ds.Tables[0].Rows[i]["Description"].ToString(),
                        ShortName = ds.Tables[0].Rows[i]["ShortName"].ToString(),
                        PhotoUrl = ds.Tables[0].Rows[i]["PhotoUrl"].ToString(),
                        Price = Convert.ToDecimal(ds.Tables[0].Rows[i]["Price"]),

                    };
                    ProductList.Add(objproduct);
                }
            }

        }

        finally
        {
            objParam = null;

        }
        return ProductList;
    }


    public List<Products> GetRandomProductsByFeaturedCategory(int CategoryLevel1)
    {
        List<Products> productList = new List<Products>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetProductsByFeaturedCategory(CategoryLevel1);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Products objProducts = new Products()
                    {
                        ProductId = Convert.ToInt32(dr["ProductId"]),
                        SubCategoryId = Convert.ToInt16(dr["SubCategoryId"]),
                        CategoryId = Convert.ToInt16(dr["CategoryId"]),
                        Name = dr["Name"].ToString(),
                        ShortName = dr["ShortName"].ToString(),
                        Description = dr["Description"].ToString(),

                        Unit1 = dr["Unit1"].ToString(),
                        Price1 = Convert.ToDecimal(dr["Price1"]),
                        Mrp1 = Convert.ToDecimal(dr["Mrp1"]),
                        Qty1 = Convert.ToDecimal(dr["Qty1"]),
                        Desc1 = dr["Desc1"].ToString(),

                        Unit2 = dr["Unit2"].ToString(),
                        Price2 = Convert.ToDecimal(dr["Price2"]),
                        Mrp2 = Convert.ToDecimal(dr["Mrp2"]),
                        Qty2 = Convert.ToDecimal(dr["Qty2"]),
                        Desc2 = dr["Desc2"].ToString(),

                        Unit3 = dr["Unit3"].ToString(),
                        Price3 = Convert.ToDecimal(dr["Price3"]),
                        Mrp3 = Convert.ToDecimal(dr["Mrp3"]),
                        Qty3 = Convert.ToDecimal(dr["Qty3"]),
                        Desc3 = dr["Desc3"].ToString(),


                        Variation1 = Convert.ToInt16(dr["Variation1"]),
                        Variation2 = Convert.ToInt16(dr["Variation2"]),
                        Variation3 = Convert.ToInt16(dr["Variation3"]),


                        PhotoUrl = dr["PhotoUrl"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        AdminId = Convert.ToInt16(dr["AdminId"]),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        CategoryLevel3 = Convert.ToInt32(dr["CategoryLevel3"]),
                        ShortDescription = dr["ShortDescription"].ToString(),
                    };
                    productList.Add(objProducts);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return productList;

    }


    public List<Products> ProductGetByCatIdSubCatId(Products objProduct)
    {
        List<Products> ProductList = new List<Products>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().ProductGetByCatIdSubCatId(objProduct);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Products objproduct = new Products()
                    {
                        ProductId = Convert.ToInt32(dr["ProductId"]),
                        SubCategoryId = Convert.ToInt16(dr["SubCategoryId"]),
                        CategoryId = Convert.ToInt16(dr["CategoryId"]),
                        Name = dr["Name"].ToString(),
                        ShortName = dr["ShortName"].ToString(),
                        Description = dr["Description"].ToString(),
                        Unit1 = dr["Unit1"].ToString(),
                        Price1 = Convert.ToDecimal(dr["Price1"]),
                        Mrp1 = Convert.ToDecimal(dr["Mrp1"]),
                        Qty1 = Convert.ToDecimal(dr["Qty1"]),
                        Desc1 = dr["Desc1"].ToString(),

                        Unit2 = dr["Unit2"].ToString(),
                        Price2 = Convert.ToDecimal(dr["Price2"]),
                        Mrp2 = Convert.ToDecimal(dr["Mrp2"]),
                        Qty2 = Convert.ToDecimal(dr["Qty2"]),
                        Desc2 = dr["Desc2"].ToString(),

                        Unit3 = dr["Unit3"].ToString(),
                        Price3 = Convert.ToDecimal(dr["Price3"]),
                        Mrp3 = Convert.ToDecimal(dr["Mrp3"]),
                        Qty3 = Convert.ToDecimal(dr["Qty3"]),
                        Desc3 = dr["Desc3"].ToString(),




                        PhotoUrl = dr["PhotoUrl"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        AdminId = Convert.ToInt16(dr["AdminId"]),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        CategoryLevel3 = Convert.ToInt32(dr["CategoryLevel3"]),
                        ShortDescription = dr["ShortDescription"].ToString(),

                    };
                    ProductList.Add(objproduct);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return ProductList;

    }


    public List<Products> GetBySubCategoryId(int SubCategoryId)
    {
        List<Products> AlbumList = new List<Products>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetBySubCategoryId(SubCategoryId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Products objAlbum = new Products()
                    {
                        ProductId = Convert.ToInt32(dr["ProductId"]),
                        SubCategoryId = Convert.ToInt16(dr["SubCategoryId"]),
                        CategoryId = Convert.ToInt16(dr["CategoryId"]),
                        Name = dr["Name"].ToString(),
                        ShortName = dr["ShortName"].ToString(),
                        Description = dr["Description"].ToString(),
                        PhotoUrl = dr["PhotoUrl"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        AdminId = Convert.ToInt16(dr["AdminId"]),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        CategoryLevel3 = Convert.ToInt32(dr["CategoryLevel3"]),
                        ShortDescription = dr["ShortDescription"].ToString(),

                    };
                    AlbumList.Add(objAlbum);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return AlbumList;

    }




     public List<Products> GetBySubCategoryId2(int SubCategoryId, Boolean IsActive)
    {
        List<Products> AlbumList = new List<Products>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetBySubCategoryId2(SubCategoryId, IsActive);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Products objAlbum = new Products()
                    {
                        ProductId = Convert.ToInt32(dr["ProductId"]),
                        SubCategoryId = Convert.ToInt16(dr["SubCategoryId"]),
                        CategoryId = Convert.ToInt16(dr["CategoryId"]),
                        Name = dr["Name"].ToString(),
                        ShortName = dr["ShortName"].ToString(),
                        Description = dr["Description"].ToString(),
                        PhotoUrl = dr["PhotoUrl"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        AdminId = Convert.ToInt16(dr["AdminId"]),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        CategoryLevel3 = Convert.ToInt32(dr["CategoryLevel3"]),
                        ShortDescription = dr["ShortDescription"].ToString(),

                    };
                    AlbumList.Add(objAlbum);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return AlbumList;

    }


    public List<Products> GetByCategoryLevel3(int CategoryLevel3)
    {
        List<Products> AlbumList = new List<Products>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetByCategoryLevel3(CategoryLevel3);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Products objAlbum = new Products()
                    {
                        ProductId = Convert.ToInt32(dr["ProductId"]),
                        SubCategoryId = Convert.ToInt16(dr["SubCategoryId"]),
                        CategoryId = Convert.ToInt16(dr["CategoryId"]),
                        Name = dr["Name"].ToString(),
                        ShortName = dr["ShortName"].ToString(),
                        Description = dr["Description"].ToString(),
                        PhotoUrl = dr["PhotoUrl"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        AdminId = Convert.ToInt16(dr["AdminId"]),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        CategoryLevel3 = Convert.ToInt32(dr["CategoryLevel3"]),
                        ShortDescription = dr["ShortDescription"].ToString(),
                    };
                    AlbumList.Add(objAlbum);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return AlbumList;

    }



    public List<Products> GetByCategoryLevel3forProducts(int CategoryLevel3,Boolean IsActive)
    {
        List<Products> AlbumList = new List<Products>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetByCategoryLevel3forProducts(CategoryLevel3,IsActive);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Products objAlbum = new Products()
                    {
                        ProductId = Convert.ToInt32(dr["ProductId"]),
                        SubCategoryId = Convert.ToInt16(dr["SubCategoryId"]),
                        CategoryId = Convert.ToInt16(dr["CategoryId"]),
                        Name = dr["Name"].ToString(),
                        ShortName = dr["ShortName"].ToString(),
                        Description = dr["Description"].ToString(),
                        PhotoUrl = dr["PhotoUrl"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        AdminId = Convert.ToInt16(dr["AdminId"]),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        CategoryLevel3 = Convert.ToInt32(dr["CategoryLevel3"]),
                        ShortDescription = dr["ShortDescription"].ToString(),
                    };
                    AlbumList.Add(objAlbum);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return AlbumList;

    }



    public List<Products> GetProductsSearchByName(int CategoryLevel1, int CategoryLevel2, int CategoryLevel3,string Name)
    {
        List<Products> AlbumList = new List<Products>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetProductsSearchByName(CategoryLevel1, CategoryLevel2, CategoryLevel3,Name );
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Products objAlbum = new Products()
                    {
                        ProductId = Convert.ToInt32(dr["ProductId"]),
                        SubCategoryId = Convert.ToInt16(dr["SubCategoryId"]),
                        CategoryId = Convert.ToInt16(dr["CategoryId"]),
                        Name = dr["Name"].ToString(),
                        ShortName = dr["ShortName"].ToString(),
                        Description = dr["Description"].ToString(),
                        PhotoUrl = dr["PhotoUrl"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        AdminId = Convert.ToInt16(dr["AdminId"]),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        CategoryLevel3 = Convert.ToInt32(dr["CategoryLevel3"]),
                        ShortDescription = dr["ShortDescription"].ToString(),
                    };
                    AlbumList.Add(objAlbum);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return AlbumList;

    }

     public List<Products> GetAll(Int32 CategoryId )
    {
        List<Products> ProductList = new List<Products>();
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = new ProductsDAL().GetAll(CategoryId);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Products objproduct = new Products()
                    {
                        ProductId = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]),
                        Name = ds.Tables[0].Rows[i]["Name"].ToString(),
                    };
                    ProductList.Add(objproduct);
                }
            }
        }
        finally
        {
            objParam = null;

        }
        return ProductList;

    }
     public List<Variations> GetProductsWithImagesByName(string PName)
     {
         List<Variations> ProductList = new List<Variations>();
         SqlParameter[] objParam = new SqlParameter[0];
         DataSet ds = null;
         try
         {
             ds = new ProductsDAL().GetProductsWithImagesByName(PName);
             if (ds.Tables[0].Rows.Count > 0)
             {
                 for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                 {
                     Variations objVar = new Variations()
                     {
                         ProductId = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]),
                         VariationId = Convert.ToInt64(ds.Tables[0].Rows[i]["VariationId"]),
                         Name = ds.Tables[0].Rows[i]["Name"].ToString(),
                         PhotoUrl = ds.Tables[0].Rows[i]["PhotoUrl"].ToString(),
                     };
                     ProductList.Add(objVar);
                 }
             }
         }
         finally
         {
             objParam = null;

         }
         return ProductList;

     }
    public List<Variations> GetProductsWithImages()
    {
        List<Variations> ProductList = new List<Variations>();
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = new ProductsDAL().GetProductsWithImages();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Variations objVar = new Variations()
                    {
                        ProductId = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]),
                        VariationId = Convert.ToInt64(ds.Tables[0].Rows[i]["VariationId"]),
                        Name = ds.Tables[0].Rows[i]["Name"].ToString(),
                        PhotoUrl = ds.Tables[0].Rows[i]["PhotoUrl"].ToString(),
                    };
                    ProductList.Add(objVar);
                }
            }
        }
        finally
        {
            objParam = null;

        }
        return ProductList;

    }
    public List<Variations> GetVariationsProductsByProductId(int ProductId)
    {
        List<Variations> ProductList = new List<Variations>();
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = new ProductsDAL().GetVariationsProductsByProductId(ProductId);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Variations objVar = new Variations()
                    {
                        VariationId = Convert.ToInt32(ds.Tables[0].Rows[i]["VariationId"]),
                        Name = ds.Tables[0].Rows[i]["Name"].ToString(),
                        PhotoUrl = ds.Tables[0].Rows[i]["PhotoUrl"].ToString(),
                    };
                    ProductList.Add(objVar);
                }
            }
        }
        finally
        {
            objParam = null;

        }
        return ProductList;

    }
    public Int16 InsertUpdate(Products objProduct, DataTable dt)
    {

        return new ProductsDAL().InsertUpdate(objProduct, dt);
    }
    public Int16 InsertVariations(Int32 ProductId, Int64 VariationId, string PhotoUrl)
    {
        return new ProductsDAL().InsertVariations(ProductId, VariationId, PhotoUrl);
    }

    public Int16 UpdateProductList(Variations objEmpSal)
    {
        return new ProductsDAL().UpdateProductList(objEmpSal);
    }

    public List<VariationList> GetVariationList(Int32 CatId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@CatLevel2", CatId);
        List<VariationList> lst = new List<VariationList>();

        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GetVariationList", objParam);

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {

                VariationList obj = new VariationList();
                obj.ProductId = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]);
                obj.VariationId = Convert.ToInt32(ds.Tables[0].Rows[i]["VariationId"]);

                obj.ItemCode = Convert.ToString(ds.Tables[0].Rows[i]["ItemCode"]);
                obj.Name = Convert.ToString(ds.Tables[0].Rows[i]["Name"]);
                obj.ShortName = Convert.ToString(ds.Tables[0].Rows[i]["ShortName"]);
                obj.Description = Convert.ToString(ds.Tables[0].Rows[i]["Description"]);
                obj.IsActive = Convert.ToBoolean(ds.Tables[0].Rows[i]["IsActive"]);
                obj.DOC = Convert.ToDateTime(ds.Tables[0].Rows[i]["DOC"]);
                obj.BrandName = Convert.ToString(ds.Tables[0].Rows[i]["BrandName"]);
                obj.Unit = Convert.ToString(ds.Tables[0].Rows[i]["Unit"]);
                obj.Qty = Convert.ToInt32(ds.Tables[0].Rows[i]["Qty"]);
                obj.Price = Convert.ToInt32(ds.Tables[0].Rows[i]["Price"]);
                obj.Mrp = Convert.ToInt32(ds.Tables[0].Rows[i]["Mrp"]);
                obj.Type = Convert.ToString(ds.Tables[0].Rows[i]["Type"]);
                obj.Units = Convert.ToString(ds.Tables[0].Rows[i]["Units"]);
                lst.Add(obj);

            }

        }

        finally
        {
            objParam = null;
        }

        return lst;
    }


    public Int16 ComboProductsInsertUpdate(Products objProduct)
    {

        return new ProductsDAL().ComboProductsInsertUpdate(objProduct);
    }

    public string GetComboProductsforindex()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new ProductsDAL().GetComboProductsForIndex();
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<div class='col-sm-3'><a href='#' ><img src='Comboproducts/{3}' style='height:120px;width:100%' alt='' class='img-responsive' /><h2>{0}</h2><h3>{1}</h3<div class='btn btn-success atc'><h4>Add</h4></div></a></div>", dr["Name"].ToString(), dr["Title"].ToString(), dr["ProductId"].ToString(), dr["PhotoUrl"].ToString()));
                }

            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }
}