﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;


public class ListBLL
{
    public string  GetByUserId(int UserId)
    {
        StringBuilder strb = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new ListDAL ().GetByUserId (UserId );
            if (dr.HasRows)
            {
               // strb.Append("<table><tr><td colspan='100%'style='font-size:25px;font-family:Cambria;background-color:Black;color:white;width: 500px;text-align:center'><b>My Lists</b></td></tr>");
               strb.Append("<table>");
                while (dr.Read())
                {

                    strb.Append("<tr><td style='padding-right:20px;padding-top:20px;font-size:15px;font-family:Cambria'><b>" + dr["Title"].ToString() + "</b></td><td style='padding-top:20px'><div name='dvViewMyList' id='dvViewMyList_" + dr["ListId"].ToString() + "' ");
                    strb.Append(" class='btn btn-primary btn-small' style='background:#134051;color:white;' ><b>View Detail</b></div></td>");
                    strb.Append("<td style='padding-left:20px;padding-top:20px'><div  name ='dvAddToCartMyList' id='dvAddToCartMyList_" + dr["ListId"].ToString() + "'  class='btn btn-primary btn-small' style='background:#ff0000;color:white;' ><b>Add To Cart</b></div></td></tr>");
                }
                strb.Append("</table>");
            //    strb.Append("<tr><td style='padding-right:2px;padding-top:20px;font-size:15px;font-family:Cambria'><b>Keep Existing</b></td><td style='padding-top:20px'><input type='checkbox' id='chkKeepExisting'/></td></tr>");
            //    strb.Append("<tr><td colspan='100%'><table><td style='padding-left:20px'><div name='btnAddToCartMyList' id='btnAddToCartMyList'  class='btn btn-primary btn-small' style='background:#cc5547;color:white;' ><b>Add To Cart</b></div></td>");
            //    strb.Append("<td style='padding-left:20px'><div id='btnCancelMyList'   class='btn btn-primary btn-small' style='background:#cc5547;color:white;' ><b>Cancel</b></div></td></table></td></tr></table>");
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }



        return strb.ToString();

    }
    public string GetProductsById(int ListId)
    {
        StringBuilder strb = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new ListDAL().GetProductsById(ListId);
            if (dr.HasRows)
            {
                strb.Append("<table>");
                while (dr.Read())
                {

                    strb.Append("<tr><td style='padding-right:20px;padding-top:20px'><img src='ProductImages/T_" + dr["PhotoUrl"].ToString() + "' style='width:50px;height:50px'/> </td><td style='padding-right:20px;padding-top:20px;font-size:15px;font-family:Cambria'><b>" + dr["Name"].ToString() + "</b></td> ");
                    strb.Append("<td style='padding-right:20px;padding-top:20px;font-size:15px;font-family:Cambria'><b>" + dr["Qty"].ToString() + " " + dr["Unit"].ToString() + " </b></td>");
                    strb.Append("<td style='padding-right:20px;padding-top:20px;font-size:15px;font-family:Cambria'><b> Rs." + dr["Price"].ToString() +" </b></td>");

                }
                strb.Append("</table>");
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }



        return strb.ToString();

    }
}