﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Linq;
using System.Data;

public class CategoryBLL
{
    public DataSet GetForMenu(string Page)
    {
        return new CategoryDAL().GetForMenu(Page);
    }

    public string GetLevel2Options()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new CategoryDAL().GetByLevel(2);
            if (dr.HasRows)
            {
                strBuilder.Append("<option value='0'></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["CategoryId"].ToString(), dr["Title"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }
    public Int16 InsertUpdate(Category objCategories)
    {

        return new CategoryDAL().InsertUpdate(objCategories);
    }
    public List<Category> GetByLevel(int level)
    {
        List<Category> catList = new List<Category>();

        SqlDataReader dr = null;
        try
        {
            dr = new CategoryDAL().GetByLevel(level);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Category objCategories = new Category()
                    {
                        Title = dr["Title"].ToString(),
                        Description = dr["Description"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        CategoryId = Convert.ToInt16(dr["CategoryId"]),
                        AdminId = Convert.ToInt32(dr["AdminId"]),
                        ShowOnHome = Convert.ToBoolean(dr["ShowOnHome"]),
                        FeaturedOrder = Convert.ToInt16(dr["FeaturedOrder"]),
                        DisplayOrder = Convert.ToInt16(dr["DisplayOrder"]),
                        MetaTitle = dr["MetaTitle"].ToString(),
                        MetaKeyword = dr["MetaKeyword"].ToString(),
                        MetaDescription = dr["MetaDescription"].ToString(),
                        PhotoUrl = dr["PhotoUrl"].ToString(),
                        
                    };
                    catList.Add(objCategories);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return catList;

    }

    public void GetById(Category objCategory)
    {
         
        SqlDataReader dr = null;
        try
        {
            dr = new CategoryDAL().GetById(objCategory.CategoryId);
            if (dr.HasRows)
            {
                dr.Read();
                 
                   
                       objCategory.Title = dr["Title"].ToString();
                       objCategory.Description = dr["Description"].ToString();
                       objCategory.IsActive = Convert.ToBoolean(dr["IsActive"]);
                       objCategory.CategoryId = Convert.ToInt16(dr["CategoryId"]);
                       objCategory.AdminId = Convert.ToInt32(dr["AdminId"]);
                       objCategory.ShowOnHome = Convert.ToBoolean(dr["ShowOnHome"]);
                       objCategory.FeaturedOrder = Convert.ToInt16(dr["FeaturedOrder"]);
                       objCategory.DisplayOrder = Convert.ToInt16(dr["DisplayOrder"]);
                       objCategory.MetaTitle = dr["MetaTitle"].ToString();
                       objCategory.MetaKeyword = dr["MetaKeyword"].ToString();
                       objCategory.MetaDescription = dr["MetaDescription"].ToString();
                       objCategory.PhotoUrl = dr["PhotoUrl"].ToString();
                       
  
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
      

    }

    
    
    
    public DataSet GetAll()
    {
        return new CategoryDAL().GetAll();
    }


    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new CategoryDAL().GetByLevel (1);
            if (dr.HasRows)
            {
                strBuilder.Append("<option value='0'></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["CategoryId"].ToString(), dr["Title"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }
    public string GetOptions(int CatId)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new CategoryDAL().GetByParentId(CatId);
            if (dr.HasRows)
            {
                strBuilder.Append("<option value='0'></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["CategoryId"].ToString(), dr["Title"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }
    public Int16 ChangeOrder(string qry)
    {
        return new CategoryDAL().ChangeOrder(qry);
    }

    public List<Category> GetFeaturedCategories()
    {
        List<Category> catList = new List<Category>();

        SqlDataReader dr = null;
        try
        {
            dr = new CategoryDAL().GetByParentId(0);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["IsActive"]) && Convert.ToBoolean(dr["ShowOnHome"]))
                    {
                        Category objCategories = new Category()
                        {
                            Title = dr["Title"].ToString(),
                            Description = dr["Description"].ToString(),
                            IsActive = Convert.ToBoolean(dr["IsActive"]),
                            CategoryId = Convert.ToInt16(dr["CategoryId"]),
                            AdminId = Convert.ToInt32(dr["AdminId"]),
                            FeaturedOrder = Convert.ToInt16(dr["FeaturedOrder"]),
                            DisplayOrder = Convert.ToInt16(dr["DisplayOrder"]),
                            MetaTitle = dr["MetaTitle"].ToString(),
                            MetaKeyword = dr["MetaKeyword"].ToString(),
                            MetaDescription = dr["MetaDescription"].ToString(),
                             PhotoUrl = dr["PhotoUrl"].ToString(),
                           
                        };
                        catList.Add(objCategories);
                    }
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        catList = catList.OrderBy(o => o.FeaturedOrder).ToList();
        return catList;

    }




    public List<Category> GetForFrontEnd()
    {
        List<Category> catList = new List<Category>();

        SqlDataReader dr = null;
        try
        {
            dr = new CategoryDAL().GetByParentId(0);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["IsActive"]))
                    {
                        Category objCategories = new Category()
                        {
                            Title = dr["Title"].ToString(),
                            Description = dr["Description"].ToString(),
                            IsActive = Convert.ToBoolean(dr["IsActive"]),
                            CategoryId = Convert.ToInt16(dr["CategoryId"]),
                            AdminId = Convert.ToInt32(dr["AdminId"]),
                            FeaturedOrder = Convert.ToInt16(dr["FeaturedOrder"]),
                            DisplayOrder = Convert.ToInt16(dr["DisplayOrder"]),
                            MetaTitle = dr["MetaTitle"].ToString(),
                            MetaKeyword = dr["MetaKeyword"].ToString(),
                            MetaDescription = dr["MetaDescription"].ToString(),
                             PhotoUrl = dr["PhotoUrl"].ToString(),
                            
                        };
                        catList.Add(objCategories);
                    }
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        catList = catList.OrderBy(o => o.DisplayOrder).ToList();
        return catList;

    }



    public List<Category> GetByParentId(int ParentId)
    {
        List<Category> catList = new List<Category>();

        SqlDataReader dr = null;
        try
        {
            dr = new CategoryDAL().GetByParentId(ParentId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Category objCategories = new Category()
                    {
                        Title = dr["Title"].ToString(),
                        Description = dr["Description"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        CategoryId = Convert.ToInt16(dr["CategoryId"]),
                        AdminId = Convert.ToInt32(dr["AdminId"]),
                        FeaturedOrder = Convert.ToInt16(dr["FeaturedOrder"]),
                        DisplayOrder = Convert.ToInt16(dr["DisplayOrder"]),
                        MetaTitle = dr["MetaTitle"].ToString(),
                        MetaKeyword = dr["MetaKeyword"].ToString(),
                        MetaDescription = dr["MetaDescription"].ToString(),
                         PhotoUrl = dr["PhotoUrl"].ToString(),
                        
                    };
                    catList.Add(objCategories);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }


        catList = catList.OrderBy(o => o.DisplayOrder).ToList();
        return catList;

    }


}