﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class UserComboDAL
{
    public DataSet GetHtmlComboByUserId(Int64 UserId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@CustomerId", UserId);
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_TempUserComboGetByUserId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public Int16 DeleteTemp(Int64 UserId)
    {
        Int16 retVal = 0;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@CustomerId", UserId );
        ObjParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        ObjParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_TempUserComboDeleteByUserID", ObjParam);
            retVal =Convert.ToInt16( ObjParam[1].Value);
        }
        finally
        {
            ObjParam = null;
        }
        return retVal;
    }
    public Int16 DeleteCombo(Int64 ComboId)
    {
        Int16 retVal = 0;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@ComboId", ComboId);
        ObjParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        ObjParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_UserComboDeleteByComboID", ObjParam);
            retVal = Convert.ToInt16(ObjParam[1].Value);
        }
        finally
        {
            ObjParam = null;
        }
        return retVal;
    }
    public void   GetProductDetailByID(UserComboDetail objcom)
    {
        string  retVal = "";
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@ComboId", objcom.ComboId);
        ObjParam[1] = new SqlParameter("@CustomerId", objcom.CustomerId);
        ObjParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        ObjParam[2].Direction = ParameterDirection.ReturnValue;
        try
        {
             SqlHelper.ExecuteNonQuery (ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_UserComboDetailGetByID", ObjParam);
             retVal = ObjParam[2].Value.ToString();
        }
        finally
        {
            ObjParam = null;
        }
 
    }
    public Int16 InsertUpdate(UserCombo  objcombo)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];

        objParam[0] = new SqlParameter("@ComboId", objcombo.ComboId);
        objParam[1] = new SqlParameter("@CustomerId", objcombo.CustomerId);
        objParam[2] = new SqlParameter("@Title", objcombo.Title);
        objParam[3] = new SqlParameter("@IsActive", objcombo.IsActive);
        objParam[4] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[4].Direction = ParameterDirection.ReturnValue;
        objParam[5] = new SqlParameter("@PhotoUrl", objcombo.PhotoUrl);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_UserComboInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[4].Value);
            objcombo.ComboId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
    public Int16 InsertTemp(UserComboDetail objcombo)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@CustomerId", objcombo.CustomerId);
        objParam[1] = new SqlParameter("@ProductId", objcombo.ProductId);
        objParam[2] = new SqlParameter("@Qty", objcombo.Qty);
        objParam[3] = new SqlParameter("@VariationId", objcombo.VariationId);
        objParam[4] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[4].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_TempUserComboDetailInsert", objParam);
            retValue = Convert.ToInt16(objParam[4].Value);
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
    public SqlDataReader GetAllByUserId(Int32 UserId)
    {
        UserId = 2;
        SqlParameter[] objParam = new SqlParameter[1];
           objParam[0] = new SqlParameter("@CustomerId",UserId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_UserComboGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public SqlDataReader UserComboIncrDecr(UserComboDetail objUserCombo, string Mode)
    {
        SqlParameter[] objParam = new SqlParameter[5];
        objParam[0] = new SqlParameter("@CustomerId", objUserCombo.CustomerId);
        objParam[1] = new SqlParameter("@ProductId", objUserCombo.ProductId);
        objParam[2] = new SqlParameter("@VariationId", objUserCombo.VariationId);
        objParam[3] = new SqlParameter("@Qty", objUserCombo.Qty);
        objParam[4] = new SqlParameter("@Mode", Mode);


        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_UserComboIncrDecr", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
}