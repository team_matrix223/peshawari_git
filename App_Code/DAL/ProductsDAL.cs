﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for ProductsDAL
/// </summary>
public class ProductsDAL
{

    public DataSet QuickListKeywordSearch(string Keyword)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Keyword", Keyword);

        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsKeywordSearchQuickList", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

    public SqlDataReader GetComboProductsHtmlByProductId(int ProductId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@ProductId", ProductId);

        SqlDataReader ds = null;
        try
        {
            ds = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "GetComboProductsHtmlByProductId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public SqlDataReader GetComboProductsFrontEndHtml(string SessionId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@SessionId", SessionId);

        SqlDataReader ds = null;
        try
        {
            ds = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GetFeaturedComboProductsBySessionId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public int UpdateVariationDetail(string ItemCode, decimal Price, decimal Mrp, string VariationId)
    {
        int retval = 0;
        SqlParameter[] objparam = new SqlParameter[5];
        SqlDataReader dr = null;
        objparam[0] = new SqlParameter("@ItemCode", ItemCode);
        objparam[1] = new SqlParameter("@Price", Price);
        objparam[2] = new SqlParameter("@Mrp", Mrp);
        objparam[3] = new SqlParameter("@RetVal", SqlDbType.Int, 4);
        objparam[3].Direction = ParameterDirection.ReturnValue;
        objparam[4] = new SqlParameter("@VariationId", VariationId);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "UpdateVariationDetail", objparam);
            retval = Convert.ToInt32(objparam[3].Value);

        }

        finally
        {
            objparam = null;
        }
        return retval;
    }
    public SqlDataReader GetProductDetail(string ItemCode, int VariationId)
    {

        SqlParameter[] objparam = new SqlParameter[2];
        SqlDataReader dr = null;
        objparam[0] = new SqlParameter("@ItemCode", ItemCode);

        objparam[1] = new SqlParameter("@VariationId", VariationId);

        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "GetProductDetailByItemCode", objparam);


        }

        finally
        {
            objparam = null;
        }
        return dr;
    }
    public SqlDataReader GetProductPaging(int PageNumber, int PageSize, int Cat2)
    {

        SqlParameter[] objparam = new SqlParameter[3];
        SqlDataReader dr = null;
        objparam[0] = new SqlParameter("@PageNumber", PageNumber);
        objparam[1] = new SqlParameter("@PageSize", PageSize);
        objparam[2] = new SqlParameter("@Cat2", Cat2);


        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "Paging", objparam);


        }

        finally
        {
            objparam = null;
        }
        return dr;
    }

    public DataSet ComparePriceDifferences()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsWithPriceDifference", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public Int16 UpdatePrice(DataTable VariationIdsType)
    {
        Int16 RetVal = 0;
        SqlParameter[] objparam = new SqlParameter[3];
        objparam[0] = new SqlParameter("@VariationIdsType", VariationIdsType);
        objparam[1] = new SqlParameter("@RetVal", SqlDbType.Int, 4);
        objparam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            //SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_ProductsUpdatePrice", objparam);
            //RetVal = Convert.ToInt16(objparam[1].Value);
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "productspricedev", objparam);
            RetVal = Convert.ToInt16(objparam[1].Value);

        }
        finally
        {
            objparam = null;
        }
        return RetVal;
    }
    public DataSet GetByStatus()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ActiveVariationsGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public Int16 UpdateStatus(DataTable ProductIdsType)
    {
        Int16 RetVal = 0;
        SqlParameter[] objparam = new SqlParameter[3];
        objparam[0] = new SqlParameter("@ProductIdsType", ProductIdsType);
        objparam[1] = new SqlParameter("@RetVal", SqlDbType.Int, 4);
        objparam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_ProductsUpdateStatus", objparam);
            RetVal = Convert.ToInt16(objparam[1].Value);

        }
        finally
        {
            objparam = null;
        }
        return RetVal;
    }
    public DataSet AdvancedSearchDateFilter(string SessionId, string Brands, Int64 CategoryLevel1, Int64 CategoryLevel2, Int64 CategoryLevel3, out int TotalRecords, int PageId, int MinPrice, int MaxPrice)
    {
        SqlParameter[] objParam = new SqlParameter[10];
        objParam[0] = new SqlParameter("@SessionId", SessionId);
        objParam[1] = new SqlParameter("@Brands", Brands);
        objParam[2] = new SqlParameter("@CategoryLevel1", CategoryLevel1);
        objParam[3] = new SqlParameter("@CategoryLevel2", CategoryLevel2);
        objParam[4] = new SqlParameter("@CategoryLevel3", CategoryLevel3);
        objParam[5] = new SqlParameter("@PageNumber", PageId);
        objParam[6] = new SqlParameter("@PageSize", 16);

        objParam[7] = new SqlParameter("@TotalRows", SqlDbType.Int, 4);
        objParam[7].Direction = ParameterDirection.Output;
        objParam[8] = new SqlParameter("@MinPrice", MinPrice);
        objParam[9] = new SqlParameter("@MaxPrice", MaxPrice);

        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_AdvancedSearchDateFilter", objParam);

            TotalRecords = Convert.ToInt32(objParam[7].Value);

        }

        finally
        {
            objParam = null;
        }
        return ds;


    }

    public DataSet KeywordSearch(string Keyword)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Keyword", Keyword);

        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsKeywordSearch", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

    public Int16 DeleteProductById(Int64 Id)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Id", Id);
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_DeleteProductById", objParam);
            retValue = Convert.ToInt16(objParam[1].Value);
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
    public Int16 DeletePhotoById(Int64 Id, string Mode)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@Id", Id);
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        objParam[2] = new SqlParameter("@Mode", Mode);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_DeletePhotoById", objParam);
            retValue = Convert.ToInt16(objParam[1].Value);
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

 public SqlDataReader GetBySubCategoryId2(int SubCategoryId, Boolean IsActive)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@SubCategoryId", SubCategoryId);
        objParam[1] = new SqlParameter("@IsActive", IsActive);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsGetBySubCategory2", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


 public SqlDataReader GetByCategoryLevel1forgroups(int CategoryId)
 {

     SqlParameter[] objParam = new SqlParameter[1];
     objParam[0] = new SqlParameter("@CategoryId", CategoryId);

     SqlDataReader dr = null;
     try
     {
         dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
         "shopping_sp_ProductsGetByCategoryLevel1forgroups", objParam);


     }

     finally
     {
         objParam = null;
     }
     return dr;

 }


 public SqlDataReader GetByCategoryLevel1(int CategoryId,Boolean IsActive)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@CategoryId", CategoryId);
        objParam[1] = new SqlParameter("@IsActive", IsActive);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsGetByCategoryLevel1", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

 public SqlDataReader GetByCategoryLevel3forProducts(int CategoryLevel3,Boolean IsActive)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@CategoryLevel3", CategoryLevel3);
        objParam[1] = new SqlParameter("@IsActive", IsActive);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsGetByCategoryLevel3forproducts", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public DataSet GetProductsWithImagesByName(string ProductName)
    {
        ProductName = "%" + ProductName + "%";
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Name", ProductName);
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsWithImagesGetByName", objParam);
        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
   
    public SqlDataReader GetProductDetailByProductId(Products objProduct)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@ProductId", objProduct.ProductId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsGetByProductId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public DataSet GetAllVariations()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_VariationsGetByAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public SqlDataReader GetProductDetailByProductIdZoom(string SessionId, int ProductId, int VariationId)
    {

        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@ProductId", ProductId);
        objParam[1] = new SqlParameter("@VariationId", VariationId);
        objParam[2] = new SqlParameter("@SessionId", SessionId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsViewDetailByProductId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public DataSet AdvancedSearch(string SessionId, string Brands, Int64 CategoryLevel1, Int64 CategoryLevel2, Int64 CategoryLevel3, out int TotalRecords, int PageId, int MinPrice, int MaxPrice, Int64 BrandId, Int64 GroupId, Int64 SubGroupId,int PageSize=16)
    {
        SqlParameter[] objParam = new SqlParameter[13];
        objParam[0] = new SqlParameter("@SessionId", SessionId);
        objParam[1] = new SqlParameter("@Brands", Brands);
        objParam[2] = new SqlParameter("@CategoryLevel1", CategoryLevel1);
        objParam[3] = new SqlParameter("@CategoryLevel2", CategoryLevel2);
        objParam[4] = new SqlParameter("@CategoryLevel3", CategoryLevel3);
        objParam[5] = new SqlParameter("@PageNumber", PageId);
        objParam[6] = new SqlParameter("@PageSize", PageSize);

        objParam[7] = new SqlParameter("@TotalRows", SqlDbType.Int, 4);
        objParam[7].Direction = ParameterDirection.Output;
        objParam[8] = new SqlParameter("@MinPrice", MinPrice);
        objParam[9] = new SqlParameter("@MaxPrice", MaxPrice);

        objParam[10] = new SqlParameter("@BrandId", BrandId);
        objParam[11] = new SqlParameter("@GroupId", GroupId);
        objParam[12] = new SqlParameter("@SubGroupId", SubGroupId);


        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_AdvancedSearch", objParam);

            TotalRecords = Convert.ToInt32(objParam[7].Value);

        }

        finally
        {
            objParam = null;
        }
        return ds;


    }


    public DataSet GetFeaturedComboTypesAndCombos(string SessionId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@SessionId", SessionId);

        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GetFeaturedComboTypesAndCombos", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public DataSet GetFeaturedCategoriesAndProducts(string SessionId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@SessionId", SessionId);

        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GetFeaturedCategoriesAndProducts", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
      public DataSet GetSchemeProducts(string SessionId,Int64 SchemeId)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@SessionId", SessionId);
        objParam[1] = new SqlParameter("@SchemeId", SchemeId);


        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GetSchemeProducts", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    


    public SqlDataReader GetProductsByFeaturedCategory(int CategoryId)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@CategoryId", CategoryId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsGetByFeaturedCategory", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }



    public DataSet GetByCategoryId(int CategoryId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@CategoryId", CategoryId);
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsGetByCategoryId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }



    public SqlDataReader ProductGetByCatIdSubCatId(Products objp)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@CatId", objp.CategoryId);
        objParam[1] = new SqlParameter("@SubCatId", objp.SubCategoryId);
        objParam[2] = new SqlParameter("@SubSubCatId", objp.CategoryLevel3);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductGetByCatIdSubCatId", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }


    public SqlDataReader GetBySubCategoryId(int SubCategoryId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@SubCategoryId", SubCategoryId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsGetBySubCategory", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetByCategoryLevel3(int CategoryLevel3)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@CategoryLevel3", CategoryLevel3);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsGetByCategoryLevel3", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetProductsSearchByName(int CategoryLevel1, int CategoryLevel2, int CategoryLevel3, string Name)
    {
        Name = "%" + Name + "%";
        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@CategoryLevel1", CategoryLevel1);
        objParam[1] = new SqlParameter("@CategoryLevel2", CategoryLevel2);
        objParam[2] = new SqlParameter("@CategoryLevel3", CategoryLevel3);
        objParam[3] = new SqlParameter("@Name", Name );
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsGetByName", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public SqlDataReader GetByProductId(int SubCategoryId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@ProductId", SubCategoryId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsGetByProductId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetAllComboProductsHtml()
    {

        SqlParameter[] objParam = new SqlParameter[0];


        SqlDataReader ds = null;
        try
        {
            ds = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "GetAllComboProducts", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public SqlDataReader GetAllComboProducts()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GetComboProducts", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }



    public DataSet GetAll(Int32 CategoryId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@CategoryId", CategoryId);
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

    public Int16 InsertUpdate(Products objProduct, DataTable dt)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[13];

        objParam[0] = new SqlParameter("@ProductId", objProduct.ProductId);
        objParam[1] = new SqlParameter("@SubCategoryId", objProduct.SubCategoryId);
        objParam[2] = new SqlParameter("@CategoryId", objProduct.CategoryId);
        objParam[3] = new SqlParameter("@Name", objProduct.Name);
        objParam[4] = new SqlParameter("@ShortName", objProduct.ShortName);
        objParam[5] = new SqlParameter("@Description", objProduct.Description);
        objParam[6] = new SqlParameter("@IsActive", objProduct.IsActive);
        objParam[7] = new SqlParameter("@AdminId", objProduct.AdminId);
        objParam[8] = new SqlParameter("@CategoryLevel3", objProduct.CategoryLevel3);
        objParam[9] = new SqlParameter("@ShortDescription", objProduct.ShortDescription);
        objParam[10] = new SqlParameter("@Brand", objProduct.BrandId);
        objParam[11] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[11].Direction = ParameterDirection.ReturnValue;
        objParam[12] = new SqlParameter("@ProductDetail", dt);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_ProductsInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[11].Value);
            objProduct.CategoryId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public Int16 ComboProductsInsertUpdate(Products objProduct)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[20];

        objParam[0] = new SqlParameter("@ProductId", objProduct.ProductId);
        objParam[1] = new SqlParameter("@CategoryId", objProduct.CategoryId);
        objParam[2] = new SqlParameter("@SubCategoryId", objProduct.SubCategoryId);
        objParam[3] = new SqlParameter("@CategoryLevel3", objProduct.CategoryLevel3);
        objParam[4] = new SqlParameter("@Name", objProduct.Name);
        objParam[5] = new SqlParameter("@ShortName", objProduct.ShortName);
        objParam[6] = new SqlParameter("@Description", objProduct.Description);
        objParam[7] = new SqlParameter("@IsActive", objProduct.IsActive);
        objParam[8] = new SqlParameter("@AdminId", objProduct.AdminId);

        objParam[9] = new SqlParameter("@ShortDescription", objProduct.ShortDescription);
        objParam[10] = new SqlParameter("@Brand", objProduct.BrandId);
        objParam[11] = new SqlParameter("@Unit", objProduct.Unit);
        objParam[12] = new SqlParameter("@Qty", objProduct.Qty);
        objParam[13] = new SqlParameter("@Price", objProduct.Price);
        objParam[14] = new SqlParameter("@Type", objProduct.Type);
        objParam[15] = new SqlParameter("@MRP", objProduct.MRP);
        objParam[16] = new SqlParameter("@desc", objProduct.descr);
        objParam[17] = new SqlParameter("@ItemCode", objProduct.ItemCode);
        objParam[18] = new SqlParameter("@Url", objProduct.PhotoUrl);
        objParam[19] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[19].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_ComboProductsInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[19].Value);
            objProduct.ProductId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public DataSet GetVariationsProductsByProductId(int ProductId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@ProductId", ProductId);
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_VariationsProductsGetByProductId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public DataSet GetProductsWithImages()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsWithImagesGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public Int16 InsertVariations(Int32 ProductId, Int64 VariationId, string PhotoUrl)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[4];

        objParam[0] = new SqlParameter("@ProductId", ProductId);
        objParam[1] = new SqlParameter("@VariationId", VariationId);
        objParam[2] = new SqlParameter("@PhotoUrl", PhotoUrl);
        objParam[3] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[3].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_VariationsUpdatePhotoUrl", objParam);
            retValue = Convert.ToInt16(objParam[3].Value);
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public Int16 UpdateProductList(Variations objVariation)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[11];

        objParam[0] = new SqlParameter("@Name", objVariation.Name);
        objParam[1] = new SqlParameter("@VariationId", objVariation.VariationId);
        objParam[2] = new SqlParameter("@Code", objVariation.ItemCode);
        objParam[3] = new SqlParameter("@Brand", objVariation.Brand);
        objParam[4] = new SqlParameter("@Unit", objVariation.Unit);
        objParam[5] = new SqlParameter("@Qty", objVariation.Qty);
        objParam[6] = new SqlParameter("@Price", objVariation.Price);
        objParam[7] = new SqlParameter("@Mrp", objVariation.Mrp);
        objParam[8] = new SqlParameter("@Type", objVariation.type);
        objParam[9] = new SqlParameter("@Active", objVariation.IsActive);

        objParam[10] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[10].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_UpdateProductList", objParam);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public SqlDataReader GetComboProductsForIndex()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GetAllComboproducts", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }
}