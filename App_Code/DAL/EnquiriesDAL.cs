﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for EnquiriesDAL
/// </summary>
public class EnquiriesDAL
{
    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_EnquiryGetAll", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }

    public int InsertUpdate(Enquiries objEnquiries)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];
        objParam[0] = new SqlParameter("@PersonName", objEnquiries.PersonName);
        objParam[1] = new SqlParameter("@EmailId", objEnquiries.EmailId);
        objParam[2] = new SqlParameter("@Enquiry", objEnquiries.Enquiry);

        objParam[3] = new SqlParameter("@MobileNo", objEnquiries.MobileNo);
        objParam[4] = new SqlParameter("@DOC", objEnquiries.DOC);
        objParam[5] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[5].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_EnquiryInsert", objParam);
            retValue = Convert.ToInt32(objParam[5].Value);
            objEnquiries.EnquiryId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }

    public SqlDataReader GetByName(Enquiries objEnquiries)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@PersonName", objEnquiries.PersonName);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsGetByCategoryId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

}