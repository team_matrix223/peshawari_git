﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for QuickListDAL
/// </summary>
public class QuickListDAL
{
    public Int32 InsertQuickList(QuickList objQuickList ,DataTable dt, string OrderType)
    {

        Int32 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[7];

        objParam[0] = new SqlParameter("@UserId", objQuickList.UserId);
        objParam[1] = new SqlParameter("@DeliveryAddressId", objQuickList.DeliveryAddressId);
        objParam[2] = new SqlParameter("@DeliverySlot", objQuickList.DeliverySlot);
        objParam[3] = new SqlParameter("@DeliveryDate", objQuickList.DeliveryDate);
        objParam[6] = new SqlParameter("@OrderType", OrderType);
        objParam[4] = new SqlParameter("@QuickList", dt);
        objParam[5] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[5].Direction = ParameterDirection.ReturnValue;


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_QuickListInsert", objParam);
            retValue = Convert.ToInt32(objParam[5].Value);
            objQuickList.ListId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_QuickListGetByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public SqlDataReader GetListProducts(Int64 ListId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@ListId", ListId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ListDetailGetAll", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int32 InsertUpdate(Order objOrder, DataTable dt)
    {

        Int32 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[9];
        objParam[0] = new SqlParameter("@ListId", objOrder.ListId);
        objParam[1] = new SqlParameter("@CustomerId", objOrder.CustomerId);
        objParam[2] = new SqlParameter("@OrderDate", objOrder.OrderDate);
        objParam[3] = new SqlParameter("@BillAmount", objOrder.BillValue);
        objParam[4] = new SqlParameter("@DeliveryAddressId", objOrder.DeliveryAddressId);
        objParam[5] = new SqlParameter("@DeliveryDate", objOrder.DeliveryDate);
        objParam[6] = new SqlParameter("@DeliverySlot", objOrder.DeliverySlot);
        objParam[7] = new SqlParameter("@ProductId", dt);
        objParam[8] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[8].Direction = ParameterDirection.ReturnValue;
       


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_QuickOrderInsert", objParam);
            retValue = Convert.ToInt32(objParam[8].Value);
            objOrder.OrderId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

}