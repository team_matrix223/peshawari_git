﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for AdminDAL
/// </summary>
public class RecipeDAL
{




    public SqlDataReader GetRecipeIngredients(int RecipeId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@RecipeId", RecipeId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_RecipesIngredientsGetById", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }


    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_RecipesGetAll", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }

    public SqlDataReader GetProducts()
    {
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_RecipesGetProducts", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }





    public SqlDataReader GetById(int RecipeId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@RecipeId", RecipeId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_RecipesGetById", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }
    public Int16 InsertUpdate(Recipe objRecipe)
    {


        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[7];

        objParam[0] = new SqlParameter("@RecipeId", objRecipe.RecipeId);
        objParam[1] = new SqlParameter("@ShortDescription", objRecipe.ShortDescription);
        objParam[2] = new SqlParameter("@Process", objRecipe.Process);
        objParam[3] = new SqlParameter("@IsActive", objRecipe.IsActive);

        objParam[4] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[4].Direction = ParameterDirection.ReturnValue;
        objParam[5] = new SqlParameter("@Title", objRecipe.Title);
        objParam[6] = new SqlParameter("@ImageUrl", objRecipe.ImageUrl);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_RecipesInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[4].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public Int16 InsertRecipeIngredients(int RecipeId, DataTable dt)
    {


        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@RecipeId", RecipeId);
        objParam[1] = new SqlParameter("@dtIngredients", dt);

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_RecipeIngredientsInsert", objParam);


        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }



}