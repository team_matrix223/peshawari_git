﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for BrandDAL
/// </summary>
public class BrandDAL
{

    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_BrandGetAll", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }

    public int InsertUpdate(Brand objBrand)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];
        objParam[0] = new SqlParameter("@Title", objBrand.Title);
        objParam[1] = new SqlParameter("@IsActive", objBrand.IsActive);
        objParam[2] = new SqlParameter("@AdminId", objBrand.AdminId);
        objParam[3] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[3].Direction = ParameterDirection.ReturnValue;
        objParam[4] = new SqlParameter("@BrandId ", objBrand.BrandId);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_BrandInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[3].Value);
            objBrand.BrandId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }

}