﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Text;
public class UsersDAL
{
    public SqlDataReader GetById(int userid)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@userid", userid );
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_UserGetByUserId", objParam);
        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetUserByMobileNo(Users objUsers)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@MobileNo", objUsers.MobileNo);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_UsersGetByMobileNo", objParam);
        }




        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GetAllUsers", objParam);
        }




        finally
        {
            objParam = null;
        }
        return dr;

    }

    public static string HashSHA1(string value)
    {
        var sha1 = System.Security.Cryptography.SHA1.Create();
        var inputBytes = Encoding.ASCII.GetBytes(value);
        var hash = sha1.ComputeHash(inputBytes);

        var sb = new StringBuilder();
        for (var i = 0; i < hash.Length; i++)
        {
            sb.Append(hash[i].ToString("X2"));
        }
        return sb.ToString();
    }
    public void ResetPassword(string UserName, string Password)
    {
        string retVal = "0";
        SqlParameter[] objParam = new SqlParameter[2];

        //objParam[0] = new SqlParameter("@Password", HashSHA1(Password));
        objParam[0] = new SqlParameter("@Password", Password);
        objParam[1] = new SqlParameter("@emailid", UserName);

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
                  "shopping_sp_ResetPasswordByEmail", objParam);


        }
        finally
        {
            objParam = null;
        }



    }
    public string userLoginCheck(Users objUser,string SessionId)
    {
        string retVal = "0";

        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@MobileNo", objUser.MobileNo);
        //objParam[1] = new SqlParameter("@Password", HashSHA1(objReg.Password));
        objParam[1] = new SqlParameter("@Password", objUser.Password);
        objParam[2] = new SqlParameter("@retval", SqlDbType.Int,4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@SessionId", SessionId);
        
     
        try
        {
             SqlHelper.ExecuteNonQuery (ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_UserLoginCheck", objParam);
             retVal = Convert.ToString ( objParam[2].Value);

        }

        finally
        {
            objParam = null;
        }


        return retVal ;
    }
    public string ChangePassword(Users objUser)
    {
        string retVal = "0";
        SqlParameter[] objParam = new SqlParameter[2];

        //objParam[0] = new SqlParameter("@Password", HashSHA1(objReg.Password));
        objParam[0] = new SqlParameter("@Password", objUser.Password);
        objParam[1] = new SqlParameter("@userid", objUser.UserId);

        try
        {
            retVal = SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure,
                 "shopping_sp_UserChangePassword", objParam).ToString();


        }
        finally
        {
            objParam = null;
        }

        return retVal;

    }
    public Int16 InsertUpdate(Users objUser)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[25];

        objParam[0] = new SqlParameter("@UserId", objUser.UserId);
        objParam[1] = new SqlParameter("@FirstName", objUser.FirstName);
        objParam[2] = new SqlParameter("@LastName", objUser.LastName);
        objParam[3] = new SqlParameter("@EmailId", objUser.EmailId);
        objParam[4] = new SqlParameter("@Password", objUser.Password);
        objParam[5] = new SqlParameter("@IsActive", objUser.IsActive);
        objParam[6] = new SqlParameter("@AdminId", objUser.AdminId);
        objParam[7] = new SqlParameter("@RecipientFirstName", objUser.RecipientFirstName);
        objParam[8] = new SqlParameter("@RecipientLastName", objUser.RecipientLastName);
        objParam[9] = new SqlParameter("@RMobileNo", objUser.RMobileNo);
        objParam[10] = new SqlParameter("@Telephone", objUser.Telephone);
        objParam[11] = new SqlParameter("@CityId", objUser.CityId);
        objParam[12] = new SqlParameter("@Area", objUser.Area);
        objParam[13] = new SqlParameter("@Street", objUser.Street);
        objParam[14] = new SqlParameter("@Address", objUser.Address);
        objParam[15] = new SqlParameter("@PinCode", objUser.PinCode);
        objParam[16] = new SqlParameter("@IsPrimary", objUser.IsPrimary);
        objParam[17] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[17].Direction = ParameterDirection.ReturnValue;
        objParam[18] = new SqlParameter("@MobileNo", objUser.MobileNo);
        objParam[19] = new SqlParameter("@DeliveryAddressId", objUser.DeliveryAddressId);
        objParam[20] = new SqlParameter("@PinCodeId", objUser.PinCodeId);
        objParam[21] = new SqlParameter("@DOA", objUser.DOA);
        objParam[22] = new SqlParameter("@DOB", objUser.DOB);
        objParam[23] = new SqlParameter("@Code", objUser.Code);
        objParam[24] = new SqlParameter("@ReferralCode", objUser.ReferralCode);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_RegisterUsersInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[17].Value);
            objUser.UserId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


 
    public Int16 Updatenewlettersubscription(Users objUser)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[3];

        objParam[0] = new SqlParameter("@NewsLetter", objUser.NewsLetter);
        objParam[1] = new SqlParameter("@UserId", objUser.UserId);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_UpdateNewLetterSubscription", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objUser.UserId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public Int16 UpdateUserActive(Users objUser)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[3];

        objParam[0] = new SqlParameter("@Active", objUser.IsActive);
        objParam[1] = new SqlParameter("@UserId", objUser.UserId);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_UpdateuserActive", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objUser.UserId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}
