﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class UserCartMstDAL
{



    public SqlDataReader GetBySessionId(string SessionId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@SessionId", SessionId);
       SqlDataReader dr= null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_UserCartMstGetBySessionId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }




    public void UserCartInsertUpdate(UserCartMst objUserCartMst)
    {
        SqlParameter[] objParam = new SqlParameter[6];


        objParam[0] = new SqlParameter("@SessionId", objUserCartMst.SessionId);
        objParam[1] = new SqlParameter("@DeliveryDate", objUserCartMst.DeliveryDate);
        objParam[2] = new SqlParameter("@DeliverySlot", objUserCartMst.DeliverySlot);
        objParam[3] = new SqlParameter("@PaymentMode", objUserCartMst.PaymentMode);
        objParam[4] = new SqlParameter("@DeliveryAddressId", objUserCartMst.DeliveryAddressId);
        objParam[5] = new SqlParameter("@UserId", objUserCartMst.UserId);
          
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_UserCartMstInsertUpdate", objParam);


        }

        finally
        {
            objParam = null;
        }
        
    }




}