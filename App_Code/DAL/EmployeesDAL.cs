﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for EmployeesDAL
/// </summary>
public class EmployeesDAL
{



    public List<Employees> GetAll()
    {
        List<Employees> UsersList = new List<Employees>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_EmployeesGetAll", objParam);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Employees objUsers = new Employees()
                    {
                        Emp_Id = Convert.ToInt32(dr["Emp_Id"]),
                        Prefix = Convert.ToString(dr["Prefix"]),
                        Emp_Name = Convert.ToString(dr["Emp_Name"]),
                        Address = Convert.ToString(dr["Address"]),
                        ContactNo = Convert.ToString(dr["ContactNo"]),
                        Email = Convert.ToString(dr["Email"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        UserName = Convert.ToString(dr["UserName"]),


                    };
                    UsersList.Add(objUsers);
                }
            }

        }

        finally
        {
            objParam = null;
        }
        return UsersList;

    }
    
    
    public static string HashSHA1(string value)
    {
        var sha1 = System.Security.Cryptography.SHA1.Create();
        var inputBytes = Encoding.ASCII.GetBytes(value);
        var hash = sha1.ComputeHash(inputBytes);

        var sb = new StringBuilder();
        for (var i = 0; i < hash.Length; i++)
        {
            sb.Append(hash[i].ToString("X2"));
        }
        return sb.ToString();
    }

    public Int32 InsertUpdate(Employees objEmp)
    {

        Int32 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[10];

        objParam[0] = new SqlParameter("@Emp_Id", objEmp.Emp_Id);
        objParam[1] = new SqlParameter("@Prefix", objEmp.Prefix);
        objParam[2] = new SqlParameter("@Emp_Name", objEmp.Emp_Name);
        objParam[3] = new SqlParameter("@Address", objEmp.Address);
        objParam[4] = new SqlParameter("@ContactNo", objEmp.ContactNo);
        objParam[5] = new SqlParameter("@Email", objEmp.Email);
        objParam[6] = new SqlParameter("@IsActive", objEmp.IsActive);
        objParam[7] = new SqlParameter("@UserName", objEmp.UserName);
        objParam[8] = new SqlParameter("@Password", HashSHA1(objEmp.Password));

        objParam[9] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[9].Direction = ParameterDirection.ReturnValue;


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_EmployeeInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[9].Value);
            objEmp.Emp_Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }



    public Int32 InsertEmpAssigned(Empassigned objEmp,DataTable dt)
    {

        Int32 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[3];

        objParam[0] = new SqlParameter("@Emp_Id", objEmp.Emp_Id);
        objParam[1] = new SqlParameter("@BillNo", dt);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_EmpAssignInsert", objParam);
            retValue = Convert.ToInt32(objParam[2].Value);
            objEmp.Emp_Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public Int32 InsertReceivedPayment(Empassigned objEmp, DataTable dt)
    {

        Int32 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[3];

        objParam[0] = new SqlParameter("@EmpId", objEmp.Emp_Id);
        objParam[1] = new SqlParameter("@BillNo", dt);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_ReceivedPaymentInsert", objParam);
            retValue = Convert.ToInt32(objParam[2].Value);
            objEmp.Emp_Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }



    public Int32 InsertDealloation(Empassigned objEmp, DataTable dt)
    {

        Int32 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[3];

        objParam[0] = new SqlParameter("@Emp_Id", objEmp.Emp_Id);
        objParam[1] = new SqlParameter("@BillNo", dt);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_DeallocateordersUpdate", objParam);
            retValue = Convert.ToInt32(objParam[2].Value);
            objEmp.Emp_Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_BillsGetByDateForEmpAssigned", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public SqlDataReader GetByDateAllocatedOrders(int EmpId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];

        ObjParam[0] = new SqlParameter("@EmpId", EmpId);
        
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_EmployeeAllocatedOrders", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }

    public SqlDataReader GetByEmpOrders(Int32 EmpId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@EmpId", EmpId);

        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_ShowEmpOrders", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public Int32 UpdateBillMaster(DataTable dt)
    {

        Int32 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@BillNoType", dt);
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_UpdateBillMaster", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
          
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}