﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class SettingsDAL
{
    public Int16 InsertUpdate(Settings objsett)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[9];

        objParam[0] = new SqlParameter("@DeliveryCharge", objsett.DeliveryCharge);
        objParam[1] = new SqlParameter("@MinimumCheckOutAmt", objsett.MinimumCheckOutAmt);
        objParam[2] = new SqlParameter("@FreeDeliveryAmt", objsett.FreeDeliveryAmt);
        objParam[3] = new SqlParameter("@PointRate", objsett.PointRate);
        objParam[4] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[4].Direction = ParameterDirection.ReturnValue;
        objParam[5] = new SqlParameter("@ReferralPoint", objsett.ReferralPoint);
        objParam[6] = new SqlParameter("@RefereePoint", objsett.RefereePoint);
        objParam[7] = new SqlParameter("@MiniReimbursementPoint", objsett.MiniReimbursementPoint);
        objParam[8] = new SqlParameter("@MiniReimbursementCash", objsett.MiniReimbursementCash);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_SettingsInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[4].Value);

        }
        finally
        {
            objParam = null;
           
        }
        return retValue;
    }

    public Settings GetSett()
    {
        Settings objlist = new Settings();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_SettingsGetAll", objParam);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                   
                        objlist.DeliveryCharge = Convert.ToDecimal(dr["DeliveryCharge"].ToString());
                       objlist. MinimumCheckOutAmt = Convert.ToDecimal(dr["MinimumCheckOutAmt"].ToString());
                       objlist.FreeDeliveryAmt = Convert.ToDecimal(dr["FreeDeliveryAmt"].ToString());
                       objlist.PointRate = Convert.ToInt32(dr["PointRate"].ToString());
                       objlist.ReferralPoint = Convert.ToInt32(dr["ReferralPoint"].ToString());
                       objlist.RefereePoint = Convert.ToInt32(dr["RefereePoint"].ToString());
                       objlist.MiniReimbursementPoint = Convert.ToInt32(dr["MiniReimbursementPoint"].ToString());
                       objlist.MiniReimbursementCash = Convert.ToInt32(dr["MiniReimbursementCash"].ToString());
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        dr.Close();
        dr.Dispose();
        return objlist;
    }
    public List< Settings> GetSettings()
    {
        List<Settings> objlist = new List<Settings>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_SettingsGetAll", objParam);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Settings objsett=new Settings()
                {
                    DeliveryCharge = Convert.ToDecimal(dr["DeliveryCharge"].ToString()),
                    MinimumCheckOutAmt = Convert.ToDecimal(dr["MinimumCheckOutAmt"].ToString()),
                    FreeDeliveryAmt = Convert.ToDecimal(dr["FreeDeliveryAmt"].ToString()),
                    PointRate = Convert.ToInt32(dr["PointRate"].ToString()),
                    ReferralPoint = Convert.ToInt32(dr["ReferralPoint"].ToString()),
                    RefereePoint = Convert.ToInt32(dr["RefereePoint"].ToString()),
                    MiniReimbursementPoint = Convert.ToInt32(dr["MiniReimbursementPoint"].ToString()),
                    MiniReimbursementCash = Convert.ToInt32(dr["MiniReimbursementCash"].ToString()),
                };
                    objlist.Add(objsett );
            }
            }

        }

        finally
        {
            objParam = null;
        }

        return objlist;
    }
}