﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class CartDAL
{
    public DataSet UserCartListInsertUpdate(UserCart objUserCart, string Mode, int ListId, bool KeepExisting)
    {
        SqlParameter[] objParam = new SqlParameter[6];


        objParam[0] = new SqlParameter("@SessionId", objUserCart.SessionId);
        objParam[1] = new SqlParameter("@CustomerId", objUserCart.CustomerId);
        objParam[2] = new SqlParameter("@ListId", ListId);
        objParam[3] = new SqlParameter("@Qty", objUserCart.Qty);
        objParam[4] = new SqlParameter("@Mode", Mode);
        objParam[5] = new SqlParameter("@KeepExisting", KeepExisting);
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_UserCartListInsertUpdate", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public DataSet RemoveFromUserCart(UserCart objUserCart)
    {
        SqlParameter[] objParam = new SqlParameter[2];


        objParam[0] = new SqlParameter("@SessionId", objUserCart.SessionId);
        objParam[1] = new SqlParameter("@VariationId", objUserCart.VariationId);


        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_RemovefromUserCart", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public Int32 GetTotalItems(string SessionID)
    {

        Int32 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@SessionId", SessionID);
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_GetTotalItems", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
    public Int16 InsertComments(string Comments, string SessionId)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[3];

        objParam[0] = new SqlParameter("@Comments", Comments);
        objParam[1] = new SqlParameter("@SessionId", SessionId);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_CartInsertComments", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public Int16 ValidateCheckout(string SessionID)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@SessionId", SessionID);
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_ValidateCheckout", objParam);
            retValue =Convert.ToInt16(objParam[1].Value);
         
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public DataSet GetCartBySessionId(string SessionId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@SessionId", SessionId);
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_UserCartGetBySessionId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }




    public DataSet UserCartInsertUpdate(UserCart objUserCart, string Mode)
    {
        SqlParameter[] objParam = new SqlParameter[5];


        objParam[0] = new SqlParameter("@SessionId", objUserCart.SessionId);
        objParam[1] = new SqlParameter("@CustomerId", objUserCart.CustomerId);
        objParam[2] = new SqlParameter("@VariationId", objUserCart.VariationId);
        objParam[3] = new SqlParameter("@Qty", objUserCart.Qty);
        objParam[4] = new SqlParameter("@Mode", Mode);

        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_UserCartInsertUpdate", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }


    public DataSet SchemeProductsInsertUpdate(UserCart objUserCart, string Mode)
    {
        SqlParameter[] objParam = new SqlParameter[5];


        objParam[0] = new SqlParameter("@SessionId", objUserCart.SessionId);
        objParam[1] = new SqlParameter("@CustomerId", objUserCart.CustomerId);
        objParam[2] = new SqlParameter("@VariationId", objUserCart.VariationId);
        objParam[3] = new SqlParameter("@Qty", objUserCart.Qty);
        objParam[4] = new SqlParameter("@Mode", Mode);

        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_SchemeProductsInsertUpdate", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }


    public SqlDataReader UserCartIncrDecr(UserCart objUserCart, string Mode)
    {
        SqlParameter[] objParam = new SqlParameter[8];


        objParam[0] = new SqlParameter("@SessionId", objUserCart.SessionId);
        objParam[1] = new SqlParameter("@CustomerId", objUserCart.CustomerId);
        objParam[2] = new SqlParameter("@VariationId", objUserCart.VariationId);
        objParam[3] = new SqlParameter("@Qty", objUserCart.Qty);
        objParam[4] = new SqlParameter("@Mode", Mode);


        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_UserCartIncrDecr", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public int UpdateCartDetailQtyByProductId(string SessionId, int CustomerId, int ProductId, string Sign)
    {
        int retval = 0;
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@SessionId", SessionId);
        ObjParam[1] = new SqlParameter("@CustomerId", CustomerId);
        ObjParam[2] = new SqlParameter("@ProductId", ProductId);
        ObjParam[3] = new SqlParameter("@Sign", Sign);
        try
        {
            retval = Convert.ToInt32(SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_UpdateCartDetailQtyByProductID", ObjParam));

        }
        finally
        {
            ObjParam = null;
        }
        return retval;
    }
    public int DeleteCartDetailByproductId(string SessionId, int CustomerId, int ProductId)
    {
        int retval = 0;
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@SessionId", SessionId);
        ObjParam[1] = new SqlParameter("@CustomerId", CustomerId);
        ObjParam[2] = new SqlParameter("@ProductId", ProductId);
        try
        {
            retval = Convert.ToInt32(SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_deleteCartDetailByProductID", ObjParam));

        }
        finally
        {
            ObjParam = null;
        }
        return retval;
    }
    public SqlDataReader GetCartDetailByID(string SessionId, int CustomerId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@SessionId", SessionId);
        ObjParam[1] = new SqlParameter("@CustomerId", CustomerId);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_CartDetailGetByID", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public void UserCartDelete(String SessionId)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@SessionId", SessionId);

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_UserCartDelete", objParam);

        }
        finally
        {
            objParam = null;
        }


    }
    public string InsertMaster(Cart objCart)
    {

        string retValue = "0";
        SqlParameter[] objParam = new SqlParameter[14];

        objParam[0] = new SqlParameter("@SessionId", objCart.SessionId);
        objParam[1] = new SqlParameter("@CustomerId", objCart.CustomerId);
        objParam[2] = new SqlParameter("@PhoneNo", objCart.PhoneNo);
        objParam[3] = new SqlParameter("@RecipientFirstName", objCart.RecipientFirstName);
        objParam[4] = new SqlParameter("@RecipientLastName", objCart.RecipientLastName);
        objParam[5] = new SqlParameter("@RecipientMobile", objCart.RecipientMobile);
        objParam[6] = new SqlParameter("@RecipientPhone", objCart.RecipientPhone);
        objParam[7] = new SqlParameter("@City", objCart.City);
        objParam[8] = new SqlParameter("@Area", objCart.Area);
        objParam[9] = new SqlParameter("@Street", objCart.Street);
        objParam[10] = new SqlParameter("@Address", objCart.Address);
        objParam[11] = new SqlParameter("@PinCode", objCart.PinCode);
        objParam[12] = new SqlParameter("@OrderDate", objCart.OrderDate);
        objParam[13] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[13].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_CartMasterInsert", objParam);
            retValue = objParam[13].Value.ToString();
            //objCart.UserId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
    public string InsertDetail(Cart objCart)
    {

        string retValue = "0";
        SqlParameter[] objParam = new SqlParameter[7];

        objParam[0] = new SqlParameter("@SessionId", objCart.SessionId);
        objParam[1] = new SqlParameter("@CustomerId", objCart.CustomerId);
        objParam[2] = new SqlParameter("@ProductId", objCart.DProductId);
        objParam[3] = new SqlParameter("@Qty", objCart.DQty);
        objParam[4] = new SqlParameter("@Price", objCart.DPrice);
        objParam[5] = new SqlParameter("@ProductQty", objCart.DProductQty);
        objParam[6] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[6].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_CartDetailInsert", objParam);
            retValue = objParam[6].Value.ToString();
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}