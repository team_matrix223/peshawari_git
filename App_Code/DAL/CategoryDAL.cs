﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;


public class CategoryDAL
{
    public DataSet GetForMenu(string Page)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Page", Page);

        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductCategoriesGetAllForMenu", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

    public Int16 ChangeOrder(string qry)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[33];

        objParam[0] = new SqlParameter("@qry", qry);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_ProductCategoriesChangeOrder", objParam);
            retValue = Convert.ToInt16(objParam[1].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
    public Int16 InsertUpdate(Category objCategories)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[13];

        objParam[0] = new SqlParameter("@CategoryId", objCategories.CategoryId);
        objParam[1] = new SqlParameter("@Title", objCategories.Title);
        objParam[2] = new SqlParameter("@Description", objCategories.Description);
        objParam[3] = new SqlParameter("@ParentId", objCategories.ParentId);
        objParam[4] = new SqlParameter("@Level", objCategories.Level);
        objParam[5] = new SqlParameter("@IsActive", objCategories.IsActive);
        objParam[6] = new SqlParameter("@AdminId", objCategories.AdminId);
        objParam[8] = new SqlParameter("@ShowOnHome", objCategories.ShowOnHome);
        objParam[9] = new SqlParameter("@MetaTitle", objCategories.MetaTitle);
        objParam[10] = new SqlParameter("@MetaDescription", objCategories.MetaDescription);
        objParam[11] = new SqlParameter("@MetaKeyword", objCategories.MetaKeyword);
        objParam[12] = new SqlParameter("@PhotoUrl", objCategories.PhotoUrl);
        objParam[7] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[7].Direction = ParameterDirection.ReturnValue;
       
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_ProductCategoriesInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[7].Value);
            objCategories.CategoryId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }



    public DataSet GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];

        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductCategoriesGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }


    public SqlDataReader GetById(int CategoryId)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@CategoryId", CategoryId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductCategoriesGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public SqlDataReader GetByParentId(int ParentId)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@ParentId", ParentId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductCategoriesGetByParentId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public SqlDataReader GetByLevel(int level)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Level", level);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductCategoriesGetByLevel", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
}