﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for PinCodeDAL
/// </summary>
public class PinCodeDAL
{
    public Int16 InsertUpdate(PinCodes  objPincode)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];

        objParam[0] = new SqlParameter("@PinCodeId", objPincode.PinCodeId);
        objParam[1] = new SqlParameter("@PinCode", objPincode.PinCode);
        objParam[2] = new SqlParameter("@Sector", objPincode.Sector);
        objParam[3] = new SqlParameter("@CityId", objPincode.CityId);
        objParam[4] = new SqlParameter("@IsActive", objPincode.IsActive);

        objParam[5] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[5].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_PinCodesInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[4].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
    public List<PinCodes> GetAll()
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];
        List<PinCodes> objlist = new List<PinCodes>();
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_PinCodesGetAll", ObjParam);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    PinCodes objpincodes = new PinCodes()
                    {
                        PinCodeId = Convert.ToInt32(dr["PinCodeId"]),
                        PinCode = dr["PinCode"].ToString(),
                        Sector = dr["Sector"].ToString(),
                        CityName = dr["CityName"].ToString(),
                        CityId = Convert.ToInt32(dr["CityId"]),
                       IsActive = Convert.ToBoolean(dr["IsActive"])
                    };
                    objlist.Add(objpincodes);
                }
            }

        }
        finally
        {
            ObjParam = null;
            dr.Close();
            dr.Dispose();
        }
       

        return objlist;
    }
    public string GetAlloptions(int CityId)
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@CityId", CityId);
        List<PinCodes> objlist = new List<PinCodes>();
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_PinCodesGetByCityId", ObjParam);
            if (dr.HasRows)
            {
                strBuilder.Append("<option value='0'></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["PinCodeId"].ToString(), dr["Sector"].ToString()));
                }
            }

        }
        finally
        {
            ObjParam = null;
            dr.Close();
            dr.Dispose();
        }


        return strBuilder.ToString(); ;
    }
    public List<PinCodes> GetByCityId(int CityId)
    {
        List<PinCodes> objlist = new List<PinCodes>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@CityId", CityId);

        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_PinCodesGetByCityId", ObjParam);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    PinCodes objpincodes = new PinCodes()
                    {
                        PinCodeId = Convert.ToInt32(dr["PinCodeId"]),
                        PinCode = dr["PinCode"].ToString(),
                        Sector = dr["Sector"].ToString(),
                        CityName = dr["CityName"].ToString(),
                        CityId = Convert.ToInt32(dr["CityId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"])
                    };
                    objlist.Add(objpincodes);
                }
            }

        }
        finally
        {
            ObjParam = null;
            dr.Close();
            dr.Dispose();
        }


        return objlist;
    }

    public string GetPinCodeById(Int32 PinCodeId)
    {
        string  retVal = "";
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@PinCodeId", PinCodeId);
        ObjParam[1] = new SqlParameter("@RetVal",SqlDbType.Int,4);
        ObjParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
             SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_PinCodesGetPinCodeByPinCodeId", ObjParam);
             retVal = ObjParam[1].Value.ToString();
        }
        finally
        {
            ObjParam = null;
        }


        return retVal;
    }
    public string GetAddressById(Int32 PinCodeId)
    {
        string Address = "";
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@PinCodeId", PinCodeId);
        SqlDataReader dr = null;
        try
        {
          dr=  SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_PinCodesGetAddressByPinCodeId", ObjParam);
          if (dr.HasRows)
          {
              if (dr.Read())
              {
                  Address = dr["Address"].ToString();
              }
          }
        }
        finally
        {
            ObjParam = null;
            dr.Close();
            dr.Dispose();
        }


        return Address;
    }
}