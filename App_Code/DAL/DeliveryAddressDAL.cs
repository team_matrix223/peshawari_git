﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class DeliveryAddressDAL
{
    public DataSet KeywordSearch(string Keyword)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Keyword", Keyword);

        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_PinCodeKeywordSearch", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public SqlDataReader GetAllByUserId(Int64 UserId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@UserId",UserId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "Shopping_sp_DeliveryAddressGetByUserId", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }
    public int DeleteById(DeliveryAddress objDelAdd)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@UserId", objDelAdd.UserId);
        objParam[1] = new SqlParameter("@DeliveryAddressId", objDelAdd.DeliveryAddressId);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int,4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_deleteDeliveryAddressByID", objParam);
            retValue = Convert.ToInt32(objParam[2].Value);
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
    public int InsertUpdate(DeliveryAddress objDelAdd)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[14];
        objParam[0] = new SqlParameter("@UserId", objDelAdd.UserId);
        objParam[1] = new SqlParameter("@RecipientFirstName", objDelAdd.RecipientFirstName);
        objParam[2] = new SqlParameter("@RecipientLastName", objDelAdd.RecipientLastName);
        objParam[3] = new SqlParameter("@MobileNo", objDelAdd.MobileNo);
        objParam[4] = new SqlParameter("@Telephone", objDelAdd.Telephone);
        objParam[5] = new SqlParameter("@CityId", objDelAdd.CityId);
        objParam[6] = new SqlParameter("@Area", objDelAdd.Area);
        objParam[7] = new SqlParameter("@Street", objDelAdd.Street);
        objParam[8] = new SqlParameter("@Address", objDelAdd.Address);
        objParam[9] = new SqlParameter("@PinCode", objDelAdd.PinCode);
        objParam[10] = new SqlParameter("@IsPrimary", objDelAdd.IsPrimary);
        objParam[11] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[11].Direction = ParameterDirection.ReturnValue;
        objParam[12] = new SqlParameter("@DeliveryAddressId", objDelAdd.DeliveryAddressId);
        objParam[13] = new SqlParameter("@PinCodeId", objDelAdd.PinCodeId);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_DeliveryAddressInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[11].Value);
            objDelAdd.DeliveryAddressId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}