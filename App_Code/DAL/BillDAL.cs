﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for BillDAL
/// </summary>
public class BillDAL
{

    public int InsertUpdate(Bills objBills)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@OrderId", objBills.OrderId);
        
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 2);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_BillInsert", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objBills.BillId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }

    public int BillUpdate(Bills objBills)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@BIllId", objBills.BillId);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 2);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_BillUpdate", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objBills.BillId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }


    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_BillsGetByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public SqlDataReader GetByBillId(Int32 BillId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@BillId", BillId);


        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_BillGetByBillId", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }

    public SqlDataReader DeliverBillsGetByDate(DateTime DateFrom, DateTime DateTo)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_DeliveredBillsGetByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }



    public void InsertUpdateTemp(Bills objBill)
    {
        //Int32 retval = 0;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@BillId", objBill.BillId);


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_TempBillDetailInsert", ObjParam);
            //retval = Convert.ToInt32(ObjParam[2].Value);
            //objOrder.OrderId = retval;
        }
        finally
        {
            ObjParam = null;

        }

    }


    public SqlDataReader GetBillsTemp(Int64 BillId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BillId", BillId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_TempBillDetailGetAll", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;

    }


    public void DeleteTempBill(Int32 BillId, Int32 ProductId, Int64  VariationId)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@BillId", BillId);
        objParam[1] = new SqlParameter("@ProductId", ProductId);
        objParam[2] = new SqlParameter("@VariationId", VariationId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_TempBillDeleteByProductid", objParam);
        }
        finally
        {
            objParam = null;
        }



    }


    public int UpdatetempBillDetailQtyByProductId(int BillId, int ProductId, Int64  VariationId, int Qty, decimal Price, string Mode)
    {
        int retval = 0;
        SqlParameter[] ObjParam = new SqlParameter[6];


        ObjParam[0] = new SqlParameter("@BillId", BillId);
        ObjParam[1] = new SqlParameter("@ProductId", ProductId);
        ObjParam[2] = new SqlParameter("@VariationId", VariationId);
        ObjParam[3] = new SqlParameter("@Qty", Qty);
        ObjParam[4] = new SqlParameter("@Price", Price);
        ObjParam[5] = new SqlParameter("@Mode", Mode);
        try
        {
            retval = Convert.ToInt32(SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_UpdateTempBillDetailQtyByProductID", ObjParam));

        }
        finally
        {
            ObjParam = null;
        }
        return retval;
    }


}