﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class OrderDAL
{
    public SqlDataReader GetMobileNos(Int64 OrderId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@OrderId", OrderId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_MobileNosGetByOrderId", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }
    public SqlDataReader GetShippingAddress(Int64 OrderId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@OrderId", OrderId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ShippingAddressGetByOrderId", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }
    public SqlDataReader GetOrdersByUserId(Int64 UserId,DateTime FromDate,DateTime ToDate,string Status)
    {
        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@UserId",UserId);
        objParam[1] = new SqlParameter("@FromDate", FromDate);
        objParam[2] = new SqlParameter("@ToDate", ToDate);
        objParam[3] = new SqlParameter("@Status", Status);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_OrdersGetByUserID", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }
    public SqlDataReader GetOrderDetailById(Int64 OrderId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@OrderId", OrderId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_OrderDetailGetByID", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }

    public SqlDataReader GetOrderDetailByOrderId(Int64 OrderId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@OrderId", OrderId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_OrderDetailGetByID", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }



    public DataSet GetCartOrderByOrderId(Int64 OrderId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@OrderId", OrderId);
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_CartOrderGetByOrderID", objParam);
        }
        finally
        {
            objParam = null;
        }
        return ds;


    }





    public SqlDataReader GetOrdersTemp(Int64 OrderId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@OrderId", OrderId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_TempOrderDetailGetAll", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;

    }


    public void DeleteTempOrder(Int32 OrderId, Int32 ProductId, Int32 VariationId)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@OrderId", OrderId);
        objParam[1] = new SqlParameter("@ProductId", ProductId);
        objParam[2] = new SqlParameter("@VariationId", VariationId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_TempOrderDeleteByProductid", objParam);
        }
        finally
        {
            objParam = null;
        }



    }
    public SqlDataReader GetOrdersSummaryByUserId(Int64 UserId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@UserId", UserId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_OrdersSummaryGetByUserId", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }


    public SqlDataReader GetOrderDetailSummaryById(Int64 OrderId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@OrderId", OrderId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_OrdersDetailSummaryGetBYOrderId", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }

    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_OrdersGetByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }

    public SqlDataReader GetByOrderId(Int32 OrderId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@OrderId", OrderId);

       
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_OrderGetByOrderId", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public SqlDataReader InProcessOrdersGetByDate(DateTime DateFrom, DateTime DateTo)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_InProcessOrdersGetByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }

    public void InsertUpdate(OrderDetail objOrder, DataTable dt)
    {
        //Int32 retval = 0;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@OrderId", objOrder.OrderId);
        ObjParam[1] = new SqlParameter("@OrderDetail", dt);
      

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_TemOrderDetailInsert", ObjParam);
            //retval = Convert.ToInt32(ObjParam[2].Value);
            //objOrder.OrderId = retval;
        }
        finally
        {
            ObjParam = null;

        }
       
    }
    public int OrderInsertUpdate(string SessionId, Int32 CustomerId, string Remarks,string PaymentMode)
    {
        Int32 retval = 0;
        SqlParameter[] ObjParam = new SqlParameter[5];
        ObjParam[0] = new SqlParameter("@SessionId", SessionId);
        ObjParam[1] = new SqlParameter("@CustomerId", CustomerId);
        ObjParam[2] = new SqlParameter("@Remarks", Remarks);
        ObjParam[3] = new SqlParameter("@PaymentMode", PaymentMode);
        ObjParam[4] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        ObjParam[4].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_OrderInsertUpdate", ObjParam);
            retval = Convert.ToInt32(ObjParam[4].Value);

        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }


    public void UpdateStatusDeliver(Int32 OrderId)
    {
      
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@OrderId", OrderId);
       

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_OrderDelivered", ObjParam);
           

        }
        finally
        {
            ObjParam = null;

        }
       
    }

    public void InProcessOrders(Int32 OrderId)
    {

        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@OrderId", OrderId);


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_OrderInProcess", ObjParam);


        }
        finally
        {
            ObjParam = null;

        }

    }



    public void BilledOrders(Int32 OrderId)
    {

        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@OrderId", OrderId);


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_OrderBilled", ObjParam);


        }
        finally
        {
            ObjParam = null;

        }

    }

    public void InsertUpdate(OrderDetail objOrder)
    {
        //Int32 retval = 0;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@OrderId", objOrder.OrderId);


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_TemOrderDetailInsert", ObjParam);
            //retval = Convert.ToInt32(ObjParam[2].Value);
            //objOrder.OrderId = retval;
        }
        finally
        {
            ObjParam = null;

        }

    }

    public int UpdatetempOrderDetailQtyByProductId(int OrderId, int ProductId, Int32  VariationId, int Qty, decimal Price, string Mode)
    {
        int retval = 0;
        SqlParameter[] ObjParam = new SqlParameter[6];


        ObjParam[0] = new SqlParameter("@OrderId", OrderId);
        ObjParam[1] = new SqlParameter("@ProductId", ProductId);
        ObjParam[2] = new SqlParameter("@VariationId", VariationId);
        ObjParam[3] = new SqlParameter("@Qty", Qty);
        ObjParam[4] = new SqlParameter("@Price", Price);
        ObjParam[5] = new SqlParameter("@Mode", Mode);
        try
        {
            retval = Convert.ToInt32(SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_UpdateTempOrderDetailQtyByProductID", ObjParam));

        }
        finally
        {
            ObjParam = null;
        }
        return retval;
    }

    public SqlDataReader GetCancelOrderDetailById(Int64 OrderId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@OrderId", OrderId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_CancelOrderDetailGetByID", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }
}