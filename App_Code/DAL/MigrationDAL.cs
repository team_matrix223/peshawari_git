﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for MigrationDAL
/// </summary>
public class MigrationDAL
{

    public DataSet GetAllDepartments()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_DepartmentsGetAll", objParam);



        }

        finally
        {
            objParam = null;
        }
        return ds;


    }


    public int  InsertUpdateDepartmentGroups(string DepartmentId, DataTable dt)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@DepartmentId", DepartmentId);
        objParam[1] = new SqlParameter("@GroupType", dt);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_DepartmentsInsertUpdateGroup", objParam);
            retValue = 1;

        }
        catch (Exception ex)
        {
            retValue = 0;
        }
        finally
        {
            objParam = null;
        }

        return retValue;

    }


    public int UpdateMigrateProducts(int oldCategoryId, int oldsubcategory, int oldcategorylevel3, int CategoryId, int SubCategoryId, int CategoryLevel3, string Status, DataTable dtProducts)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[8];
        objParam[0] = new SqlParameter("@oldCategoryId", oldCategoryId);
        objParam[1] = new SqlParameter("@oldSubCategoryId", oldsubcategory);
        objParam[2] = new SqlParameter("@oldCategoryLevel3", oldcategorylevel3);
        objParam[3] = new SqlParameter("@CategoryId", CategoryId);
        objParam[4] = new SqlParameter("@SubCategoryId", SubCategoryId);
        objParam[5] = new SqlParameter("@CategoryLevel3", CategoryLevel3);
        objParam[6] = new SqlParameter("@Status ", Status);
        objParam[7] = new SqlParameter("@Products", dtProducts);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_MigrateProducts", objParam);
            retValue = 1;

        }
        catch (Exception ex)
        {
            retValue = 0;
        }
        finally
        {
            objParam = null;
        }

        return retValue;

    }



    public DataSet GetGroupsByDepartment(string DepartmentId)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        DataSet ds = null;
        objParam[0] = new SqlParameter("@DepartmentId", DepartmentId);

        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GroupsGetByDepartmentId", objParam);



        }

        finally
        {
            objParam = null;
        }
        return ds;


    }




}