﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for GetSectors
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class GetSectors : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string GetSectorList(int CityId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@CityId", CityId);
        DataSet ds = null;
        ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_PinCodesGetByCityId", objParam);

        List<WSPincode> lstPincode = new List<WSPincode>();

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
                WSPincode objPincode = new WSPincode();
                objPincode.PinCodeId = Convert.ToInt32(ds.Tables[0].Rows[i]["PinCodeId"]);
                objPincode.Sector = ds.Tables[0].Rows[i]["Sector"].ToString();
                lstPincode.Add(objPincode);

        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

           SectorList = lstPincode,
        };
        return ser.Serialize(JsonData);



    }
}
