﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for LoginUser
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class LoginUser : System.Web.Services.WebService {
    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string Login(string MobileNo, string Password )
    {
        WSUser objuser = new WSUser()
        {
            MobileNo = MobileNo,
            Password = Password
        };

        string retVal = "";
        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@MobileNo", objuser.MobileNo);
        objParam[1] = new SqlParameter("@Password", objuser.Password);
        objParam[2] = new SqlParameter("@RetVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@SessionId", "");
        SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
       "shopping_sp_UserLoginCheck", objParam);
        retVal = objParam[2].Value.ToString();


        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Status = retVal
        };
        return ser.Serialize(JsonData);


    }

    
}
