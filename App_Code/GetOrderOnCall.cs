﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using System.Data;

/// <summary>
/// Summary description for GetOrderOnCall
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class GetOrderOnCall : System.Web.Services.WebService {
    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string GetMobileNo()
    {
        string MobileNo = "";
        DataSet ds = null;
        SqlParameter[] objParam = new SqlParameter[0];

      ds=  SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_MessageCredentialsGetAll", objParam);

      if (ds.Tables[0].Rows.Count > 0)
      {
          MobileNo = ds.Tables[0].Rows[0]["OrderOnCall"].ToString();
      }
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            OrderOnCall = MobileNo.Trim(),
        };
        return ser.Serialize(JsonData);


    }   

}
