﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for GetSlots
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class GetSlots : System.Web.Services.WebService
{

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string SlotList(string Day)
    {



        string Date = Convert.ToDateTime(Day).ToShortDateString() + " ";
        TimeSlots objTimeSlots = new TimeSlots();

        List<TimeSlots> Slots = null;
       
        DateTime dd;
        dd = Convert.ToDateTime(DateTime.Now.ToString("d"));

        string curday = dd.DayOfWeek.ToString();
        string slctday = Day;
        if (curday != slctday)
        {

            Slots = new TimeSlotsBLL().GetAllSlots();



        }
        else
        {

            Slots = new TimeSlotsBLL().GetSlots();


        }





        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            Slots = Slots

        };
        return ser.Serialize(JsonData);


    }



}
