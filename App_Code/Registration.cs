﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Services;

/// <summary>
/// Summary description for Registration
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class Registration : System.Web.Services.WebService {

     [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string UserRegistration(string Name="",  string MobileNo="", string Password="",string EmailId="",string DOB="",string DOA="")
    {
        WSUser objuser = new WSUser()
        {
            FirstName=Name,
            MobileNo=MobileNo,
            Password=Password,
            EmailId=EmailId,
            DOB= DOB,
            DOA= DOA
        };
       
        string retVal="";
        SqlParameter[] objParam = new SqlParameter[8];
        objParam[0] = new SqlParameter("@FirstName", objuser.FirstName );
        objParam[1] = new SqlParameter("@EmailId", objuser.EmailId);
        objParam[2] = new SqlParameter("@MobileNo",objuser.MobileNo);
        objParam[3] = new SqlParameter("@Password", objuser.Password);
        objParam[4] = new SqlParameter("@RetVal", SqlDbType.Int,4);
        objParam[4].Direction = ParameterDirection.ReturnValue;
        objParam[5] = new SqlParameter("@UserId", objuser.UserId);
        objParam[6] = new SqlParameter("@DOB", objuser.DOB);
        objParam[7] = new SqlParameter("@DOA", objuser.DOA);
         SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_RegisterUsersAPP", objParam);
         retVal = objParam[4].Value.ToString();


         JavaScriptSerializer ser = new JavaScriptSerializer();

         var JsonData = new
         {

             Status = retVal
         };
         return ser.Serialize(JsonData);


    }
}
