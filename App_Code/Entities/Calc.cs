﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Calc
/// </summary>
public class Calc
{
    public int SubTotal { get; set; }
    public int DeliveryCharges { get; set; }
    public decimal MinimumCheckOutAmt { get; set; }
    public decimal FreeDeliveryAmt { get; set; }
    public int NetAmount { get { return SubTotal + DeliveryCharges; } }
    public int Qty { get; set; }
    public int ProductSubTotal { get; set; }
    public int Price { get; set; }
    public string VariationId { get; set; }
    public int TotalItems { get; set; }
    public string Comments { get; set; }
    public decimal DisAmt { get; set; }
	public Calc()
	{
        TotalItems = 0;
        Qty = 0;
        Price = 0;
        SubTotal = 0;
        DeliveryCharges = 0;
        ProductSubTotal = 0;
        VariationId = "";
        FreeDeliveryAmt = 0;
        MinimumCheckOutAmt = 0;
        Comments = "";
        DisAmt = 0;
	}
}