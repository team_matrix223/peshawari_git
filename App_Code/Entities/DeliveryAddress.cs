﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DeliveryAddress
/// </summary>
public class DeliveryAddress
{
    public Int64 DeliveryAddressId { get; set; }
    public Int64 UserId { get; set; }
    public string RecipientFirstName { get; set; }
    public string RecipientLastName { get; set; }
    public string MobileNo { get; set; }
    public string Telephone { get; set; }
    public int CityId { get; set; }
    public string Area { get; set; }
    public string Street { get; set; }
    public string Address { get; set; }
    public string PinCode { get; set; }
    public bool IsPrimary { get; set; }
    public string CityName { get; set; }
    public DateTime DOC { get; set; }
    public int PinCodeId { get; set; }
    public DeliveryAddress()
    {
        CityName = string.Empty;
        DeliveryAddressId = 0;
        UserId = 0;
        RecipientFirstName = string.Empty;
        RecipientLastName = string.Empty;
        MobileNo = string.Empty;
        Telephone = string.Empty;
        CityId = 0;
        Area = string.Empty;
        Street = string.Empty;
        Address = string.Empty;
        PinCode = string.Empty;
        IsPrimary = false;
        DOC = DateTime.Now;
        PinCodeId = 0;

    }
}