﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserCartMst
/// </summary>
public class UserCartMst
{

    public string SessionId { get; set; }
    public DateTime DeliveryDate { get; set; }
    public string DeliverySlot { get; set; }
    public string PaymentMode { get; set; }
    public string RecipientName { get; set; }
    public string MobileNo { get; set; }
    public string Telephone { get; set; }
    public string City { get; set; }
    public string Area { get; set; }
    public string Street { get; set; }
    public string Address { get; set; }
    public string PinCode { get; set; }
    public Int64 UserId { get; set; }
    public Int64 DeliveryAddressId { get; set; }
    public UserCartMst()
    {

        SessionId = string.Empty;
        DeliveryDate = DateTime.Now;
        DeliverySlot = string.Empty;
        PaymentMode = string.Empty;
        RecipientName = string.Empty;
        MobileNo = string.Empty;
        Telephone = string.Empty;
        City = string.Empty;
        Area = string.Empty;
        Street = string.Empty;
        Address = string.Empty;
        PinCode = string.Empty;
        UserId = 0;
        DeliveryAddressId = 0;
    }
}