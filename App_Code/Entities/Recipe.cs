﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Admin
/// </summary>
public class Recipe
{


    public string Title { get; set; }
    public int RecipeId { get; set; }
    public string ShortDescription { get; set; }
    public string Process { get; set; }
    public DateTime DOC { get; set; }
    public bool IsActive { get; set; }
    public string ImageUrl { get; set; }


    public Recipe()
    {
        ImageUrl = string.Empty;
        Title = string.Empty;
        RecipeId = 0;
        ShortDescription = string.Empty;
        Process = string.Empty;
        DOC = DateTime.Now;
        IsActive = true;
    }
}