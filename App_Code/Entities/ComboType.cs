﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class ComboType
{
    public int ComboTypeId { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public bool IsActive { get; set; }
    public int AdminId { get; set; }
    public DateTime DOC { get; set; }
	public ComboType()
	{
        ComboTypeId = 0;
        Title = string.Empty;
        IsActive = false;
        AdminId = 0;
        DOC = DateTime.Now;
        Description = string.Empty;
	}
}