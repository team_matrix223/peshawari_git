﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Products
/// </summary>
public class Products
{


    public int ProductId { get; set; }
    public int CategoryId { get; set; }
    public int SubCategoryId { get; set; }
    public string Name { get; set; }
    public string ShortName { get; set; }
    public string Description { get; set; }
    public string ShortDescription { get; set; }
    public string PhotoUrl { get; set; }
    public bool IsActive { get; set; }
    public DateTime DOC { get; set; }
    public int AdminId { get; set; }
    public string CategoryName { get; set; }
    public string SubCategoryName { get; set; }


    public string Unit1 { get; set; }
    public decimal Price1 { get; set; }
    public decimal Mrp1 { get; set; }
    public decimal Qty1 { get; set; }
    public string Desc1 { get; set; }


    public string Unit2 { get; set; }
    public decimal Price2 { get; set; }
    public decimal Mrp2 { get; set; }
    public decimal Qty2 { get; set; }
    public string Desc2 { get; set; }


    public string Unit3 { get; set; }
    public decimal Price3 { get; set; }
    public decimal Mrp3 { get; set; }
    public decimal Qty3 { get; set; }
    public string Desc3 { get; set; }
    public int BrandId { get; set; }
    public int Variation1 { get; set; }
    public int Variation2 { get; set; }
    public int Variation3 { get; set; }
    public decimal Price { get; set; }
    public int VariationId { get; set; }

    public int CategoryLevel3 { get; set; }
    public int Qty { get; set; }

    public string Unit { get; set; }
    public string Type { get; set; }
    public decimal MRP { get; set; }
    public string ItemCode { get; set; }
    public string descr { get; set; }


    public Products()
    {
        VariationId = 0;
        Price = 0;
        Variation1 = 0;
        Variation2 = 0;
        Variation3 = 0;
        ShortDescription = "";
        ProductId = 0;
        CategoryId = 0;
        SubCategoryId = 0;
        Name = string.Empty;
        ShortName = string.Empty;
        Description = string.Empty;
        PhotoUrl = string.Empty;
        IsActive = false;
        DOC = DateTime.Now;
        AdminId = 0;
        CategoryName = string.Empty;
        SubCategoryName = string.Empty;
        Unit1 = string.Empty;
        Price1 = 0;
        Mrp1 = 0;
        Qty1 = 0;
        Desc1 = string.Empty;


        Unit2 = string.Empty;
        Price2 = 0;
        Mrp2 = 0;
        Qty2 = 0;
        Desc2 = string.Empty;



        Unit3 = string.Empty;
        Price3 = 0;
        Mrp3 = 0;
        Qty3 = 0;
        Desc3 = string.Empty;

        CategoryLevel3 = 0;
        BrandId = 0;
        Type = "";
        Unit = "";
        MRP = 0;
        descr = "";
        Qty = 0;
        ItemCode = "";

    }
}