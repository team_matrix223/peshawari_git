﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Order
{
    public int OrderId { get; set; }
    public int CustomerId { get; set; }
    public DateTime OrderDate { get; set; }
    public string strOD { get { return OrderDate.ToString("d"); } }
    public string PhoneNo { get; set; }
    public string RecipientFirstName { get; set; }
    public string RecipientLastName { get; set; }
    public string RecipientMobile { get; set; }
    public string RecipientPhone { get; set; }
    public int  City { get; set; }
    public string Area { get; set; }
    public string Street { get; set; }
    public string Address { get; set; }
    public string Pincode { get; set; }
    public decimal  BillValue { get; set; }
    public decimal  DisPer { get; set; }
    public decimal DisAmt { get; set; }
    public decimal ServiceTaxPer { get; set; }
    public decimal ServiceTaxAmt { get; set; }
    public decimal ServiceChargePer { get; set; }
    public decimal ServiceChargeAmt { get; set; }
    public decimal VatPer { get; set; }
    public decimal VatAmt { get; set; }
    public decimal NetAmount { get; set; }
    public string  Status { get; set; }
    public string Remarks { get; set; }
    public int  ExecutiveId { get; set; }
    public string  IPAddress { get; set; }
    public string Recipient { get; set; }
    public string CityName { get; set; }
    public string CompleteAddress { get; set; }
    public string CustomerName { get; set; }
    public string DeliverySlot { get; set; }
    public string PaymentMode { get; set; }
    public decimal DeliveryCharges { get; set; }
    public decimal FreeDeliveryAmt { get; set; }
    public string Comment { get; set; }
    public string CouponNo { get; set; }
    public DateTime DeliveryDate { get; set; }
    public string strDD { get { return DeliveryDate.ToString("d"); } }

    public Int32 DeliveryAddressId { get; set; }
   
    public Int32 ListId { get; set; }

    public Int32 PictureOrderId { get; set; }

    public int GiftId { get; set; }
    public string GiftName { get; set; }

    public string PaymentStatus { get; set; }
    public string ResponseCode { get; set; }
    public string ResponseMessage { get; set; }
    public string TxnId { get; set; }
    public string OrderType { get; set; }

         
	public Order()
	{
        OrderId=0;
        CustomerId=0;
OrderDate=DateTime.Now;
PhoneNo="";
RecipientFirstName="";
RecipientLastName="";
RecipientMobile="";
RecipientPhone="";
City=0;
Area="";
Street="";
Address="";
Pincode="";
BillValue=0;
DisPer=0;
DisAmt=0;
ServiceTaxPer=0;
ServiceTaxAmt=0;
ServiceChargePer=0;
ServiceChargeAmt=0;
VatPer = 0; ;
VatAmt = 0;
NetAmount=0;
Status="";
Remarks="";
ExecutiveId=0;
IPAddress="";
DeliverySlot = "";
PaymentMode = "";

DeliveryCharges = 0;
FreeDeliveryAmt = 0;
Comment = "";
CouponNo = "";
DeliveryDate = DateTime.Now;
ListId = 0;
PictureOrderId = 0;
    }
}