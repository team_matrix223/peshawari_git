﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Packing
/// </summary>
public class Packing
{
    public string ItemCode { get; set; }
    public string ItemName { get; set; }
    public string ShortName { get; set; }
    public string Department { get; set; }
    public string Brand { get; set; }
    public decimal Price { get; set; }
    public decimal MRP { get; set; }
    public string Unit { get; set; }
    public int Qty { get; set; }
    public Packing()
    {
        Unit = string.Empty;
        Qty = 0;
        Price = 0;
        MRP = 0;
        ItemCode = string.Empty;
        ItemName = string.Empty;
        ShortName = string.Empty;
        Department = string.Empty;
        Brand = string.Empty;


    }
}