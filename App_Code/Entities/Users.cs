﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class Users
{
    public int UserId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string EmailId { get; set; }
    public string Password { get; set; }
    public bool IsActive { get; set; }
    public int AdminId { get; set; }
    public int DeliveryAddressId { get; set; }
    public string RecipientFirstName { get; set; }
    public string RecipientLastName { get; set; }
    public string MobileNo { get; set; }
    public string RMobileNo { get; set; }
    public string Telephone { get; set; }
    public int  CityId { get; set; }
    public string Area { get; set; }
    public string Street { get; set; }
    public string Address { get; set; }
    public string PinCode { get; set; }
    public bool IsPrimary { get; set; }
    public DateTime DOC { get; set; }
    public string strDOC { get { return DOC.ToShortDateString(); } }
    public int PinCodeId { get; set; }
    public DateTime DOA { get; set; }
    public string strDOA { get { return DOA.ToShortDateString(); } }
    public DateTime DOB { get; set; }
    public string strDOB { get { return DOB.ToShortDateString(); } }
    public string  Code { get; set; }
    public string ReferralCode { get; set; }
    public bool NewsLetter { get; set; }

    public Users()
	{
        UserId = 0;
        FirstName = "";
        LastName = "";
        EmailId = "";
        Password = "";
        IsActive = true;
        AdminId = 0;
         RecipientFirstName = "";
        RecipientLastName = "";
        MobileNo = "";
        RMobileNo = "";
        Telephone = "";
        CityId = 0;
        Area = "";
        Street = "";
        Address = "";
        PinCode = "";
        IsPrimary = false;
        DOC = DateTime.Now;
        DeliveryAddressId = 0;
        PinCodeId = 0;
        DOA = DateTime.Now;
        DOB = DateTime.Now;
        Code = "";
        ReferralCode = "";
        NewsLetter = true;
	}
}