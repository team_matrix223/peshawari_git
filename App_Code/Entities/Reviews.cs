﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Reviews
/// </summary>
public class Reviews
{
    public int ReviewId { get; set; }
    public int UserId { get; set; }
    public string UserName { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Rating { get; set; }
    public bool IsApproved { get; set; }
    public DateTime ReviewDate { get; set; }
    public string strRD { get { return ReviewDate.ToString("d"); } }
	public Reviews()
	{
        ReviewId = 0;
        UserId = 0;
        UserName = "";
        Title = string.Empty;
        Description = string.Empty;
        Rating = string.Empty;
        IsApproved = false;
        ReviewDate = DateTime.Now;
	}
}