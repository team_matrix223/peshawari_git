﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class OrderDetail
{
    public int SchemeId { get; set; }
    public int VariationId { get; set; }
    public int OrderDetailId { get; set; }
    public int OrderId { get; set; }
    public int ProductId { get; set; }
    public string  ProductName { get; set; }
    public string   ProductDescription { get; set; }
    public string Units { get; set; }
    public int Qty { get; set; }
    public decimal Price { get; set; }
    public decimal DisPer { get; set; }
    public decimal DisAmt { get; set; }
    public decimal VatPer { get; set; }
    public decimal VatAmt { get; set; }
    public decimal ServiceTaxPer { get; set; }
    public decimal ServiceTaxAmt { get; set; }
    public decimal ServiceChargePer { get; set; }
    public decimal ServiceChargeAmt { get; set; }
    public decimal Amount { get; set; }
    public string ProductQuantityIn { get; set; }
    public decimal Rate { get; set; }
    public string  PhotoUrl { get; set; }
    public string BrandName { get; set; }

    public decimal DeliveryCharges { get; set; }
	public OrderDetail()
	{
        SchemeId = 0;
        OrderDetailId = 0;
        OrderId = 0;
        ProductId = 0;
        ProductDescription = "";
        Units = "";
        Qty = 0;
        Price = 0;
        DisPer = 0;
        DisAmt = 0;
        VatPer = 0;
        VatAmt = 0;
        ServiceTaxPer = 0;
        ServiceTaxAmt = 0;
        ServiceChargePer = 0;
        ServiceChargeAmt = 0;
        Amount = 0;
        PhotoUrl = "";
        DeliveryCharges = 0;
        BrandName = "";
	}
}