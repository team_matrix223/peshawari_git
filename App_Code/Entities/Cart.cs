﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Cart
{
    public string  SessionId { get; set; }
    public int CustomerId { get; set; }
    public string  PhoneNo { get; set; }
    public string  RecipientFirstName { get; set; }
    public string  RecipientLastName { get; set; }
    public string  RecipientMobile { get; set; }
    public string RecipientPhone { get; set; }
    public int  City { get; set; }
    public string Area { get; set; }
    public string Street { get; set; }
    public string Address { get; set; }
    public string PinCode { get; set; }
    public DateTime  OrderDate { get; set; }
    public int DProductId { get; set; }
    public decimal DProductQty { get; set; }
    public decimal  DQty { get; set; }
    public decimal  DPrice { get; set; }
    public string  DProductName { get; set; }
    public string  PhotoUrl { get; set; }
	public Cart()
	{
        SessionId = "";
        CustomerId = 0;
        PhoneNo = "";
        RecipientFirstName = "";
        RecipientLastName = "";
        RecipientMobile = "";
        RecipientPhone = "";
        City = 0;
        Area = "";
        Street ="";
        Address = "";
        PinCode = "";
        OrderDate = DateTime.Now;
        DProductId = 0;
        DQty = 0;
        DPrice = 0;
        DProductQty = 0;
	}
}