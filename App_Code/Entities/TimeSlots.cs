﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TimeSlots
/// </summary>
public class TimeSlots
{
    public int Id { get; set; }
    public string StartTime { get; set; }
    public string SlotStartTime { get; set; }
    public string SlotEndTime { get; set; }
    public string EndTime { get; set; }
    public bool IsActive { get; set; }

    public int AdminId { get; set; }

	public TimeSlots()
	{
        Id = 0;
        StartTime = string.Empty;
        EndTime = string.Empty;
        SlotEndTime = string.Empty;
        SlotEndTime = string.Empty;
        AdminId = 0;
        IsActive = true;
	}
}