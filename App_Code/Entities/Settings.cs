﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class Settings
{
    public decimal DeliveryCharge { get; set; }
    public decimal MinimumCheckOutAmt { get; set; }
    public decimal FreeDeliveryAmt { get; set; }
    public int PointRate { get; set; }
    public int ReferralPoint { get; set; }
    public int RefereePoint { get; set; }
    public int MiniReimbursementPoint { get; set; }
    public int MiniReimbursementCash { get; set; }

    public Settings()
    {
        DeliveryCharge = 0;
        MinimumCheckOutAmt = 0;
        FreeDeliveryAmt = 0;
        PointRate = 0;
        ReferralPoint = 0;
        RefereePoint = 0;
        MiniReimbursementPoint = 0;
        MiniReimbursementCash = 0;
	}
   
}