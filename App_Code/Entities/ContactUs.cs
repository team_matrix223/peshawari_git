﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class ContactUs
{
    public int ContactId { get; set; }
    public string FirstName { get; set; }
    public string FirstName1 { get; set; }
    public string LastName { get; set; }
    public string  Email { get; set; }
    public string  Phoneno { get; set; }
    public string  Subject { get; set; }
    public string Enquiry { get; set; }
    public string Enquiry1 { get; set; }
    public int AdminId { get; set; }
    public DateTime  Sdate { get; set; }
    public string strSD { get { return Sdate.ToString("d"); } }
    public int ParentId { get; set; }
    public string  Status { get; set; }
    public string FullName { get; set; }
	public ContactUs()
	{
        ContactId = 0;
        FirstName = "";
        FirstName1 = "";
        LastName = "";
        Email = "";
        Phoneno = "";
        Subject = "";
        Enquiry = "";
        Enquiry1 = "";
        AdminId = 0;
        Sdate = DateTime.Now;
        ParentId = 0;
        Status = "";
        FullName = "";
	}
}