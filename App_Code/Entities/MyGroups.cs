﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyGroups
/// </summary>
public class MyGroups
{
	public int Id { get; set; }
    public string Title { get; set; }
    public bool IsActive { get; set; }
    public int AdminId { get; set; }
    public string PhotoUrl1 { get; set; }
    public string PhotoUrl2 { get; set; }
    public string ShowOn { get; set; }
    public string LinkUrl { get; set; }
    public int GroupId { get; set; }
    public int ProductId { get; set; }
    public bool HasSubGroup { get; set; }
    public string MobileImgUrl { get; set; }

	public MyGroups()
	{
        AdminId = 0;
        Id = 0;
        Title = string.Empty;
        PhotoUrl1 = string.Empty;
        PhotoUrl2 = string.Empty;
        ShowOn = string.Empty;
        LinkUrl = string.Empty;
        GroupId = 0;
        ProductId = 0;
        IsActive = true;
        HasSubGroup = false;
        MobileImgUrl = "";
	}
}