﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class List
{
    public int ListId { get; set; }
    public string  Title { get; set; }
    public int UserId { get; set; }
    public int VariationId { get; set; }
    public int Qty { get; set; }
    public DateTime  DOC { get; set; }
    public bool  IsActive { get; set; }
	public List()
    {
        ListId = 0;
        Title = "";
        UserId = 0;
        VariationId = 0;
        Qty = 0;
        DOC = System.DateTime.Now;
        IsActive = true;
	}
}