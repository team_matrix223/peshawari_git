﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Headings
/// </summary>
public class SubCategory 
{

    public int SubCategoryId { get; set; }
    public string Title{ get; set; }
    public string Description { get; set; }
    public int CategoryId { get; set; }
    public bool IsActive{ get; set; }
    public int AdminId { get; set; }
    public DateTime DOC { get; set; }
    public string CategoryName { get; set; }
    public SubCategory()
	{
        CategoryName = string.Empty;
        Title = string.Empty;
        IsActive = true;
        SubCategoryId = 0;
        CategoryId = 0;
        Description = string.Empty;
        AdminId = 0;
        DOC = DateTime.Now;
 

    }
}