﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CancelOrders
/// </summary>
public class CancelOrders
{


    public int OrderCancelId { get; set; } 
    public int OrderId { get; set; }
    public int CustomerId { get; set; }
    public DateTime OrderDate { get; set; }
    public string strOD { get { return OrderDate.ToString("d"); } }
    public string CustomerName { get; set; }
    public string RecipientFirstName { get; set; }
    public string RecipientLastName { get; set; }
    public string RecipientMobile { get; set; }
    public string RecipientPhone { get; set; }
    public string Recipient { get; set; }
    public string CompleteAddress { get; set; }
    public int City { get; set; }
    public string Area { get; set; }
    public string Street { get; set; }
    public string Address { get; set; }
    public string Pincode { get; set; }
    public decimal BillValue { get; set; }
    public decimal DisPer { get; set; }
    public decimal DisAmt { get; set; }
    public decimal ServiceTaxPer { get; set; }
    public decimal ServiceTaxAmt { get; set; }
    public decimal ServiceChargePer { get; set; }
    public decimal ServiceChargeAmt { get; set; }
    public decimal VatPer { get; set; }
    public decimal VatAmt { get; set; }
    public decimal NetAmount { get; set; }
    public string Remarks { get; set; }
    public int ExecutiveId { get; set; }
    public string IPAddress { get; set; }
    public Decimal DeliveryCharges { get; set; }
    public string DeliverySlot { get; set; }
    public string PaymentMode { get; set; }
    public DateTime canceldate { get; set; }
    public string strCD { get { return canceldate.ToString("d"); } }
    public string CancelRemarks { get; set; }
  
	public CancelOrders()
	{
        OrderCancelId = 0;
        OrderId = 0;
        CustomerId = 0;
        OrderDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
        RecipientFirstName = string.Empty;
        RecipientLastName = string.Empty;
        RecipientMobile = string.Empty;
        RecipientPhone = string.Empty;
        CustomerName = string.Empty;
        Recipient = string.Empty;
        CompleteAddress = string.Empty;
        City = 0;
        Area = string.Empty;
        Street = string.Empty;
        Address = string.Empty;
        Pincode = string.Empty;
        BillValue = 0;
        DisPer = 0;
        DisAmt = 0;
        ServiceTaxPer = 0;
        ServiceTaxAmt = 0;
        ServiceChargeAmt = 0;
        ServiceChargePer = 0;
        VatPer = 0;
        VatAmt = 0;
        NetAmount = 0;
        Remarks = "";
        ExecutiveId = 0;
        IPAddress = "";
        DeliveryCharges =0;
        DeliverySlot = "";
        PaymentMode = "";
        canceldate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
        CancelRemarks = "";
	}
}