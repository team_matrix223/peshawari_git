﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillCancel
/// </summary>
public class BillCancel
{
    public int BillCancelId { get; set; }
    public int BillId { get; set; }
    public int OrderId { get; set; }
    public int CustomerId { get; set; }
    public DateTime BillDate { get; set; }
    public string strBD { get { return BillDate.ToString("d"); } }
    public string PhoneNo { get; set; }
    public string RecipientFirstName { get; set; }
    public string RecipientLastName { get; set; }
    public string RecipientMobile { get; set; }
    public string RecipientPhone { get; set; }
    public string City { get; set; }
    public string Area { get; set; }
    public string Street { get; set; }
    public string Address { get; set; }
    public string Pincode { get; set; }
    public decimal BillValue { get; set; }
    public decimal DisPer { get; set; }
    public decimal DisAmt { get; set; }
    public decimal ServiceTaxPer { get; set; }
    public decimal ServiceTaxAmt { get; set; }
    public decimal ServiceChargePer { get; set; }
    public decimal ServiceChargeAmt { get; set; }
    public decimal VatPer { get; set; }
    public decimal VatAmt { get; set; }
    public decimal NetAmount { get; set; }
    public string Remarks { get; set; }
    public int ExecutiveId { get; set; }
    public string IPAddress { get; set; }
    public string CustomerName { get; set; }
    public string Recipient { get; set; }
    public string CompleteAddress { get; set; }
    public string CancelRemarks { get; set; }
    public DateTime CancelDate { get; set; }
    public string Status { get; set; }
	public BillCancel()
	{
        BillCancelId = 0;
        BillId = 0;
        OrderId = 0;
        CustomerId = 0;
        BillDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
        PhoneNo = string.Empty;
        RecipientFirstName = string.Empty;
        RecipientLastName = string.Empty;
        RecipientMobile = string.Empty;
        RecipientPhone = string.Empty;
        City = string.Empty;
        Area = string.Empty;
        Street = string.Empty;
        Address = string.Empty;
        Pincode = string.Empty;
        BillValue = 0;
        DisPer = 0;
        DisAmt = 0;
        ServiceTaxPer = 0;
        ServiceTaxAmt = 0;
        ServiceChargeAmt = 0;
        ServiceChargePer = 0;
        VatPer = 0;
        VatAmt = 0;
        NetAmount = 0;
        Remarks = "";
        ExecutiveId = 0;
        IPAddress = "";
        CancelRemarks = "";
        CancelDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
	}
}