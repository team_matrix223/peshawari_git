﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Category
/// </summary>
public class TestCategory
{
    public int CategoryId { get; set; }
    public string Title { get; set; }
	public TestCategory()
	{
        CategoryId = 0;
        Title = string.Empty;

	}
}