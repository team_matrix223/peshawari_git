﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class list : System.Web.UI.Page
{
     
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Session["dummy"] == null)
        {
            this.Session["dummy"] = 1;
        }

        if (!IsPostBack)
        {
            hdnLevel1.Value = Request.QueryString["c"] != null ? Request.QueryString["c"] : "0";
            hdnLevel2.Value = Request.QueryString["s"] != null ? Request.QueryString["s"] : "0";
            hdnLevel3.Value = Request.QueryString["sc"] != null ? Request.QueryString["sc"] : "0";



            Int32 CategoryId = hdnLevel1.Value != "0" ? Convert.ToInt32(hdnLevel1.Value) : hdnLevel2.Value != "0" ? Convert.ToInt32(hdnLevel2.Value) : hdnLevel3.Value != "0" ? Convert.ToInt32(hdnLevel3.Value) : 0;
    

            BindMetaInformation(CategoryId);
        }
    }


    void BindMetaInformation(Int32 CategoryId)
    {
        Category objCategory = new Category();
        objCategory.CategoryId = CategoryId;
        new CategoryBLL().GetById(objCategory);
        this.Master.PageTitle = objCategory.MetaTitle;
        this.MetaKeywords = objCategory.MetaKeyword;
        this.MetaDescription = objCategory.MetaDescription;


     
    
    }

    [WebMethod]
    public static string AdvancedSearch(string Brands, string Level1, string Level2, string Level3, int PageId,int MinPrice,int MaxPrice)
    {
        //System.Threading.Thread.Sleep(15000);
        Int64 l1 = CommonFunctions.IsNumeric(Level1);
        Int64 l2 = CommonFunctions.IsNumeric(Level2);
        Int64 l3 = CommonFunctions.IsNumeric(Level3);



        if (l3 > 0)
        {
            l1 = 0;
            l2 = 0;
        }
        else if (l2 > 0)
        {
            l1 = 0;
            l3 = 0;
        }
        else if (l1 > 0)
        {
            l2 = 0;
            l3 = 0;
        }
        else
        {
            l1 = 1;
            l2 = 0;
            l3= 0;
        }


        string BrandString = "";
        string CategoryString = "";
        string CategoryName = "";
        string Desc = "";
        int totalRecords = 0;
        int lPrice = 0;
        int hPrice = 0;
        string products = new ProductsBLL().AdvancedSearch(HttpContext.Current.Session.SessionID, Brands, l1, l2, l3, out BrandString, out CategoryString, out CategoryName, out Desc,out totalRecords,PageId,MinPrice,MaxPrice,out lPrice,out hPrice);

        


        var JsonData = new
        {

            ProductData = products,
            CategoryData = CategoryString,
            BrandData= BrandString,
            CatTitle=CategoryName,
            CatDesc=Desc,
            TotalRecords = totalRecords,
            MinP=lPrice,
            MaxP=hPrice
             
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
       ser.MaxJsonLength = int.MaxValue;
       return ser.Serialize(JsonData);
   
    }
}