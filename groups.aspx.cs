﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class groups : System.Web.UI.Page
{
    public int GroupId { get { return Request.QueryString["id"] != null ?Convert.ToInt32(Request.QueryString["id"]) : 0; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindSubGroups();
        }
    }
    void BindSubGroups()
    {
        string GroupImage = "";
        string CategoriesHTML = "";
       ltSubGroups.Text = new GroupLinksBLL().GetSubGroupHtml(GroupId,out GroupImage,out CategoriesHTML);
       imgCategory.ImageUrl = "http://www.thevegancrew.com/wp-content/uploads/2011/10/AmysKitchen.png";
       ltCategoriesHTML.Text = CategoriesHTML;
    }
}