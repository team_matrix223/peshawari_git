﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Data;

public partial class index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Session["dummy"] == null)
        {
            this.Session["dummy"] = 1;
        }
        if (!IsPostBack)
        {
            Master.PageTitle = "Peshawari Super Market | +91 9041781535";
          BindProductCategories();
         
           BindSlider();
           BindCart();
           // BindSchemes();
           Int32 totitems = new CartBLL().GetTotalItems(HttpContext.Current.Session.SessionID);
           Master.TotalItems = totitems;

        }
    }
    [WebMethod]
    public static string GetListDetail(int ListId)
    {

        Int32 Uid = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId] == null ? "0" : HttpContext.Current.Session[Constants.UserId].ToString());
        string lists = new ListBLL().GetProductsById(ListId);

        var JsonData = new
        {
            ProductDetail = lists

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string AddToCartList(Int32 ListId, string qty, string st, string type, bool KeepExisting)
    {


        string m_Status = "Plus";
        Int16 IsError = 0;
        int m_Qty = CommonFunctions.ValidateQuantity(qty);
        Calc objCalc = new Calc();
        UserCart objUserCart = new UserCart();
        string html = "";
        string ProductHTML = "";
        if (IsError == 0)
        {
            objUserCart.SessionId = HttpContext.Current.Session.SessionID;
            objUserCart.ProductId = 0;
            objUserCart.CustomerId = 0;
            objUserCart.ProductDesc = "";
            objUserCart.Qty = m_Qty;
            objUserCart.Price = 0;
            objUserCart.type = type;

            html = new CartBLL().UserCartListInsertUpdate(objUserCart, m_Status, objCalc, out  ProductHTML, ListId, KeepExisting);

        }
        var JsonData = new
        {
            cartHTML = html
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

    }
    [WebMethod]
    public static string CheckUser()
    {


        Int32 retVal = 0;
        string userid = HttpContext.Current.Session[Constants.UserId] == null ? "0" : HttpContext.Current.Session[Constants.UserId].ToString();
        if (userid == "0")
        {
            retVal = 0;
        }
        else
        {
            retVal = 1;
        }
        return retVal.ToString();
    }
    [WebMethod]
    public static string SaveList(String ListName)
    {
        Int32 Uid = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId] == null ? "0" : HttpContext.Current.Session[Constants.UserId].ToString());
        List objList = new List()
        {
            Title = ListName.ToUpper(),
            UserId = Uid,
        };
        string SessionId = HttpContext.Current.Session.SessionID;
        Int16 status = new ListDAL().InsertUpdate(objList, SessionId);


        return status.ToString();
    }
    [WebMethod]
    public static string Login(string MobileNo, string Password)
    {
        Users objuser = new Users()
        {
            MobileNo = MobileNo.Trim(),
            Password = Password.Trim()
        };
        string status = new UsersBLL().UserLoginCheck(objuser,HttpContext.Current.Session.SessionID);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            Status = status
        };
        if (status != "0" && status != "-1" && status != "-2")
        {
            HttpContext.Current.Session[Constants.UserId] = status;
            HttpContext.Current.Session[Constants.Email] = MobileNo.Trim();
            HttpContext.Current.Session["Roles"] = status;
        }
        return ser.Serialize(JsonData);

    }
    [WebMethod]
    public static string GetMyLists()
    {

        Int32 Uid = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId] == null ? "0" : HttpContext.Current.Session[Constants.UserId].ToString());
        string lists = new ListBLL().GetByUserId(Uid);

        var JsonData = new
        {
            MyLists = lists

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string GetComboProductsFrontEndHtml()
    {
        string categories = new ProductsBLL().GetComboProductsFrontEndHtml(HttpContext.Current.Session.SessionID);
        return categories;
    }
    [WebMethod]
    public static string BindBottomAdds(string Location, int Top)
    {

        string groupHTML = new MyGroupsBLL().GetAddsGroupHTMLByLocation(Location, Top);

        return groupHTML;
    }

    [WebMethod]
    public static string BindSideAdds(string Location, int Top)
    {

        string groupHTML = new MyGroupsBLL().GetSideAddsGroupHTMLByLocation(Location, Top);

        return groupHTML;
    }
    [WebMethod]
    public static string GetAddress()
    {
        Int32 retVal = 0;
        string Address = "";
        string city = HttpContext.Current.Request.Cookies["City"] == null ? "0" : HttpContext.Current.Request.Cookies["City"].Value.ToString();
        if (city == "0")
        {
            retVal = 0;
        }
        else
        {
            retVal = 1;
            Address = new PinCodeDAL().GetAddressById(Convert.ToInt32(city));
        }
        MessageCredentials objMsg = new MessageCredentialsDAL().GetAll();
        string m_OrderOnCall = objMsg.OrderOnCall;
        string m_CallUs = objMsg.CallUs;

        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {

            SAddress = Address,
            Status = retVal,
            OrderOnCall = m_OrderOnCall,
            CallUs = m_CallUs
        };
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string CheckAddress()
    {

        
        Int32 retVal = 0;
        string city = HttpContext.Current.Request.Cookies["City"] == null ? "0" : HttpContext.Current.Request.Cookies["City"].Value.ToString();
        if (city == "0")
        {
            retVal = 0;
        }
        else
        {
            retVal = 1;
        }
        return retVal.ToString();
    }
    [WebMethod]
    public static string RestoreAddress(int Id)
    {
        HttpContext.Current.Response.Cookies["City"].Value = Id.ToString();
        HttpContext.Current.Response.Cookies["City"].Expires = DateTime.Now.AddDays(30);

     string   Address = new PinCodeDAL().GetAddressById(Convert.ToInt32(Id));
     return Address;
    }
    [WebMethod]
    public static string GetTimeSlots()
    {


        string timeslots = new TimeSlotsBLL().GetTimeSlotsHTML();

        var JsonData = new
        {
            TSlots = timeslots

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string BindCities()
    {

        string Cities = new CitiesBLL ().GetActiveOptions();
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            CityList = Cities,
        };
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string BindPincodes(int CityId)
    {

        string Pincodes = new PinCodeDAL().GetAlloptions(CityId);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            PinCodeList = Pincodes,
        };
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string BindGroups()
    {

        string groupHTML = new MyGroupsBLL().GetGroupHTMLByLocation("HomeGuide");

        return groupHTML;
    }


    [WebMethod]
    public static string ValidateCheckOut()
    {
     string SessionId= HttpContext.Current.Session.SessionID;
     int retVal = new CartBLL().ValidateCheckout(SessionId);

        return retVal.ToString();
    }


    public void BindSchemes()
    {

        List<Schemes> schList = new SchemeBLL().GetActiveSchemes();
        string schemeData = "";
        foreach (var items in schList)
        {

            schemeData += "<tr><td><a href='promotions.aspx?sc=" + items.SchemeId + "'><img src='SchemeImages/T_" + items.PhotoUrl + "' class='img-responsive' style='width:100%;margin-bottom:5px'/></a></td></tr>";
           // schemeData += "<div class='col-md-4'><a href='promotions.aspx?sc=" + items.SchemeId + "'><img src='SchemeImages/T_" + items.PhotoUrl + "' class='img-responsive' alt=''/></a></div>";
        }
        //ltSchemeData.Text = schemeData;


    }

    [WebMethod]
    public static string FirstTimeATC(string vid, string qty, string st, string type)
    {
 

        string m_Status = "Plus";
      
        if (st == "m")
        {
            m_Status = "Minus";

        }

       

        Int16 IsError = 0;
        

   

        int m_Qty = CommonFunctions.ValidateQuantity(qty);
        if (!CommonFunctions.IsValidProductId(vid))
        {
            IsError = 1;
        }

        Calc objCalc = new Calc();
        UserCart objUserCart = new UserCart();
        string html = "";
        string ProductHTML = "";
        if (IsError == 0)
        {
            objUserCart.SessionId = HttpContext.Current.Session.SessionID;
            objUserCart.ProductId = 0;
            objUserCart.CustomerId = HttpContext.Current.Session[Constants.UserId] != null ? Convert.ToInt32(HttpContext.Current.Session[Constants.UserId]) : 0;
            objUserCart.VariationId = Convert.ToInt64(vid);
            objUserCart.ProductDesc = "";
            objUserCart.Qty = m_Qty;
            objUserCart.Price = 0;
            objUserCart.type = type;
        
            html = new CartBLL().UserCartInsertUpdate(objUserCart, m_Status, objCalc,out  ProductHTML);

        }
        var JsonData = new
        {
            
            qty = objUserCart.Qty,
            error = IsError,
            cartHTML = html,
            Calc = objCalc,
            productHTML = ProductHTML
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

    }
    [WebMethod]
    public static string GetFeaturedCategoriesAndProductsList()
    {
        string categories = new ProductsBLL().GetFeaturedCategoriesAndProductsList(HttpContext.Current.Session.SessionID);

        return categories;
    }
    [WebMethod]
    public static string GetFeaturedComboTypesAndCombos()
    {
        //string categories = new ProductsBLL().GetFeaturedComboTypesAndCombos(HttpContext.Current.Session.SessionID);
       // return categories;
        return " ";
    }
    [WebMethod]
    public static string GetCartHTML()
    {

        Calc objCalc = new Calc();
        string cart = new CartBLL().GetCartHTML(HttpContext.Current.Session.SessionID, objCalc);

        var JsonData = new
        {
            MCOA = objCalc.MinimumCheckOutAmt,
            FDA = objCalc.FreeDeliveryAmt,

            DC = objCalc.DeliveryCharges,
            ST = objCalc.SubTotal,
            NA = objCalc.NetAmount,
            html = cart,
            TotalItems = objCalc.TotalItems
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string GetComboDetail(string PId)
    {
        string[] arr_p = PId.Split('_');

        string ProductId = arr_p.Length > 1 ? Convert.ToString(arr_p[1]) : "";
        Combo objcombo = new Combo()
        {
            ComboId = Convert.ToInt32(ProductId)
        };
        var cart = new ComboBLL().GetProductDetailByID(objcombo);

        var JsonData = new
        {
            Cart=cart,
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    void BindCart()
    {
        // repCart.DataSource = new CartBLL().GetCartBySessionId(Session.SessionID);
        // repCart.DataBind();
    }
    void BindSlider()
    {
        List<Sliders> sliders = new SlidersBLL().GetAll();
        string strSliders = "";
        foreach (var i in sliders)
        {
            if (i.IsActive == true)
            {
                if (i.UrlName.ToString() != "")
                {
                    strSliders += "<div><a href='"+i.UrlName.Trim()  +"'><img u='image' src2='SliderImages/" + i.ImageUrl + "' /></a></div>";
                }
                else
                {
                    strSliders += "<div><img u='image' src2='SliderImages/" + i.ImageUrl + "' /></div>";
                }
            }
        }
        ltSlider.Text = strSliders;
   }

   
 
    void BindProductCategories()
    {

        // repMenuCategories.DataSource = new CategoryBLL().GetForFrontEnd();
        //   repMenuCategories.DataBind();

    }
    [WebMethod]
    public static string RemoveFromCart(string vid, string st, string type)
    {


        string m_Status = "Minus";

        Int16 IsError = 0;

        if (!CommonFunctions.IsValidProductId(vid))
        {
            IsError = 1;
        }

        Calc objCalc = new Calc();
        UserCart objUserCart = new UserCart();
        string html = "";
        string ProductHTML = "";
        if (IsError == 0)
        {
            objUserCart.SessionId = HttpContext.Current.Session.SessionID;
            objUserCart.VariationId = Convert.ToInt64(vid);
            objUserCart.type = type;

            html = new CartBLL().RemoveFromUserCart(objUserCart, objCalc, out  ProductHTML);

        }
        var JsonData = new
        {

            qty = objUserCart.Qty,
            error = IsError,
            cartHTML = html,
            Calc = objCalc,
            productHTML = ProductHTML
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

    }
}