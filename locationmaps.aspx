﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="locationmaps.aspx.cs" Inherits="backoffice_locationmaps" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
    <script type="text/javascript">
        var source, destination;
        var directionsDisplay;
        var directionsService = new google.maps.DirectionsService();
        google.maps.event.addDomListener(window, 'load', function () {
            new google.maps.places.SearchBox(document.getElementById('txtSource'));
            new google.maps.places.SearchBox(document.getElementById('txtDestination'));
            directionsDisplay = new google.maps.DirectionsRenderer({ 'draggable': true });
        });

        function GetRoute() {
            var mumbai = new google.maps.LatLng(18.9750, 72.8258);
            var mapOptions = {
                zoom: 7,
                center: mumbai
            };
            map = new google.maps.Map(document.getElementById('dvMap'), mapOptions);
            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById('dvPanel'));

            //*********DIRECTIONS AND ROUTE**********************//
            source = document.getElementById("txtSource").value;
            destination = document.getElementById("txtDestination").value;

            var request = {
                origin: source,
                destination: destination,
                travelMode: google.maps.TravelMode.DRIVING
            };
            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                }
            });

            //*********DISTANCE AND DURATION**********************//
            var service = new google.maps.DistanceMatrixService();
            service.getDistanceMatrix({
                origins: [source],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function (response, status) {
                if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
                    var distance = response.rows[0].elements[0].distance.text;
                    var duration = response.rows[0].elements[0].duration.text;
                    var dvDistance = document.getElementById("dvDistance");
                    dvDistance.innerHTML = "";
                    dvDistance.innerHTML += "Distance: " + distance + "<br />";
                    dvDistance.innerHTML += "Duration:" + duration;

                } else {
                    alert("Unable to find the distance via road.");
                }
            });
        }
</script>
</head>
<body>
    <form id="Action" runat ="server"> 
<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Find Your Location</span>
                     
                        <br />
                    </h3>
				   <div class="youhave" style="padding-left:30px">

 <div class="youhave" style="padding-left:30px">
<table border="0" cellpadding="0" cellspacing="3" width ="100%">
<tr>
    <td >Source:</td>
        <td>
        <input type="text" id="txtSource" value="Bandra, Mumbai, India" style="width: 200px" runat="server"  /></td>
        <td>Destination:</td>
        <td>
        <input type="text" id="txtDestination" value="Peshawari Super Market, Manimajra, Chandigarh, India" 
                style="width: 220px"  /></td>
        <td><input type="button" value="Get Route"  class="btn btn-primary btn-small" onclick="GetRoute()" /></td>
        
       <%--  <td><asp:Button ID="btn_getroute" runat="server" Text="Get Route"  
               class="btn btn-primary btn-small" onclick="GetRoute()"  />
               </td>--%>
               <td></td>

</tr>
<tr>
    
</tr>
<tr>
    <td colspan ="100">
      <%--  <div id="dvMap" style="width: 500px; height: 500px">--%>
        <div id="dvMap" style="width: 100%px; height: 500px">
        </div>
    </td>
    </tr>
    <tr>
    <td colspan ="2">
        <div id="dvPanel" style="width: 400px; height: 500px">
        </div>
    </td>
</tr>
</table>
</div>
</div>
</div>
</div>
</form> 
</body>
</html>
