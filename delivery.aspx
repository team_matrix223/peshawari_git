﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="delivery.aspx.cs" Inherits="delivery" %>
<%@ Register src="usercontrols/ucEnquiryForm.ascx" tagname="ucEnquiryForm" tagprefix="uc1" %>
<%@ Register Src="~/usercontrols/ucMenu.ascx" TagPrefix="uc1" TagName="ucMenu" %>
<%@ MasterType VirtualPath="main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnMaster" Runat="Server">
  <%--<script src="jquery-1.4.4.min.js" type="text/javascript"></script>--%>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="smartpaginator.js" type="text/javascript"></script>
    <link href="smartpaginator.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.uilocksearch.js" type="text/javascript"></script>



     <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <%--<script src="//code.jquery.com/jquery-1.10.2.js"></script>--%>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="css/jquery.notifyBar.css">
   <script src="jquery.notifyBar.js"></script>

    
         <link rel="stylesheet" href="styles.css">
 
    <link href="tabcontent.css" rel="stylesheet" type="text/css" />
   <script src="script.js"></script>

<script language="javascript" type="text/javascript">




    function showHide(val) {

       
        var divP = document.getElementById("view1");
        var divD = document.getElementById("view2");
       


        if (val == 1) {
            document.getElementById("ancV1").style.borderBottom = "solid 1px white";
            document.getElementById("ancV1").style.backgroundColor = "white";

            document.getElementById("ancV2").style.borderBottom = "solid 1px #B7B7B7";
            document.getElementById("ancV2").style.backgroundColor = "";


            divP.style.display = "block";
            divD.style.display = "none";
          



        }
        else if (val == 2) {
            document.getElementById("ancV1").style.borderBottom = "solid 1px #B7B7B7";
            document.getElementById("ancV1").style.backgroundColor = "";

            document.getElementById("ancV2").style.borderBottom = "solid 1px white";
            document.getElementById("ancV2").style.backgroundColor = "white";


            divP.style.display = "none";
            divD.style.display = "block";
          


        }
       



    }
     

    function ActiveOffer() {

        $("#dvOffers").fadeIn(500);
        $("html, body").animate({ scrollTop: 0 }, 500);
    
    }

    function BindOrderSummary() {
        $.ajax({
            type: "POST",
            data: '{}',
            url: "delivery.aspx/BindOrderSummary",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#ltItemList").html(obj.IL);
                $("#ltCartSubTotal").html(obj.ST);
                $("#ltCartDeliveryCharges").html(obj.DC);
                $("#ltCartDisAmount").html(obj.DA);
                $("#ltCartNetAmount").html(obj.NA);
                var FreeDelivery = obj.FRE;
                var SST = obj.ST;
                if (Number(obj.DA) == 0) {
                    $("#trDisAmt").css({ "display": "none" });
                }
                else {
                    $("#trDisAmt").css({ "display": "block" });
                }

                if (Number(SST) >= Number(FreeDelivery)) {
                  
                    $("#sp2").html("");
                   
                    $("#ltCartDeliveryCharges").html("");
                 

                    $("#ltCartNetAmount").html(Number(obj.NA) - Number(obj.DC));
                }


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {



                $.uiUnlock();

            }

        });
    }
    function ApplyVouher(coupon, status, DisCountType) {
        var GiftId = 0;
        var rates = "";
        if (DisCountType == "Gifts") {
            if (status == 1) {
                rates = document.getElementsByName('rbChooseGift');
            }
            else {

                rates = document.getElementsByName(coupon);
            }
            for (var i = 0; i < rates.length; i++) {
                if (rates[i].checked) {
                    GiftId = rates[i].value;
                }
            }

        }


        var CouponNo = "";
        if (status == 1) {

            if ($("#txtCouponCode").val().trim() == "") {
                $("#txtCouponCode").val('').focus();
                return;
            }
            CouponNo = $("#txtCouponCode").val();
        }
        else {
            CouponNo = coupon

        }

        $.uiLock('');


        $.ajax({
            type: "POST",
            data: '{"CouponNo":"' + CouponNo + '","GiftId":"' + GiftId + '"}',
            url: "delivery.aspx/ApplyCoupon",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                if (obj.Status == -1) {
                    alert(obj.Message);
                }
                else if (obj.Status == 1) {

                    $("#PesOffers").html(obj.OfferHtml);
                    if (status != 1) {
                        $("#dvOffers").fadeOut(500);

                    }
                }
                if (DisCountType != "Gifts") {
                    BindOrderSummary();
                }
                else {
                    $("#<%=lblGiftMsg.ClientID%>").text(obj.GiftMessage);
                }

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {
          

                BindActivePoints();

                $.uiUnlock();

            }

        });

    }


    function ApplyPoints(points) {

        if ($("#txtwallet").val().trim() == "") {
            $("#txtwallet").val('').focus();
            return;
        }

        if ($("#txtwallet").val() > points) {
            alert("Sorry! You cannot Redeem more than " + points + " Points");
            $("#txtwallet").val('').focus();
            return;
        }

        var wallet = $("#txtwallet").val();

        $.ajax({
            type: "POST",
            data: '{"DisAmt":"' + wallet + '"}',
            url: "delivery.aspx/ApplyOfferForPoints",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                if (obj.Status == 1) {
                    $("#dvpoints").html(obj.OfferHtml);

                }


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {



                BindActiveOffer();
                BindActivePoints();



                $.uiUnlock();

            }

        });


    }


    function ApplyVouherNew(coupon,status,DisCountType) {
       
    var CouponNo="";
    if(status==1) {

        if ($("#txtCouponCode").val().trim() == "") {
            $("#txtCouponCode").val('').focus();
            return;
        }
    CouponNo=$("#txtCouponCode").val();
    }
    else
    {
    CouponNo=coupon
 
    }

     $.uiLock('');


     $.ajax({
         type: "POST",
         data: '{"CouponNo":"' + CouponNo + '"}',
         url: "delivery.aspx/GetGifts",
         contentType: "application/json",
         dataType: "json",
         success: function (msg) {

             var obj = jQuery.parseJSON(msg.d);
            // alert(obj.GiftHtml);

             if (obj.DiscountType == "Gifts") {
                 $("#dvGifts").html(obj.GiftHtml);
                 $("#dvPopup").dialog({ modal: true, closeOnEscape: false, width: 700 });
                 // $(".ui-dialog-titlebar").hide();

             }
             else {
                 ApplyVouher(coupon,1,"");
             }



         }, error: function (xhr, ajaxOptions, thrownError) {

             var obj = jQuery.parseJSON(xhr.responseText);
             alert(obj.Message);
         },
         complete: function (msg) {

             BindActivePoints();

             $.uiUnlock();

         }

     });
    
    }


    function RemoveOfferPoint() {

      
        $.uiLock('');

        $.ajax({
            type: "POST",
            data: '{}',
            url: "delivery.aspx/RemovePointOffer",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);





                if (obj.Status == -1) {
                    alert("No such Offer associated with this Order");
                }
                else if (obj.Status == 1) {

                    $("#dvpoints").html(obj.OfferHtml);

                }
         
                BindOrderSummary();

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


                $.uiUnlock();

            }

        });






    }

    function RemoveVoucher(coupon) {

        $.uiLock('');

        $.ajax({
            type: "POST",
            data: '{"CouponNo":"' + coupon + '"}',
            url: "delivery.aspx/RemoveCoupon",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


               
            
              
                if (obj.Status == -1) {
                    alert("No such Coupon is associated with this Order");
                }
                else if (obj.Status == 1) {

                    $("#PesOffers").html(obj.OfferHtml);

                }
                $("#<%=lblGiftMsg.ClientID%>").text("");
                BindOrderSummary();

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


                $.uiUnlock();

            }

        });






    }
    $(document).on("change", "input[name='delivery']", function (event) {
        $("div.box5").css("background", "white");
        $(this).closest("div.box5").css("background", "#E6E8B5");

        $("#<%=txtDeliveryAddressId.ClientID %>").val($(this).val());


    });

    $(document).keydown(function (e) {
        // ESCAPE key pressed
        if (e.keyCode == 27) {
            $("#dvOffers").fadeOut(500);
        }
    });



    function BindActivePoints() {

        $.ajax({
            type: "POST",
            data: '{}',
            url: "delivery.aspx/BindActivePoints",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
              

                $("#dvpoints").html(obj.PointHtml);


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {



            }

        });

    }


    function BindActiveOffer() {

        $.ajax({
            type: "POST",
            data: '{}',
            url: "delivery.aspx/BindActiveOffer",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#PesOffers").html(obj.OfferHtml);
                $("#<%=lblGiftMsg.ClientID%>").text(obj.Gift);



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


                BindOrderSummary();


            }

        });

    }


    $(document).ready(
    function () {

        BindActiveOffer();
        BindActivePoints();
        BindOrderSummary();

        $("#btnChooseGift").click(
        function () {
            var dialogDiv = $('#dvPopup');
            dialogDiv.dialog("option", "position", [500, 200]);
            dialogDiv.dialog('close');

            ApplyVouher("", 1, "Gifts");
        }
        );

        $("#dvClose").click(
        function () {
            $("#dvOffers").fadeOut(500);
        }
        );



    }
    );
</script>

<style type="text/css">
.btnVoucher {
    border: 1px solid #ccc;
    border-radius: 5px;
    color: black;
   
    cursor: pointer;
    font-family: "Roboto Slab",sans-serif;
    font-size: 19px;
    margin: 15px 10px 9px;
    outline: 0 none;
    padding: 2px 15px 2px;
    width:90px;
    text-transform:uppercase;
   
    background:linear-gradient(to bottom, #fee67c 0px, #f2cb76 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)
}
</style>

<div class="row">
<div class="col-md-12">
<div class="col-md-12">
<div id="dvOffers" style="left:0px;display:none;position:absolute;z-index:9999; border-radius: 5px; border:solid 1px silver 
    ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%)
     repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:10px;z-index:999;border-bottom:0px;min-height:615px;right:0px">
<div style="width:100%;text-align:right;cursor:pointer;" id="dvClose">Close</div>
<asp:Literal ID="ltOffers" runat="server"></asp:Literal>
 

 

</div>

</div>


</div>

</div>

<div class="row">


<div class="col-sm-9">
<div class="row" style="margin-top:15px;margin-bottom:15px">

<div class="col-md-12" >
<div class="row">
<div class="col-xs-12"  >
 
</div>

</div>

<div  class="col-md-12" style=" border-radius: 1px; border:solid 1px silver ;padding:5px 5px 5px">
 
 <div class="row">
 <div class="col-md-12">
<span style="color:#58595b;font-size:16px;font-weight:bold;">DELIVERY INFORMATION</span>
 <hr  style="margin:2px;"/>

 <div id="dvDeliveryAddresses" >
 
  <asp:Literal ID="ltDeliveryAddress" runat="server"></asp:Literal>
 </div>
 
 

  

 </div>
 
 </div>

 


</div>
</div>

 

 


</div>
   
<div class="row" style="margin-top:15px;">
     
            <div class="col-md-4">
            </div>
                 
            <div class="col-md-4">
            <div id="dvPopup" style="display:none;border:solid 2px silver" >
  <table width="100%" cellpadding="3" >
                    

                     <tr>
                     <td valign="top" style="background-color:gray ;text-align:center;color:white;text-transform:uppercase;padding:3px;font-family:sans-serif" colspan="100%" >
                    <b> Choose Gift</b>
                    </td>
                    </tr>
                    <tr>
                   <td>
                   <div id="dvGifts">
                   </div>
                   </td></tr>
                    
               
                    <tr>
                    <td></td>
                    <td>
                    <button id="btnChooseGift" class="btn" title="Ok" style="color:white;background:gray;  " name="btnChooseGift" type="button"><span>Ok</span></button>
                    </td>
                    </tr>
                    </table>
                    </div>
            </div>
            <div class="col-md-4">
            </div>


</div>
<div class="row" style="margin-top:15px;margin-bottom:15px">

<div class="col-md-12" >
<div class="row">
<div class="col-xs-12"  >
 
</div>

</div>

<div  class="col-md-12" style=" border-radius: 1px; border:solid 1px silver ; padding:5px 5px 5px">
 
 <div class="row">
 <div class="col-md-12">
 <asp:TextBox ID="txtDeliveryAddressId" runat="server" Width="0" Height="0" style="border:0px"></asp:TextBox>
<span style="color:#58595b;font-size:16px;font-weight:bold;">CHOOSE DELIVERY SLOT</span>
 <hr  style="margin:2px;"/><br />
 <asp:UpdatePanel ID="upd1" runat="server">
      
                   
 <ContentTemplate>

 <div class="row">
 <div class="col-md-3"><b>Delivery Day:</b></div>
 <div class="col-md-3"> <asp:DropDownList ID="ddlDays" runat="server" CssClass="form-control" AutoPostBack="True" Width="100%" 
         onselectedindexchanged="ddlDays_SelectedIndexChanged"  ></asp:DropDownList></td><td style="padding-left:10px">
          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ForeColor="Red" 
 ControlToValidate="ddlDays"></asp:RequiredFieldValidator></div>
 <div class="col-md-2"><b>Time Slot:</b></div>
 <div class="col-md-3">   <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="upd1" runat="server">
                        <ProgressTemplate>
                            <div id="dvProgress" style="padding-top:5px;position: absolute; width: 200px; height: 35px; background-color: Black;
                                color: White; text-align: center; opacity: 0.7; vertical-align: middle">
                                loading please wait..
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
       

 <asp:DropDownList ID="ddlTimeSlot" runat="server"  CssClass="form-control" Width="100%"></asp:DropDownList>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ForeColor="Red" 
 ControlToValidate="ddlTimeSlot"></asp:RequiredFieldValidator>
 </div>
<%-- <div class="col-md-3"><b>Payment Mode:</b></div>--%>
<%-- <div class="col-md-3">--%>
<%-- <asp:DropDownList ID="ddlPaymentMode" runat="server" CssClass="form-control"  Width="100%">
 <asp:ListItem Text=""></asp:ListItem>
 <asp:ListItem Text="Cash On Delivery" Value="Cash On Delivery"></asp:ListItem>
  <asp:ListItem Text="Online Payment" Value="Online Payment"></asp:ListItem>
 </asp:DropDownList>--%>

<%--  <asp:RequiredFieldValidator ID="reqPaymentMode" runat="server" ErrorMessage="*" 
 ForeColor="Red" ControlToValidate="ddlPaymentMode"></asp:RequiredFieldValidator>--%>
<%-- </div>--%>
 
 </div>


  
 </ContentTemplate>
 </asp:UpdatePanel>
 
 
 

  

 </div>
 
 </div>

 


</div>
</div>

 

 


</div>



<div class="row" style="margin-top:15px;margin-bottom:15px">

<div class="col-md-12" >
<div class="row">
<div class="col-xs-12"  >
 
</div>

</div>

<div  class="col-md-12" style=" border-radius: 1px; border:solid 1px silver ; padding:5px 5px 5px">
 
 <div class="row">
 <div class="col-md-12" >
<span style="color:#58595b;font-size:16px;font-weight:bold;">PESHAWARI OFFERS</span>
 <hr  style="margin:2px;"/>
 

 
 
 
 
  

 </div>
 
 </div>

   <div id="addDialog">

    <ul class="tabs" data-persist="true">
            <li ><a id="ancV1"  style="background-color:White;border-bottom:solid 1px white;"  href="javascript:showHide(1,'ancV1');">Peshawari Points</a></li>
            <li ><a id="ancV2"  href="javascript:showHide(2,'ancV2');">E-Coupon Code</a></li>
             
         
            
             
        </ul>
 <div class="tabcontents">
            <div id="view1">
     

                  <div id="dvpoints">
 <div id="ltpoints"></div>
<%-- <asp:Literal ID="ltpoints" runat="server"></asp:Literal>--%>

 
 </div>


      <%--<div  class="row"><div class="col-md-12" style ="text-align:center;font-weight:bold;color:Red"> You Have 5 Peshawari Points</div></div>--%>

<%--        <div  class="row"><div class="col-md-12" style ="text-align:center;font-weight:bold">How many Peshawari Points do you wish to Redeem?</div></div>--%>

            <%--  <div  class="row" ><div class="col-md-3"></div><div class='col-md-9' style ="text-align:center"><table><tr><td><input type='text' id='txtCouponCode' class='form-control' style='background:white' /></td><td><div id='btnApplyCoupon' onclick='ApplyVouherNew(\"\",1)'  class='btnVoucher' style='margin:5px'>Apply</div></td> </tr></table></div></div>--%>





   
      </div>
       
            <div id="view2" style="display:none">


             <div id="PesOffers">
              <div id="ltPesOffers"></div>

<%-- <asp:Literal ID="ltPesOffers" runat="server"></asp:Literal>
--%>
 
 </div>

            

                        

            </div>


            
            </div>
            </div>

 


</div>
</div>

 

 


</div>
 



</div>


<div class="col-md-3">


<div style="border:solid 2px #EEEEEE">

<div id="dvTopBasket" style="border-bottom:solid 2px #d4d4d4;padding:6px">
<img src="images/basket.png" alt="" > <h6 style="float:right;margin-right:20px;font-size:16px">ORDER SUMMARY</h6>
 </div>
 
<div class="basketitem" >

 <div id="cartcontainer"  >
 <span ID="ltItemList"></span>
<%-- <asp:Literal ID="ltItemList" runat="server"></asp:Literal>--%>
 </div>
   
  
 
 <table>
 <tr>
 <td style="padding:10px 0px 0px 70px;">
 <table>
 <tr><td><h4><b>Sub Total: Rs.<span id="ltCartSubTotal"></span> </b></h4></td></tr>
 <tr>
 <td><h5><span id="sp2">Delivery Charges: Rs.</span>  <span id="ltCartDeliveryCharges"></span></h5></td>
 </tr>
  <tr id="trDisAmt">
 <td><h5 >DisAmt: Rs.<span id="ltCartDisAmount" ></span></h5></td>
 </tr>
 <tr>
 <td><h5 style="color:#f40f00;margin-left:10px">Total: Rs. <span id="ltCartNetAmount"></span></h5></td>
 </tr>
 <tr>
 <td style="padding:10px 0  20px;">

 
 </td>
 </tr>
 </table>
 </td>
 </tr>
 </table>
 
 
 
 <table style="margin-left:40px">
 <tr>
 <td>
  <a href="basket.aspx">
 <button type="button" class="btn btn-danger">Back</button>
 </a>
 </td>
 <td>
  <asp:Button ID="btnReviewOrder" runat="server" Text="Place Order"  
         CssClass="btn btn-danger" onclick="btnReviewOrder_Click" style="width:100px;margin:5px"/>
 
 </td>
 </tr>
 <tr>
 <td colspan="100%" >

 <asp:Label ID="lblGiftMsg" runat="server" style="background:White;font-weight:bold;font-size:20px"> </asp:Label></td></tr>
 </table>
 
<br />

 
 
 
 </div>
 
</div>



 
 
  

</div>


 </div>
         <div id="dvMenuCategories" style="position:absolute;top:153px;display:none">

<uc1:ucMenu ID="ucMenu1" runat="server" />

</div>
</asp:Content>

