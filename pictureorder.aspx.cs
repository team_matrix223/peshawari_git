﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;

public partial class pictureorder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    [WebMethod]
    public static string CheckUser()
    {


        Int32 retVal = 0;
        string userid = HttpContext.Current.Session[Constants.UserId] == null ? "0" : HttpContext.Current.Session[Constants.UserId].ToString();
        if (userid == "0")
        {
            retVal = 0;
        }
        else
        {
            retVal = 1;
        }
        return retVal.ToString();
    }


    [WebMethod]
    public static string Login(string MobileNo, string Password)
    {
        Users objuser = new Users()
        {
            MobileNo = MobileNo.Trim(),
            Password = Password.Trim()
        };
        string status = new UsersBLL().UserLoginCheck(objuser,HttpContext.Current.Session.SessionID);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            Status = status
        };
        if (status != "0" && status != "-1" && status != "-2")
        {
            HttpContext.Current.Session[Constants.UserId] = status;
            HttpContext.Current.Session[Constants.Email] = MobileNo.Trim();
            HttpContext.Current.Session["Roles"] = status;
        }
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string BindSlots(string SelectedDay)
    {
        string ggg = "";
        // string Date = Convert.ToDateTime(SelectedDay).ToShortDateString() + " ";
        TimeSlots objTimeSlots = new TimeSlots();


        bool IsAvailable = false;
        DateTime dd;
        dd = Convert.ToDateTime(DateTime.Now.ToString("d"));

        string curday = dd.DayOfWeek.ToString();
        // string[] slctday = SelectedDay.Split(','); 
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.Append(string.Format("<option value={0}>{1}</option>", "", ""));

        if (curday != SelectedDay)
        {

            ggg = new TimeSlotsBLL().GetAllSlotsOptions();


        }
        else
        {

            ggg = new TimeSlotsBLL().GetSlotsoption();
        }

        var JsonData = new
        {
            Slots = ggg,
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);


    }


    [WebMethod]
    public static string BindDeliveryAddress()
    {
        Int32 Uid = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId] == null ? "0" : HttpContext.Current.Session[Constants.UserId].ToString());
        Int64 DeliveryAddressId = 0;
        string groupHTML = new DeliveryAddressBLL().GetHtmlByUserId(Uid, out DeliveryAddressId);

        return groupHTML;
    }


    [WebMethod]
    public static string BindDays()
    {

        ListItem li = new ListItem();
        li.Text = "";
        li.Value = "";


        DateTime dd;
        DateTime totaldd;
        dd = Convert.ToDateTime(DateTime.Now.ToString("d"));
        totaldd = dd.AddDays(7);
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.Append(string.Format("<option value={0}>{1}</option>", "", ""));


        while (dd != totaldd)
        {
            li = new ListItem();
            li.Text = dd.ToLongDateString();
            li.Value = dd.DayOfWeek.ToString();
            if (li.Value != "Sunday")
            {

                strBuilder.Append(string.Format("<option value={0}>{1}</option>", dd.DayOfWeek.ToString(), dd.ToLongDateString()));



            }
            dd = dd.AddDays(1);

        }
        var JsonData = new
        {
            ddd = strBuilder.ToString()

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string InsertPictureOrder(string PhotoUrl, Int32 DeliveryAddress, string DeliveryDay, Int32 DeliverySlot)
    {
        Int32 uId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        PictureOrders objPictureOrder = new PictureOrders()
        {
            UserId = uId,
            DeliveryAddressId = Convert.ToInt32(DeliveryAddress),
            DeliveryDate = Convert.ToDateTime(DeliveryDay),
            DeliverySlot = DeliverySlot,
            PhotoUrl = PhotoUrl,
        };


       


        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new PictureOrderBLL().InsertPictureOrder(objPictureOrder);
        var JsonData = new
        {
            Picture = status,

        };
        return ser.Serialize(JsonData);
    }

    
}