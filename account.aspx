﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="account.aspx.cs" Inherits="account" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<script src="js/bootstrap.min.js" type="text/javascript" ></script>
<script src="js/jquery.min.js" type="text/javascript" ></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
     <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-ui.js"></script>
     <link rel="stylesheet" type="text/css" href="css/jquery-uiJQGrid.css" />  
     <script src="../js/customValidation.js"></script>
<title>Index</title>
    <script language="javascript" >



        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                alert("Please Enter Only Numeric Value:");
                return false;
            }

            return true;
        }
        $(document).on("click", "div[name='dvpincode']", function (event) {
            var data = $(this).attr("id");
            var arrData = data.split('_');
            var vid = arrData[1];
            $("#txtPinCode").val(vid);
            $("#result").css({ "display": "none" });
        });
    </script>
</head>
   
<body>

<form runat="server">


<div class="container-fluid">
<div class="row" style="background:black;">
<div class="col-xs-12">
<div class="menu">
        <ul>
        <li><a href="dashboard.aspx">My Account</a></li>
      
        <li><a href="../basket.aspx">Checkout</a></li>
        </ul>
      </div>
</div>
</div>

<div class="row">
    <div class="col-md-9">
 <a href="../index.aspx">    <img src="../images/logo.png" alt="" style="width:200px" /></a>
    <%--<img src="images/logo.jpg" alt="" />--%>
    </div>
    
    <div class="col-md-3">
    <div class="phone" style="width:350px">
                <ul>
                        <h3>
                          <b style="font-size:22px ">  Order on call:</b></h3>
                        <li><a href="tel:8699099035" style="color:red "><b style="font-size:22px ">869-9099-035</b> </a></li>
                    </ul>
                </div>
      
      <div class="item">
         <table>
 <tr>
 <td><img src="images/shopping-cart.png" alt="" /></td>
 <td><h5><a href="../basket.aspx" style="color: #6ba52e">MY CART</a></h5>
</td>
 </tr>
 
 </table>
   
      </div>
      
    </div>
  </div>
 
 
         <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar">
                        </span>
                    </button>
                    <a class="navbar-brand" style="color: #000000;" href="../index.aspx">Home</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <asp:Literal ID="ltFeaturedCategories" runat="server"></asp:Literal>
                        
                        <asp:Repeater ID="repHeaderLinks" runat="server">
                        <ItemTemplate>
                        <li class="active"><a href="../info.aspx?pid=<%#Eval("PageId") %>"> <%#Eval("Title") %></a></li>
                        
                        </ItemTemplate>
                        </asp:Repeater>

                         <%if (Session[Constants.UserId] == null)
                          { %>
                        <li><a href="account.aspx" style="color: #000000">JOIN FREE</a></li>
                        <li><a href="account.aspx" style="color: #000000">SIGN IN</a></li>
                        <%}
                          else
                          { %>
                        <li><a href="dashboard.aspx" style="color: #000000">MY ACCOUNT</a></li>
                        <li><a href="signout.aspx" style="color: #000000">SIGN OUT</a></li>
                        <%} %>
                    </ul>
                </div>
            </div>
        </nav>
 
<!--Row Start-->
<%--<div class="row">
<div class="col-sm-3 border ">
<div class="freedelivery">
 <table >
 <tr>
 <td><img src="images/free-delivery.png" alt="" /></td>
 <td><h2>FREE DELIVERY</h2>
  <p style="font-size:12px">On order Value Above $250</p></td>
 </tr>
 </table>
 
 
 </div>
</div>

<div class="col-sm-3 border ">
<div class="freedelivery">
 <table >
 <tr>
 <td><img src="images/calender.png" alt="" /></td>
 <td><h2>FREE DELIVERY</h2>
  <p style="font-size:12px">On order Value Above $250</p></td>
 </tr>
 </table>
 
 </div>
</div>



<div class="col-sm-3 ">
<div class="freedelivery">
 <table >
 <tr>
 <td><img src="images/money.png" alt="" /></td>
 <td><h2>FREE DELIVERY</h2>
  <p style="font-size:12px">On order Value Above $250</p></td>
 </tr>
 </table>
 
 
 </div>
</div>


<div class="col-sm-3">
<div class="callus">
 <table>
 <tr>
 <td><img src="images/phonepic.png" alt="" /></td>
 <td><h2>CALL US : + 91-8699099035</h2>
  <p style="font-size:12px; color:#FFFFFF;">Need Help Call Us</p></td>
 </tr>
 </table>
 
 
 </div>
</div>
 
</div>--%>
<!--Row End-->


 


<!--Row Start-->
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
   <ContentTemplate>
<div class="row" style="margin-top:15px;margin-bottom:15px" >

<div class="col-md-12" >
<div  class="col-md-10" style="margin-left:8%;margin-right:8%;border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:25px 20px 60px">
<div class="row">
<div class="col-md-6">

<table style="width:100%"   >
<tr>
<td colspan="100%"  >
<h4>LOGIN PANEL</h4>
<hr />
 
 
</td>
</tr>
 
<tr>
<td class="heading">User Name:</td><td><asp:TextBox ID="txtName" class="inputtext" runat="server" style="padding-left:10px"></asp:TextBox></td><td style="padding-left: 10px">
    <asp:RequiredFieldValidator ID="reqUName" runat="server" ControlToValidate="txtName" ErrorMessage="*" ForeColor="Red" ValidationGroup ="a">
    </asp:RequiredFieldValidator>
        </td>
                                                                                                                 
</tr>
 


<tr>
<td class="heading">Password:</td><td><asp:TextBox ID="txtPassword" TextMode="Password"  class="inputtext" runat="server" ></asp:TextBox></td>
    <td style="padding-left: 10px">
     <asp:RequiredFieldValidator ID="reqPswd" runat="server" ControlToValidate="txtPassword" ErrorMessage="*" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                                  </td>
</tr>

 <tr><td>
  
                    
                       </td><td  >
               
       <asp:Button ID="btnLogin_Click"    runat="server" Text="Login" ValidationGroup="a"
                       class="btnYellow" OnClick="btnLogin_Click_Click"/>
                       
                        
                        <%-- <asp:LinkButton ID="btnForgetpswd" runat="server" Text="Forgot password?" CausesValidation="false" OnClick="btnForgetpswd_Click" 
                             /> --%>
                   
                    </td><td>
                           &nbsp;</td></tr>
                           <tr>
                           <td colspan="100%" align="center">
                            <b><asp:Label ID="lblLogin" runat="server" Text=""  ForeColor="Red"></asp:Label></b>
                           </td>
                           
                           </tr>

                           <tr>
                           <td colspan="100%">
                           <div style="padding:5px;margin-top:50px;border:solid 1px silver;border-radius:5px; background:none repeat scroll 0 0 #f4f4f4">

                           <h3>New to Our Website? Register</h3><div class="uiv2-why-register"><p class="why">Why register?</p><ul class="uiv2-mar-t-10"><li>- Wide selection of products</li><li>- Quality and service you'll love</li><li>- On time delivery guarantee</li><li>- Shop on the go from Android and iOS devices</li></ul></div>
                           </div>                           
                           </td>
                           
                           </tr>
 
</table>

<br />
 


</div>


<div class="col-md-6">

<table style="width:100%"   >
<tr>
<td colspan="100%"  >
<h4>NEW ACCOUNT REGISTRATION</h4>
<hr />
 
 
</td>
</tr>

<tr>
<td class="headingStyle" colspan="100%">PERSONAL DETAILS</td>
</tr>
<tr><td colspan="100%">&nbsp;</td></tr>
<tr>
<td class="heading">First Name:</td><td><asp:TextBox ID="txtFirstName" class="inputtext" runat="server" ></asp:TextBox></td>
    <td style="padding-left: 10px"><asp:RequiredFieldValidator ID="req_FirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="*" ForeColor="#CC0000" ValidationGroup="b"></asp:RequiredFieldValidator>
                         </td>
</tr>

<tr>
<td class="heading">Last Name:</td><td><asp:TextBox ID="txtLastName" class="inputtext" runat="server" ></asp:TextBox></td><td></td>
</tr>
     <tr>
<td class="heading">MobileNo: +91</td><td><asp:TextBox ID="txtUserMobile" class="inputtext" onkeypress="return isNumberKey(event)" MaxLength="10" runat="server" ></asp:TextBox></td>
         <td style="padding-left: 10px"> <asp:RegularExpressionValidator ID="RequiredFieldValidator4"   ValidationExpression="^\d+$" runat="server" ControlToValidate="txtUserMobile" ErrorMessage="*" ForeColor="#CC0000"></asp:RegularExpressionValidator>
</td>

</tr>
<tr>
<td class="heading">Email Id(UserName):</td><td><asp:TextBox ID="txtEmailId" class="inputtext" runat="server"  ></asp:TextBox></td>
    <td style="padding-left: 10px">                                 <asp:RequiredFieldValidator ID="req_Email" runat="server" ControlToValidate="txtEmailId" ErrorMessage="*" ForeColor="#CC0000" ValidationGroup="b"></asp:RequiredFieldValidator>
                                  <asp:RegularExpressionValidator ID="RegExp_Email" runat="server" ControlToValidate="txtEmailId" ErrorMessage="Enter Valid EmailID" ValidationGroup="b" Font-Bold="False" ForeColor="#CC0000" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                  </td>
</tr>

<tr>
<td class="heading">Password:</td><td><asp:TextBox ID="txtPswd" class="inputtext" runat="server" TextMode="Password"  ></asp:TextBox></td>
    <td style="padding-left: 10px"><asp:RequiredFieldValidator ID="req_Password" runat="server" ControlToValidate="txtPswd" ErrorMessage="*" ForeColor="#CC0000"  ValidationGroup="b"></asp:RequiredFieldValidator>
</td>
</tr>

<tr>
<td class="heading">Confirm Password:</td><td><asp:TextBox ID="txtCPassword" class="inputtext" runat="server"  TextMode="Password"></asp:TextBox></td>
    <td style="padding-left: 10px"> <asp:RequiredFieldValidator ID="req_CPassword" runat="server" ControlToValidate="txtCPassword" ErrorMessage="*" ForeColor="#CC0000" ValidationGroup="b"></asp:RequiredFieldValidator>
      <asp:CompareValidator ID="Comp_Password" runat="server" ControlToCompare="txtPswd" ControlToValidate="txtCPassword" ValidationGroup="b" ErrorMessage="These passwords don't match. Try again?" ForeColor="#CC0000"></asp:CompareValidator>
</td>
</tr>
<tr><td colspan="100%">&nbsp;</td></tr>
<tr>
<td  class="headingStyle" colspan="100%">ADDRESS INFORMATION</td>
</tr>

<tr>
<td class="heading" style="width:150px">Recipient FirstName:</td><td><asp:TextBox ID="txtRFirstName" class="inputtext" runat="server"  ValidationGroup="b"></asp:TextBox></td>
    <td style="padding-left: 10px"> <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="b" ControlToValidate="txtRFirstName" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
</td>
</tr>

<tr>
<td class="heading">Recipient LastName:</td><td><asp:TextBox ID="txtRLastName" class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
    <tr>
<td class="heading">MobileNo: +91</td><td><asp:TextBox ID="txtMobileNo" class="inputtext" onkeypress="return isNumberKey(event)" MaxLength="10" runat="server" ></asp:TextBox></td>
         <td style="padding-left: 10px"> 
             <asp:RequiredFieldValidator ID="RequiredFieldValidator2"   ValidationGroup="b" runat="server" ControlToValidate="txtMobileNo" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
</td>

</tr>
     <tr>
<td class="heading">Telephone:</td><td><asp:TextBox ID="txtTelephone" class="inputtext" onkeypress="return isNumberKey(event)"  runat="server"></asp:TextBox></td><td></td>
</tr>
    
    <tr>
<td class="heading">Area:</td><td><asp:TextBox ID="txtArea" class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
    <tr>
<td class="heading">HNo/Street:</td><td><asp:TextBox ID="txtStreet" class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
        <tr>
<td class="heading">Address:</td><td><asp:TextBox ID="txtAddress" TextMode="MultiLine"  class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
<tr>
<td class="heading">City:</td><td><asp:DropDownList ID="ddlCities" runat="server"  style="height:30px;margin:5px" AutoPostBack="true" 
            onselectedindexchanged="ddlCities_SelectedIndexChanged"></asp:DropDownList></td>
        <td style="padding-left: 10px"> 
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="b" ControlToValidate="ddlCities" InitialValue="--Select City--" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>

                                                                                                          </td>
</tr>
 <tr>
<td class="heading">Sector:</td><td>
     <asp:DropDownList ID="ddlSector" runat="server" style="height:30px;margin:5px" AutoPostBack="true" onselectedindexchanged="ddlSector_SelectedIndexChanged" 
           ></asp:DropDownList>
            <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server" >
                        <ProgressTemplate>
                            <div id="dvProgress" style="position: absolute; width: 150px; height: 30px; background-color: Black;
                                color: White; text-align: center; opacity: 0.7; vertical-align: middle">
                                loading please wait..
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
           </td>
        <td style="padding-left: 10px"> 
            <asp:RequiredFieldValidator ID="ReqSector" runat="server" ValidationGroup="b" ControlToValidate="ddlSector" InitialValue="--Select Sector--" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
 
                                                                                                          </td>
</tr>
        <tr>
<td class="heading">PinCode:</td><td><asp:TextBox ID="txtPinCode" class="inputtext" runat="server" ReadOnly="true"></asp:TextBox>
    
     
                                 </td>  <td style="padding-left: 10px"> 
             <asp:RequiredFieldValidator ID="RequiredFieldValidator5"   ValidationGroup="b" runat="server" ControlToValidate="txtPinCode" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>

</td>
</tr>

    <tr><td></td><td style="padding-left:5px">
                   <div id="result" style="position:absolute;z-index:9999;background:white;max-height:400px;overflow-y:scroll;display:none;width:400px">
   <div id="tbKeywordSearch" style="border:solid 1px silver;border-top:0px;text-align:center " >
  
 
 
 
   </div>
        
    </div>
        </td></tr>
<tr>
<td></td>

<td>
<asp:Button ID="btnSubmit" class="btnGray" runat="server" Text="Register" OnClick="btnSubmit_Click" ValidationGroup="b" />

</td>
</tr>
<tr>
<td colspan="100%" align="center"  >
    <b><asp:Label ID="lblMsg" runat="server" Text=""  ForeColor="Red"></asp:Label></b>
</td></tr>
</table>
   
<br />
<br />

 
</div>

</div>


</div>
</div>

 

 


</div>
</ContentTemplate>
 </asp:UpdatePanel>
<!--Row End-->


<!--Row Start-->
 
 

<!--Row Fluid Start-->


 <div class="row" style="background: #1F1F1F no-repeat; margin-top:5px; padding-bottom: 10px">
            <div class="col-md-4">
                <div class="link">
                    <h2>
                        Categories</h2>
                    <ul>
                        <asp:Repeater ID="repCategories" runat="server">
                            <ItemTemplate>
                                <li>
                                   <a href="../list.aspx?c=<%#Eval("CategoryId") %>" style='color:white' > <%#Eval("Title") %></a> </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </div>
            <asp:Repeater ID="repOuterLinks" runat="server">
                <ItemTemplate>
                    <div class="col-md-4">
                        <div class="link">
                            <h2>
                                <%#Eval("Title") %></h2>
                            <ul>
                                <asp:Repeater ID="repInnerLinks" runat="server" DataSource='<%#GetInnerLinks(Eval("PageId")) %>'>
                                    <ItemTemplate>
                                        <li>
                                         <a style="color:White" href='../info.aspx?pid=<%#Eval("PageId") %>'><%#Eval("Title") %></a>   </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <div class="col-md-4">
                <div class="link">
                    <h2>
                        Contact</h2>
                    <ul>
                        <li><a href="01725005544" style="color:white "> 0172-5005544</a></li>
                        
                       
                    </ul>
                </div>
            </div>
        </div>
        <!--Row Fluid End-->
        <!-footer End--->
    </div>
    </form>
</body>

<script src="js/bootstrap.min.js" type="text/javascript"></script>
<link href="css/sm-core-css.css" rel="stylesheet" type="text/css" />

<link href="css/sm-mint/sm-mint.css" rel="stylesheet" type="text/css" />

<%--<script type="text/javascript" src="libs/jquery-loader.js"></script>--%>
 
<script type="text/javascript" src="jquery.smartmenus.js"></script>
 
<script type="text/javascript">
    $(function () {
        $('#main-menu').smartmenus({
            mainMenuSubOffsetX: -3,
            mainMenuSubOffsetY: -8,
            subMenusSubOffsetX: 1,
            subMenusSubOffsetY: -8,
            subMenusMinWidth: 700

        });

    });
</script>


 <script type="text/javascript">


     $("#main-menu").css("display", "block");
     //
     // $('#element').donetyping(callback[, timeout=1000])
     // Fires callback when a user has finished typing. This is determined by the time elapsed
     // since the last keystroke and timeout parameter or the blur event--whichever comes first.
     //   @callback: function to be called when even triggers
     //   @timeout:  (default=1000) timeout, in ms, to to wait before triggering event if not
     //              caused by blur.
     // Requires jQuery 1.7+
     //
     ; (function ($) {
         $.fn.extend({
             donetyping: function (callback, timeout) {
                 timeout = timeout || 1e3; // 1 second default timeout
                 var timeoutReference,
                doneTyping = function (el) {
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
                 return this.each(function (i, el) {
                     var $el = $(el);
                     // Chrome Fix (Use keyup over keypress to detect backspace)
                     // thank you @palerdot
                     $el.is(':input') && $el.on('keyup keypress', function (e) {
                         // This catches the backspace button in chrome, but also prevents
                         // the event from triggering too premptively. Without this line,
                         // using tab/shift+tab will make the focused element fire the callback.
                         if (e.type == 'keyup' && e.keyCode != 8) return;

                         // Check if timeout has been set. If it has, "reset" the clock and
                         // start over again.
                         if (timeoutReference) clearTimeout(timeoutReference);
                         timeoutReference = setTimeout(function () {
                             // if we made it here, our timeout has elapsed. Fire the
                             // callback
                             doneTyping(el);
                         }, timeout);
                     }).on('blur', function () {
                         // If we can, fire the event since we're leaving the field
                         doneTyping(el);
                     });
                 });
             }
         });
     })(jQuery);

     $('#txtSearch').donetyping(function () {
         // $('#example-output').text('Event last fired @ ' + (new Date().toUTCString()));


         var Keyword = $(this).val();

         if (Keyword.length >= 3) {

             $("#result").css("display", "block");
             $("#tbKeywordSearch").html("<img src='images/searchloader.gif'>");

             $.ajax({
                 type: "POST",
                 data: '{"Keyword":"' + Keyword + '"}',
                 url: "productsearch.aspx/KeywordSearch",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {
                     // var obj = jQuery.parseJSON(msg.d);


                     var Content = msg.d;
                     $("#tbKeywordSearch").html(Content);
                     if (Content.length < 10) {

                         $("#tbKeywordSearch").html('No record found for the Search');
                     }







                 }, error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function (msg) {


                 }

             });














         }
         else {


             $("#result").css("display", "none");


         }



     });
    </script>

 

 
<style type="text/css">
	#main-menu {
		position:relative;
		z-index:9999;
		width:100%;
		margin-top:5px;
		background:black;
	}
	#main-menu ul {
		width:12em;  
	}
</style>

</html>
