﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="test.aspx.cs" Inherits="test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 

<head runat="server">
    <title></title>
    
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>

    
    <script src="js/jquery-1.10.2.js"  type="text/javascript"></script>
    <script src="js/jquery-ui.js"  type="text/javascript"></script>
   
 
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   


 
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>


    <style type="text/css">
    .active
    {
         background:orange;
    border:solid 1px gray;
    padding:3px;
    margin:2px;
   cursor:pointer;
    text-decoration:none;
     margin-bottom:15px;
    
    }
        
    .paging
    {
    background:silver;
    border:solid 1px gray;
    padding:3px;
    margin:2px;
    cursor:pointer;
    text-decoration:none;
    margin-bottom:15px;
    
    
        
    }
    
    </style>




    <script language="javascript">

        var PageSize = 25;
        var PageNo = 1;
        var VID = 0;
        var UpdateStatus = 0;
        function Printt(PageNumber, PageSize, Cat2) {
       

            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
            var iframe = document.getElementById('reportout');
            iframe = document.createElement("iframe");
            iframe.setAttribute("id", "reportout");
            iframe.style.width = 0 + "px";
            iframe.style.height = 0 + "px";
            document.body.appendChild(iframe);
            //document.getElementById('reportout').contentWindow.location = "Reports/RptBilling.aspx?BillNowPrefix=" + celValue;
            //document.getElementById('reportout').contentWindow.location = "Reports/rptproducts.aspx?PageNumber=" + PageNumber + "&PageSize=" + PageSize;
            window.location = "Backoffice/Reports/rptproducts.aspx?PageNumber=" + PageNumber + "&PageSize=" + PageSize + "&Cat2=" + Cat2;
            $.uiUnlock();

        }

        function CheckRecord(Id) {
            VID = Id;
            var Code = $("#txtItemCode_" + Id).val();

            $.ajax({
                type: "POST",
                data: '{ "ItemCode": "' + Code + '","VariationId":"' + Id + '"}',
                url: "test.aspx/GetProductDetail",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                 
                    $("#sp_ProductDetailLocal").html(obj.HL);
                    if (obj.LStatus == 0) {
                        $("#btnOk").css({ "display": "none" });
                        $("#btnCancel").css({ "display": "none" });
                    }
                    else {
                        if (obj.OStatus == 1) {
                            $("#btnOk").css({ "display": "block" });
                            $("#btnCancel").css({ "display": "block" });
                        }
                        else {
                            $("#btnOk").css({ "display": "none" });
                            $("#btnCancel").css({ "display": "none" });
                        }
                    }
                    $("#sp_ProductDetail").html(obj.HTML);
                    $(this).val("");
                    $(this).focus();
                    $("#dvPopup").dialog({ modal: true, closeOnEscape: false, width: 500 });
                    $(".ui-dialog-titlebar").hide();

                },
                complete: function (msg) {

                }

            });
         
        }
        function UpdateRecord(Id) {

          
                       
                        var VariationId = Id;


                        var Code = $("#txtItemCode_" + VariationId).val();
                        var Price = $("#txtPrice_" + VariationId).val();
                        var Mrp = $("#txtMrp_" + VariationId).val();

                        $.ajax({
                            type: "POST",
                            data: '{ "ItemCode": "' + Code + '","Price":"' + Price + '","Mrp":"' + Mrp + '","VariationId":"' + VariationId + '"}',
                            url: "test.aspx/UpdateVariationDetail",
                            contentType: "application/json",
                            dataType: "json",
                            success: function (msg) {

                                var obj = jQuery.parseJSON(msg.d);

                                if (obj.Status == 1) {
                                    alert("Information is updated");
                                    return;
                                }
                                else {
                                    alert("Entered ItemCode is already allotted");
                                    return;
                                }

                            },
                            complete: function (msg) {

                            }

                        });


                
         
//            var VariationId = Id;
//            var Code = $("#txtItemCode_" + VariationId).val();
//            var Price = $("#txtPrice_" + VariationId).val();
//            var Mrp = $("#txtMrp_" + VariationId).val();

//            $.ajax({
//                type: "POST",
//                data: '{ "ItemCode": "' + Code + '","Price":"' + Price + '","Mrp":"' + Mrp + '","VariationId":"' + VariationId + '"}',
//                url: "test.aspx/UpdateVariationDetail",
//                contentType: "application/json",
//                dataType: "json",
//                success: function (msg) {

//                    var obj = jQuery.parseJSON(msg.d);

//                    if (obj.Status == 1) {
//                        alert("Information is updated");
//                        return;
//                    }
//                    else {
//                        alert("Entered ItemCode is already allotted");
//                        return;
//                    }

//                },
//                complete: function (msg) {

//                }

//            });

        }
 
        $(document).keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '27') {
                var dialogDiv1 = $('#dvPopup');
                dialogDiv1.dialog("option", "position", [500, 200]);
                dialogDiv1.dialog('close');
               
            }

        });

        $(document).on("keypress", "input[name='pitemcode']", function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {

                var pid = $(this).attr("id");
                var arrPid = pid.split('_');

                var VariationId = arrPid[1];
                VID = VariationId;
                CheckRecord(VariationId);

                return;


            }
        });

        function GetData(PageNumber, PageSize,Cat2) {
            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
            PageNo = PageNumber;
            $.ajax({
                type: "POST",
                data: '{ "PageNumber": "' + PageNumber + '","PageSize":"' + PageSize + '","Cat2":"' + Cat2 + '"}',
                url: "test.aspx/GetData",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    $("#dvProducts").html(obj.HTML);
                    $("#dvPages").html("");
                    var TotalRows = obj.TR;
                    var pages = Number(TotalRows) / Number(PageSize);
                    for (var i = 0; i < pages; i++) {
                        var PageNumber = i + 1;
                        var cls = "paging";
                        if ((i + 1) == PageNo) {
                            cls = "active";
                        }
                        var Cat2 = $("#<%=ddlCat2.ClientID%>").val();
                        $("#dvPages").append("<div style='margin-bottom:8px;float:left'><a style='text-decoration:none;' class='" + cls + "' href='javascript:GetData(" + PageNumber + "," + PageSize + ","+ Cat2 +")'><b>" + PageNumber + " <b/></a></div>");
                    }

                },
                complete: function (msg) {
                    $.uiUnlock();
                }

            });


        }
        function BindCat2() {
            $.ajax({
                type: "POST",
                data: '',
                url: "test.aspx/BindCat2",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    $("#<%=ddlCat2.ClientID%>").html(obj.Category2);


                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {
                    $.uiUnlock();

                }

            });
        }
        $(document).ready(function () {
            BindCat2();
            $("#<%=ddlCat2.ClientID %>").change(
    function () {
        var Cat2 = $("#<%=ddlCat2.ClientID%>").val();
       
        if ($.trim(Cat2) != "") {
            GetData(1, PageSize, Cat2);
        }
    });
            //GetData(1, PageSize);
            $("#dvclose").click(function () {

                var dialogDiv1 = $('#dvPopup');
                dialogDiv1.dialog("option", "position", [500, 200]);
                dialogDiv1.dialog('close');
            });


            $("#dvPrint").click(function () {
                var Cat2 = $("#<%=ddlCat2.ClientID%>").val();
                Printt(PageNo, PageSize, Cat2);
            });
            $("#btnOk").click(function () {

                UpdateRecord(VID);
                var dialogDiv1 = $('#dvPopup');
                dialogDiv1.dialog("option", "position", [500, 200]);
                dialogDiv1.dialog('close');
            });
            $("#btnCancel").click(function () {

                var dialogDiv1 = $('#dvPopup');
                dialogDiv1.dialog("option", "position", [500, 200]);
                dialogDiv1.dialog('close');
            });
        });
       
    </script>
</head>
<body>
<iframe id="reportout" width="0" height="0"   onload="processingComplete()"></iframe>
    <form id="form1" runat="server">
    <div>
    <table>
      <tr><td class="headings" style="padding-bottom:20px" colspan="100%"><table><tr>
      <td ><b>Choose CategoryLevel1:</b></td><td >  
                         <asp:DropDownList ID="ddlCat2" runat="server"></asp:DropDownList>
                  
                     </td></tr></table></td></tr>
    <tr><td colspan="100%">
    <table class="table">
    <tbody  id="dvProducts">
    
    </tbody>
    
    </table>

    </td></tr>
   <tr><td colspan="100%"><div id="dvPages"></div></td></tr>
   <tr><td colspan="100%" align="center"><div id="dvPrint" style="cursor:pointer" class="btn btn-primary btn-small"><b>Print</b></div></td></tr>

   <tr><td colspan="100%">
      <div id="dvPopup" style="display:none;background-color:Black;color:White" >
  <table width="100%" cellpadding="3" >
                    
                  
                     <tr>
                     <td valign="top" style="text-align:center " colspan="100%" >
                     
                    <h3 style="padding-bottom: 10px">Product Detail</h3>
                  
                    </td>
                    <td valign="top">  <div id="dvclose" style="cursor:pointer"><b>Close</b></div></td>
                    </tr>
                  
                    <tr>
                    <td style="padding-bottom:10px" align="center">
                   <span id="sp_ProductDetail" style="float:left;padding-left:30px"></span>
                  
                    </td></tr>
               <tr>
                    <td style="padding-bottom:10px" align="center">
                   <span id="sp_ProductDetailLocal" style="float:left;padding-left:30px"></span>
                  
                    </td></tr>
                    <tr><td colspan="100%" style="float:right;padding-left:20px"><table><tr>
                    <td style="padding-right:20px">
                      <div id="btnOk" class="btn btn-primary btn-small" style="width:80px;display:none">Update</div>
                    </td>
                    <td>
                  
                    <div id="btnCancel" class="btn btn-primary btn-small" style="width:80px">Cancel</div>
                    </td>
                    </tr>
                    </table></td></tr>
                    </table>
                    </div>
   </td></tr>
    
    </table>

    </div>
    </form>
</body>
</html>
