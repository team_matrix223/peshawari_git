﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="list.aspx.cs" Inherits="list" %>
 
<%@ MasterType VirtualPath="main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnMaster" Runat="Server">
  
     <%--<script src="jquery-1.4.4.min.js" type="text/javascript"></script>--%>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="smartpaginator.js" type="text/javascript"></script>
    <link href="smartpaginator.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.uilocksearch.js" type="text/javascript"></script>



     <link rel="stylesheet" href="jquery-ui.css">
  <%--<script src="//code.jquery.com/jquery-1.10.2.js"></script>--%>
  <script src="jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="css/jquery.notifyBar.css">
   <script src="jquery.notifyBar.js"></script>

    
         <link rel="stylesheet" href="styles.css">
   <%--<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>--%>
   <script src="script.js"></script>

<style type="text/css">
  
   

#facet-loading {
    background: none repeat scroll 0 0 #fff;
    border: 4px double #ccc;
    border-radius: 12px;
    float: left;
    margin-left: 280px;
    margin-top: 200px;
    opacity: 0.9;
    padding: 10px;
    position: absolute;
    z-index: 999;
   
}
#facet-loading span {
    font: 400 17px/23px Lato,Arial,Helvetica,sans-serif;
    padding: 10px;
    vertical-align: middle;
}
#facet-loading img {
    vertical-align: middle;
}
#facet-loading-mask {
    background-color: rgba(61, 61, 61, 0.3);
    height: 100% !important;
    left: 0;
    margin: 0 auto;
    position: absolute;
    top: 0;
    width: 100% !important;
    z-index: 998;
}
#facet-products-wrapper {
    position: relative;
}


</style>

  
 

<script language="javascript" type="text/javascript">
    var pageIndex = 1;
    var pageCount = 1;
    var busy = false;

    $(window).scroll(function () {


        if ($(window).scrollTop() + $(window).height() > $("#productlist").height() && !busy) {

            busy = true;

            GetRecords();
        }
    });

    function ShowHideFilter() {

        $("#dvMobileFilter").slideToggle(500);
    }



    function ChangeCategory(l1, l2, l3)
     {
       $("#<%=hdnLevel1.ClientID%>").val(l1);
        $("#<%=hdnLevel2.ClientID%>").val(l2);
        $("#<%=hdnLevel3.ClientID%>").val(l3);
        $("#<%=hdnBrandId.ClientID%>").val(0);
         $("#<%=hdnGroupId.ClientID%>").val(0);
         $("#<%=hdnSubGroupId.ClientID%>").val(0);


        Search("", 1, 1);
     }

    function GetRecords() {


        if (pageIndex != 1 && pageIndex <= pageCount) {


            var selValues = $("input[name='brandfilter']:checked").map(function () {
                return $(this).val();
            }).get();
            Search(selValues, 3, pageIndex);
        }
    }



    var m_MinPrice = -1;
    var m_MaxPrice = -1;
    var selValues = "";
    $(document).ready(function () {
 BindCart();

        Search("", 1, 1);
        $("#btnCheckOut").click(
function () {
    var totalAmt = Number($("#hdTotalAmt").val());
    var minAmt = Number($("#hdMinimumCheckOutAmt").val());
    if (totalAmt < minAmt) {
        $("#sp_msg").html("* Order Amount should be greater than " + minAmt);
        return false;
    }
    else {



        $.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/ValidateCheckOut",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                if (msg.d == "1") {
                    var url = "basket.aspx";
                    $(location).attr('href', url);
                }
                else {
                    $("#sp_msg").html("* Insufficient Order Amount for Checkout");
                }



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });






    }
});
});


    $(document).on("change", "input[name='brandfilter']", function (event) {

         selValues = $("input[name='brandfilter']:checked").map(function () {
            return $(this).val();
        }).get();


        Search(selValues,2,1) 


    });




    $(document).on("change", "select[name='variation']", function (event) {

        var pid = $(this).attr("id");
        var oldSelVal = $(this).attr("selvar");
        var arrPid = pid.split('_');
        var fPid = arrPid[1];
        var vid = $(this).val();


        $(this).find("option[value='" + oldSelVal + "']").prop("selected", true);


        $("div[name='product_" + fPid + "']").css("display", "none");
       
        $("#variation_" + vid).css("display", "block");






    });



    $(document).on("click", "div[name='decr']", function (event) {
        var data = $(this).attr("id");


        var arrData = data.split('_');
        var vid = arrData[1];
        $("#cqty_" + vid).css("height", "20px");

//        $("#cqty_" + vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");


        var tempQty = $("#cqty_" + vid).html();
        var tempQ = Number(tempQty) - 1;
        if (tempQ <= 1) {
            tempQ = 1;
        }
        $("#cqty_" + vid).html(tempQ);


        var qty = 1;
        var st = "m";
        var type = "Product";
        ATC(vid, qty, st, type, false);
    });


    $(document).on("click", "div[name='incr']", function (event) {


        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = 1;
        var st = "p";
        var type = "Product";
        $("#cqty_" + vid).css("height", "20px");

        //        $("#cqty_" + vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");



        var tempQty = $("#cqty_" + vid).html();
        var tempQ = Number(tempQty) + 1;
         
        $("#cqty_" + vid).html(tempQ);

        ATC(vid, qty, st, type,false);



    });

    $(document).on("click", "div[name='dvClose']", function (event) {

        var vid = $(this).attr("id");
        var st = "m";
        var type = "Combo";
        DFC(vid, st, type);



    });
    function DFC(vid, st, type) {
        $.ajax({
            type: "POST",
            data: '{"vid":"' + vid + '","st":"' + st + '","type":"' + type + '"}',
            url: "index.aspx/RemoveFromCart",
            async: false,
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);

                if (obj.error == 1) {

                    alert("An Error occured during transaction. Please refresh the page and try again Later.");
                    return;
                }


         
     
                $("#variation_" + vid).html(obj.productHTML);
                BindCart();
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });

    }


    function ATC(vid, qty, st, type,isfirsttime) {
        $.ajax({
            type: "POST",
            data: '{"vid":"' + vid + '","qty":"' + qty + '","st":"' + st + '","type":"' + type + '"}',
            url: "index.aspx/FirstTimeATC",
            async: true,
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);

                if (isfirsttime) {

                    $.notifyBar({ cssClass: "success", html: "Product Added Succcessfully!" });
                }

                if (obj.error == 1) {

                    alert("An Error occured during transaction. Please refresh the page and try again Later.");
                    return;
                }


                if (type == "Combo") {

                    $("#cartcontainer").html(obj.cartHTML);

                    $("#cbox_" + obj.pid).html(obj.cartHTML);


                    if (obj.qty <= 0) {

                        $("#cbox_" + obj.pid).css("background", "white")

                    }
                    else {

                        $("#cbox_" + obj.pid).css("background", "#D8EDC0")

                    }



                }
                else {


                    //$("#cartcontainer").html(obj.cartHTML);


                    $("#variation_" + vid).html(obj.productHTML);


                    //                    if (obj.qty <= 0) {

                    //                        $("#box_" + obj.pid).css("background", "white")

                    //                    }
                    //                    else {

                    //                        $("#box_" + obj.pid).css("background", "#D8EDC0")

                    //                    }

                }


                //$("#cqty_" + obj.pid).html(totalQ);


                BindCart();
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });

    }

    $(document).on("click", "button[name='btnAddToCart']", function (event) {

        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = $("#q_" + arrData[1]).val();
        var st = "p";
        var type = "Product";

        $("#a_" + vid).css("width", "55px");
        $("#a_" + vid).html("<img src='images/loaderadd.gif' style='margin-top:2px'   alt='.....'/>");



        ATC(vid, qty, st, type, true);
       

    });


    $(document).on("click", "div[name='cartminus']", function (event) {

        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = 1;
        var st = "m";
        var type = "Product";
        $("#cq_" + vid).css("height", "20px");

        $("#cq_" + vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");

        ATC(vid, qty, st, type, false);

    });

    $(document).on("click", "div[name='cartadd']", function (event) {



        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = 1;
        var st = "p";
        var type = "Product";
        $("#cq_" + vid).css("height", "20px");

        $("#cq_" + vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");

        ATC(vid, qty, st, type,false);


    });



    function BindCart() {

        $.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/GetCartHTML",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);

                $("#cartcontainer").html(obj.html);
                $("#sp_msg").html("");
                $("#hdMinimumCheckOutAmt").val(obj.MCOA);
                $("#subtotal").html(obj.ST);
                $("#hdTotalAmt").val(obj.ST);
               
                $("#delivery").html(obj.DC);
                $("#netamount").html(obj.NA);
                $("#sp_TotalItems").html(obj.TotalItems);
                $("#sp_TotalItems1").html("MY CART(" + obj.TotalItems + ")");
                if (obj.ST == 0) {

                    $("#msg").html("Cart is Empty");
                    $("#subtotal").html("");
                    $("#delivery").html("");
                    $("#netamount").html("");
                    $("#sp1").html("");
                    $("#sp2").html("");
                    $("#sp3").html("");
                    $("#btnCheckOut").hide();

                }
                else {
                    $("#msg").html("");
                    $("#sp1").html("Sub Total: Rs.");
                    $("#sp2").html("Delivery Charges: Rs.");
                    $("#sp3").html("Total: Rs.");
                    $("#btnCheckOut").show();
                    if (Number(obj.ST) >= Number(obj.FDA)) {

                        $("#sp2").html("");
                        $("#delivery").html("");
                        $("#netamount").html($("#hdTotalAmt").val());
                    }
                }


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

            }

        });
    }


    function Search(Brands,type,pageid) {


        if (type == 1 || type == 2) {
            $("#facet-loading-wrap").css("display", "block");

        }
        else {
            $("#loader").show();
            $("#loader").css("display", "block");

        } 
    
        var l1 = $("#<%=hdnLevel1.ClientID%>").val();
        var l2 = $("#<%=hdnLevel2.ClientID%>").val();
        var l3 = $("#<%=hdnLevel3.ClientID%>").val();
       
        var BID = $("#<%=hdnBrandId.ClientID%>").val();
        var GID = $("#<%=hdnGroupId.ClientID%>").val();
        var SGID = $("#<%=hdnSubGroupId.ClientID%>").val();


        $.ajax({
            type: "POST",
            data: '{"Brands":"' + Brands + '","Level1":"' + l1 + '","Level2":"' + l2 + '","Level3":"' + l3 + '","PageId":"' + pageid + '","MinPrice":"' + m_MinPrice + '","MaxPrice":"' + m_MaxPrice + '","BrandId":"' + BID + '","GroupId":"' + GID + '","SubGroupId":"' + SGID + '"}',
            url: "list.aspx/AdvancedSearch",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);
                $("#imgloading").remove();

                if (pageIndex >= 2 && pageIndex <= pageCount) {

                    $("#productlist").append("<h3 style='margin-left:15px;'>Page" + pageIndex + "</h3>");
                }
                if (type == 3) {
                    $("#productlist").append(obj.ProductData);
                }
                else {


                    pageIndex = 1;
                    $("#productlist").html(obj.ProductData);
                }


                if (type == 1) {



                    $("#brandlist").html(obj.BrandData);
                    $("#brandlist2").html(obj.BrandData);

                    $("#CatTitle").html(obj.CatTitle);
                    $("#CatDesc").html(obj.CatDesc);

                    $("#categorylist").html(obj.CategoryData);
                    $("#categorylist2").html(obj.CategoryData);

                    var min = obj.MinP;
                    var max = obj.MaxP;
                    $(function () {
                        $("#slider-range").slider({
                            range: true,
                            min: min,
                            max: max,
                            values: [min, max],
                            slide: function (event, ui) {
                                $("#amount").val("र " + ui.values[0] + " -  र " + ui.values[1]);
                            },
                            stop: function (event, ui) {
                                var curVal = ui.value;
                                m_MinPrice = ui.values[0];
                                m_MaxPrice=ui.values[1];

                                Search(selValues, 2, 1) 
                            }
                        });
                        $("#amount").val("र " + $("#slider-range").slider("values", 0) +
      " - र " + $("#slider-range").slider("values", 1));
                   
                   //------------------------------------------------

                        $("#slider-range2").slider({
                            range: true,
                            min: min,
                            max: max,
                            values: [min, max],
                            slide: function (event, ui) {
                                $("#amount").val("र " + ui.values[0] + " -  र " + ui.values[1]);
                            },
                            stop: function (event, ui) {
                                var curVal = ui.value;
                                m_MinPrice = ui.values[0];
                                m_MaxPrice = ui.values[1];

                                Search(selValues, 2, 1)
                            }
                        });
                        $("#amount2").val("र " + $("#slider-range2").slider("values", 0) +
      " - र " + $("#slider-range2").slider("values", 1));

                   //------------------------------------------------
                   
                    });

                }

                var totRec = obj.TotalRecords;
                pageCount = Math.ceil(totRec / 16);

       


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

                busy = false;
                pageIndex++;
                if (type == 1 || type == 2) {
                    $("#facet-loading-wrap").css("display", "none");
                    $("html, body").animate({ scrollTop: 0 }, 800);
                }
                $("#loader").hide();
                //$("html, body").animate({ scrollTop: 0 }, 800);


            }

        });

    }


  

 

  


 


</script>

  <script type="text/javascript" src="jquery.sticky.js"></script>
  <script>
      $(window).load(function () {
          $("#sticker").sticky({ topSpacing: 0, center: true, className: "hey" });
      });
  </script>

<input type="hidden" id="hdMinimumCheckOutAmt" />
     
      <input type="hidden" id="hdTotalAmt" />
 
<div class="row">
<div class="col-md-12">
<div class="filterRecords" id="dvFRecords" onclick="javascript:ShowHideFilter();">
FILTER RECORDS
</div>
</div>
</div>

<div class="row">
<div class="col-md-3">

 
<div class="yourbasket">

 


<div id="dvTopBasket" style="border-bottom:solid 2px #d4d4d4;;padding:6px">

 
<img src="images/basket.png" alt="" > <h6 style="float:right;margin-right:0px;font-size:16px">Your Basket (<span id="sp_TotalItems"></span> items)</h6>
 </div>
 
<div class="basketitem" >

 <div id="cartcontainer" style="max-height:200px;overflow-y:scroll">
 </div>
   
  
 
 <table>
 <tr>
 <td style="padding:10px 0px 0px 70px;">
 <table>
    <tr ><td><h3 style="color:red"><b><span id="msg"></span> </b></h3> </td></tr>
 <tr><td><h4><b><span id="sp1">Sub Total: Rs.</span> <span id="subtotal"></span></b></h4></td></tr>
 <tr>
 <td><h5><span id="sp2">Delivery Charges: Rs.</span>  <span id="delivery"></span></h5></td>
 </tr>
 <tr>
 <td><h5 style="color:#f40f00;margin-left:10px"><span id="sp3">Total: Rs.</span><span id="netamount"></span></h5></td>
 </tr>
 <tr>
 <td style="padding:10px 0 0 50px;">
 <%--<a href="basket.aspx" id="ancAdd">--%>
 <button type="button" class="btn btn-success" id="btnCheckOut">Checkout</button>
 <%--</a>--%>
 
 </td>
 </tr>
 </table>
 </td>
 </tr>
 </table>
 
 
 
 
 
<br />

 
 
 
 </div>
 <span id="sp_msg" style="color:red;font-size:10px;padding-left:5px"></span>
</div>

 <br />
 <br />


 <div id="sticker" class="pcFilter">
 
<div class="brands"  >
  <a href="#" style="color:#4e8425; font-size:16px; padding:10px 0 0 10px;font-weight:bold ">RELATED CATEGORIES</a>
        
   <ul style="padding:0px;max-height:200px;overflow-y:scroll" id="categorylist">
   

    </ul>
        
    </div>
<br />
 
  
  

 
<div class="price" >
            <a href="#" style="color:#4e8425; font-size:16px; padding:10px 0 0 10px;font-weight:bold">BRANDS</a>
             <ul style="padding:0px;max-height:200px;overflow-y:scroll" id="brandlist" >
   

    </ul>
  
        
    </div>
 
<br />
 <div class="price" >
            <a href="#" style="color:#4e8425; font-size:16px; padding:10px 0 0 10px;font-weight:bold">PRICE RANGE</a>
    <p style="padding:10px 0 5px 0">
  
  <input type="text" id="amount" readonly style="border:0; color:BLACK; font-weight:bold;FONT-WEIGHT: NORMAL">
</p>
 
<div id="slider-range"></div>
  <br />
  <br />
<br><br><br><br><br><br><br><br><br><br><br><br>

        
    </div>
 </div> 


 <div id="dvMobileFilter" class="mobileFilter">
 
<div class="brands"  >
  <a href="#" style="color:#4e8425; font-size:16px; padding:10px 0 0 10px;font-weight:bold ">RELATED CATEGORIES</a>
        
   <ul style="padding:0px;max-height:200px;overflow-y:scroll" id="categorylist2">
   

    </ul>
        
    </div>
<br />
 
  
  

 
<div class="price" >
            <a href="#" style="color:#4e8425; font-size:16px; padding:10px 0 0 10px;font-weight:bold">BRANDS</a>
             <ul style="padding:0px;max-height:200px;overflow-y:scroll" id="brandlist2" >
   

    </ul>
  
        
    </div>
 
<br />
 <div class="price" >
            <a href="#" style="color:#4e8425; font-size:16px; padding:10px 0 0 10px;font-weight:bold">PRICE RANGE</a>
    <p style="padding:10px 0 5px 0">
  
  <input type="text" id="amount2" readonly style="border:0; color:BLACK; font-weight:bold;FONT-WEIGHT: NORMAL">
</p>
 
<div id="slider-range2"></div>
  <br />
  <br />
 

        
    </div>
 </div>


</div>


<div class="col-sm-9">

<div class="vegetables">
<div class="line">
<span style="position: relative; top:-0.6em;font-size:15px; font-family:Verdana, Geneva, sans-serif; color:#ef1619; padding-left: 15px;" id="CatTitle">
    
</span>
<p id="CatDesc">
 </p>

<asp:HiddenField ID="hdnLevel1" runat="server" />
<asp:HiddenField ID="hdnLevel2" runat="server" />
<asp:HiddenField ID="hdnLevel3" runat="server" />
<asp:HiddenField ID="hdnBrandId" runat="server" />
<asp:HiddenField ID="hdnGroupId" runat="server" />
<asp:HiddenField ID="hdnSubGroupId" runat="server" />

</div>
</div>


<div class="row">
<div class="col-md-12"  >
<table style="width:100%">
<tr>
<td>

<div id="facet-loading-wrap" style="display:none"><div id="facet-loading"><img src="images/loading_brown.gif"><span>Loading...</span></div><div id="facet-loading-mask"></div></div>

 <div id="productlist" style="clear:both">
 </div>
 
</td>


</tr>

<tr>
<td align="center">
 <img id="loader" alt="" src="loading.gif" style="display: none" />
<%--<div id="red" style="margin: auto;padding:5px">
            </div>--%>

</td>
</tr>
</table>

            

 


 


 


 
</div>


 


 


 




</div>


 


<!--Fruit End-->
 
</div>




 
 </div>
</asp:Content>

