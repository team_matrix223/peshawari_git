﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class delivery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Constants.UserId] == null)
        {
            Response.Redirect("user/account.aspx?ReturnUrl=Delivery");
        }


        if (!IsPostBack)
        {


            BindDeliveryAddress();
            BindDays();
           // BindOrderSummary();
            BindOffers();
            
          
            //BindActivePoints();
           // BindActiveOffer();
            Int32 totitems = new CartBLL().GetTotalItems(HttpContext.Current.Session.SessionID);
            Master.TotalItems = totitems;
        }
    }

    [WebMethod]
    public static string GetGifts(string CouponNo)
    {
        string DisType = "";
        string html = new OffersBLL().GetGiftsHTMLByCouponNo(CouponNo,out DisType );
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new 
        {
            GiftHtml = html,
            DiscountType=DisType 
        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string BindActivePoints()
    {

        Settings objSetting = new Settings();
        objSetting = new SettingsDAL().GetSett();
        Wallet objwallet = new Wallet();
        {
            objwallet.UserId = Convert.ToInt16(HttpContext.Current.Session[Constants.UserId]);
        }

        new WalletBLL().GetWalletStatusCart(objwallet,HttpContext.Current.Session.SessionID);
        string ltpoints = "";
        Int64 Points = 0;
        Points = (Convert.ToInt64(objwallet.Total) - Convert.ToInt64(objwallet.Used));
        if (objwallet.Status == "-1")
        {


            ltpoints = "<div  class='row'><div class='col-md-12' style ='text-align:center;font-weight:bold;color:Red'> You Have " + Points + " Peshawari Points.1Point = Rs " + objSetting.PointRate + ".</div></div>";
            ltpoints += " <div  class='row'><div class='col-md-12' style ='text-align:center;font-weight:bold'>How many Peshawari Points do you wish to Redeem?</div></div>";
            ltpoints += " <div  class='row' ><div class='col-md-3'></div><div class='col-md-9' style ='text-align:center'><table><tr><td><input type='text' id='txtwallet' class='form-control' style='background:white' /></td><td><div id='btnApplyPoints' onclick='javascript:ApplyPoints(" + Points + ");'  class='btnVoucher' style='margin:5px'>Apply</div></td> </tr></table></div></div>";

        }
        else if (objwallet.Status == "1")
        {

            ltpoints = "<div class='row'><div class='col-md-4'>";


            ltpoints += " <div style='border:dashed 1px silver;padding:24px;width:225px;'> " +
    " <b style='font-size:20px'>Peshawari Point</b><br><span style='font-weight:bold;font-size:24px'> " + " " + objwallet.Points + "Points</span><br/><span style='color:green'> Applied Successfully</span></div></div>";

            ltpoints += "<div class='col-md-8'>";

            ltpoints += "<p style='font-weight:bold;color:green;font-size: 20px; '>" +
    " PESHAWARI POINTS DISCOUNT OFFER</p><p style='font-size:20px;'> Congratulations,You Got Rs. " + objwallet.DisAmt + " Discount on Your Bill. " +
    "   </p><hr style='margin:5px'><table width='100%'> " +
    " <tbody><tr><td><div id='btnRemoveVoucher1' class='btnVoucher' onclick='RemoveOfferPoint()' style='margin-top:0px;width:119px'>Remove </div> " +
    " </td></tr></tbody></table>";


            ltpoints += "</div></div>";
        }
            JavaScriptSerializer ser = new JavaScriptSerializer();
            var JsonData = new
            {
                PointHtml = ltpoints,
               
            };
            return ser.Serialize(JsonData);
        


    }

    [WebMethod]
    public static string BindActiveOffer()
    {

        OfferStatus obj = new OfferStatus();
        obj = new OffersBLL().GetActiveOfferForUser(HttpContext.Current.Session.SessionID, Convert.ToInt64(HttpContext.Current.Session[Constants.UserId]));
        string ltPesOffers = "";
        string Gift = "";
        if (obj.Status == -1)
        {

   
             ltPesOffers = "<div class='row'><div class='col-md-3'><b>e-Coupon Code:</b></div><div class='col-md-6'><table><tr><td><input type='text' id='txtCouponCode' class='form-control' style='background:white' /></td><td><div id='btnApplyCoupon' onclick='ApplyVouherNew(\"\",1)'  class='btnVoucher' style='margin:5px'>Apply</div></td></tr></table></div>";
 
            ltPesOffers += "<div class='col-md-3'><div id='btnViewActiveOffers' onclick='ActiveOffer()' style='cursor:pointer'><img src='images/btnSpecialOffer.png' style='width:150px'/></div></div></div>";
            ltPesOffers += "<div class='row'><div class='col-md-12'><span style='color:Red'>*</span > A maximum of one voucher is applicable for an order. </div></div>";




          
        }
        else
        {


            ltPesOffers = "<div class='row'><div class='col-md-4'>";


            ltPesOffers += " <div style='border:dashed 1px silver;padding:24px;width:225px;'> " +
" <b style='font-size:20px'>e-Coupon Code</b><br><span style='font-weight:bold;font-size:24px'> " +
" " + obj.CouponNo + "</span><br/><span style='color:green'> Applied Successfully</span></div></div>";

            ltPesOffers += "<div class='col-md-8'>";

            ltPesOffers += "<p style='font-weight:bold;color:green;font-size: 20px; '>" +
" " + obj.Title + "</p><p style='font-size:20px;'>" + obj.Description + " " +
"   </p><hr style='margin:5px'><table width='100%'> " +
" <tbody><tr><td><div id='btnRemoveVoucher' class='btnVoucher' onclick='RemoveVoucher(\"" + obj.CouponNo + "\")' style='margin-top:0px;width:119px'>Remove </div> " +
" </td></tr></tbody></table>";


            ltPesOffers += "</div></div>";
          
            if (obj.GiftName  !="")
            {
                Gift = "Congratulation:You got " + obj.GiftName + "";
            }




        }

        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            OfferHtml = ltPesOffers,
            Gift =  Gift,
        };
        return ser.Serialize(JsonData);
    
    }

    [WebMethod]
    public static string ApplyCoupon(string CouponNo, Int32 GiftId)
    {
        string GiftMsg = "";
        OfferStatus obj = new OfferStatus();
        obj = new OffersBLL().ApplyOffer(CouponNo, HttpContext.Current.Session.SessionID, Convert.ToInt64(HttpContext.Current.Session[Constants.UserId]),GiftId);

//        string offerHtml = "<table style='margin-bottom:20px'><tbody><tr><td valign='top' " +
//" style='padding-top:5px'><div style='border:dashed 1px silver;padding:24px;width:225px;'> " +
//" <b style='font-size:20px'>e-Coupon Code</b><br><span style='font-weight:bold;font-size:24px'> " +
//" " + obj.CouponNo + "</span><span style='color:green'> Applied Successfully</span></div></td><td valign='top' style='padding:5px'><p style='font-weight:bold;color:green;font-size: 20px; '> " +
//" "+obj.Title+"</p><p style='font-size:20px;'>"+obj.Description+" " +
//"   </p><hr style='margin:5px'><table width='100%'> " +
//" <tbody><tr><td><div id='btnRemoveVoucher' class='btnVoucher' onclick='RemoveVoucher(\""+obj.CouponNo+"\")' style='margin-top:0px;width:119px'>Remove </div> " +
//" </td></tr></tbody></table></td></tr></tbody> " +
//" </table>";
        string offerHtml = "<div class='row'><div class='col-md-4'>";


        offerHtml += " <div style='border:dashed 1px silver;padding:24px;width:225px;'> " +
" <b style='font-size:20px'>e-Coupon Code</b><br><span style='font-weight:bold;font-size:24px'> " +
" " + obj.CouponNo + "</span><br/><span style='color:green'> Applied Successfully</span></div></div>";

        offerHtml += "<div class='col-md-8'>";

        offerHtml += "<p style='font-weight:bold;color:green;font-size: 20px; '>" +
" " + obj.Title + "</p><p style='font-size:20px;'>" + obj.Description + " " +
"   </p><hr style='margin:5px'><table width='100%'> " +
" <tbody><tr><td><div id='btnRemoveVoucher' class='btnVoucher' onclick='RemoveVoucher(\"" + obj.CouponNo + "\")' style='margin-top:0px;width:119px'>Remove </div> " +
" </td></tr></tbody></table>";


        offerHtml += "</div></div>";
        if (GiftId != 0)
        {
            GiftMsg = "Congratulation:You got "+ obj.GiftName +"";
        }
       
        var JsonData = new
        {
            Status = obj.Status,
            Message = obj.Message,
            OfferHtml = offerHtml,
            GiftMessage=GiftMsg
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }






    [WebMethod]
    public static string ApplyOfferForPoints(decimal DisAmt)
    {
        decimal DisVal = 0;
     
        Settings objSetting = new Settings();
        objSetting = new SettingsDAL().GetSett();
        DisVal = (DisAmt * objSetting.PointRate);
        string GiftMsg = "";
        int Status = new OffersBLL().ApplyOfferForPoints(HttpContext.Current.Session.SessionID, Convert.ToInt64(HttpContext.Current.Session[Constants.UserId]), DisAmt);
        string offerHtml = "<div class='row'><div class='col-md-4'>";


        offerHtml += " <div style='border:dashed 1px silver;padding:24px;width:225px;'> " +
" <b style='font-size:20px'>Peshawari Point</b><br><span style='font-weight:bold;font-size:24px'> " + " " + DisAmt + "Points</span><br/><span style='color:green'> Applied Successfully</span></div></div>";

        offerHtml += "<div class='col-md-8'>";

        offerHtml += "<p style='font-weight:bold;color:green;font-size: 20px; '>" +
" PESHAWARI POINTS DISCOUNT OFFER</p><p style='font-size:20px;'> Congratulations,You Got Rs." + DisVal + "  Discount  on Your BIll. " +
"   </p><hr style='margin:5px'><table width='100%'> " +
" <tbody><tr><td><div id='btnRemoveVoucher' class='btnVoucher' onclick='RemoveOfferPoint()' style='margin-top:0px;width:119px'>Remove </div> " +
" </td></tr></tbody></table>";


        offerHtml += "</div></div>";



        var JsonData = new
        {
            Status = Status,
           OfferHtml=offerHtml
      
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string RemovePointOffer()
    {


        int Status = new OffersBLL().RemovePointOffer(HttpContext.Current.Session.SessionID);
        Settings objSetting = new Settings();
        objSetting = new SettingsDAL().GetSett();
        Wallet objwallet = new Wallet();
        {
            objwallet.UserId = Convert.ToInt16(HttpContext.Current.Session[Constants.UserId]);
        }

        new WalletBLL().GetBalanceByUser(objwallet);

        Int64 Points = 0;
        Points = (Convert.ToInt64(objwallet.Total) - Convert.ToInt64(objwallet.Used));


        string offerHtml = "<div  class='row'><div class='col-md-12' style ='text-align:center;font-weight:bold;color:Red'> You Have " + Points + " Peshawari Points.1Point = Rs " + objSetting.PointRate + ".</div></div>";
        offerHtml+= " <div  class='row'><div class='col-md-12' style ='text-align:center;font-weight:bold'>How many Peshawari Points do you wish to Redeem?</div></div>";
        offerHtml+= " <div  class='row' ><div class='col-md-3'></div><div class='col-md-9' style ='text-align:center'><table><tr><td><input type='text' id='txtwallet' class='form-control' style='background:white' /></td><td><div id='btnApplyPoints' onclick='javascript:ApplyPoints(" + Points + ");'  class='btnVoucher' style='margin:5px'>Apply</div></td> </tr></table></div></div>";


      


        var JsonData = new
        {
            Status = Status,
            OfferHtml = offerHtml

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string RemoveCoupon(string CouponNo)
    {

        
        int Status= new OffersBLL().RemoveOffer(CouponNo, HttpContext.Current.Session.SessionID);

 //       string offerHtml = "<table><tr><td style='font-weight:bold;padding-right:5px'>e-Coupon Code:</td>"+
 //"<td><input type='text' id='txtCouponCode' class='form-control' style='background:white' /></td>"+
 //"<td><div id='btnApplyCoupon' onclick='ApplyVouher(\"\",1)'  class='btnVoucher'>Apply</div></td><td>" +
 // "<div id='btnViewActiveOffers' onclick='ActiveOffer()'  style='cursor:pointer'>" +
 // "<img src='images/btnSpecialOffer.png'  style='width:150px'/></div></td></tr><tr><td colspan='100%' style='color:Red;font-style:italic'>" +
 // "<span style='color:Red'>*</span > A maximum of one voucher is applicable for an order. </td></tr>"+
 //"<tr><td colspan='100%'></td></tr></table>";
        string offerHtml = "<div class='row'><div class='col-md-3'><b>e-Coupon Code:</b></div><div class='col-md-6'><table><tr><td><input type='text' id='txtCouponCode' class='form-control' style='background:white' /></td><td><div id='btnApplyCoupon' onclick='ApplyVouherNew(\"\",1)'  class='btnVoucher' style='margin:5px'>Apply</div></td></tr></table></div>";

        offerHtml += "<div class='col-md-3'><div id='btnViewActiveOffers' onclick='ActiveOffer()' style='cursor:pointer'><img src='images/btnSpecialOffer.png' style='width:150px'/></div></div></div>";
        offerHtml += "<div class='row'><div class='col-md-12'><span style='color:Red'>*</span > A maximum of one voucher is applicable for an order. </div></div>";


        var JsonData = new
        {
            Status = Status,
           OfferHtml=offerHtml
      
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    
    
    
    void BindOffers()
    {
        ltOffers.Text = new OffersBLL().GetActiveOffersHTML();

    }

    [WebMethod]
    public static string BindOrderSummary()
    {
        Calc objCalc = new Calc();
        string SessionId = HttpContext.Current.Session.SessionID;
        string ItemList = new CartBLL().GetOrderSummary(SessionId, objCalc);
        string    SubTotal = objCalc.SubTotal.ToString();
        string FreeDeliveryAmt = objCalc.FreeDeliveryAmt.ToString();
        string DeliveryCharges = objCalc.DeliveryCharges.ToString();

        string DisAmount = objCalc.DisAmt.ToString();
        string NetAmount = Convert.ToDecimal(Convert.ToDecimal(objCalc.NetAmount.ToString()) - Convert.ToDecimal(objCalc.DisAmt.ToString())).ToString();
        var JsonData = new
        {
            IL = ItemList,
            ST=SubTotal ,
            DC = DeliveryCharges,
            DA = DisAmount,
            NA = NetAmount,
            FRE = FreeDeliveryAmt,
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
       
    }
    //void BindOrderSummary()
    //{

    //    Calc objCalc = new Calc();
    //    ltItemList.Text = new CartBLL().GetOrderSummary(Session.SessionID, objCalc);
    //    ltCartSubTotal.Text = objCalc.SubTotal.ToString();
    //    ltCartDeliveryCharges.Text = objCalc.DeliveryCharges.ToString();

    //    ltCartDisAmount.Text = objCalc.DisAmt.ToString(); 
    //    ltCartNetAmount.Text =Convert.ToDecimal (Convert.ToDecimal( objCalc.NetAmount.ToString())-Convert.ToDecimal( objCalc.DisAmt.ToString())).ToString();
    //}

    void BindDeliveryAddress()
    {
        Int64 DeliveryAddressId = 0;
        ltDeliveryAddress.Text = new DeliveryAddressBLL().GetHtmlByUserId(Convert.ToInt64(Session[Constants.UserId]), out DeliveryAddressId);
        txtDeliveryAddressId.Text = DeliveryAddressId.ToString();
    }

    void BindDays()
    {
        ListItem li = new ListItem();
        li.Text = "";
        li.Value = "";
        ddlDays.Items.Add(li);

        DateTime dd;
        DateTime totaldd;
        dd = Convert.ToDateTime(DateTime.Now.ToString("d"));
        totaldd = dd.AddDays(7);

        while (dd != totaldd)
        {
            li = new ListItem();
            li.Text = dd.ToLongDateString();
            li.Value = dd.DayOfWeek.ToString();
            if (li.Value == "Sunday")
            {

            }
            else
            {
                ddlDays.Items.Add(li);
            }

            dd = dd.AddDays(1);
        }



    }
    protected void ddlDays_SelectedIndexChanged(object sender, EventArgs e)
    {

  
        ddlTimeSlot.Items.Clear();
        string Date = Convert.ToDateTime(ddlDays.SelectedItem.Text).ToShortDateString() + " ";
        TimeSlots objTimeSlots = new TimeSlots();


        bool IsAvailable = false;
        DateTime dd;
        dd = Convert.ToDateTime(DateTime.Now.ToString("d"));

        string curday = dd.DayOfWeek.ToString();
        string slctday = ddlDays.Text;
        if (curday != slctday)
        {

            ddlTimeSlot.DataSource = new TimeSlotsBLL().GetAllSlots();
            ddlTimeSlot.DataTextField = "SlotEndTime";
            ddlTimeSlot.DataValueField = "Id";
            ddlTimeSlot.DataBind();
            ddlTimeSlot.Items.Insert(0, "");

        }
        else
        {

            ddlTimeSlot.DataSource = new TimeSlotsBLL().GetSlots();
            ddlTimeSlot.DataTextField = "SlotEndTime";
            ddlTimeSlot.DataValueField = "Id";
            ddlTimeSlot.DataBind();
            ddlTimeSlot.Items.Insert(0, "");

        }
        //if (objTimeSlots.StartTime != "" && objTimeSlots.EndTime != "" && Convert.ToDateTime(Date + objTimeSlots.StartTime) > DateTime.Now)
        //{
        //    ListItem li = new ListItem();
        //    li.Text = Convert.ToDateTime("1/1/1 " + objSlots.Slot1Start).ToShortTimeString() + " - " + Convert.ToDateTime("1/1/1 " + objSlots.Slot1End).ToShortTimeString();
        //    ddlTimeSlot.Items.Add(li);
        //    IsAvailable = true;

        //}

        //if (!IsAvailable)
        //{
        //    ListItem li = new ListItem();
        //    li.Text = "No Slots Available for Today";
        //    li.Value = "";
        //    ddlTimeSlot.Items.Add(li);

        //}



    }
    protected void btnReviewOrder_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Int64 DeliveryAddressId = CommonFunctions.IsNumeric(txtDeliveryAddressId.Text);
            UserCartMst UCM = new UserCartMst()
            {
                SessionId = Session.SessionID,
                DeliveryDate = Convert.ToDateTime(ddlDays.SelectedItem.Text),
                DeliverySlot = ddlTimeSlot.SelectedValue,
                UserId = Convert.ToInt64(Session[Constants.UserId]),
                PaymentMode = "Cash On Delivery",
                DeliveryAddressId = DeliveryAddressId
            };
            new UserCartMstBLL().InsertUpdate(UCM);
            Response.Redirect("orderreview.aspx");
        }


    }
}