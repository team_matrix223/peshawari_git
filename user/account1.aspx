﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="account.aspx.cs" Inherits="account" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<script src="js/bootstrap.min.js" type="text/javascript" ></script>
<script src="js/jquery.min.js" type="text/javascript" ></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Index</title>
</head>

<body>

<form runat="server">


<div class="container-fluid">
<div class="row" style="background:black;">
<div class="col-xs-12">
<div class="menu">
        <ul>
        <li><a href="#">My Account</a></li>
        <li><a href="#">Help</a></li>
        <li><a href="#">Checkout</a></li>
        </ul>
      </div>
</div>
</div>

<div class="row">
    <div class="col-md-9">
    <img src="images/logo.jpg" alt="" />
    </div>
    
    <div class="col-md-3">
    <div class="phone">
    <ul>
        <h3>Order on call:</h3>
        <li><a href="#">21 75 01 89 23</a></li>
        </ul>
        </div>
      
      <div class="item">
         <table>
 <tr>
 <td><img src="images/shopping-cart.png" alt="" /></td>
 <td><h5>MY CART</h5>
</td>
 </tr>
 <tr><td colspan="100%">  <p style="font-size:12px; color:#808182;">item(s) -00.00</p></td></tr>
 </table>
   
      </div>
      
    </div>
  </div>
 
 
         <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar">
                        </span>
                    </button>
                    <a class="navbar-brand" style="color: #000000;" href="../index.aspx">Home</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <asp:Literal ID="ltFeaturedCategories" runat="server"></asp:Literal>
                        
                        <asp:Repeater ID="repHeaderLinks" runat="server">
                        <ItemTemplate>
                        <li class="active"><a href="../info.aspx?pid=<%#Eval("PageId") %>"> <%#Eval("Title") %></a></li>
                        
                        </ItemTemplate>
                        </asp:Repeater>

                         <%if (Session[Constants.UserId] == null)
                          { %>
                        <li><a href="account.aspx" style="color: #000000">JOIN FREE</a></li>
                        <li><a href="account.aspx" style="color: #000000">SIGN IN</a></li>
                        <%}
                          else
                          { %>
                        <li><a href="dashboard.aspx" style="color: #000000">MY ACCOUNT</a></li>
                        <li><a href="signout.aspx" style="color: #000000">SIGN OUT</a></li>
                        <%} %>
                    </ul>
                </div>
            </div>
        </nav>
 
<!--Row Start-->
<div class="row">
<div class="col-sm-3 border ">
<div class="freedelivery">
 <table >
 <tr>
 <td><img src="images/free-delivery.png" alt="" /></td>
 <td><h2>FREE DELIVERY</h2>
  <p style="font-size:12px">On order Value Above $250</p></td>
 </tr>
 </table>
 
 
 </div>
</div>

<div class="col-sm-3 border ">
<div class="freedelivery">
 <table >
 <tr>
 <td><img src="images/calender.png" alt="" /></td>
 <td><h2>FREE DELIVERY</h2>
  <p style="font-size:12px">On order Value Above $250</p></td>
 </tr>
 </table>
 
 </div>
</div>



<div class="col-sm-3 ">
<div class="freedelivery">
 <table >
 <tr>
 <td><img src="images/money.png" alt="" /></td>
 <td><h2>FREE DELIVERY</h2>
  <p style="font-size:12px">On order Value Above $250</p></td>
 </tr>
 </table>
 
 
 </div>
</div>


<div class="col-sm-3">
<div class="callus">
 <table>
 <tr>
 <td><img src="images/phonepic.png" alt="" /></td>
 <td><h2>CALL US : + 91-808-227</h2>
  <p style="font-size:12px; color:#FFFFFF;">Need Help Call Us</p></td>
 </tr>
 </table>
 
 
 </div>
</div>
 
</div>
<!--Row End-->


 


<!--Row Start-->
<div class="row" style="margin-top:15px;margin-bottom:15px">

<div class="col-md-12" >
<div  class="col-md-10" style="margin-left:8%;margin-right:8%;border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:25px 20px 60px">
<div class="row">
<div class="col-md-6">

<table style="width:100%"   >
<tr>
<td colspan="100%"  >
<h4>LOGIN PANEL</h4>
<hr />
 
 
</td>
</tr>
 
<tr>
<td class="heading">User Name:</td><td><asp:TextBox ID="txtName" class="inputtext" runat="server" style="padding-left:10px"></asp:TextBox></td><td style="padding-left: 10px">
    <asp:RequiredFieldValidator ID="reqUName" runat="server" ControlToValidate="txtName" ErrorMessage="*" ForeColor="Red" ValidationGroup ="a">
    </asp:RequiredFieldValidator>
        </td>
                                                                                                                 
</tr>
 


<tr>
<td class="heading">Password:</td><td><asp:TextBox ID="txtPassword" TextMode="Password"  class="inputtext" runat="server" ></asp:TextBox></td>
    <td style="padding-left: 10px">
     <asp:RequiredFieldValidator ID="reqPswd" runat="server" ControlToValidate="txtPassword" ErrorMessage="*" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                                  </td>
</tr>

 <tr><td>
  
                    
                       </td><td  >
               
       <asp:Button ID="btnLogin_Click"    runat="server" Text="Login" ValidationGroup="a"
                       class="btnYellow" OnClick="btnLogin_Click_Click"/>
                       
                        
                         <asp:LinkButton ID="btnForgetpswd" runat="server" Text="Forgot password?" CausesValidation="false" OnClick="btnForgetpswd_Click" 
                             /> 
                   
                    </td><td>
                           &nbsp;</td></tr>


                           <tr>
                           <td colspan="100%">
                           <div style="padding:5px;margin-top:50px;border:solid 1px silver;border-radius:5px; background:none repeat scroll 0 0 #f4f4f4">

                           <h3>New to Our Website? Register</h3><div class="uiv2-why-register"><p class="why">Why register?</p><ul class="uiv2-mar-t-10"><li>- Wide selection of products</li><li>- Quality and service you'll love</li><li>- On time delivery guarantee</li><li>- Shop on the go from Android and iOS devices</li></ul></div>
                           </div>                           
                           </td>
                           
                           </tr>
 
</table>

<br />
 


</div>


<div class="col-md-6">

<table style="width:100%"   >
<tr>
<td colspan="100%"  >
<h4>NEW ACCOUNT REGISTRATION</h4>
<hr />
 
 
</td>
</tr>
<tr>
<td class="headingStyle" colspan="100%">PERSONAL DETAILS</td>
</tr>
<tr><td colspan="100%">&nbsp;</td></tr>
<tr>
<td class="heading">First Name:</td><td><asp:TextBox ID="txtFirstName" class="inputtext" runat="server" ></asp:TextBox></td>
    <td style="padding-left: 10px"><asp:RequiredFieldValidator ID="req_FirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="*" ForeColor="#CC0000" ValidationGroup="b"></asp:RequiredFieldValidator>
                         </td>
</tr>

<tr>
<td class="heading">Last Name:</td><td><asp:TextBox ID="txtLastName" class="inputtext" runat="server" ></asp:TextBox></td><td></td>
</tr>
     <tr>
<td class="heading">MobileNo:</td><td><asp:TextBox ID="txtUserMobile" class="inputtext" runat="server" ></asp:TextBox></td>
         <td style="padding-left: 10px"> <asp:RequiredFieldValidator ID="RequiredFieldValidator4"   ValidationGroup="b" runat="server" ControlToValidate="txtUserMobile" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
</td>

</tr>
<tr>
<td class="heading">Email Id:</td><td><asp:TextBox ID="txtEmailId" class="inputtext" runat="server"  ></asp:TextBox></td>
    <td style="padding-left: 10px">                                 <asp:RequiredFieldValidator ID="req_Email" runat="server" ControlToValidate="txtEmailId" ErrorMessage="*" ForeColor="#CC0000" ValidationGroup="b"></asp:RequiredFieldValidator>
                                  <asp:RegularExpressionValidator ID="RegExp_Email" runat="server" ControlToValidate="txtEmailId" ErrorMessage="Enter Valid EmailID" ValidationGroup="b" Font-Bold="False" ForeColor="#CC0000" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                  </td>
</tr>

<tr>
<td class="heading">Password:</td><td><asp:TextBox ID="txtPswd" class="inputtext" runat="server" TextMode="Password"  ></asp:TextBox></td>
    <td style="padding-left: 10px"><asp:RequiredFieldValidator ID="req_Password" runat="server" ControlToValidate="txtPswd" ErrorMessage="*" ForeColor="#CC0000"  ValidationGroup="b"></asp:RequiredFieldValidator>
</td>
</tr>

<tr>
<td class="heading">Confirm Password:</td><td><asp:TextBox ID="txtCPassword" class="inputtext" runat="server"  TextMode="Password"></asp:TextBox></td>
    <td style="padding-left: 10px"> <asp:RequiredFieldValidator ID="req_CPassword" runat="server" ControlToValidate="txtCPassword" ErrorMessage="*" ForeColor="#CC0000" ValidationGroup="b"></asp:RequiredFieldValidator>
      <asp:CompareValidator ID="Comp_Password" runat="server" ControlToCompare="txtPswd" ControlToValidate="txtCPassword" ValidationGroup="b" ErrorMessage="These passwords don't match. Try again?" ForeColor="#CC0000"></asp:CompareValidator>
</td>
</tr>
<tr><td colspan="100%">&nbsp;</td></tr>
<tr>
<td  class="headingStyle" colspan="100%">ADDRESS INFORMATION</td>
</tr>

<tr>
<td class="heading" style="width:150px">Recipient FirstName:</td><td><asp:TextBox ID="txtRFirstName" class="inputtext" runat="server"  ValidationGroup="b"></asp:TextBox></td>
    <td style="padding-left: 10px"> <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="b" ControlToValidate="txtRFirstName" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
</td>
</tr>

<tr>
<td class="heading">Recipient LastName:</td><td><asp:TextBox ID="txtRLastName" class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
    <tr>
<td class="heading">MobileNo:</td><td><asp:TextBox ID="txtMobileNo" class="inputtext" runat="server" ></asp:TextBox></td>
         <td style="padding-left: 10px"> <asp:RequiredFieldValidator ID="RequiredFieldValidator2"   ValidationGroup="b" runat="server" ControlToValidate="txtMobileNo" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
</td>

</tr>
     <tr>
<td class="heading">Telephone:</td><td><asp:TextBox ID="txtTelephone" class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
    <tr>
<td class="heading">City:</td><td><asp:DropDownList ID="ddlCities" runat="server"></asp:DropDownList></td>
        <td style="padding-left: 10px"> <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="b" ControlToValidate="ddlCities" InitialValue="--Select City--" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>

                                                                                                          </td>
</tr>
    <tr>
<td class="heading">Area:</td><td><asp:TextBox ID="txtArea" class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
    <tr>
<td class="heading">HNo/Street:</td><td><asp:TextBox ID="txtStreet" class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
        <tr>
<td class="heading">Address:</td><td><asp:TextBox ID="txtAddress" TextMode="MultiLine"  class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
        <tr>
<td class="heading">PinCode:</td><td><asp:TextBox ID="txtPinCode" class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
<tr>
<td></td>

<td>
<asp:Button ID="btnSubmit" class="btnGray" runat="server" Text="Register" OnClick="btnSubmit_Click" ValidationGroup="b" />

</td>
</tr>
</table>

<br />
<br />

 
</div>

</div>


</div>
</div>

 

 


</div>
<!--Row End-->


<!--Row Start-->
 





<div class="row">
<div class="col-md-4">
<div class="paypal">
<img src="images/footer-img.png" alt="" />
</div>
</div>
<div class="col-md-4">
<div class="paypal">
<table>
<tr><td><h4 style="font-size:18px; font-weight:700;">Are you Hooked on Gadgets?</h4></td>
<tr><td><h4 style="font-size:14px; padding:5px 0 0 0; font-weight:300;">Then, you will love our Mobile Apps
Download the HungerQuenchers Apps on</h4></td></tr>
<td style="vertical-align:top;padding-top:8px"><img src="images/googleply.png" style="margin:5px" alt=""> <img src="images/appstore.png" style="margin:5px" alt=""></td>
<tr><td><h4 style="font-size:14px; font-weight:300;">and Enjoy our Services on the Go</h4></td>

</table>
</div>
</div>
<div class="col-md-4">
<div class="paypal">
<img src="images/paypalimg.png" alt="" />

</div>
</div>

</div>


<!-footer start--->

<!--Row Fluid Start-->


 <div class="row" style="background: #1F1F1F no-repeat; margin-top:5px; padding-bottom: 10px">
            <div class="col-md-4">IN

                <div class="link">
                    <h2>
                        Categories</h2>
                    <ul>
                        <asp:Repeater ID="repCategories" runat="server">
                            <ItemTemplate>
                                <li>
                                   <a href="../list.aspx?c=<%#Eval("CategoryId") %>" style='color:white' > <%#Eval("Title") %></a> </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </div>
            <asp:Repeater ID="repOuterLinks" runat="server">
                <ItemTemplate>
                    <div class="col-md-4">
                        <div class="link">
                            <h2>
                                <%#Eval("Title") %></h2>
                            <ul>
                                <asp:Repeater ID="repInnerLinks" runat="server" DataSource='<%#GetInnerLinks(Eval("PageId")) %>'>
                                    <ItemTemplate>
                                        <li>
                                            <%#Eval("Title") %></li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <div class="col-md-4">
                <div class="link">
                    <h2>
                        Contact</h2>
                    <ul>
                        <li>040-27704444</li>
                        <li>Subscribe to our mailing list</li>
                       <%-- <uc2:ucNewsletter ID="ucNewsLetter" runat="server" />--%>
                    </ul>
                </div>
            </div>
        </div>          
<!--Row Fluid End-->     
          
<!-footer End--->

</div>
</form>

</body>
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
</html>
