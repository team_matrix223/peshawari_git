﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/main.master" AutoEventWireup="true" CodeFile="userorders.aspx.cs" Inherits="user_userorders" %>


<asp:Content ID="cntBreadCrumbs" ContentPlaceHolderID="cntBreadCrumbs" runat="server">
      <div class="breadcrumbs_container">
            <article class="breadcrumbs">
                <a href="dashboard.aspx">Dashboard</a>
                <div class="breadcrumb_divider">
                </div>
                <a href="userorders.aspx" class="current">My Orders</a>
            
            </article>
        </div>


</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
       <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
  <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
  <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
   <script src="js/jquery-ui.js" type="text/javascript"></script>
     <script src="js/jquery-ui.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="../js/jquery.uilock.min.js"></script>


    <script language="javascript">
        $(document).ready(
      function () {

          $("#txtFromDate").datepicker({
              yearRange: '1900:2030',
              changeMonth: true,
              changeYear: true,
              dateFormat: 'dd-mm-yy'
          });
          $("#txtToDate").datepicker({
              yearRange: '1900:2030',
              changeMonth: true,
              changeYear: true,
              dateFormat: 'dd-mm-yy'
          });
      });
        function ShowHide(id)
        {
            $("#dv" + id).toggle(500, function () {

                if ($("#img" + id).html() == "-") {
                    $("#img" + id).html("+");

                }
                else {
                    $("#img" + id).html("-");

                }
            }
            );

            
        }

        function CancelOrder(Id) {

          //  $.uiLock('');
            var OrderId = Id;
          
            var CancelRemarks = "Cancel Order";
          
           
            $.ajax({
                type: "POST",
                data: '{"OrderId": "' + OrderId + '","CancelRemarks": "' + CancelRemarks + '"}',
                url: "userorders.aspx/InsertCancelOrder",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == 0) {

                        alert("Cancelation Failed");
                        return;
                    }

                    alert("Order has been Cancelled successfully.");


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                  //  $.uiUnlock();
                }
            });




        }

    </script>

   <form id="frmMain" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
   <asp:HiddenField ID="hdnId" runat="server"/>
<div id="content" style="padding:6px">
       			<div id="rightnow">
                 
                       				   <div class="youhave" style="padding-left:10px;padding-top:18px">
                   
                 <table cellpadding="0" cellspacing="0">
                     <tr><td style="padding-right:10px">From:</td><td style="padding-right:10px">
                         <asp:TextBox ID="txtFromDate" ClientIDMode="Static" runat="server" Width="100px"></asp:TextBox>
<%--                         <asp:CalendarExtender ID="cc1" TargetControlID="txtFromDate" EnabledOnClient="true"  runat="server" Format="dd-MMM-yyyy"></asp:CalendarExtender>--%>
                                       </td><td style="padding-right:10px">To:</td><td style="padding-right:10px">
                                           <asp:TextBox ID="txtToDate" runat="server" ClientIDMode="Static"  Width="100px"></asp:TextBox>
<%--                                           <asp:CalendarExtender ID="cc2" TargetControlID="txtToDate" EnabledOnClient="true" Format="dd-MMM-yyyy"  runat="server"></asp:CalendarExtender>--%>
                                                        </td><td style="padding-right:10px">Status</td><td style="padding-right:10px"><asp:DropDownList  id="ddlStatus" runat="server" >
                                                            <asp:ListItem value="All"   Text="All"></asp:ListItem>
                                                              <asp:ListItem value="Pending" Text="Pending"></asp:ListItem>
                                                              <asp:ListItem value="InProcess" Text="InProcess"></asp:ListItem>
                                                              <asp:ListItem value="Dispatched" Text="Dispatched"></asp:ListItem>
                                                             <asp:ListItem value="Billed" Text="Billed"></asp:ListItem>
                                                              <asp:ListItem value="Canceled" Text="Canceled"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                            </td>
                         <td>  </td>
                         <td>
                             <asp:Button ID="btnView" runat="server" Width="60px" Text="View" class="btn btn-success" OnClick="btnView_Click" /></td>
                     </tr>
                     </table>
                       <table style="margin-top:10px">
                       <tr>
                         <td >
           
                             
                                      <asp:Repeater ID="repOrders" runat="server" OnItemDataBound="repOrders_ItemDataBound">
                                      <HeaderTemplate>
                                          <table style="border-color :maroon;border: solid 2px;color:black " >
                                              <thead>
                                                  <tr style="background-color:black ;border: solid 2px">
                                                       <th style="width:10px;color:white " ></th>
                                                      <th style="width:200px;color:white">Recipient Name</th>
                                                      <th style="width:200px;color:white">MobileNo</th>
                                                      <th style="width:200px;color:white">Address</th>
                                                       <th style="width:150px;color:white">BillValue</th>
                                                       <th style="width:180px;color:white">Delivery Charges</th>
                                                       <th style="width:150px;color:white">NetAmount</th>
                                                          <th style="width:150px;color:white">OrderDate</th>
                                                             <th style="width:150px;color:white">Status</th>
                                                             
                                                  </tr>

                                              </thead>

                                      </HeaderTemplate>
                                          
                                             <ItemTemplate>

                                               <tr><td>
                                                   <div id='img<%#Eval("OrderId") %>' onclick='javascript:ShowHide(<%#Eval("OrderId") %>)' style="font-weight:bold;cursor:pointer">
                                                       +
                                                   </div>
                                             

                                                   
                                                 </td><td><%#Eval("RecipientFirstName") %><%#Eval("RecipientLastName") %></td><td><%#Eval("RecipientMobile") %></td><td><%#Eval("Address") %></td><td><%#Eval("BillValue") %></td><td><%#Eval("DeliveryCharges") %></td><td><%#Eval("NetAmount") %></td><td><%#Eval("strOD")%></td><td><%#Eval("Status") %></td>
                                                 
                                                   <td>
                                                   <asp:HiddenField ID="hdnOrderId" runat="server" Value='<%#Eval("OrderId") %>' />
                                                    <asp:HiddenField ID="hdnStatus" runat="server" Value='<%#Eval("Status") %>' />
                                                   <asp:Literal ID="ltData" runat="server"></asp:Literal>
                                                   
                             </td>
                                                 
                                                 <tr>
                                                     <td colspan="100%">
     <div id='dv<%#Eval("OrderId") %>' style="font-weight:bold;display:none;width:100%">
                                                      
    <asp:Repeater ID="repOrderDetail"  runat="server"  DataSource='<%#BindOrderDetailList(Eval("OrderId"),Eval("Status")) %>' >
                                      <HeaderTemplate>
                                          <table style="background-color:#FBFBF9 ;color: black ;">
                                              <thead>
                                                 <tr style="background-color:#808080 ;border: solid 2px">
                                                      <th style="width:150px">ProductId</th>
                                                      <th style="width:200px">ProductName</th>
                                                      <th style="width:50px">Qty</th>
                                                     <th style="width:150px">Price</th>
                                                  </tr>

                                              </thead>

                                      </HeaderTemplate>
                                          
                                             <ItemTemplate>

                                               <tr><td><%#Eval("ProductId") %></td><td><%#Eval("ProductName") %></td><td><%#Eval("Qty") %></td><td><%#Eval("Price") %></td></tr>  

                                         </ItemTemplate>
                                          <FooterTemplate>

                                              
                                          </table>

                                          </FooterTemplate>

                                     </asp:Repeater>
   </div>
                                                         </td>
                                                     </tr>

                                                            

                                       
                                             </ItemTemplate>
                                          <FooterTemplate>

                                              
                                          </table>

                                          </FooterTemplate>

                                     </asp:Repeater>
                              
                              
                                                     </td>

                                                 </tr> 




                             
                     </table>
                       </div>
                       </div>
    </div>
       </form>
</asp:Content>

