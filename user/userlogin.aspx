﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="userlogin.aspx.cs" Inherits="backoffice_userlogin" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shopping Cart User Block</title>
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/theme.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" href="css/unicorn.main.css" />
<script src="js/customValidation.js" type="text/javascript"></script>
<script>
    var StyleFile = "theme" + "4" + ".css";
    document.writeln('<link rel="stylesheet" type="text/css" href="css/' + StyleFile + '">')

    $(document).ready(
    function () {
        $(".anc").click(
        function () {

            $("#u" + $(this).attr("id")).toggle(500);
        }

        );


    }
    );
    function ExpandCollapse(li) {
        alert(li);
    }

    function ShowTLinks(val) {


        $("div [name='tLinks']").hide();
        var ul = $("#ulT" + val);

        $(".current").removeClass("current");
        $("#anT" + val).addClass("current");
        ul.show();


    }
</script>
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="css/ie-sucks.css" />
<![endif]-->
    <style type="text/css">
        .auto-style1 {
            width: 130px;
        }
    </style>
</head>

<body>
	<div id="container">
    	<div id="header">
        	<h2>Shopping Cart<sup style="font-size:10px;font-weight:bold;font-style:italic">SC </sup></h2>
   
      </div>
       

<div id="wrapper">
<form id="frmLogin" runat="server">
            <div id="content" style="width:400px;margin-left:300px;margin-top:100px">
       			<div id="rightnow"   >
                    <h3 class="reallynow">
                        <span>User Login Panel</span>
                        
                        <br />
                    </h3>
				    <div class="youhave">
 
                           <table cellpadding="0" cellspacing="0" border="0">
                     <tr><td class="headings">UserName:</td><td class="auto-style1">  
                     <input type="text" runat="server" name="txtAdminName" class="inputtxt validate required alphanumeric"  data-index="1" id="txtName" /></td><td>  
                             &nbsp;</td></tr>
                     <tr><td class="headings">Password:</td><td class="auto-style1">  <input type="password" runat="server" name="txtPassword" class="inputtxt validate required alphanumeric"  data-index="1" id="txtPassword" /></td><td>  
                         &nbsp;</td></tr>
                   <tr><td colspan="100%">
                       <table>
                           <tr><td></td><td>
                                <asp:Button ID="btnLogin" runat="server" Text="Login" style="width:100px"
                           CssClass="btn btn-primary btn-small" onclick="btnLogin_Click"/> 
                               </td>
                              <td class="auto-style1">
               
                           <asp:LinkButton ID="btnForgetpswd" runat="server" Text="Forgot password?" 
                            onclick="btnForgetpswd_Click"/> 
                   
                    </td>
                               <td>
                            <a href="userregistration.aspx">New User?</a>  </td>
                           </tr>
                       </table>
               
                          
                   
                       </td></tr>
                            
                    </table>

                    </div>
			  </div>
               
            </div>
</form>
             
      </div>







        <div id="footer">
        <div id="credits" style="width:400px">
   		Website Designed and Developed By <a href="#">Matrix Software Solutions Pvt Ltd.</a>
        </div>
        <br />

        </div>
</div>
</body>
</html>
