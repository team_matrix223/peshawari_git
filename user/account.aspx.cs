﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class account : System.Web.UI.Page
{
    public Int64 FreeDelAmt = 0;
    public Int64 MiniAmt = 0;
    DataSet ds = null;
    public string ReturnUrl { get { return Request.QueryString["ReturnUrl"] != null ? Request.QueryString["ReturnUrl"] : ""; } }
    int m_TotalItems = 0;
    public int TotalItems { get { return m_TotalItems; } set { m_TotalItems = value; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Constants.UserId ] != null)
        {
            Response.Redirect("dashboard.aspx");
        }
        if (!IsPostBack)
        {
           // BindHeaderLinks();
            BindOuterLinks();
            BindProductCategories();
        //  BindFeaturedCategories();
            BindCities();
            ddlSector.Items.Insert(0, "--Select Sector--");
            Settings objsett = new Settings();
            objsett = new SettingsDAL().GetSett();
            MiniAmt = Convert.ToInt64(objsett.MinimumCheckOutAmt);
            FreeDelAmt = Convert.ToInt64(objsett.FreeDeliveryAmt);
            Int32 totitems = new CartBLL().GetTotalItems(HttpContext.Current.Session.SessionID);
            TotalItems = totitems;
        }

    }
    [WebMethod]
    public static string GetTimeSlots()
    {


        string timeslots = new TimeSlotsBLL().GetTimeSlotsHTML();

        var JsonData = new
        {
            TSlots = timeslots

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string GetAddress()
    {
        Int32 retVal = 0;
        string Address = "";
        string city = HttpContext.Current.Request.Cookies["City"] == null ? "0" : HttpContext.Current.Request.Cookies["City"].Value.ToString();
        if (city == "0")
        {
            retVal = 0;
        }
        else
        {
            retVal = 1;
            Address = new PinCodeDAL().GetAddressById(Convert.ToInt32(city));
        }
        MessageCredentials objMsg = new MessageCredentialsDAL().GetAll();
        string m_OrderOnCall = objMsg.OrderOnCall;
        string m_CallUs = objMsg.CallUs;

        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {

            SAddress = Address,
            Status = retVal,
            OrderOnCall = m_OrderOnCall,
            CallUs = m_CallUs
        };
        return ser.Serialize(JsonData);
    }
    void BindProductCategories()
    {

        repCategories.DataSource = new CategoryBLL().GetByParentId(0);
        repCategories.DataBind();

    }

    void BindOuterLinks()
    {
        DataSet ds = new Cms().GetAll();
        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "ParentPage=0 and ShowOn=1";
        repOuterLinks.DataSource = dv;
        repOuterLinks.DataBind();
    }
    public DataView GetInnerLinks(object ParentId)
    {
        DataSet ds = new Cms().GetAll();
        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "ParentPage=" + Convert.ToInt16(ParentId);
        return dv;

    }
    public void BindCities()
    {
        ddlCities.DataSource = new CitiesBLL().GetAll();
        ddlCities.DataTextField = "Title";
        ddlCities.DataValueField = "CityId";
        ddlCities.DataBind();
        ddlCities.Items.Insert(0, "--Select City--");
    }
    public void ResetControls()
    {
        txtUserMobile.Text = "";
        txtFirstName.Text = "";
        txtPswd.Text = "";
        txtCPassword.Text = "";
        ddlCities.SelectedIndex = 0;
        txtAddress.Text = "";
        txtPinCode.Text = "";
    }
    protected void btnLogin_Click_Click(object sender, EventArgs e)
    {
        Users objuser = new Users()
        {
            MobileNo  = txtName.Text.Trim(),
            Password = txtPassword.Text.Trim()

        };

        string status = new UsersBLL().UserLoginCheck(objuser,Session.SessionID);
        if (status.ToString() == "-1")
        {
            lblLogin.Text = "** Invalid User Name";
          //  Response.Write("<script>alert('Invalid User Name');</script>");
            return;
        }
        else if (status.ToString() == "-2")
        {
            lblLogin.Text = "** Invalid Password";
           // Response.Write("<script>alert('Invalid Password');</script>");
            return;
        }
        else if (status.ToString() == "0")
        {
            lblLogin.Text = "** First Register,Then SignIn";
           
            //Response.Write("<script>alert('First Register,Then SignIn');</script>");
            return;

        }
        else
        {

            Session["Roles"] = status;
        
            Session[Constants.UserId] = status;
            Session[Constants.Email] = txtName.Text.Trim();

            if (ReturnUrl != "")
            {
                Response.Redirect("../delivery.aspx");
            }
            else
            {
                Response.Redirect("dashboard.aspx");
            }
        }
    }
    protected void btnForgetpswd_Click(object sender, EventArgs e)
    {
        string UserName = txtName.Text.Trim();
        string Password = CommonFunctions.GenerateRandomPassword(8);
        new UsersBLL().ResetPassword(UserName, Password);

        try
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress("support@matrixposs.com");
            message.To.Add(new MailAddress(UserName));
            message.Subject = "Password Recovery";
            message.Body = "Dear User. Your new password is:" + Password;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            Response.Write("<script>alert('Please check your inbox for new Password.');</script>");
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('Mailing Server Down. Please try again Later');</script>");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        DateTime DOBDate;
        DateTime ADate;
        if (txtDOB.Text.Trim() == "")
        {
            DOBDate = Convert.ToDateTime("1900-01-01 00:00:00");
        }
        else
        {
            DOBDate = Convert.ToDateTime(txtDOB.Text);
        }
        if (txtAnniversaryDate.Text.Trim() == "")
        {
            ADate = Convert.ToDateTime("1900-01-01 00:00:00");
        }
        else
        {
            ADate = Convert.ToDateTime(txtAnniversaryDate.Text);
        }
        string UserCode = CommonFunctions.GenerateRandomUserCode(3);
        Users objUser = new Users()
        {
            UserId = 0,
            FirstName = txtFirstName.Text.Trim(),           
            Password = txtPswd.Text.Trim(),
            IsActive = true,
            MobileNo = txtUserMobile.Text.Trim(),
            AdminId = 0,
            RecipientFirstName = txtFirstName.Text.Trim(),
            RMobileNo = txtUserMobile.Text.Trim(),
            CityId = Convert.ToInt32(ddlCities.SelectedValue.ToString().Trim()),   
            Address = txtAddress.Text.Trim(),
            PinCode = txtPinCode.Text.Trim(),
            IsPrimary = true,
            PinCodeId = Convert.ToInt32(ddlSector.SelectedValue.ToString().Trim()),
            DOB=DOBDate,
            DOA =ADate,
            Code =UserCode,
            ReferralCode = txtReferrralCode.Text 
        };
        int result = new UsersBLL().InsertUpdate(objUser);
        if (result == 0)
        {
            lblMsg.Text = "** User Registraion is not completed,Plz try again";
           // Response.Write("<script>alert('User Registraion is not completed,Plz try again')</script>");
            return;
        }
        else if (result == -2)
        {
            lblMsg.Text = "** Current MobileNo is already In Use";
           // Response.Write("<script>alert('Current MobileNo is already In Use')</script>");
            txtUserMobile.Focus();
            return;
        }
        else if (result == -3)
        {
            lblMsg.Text = "** Referral Code does not exist";
            // Response.Write("<script>alert('Current MobileNo is already In Use')</script>");
            txtReferrralCode.Focus();
            return;
        }
        else
        {
            Session[Constants.UserId] = result;
           // Session[Constants.Email] = txtEmailId.Text.Trim();
            if (ReturnUrl != "")
            {
                Response.Redirect("../delivery.aspx");
            }
            else
            {
                Response.Redirect("dashboard.aspx");
            }
           // ResetControls();
        }
       
    }
    protected void ddlCities_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtPinCode.Text = "";
        if (ddlCities.SelectedIndex > 0)
        {
            ddlSector.DataSource = new PinCodeDAL().GetByCityId(Convert.ToInt32(ddlCities.SelectedValue.ToString()));
            ddlSector.DataTextField = "Sector";
            ddlSector.DataValueField = "PinCodeId";
            ddlSector.DataBind();
            ddlSector.Items.Insert(0, "--Select Sector--");
        }
    }
    protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtPinCode.Text = "";
        if (ddlSector .SelectedIndex > 0)
        {
           
            txtPinCode.Text =new PinCodeDAL().GetPinCodeById( Convert.ToInt32(ddlSector.SelectedValue.ToString()));
        }
    }
}