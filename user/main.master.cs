﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class main : System.Web.UI.MasterPage
{
    public Int64 FreeDelAmt = 0;
    public Int64 MiniAmt = 0;
    DataSet ds = null;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session[Constants.UserId] == null)
        {
            Response.Redirect("account.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                BindOuterLinks();
                BindProductCategories();
                Settings objsett = new Settings();
                objsett = new SettingsDAL().GetSett();
                MiniAmt = Convert.ToInt64(objsett.MinimumCheckOutAmt);
                FreeDelAmt = Convert.ToInt64(objsett.FreeDeliveryAmt);
            }
        }
    }


    void BindProductCategories()
    {
        repCategories.DataSource = new CategoryBLL().GetByParentId(0);
        repCategories.DataBind();

    }


    void BindOuterLinks()
    {
        ds = new Cms().GetAll();
        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "ParentPage=0 and ShowOn=1";
        repOuterLinks.DataSource = dv;
        repOuterLinks.DataBind();
    }




    public DataView GetInnerLinks(object ParentId)
    {
        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "ParentPage=" + Convert.ToInt16(ParentId);
        return dv;
    }
 
}
