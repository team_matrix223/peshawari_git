﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_changepassworduser : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Users  objUser = new Users();

        objUser.MobileNo = Session[Constants.Email].ToString();
        objUser.UserId =Convert.ToInt32 ( Session[Constants.UserId].ToString());
        objUser.Password = txtOldPassword.Text;
       int  result=Convert.ToInt32( new UsersBLL ().UserLoginCheck(objUser,Session.SessionID));

       if (result <=0)
        {
            Response.Write("<script>alert('Invalid Old Password');</script>");
        }
        else
        {
            objUser.UserId = Convert.ToInt32(Session[Constants.UserId]);
            objUser.Password = txtNewPassword.Text;
            string str = new UsersBLL().ChangePassword(objUser );
            if (str == "1")
            {
                Response.Write("<script>alert('Password Changed Successfully');</script>");
            }
            else
            {
                Response.Write("<script>alert('Operation Failed. Please try again later');</script>");
            }

        }



    }
}