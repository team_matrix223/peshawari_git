﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/main.master" AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="user_dashboard" %>


<asp:Content ID="cntBreadCrumbs" ContentPlaceHolderID="cntBreadCrumbs" runat="server">
      <div class="breadcrumbs_container">
            <article class="breadcrumbs">
                <a href="dashboard.aspx">Dashboard</a>
                <div class="breadcrumb_divider">
                </div>
                <a href="dashboard.aspx" class="current">My Profile</a>
            
            </article>
        </div>


</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <form id="frmMain" runat="server">
   <asp:HiddenField ID="hdnId" runat="server"/>
         <asp:HiddenField ID="hdnDeliveryId" runat="server"/>
  <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
   <ContentTemplate>
<div  class="col-md-12" style=" border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
 
 <table style="width:99%" >
<tr>
<td  class="headingStyle" colspan="100%">PERSONAL DETAILS</td>
</tr>
<tr>
<td class="heading" style="width:150px">Code:</td>
<td>
<asp:TextBox ID="txtCode" class="inputtext" runat="server" ReadOnly="true"></asp:TextBox>
</td>
    <td style="padding-left: 10px; ">
    
   
    
</td>

</tr>
<tr>
<td class="heading" style="width:150px">First Name:</td>
<td>
<asp:TextBox ID="txtFirstName" class="inputtext" runat="server" ></asp:TextBox>
</td>
    <td style="padding-left: 10px; ">
    
    <asp:RequiredFieldValidator ID="req_FirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="*" ForeColor="#CC0000" ValidationGroup="b"></asp:RequiredFieldValidator>
    
</td>

</tr>

<tr>
<td class="heading">Last Name:</td><td><asp:TextBox ID="txtLastName" class="inputtext" runat="server" ></asp:TextBox></td>
<td></td>
</tr>


   <tr>
<td class="heading">MobileNo:</td><td><asp:TextBox ID="txtUserMobile" class="inputtext" runat="server" ></asp:TextBox></td>
         <td style="padding-left: 10px"> <asp:RequiredFieldValidator ID="RequiredFieldValidator4"   ValidationGroup="b" runat="server" ControlToValidate="txtMobileNo" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
</td>

</tr>     
<tr>
<td class="heading">Email Id:</td><td><asp:TextBox ID="txtEmailId" class="inputtext" runat="server"  ></asp:TextBox></td>
    <td style="padding-left: 10px">
    <asp:RequiredFieldValidator ID="req_Email" runat="server" ControlToValidate="txtEmailId" ErrorMessage="*" ForeColor="#CC0000" ValidationGroup="b"  Display="Dynamic" ></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="RegExp_Email" runat="server" ControlToValidate="txtEmailId" ErrorMessage="Enter Valid EmailID" ValidationGroup="b" Font-Bold="False" Display="Dynamic" ForeColor="#CC0000" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                  </td>
</tr>
</table>

 

</div>


 

<div  class="col-md-12" style="margin-bottom:20px;margin-top:10px; border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
 

<table style="width:99%" >
<tr>
<td  class="headingStyle" colspan="100%">ADDRESS INFORMATION</td>
</tr>

<tr>
<td class="heading" style="width:150px">Recipient FirstName:</td>
<td><asp:TextBox ID="txtRFirstName" class="inputtext" runat="server"  ValidationGroup="b"></asp:TextBox></td>
    <td style="padding-left: 10px; "> <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="b" ControlToValidate="txtRFirstName" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
</td>

</tr>

<tr>
<td class="heading">Recipient LastName:</td><td><asp:TextBox ID="txtRLastName" class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
 <tr>
<td class="heading">MobileNo:</td><td><asp:TextBox ID="txtMobileNo" class="inputtext" runat="server" ></asp:TextBox></td>
         <td style="padding-left: 10px"> <asp:RequiredFieldValidator ID="RequiredFieldValidator2"   ValidationGroup="b" runat="server" ControlToValidate="txtMobileNo" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
</td>

</tr>
     <tr>
<td class="heading">Telephone:</td><td><asp:TextBox ID="txtTelephone" class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
  
    <tr>
<td class="heading">Area:</td><td><asp:TextBox ID="txtArea" class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
    <tr>
<td class="heading">HNo/Street:</td><td><asp:TextBox ID="txtStreet" class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
        <tr>
<td class="heading">Address:</td><td><asp:TextBox ID="txtAddress" TextMode="MultiLine"  class="inputtext" runat="server"></asp:TextBox></td><td></td>
</tr>
  <tr>
<td class="heading">City:</td><td><asp:DropDownList ID="ddlCities" style="width:100%;height:30px;margin-left:6px;" runat="server" AutoPostBack="true" 
            onselectedindexchanged="ddlCities_SelectedIndexChanged"></asp:DropDownList></td>
        <td style="padding-left: 10px"> <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="b" ControlToValidate="ddlCities" InitialValue="--Select City--" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>

                                                                                                          </td>
</tr>
 <tr>
<td class="heading">Sector:</td><td>
     <asp:DropDownList ID="ddlSector" runat="server" style="width:100%;height:30px;margin-left:6px;" AutoPostBack="true" onselectedindexchanged="ddlSector_SelectedIndexChanged" 
           ></asp:DropDownList>
            <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server" >
                        <ProgressTemplate>
                            <div id="dvProgress" style="position: absolute; width: 150px; height: 30px; background-color: Black;
                                color: White; text-align: center; opacity: 0.7; vertical-align: middle">
                                loading please wait..
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
           </td>
        <td style="padding-left: 10px"> 
            <asp:RequiredFieldValidator ID="ReqSector" runat="server" ValidationGroup="b" ControlToValidate="ddlSector" InitialValue="--Select Sector--" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
 
                                                                                                          </td>
</tr>
        <tr>
<td class="heading">PinCode:</td><td><asp:TextBox ID="txtPinCode" class="inputtext" runat="server" ReadOnly="true"></asp:TextBox></td><td></td>
</tr>
<tr>
<td></td>

<td>
<asp:Button ID="btnSubmit" class="btn btn-success" runat="server" Text="Submit" OnClick="btnSubmit_Click" ValidationGroup="b" />

</td>
</tr>
</table>
</div>
<br />
<br />
 </ContentTemplate>
 </asp:UpdatePanel>
 
</form>
</asp:Content>

