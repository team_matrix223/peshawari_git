﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/main.master" AutoEventWireup="true" CodeFile="userchangepassword.aspx.cs" Inherits="backoffice_changepassworduser" %>
<asp:Content ID="cntBreadCrumbs" ContentPlaceHolderID="cntBreadCrumbs" runat="server">
      <div class="breadcrumbs_container">
            <article class="breadcrumbs">
                <a href="dashboard.aspx">Dashboard</a>
                <div class="breadcrumb_divider">
                </div>
                <a href="userchangepassword.aspx" class="current">Change Password</a>
            
            </article>
        </div>


</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
     <form id="frmMain" runat="server">
			<div id="content">
       							
							<div class="widget-content nopadding" style="padding-top:0px">
                                <asp:ValidationSummary ID="smry" runat="server" ShowMessageBox="true" ShowSummary="false" />
						<div  class="col-md-12" style="margin-bottom:20px;margin-top: 0px; border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
 
                            <table style="width:100%">
                            <tr>
<td class="headingStyle" colspan="100%">CHANGE PASSWORD</td>
</tr>
                                <tr>
                                <td>
                                <table  style="margin-top:10px">
                                 
                               <tr>
                                    <td>Old Password</td>
                                    <td>
	<asp:TextBox ID="txtOldPassword" runat="server"  TextMode="Password"  class="inputtext"></asp:TextBox>
                                    </td>
                                    <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"   runat="server" ControlToValidate="txtOldPassword"  ErrorMessage="Please enter old password" SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>           

                                    </td>
                                </tr>
                                <tr>
                                    <td>New Password</td>
                                    <td>
                                        <asp:TextBox ID="txtNewPassword" runat="server"  class="inputtext" TextMode="Password" ></asp:TextBox>

                                    </td>
                                    <td>
         		 			              <asp:RequiredFieldValidator ID="reqPassword"   runat="server" ControlToValidate="txtNewPassword" ErrorMessage="Please enter new password" SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>

                                    </td>
                                </tr>
                                <tr><td>Confirm Password</td>
                                    <td>
                                        <asp:TextBox ID="txtConfirmPassword" runat="server"   class="inputtext" TextMode="Password" ></asp:TextBox>

                                    </td>
                                    <td>
  <asp:CompareValidator SetFocusOnError="true" ID="RequiredFieldValidator2" runat="server" ControlToCompare="txtNewPassword" ErrorMessage="Password and confirm password do not match." Display="None" Operator="Equal" Type="String" ControlToValidate="txtConfirmPassword"></asp:CompareValidator>

                                    </td>
                                </tr>
                                <tr><td></td>
                                    <td colspan="100%">
                                          <asp:Button ID="btnAdd" Text="Change Password" class="btn btn-success" runat="server" style="margin-left:10px" onclick="btnAdd_Click" /> 
                                    </td>
                                </tr>
                                
                               
                                
                                
                                </table>
                                
                                </td>
                                </tr>
                             
                            </table>
										
                                  
  
									</div>
								</div>
						
					</div>
         </form>

</asp:Content>

