﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class user_userdeliveryaddress : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string GetPinCode(int PinCodeId)
    {

        string PCode = new PinCodeDAL().GetPinCodeById(PinCodeId);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Pincode = PCode,
        };
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string BindPincodes(int CityId)
    {

        string Pincodes = new PinCodeDAL().GetAlloptions(CityId);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            PinCodeList = Pincodes,
        };
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string KeywordSearch(string Keyword)
    {
       string Data = new DeliveryAddressBLL().KeywordSearch(Keyword.Trim());

        return Data;
    }
    [WebMethod]
    public static string BindCity()
    {

        string cities = new CitiesBLL ().GetActiveOptions();
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            CitiessOptions = cities

        };
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string Insert(Int32 DeliveryAddressId, string RecipientFirstName, string RecipientLastName, string MobileNo, string Telephone, Int32 CityId, string Area, string Street,string Address,string PinCode, bool IsPrimary,int PinCodeId)
    {
        Int32 uId =Convert.ToInt32( HttpContext.Current.Session[Constants.UserId].ToString());
        DeliveryAddress objDelAdd = new DeliveryAddress()
        {
            DeliveryAddressId = DeliveryAddressId,
            UserId =uId ,
            RecipientFirstName =RecipientFirstName,
            RecipientLastName = RecipientLastName,
            MobileNo = MobileNo,
            Telephone = Telephone,
            CityId = CityId,
            Area = Area,
            Street = Street,
            Address = Address,
            PinCode = PinCode,
            IsPrimary = IsPrimary,
            PinCodeId=PinCodeId

        };
        int status = new DeliveryAddressBLL().InsertUpdate(objDelAdd);
        JavaScriptSerializer ser = new JavaScriptSerializer();
      
        var JsonData = new
        {
            DelAdd = objDelAdd,
            Status = status
        };
        return ser.Serialize(JsonData);
    }
    

    [WebMethod]
    public static string Delete(int DeliveryAddressId)
    {
        int UId = Convert.ToInt32(HttpContext.Current.Session  [Constants.UserId]);
        DeliveryAddress objAddress = new DeliveryAddress()
        {
            DeliveryAddressId = DeliveryAddressId,
            UserId = UId
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        int status = new DeliveryAddressBLL().DeleteById(objAddress);
        var JsonData = new
        {
            Status = status
        };
        return ser.Serialize(JsonData);
    }
}