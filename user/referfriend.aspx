﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/main.master" AutoEventWireup="true" CodeFile="referfriend.aspx.cs" Inherits="user_referfriend" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntBreadCrumbs" Runat="Server">
<article class="breadcrumbs">
                <a href="dashboard.aspx">Dashboard</a>
                <div class="breadcrumb_divider">
                </div>
                <a href="#" class="current">Refer A Friend</a>
            
            </article>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMaster" Runat="Server">

<div id="content">
       							
							<div class="widget-content nopadding" style="padding-top:0px">
                        
						<div  class="col-md-12" style="margin-bottom:20px;margin-top: 0px; border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
 
                             <table style="width:100%">
                                                    <tr>
<td class="headingStyle" colspan="100%">REFER FRIENDS</td>
</tr>

<tr>
<td style="text-align:center">
<div style="width:600px;margin:auto;margin-top:10px">
Help spread the joy and convenience of shopping with peshawarisupermarket.com.
Refer us to a friend & get  <b><asp:Literal ID="ltReferrarPoint" runat="server"></asp:Literal></b> Peshawari Points when they complete their first purchase.
Your Friend will also get  <b><asp:Literal ID="ltRefreePoint" runat="server"></asp:Literal></b> Peshawari Points on their First Order, if referred through You. 
 <br /> <br />
It’s quick & easy – all your friend need to do is to mention your Referal Code  "<b><asp:Literal ID="ltReferarCode" runat="server"></asp:Literal></b>" during Registration  
</div>

</td>
</tr>

                             </table>
										
                                  
  
									</div>
								</div>
						
					</div>
</asp:Content>

