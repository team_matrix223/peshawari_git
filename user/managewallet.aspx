﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/main.master" AutoEventWireup="true" CodeFile="managewallet.aspx.cs" Inherits="user_managewallet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntBreadCrumbs" Runat="Server">
<div class="breadcrumbs_container">
            <article class="breadcrumbs">
                <a href="dashboard.aspx">Dashboard</a>
                <div class="breadcrumb_divider">
                </div>
                <a href="managewallet.aspx" class="current">Your Wallet</a>
            
            </article>
        </div>
   <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
  <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
  <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
   <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.uilock.min.js"></script>
    <script type="text/javascript" src="../js/jquery.uilock.js"></script>
       <script src="js/customValidation.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMaster" Runat="Server">
<style type="text/css">
.fruitbox {
    border: 1px solid #eeeeee;
    border-radius: 5px;
    margin: 10px 0 0;
    padding: 0;
    text-align:center;
}
.fruitbox img {
    cursor: pointer;
    margin: 5px auto auto;
}
.fruitbox:hover {
    border: 1px solid #eeeeee;
    border-radius: 5px;
    box-shadow: 3px 3px 3px #676767;
    margin: 10px 0 0;
    padding: 0;
    transform: translateX(-2px) translateY(-2px);
    transition: all 0.3s ease 0s;
}
.fruitbox h2 {
    color: #444140;
    cursor: pointer;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 18px;
    margin: 10px 0 0;
    padding: 0;
    text-align: center;
}
</style>

<script language="javascript" type="text/javascript">


    var m_bal = 0;

    function BindGifts() {


        $.ajax({
            type: "POST",
            data: '{}',
            url: "managewallet.aspx/BindGifts",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                $("#dvGifts").html(msg.d);




            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
    }


    $(document).ready(
       function () {


           $.ajax({
               type: "POST",
               data: '{}',
               url: "managewallet.aspx/GetBalance",
               contentType: "application/json",
               dataType: "json",
               success: function (msg) {


                   var obj = jQuery.parseJSON(msg.d);

                   var total = obj.Balance["Total"];
                   var Used = obj.Balance["Used"];
                   var Bal = Number(total) - Number(Used)
                   m_bal = Bal;
                   $("#dvBal").html("Dear Customer you have " + Bal + " Peshawari points left in your Wallet.");
                   $("#spTotalPoints").html(total);
                   $("#spUsedPoints").html(Used);
                   $("#spLeftPoints").html(Bal);

               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   BindGifts();
               }
           });






           $("#btnAdd").click(
           function () {
               var giftid = 0;

               var Giftval = $("input[name='Gift']:checked").val();
               var ff = document.getElementsByName('Gift');
               for (var i = 0; i < ff.length; i++) {
                   if (ff[i].checked) {
                       giftid = ff[i].value;
                   }
               }

               if (giftid == "0") {
                   alert("Choose Gift");
                   return;
               }

               $.ajax({
                   type: "POST",
                   data: '{"GiftId": "' + giftid + '"}',
                   url: "managewallet.aspx/Insert",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);
                     


                       if (obj.Status == "-1") {
                           alert("Points are not enough for gift which you have choosen");
                           return;
                       }
                       else {
                           alert("Your Request has been sent. Our Technical team will contact you soon.");

                       }



                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {

                       $.uiUnlock();
                   }

               });

           }

         );

       });
     
      

 </script>


<form id="frmMain" runat="server">
   <input  type ="hidden" id="hdId" value="0"/>
<div id="content" style="padding:6px">
       			
				   <div class="youhave" style="padding-left:30px;">
            <div  class="col-md-12" style="margin-bottom:10px;margin-top: 0px; border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
 
            
                 <table cellpadding="0" cellspacing="0" width="99%"  id="frmAddress">
<tr>
<td  class="headingStyle" colspan="100%" >Your Balance</td>
</tr>


<tr>
<td colspan="100%">
<table width="100%">



 <tr>

  <td style="padding:5px;font-style:italic;text-align:center;font-size:larger;font-weight:bold"><div id="dvBal" style="margin:auto"></div>
  
  
  </td>   
</tr>

<tr>
<td colspan="100%">
<table class="table">
<tr>
<th>Total Points Earned</th>
<th>Points Used</th>
<th>Points Left</th>
</tr>

<tr>
<td><span id="spTotalPoints"></span></td>
<td><span id="spUsedPoints"></span></td>
<td><span id="spLeftPoints"></span></td>

</tr>

  </table>
  
</td>
</tr>


</table>
   
 


</td>
</tr>


</table>

<div class="col-md-12">
 

<div id="dvGifts">

</div>

 
</div>

<table style="margin-left:30px">
<tr>

<td style="padding-right:10px;padding-top:10px" >
    <div id="btnAdd" class="btn btn-success">Get Gifts</div>
    </td>

</tr>

</table>

 </div>


                    </div>
			  </div>
           
       </form>
</asp:Content>

