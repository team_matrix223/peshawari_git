﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/main.master" AutoEventWireup="true" CodeFile="userdeliveryaddress.aspx.cs" Inherits="user_userdeliveryaddress" %>

<asp:Content ID="cntBreadCrumbs" ContentPlaceHolderID="cntBreadCrumbs" runat="server">
      <div class="breadcrumbs_container">
            <article class="breadcrumbs">
                <a href="dashboard.aspx">Dashboard</a>
                <div class="breadcrumb_divider">
                </div>
                <a href="userdeliveryaddress.aspx" class="current">Delivery Addresses</a>
            
            </article>
        </div>


</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
     <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-ui.js"></script>
     <link rel="stylesheet" type="text/css" href="css/jquery-uiJQGrid.css" />  
     <script src="../js/customValidation.js"></script>

    <script type="text/javascript" src="js/jquery.uilock.js"></script>

   <script language="javascript" type="text/javascript">
       var m_AddressId = 0;
       function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                alert("Please Enter Only Numeric Value:");
                return false;
            }

            return true;
        }
       function GetPincode(PinCodeId) {
        $.ajax({
            type: "POST",
            data: '{"PinCodeId":' + PinCodeId + '}',
            url: "userdeliveryaddress.aspx/GetPinCode",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                
                $("#txtPinCode").val(obj.Pincode);


             }, error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function (msg) {
                 $.uiUnlock();

             }

         });
     }

           function BindPincodes(CityId) {
        $.ajax({
            type: "POST",
            data: '{"CityId":' + CityId + '}',
            url: "userdeliveryaddress.aspx/BindPincodes",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#ddlSector").html(obj.PinCodeList);


             }, error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function (msg) {
                 $.uiUnlock();

             }

         });
     }
       $(document).ready(
       function () {

              $("#ddlSector").change(function () {
        var PinCodeId = $("#ddlSector").val();
        GetPincode(PinCodeId);
    });

         $("#ddlCities").change(function () {
        var CityId = $("#ddlCities").val();
        BindPincodes(CityId);
    });

           BindCities();
           $("#chkprimary").prop("checked", false);
           $("#btnCancel").css({ display: "none" });
           $("#btnAdd").click(
           function () {
               Insert();
           }
           );
           $("#btnDelete").click(
           function () {

               DeleteRecord();
           }
           );
           $("#btnCancel").click(
           function () {
               ResetControls();
           });
       }
       );

       $(document).on("click", "div[name='dvpincode']", function (event) {
           var data = $(this).attr("id");
           var arrData = data.split('_');
           var vid = arrData[1];
           $("#txtPinCode").val(vid);
           $("#result").css({ "display": "none" });
       });

       function DeleteRecord() {
           if (m_AddressId == 0) {
               return;
           }
           var isprimary = $("#chkprimary").prop("checked");

           if (isprimary) {
               return;
           }
           if (!confirm('Are you sure to delete current record')) {

               return;
           }

           m_AddressId = $("#hdId").val();

           $.ajax({
               type: "POST",
               data: '{"DeliveryAddressId":"' + m_AddressId + '"}',
               url: "userdeliveryaddress.aspx/Delete",
               contentType: "application/json",
               datatype: "json",
               success: function (msg) {
                   var obj = jQuery.parseJSON(msg.d);
                   if (obj.Status == 1) {
                       BindGrid(<%=Session[Constants.UserId].ToString()%>);
                       alert("Current Address is deleted");
                       ResetControls();
                       return;
                   }
               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {


               }
           });
       }
       function ResetControls() {
           $("#hdId").val("0");
           $("#txtFirstName").val("");
           $("#txtLastName").val("");
           $("#txtMobileNo").val("");
           $("#txtTelephone").val("");
           $("#txtStreet").val("");
           $("#txtArea").val("");
           $("#txtPinCode").val("");
           $("#txtAddress").val("");
           $("#ddlCities").val("");
            $("#ddlSector").val("");
           $("#chkprimary").prop("checked", false);
           $("#btnAdd").html("Add Information");
           $("#btnCancel").css({ display: "none" });
           m_AddressId = 0;
       }
       function BindCities() {
           $.ajax({
               type: "POST",
               data: '',
               url: "userdeliveryaddress.aspx/BindCity",
               contentType: "application/json",
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);


                   $("#ddlCities").html(obj.CitiessOptions);

               },

               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                  // $("#ddlCities option").removeAttr("selected");
                  // $('#ddlCities option[value=' + obj.Customer.AccSubGroupId + ']').prop('selected', 'selected');


               }


           });
       }
       function Insert() {
           if (!validateForm("frmAddress")) {
               return;
           }
           var m_AddressId = $("#hdId").val();
           var FName = $("#txtFirstName").val();
           var LName = $("#txtLastName").val();
           var MobileNo = $("#txtMobileNo").val();
           var Telephone = $("#txtTelephone").val();
           var street = $("#txtStreet").val();
           var area = $("#txtArea").val();
           var pincode = $("#txtPinCode").val();
           var address = $("#txtAddress").val();
           var Isprimary = $("#chkprimary").prop("checked");
           
           var city = $("#ddlCities").val();

           if ($.trim(city) == "") {
               $("#ddlCities").focus();
               alert("Choose city");
               return;
           }

            var sector = $("#ddlSector").val();

           if ($.trim(sector) == "") {
               $("#ddlSector").focus();
               alert("Choose Sector");
               return;
           }
           $.uiLock('');
          // btnAdd.html("<img src='img/loader.gif' alt='loading...'/>")
          

           $.ajax({
               type: "POST",
               data: '{"DeliveryAddressId":"' + m_AddressId + '","RecipientFirstName":"' + $.trim(FName) + '","RecipientLastName":"' + $.trim(LName) + '","MobileNo":"' + $.trim(MobileNo) + '","Telephone":"' + $.trim(Telephone) + '","CityId":"' + $.trim(city) + '","Area":"' + $.trim(area) + '","Street":"' + $.trim(street) + '","Address":"' + address + '","PinCode":"' + pincode + '","IsPrimary":"' + Isprimary + '","PinCodeId":"'+ sector+'"}',
               url: "userdeliveryaddress.aspx/Insert",
               contentType: "application/json",
               datatype: "json",
               success: function (msg) {
                   var obj = jQuery.parseJSON(msg.d);
                   if (obj.Status == 0) {
                       alert("Insertion failed,Plz try again");
                       return;
                   }
                   $.uiUnlock('');
                   if (m_AddressId == "0") {
                       BindGrid(<%=Session[Constants.UserId].ToString()%>);
                       alert("Information is saved successfully");

                       ResetControls();
                       return;
                   }
                   else {
                       BindGrid(<%=Session[Constants.UserId].ToString()%>);
                       alert("Information is updated successfully");
                       ResetControls();


                       return;
                   }
               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   $.uiUnlock();

               }
           });
       }
</script>

   <form id="frmMain" runat="server">
   <input  type ="hidden" id="hdId" value="0"/>
<div id="content" style="padding:6px">
       			
				   <div class="youhave" style="padding-left:30px;">
            <div  class="col-md-12" style="margin-bottom:10px;margin-top: 0px; border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
 
            
                 <table cellpadding="0" cellspacing="0" width="99%"  id="frmAddress">
<tr>
<td  class="headingStyle" colspan="100%" >ADD/EDIT DELIVERY ADDRESS</td>
</tr>

<tr><td style="padding-top:20px"> First Name:</td><td style="padding-top:20px"> <input  type="text" id="txtFirstName" name="txtFirstName" style="width:215px" class="form-control input-small validate required"/>
   </td><td style="padding-top:20px">Last Name:</td><td style="padding-top:20px">

        <input  type="text" id="txtLastName" name="txtLastName" style="width:215px" class="form-control input-small validate required"/>
                           </td></tr>
<tr><td style="padding-top:10px"> Mobile:</td><td style="padding-top:10px">

     <input  type="text" id="txtMobileNo" name="txtMobileNo" style="width:215px" class="form-control input-small validate required" onkeypress="return isNumberKey(event)"/>
                     </td>
    
    <td style="padding-top:10px">Telephone:</td><td style="padding-top:10px">
         <input  type="text" id="txtTelephone" name="txtTelephone" style="width:215px" class="form-control input-small" onkeypress="return isNumberKey(event)"/>
                       </td>                                                                                                                                                                                          
                                                                                    </tr>
<tr>
<td style="padding-top:10px">Street:</td><td style="padding-top:10px">
         <input  type="text" id="txtStreet" name="txtStreet" style="width:215px" class="form-control input-small"/>
            </td>
            <td style="padding-top:10px"> City:</td><td style="padding-top:10px">
    <select id="ddlCities" style="height:30px;width:215px"></select>
     </td>
     
    </tr>
<tr><td style="padding-top:10px"> Area:</td><td style="padding-top:10px">
     <input  type="text" id="txtArea" name="txtArea" style="width:215px" class="form-control input-small"/>
   </td>
             <td style="padding-top:10px"> Sector:</td><td style="padding-top:10px">
    <select id="ddlSector" style="height:30px;width:215px"></select>
     </td>
    </tr>
<tr><td style="padding-top:10px"> Address:</td><td style="padding-top:10px">
     <textarea id="txtAddress" name="txtAddress" style="width:215px" class="form-control input-small validate required"></textarea>
   </td>
    <td style="padding-top:10px">PinCode:</td><td style="padding-top:10px">
         <input  type="text" id="txtPinCode" name="txtPinCode" style="width:215px" readonly="readonly" class="form-control input-small validate required"/>
            <div id="result" style="position:absolute;z-index:9999;background:white;max-height:400px;overflow-y:scroll;display:none;width:370px">
   <div id="tbKeywordSearch" style="border:solid 1px silver;border-top:0px;text-align:center " >
  
 
 
 
   </div>
        
    </div>
         </td>
    
</tr>
<tr>
<td>PrimaryAddress</td>
      <td style="padding-top:10px">
                 <input  type="checkbox" id="chkprimary" />
    </td></tr>
<tr><td></td><td><table><tr><td style="padding-right:10px;padding-top:10px" >
    <div id="btnAdd" class="btn btn-success">Add Information</div>
    </td>
    <td style="padding-right:10px;padding-top:10px" >
        <div id="btnCancel" class="btn btn-success" style="display:none">Cancel</div>
                            </td></tr></table>
   
</td></tr>
</table>
 </div>

 <div  class="col-md-12" style="margin-bottom:20px;margin-top: 0px; border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
   <table style="width:99%">
   
   <tr>
<td  style=" background:black;color:White;font-weight:bold;padding:5px;box-shadow:2px 2px 2px black;" colspan="100%">DELIVERY ADDRESS LIST</td>
</tr>
       <tr>
           <td>
                    <div id="Div1" style="margin-top:10px">
                
				    <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      <div id="btnDelete"  class="btn btn-success" style ="width:120px">Delete Address</div>
                    </div>
			  </div>
           </td>
       </tr>
 </table>
 </div>
                    </div>
			  </div>
           
       </form>
    <script language="javascript" type="text/javascript">
        BindGrid(<%=Session[Constants.UserId].ToString()%>);
        function BindGrid(UserId) {
            jQuery("#jQGridDemo").GridUnload();
            jQuery("#jQGridDemo").jqGrid(
            {               
                url: 'handlers/deliveryaddress.ashx?UserId='+UserId,
                ajaxGridOptions: { contentType: "Application/json" },
                datatype: "json",
                colNames: ['DeliveryAddressId', 'FirstName', 'LastName', 'MobileNo', 'Telephone', 'CityId', 'City', 'Area', 'Street', 'Address', 'PinCode', 'IsPrimary','PinCodeId'],
                colModel: [{ name: 'DeliveryAddressId', key: true, index: 'DeliveryAddressId', stype: 'text', sorttype: 'int', hidden: true },
                        { name: 'RecipientFirstName', index: 'RecipientFirstName', stype: 'text', width: '80px', sortable: true, editable: true, editrules: { required: true } },
                        { name: 'RecipientLastName', index: 'RecipientLastName', stype: 'text', width: '80px', sortable: true, editable: true, editrules: { required: true }, },
                        { name: 'MobileNo', index: 'MobileNo', stype: 'text', width: '120px', sortable: true, editable: true, editrules: { required: true }, hidden: false  },
                        { name: 'Telephone', index: 'Telephone', stype: 'text', width: '120px', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                        { name: 'CityId', index: 'CityId', stype: 'text', width: '80px', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                        { name: 'CityName', index: 'CityName', stype: 'text', width: '80px', sortable: true, editable: true, editrules: { required: true } },
                        { name: 'Area', index: 'Area', stype: 'text', width: '80px', sortable: true, editable: true, editrules: { required: true } , hidden: true},
                        { name: 'Street', index: 'Street', stype: 'text', width: '70px', sortable: true, editable: true, editrules: { required: true } , hidden: true},
                        { name: 'Address', index: 'Address', stype: 'text', width: '70px', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                        { name: 'PinCode', index: 'PinCode', stype: 'text', width: '70px', sortable: true, editable: true, editrules: { required: true } },
                        { name: 'IsPrimary', index: 'IsPrimary', width: '60px', editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                        { name: 'PinCodeId', index: 'PinCodeId', stype: 'text', width: '70px', sortable: true, editable: true, editrules: { required: true },hidden:true },
                ],
                rowNum: 10,

                mtype: 'GET',
                toolbar: [true, "top"],
                loadonce: true,
                rowList: [10, 20, 30],
                pager: '#jQGridDemoPager',
                sortname: 'DeliveryAddressId',
                viewrecords: true,
                height: "100%",
                width: "750px",
                sortorder: 'asc',
                caption: "Address List",
                ignoreCase: true,
                editurl: 'handlers/deliveryaddress.ashx',
                rowattr: function (rd) {
                    if (rd.IsActive == false) {
                        return { "class": "myAltRowClass" };
                    }
                }
            });
            var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                       {
                           refresh: false,
                           edit: false,
                           add: false,
                           del: false,
                           search: false,
                           searchtext: "Search",
                           addtext: "Add",
                       },

                       {//SEARCH
                           closeOnEscape: true

                       }

                         );
            $("#jQGridDemo").jqGrid('setGridParam',
{
    onSelectRow: function (rowid, iRow, iCol, e) {

        var txtFirstName = $("#txtFirstName");
        $("#hdId").val($('#jQGridDemo').jqGrid('getCell', rowid, 'DeliveryAddressId'));

        m_AddressId = $('#jQGridDemo').jqGrid('getCell', rowid, 'DeliveryAddressId');
     
        if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsPrimary') == "true") {
            $('#chkprimary').prop('checked', true);
         
          
            $("#btnDelete").css({ display: "none" });
        }
        else {
            $('#chkprimary').prop('checked', false);
            $("#btnDelete").css({ display: "block" });

        }
        txtFirstName.val($('#jQGridDemo').jqGrid('getCell', rowid, 'RecipientFirstName'));

        $("#txtLastName").val($('#jQGridDemo').jqGrid('getCell', rowid, 'RecipientLastName'))
        $("#txtMobileNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'MobileNo'))
        $("#txtTelephone").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Telephone'))
        $("#ddlCities").val($('#jQGridDemo').jqGrid('getCell', rowid, 'CityId'))
          var CityId = $('#jQGridDemo').jqGrid('getCell', rowid, 'CityId');
          $.ajax({
            type: "POST",
            data: '{"CityId":' + CityId + '}',
            url: "userdeliveryaddress.aspx/BindPincodes",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#ddlSector").html(obj.PinCodeList);


             }, error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function (msg) {
                  $("#ddlSector").val($('#jQGridDemo').jqGrid('getCell', rowid, 'PinCodeId'))

             }

         });
        
        $("#txtArea").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Area'))
        $("#txtStreet").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Street'))
        $("#txtAddress").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Address'))
        $("#txtPinCode").val($('#jQGridDemo').jqGrid('getCell', rowid, 'PinCode'))
      
        txtFirstName.focus();
        $("#btnAdd").css({ "display": "block" });
        $("#btnAdd").html("Update Information");
        $("#btnCancel").css({ display: "block" });
        
      
    }
});
        }
    </script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
<link href="css/sm-core-css.css" rel="stylesheet" type="text/css" />

<link href="css/sm-mint/sm-mint.css" rel="stylesheet" type="text/css" />

<%--<script type="text/javascript" src="libs/jquery-loader.js"></script>--%>
 
<script type="text/javascript" src="jquery.smartmenus.js"></script>
 
<script type="text/javascript">
    $(function () {
        $('#main-menu').smartmenus({
            mainMenuSubOffsetX: -3,
            mainMenuSubOffsetY: -8,
            subMenusSubOffsetX: 1,
            subMenusSubOffsetY: -8,
            subMenusMinWidth: 700

        });

    });
</script>


 <script type="text/javascript">



     //
     // $('#element').donetyping(callback[, timeout=1000])
     // Fires callback when a user has finished typing. This is determined by the time elapsed
     // since the last keystroke and timeout parameter or the blur event--whichever comes first.
     //   @callback: function to be called when even triggers
     //   @timeout:  (default=1000) timeout, in ms, to to wait before triggering event if not
     //              caused by blur.
     // Requires jQuery 1.7+
     //
     ; (function ($) {
         $.fn.extend({
             donetyping: function (callback, timeout) {
                 timeout = timeout || 1e3; // 1 second default timeout
                 var timeoutReference,
                doneTyping = function (el) {
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
                 return this.each(function (i, el) {
                     var $el = $(el);
                     // Chrome Fix (Use keyup over keypress to detect backspace)
                     // thank you @palerdot
                     $el.is(':input') && $el.on('keyup keypress', function (e) {
                         // This catches the backspace button in chrome, but also prevents
                         // the event from triggering too premptively. Without this line,
                         // using tab/shift+tab will make the focused element fire the callback.
                         if (e.type == 'keyup' && e.keyCode != 8) return;

                         // Check if timeout has been set. If it has, "reset" the clock and
                         // start over again.
                         if (timeoutReference) clearTimeout(timeoutReference);
                         timeoutReference = setTimeout(function () {
                             // if we made it here, our timeout has elapsed. Fire the
                             // callback
                             doneTyping(el);
                         }, timeout);
                     }).on('blur', function () {
                         // If we can, fire the event since we're leaving the field
                         doneTyping(el);
                     });
                 });
             }
         });
     })(jQuery);

     $('#txtPinCode').donetyping(function () {
         // $('#example-output').text('Event last fired @ ' + (new Date().toUTCString()));
    
         var Keyword = $(this).val();

         if (Keyword.length >= 2) {

             $("#result").css("display", "block");
             $("#tbKeywordSearch").html("<img src='images/searchloader.gif'>");

             $.ajax({
                 type: "POST",
                 data: '{"Keyword":"' + Keyword + '"}',
                 url: "userdeliveryaddress.aspx/KeywordSearch",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {
                     // var obj = jQuery.parseJSON(msg.d);


                     var Content = msg.d;
                     $("#tbKeywordSearch").html(Content);
                     if (Content.length < 10) {

                         $("#tbKeywordSearch").html('No record found for the Search');
                         $("#txtPinCode").val("");
                         $("#txtPinCode").focus();
                     }







                 }, error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function (msg) {


                 }

             });














         }
         else {


             $("#result").css("display", "none");


         }



     });
    </script>

 

 
<style type="text/css">
	#main-menu {
		position:relative;
		z-index:9999;
		width:100%;
	}
	#main-menu ul {
		width:12em;  
	}
</style>
</asp:Content>

