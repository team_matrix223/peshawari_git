﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/main.master" AutoEventWireup="true" CodeFile="Settings.aspx.cs" Inherits="user_Settings" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cntBreadCrumbs" Runat="Server">

 <div class="breadcrumbs_container">
            <article class="breadcrumbs">
                <a href="dashboard.aspx">Dashboard</a>
                <div class="breadcrumb_divider">
                </div>
                <a href="managereviews.aspx" class="current">NewsLetter Subscription</a>
            
            </article>
        </div>
    <script src="js/customValidation.js" type="text/javascript"></script>
        <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
   <script type="text/javascript" src="../js/jquery.uilock.min.js"></script>
    <script type="text/javascript" src="../js/jquery.uilock.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cntMaster" Runat="Server">

 <script language="javascript" type="text/javascript">



     $(document).ready(
       function () {


           getdetail();

           var m_Subscribe = 0;

           function ResetControls() {

               $("#chkIsSubscribe").prop("checked", false);
           }




           $("#btnAdd").click(
           function () {


               InsertUpdate();


           });




           function InsertUpdate() {

               
               if (!validateForm("frmMain")) {
                   return;
               }

               var NewsLetter = "";
               if ($("#chkIsSubscribe").prop("checked") == true) {
                   NewsLetter = "True";
               }
               else {
                   NewsLetter = "False";
               }


               $.ajax({
                   type: "POST",
                   data: '{ "NewsLetter": "' + NewsLetter + '"}',
                   url: "Settings.aspx/UpdateSubscription",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);





                       if (NewsLetter == "True") {

                           alert("NewLetters Subscribed successfully.");
                       }
                       else {
                           alert("NewLetters Unsubscribed successfully.");

                       }


                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {
                       $.uiUnlock();
                   }
               });

           }




           function getdetail() {


               $.ajax({
                   type: "POST",
                   data: '{ }',
                   url: "Settings.aspx/Filldetail",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);


                       var newletter = obj.Users["NewsLetter"];

                       if (newletter == true) {

                           $("#chkIsSubscribe").prop("checked", true);
                       }
                       else {
                           $("#chkIsSubscribe").prop("checked", false);
                       }


                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {
                       $.uiUnlock();
                   }
               });

           }

       });
     
      

 </script>




<form id="frmMain" runat="server">
   <input  type ="hidden" id="hdId" value="0"/>
<div id="content" style="padding:6px">
       			
				   <div class="youhave" style="padding-left:30px;">
            <div  class="col-md-12" style="margin-bottom:10px;margin-top: 0px; border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
 
            
                 <table cellpadding="0" cellspacing="0" width="99%"  id="frmAddress">
<tr>
<td  class="headingStyle" colspan="100%" >NewsLetter Subscription</td>
</tr>

<tr>
<td colspan="100%">
<table>
<tr>

<td style="padding-top:20px;padding-right:10px"></td><td  style="padding-top:20px"><input type="checkbox" name = "CheckBox" value= "Order" id="chkIsSubscribe"><label for="chkIsSubscribe">Subscribe/Unsubscribe </label></td> 
 
</tr>


<tr><td></td> <td>    <div id="btnAdd" class="btn btn-success">Submit</div></td></tr></table>
   
</td></tr>
</table>
</td>
 </tr>

</table>
 </div>


                    </div>
			  </div>
           
       </form>
</asp:Content>

