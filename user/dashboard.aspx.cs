﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class user_dashboard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCities();
            FillDetail();

        }

    }
    public void BindCities()
    {
        ddlCities.DataSource = new CitiesBLL().GetAll();
        ddlCities.DataTextField = "Title";
        ddlCities.DataValueField = "CityId";
        ddlCities.DataBind();
        ddlCities.Items.Insert(0, "--Select City--");
    }
    public void FillDetail()
    {
        DeliveryAddress objDel = new DeliveryAddress();
        Users objUsers = new Users() { UserId = Convert.ToInt32(Session[Constants.UserId]) };
        new UsersBLL().GetById(objUsers, objDel);
        hdnId.Value = objUsers.UserId.ToString();
        txtFirstName.Text = objUsers.FirstName;
        txtLastName.Text = objUsers.LastName;
        txtUserMobile.Text = objUsers.MobileNo;
        txtEmailId.Text = objUsers.EmailId;
        txtCode.Text = objUsers.Code;

        hdnDeliveryId.Value = objDel.DeliveryAddressId.ToString();
        txtRFirstName.Text = objDel.RecipientFirstName;
        txtRLastName.Text = objDel.RecipientLastName;
        txtMobileNo.Text = objDel.MobileNo;
        txtTelephone.Text = objDel.Telephone;
        txtPinCode.Text = objDel.PinCode;
        txtStreet.Text = objDel.Street;
        txtAddress.Text = objDel.Address;
        txtArea.Text = objDel.Area;
        ddlCities.SelectedValue = objDel.CityId.ToString();
        if (ddlCities.SelectedValue != "--Select City--")
        {
        ddlSector.DataSource = new PinCodeDAL().GetByCityId(Convert.ToInt32(ddlCities.SelectedValue.ToString()));
        ddlSector.DataTextField = "Sector";
        ddlSector.DataValueField = "PinCodeId";
        ddlSector.DataBind();
        ddlSector.Items.Insert(0, "--Select Sector--");
        ddlSector.SelectedValue = objDel.PinCodeId.ToString();}
    }

    public void ResetControls()
    {
        hdnId.Value = "0";
        txtFirstName.Text = "";
        txtLastName.Text = "";
        txtMobileNo.Text = "";

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Users objUser = new Users()
        {
            UserId = Convert.ToInt32(hdnId.Value),
            FirstName = txtFirstName.Text.Trim(),
            LastName = txtLastName.Text.Trim(),
            MobileNo = txtUserMobile.Text,
            IsActive = true,
            AdminId = 0,
            DeliveryAddressId = Convert.ToInt32(hdnDeliveryId.Value),
            Address = txtAddress.Text,
            Area = txtArea.Text,
            Street = txtStreet.Text,
            CityId = Convert.ToInt32(ddlCities.SelectedValue.ToString()),
            EmailId = txtEmailId.Text,
            RMobileNo = txtMobileNo.Text,
            Telephone = txtTelephone.Text,
            RecipientFirstName = txtRFirstName.Text,
            RecipientLastName = txtRLastName.Text,
            IsPrimary = true,
            PinCode = txtPinCode.Text,
            PinCodeId = Convert.ToInt32(ddlSector.SelectedValue.ToString()),
        };
        int result = new UsersBLL().InsertUpdate(objUser);
        if (result == 0)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Current EmailId is already InUse');", true);
            // Response.Write("<script>alert('Current EmailId is already InUse')</script>");
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Your Profile is updated successfully');", true);
            //Response.Write("<script>alert('Your Profile is updated successfully')</script>");
            //   ResetControls();
            FillDetail();
        }
    }
    protected void ddlCities_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtPinCode.Text = "";
        if (ddlCities.SelectedIndex > 0)
        {
            ddlSector.DataSource = new PinCodeDAL().GetByCityId(Convert.ToInt32(ddlCities.SelectedValue.ToString()));
            ddlSector.DataTextField = "Sector";
            ddlSector.DataValueField = "PinCodeId";
            ddlSector.DataBind();
            ddlSector.Items.Insert(0, "--Select Sector--");
        }
    }
    protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtPinCode.Text = "";
        if (ddlSector.SelectedIndex > 0)
        {

            txtPinCode.Text = new PinCodeDAL().GetPinCodeById(Convert.ToInt32(ddlSector.SelectedValue.ToString()));
        }
    }
}