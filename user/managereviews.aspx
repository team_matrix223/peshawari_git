﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/main.master" AutoEventWireup="true" CodeFile="managereviews.aspx.cs" Inherits="user_managereviews" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntBreadCrumbs" Runat="Server">

 <div class="breadcrumbs_container">
            <article class="breadcrumbs">
                <a href="dashboard.aspx">Dashboard</a>
                <div class="breadcrumb_divider">
                </div>
                <a href="managereviews.aspx" class="current">Write a Review</a>
            
            </article>
        </div>
   <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
  <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
  <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
   <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.uilock.min.js"></script>
    <script type="text/javascript" src="../js/jquery.uilock.js"></script>
       <script src="js/customValidation.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMaster" Runat="Server">
 <script language="javascript" type="text/javascript">



     $(document).ready(
       function () {

           var m_ReviewId = 0;

           function ResetControls() {

               $("#txtTitle").val("");
               $("#txtTitle").focus();
               $("#txtDescription").val("");

               $("#<%=ddlRate.ClientID %>").val("");
           }

           $("#btnCancel").click(
           function () {
               ResetControls();
           });


           $("#btnAdd").click(
           function () {


               InsertUpdate();


           });




           function InsertUpdate() {

           
               if (!validateForm("frmMain")) {
                   return;
               }

               var Title = $("#txtTitle").val();
               if ($.trim(Title) == "") {
                   $("#txtTitle").focus();

                   return;
               }

               var Description = $("#txtDescription").val();
               if ($.trim(Description) == "") {
                   $("#Description").focus();

                   return;
               }


               var Rating = $("#<%=ddlRate.ClientID %>").val();

               $.ajax({
                   type: "POST",
                   data: '{ "Title": "' + Title + '","Description": "' + Description + '","Rating": "' + Rating + '"}',
                   url: "managereviews.aspx/Insert",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);

                       if (obj.Status == 0) {

                           alert("Insertion Failed.slot already exists.");
                           return;
                       }

                       if (m_ReviewId == "0") {

                           alert("Review is submited successfully.");
                           ResetControls();
                       }


                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {
                     
                   }
               });

           }

       });
     
      

 </script>


 
 

<form id="frmMain" runat="server">
   <input  type ="hidden" id="hdId" value="0"/>
<div id="content" style="padding:6px">
       			
				   <div class="youhave" style="padding-left:30px;">
            <div  class="col-md-12" style="margin-bottom:10px;margin-top: 0px; border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
 
            
                 <table cellpadding="0" cellspacing="0" width="99%"  id="frmAddress">
<tr>
<td  class="headingStyle" colspan="100%" >Write a Review</td>
</tr>


<tr>
<td colspan="100%">
<table>


<tr><td style="padding-top:20px;width:100px">Title:</td><td style="padding-top:20px"> <input  type="text" id="txtTitle" name="txtTitle" style="width:215px" class="form-control input-small validate required"/>
   </td></tr>

<tr><td style="padding-top:10px"> Description:</td><td style="padding-top:10px">
     <textarea id="txtDescription" name="txtDescription" style="width:215px" class="form-control input-small validate required"></textarea>
   </td>
  
</tr>

<tr><td style="padding-top:20px" >Rating</td><td> <asp:DropDownList ID="ddlRate"    runat="server"  style="width:217px;height:35px">
 <asp:ListItem Value ="Poor" Text ="Poor"></asp:ListItem>
 <asp:ListItem Value ="Average" Text ="Average"></asp:ListItem>
 <asp:ListItem Value ="Good" Text ="Good"></asp:ListItem>
 <asp:ListItem Value ="Excellent" Text ="Excellent"></asp:ListItem>
</asp:DropDownList>
 


</td></tr>

<tr><td></td><td><table><tr><td style="padding-right:10px;padding-top:10px" >
    <div id="btnAdd" class="btn btn-success">Submit</div>
    </td>
    <td style="padding-right:10px;padding-top:10px" >
        <div id="btnCancel" class="btn btn-success" >Reset</div>
                            </td></tr></table>
   
</td></tr>

</table>


</td>
</tr>

</table>
 </div>


                    </div>
			  </div>
           
       </form>
</asp:Content>

