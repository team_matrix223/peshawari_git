﻿<%@ WebHandler Language="C#" Class="deliveryaddress" %>

using System;
using System.Web;

public class deliveryaddress : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;

       Int32 userid =Convert.ToInt32( context.Request.QueryString ["UserId"].ToString());
        //string  userid1 = HttpContext.Current.Response.Cookies[Constants.UserId].Value.ToString();
       // string userid2 = HttpContext.Current.Request.Cookies[Constants.UserId].Value.ToString();
        //string userid3 = context.Request.Cookies[Constants.UserId].Value;
        //oper = null which means its first load.
        var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        var xss = jsonSerializer.Serialize(
         new DeliveryAddressBLL().GetAllByUserId(userid)
           );

        context.Response.Write(xss);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}