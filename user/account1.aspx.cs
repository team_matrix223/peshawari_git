﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class account : System.Web.UI.Page
{
    DataSet ds = null;
    public string ReturnUrl { get { return Request.QueryString["ReturnUrl"] != null ? Request.QueryString["ReturnUrl"] : ""; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindHeaderLinks();
            BindOuterLinks();
            BindProductCategories();
            BindFeaturedCategories();
            BindCities();
        }

    }
    void BindHeaderLinks()
    {
        ds = new Cms().GetAll();
        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "ParentPage=0 and ShowOn=0";
        repHeaderLinks.DataSource = dv;
        repHeaderLinks.DataBind();
    }
    
    void BindFeaturedCategories()
    {
        List<Category> lst = new CategoryBLL().GetFeaturedCategories();
        string str = "";
        foreach (var item in lst)
        {
            str += "<li class='active'><a href='../list.aspx?c=" + item.CategoryId + "' >" + item.Title + "</a></li>";
            //str += "<li class='active'><a href='list.aspx?s=" + item.CategoryId + "' style='color:black'>" + item.Title + "</a></li>";
        }
        ltFeaturedCategories.Text = str;
    }

    void BindProductCategories()
    {

        repCategories.DataSource = new CategoryBLL().GetByParentId(0);
        repCategories.DataBind();

    }

    void BindOuterLinks()
    {
        DataSet ds = new Cms().GetAll();
        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "ParentPage=0 and ShowOn=1";
        repOuterLinks.DataSource = dv;
        repOuterLinks.DataBind();
    }
    public DataView GetInnerLinks(object ParentId)
    {
        DataSet ds = new Cms().GetAll();
        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "ParentPage=" + Convert.ToInt16(ParentId);
        return dv;

    }
    public void BindCities()
    {
        ddlCities.DataSource = new CitiesBLL().GetAll();
        ddlCities.DataTextField = "Title";
        ddlCities.DataValueField = "CityId";
        ddlCities.DataBind();
        ddlCities.Items.Insert(0, "--Select City--");
    }
    public void ResetControls()
    {
        txtUserMobile.Text = "";
        txtFirstName.Text = "";
        txtLastName.Text = "";
        txtEmailId.Text = "";
        txtPswd.Text = "";
        txtCPassword.Text = "";
        txtMobileNo.Text = "";
        txtTelephone.Text = "";
        ddlCities.SelectedIndex = 0;
        txtAddress.Text = "";
        txtArea.Text = "";
        txtStreet.Text = "";
        txtPinCode.Text = "";
        //chkAgree.Checked = false;
        txtRLastName.Text = "";
        txtRFirstName.Text = "";
    }
    protected void btnLogin_Click_Click(object sender, EventArgs e)
    {
        Users objuser = new Users()
        {
            EmailId = txtName.Text.Trim(),
            Password = txtPassword.Text.Trim()

        };

        string status = new UsersBLL().UserLoginCheck(objuser);
        if (status.ToString() == "-1")
        {
            Response.Write("<script>alert('Invalid User Name');</script>");
        }
        else if (status.ToString() == "-2")
        {
            Response.Write("<script>alert('Invalid Password');</script>");
        }
        else if (status.ToString() == "0")
        {
           
            Response.Write("<script>alert('First Register,Then SignIn');</script>");

        }
        else
        {

            Session["Roles"] = status;
        
            Session[Constants.UserId] = status;
            Session[Constants.Email] = txtName.Text.Trim();

            if (ReturnUrl != "")
            {
                Response.Redirect("../delivery.aspx");
            }
            else
            {
                Response.Redirect("dashboard.aspx");
            }
        }
    }
    protected void btnForgetpswd_Click(object sender, EventArgs e)
    {
        string UserName = txtName.Text.Trim();
        string Password = CommonFunctions.GenerateRandomPassword(8);
        new UsersBLL().ResetPassword(UserName, Password);

        try
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress("support@matrixposs.com");
            message.To.Add(new MailAddress(UserName));
            message.Subject = "Password Recovery";
            message.Body = "Dear User. Your new password is:" + Password;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            Response.Write("<script>alert('Please check your inbox for new Password.');</script>");
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('Mailing Server Down. Please try again Later');</script>");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
       // if (chkAgree.Checked == false)
       // {
      //      return;
      //  }
        Users objUser = new Users()
        {
            UserId = 0,
            FirstName = txtFirstName.Text.Trim(),
            LastName = txtLastName.Text.Trim(),
            EmailId = txtEmailId.Text.Trim(),
            Password = txtPswd.Text.Trim(),
            IsActive = true,
            MobileNo = txtUserMobile.Text.Trim(),
            AdminId = 0,
            RecipientFirstName = txtRFirstName.Text.Trim(),
            RecipientLastName = txtRLastName.Text.Trim(),
            RMobileNo = txtMobileNo.Text.Trim(),
            Telephone = txtTelephone.Text.Trim(),
            CityId = Convert.ToInt32(ddlCities.SelectedValue.ToString().Trim()),
            Area = txtArea.Text.Trim(),
            Street = txtStreet.Text.Trim(),
            Address = txtAddress.Text.Trim(),
            PinCode = txtPinCode.Text.Trim(),
            IsPrimary = true,
        };
        int result = new UsersBLL().InsertUpdate(objUser);
        if (result == 0)
        {
            Response.Write("<script>alert('User Registraion is not completed,Plz try again')</script>");
            return;
        }
        else if (result == -1)
        {
            Response.Write("<script>alert('Current EmailId is already In Use')</script>");
            txtEmailId.Focus();
            return;
        }
        else if (result == -2)
        {
            Response.Write("<script>alert('Current MobileNo is already In Use')</script>");
            txtUserMobile.Focus();
            return;
        }
        else
        {
            Session[Constants.UserId] = result;
            Session[Constants.Email] = txtEmailId.Text.Trim();
            if (ReturnUrl != "")
            {
                Response.Redirect("../delivery.aspx");
            }
            else
            {
                Response.Redirect("dashboard.aspx");
            }
           // ResetControls();
        }
       
    }
}