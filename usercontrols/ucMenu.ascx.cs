﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

public partial class usercontrols_ucMenu : System.Web.UI.UserControl
{
    DataSet ds;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
          
         BindLevel1();
        }
    }
  
    void BindLevel1()
    {
        string Title = Request.Url.Segments.Last();
        if (Title.ToLower() == "list.aspx")
        {
            Title = "Search";
        }
        else
        {
            Title = "";
        }
        ds = new CategoryBLL().GetForMenu(Title);
        DataView dvOuter = ds.Tables[0].DefaultView;
        dvOuter.RowFilter = "Level=1";
        DataTable dt = dvOuter.ToTable();
        repOuter.DataSource = dt;
        repOuter.DataBind();

         repMobileMenu.DataSource = dt;
        repMobileMenu.DataBind();
    }


    public DataTable BindLevel2(object CategoryId)
    {
        DataView dvInner = ds.Tables[0].DefaultView;
        dvInner.RowFilter = "ParentId=" +Convert.ToInt32(CategoryId);
        DataTable dtInner = dvInner.ToTable();
        return dtInner;
    }


    

    void BindMenuCategories()
    {
        DataSet ds = new CategoryBLL().GetAll();
        DataView dvOuter = ds.Tables[0].DefaultView;
        dvOuter.RowFilter = "Level=1";
        DataTable dt = dvOuter.ToTable();
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            str.Append("<li><a href='list.aspx?c=" + ds.Tables[0].Rows[i]["CategoryId"] + "'>" + dt.Rows[i]["Title"] + "</a><ul class='mega-menu'><li><div style='width:600px;'><div style='padding:5px 24px;'>");
             int CategoryId = Convert.ToInt32(dt.Rows[i]["CategoryId"]);
            int SubCount = Convert.ToInt32(dt.Rows[i]["SubCount"]);
            str.Append(BindSubCategories(CategoryId, ds.Tables[0], SubCount));
            str.Append("</div></div></li></ul></li>");
        }
      //  ltMenuItems.Text = str.ToString();

    
    }

 

    string BindSubCategories(int CategoryId, DataTable dt, int SubCount)
    {


        string st = "<div class='row'><div class='col-md-4'><h3>Column1</h3>Sub Col 1<br />Sub Col 2<br />Sub Col 3<br />Sub Col 4<br />Sub Col 5<br />Sub Col 6<br /></div>";
        st += "<div class='col-md-4'><h3>Column1</h3>Sub Col 1<br />Sub Col 2<br />Sub Col 3<br />Sub Col 4<br />Sub Col 5<br />Sub Col 6<br /></div>";
        st += "<div class='col-md-4'><h3>Column1</h3>Sub Col 1<br />Sub Col 2<br />Sub Col 3<br />Sub Col 4<br />Sub Col 5<br />Sub Col 6<br /></div></div>";


         return st;

        StringBuilder str = new StringBuilder();
        DataView dvInner = dt.DefaultView;
        dvInner.RowFilter = "ParentId=" + CategoryId;
        DataTable dtInner = dvInner.ToTable();


       

        for (int i = 0; i < dtInner.Rows.Count; i++)
        {
            int SubCnt = Convert.ToInt32(dtInner.Rows[i]["SubCount"]);

            if (SubCnt == 0)
            {
                string Level = dtInner.Rows[i]["Level"].ToString();
                if (Level == "2")
                {

                     
                    
                    str.Append("<div class='row'><div class='col-md-4'><h3><a href='list.aspx?s=" + dtInner.Rows[i]["CategoryId"] + "'>" + dtInner.Rows[i]["Title"] + "</a></h3></div>");
                    
                    
                }
                else if (Level == "3")
                {
                    str.Append("<div class='row'><div class='col-md-4'><h3><a href='list.aspx?sc=" + dtInner.Rows[i]["CategoryId"] + "'>" + dtInner.Rows[i]["Title"] + "</a></h3></div>");


 
                }


            }
            else
            {
                string Level = dtInner.Rows[i]["Level"].ToString();
                if (Level == "2")
                {
                    str.Append("<li class='has-sub'><a  href='list.aspx?s=" + dtInner.Rows[i]["CategoryId"] + "'><span>" + dtInner.Rows[i]["Title"] + "</span><i style='float:right;width:50px;height:30px;left:185px;top:0px;text-align:center;padding-top:8px;padding-left:7px;margin-top:-10px;margin-right:-20px'><img src='images/arrowdown.png'/></i></a><ul>");
                }
                else
                {
                    str.Append("<li class='has-sub'><a  href='list.aspx?sc=" + dtInner.Rows[i]["CategoryId"] + "'><span>" + dtInner.Rows[i]["Title"] + "</span><i style='float:right;width:50px;height:30px;left:185px;top:0px;text-align:center;padding-top:8px;padding-left:7px;margin-top:-10px;margin-right:-20px'><img src='images/arrowdown.png'/></i></a><ul>");

                }
                int cid = Convert.ToInt32(dtInner.Rows[i]["CategoryId"]);
                int sc = Convert.ToInt32(dtInner.Rows[i]["SubCount"]);
                str.Append(BindSubCategories(cid, dt, sc));

                str.Append("</ul></li>");


            }
        }


        return str.ToString();
    }

}