﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucMenu2.ascx.cs" Inherits="usercontrols_ucMenu" %>

<script language="javascript" type="text/javascript">

    $(document).ready(

    function () {


        $("#lnkHome").mouseover(
        function () {

            $('#main-menu').smartmenus('enable');

            $("#dvMenuCategories").show();
        }
        );

        $("#cssmenu").css("width", "100%");

        $("#lnkHome").mouseleave(
                function () {
                  //  $('#main-menu').smartmenus('disable');
                    $("#dvMenuCategories").hide();
                }
                );

        $("#dvMenuCategories").mouseover(
                function () {


                    $('#main-menu').smartmenus('enable');
                    $("#dvMenuCategories").show();
                }
                );

        $("#dvMenuCategories").mouseleave(
                function () {

                    $('#main-menu').smartmenus('disable');
                    $(this).hide();
                }
                );
    
     }
    );


</script>


     <asp:Literal ID="ltMenuItems" runat="server"></asp:Literal>

<ul id="main-menu"  class="sm sm-vertical sm-mint sm-mint-vertical"">
  <li style="background:#6BA52E;color:White"><a href="#" style="color:White;border-radius:0px"> OUR CATEGORIES</a></li>
 
  
     <asp:Repeater ID="repOuter" runat="server">
     <ItemTemplate>
     
    <li>
    <a href="checklist.aspx?c=<%#Eval("CategoryId") %>"><%#Eval("Title") %>( <%#Eval("ProductCount") %>)</a><ul class="mega-menu">
    <li><div style="width:100%;"><div style="padding:5px 8px;">

    <asp:ListView ID="lstInner" runat="server" GroupItemCount="3" DataSource='<%#BindLevel2(Eval("CategoryId")) %>'>
    <LayoutTemplate>
      <div style='width:100%;'>
        <div style='padding:5px 8px;'>


           	 <asp:PlaceHolder ID="groupPlaceHolder" runat="server"></asp:PlaceHolder>
             
             
              </div></div>

    </LayoutTemplate>

    <GroupTemplate>
      <div class='row'>
      <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
       </div>

    </GroupTemplate>
    
    <ItemTemplate>
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="checklist.aspx?s=<%#Eval("CategoryId") %>" style="color:Green;font-size:11px">
        <%#Eval("Title") %> ( <%#Eval("ProductCount") %>)</a> </b>  
        <asp:Repeater ID="repLevel3" runat="server"  DataSource='<%#BindLevel2(Eval("CategoryId")) %>'>
        <ItemTemplate>
       
       <span style="width:100%">
        <a href="checklist.aspx?sc=<%#Eval("CategoryId") %>" style="font-size:11px;margin:3px">
        <%#Eval("Title") %>( <%#Eval("ProductCount") %>)</a>
       </span> 
      
        
        
        </ItemTemplate>
        
        </asp:Repeater>
 
         
       
         </div>


    </ItemTemplate>

    </asp:ListView>


   </li></ul></li>

     </ItemTemplate>
     
     </asp:Repeater>

   
</ul>



<div class="list-group" style="margin:10px 0 0 0;position:relative;width:100%;z-index:99999">
 <%--  <a href="#" class="list-group-item active">OUR CATEGORIES</a>
--%>






   <div id='cssmenu' style="width:94%">
<ul>

<!--LITERAL MENU-->
</ul>
</div>


<asp:Repeater ID="repMenuCategories" runat="server">
<ItemTemplate>
         
   <a href='list.aspx?c=<%#Eval("CategoryId") %>' class="list-group-item" style="text-transform:uppercase"><%#Eval("Title") %>( <%#Eval("ProductCount") %>)</a>
</ItemTemplate>

</asp:Repeater>

    
          </div>
          <img src="images/shado.png" style="margin-top:-12px"/>
      
