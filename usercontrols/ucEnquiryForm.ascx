﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucEnquiryForm.ascx.cs" Inherits="usercontrols_ucEnquiryForm" %>
<div id="contact_form"  style="margin:15px 0 0 30px";  >
	
    
    <div class="row"  style="margin-bottom:10px">
       
        <asp:TextBox ID="txtPersonName" class="inputCommon"    style="font-size:8pt;font-style:italic"  PlaceHolder = "Enter Your Name" runat="server"></asp:TextBox>

         <asp:RequiredFieldValidator ID="reqPersonName"  SetFocusOnError="true"  ValidationGroup="enquiry" runat="server" ControlToValidate="txtPersonName" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
	</div>

    <div class="row" style="margin-bottom:10px">
	 <asp:TextBox ID="txtEmail" class="inputCommon"  style="font-size:8pt;font-style:italic"  PlaceHolder = "Enter Your Email" runat="server"></asp:TextBox>
	  <asp:RequiredFieldValidator ID="reqEmailID" SetFocusOnError="true" Display="Dynamic"  ValidationGroup="enquiry"  runat="server" ControlToValidate="txtEmail" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

	  <asp:RegularExpressionValidator ID="RequiredFieldValidator1" Display="Dynamic"  
            ValidationGroup="enquiry"  runat="server"  SetFocusOnError="true"  ControlToValidate="txtEmail" 
            ForeColor="Red" 
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
            ErrorMessage="*"></asp:RegularExpressionValidator>

    <br />
    </div>

    <div class="row" style="margin-bottom:10px">
    <asp:TextBox ID="txtMessage"  class="input inputEnquiry bigtext"  PlaceHolder = "Help Us Improve Our Services by reporting Issue or Suggesting new Ideas. If we found it Valuable, you will get a Gift Voucher. Thank you." Multiline ="true"  
            runat="server" Height="85px"  Width="190px"  TextMode="MultiLine"></asp:TextBox>
              <asp:RequiredFieldValidator  ValidationGroup="enquiry"  SetFocusOnError="true"   ID="reqEnquiryQuestion" runat="server" ControlToValidate="txtMessage" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
	</div>
    
    
	<div class="row" style="margin-bottom:10px" >
	 <asp:TextBox ID="txtMobile"  class="inputCommon"    style="font-size:8pt;font-style:italic"  PlaceHolder = "Enter Your MobileNo" runat="server"></asp:TextBox>
	
    </div>
 <asp:Button ID="btnAsk" runat="server"   ValidationGroup="enquiry"    Text="Send"  class="btn btn-success" 
        onclick="btnAsk_Click"/>


  
    </div>