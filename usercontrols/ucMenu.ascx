﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucMenu.ascx.cs" Inherits="usercontrols_ucMenu" %>

<script language="javascript" type="text/javascript">

    $(document).ready(

    function () {


        $("#lnkHome").mouseover(
        function () {

            $('#main-menu').smartmenus('enable');

            $("#dvMenuCategories").show();
        }
        );

        $("#cssmenu").css("width", "100%");

        $("#lnkHome").mouseleave(
                function () {
                  //  $('#main-menu').smartmenus('disable');
                    $("#dvMenuCategories").hide();
                }
                );

        $("#dvMenuCategories").mouseover(
                function () {


                    $('#main-menu').smartmenus('enable');
                    $("#dvMenuCategories").show();
                }
                );

        $("#dvMenuCategories").mouseleave(
                function () {

                    $('#main-menu').smartmenus('disable');
                    $(this).hide();
                }
                );
    
     }
    );


</script>

<nav class="navbar navbar-inverse mobMenu">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button data-target="#myNavbar" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar">
                        </span>
                    </button>
                    <a href="../index.aspx" style="color: WHITE;" class="navbar-brand">PSM</a>
                </div>
                <div id="myNavbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">

                      <asp:Repeater ID="repMobileMenu" runat="server">
     <ItemTemplate>

      <li  class="active"><a href="<%#Eval("Url") %>"><%#Eval("Title") %> </a></li>
       
     </ItemTemplate>
     </asp:Repeater>


                        
                        

                         
                        <li><a style="color: #000000" href="user/account.aspx">MY ACCOUNT</a></li>
                         
                        
                    </ul>
                </div>
            </div>
        </nav>


     <asp:Literal ID="ltMenuItems" runat="server"></asp:Literal>


<div class="pcMenu">
<ul id="main-menu"   class="sm sm-mint" style="display:none;z-index:99">
  <li style="background:#6BA52E;color:White"><a href="index.aspx" style="color:White;border-radius:0px;padding:12px">Home</a></li>
 
  
     <asp:Repeater ID="repOuter" runat="server">
     <ItemTemplate>
     
    <li>
    <a href="<%#Eval("Url") %>"><%#Eval("Title") %> </a><ul class="mega-menu">
    <li><div style="width:100%;"><div style="padding:5px 8px;">

    <asp:ListView ID="lstInner" runat="server" GroupItemCount="3" DataSource='<%#BindLevel2(Eval("CategoryId")) %>'>
    <LayoutTemplate>
      <div style='width:100%;'>
        <div style='padding:5px 8px;'>


           	 <asp:PlaceHolder ID="groupPlaceHolder" runat="server"></asp:PlaceHolder>
             
             
              </div></div>

    </LayoutTemplate>

    <GroupTemplate>
      <div class='row'>
      <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
       </div>

    </GroupTemplate>
    
    <ItemTemplate>
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="<%#Eval("Url") %>" style="color:Green;font-size:11px">
        <%#Eval("Title") %> ( <%#Eval("ProductCount") %>)</a> </b>  
        <asp:Repeater ID="repLevel3" runat="server"  DataSource='<%#BindLevel2(Eval("CategoryId")) %>'>
        <ItemTemplate>
       
       <span style="width:100%">
        <a href="<%#Eval("Url") %>" style="font-size:11px;margin:3px">
        <%#Eval("Title") %>( <%#Eval("ProductCount") %>)</a>
       </span> 
      
        
        
        </ItemTemplate>
        
        </asp:Repeater>
 
         
       
         </div>


    </ItemTemplate>

    </asp:ListView>


   </li></ul></li>

     </ItemTemplate>
     
     </asp:Repeater>

     <li style="background:#6BA52E;color:White"><a href="user/account.aspx" style="color:White;border-radius:0px;padding:12px">Login</a></li>
 
</ul>
</div>


<div class="list-group" style="margin:10px 0 0 0;position:relative;width:100%;z-index:99999">
 <%--  <a href="#" class="list-group-item active">OUR CATEGORIES</a>
--%>






   <div id='cssmenu' style="width:94%">
<ul>

<!--LITERAL MENU-->
</ul>
</div>


<asp:Repeater ID="repMenuCategories" runat="server">
<ItemTemplate>
         
   <a href='list.aspx?c=<%#Eval("CategoryId") %>' class="list-group-item" style="text-transform:uppercase"><%#Eval("Title") %>( <%#Eval("ProductCount") %>)</a>
</ItemTemplate>

</asp:Repeater>

    
          </div>
        