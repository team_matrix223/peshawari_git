<%@ Page Language="c#" %>

<%@ Import Namespace="SFA" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>SFAResponse</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <script language="C#" runat="server">
        public void display()
        {

            PGResponse oPgResp = new PGResponse();
            EncryptionUtil lEncUtil = new EncryptionUtil();
            string respcd = null;
            string respmsg = null;
            string astrResponseData = null;
            string strMerchantId, astrFileName = null;
            string strKey = null;
            string strDigest = null;
            string astrsfaDigest = null;

            strMerchantId = "00215442";
            astrFileName = "c://key//00215442.key";

            if (Request.ServerVariables["REQUEST_METHOD"] == "POST")
            {

                astrResponseData = Request.Form["DATA"];
                strDigest = Request.Form["EncryptedData"];
                astrsfaDigest = lEncUtil.getHMAC(astrResponseData, astrFileName, strMerchantId);

                if (strDigest.Equals(astrsfaDigest))
                {
                    oPgResp.getResponse(astrResponseData);
                    respcd = oPgResp.RespCode;
                    respmsg = oPgResp.RespMessage;
                }
            }

            //Response.Write ("Response code      :" +oPgResp.RespCode);
            //Response.Write  ("<br>");
            //Response.Write ("\nResponse Message :" + oPgResp.RespMessage);
            //Response.Write  ("<br>");
            //Response.Write ("\nMerchant Txn Id  :" + oPgResp.TxnId);
            //Response.Write  ("<br>");
            //Response.Write ("\nEpg Txn Id		:" + oPgResp.EPGTxnId);
            //Response.Write  ("<br>");
            //Response.Write ("\nAuthId Code		:" + oPgResp.AuthIdCode);
            //Response.Write  ("<br>");
            //Response.Write ("RRN			    :" + oPgResp.RRN);
            //Response.Write  ("<br>");
            //Response.Write ("CVRESP Code	    :" + oPgResp.CVRespCode);
            //Response.Write  ("<br>");
            //Response.Write ("Cookie String	    :" + oPgResp.Cookie);
            //Response.Write  ("<br>");
            //Response.Write ("FDMS Score		    :" + oPgResp.FDMSScore);
            //Response.Write  ("<br>");
            //Response.Write ("FDMS Result        :" +oPgResp.FDMSResult);

            Transaction objTransaction = new Transaction();
            objTransaction.Responsecode = oPgResp.RespCode;
            objTransaction.ResponseMessage = oPgResp.RespMessage;
            objTransaction.MerchantTxnId = oPgResp.TxnId;
            objTransaction.EpgTxnId = oPgResp.EPGTxnId;
            objTransaction.AuthIdCode = oPgResp.AuthIdCode;
            objTransaction.RRN = oPgResp.RRN;
            objTransaction.CVRESPCode = oPgResp.CVRespCode;
            objTransaction.CookieString = oPgResp.Cookie;
            objTransaction.FDMSScore = oPgResp.FDMSScore;
            objTransaction.FDMSResult = oPgResp.FDMSResult;

            new TransactionDAL().InsertUpdate(objTransaction);
            if (oPgResp.RespCode == "0")
            {
                new CartBLL().UserCartDelete(Session.SessionID);

                Response.Redirect("OrderSuccess.aspx");



            }
            else
            {
               //Session[Constants.OrderId] = oPgResp.TxnId;
                Session["Error"] = oPgResp.RespMessage;
                Response.Redirect("repay.aspx");

            }

        }
    </script>
</head>
<body ms_positioning="GridLayout">
    <%
        display();
    %>
</body>
</html>
