﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="dashboard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 
 
<head>

<style type="text/css">
    section#secondary_bar {
    background: url("images/secondary_bar.png") repeat-x scroll 0 0 #f1f1f4;
    height: 38px;
    width: 100%;
}
section#secondary_bar .user {
    float: left;
    height: 38px;
    width: 23%;
}
    section#secondary_bar .breadcrumbs_container {
    background:  no-repeat scroll left top rgba(0, 0, 0, 0);
    float: left;
    height: 38px;
    width: 100%;
}
article.breadcrumbs {
    border: 1px solid #ccc;
    border-radius: 5px;
    box-shadow: 0 1px 0 #fff;
    float: left;
    height: 25px;
    margin: 2px 0%;
    padding: 0 10px;
}
.breadcrumbs a {
    display: inline-block;
    float: left;
    height: 24px;
    line-height: 23px;
}
.breadcrumbs a.current, .breadcrumbs a.current:hover {
    color: #9e9e9e;
    font-weight: bold;
    text-decoration: none;
    text-shadow: 0 1px 0 #fff;
}
.breadcrumbs a:link, .breadcrumbs a:visited {
    color: #44474f;
    font-weight: bold;
    text-decoration: none;
    text-shadow: 0 1px 0 #fff;
}
.breadcrumbs a:hover {
    color: #222222;
}
.breadcrumb_divider {
    background: url("images/breadcrumb_divider.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
    display: inline-block;
    float: left;
    height: 24px;
    margin: 0 5px;
    width: 12px;
}
    
    
    
    
    
    
    
    
ul.vert-one{margin:0;padding:0;list-style-type:none;display:block;
font:bold 14px Helvetica, Verdana, Arial, sans-serif;line-height:165%;
width:200px;}

ul.vert-one li{margin:0;padding:0;border-top:1px solid #4D0000;
border-bottom:1px solid #761A1A;}

ul.vert-one li a{display:block;text-decoration:none;color:#fff;
background:#600;padding:0 0 0 20px;}

ul.vert-one li a:hover{
background:#900 url("images/vert-one_arrow.gif") no-repeat 0 9px;}

ul.vert-one li a.current,ul.vert-one li a.current:hover{
background:#933 url("images/vert-one_arrow.gif") no-repeat 0 9px;}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<script src="js/bootstrap.min.js" type="text/javascript" ></script>
<script src="js/jquery.min.js" type="text/javascript" ></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Index</title>
</head>

<body>


<div class="container-fluid">
<div class="row" style="background:black;">
<div class="col-xs-12">
<div class="menu">
        <ul>
        <li><a href="#">My Account</a></li>
        <li><a href="#">Help</a></li>
        <li><a href="#">Checkout</a></li>
        </ul>
      </div>
</div>
</div>

<div class="row">
    <div class="col-md-9">
    <img src="images/logo.png" alt="" />
    </div>
    
    <div class="col-md-3">
    <div class="phone">
    <ul>
        <h3>Order on call:</h3>
        <li><a href="#">21 75 01 89 23</a></li>
        </ul>
        </div>
      
      <div class="item">
         <table>
 <tr>
 <td><img src="images/shopping-cart.png" alt="" /></td>
 <td><h5>MY CART</h5>
</td>
 </tr>
 <tr><td colspan="100%">  <p style="font-size:12px; color:#808182;">item(s) -00.00</p></td></tr>
 </table>
   
      </div>
      
    </div>
  </div>
 
 
 <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" style="color:#000000;" href="#">Menu</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li class="active"><a href="#">Fruits</a></li>
        <li class="active"><a href="#">Vegetables</a></li>
        <li class="active"><a href="#">Quick order</a></li>
        <li class="active"><a href="#">About us</a></li>
        <li class="active"><a href="#">Bulk order </a></li>
        <li class="active"><a href="#"> Quick order </a></li>
        <li class="active"><a href="#"> FAQ's  </a></li>
        <li><a href="#" style="color:#000000"; />JOIN FREE</a></li>
        <li><a href="#" style="color:#000000"; />SIGN IN</a></li>
        
      </ul>

        
   
    </div>
  </div>
</nav> 
 
 
 
 


<!--Row Start-->
<div class="row" style="margin-top:15px;margin-bottom:15px">

<div class="col-md-12" >
<div class="row">
<div class="col-xs-12"  >
 
</div>

</div>

<div  class="col-md-12" style=" border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
 
 <div class="row">
 <div class="col-md-12">
 <section id="secondary_bar">
       

        
   <div class="breadcrumbs_container">
            <article class="breadcrumbs">
                <a href="#">Dashboard</a>
                <div class="breadcrumb_divider">
                </div>
                <a class="current">Change Password</a></article>
        </div>



     
    </section>
 </div>
 
 </div>

<div class="row">
<div class="col-md-3">
 

<ul class="vert-one">
  <li ><a href="#" title="CSS Menus" style="color:White;background:black;font-size:16px;font-weight:bold;padding:5px;padding-left:18px">QUICK LINKS</a></li>
  
  <li><a href="#" title="CSS Menus" class="current">Home</a></li>
  <li><a href="#" title="CSS Menus">Edit Profile</a></li>
  <li><a href="#" title="CSS Menus" >Change Password</a></li>
  <li><a href="#" title="CSS Menus">Delivery Addresses</a></li>
  <li><a href="#" title="CSS Menus">Order History</a></li>
  <li><a href="#" title="CSS Menus">My Combos</a></li>
  <li><a href="#" title="CSS Menus">Sign Out</a></li>
</ul>
        

<br />

</div>


<div class="col-md-9">
<br />

 test
<br />

</div>
 
</div>


</div>
</div>

 

 


</div>
<!--Row End-->


<!--Row Start-->
 





<div class="row">
<div class="col-md-4">
<div class="paypal">
<img src="images/footer-img.png" alt="" />
</div>
</div>
<div class="col-md-4">
<div class="paypal">
<table>
<tr><td><h4 style="font-size:18px; font-weight:700;">Are you Hooked on Gadgets?</h4></td>
<tr><td><h4 style="font-size:14px; padding:5px 0 0 0; font-weight:300;">Then, you will love our Mobile Apps
Download the HungerQuenchers Apps on</h4></td></tr>
<td style="vertical-align:top;padding-top:8px"><img src="images/googleply.png" style="margin:5px" alt=""> <img src="images/appstore.png" style="margin:5px" alt=""></td>
<tr><td><h4 style="font-size:14px; font-weight:300;">and Enjoy our Services on the Go</h4></td>

</table>
</div>
</div>
<div class="col-md-4">
<div class="paypal">
<img src="images/paypalimg.png" alt="" />

</div>
</div>

</div>


<!-footer start--->

<!--Row Fluid Start-->


  <div class="row" style="background:#1F1F1F no-repeat; margin:5px 0 0px 0;padding-bottom:10px">
            <div class="col-md-4">
			<div class="link">
		    <h2>Categories</h2>
            <ul>
            <li>Grocery</li>
            <li>Household</li>
            <li>Personal Care</li>
            <li>Fruits and Vegetables</li>
            <li>Confectionery and Biscuits</li>
            <li>Beverages</li>
            <li>Baby Needs</li>
            </ul>
        </div>
   </div>
   
   
            <div class="col-md-4">
			<div class="link">
		    <h2>Information</h2>
            <ul>
            <li>Contact</li>
            <li>FAQ</li>
            <li>About us</li>
            <li>Used Equipment For Sale</li>
            <li>Request Info</li>
            </ul>
        </div>
   </div>
   
   
            <div class="col-md-4">
			<div class="link">
		    <h2>Contact</h2>
            <ul>
            <li>040-27704444</li>
            <li>Subscribe to our mailing list</li>
            <li><input id="email" class="input " name="email" type="text" value="" size="30" style="margin:10px 0 0 0px"  /></li>
            <li><button type="button" style="margin:10px 0 0 100px" class="btn btn-success">subscribe</button></li>
            </ul>
        </div>
        
   </div>

                    
                

                
</div>              
<!--Row Fluid End-->     
          
<!-footer End--->

</div>

</body>
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
</html>
