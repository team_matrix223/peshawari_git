﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class basket : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Session["dummy"] == null)
        {
            this.Session["dummy"] = 1;
        }
        //if (new CartBLL().ValidateCheckout(Session.SessionID) == 0)
        //{
        //    Response.Redirect("index.aspx");
        //}
        Int32 totitems = new CartBLL().GetTotalItems(HttpContext.Current.Session.SessionID);
        Master.TotalItems = totitems;
    }

    [WebMethod]
    public static string InsertComments(string Comments)
    {
        string SessionId = HttpContext.Current.Session.SessionID;
        Int32 st = new CartBLL().InsertComments(Comments, SessionId);

        return st.ToString();
    }


    [WebMethod]
    public static string ValidateCheckOut()
    {
        string SessionId = HttpContext.Current.Session.SessionID;
        int retVal = new CartBLL().ValidateCheckout(SessionId);

        return retVal.ToString();
    }
    [WebMethod]
    public static string ATC(string vid, string qty, string st, string type)
    {
        string m_Status = "Plus";

        if (st == "m")
        {
            m_Status = "Minus";

        }



        Int16 IsError = 0;




        int m_Qty = CommonFunctions.ValidateQuantity(qty);
        if (!CommonFunctions.IsValidProductId(vid))
        {
            IsError = 1;
        }

        Calc objCalc = new Calc();
        UserCart objUserCart = new UserCart();
        string html = "";
    
        if (IsError == 0)
        {
            objUserCart.SessionId = HttpContext.Current.Session.SessionID;
            objUserCart.ProductId = 0;
            objUserCart.CustomerId = 0;
            objUserCart.VariationId = Convert.ToInt64(vid);
            objUserCart.ProductDesc = "";
            objUserCart.Qty = m_Qty;
            objUserCart.Price = 0;
            objUserCart.type = type;

            html = new CartBLL().UserCartIncrDecr(objUserCart, m_Status, objCalc);

        }
       
          var JsonData = new
        {
        
            qty = objUserCart.Qty,
            vid = vid,
            error = IsError,
            cartQtyHTML = html,
            Calc = objCalc
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string GetCartHTML()
    {

        Calc objCalc = new Calc();
        string cart = new CartBLL().GetBasketHTML(HttpContext.Current.Session.SessionID, objCalc);
      
        var JsonData = new
        {
            MCOA = objCalc.MinimumCheckOutAmt,
            FDA = objCalc.FreeDeliveryAmt,

            DC = objCalc.DeliveryCharges,
            ST = objCalc.SubTotal,
            NA = objCalc.NetAmount,
            html = cart,
            TotalItems=objCalc.TotalItems,
            Comments = objCalc.Comments 
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

}