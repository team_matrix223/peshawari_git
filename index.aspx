﻿<%@ Page Title="" Language="C#" MasterPageFile="main.master"  EnableEventValidation="false" EnableViewState="false"  AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<%@ Register src="usercontrols/ucEnquiryForm.ascx" tagname="ucEnquiryForm" tagprefix="uc1" %>
 <%@ MasterType VirtualPath="main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnMaster" Runat="Server">
    
        <link rel="shortcut icon" type="image/png" href="img/pslogo.png">
    <script src="jquery-1.8.3.min"></script>

     <script language="javascript" type="text/javascript">
      var ListStatus="";
     BindCustomGroups();




     
      $(document).keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '27') {
                var dialogDiv1 = $('#dvListProductDetail');
                dialogDiv1.dialog("option", "position", [500, 200]);
                dialogDiv1.dialog('close');
            }

        });

     function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                alert("Please Enter Only Numeric Value:");
                return false;
            }

            return true;
        }
    function BindMyList()
    { $.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/GetMyLists",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
            var obj = jQuery.parseJSON(msg.d);
            $("#tbMyList").html(obj.MyLists);
              $("#dvMyList").dialog({ modal: true, closeOnEscape: false, width: 550 });
                $(".ui-dialog-titlebar").hide();
             

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });    
    
    }
    
    
     $(document).ready(
    function () {
         $("#dvCloseMe").click(
     function()
     {
     $("#dvListProductDetail").dialog("close");
     }
     
     );
   
   
        $.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/GetFeaturedCategoriesAndProductsList",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                $("#imgloading").remove();

                $("#featuredCategories").html(msg.d);



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });


    BindBottomAdds();
    BindRightAdds();

    CheckAddress(); 

    
    $("#zipcheck").click(function () {
        var city = $("#<%=ddlCity.ClientID%> option:selected").text();
        var pincode = $("#<%=ddlPincode.ClientID%> option:selected").text();
      
        if (city == "") {
            $("#<%=ddlCity.ClientID%>").focus();
            return;
        }
        if (pincode == "" || pincode == "Please wait while loading..") {
            $("#<%=ddlPincode.ClientID%>").focus();
            return;
        }
         var Id = $("#<%=ddlPincode.ClientID%>").val();
        $.ajax({
            type: "POST",
            data: '{"Id":"'+ Id +'"}',
            url: "index.aspx/RestoreAddress",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
             $("#sp_SelectedCity").html(msg.d);

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {
              var dialogDiv = $('#dvPopupCheck');
        dialogDiv.dialog("option", "position", [500, 200]);
        dialogDiv.dialog('close');

            }

        });
      
    });
   

    BindCities();
    
    $("#<%=ddlCity.ClientID%>").change(function () {
  

        var CityId = $("#<%=ddlCity.ClientID%>").val();
        if(CityId=="")
        {
         $("#<%=ddlPincode.ClientID%>").html("");
        return;
        }
        BindPincodes(CityId);
    });

       
       
       
        $("#dvMenu").css("display", "block");
            
       
       
        $("#featuredCategories").html("<img id='imgloading' src='images/ajax-loader.gif' style='margin-top:200px;margin-left:300px' alt='loading please wait..'/>");

 


        BindCart();
       




        $.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/GetComboProductsFrontEndHtml",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                $("#imgloading").remove();
                if($.trim(msg.d)=="")
                {
                $("#comboheader").css({"display":"none"});
                  $("#ComboProducts").append("");
                }
                else{ 
                $("#comboheader").css({"display":"block"});
                  $("#ComboProducts").append(msg.d);
                }
              



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
        $("#btnCancelMyList").click(
            function () {
                var dialogDiv = $('#dvMyList');
                dialogDiv.dialog("option", "position", [500, 200]);
                dialogDiv.dialog('close');
                return false;
            });
        $("#btnCloseListPopUp").click(
            function () {
                var dialogDiv = $('#dvListName');
                dialogDiv.dialog("option", "position", [500, 200]);
                dialogDiv.dialog('close');
                return false;
            });
       
            $("#btnclose").click(
    function(){
      var dialogDiv = $('#inner');
        dialogDiv.dialog("option", "position", [500, 200]);
        dialogDiv.dialog('close');
    }
    );
    $("#btnlogin").click(
    function(){

      var MobileNo = $("#txtName").val();
      if(MobileNo == "")
      {
         alert("Enter Username");
         $("#txtName").focus();
         return;
      }
      var Password = $("#txtPassword").val();
      if(Password == "")
      {
         alert("Enter Password");
         $("#txtPassword").focus();
         return;
      }

    $.ajax({
                    type: "POST",
                    data: '{"MobileNo": "' + MobileNo + '","Password": "' + Password + '"}',

                    url: "index.aspx/Login",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                        
                        if (obj.Status == "-1") {
                            alert("Invalid User Name.");
                            $("#txtName").focus();
                            return;
                        }
                        else if(obj.Status == "-2")
                        {
                             alert("Invalid Password");
                             $("#txtPassword").focus();
                             return;
                        }
                        else if(obj.Status == "0")
                        {
                             alert("First Register,Then SignIn");
                             return;
                        }
                        else
                        {         
                 var dialogDiv = $('#inner');
        dialogDiv.dialog("option", "position", [500, 200]);
        dialogDiv.dialog('close');
                       if(ListStatus=="Save")
                       {

                        $("#dvListName").dialog({ modal: true, closeOnEscape: false, width: 399 });
                $(".ui-dialog-titlebar").hide();

                           }
                           else
                           {         BindMyList();
                           }
                        }



                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {


                        $.uiUnlock();
                    }


                });


    
    }
    );
     $("#btnAddList").click(
function () {
var ListName=$("#txtListName").val() ;
if($.trim(ListName)=="")
{
$("#txtListName").focus();
return;
}
$.ajax({
            type: "POST",
            data: '{"ListName":"'+ListName+'"}',
            url: "index.aspx/SaveList",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
             
              if(msg.d!="0")
              {$("#txtListName").val("") ;
               alert("List is saved successfully");
                 var dialogDiv = $('#dvListName');
                dialogDiv.dialog("option", "position", [500, 200]);
                dialogDiv.dialog('close');
                return false;
              }
              else{ 
              alert("List is not saved,Try again");    
              }

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

            }

        });

});
  $("#btnMyList").click(
function () {
ListStatus="View";
$.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/CheckUser",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
             
              if(msg.d=="0")
              {
                 $("#inner").dialog({ modal: true, closeOnEscape: false, width: 399 });
                $(".ui-dialog-titlebar").hide();
              }
              else{
             BindMyList();
              }

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
 
});
              $("#btnSaveList").click(
function () {
ListStatus="Save";
$.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/CheckUser",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
             
              if(msg.d=="0")
              {
                 $("#inner").dialog({ modal: true, closeOnEscape: false, width: 399 });
                $(".ui-dialog-titlebar").hide();
              }
              else{       $("#dvListName").dialog({ modal: true, closeOnEscape: false, width: 399 });
                $(".ui-dialog-titlebar").hide();

              }

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });

});
        $("#closepopup").click(
            function () {
                var dialogDiv = $('#dvPopupProduct');
                dialogDiv.dialog("option", "position", [500, 200]);
                dialogDiv.dialog('close');
                return false;
            });
        $("#btnCheckOut").click(
function () {
    var totalAmt = Number($("#hdTotalAmt").val());
           var minAmt = Number($("#hdMinimumCheckOutAmt").val());
           if (totalAmt < minAmt) {
               $("#sp_msg").html("* Order Amount should be greater than " + minAmt);
               return false;
           }
           else {



              $.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/ValidateCheckOut",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
              

              if(msg.d=="1")
              {
                  var url = "basket.aspx";
               $(location).attr('href', url);
              }
              else
              {
                $("#sp_msg").html("* Insufficient Order Amount for Checkout");
              }



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });





           
           }
       });
      }
    );
     
     

    function BindRightAdds()
    {
         var Location = "HomeRight";
         var Top = "1";


       $.ajax({
            type: "POST",
            data: '{ "Location":  "' + Location + '","Top":  "' + Top + '"}',
            url: "index.aspx/BindSideAdds",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
              
              $("#dvRightAdd").html(msg.d);

               


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
    
    
    
    }



function BindBottomAdds()
    {
         var Location = "Bottom";
         var Top = "2";


       $.ajax({
            type: "POST",
            data: '{ "Location":  "' + Location + '","Top":  "' + Top + '"}',
            url: "index.aspx/BindBottomAdds",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
              
              $("#dvBottomAdd").html(msg.d);

               


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
    
    
    }


    $(document).on("change", "select[name='variation']", function (event)
     {
       var pid = $(this).attr("id");
       var oldSelVal=$(this).attr("selvar");
          var arrPid = pid.split('_');
        var fPid=  arrPid[1];
       var vid = $(this).val();
 
 
     $(this).find("option[value='"+oldSelVal+"']").prop("selected",true);
        
       
       $("div[name='product_"+fPid+"']").css("display","none");
 

       $("#variation_"+vid).css("display","block");
 


       });


    $(document).on("click", "div[name='decr']", function (event) {
            var data = $(this).attr("id");
            

        var arrData = data.split('_');
        var vid = arrData[1];
      $("#cqty_"+vid).css("height","20px");
        
//           $("#cqty_"+vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");
//         
  var tempQty=$("#cqty_"+vid).html();

        var tempQ=Number(tempQty)-1;
         if (tempQ <= 1) {
            tempQ = 1;
        }
           $("#cqty_"+vid).html(tempQ);
        var qty = 1; 
        var st = "m";
        var type = "Product";
          ATC(vid, qty, st, type);
    });


    $(document).on("click", "div[name='incr']", function (event) {


        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = 1; 
        var st = "p";
        var type = "Product";
           $("#cqty_"+vid).css("height","20px");
        
        var tempQty=$("#cqty_"+vid).html();

        var tempQ=Number(tempQty)+1;
         
           $("#cqty_"+vid).html(tempQ);
         
          ATC(vid, qty, st, type);

//        var pid = $(this).attr("id");
//        var arrPid = pid.split('_');
//        var vid = $("#d_" + arrPid[1]).val();
//        var qty = $("#q_" + arrPid[1]).val();
//        var st = "p";
//        var type = "Product";
//        ATC(pid, vid, qty, st,type);


    });


    $(document).on("click", "div[name='Combodecr']", function (event) {
        var pid = $(this).attr("id");
        var arrPid = pid.split('_');
        var vid = $("#d_" + arrPid[1]).val();
        var qty = $("#qc_" + arrPid[1]).val();
        var st = "m";
        var type = "Combo";
        ATC(pid, vid, qty, st, type);
    });

       $(document).on("click", "div[name='dvClose']", function (event) {

        var vid = $(this).attr("id");
        var st = "m";
        var type = "Combo";
        DFC(vid, st,type);

     

    });
    $(document).on("click", "div[name='Comboincr']", function (event) {

        var pid = $(this).attr("id");
        var arrPid = pid.split('_');
        var vid = $("#d_" + arrPid[1]).val();
        var qty = $("#qc_" + arrPid[1]).val();
        var st = "p";
        var type = "Combo";
        ATC(pid, vid, qty, st, type);


    });
    
     function DFC(vid,st,type) {
        $.ajax({
            type: "POST",
            data: '{"vid":"' + vid + '","st":"' + st + '","type":"'+ type +'"}',
            url: "index.aspx/RemoveFromCart",
            async:false,
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);

                if (obj.error == 1) {

                    alert("An Error occured during transaction. Please refresh the page and try again Later.");
                    return;
                }


               
                  $("#variation_" + vid).html(obj.productHTML);
                  BindCart();
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
    
    }

    function ATC(vid, qty, st, type) {
        $.ajax({
            type: "POST",
            data: '{"vid":"' + vid + '","qty":"' + qty + '","st":"' + st + '","type":"'+ type +'"}',
            url: "index.aspx/FirstTimeATC",
            async:false,
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);

                if (obj.error == 1) {

                    alert("An Error occured during transaction. Please refresh the page and try again Later.");
                    return;
                }


                if (type == "Combo") {

                 $("#cartcontainer").html(obj.cartHTML);

                    $("#cbox_" + obj.pid).html(obj.cartHTML);


                    if (obj.qty <= 0) {

                        $("#cbox_" + obj.pid).css("background", "white")

                    }
                    else {

                        $("#cbox_" + obj.pid).css("background", "#D8EDC0")

                    }



                }
                else {


                 //$("#cartcontainer").html(obj.cartHTML);
               
           
                   $("#variation_" + vid).html(obj.productHTML);


//                    if (obj.qty <= 0) {

//                        $("#box_" + obj.pid).css("background", "white")

//                    }
//                    else {

//                        $("#box_" + obj.pid).css("background", "#D8EDC0")

//                    }

                }

             
                //$("#cqty_" + obj.pid).html(totalQ);
             

                  BindCart();
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
    
    }
    $(document).on("click", "div[name='dvAddToCartMyList']", function (event) {

        var data = $(this).attr("id");
        var arrData = data.split('_');
        var Listid = arrData[1];
       var KeepExisting =false;
       if($("#chkKeepExisting").is(":checked"))
       {KeepExisting=true;
       }
       
        var qty=0;
         var st = "p";
        var type = "Product";
         $.ajax({
            type: "POST",
            data: '{"ListId":"' + Listid + '","qty":"' + qty + '","st":"' + st + '","type":"'+ type +'","KeepExisting":"'+ KeepExisting+'"}',
            url: "index.aspx/AddToCartList",
            async:false,
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);

                if (obj.error == 1) {

                    alert("An Error occured during transaction. Please refresh the page and try again Later.");
                    return;
                }
                alert("Your Choosed List Is added to your Cart");
                 BindCart();
                  var dialogDiv = $('#dvMyList');
                dialogDiv.dialog("option", "position", [500, 200]);
                dialogDiv.dialog('close');
               return false;

                 
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });

    });
               $(document).on("click", "div[name='dvViewMyList']", function (event) {

        var data = $(this).attr("id");
        var arrData = data.split('_');
        var Listid = arrData[1];
         $.ajax({
                    type: "POST",
                    data: '{"ListId": "' + Listid + '"}',

                    url: "index.aspx/GetListDetail",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                            $("#dvListProductDetail").dialog({ modal: true, closeOnEscape: false, width: 500 });
                $(".ui-dialog-titlebar").hide();
                $("#tbListProdoctDetail").html(obj.ProductDetail);
                       



                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {


                        $.uiUnlock();
                    }


                });

    });
           $(document).on("click", "div[name='dvAddToCart']", function (event) {

        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = $("#q_" + arrData[1]).val();
        var st = "p";
        var type = "Product";
//        
//      $("#a_"+vid).css("width","55px");
//          $("#a_"+vid).html("<img src='images/loaderadd.gif' style='margin-top:2px'   alt='.....'/>");
         


        ATC(vid, qty, st, type);
           $.notifyBar({ cssClass: "success", html: "Product Added Succcessfully!" });

    });
    $(document).on("click", "button[name='btnAddToCart']", function (event) {

        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = $("#q_" + arrData[1]).val();
        var st = "p";
        var type = "Product";
        
      $("#a_"+vid).css("width","55px");
          $("#a_"+vid).html("<img src='images/loaderadd.gif' style='margin-top:2px'   alt='.....'/>");
         


        ATC(vid, qty, st, type);
           $.notifyBar({ cssClass: "success", html: "Product Added Succcessfully!" });

    });
    $(document).on("click", "div[name='btnView']", function (event) {
        var pid = $(this).attr("id");
        $.ajax({
            type: "POST",
            data: '{"PId":"'+pid +'"}',
            url: "index.aspx/GetComboDetail",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);
               
                
                for (var i = 0; i < obj.Cart.length; i++) {

                    var pname = "";
                    pname = obj.Cart[i]["ProductName"];
                    var PhotoUrl = "";
                    PhotoUrl = obj.Cart[i]["PhotoUrl"];
                    var Qty = "";
                    PhotoUrl = obj.Cart[i]["Qty"];
                    var Price = "";
                    PhotoUrl = obj.Cart[i]["Price"];
                    var Desc = "";
                    PhotoUrl = obj.Cart[i]["Desc"];
                    var Unit = "";
                    PhotoUrl = obj.Cart[i]["Unit"];

               

                    var tr = "<tr><td><img style='height:30px;width:30px' src='../ProductImages/" + PhotoUrl + "' /></td><td>" + pname + "</td><td>" + Desc + "</td><td>" + Unit + "</td><td>" + Qty + "/></td><td>" + Price + "</td></tr>";

                    $("#tbComboDetail").append(tr);
                }
                alert("ddd");




                


                $('#dvPopupCombo').dialog(
              {
                  autoOpen: false,

                  width: 300,
                  height: 170,
                  resizable: false,
                  modal: false,

              });
                alert("ddd1");
                linkObj = $(this);
                var dialogDiv = $('#dvPopupCombo');
                dialogDiv.dialog("option", "position", [500, 200]);
                dialogDiv.dialog('open');
            
                

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });


    });
    $(document).on("click", "button[name='btnAddToCombo']", function (event) {

        var pid = $(this).attr("id");
        var arrPid = pid.split('_');
        var vid = $("#d_" + arrPid[1]).val();
        var qty = $("#cq_" + arrPid[1]).val();
        var st = "p";
        var type = "Combo";
        ATC(pid, vid, qty, st, type);
    });

    $(document).on("click", "div[name='cartminus']", function (event) {

       var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = 1; 
        var st = "m";
        var type = "Product";
           $("#cq_"+vid).css("height","20px");
        
           $("#cq_"+vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");
         
          ATC(vid, qty, st, type);

    });

    $(document).on("click", "div[name='cartadd']", function (event) {

 

    var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = 1; 
        var st = "p";
        var type = "Product";
           $("#cq_"+vid).css("height","20px");
        
           $("#cq_"+vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");
         
          ATC(vid, qty, st, type);


    });
    $(document).on("click", "div[name='cartcombominus']", function (event) {

        var pid = $(this).attr("id");
        var arrPid = pid.split('_');
        var vid = $(this).attr("v");
        var qty = 1;
        var st = "m";
        var type = "Combo";
        ATC(pid, vid, qty, st,type);

    });

    $(document).on("click", "div[name='cartcomboadd']", function (event) {

        var pid = $(this).attr("id");
        var arrPid = pid.split('_');
        var vid = $(this).attr("v");
        var qty = 1;
        var st = "p";
        var type = "Combo";
        ATC(pid, vid, qty, st,type);




    });
    function BindCart() {
    
        $.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/GetCartHTML",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);

                $("#cartcontainer").html(obj.html);
                 
                $("#sp_msg").html("");
               
                $("#hdFreedeliveryAmt").val(obj.FDA);

               
                $("#hdMinimumCheckOutAmt").val(obj.MCOA);
                $("#subtotal").html(obj.ST);
                $("#hdTotalAmt").val(obj.ST);
                $("#delivery").html(obj.DC);
                $("#hdDeliveryCharge").val(obj.DC);
                $("#netamount").html(obj.NA);
                $("#sp_TotalItems").html(obj.TotalItems);
                  $("#sp_TotalItems1").html("MY CART(" + obj.TotalItems +")");
               
               
               
                
                if (obj.ST == 0) {

                    $("#msg").html("Cart is Empty");
                    $("#subtotal").html("");
                    $("#delivery").html("");
                    $("#netamount").html("");
                    $("#sp1").html("");
                    $("#sp2").html("");
                    $("#sp3").html("");
                    $("#btnCheckOut").hide();
                     $("#btnSaveList").hide();
                }
                else {
                    $("#msg").html("");
                    $("#sp1").html("Sub Total: Rs.");
                    $("#sp2").html("Delivery Charges: Rs.");
                    $("#sp3").html("Total: Rs.");
                        $("#btnCheckOut").show();
                       $("#btnSaveList").show();
                        if (Number($("#hdTotalAmt").val()) >= Number($("#hdFreedeliveryAmt").val())) {
                  
                    $("#sp2").html("");
                    $("#delivery").html("");
                    $("#netamount").html($("#hdTotalAmt").val());
                }
                }
                

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {
               
            }

        });
    }



    function BindCustomGroups()
    {
       $.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/BindGroups",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
              
              $("#myGroups").html(msg.d);

               


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
    
    
    }
 
    function BindCities() {
        $.ajax({
            type: "POST",
            data: '',
            url: "index.aspx/BindCities",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#<%=ddlCity.ClientID%>").html(obj.CityList);


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {
              

                $.uiUnlock();
              
            }

        });
    }
    function BindPincodes(CityId) {

    $("#<%=ddlPincode.ClientID%>").html("<option>Please wait while loading..</option>");
        $.ajax({
            type: "POST",
            data: '{"CityId":' + CityId + '}',
            url: "index.aspx/BindPincodes",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#<%=ddlPincode.ClientID%>").html(obj.PinCodeList);


             }, error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function (msg) {
         
                 $.uiUnlock();
                  
             }

         });
     }
     function CheckAddress()
     {$.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/CheckAddress",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
              

              if(msg.d=="0")
              {
                 $("#dvPopupCheck").dialog({ modal: true, closeOnEscape: false, width: 400 });
                $(".ui-dialog-titlebar").hide();
              }

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
     }

    


 
</script>



<!--Row End-->


<!--Row Start-->
 
<div class="row">




 <div class="col-md-12" style="padding-right:0px" >
<div class="slider">

<div id="slider2_container" style="display: none; position: relative; margin: 0 auto; width: 980px;
        height: 350px; overflow: hidden;">

       
            <div    u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 980px; height: 350px;
            overflow: hidden;">
                  <asp:Literal ID="ltSlider" runat="server"></asp:Literal>


            </div>
            <!-- Bullet Navigator Skin Begin -->
            <style>
                /* jssor slider bullet navigator skin 05 css */
                /*
                .jssorb05 div           (normal)
                .jssorb05 div:hover     (normal mouseover)
                .jssorb05 .av           (active)
                .jssorb05 .av:hover     (active mouseover)
                .jssorb05 .dn           (mousedown)
                */
                .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
                    background: url(img/b05.png) no-repeat;
                    overflow: hidden;
                    cursor: pointer;
                }

                .jssorb05 div {
                    background-position: -7px -7px;
                }

                    .jssorb05 div:hover, .jssorb05 .av:hover {
                        background-position: -37px -7px;
                    }

                .jssorb05 .av {
                    background-position: -67px -7px;
                }

                .jssorb05 .dn, .jssorb05 .dn:hover {
                    background-position: -97px -7px;
                }
            </style>
            <!-- bullet navigator container -->
            <div u="navigator" class="jssorb05" style="position: absolute; bottom: 16px; right: 6px;">
                <!-- bullet navigator item prototype -->
                <div u="prototype" style="POSITION: absolute; WIDTH: 16px; HEIGHT: 16px;"></div>
            </div>
            <!-- Bullet Navigator Skin End -->
            <!-- Arrow Navigator Skin Begin -->
            <style>
                /* jssor slider arrow navigator skin 11 css */
                /*
                .jssora11l              (normal)
                .jssora11r              (normal)
                .jssora11l:hover        (normal mouseover)
                .jssora11r:hover        (normal mouseover)
                .jssora11ldn            (mousedown)
                .jssora11rdn            (mousedown)
                */
                .jssora11l, .jssora11r, .jssora11ldn, .jssora11rdn {
                    position: absolute;
                    cursor: pointer;
                    display: block;
                    background: url(img/a11.png) no-repeat;
                    overflow: hidden;
                }

                .jssora11l {
                    background-position: -11px -41px;
                }

                .jssora11r {
                    background-position: -71px -41px;
                }

                .jssora11l:hover {
                    background-position: -131px -41px;
                }

                .jssora11r:hover {
                    background-position: -191px -41px;
                }

                .jssora11ldn {
                    background-position: -251px -41px;
                }

                .jssora11rdn {
                    background-position: -311px -41px;
                }
            </style>
            <!-- Arrow Left -->
            <span u="arrowleft" class="jssora11l" style="width: 37px; height: 37px; top: 123px; left: 8px;">
            </span>
            <!-- Arrow Right -->
            <span u="arrowright" class="jssora11r" style="width: 37px; height: 37px; top: 123px; right: 8px">
            </span>
            <!-- Arrow Navigator Skin End -->
            <a style="display: none" href="http://www.jssor.com">jQuery Slider</a>
        </div>


 </div>

</div>

<%--<div class="col-sm-3"   style="display:block">
<div   id="dvMenu" style="display:block">
 
 
<table width="100%">
  <tbody>

   <asp:Literal ID="ltSchemeData" runat="server" Visible="false"></asp:Literal>
      <img src="images/new.png" style="width:100%" />
 </tbody></table>

</div>
     
</div>--%>
 
</div>
<!--Row End-->


<!--GROUP CATEGORIES  -->
 <div class="row">
            
            
            <div class="col-md-12">
  				<div id="comboheader" style="display:none">	
              <h2 style="border-bottom: 1px solid silver; border-top: 1px solid silver; color: gray; padding: 7px; font-size: 20px; margin-top: 10px; background: none repeat scroll 0% 0% rgb(238, 238, 238); font-family: serif;">PESHAWARI COMBO OFFERS</h2>
  					</div>
            </div>
        </div>
 <div class="row">
            
            
            <div class="col-md-12">
<div  id="ComboProducts">


</div>
</div>
</div>
 <div class="row">
            
            
            <div class="col-md-12">
  					
              <h2 style="border-bottom: 1px dashed silver; border-top: 1px dashed silver; color: gray; padding: 7px; font-size: 20px; margin-top: 10px; background: none repeat scroll 0% 0% rgb(238, 238, 238); font-family: serif;">PESHAWARI QUICK SHOPPING GUIDE</h2>
  					</div>
            
        </div>
        <div class="row">


    <div class="col-md-12">

                  <div id ="dvListProductDetail"  style="z-index: 1;display:none;font-family:Cambria;width:800px">
            
               
                <div id="tbListProdoctDetail">
               
                </div>
                       <div style="float:right;color:Orange;font-size:20px;cursor:pointer" id="dvCloseMe"><b>Close</b></div>       
                                         </div>
                                        </div>
                                        </div> 

         <div class="row">


    <div class="col-md-12">
                  <div id ="dvMyList"  style="z-index: 1;display:none;border:solid 1px silver;font-family:Cambria;width:800px">
                 <table><tr>

                <td colspan='100%'style='font-size:25px;font-family:Cambria;background-color:Black;color:white;width: 500px;text-align:center'>
                <b>My Lists</b></td></tr>
                 <tr><td colspan='100%' style='padding-top:20px'><table><tr><td style='padding-left:0px;font-size:15px;font-family:Cambria'><b>
                Keep Existing</b></td><td style='padding-left:5px'><input type='checkbox' id='chkKeepExisting'/></td></tr> </table> </td></tr>
                <tr><td colspan ="100%" style='padding-top:0px'> 
                 <div id="tbMyList">
               
                </div></td></tr>
               
                
                <tr>
                <td style='padding-top:20px' colspan="100%"><div id='btnCancelMyList'   class='btn btn-primary btn-small' style='background:#cc5547;color:white;float:right' ><b>Cancel</b></div></td>
                </tr></table>
               

                                   
                                         </div>
                                        </div>
                                        </div> 
 <div class="row">


    <div class="col-md-12">
                  <div id ="dvListName" class="formy well" style="z-index: 1;display:none;border:solid 1px silver;background-color:#f5f5f5">
                


                                         <table><tr><td style="font-weight:bold;padding-left:10px;color:Black" class="title">
                                         Enter Your List Name</td>
                                         <td style="padding-left:10px" >
                                                <input id="txtListName" type ="text" style="margin-bottom:10px" class = "form-control"  />
                                         </td></tr>
                                          <tr><td></td><td colspan="100%"><table><tr>
                                         <td style="padding-right:15px">
                                           <button id="btnAddList" class="btn btn-success" style ="background-color:#5cb85c;border-color: #398439;"  title="Check"   name="Login" type="button"><span>Add</span></button>
</td><td >
	<button id = "btnCloseListPopUp" class="btn btn-danger"  type="reset">Close</button>
</td></tr></table></td></tr>
                                         </table>
                                         </div>
                                        </div>
                                        </div> 
 <div class="row">


    <div class="col-md-12">
                  <div id ="inner" class="formy well" style="z-index: 1;display:none;border:solid 2px silver;background-color:#f5f5f5">
                     <h3  style="font-weight:bold;padding-bottom:20px;padding-left:20px" class="title">Login to Your Account</h3>
                                  <div class="form">
                                

                                      <!-- Login  form (not working)-->
                                                                       
                                          <!-- Username -->
                                         <table><tr><td>
                                           <label for="txtName" class="control-label col-md-3">MobileNo</label>
                                         </td>
                                         <td>
                                                <input id="txtName" type ="text" style="margin-bottom:10px" class = "form-control" onkeypress="return isNumberKey(event)" />
                                         </td></tr><tr>
                                         <td>
                                            <label for="txtPassword" class="control-label col-md-3">Password</label>
                                         </td>
                                         <td>   <input type="password" id = "txtPassword" style="margin-bottom:10px" class="form-control" />
                                         </td></tr>
                                         <tr><td></td><td colspan="100%"><table><tr>
                                         <td style="padding-right:15px">
                                           <button id="btnlogin" class="btn btn-success" style ="background-color:#5cb85c;border-color: #398439;"  title="Check"   name="Login" type="button"><span>Login</span></button>
</td><td >
	<button id = "btnclose" class="btn btn-danger"  type="reset">Close</button>
</td></tr></table></td></tr>
<tr><td>    <a href="user/account.aspx" style="color:Blue;">New User Register Here</a>
</td><td></td></tr></table>

                                       
                                      

  
                                    </div> 
                                  </div>

                </div>

                

                </div>
<div class="row">
 
 

 
<div id="myGroups">

</div>

 
</div>

<!--GROUP END  -->

    <!--Row Start-->
<div class="row" style="margin-top:15px;">
     
            <div class="col-md-4">
            </div>
                 
            <div class="col-md-4">
            <div id="dvPopupCheck" style="display:none;border:solid 2px silver" >
  <table width="100%" cellpadding="3" >
                    

                     <tr>
                     <td valign="top" style="background-color:green ;text-align:center;color:white;text-transform:uppercase;padding:3px;font-family:sans-serif" colspan="100%" >
                    <b> Check Delivery Availability</b>
                    </td>
                    </tr>
                    <tr>
                    <td style="padding-bottom:10px;padding-top:20px">Choose City</td>
                    <td style="padding-bottom:10px;padding-top:20px"">
                    <select  id="ddlCity" runat="server" class="form-control" style="width:200px">
              
              
            </select>
                    </td></tr>
                    
                    <tr>
                    <td style="padding-bottom:10px">
                    Choose Sector</td>
                    <td style="padding-bottom:10px">
                    <select id="ddlPincode" runat="server" class="form-control"  style="width:200px">
                    </select>
                    </td></tr>
                    <tr>
                    <td></td>
                    <td>
                    <button id="zipcheck" class="btn" title="Check" style="color:white;background:green;  " name="zip-check" type="button"><span>Proceed</span></button>
                    </td>
                    </tr>
                    </table>
                    </div>
            </div>
            <div class="col-md-4">
            </div>


</div>
<!--Row End-->

<!--Row Start-->
<div class="row" style="margin-top:15px;">





</div>
<!--Row End-->


<!--Row Start-->
<div class="row">

<div class="col-sm-9">
 
    <div    id="featuredCategories">


    </div>


    <div  id="featuredComboTypes">


</div>

</div>



<div class="col-md-3"   >
<div class="yourbasket">




<div id="dvTopBasket" style="border-bottom:solid 2px #d4d4d4;;padding:6px">

 
<img src="images/basket.png" alt="" > <h6 style="float:right;margin-right:0px;font-size:16px">Your Basket (<span id="sp_TotalItems"></span> items)</h6>
 </div>
 
<div class="basketitem" >

 <div id="cartcontainer" style="max-height:200px;overflow-y:scroll">
 </div>
   
  
 
 <table>
 <tr>
 <td style="padding:10px 0px 0px 40px;">
 <table>
    <tr ><td><h3 style="color:red"><b><span id="msg"></span> </b></h3> </td></tr>
 <tr><td><h4><b><span id="sp1">Sub Total: Rs.</span> <span id="subtotal"></span></b></h4></td></tr>
 <tr>
 <td><h5><span id="sp2">Delivery Charges: Rs.</span>  <span id="delivery"></span></h5></td>
 </tr>
 <tr>
 <td><h5 style="color:#f40f00;margin-left:10px"><span id="sp3">Total: Rs.</span><span id="netamount"></span></h5></td>
 </tr>
 <tr>
 <td style="padding:10px 0 0 50px;">
<%-- <a href="basket.aspx" id="ancAdd">--%>
<button id="btnCheckOut" type="button" class="btn btn-success">Checkout</button>
     <%-- </a>--%>
 
 </td>
 </tr>
  <tr>
 <td style="padding:10px 0 0 5px;">
  <table><tr>
 <td><button class="btn btn" type="button" id="btnMyList" style="width:100%;background:#ed8f1f;color:White;display:block">My List</button></td>
 <td style="padding-left:5px">
 
 <button class="btn btn" type="button" id="btnSaveList" style="width:100%;background:#00915C;color:White">Save List</button>
 </td>
 </tr></table>
 </td>
 </tr>
 </table>
 </td>
 </tr>
 </table>
 
 
 
 
 
<br />

 
 
 
 </div>
 <span id="sp_msg" style="color:red;font-size:10px;padding-left:5px"></span>
</div>

 <input type="hidden" id="hdDeliveryCharge" />
      <input type="hidden" id="hdMinimumCheckOutAmt" />
      <input type="hidden" id="hdFreedeliveryAmt" />
      <input type="hidden" id="hdTotalAmt" />
<div class="yourbasket"   >


 <div class="enqury"><h2>HELP US IMPROVE</h2></div>
<uc1:ucEnquiryForm ID="ucEnquiryForm1" runat="server" />

  
    </div>
  
  <a href="recipies.aspx" style="display:block"><img style="width:100%;margin-top:10px" src="images/recipies.jpg"></a>
 
<div class="yourbasket" style="border:0px" id="dvRightAdd">


<%--<div class="enqury"><h2>ENQUIRY FORM</h2></div>
<uc1:ucEnquiryForm ID="ucEnquiryForm1" runat="server" />

 --%>
    </div>
   	





    <br />
 <br />
    <br />
 
 
</div>


</div>





<div class="row">
<div class="col-md-12" id="dvBottomAdd">




</div>
 
</div>


    <link rel="stylesheet" href="css/jquery.notifyBar.css">
    
    <link href="jquery-ui.min.css" rel="stylesheet" />
    <script src="jquery.notifyBar.js"></script>
    <script src="jquery-ui.min.js"></script>


 <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
<script>





        jQuery(document).ready(function ($) {
         





               
            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 6000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseOutQuint,          //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration: 800,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Scale: false,                                  //Scales bullets navigator or not while slider scale
                },

                $BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 12,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 4,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1,                                //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                    $Scale: false,                                  //Scales bullets navigator or not while slider scale
                }
            };

            //Make the element 'slider1_container' visible before initialize jssor slider.
            $("#slider2_container").css("display", "block");
            var jssor_slider1 = new $JssorSlider$("slider2_container", options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth) {
                    jssor_slider1.$ScaleWidth(parentWidth - 30);
                }
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });

</script>
</asp:Content>

