﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class OrderSuccess : System.Web.UI.Page
{
    protected string ShippingAddress = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           // ltOrderNumber.Text  = Request.QueryString["o"] != null ? Request.QueryString["o"] : "0";
            GetAddress();
        }

    }
    public void GetAddress()
    {
        Int64 OrderId = Convert.ToInt64(Session[Constants.OrderId]);
        ltOrderNumber.Text = OrderId.ToString();
       //new OrderBLL().GetShippingAddress(Convert.ToInt64(ltOrderNumber.Text.Trim()),out ShippingAddress);
        new OrderBLL().GetShippingAddress(OrderId , out ShippingAddress);
    }



    [WebMethod]
    public static string Insert(string Title, string Description, string Rating)
    {
        Int32 uId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        Reviews objReviews = new Reviews()
        {

            UserId = uId,
            Title = Title,
            Description = Description,
            Rating = Rating,
            IsApproved = false,


        };
        int status = new ReviewsBLL().InsertUpdate(objReviews);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            DelAdd = objReviews,
            Status = status
        };
        return ser.Serialize(JsonData);
    }
    
}