﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="CustomerInfo.aspx.cs" Inherits="backoffice_CustomerInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">


 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>


    <script type="text/javascript">
        $(document).ready(
        function () {
            GetCustomerDetail();

            function GetCustomerDetail() {

                var UserId = $("#<%=hdnUser.ClientID %>").val();
                

                $.ajax({
                    type: "POST",
                    data: '{ "UserId": "' + UserId + '"}',
                    url: "CustomerInfo.aspx/GetDetail",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                        $("#txtFirstname").val(obj.Users.FirstName);
                        $("#txtlastname").val(obj.Users.LastName);
                        $("#txtmobile").val(obj.Delivery.MobileNo);
                        $("#txtphone").val(obj.Delivery.Telephone);
                        $("#txtemail").val(obj.Users.EmailId);
                        $("#txtAddress").val(obj.Delivery.Address);
                        $("#txtStreet").val(obj.Delivery.CityName);
                        $("#txtArea").val(obj.Delivery.Area);
                        $("#txtCity").val(obj.Delivery.Street);
                        $("#txtpincode").val(obj.Delivery.PinCode);
                        

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                        

                    }

                });




            }

        }
        );
    </script>

    <style type="text/css">
        .headings
        {
           
            font-weight:200px;
            }
    </style>
<form id= "frm" runat="server">
<asp:HiddenField ID="hdnUser" runat="server" Value= "0" />
<div id="content" style="padding:6px">

             <div id="rightnow" style="margin-top: 10px">
            <h3 class="reallynow">
                <span>Users List</span>
                <br />
            </h3>
            <div class="youhave" style="padding-left: 30px">
          
          <table><tr class="headings"><td>FirstName:</td><td><input type="text" id="txtFirstname" style ="width:200px" /></td><td>Address:</td><td><input type="text" id="txtAddress" style="width:200px" /></td></tr>
          <tr class="headings"><td>LastName:</td><td><input type="text"  id="txtlastname" style="width:200px" /></td><td>Street:</td><td><input type="text" id="txtStreet" style ="width:200px" /></td></tr>
          <tr class="headings"><td>MobileNo:</td><td><input type="text" id="txtmobile" style="width:200px" /></td><td>Area:</td><td><input type="text" id="txtArea" style="width:200px" /></td></tr>
          <tr class="headings"><td>Phone No:</td><td><input type="text" id="txtphone" style="width:200px" /></td><td>City:</td><td><input type="text" id="txtCity" style="width:200px" /></td></tr>

          <tr class="headings"><td>Email:</td><td><input type="text" id="txtemail" style="width:200px" /></td><td>PinCode:</td><td><input type="text" id="txtpincode" /></td></tr>



          </table>




                <table id="jQGridDemo">
                </table>
                <div id="jQGridDemoPager">
                </div>



               
            </div>
        </div>

            </div>
            </form>


               <script type="text/javascript">
    
      var customerid = $("#<%=hdnUser.ClientID %>").val();

        jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/managecustomerorder.ashx?Id='+customerid,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['Order No', 'Order Date', 'NetAmount','Status','DeliveryDate','PaymentMode'],
            colModel: [
                        { name: 'OrderId',key:true, index: 'OrderId', stype: 'text',sorttype:'int',hidden:false },
   		                { name: 'strOD', index: 'strOD', width: 100, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
                         { name: 'NetAmount', index: 'NetAmount', width: 100, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
                          { name: 'Status', index: 'Status', width: 100, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
                           { name: 'strDD', index: 'strDD', width: 100, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
                            { name: 'PaymentMode', index: 'PaymentMode', width: 100, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                
   		                
                       ],
            rowNum: 10,
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'StateId',
            viewrecords: true,
            height: "100%",
            width:"400px",
             toolbar: [true, "top"],
            ignoreCase: true,
            sortorder: 'asc',
            caption: "States List",
            editurl: 'handlers/ManageStates.ashx',
             
        });

         var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });
 
        $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }



                     );
             
    </script>  



</asp:Content>

