﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_managedeliveredorders : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {


            if (!User.IsInRole("DeliveredOrders"))
            {
                Response.Redirect("default.aspx");

            }
            hdnDate.Value = DateTime.Now.ToShortDateString();
           
        }

 

    }


    [WebMethod]

    public static string InsertTemp(Int32 BI)
    {


        Bills objBill = new Bills()
        {
            BillId = Convert.ToInt32(BI),


        };


        JavaScriptSerializer ser = new JavaScriptSerializer();

        new BillBLL().InsertTemp(objBill);
        var JsonData = new
        {
            Bill = objBill

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetTemparoryBills(int Bid)
    {
        BillDetail objBill = new BillDetail() { BillId = Bid };


        var BillDetail = new BillBLL().GetBillsTemp(objBill);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            TempBill = BillDetail
        };
        return ser.Serialize(JsonData);
    }

}