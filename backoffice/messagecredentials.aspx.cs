﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_messagecredentials : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (!User.IsInRole("MessageCredentials"))
            {
                Response.Redirect("default.aspx");

            }
            BindData();
           
        }
    }
    void BindData()
    {
        MessageCredentials objMsg = new MessageCredentials();
        objMsg = new MessageCredentialsDAL().GetAll();
        txtAdminMsg.Text = objMsg.AdminMessageText;
        txtCustomerMsg.Text = objMsg.CustomerMessageText;
        txtAdminMobileNo.Text = objMsg.AdminMobileNo;
        txtOrderOnCall.Text = objMsg.OrderOnCall;
        txtCallUs.Text = objMsg.CallUs;
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        MessageCredentials objmsg = new MessageCredentials();
        objmsg.AdminMobileNo = txtAdminMobileNo.Text;
        objmsg.AdminMessageText = txtAdminMsg.Text;
        objmsg.CustomerMessageText = txtCustomerMsg.Text;
        objmsg.OrderOnCall = txtOrderOnCall.Text;
        objmsg.CallUs = txtCallUs.Text;
        Int16 retval = new MessageCredentialsDAL().Insert(objmsg );
        if (retval != 0)
        {
            Response.Write("<script>alert('Information is saved');</script>");
            BindData();
            return;
        }
        else
        {
            Response.Write("<script>alert('Process is not completed,Plz try again');</script>");
            return;
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtAdminMobileNo.Text = "";
        txtAdminMsg.Text = "";
        txtCustomerMsg.Text = "";
        txtCallUs.Text = "";
        txtOrderOnCall.Text = "";
    }
}