﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_managetimeslots : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (!User.IsInRole("ManageDeliverySlots"))
            {
                Response.Redirect("default.aspx");

            }
        }
    }


    [WebMethod]
    public static string Insert(int Id, string StartTime, string EndTime,Boolean IsActive)
    {
        TimeSlots objTimeSlots = new TimeSlots()
        {
            Id = Id,
            StartTime = StartTime,
            EndTime = EndTime,
            AdminId = 0,
            IsActive = IsActive,

        };
        int status = new TimeSlotsBLL().InsertUpdate(objTimeSlots);
        var JsonData = new
        {
            TimeSlot = objTimeSlots,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
}