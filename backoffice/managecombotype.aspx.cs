﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_managecombotype : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!User.IsInRole("ComboType"))
            {
                Response.Redirect("default.aspx");

            }
        }
    }
    [WebMethod]
    public static string Insert(int id, string title, string Description, bool isActive)
    {

        ComboType objBookCategory = new ComboType()
        {
            ComboTypeId = id,
            Title = title.Trim(),
            Description = Description.Trim(),
            IsActive = isActive

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new ComboTypeBLL ().InsertUpdate(objBookCategory);
        var JsonData = new
        {
            Category = objBookCategory,
            Status = status
        };
        return ser.Serialize(JsonData);



    }
}