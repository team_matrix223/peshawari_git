﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="managecombo.aspx.cs" Inherits="backoffice_managecombo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/master.css" rel="stylesheet" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">
    var m_ComboId = 0;

    var Product_Id = "";
    var Product_Name = "";
    var Price = 0;
    var Qty = 0
    var Units = "";

    var LSno = 0;
    var LProduct_Id =0;
    var LProduct_Name = "";
    var LQty = 0;
    var LPrice =0;
    var LUnits = "";

    function BindSubSubCategories(sid) {
        $("#ddlSubSubCategory").html('');
        $.uiLock('');
        var SubCatId = sid;
        $.ajax({
            type: "POST",
            data: '{ "SubCatId": "' + SubCatId + '"}',
            url: "managecombo.aspx/BindSubSubCategories",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
               
                var obj = jQuery.parseJSON(msg.d);
                if (obj.result == 1) {
                    $("#myrow").css({ "display": "" });
                   
                }
                else {
                    $("#myrow").css({ "display": "none" });
                   
                    var CatId = $("#<%=ddlCategory.ClientID%>").val();             
                    var SubCatId = $("#ddlSubCategory").val();
                    var SubSubCatId = 0;
                  
                    BindProductList(CatId, SubCatId, SubSubCatId);
                }
                $("#ddlSubSubCategory").append(obj.SubSubCatOptions);
              
            },
            complete: function (msg) {
                $.uiUnlock();

            }

        });


    }
    function BindSubCategories(sid) {
        $("#ddlSubCategory").html('');
        $("#ddlSubSubCategory").html('');

        $.uiLock('');
        var CatId = sid;
        $.ajax({
            type: "POST",
            data: '{ "CatId": "' + CatId + '"}',
            url: "managecombo.aspx/BindSubCategories",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#ddlSubCategory").append(obj.SubCatOptions);

            },
            complete: function (msg) {
                $.uiUnlock();

            }

        });


    }

    var ProductCollection = [];
    var MainProductCollection = [];
    function clsMainProduct() {
       this.LProduct_Id = 0;
       this.LProduct_Name = "";
       this.LSno = 0;
       this.LQty = 0;
       this.LPrice = 0;
       this.LUnits = "";
    }
    function clsProduct() {
        this.Product_Id =0;
        this.Product_Name = "";
        this.Price = 0;
        this.Qty = 0;
        this.Units = "";
    }
    function ResetControls() {
        ProductCollection = [];
        MainProductCollection = [];
       // $("#tbMainProductList").css({ "display": "none" });
      //  $("#tbProductList").css({ "display": "none" });
     //   $("#btnAddProduct").css({ "display": "none" });
        Product_Id = 0;
        Product_Name = "";
        Price = 0;
        Qty = 0;
        Units = "";
        LProduct_Id = 0;
        LProduct_Name = "";
        LSno = 0;
        LQty = 0;
        LPrice = 0;
        LUnits = "";
        m_ComboId = 0;
        $("#txtPrice").val("");
        var txtTitle = $("#txtTitle");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
      
        $('#tbProductList tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
        $('#tbMainProductList tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
        $("#<%=ddlCategory.ClientID%> option[value= 0 ]").prop("selected", true);
        $("#ddlSubCategory").val("");
        $("#ddlSubSubCategory").val("");
        $("#<%=ddlComboType.ClientID%> option[value= 0 ]").prop("selected", true);
        txtTitle.focus();
        txtTitle.val("");
        txtTitle.focus();
        btnAdd.css({ "display": "block" });
        btnAdd.html("Add Combo");
        $("#txtDescription").val("");

        btnUpdate.css({ "display": "none" });
        btnUpdate.html("Update Combo");
        $("#chkIsActive").prop("checked", "checked");
        $("#btnReset").css({ "display": "none" });
        $("#hdnId").val("0");
        validateForm("detach");
    }



    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {
        if (!validateForm("frmCombo")) {
            return;
        }
        var Id = m_ComboId;
        var Title = $("#txtTitle").val();
        if ($.trim(Title) == "") {
            $("#txtTitle").focus();

            return;
        }
        var Description = $("#txtDescription").val();
        var Price = $("#txtPrice").val();
        var ComboTypeId = $("#<%=ddlComboType.ClientID%>").val();
        var IsActive = false;

        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
        }
        var ProductIdArr = [];
        var QtyArr = [];
        var PriceArr = [];
        if (MainProductCollection.length == 0) {
            alert("Please first add a Product");
            return;
        }
        for (var i = 0; i < MainProductCollection.length; i++) {

            ProductIdArr[i] = MainProductCollection[i]["LProduct_Id"];
            QtyArr[i] = MainProductCollection[i]["LQty"];
            PriceArr[i] = MainProductCollection[i]["LPrice"];

        }
        if (ProductIdArr.length == 0) {

            alert("Please select atleast one Product");
            
            return;
        }
        $.uiLock('');

        $.ajax({
            type: "POST",
            data: '{"ComboId":"' + Id + '", "ComboTypeId": "' + ComboTypeId + '", "Title": "' + Title + '", "Description": "' + Description + '","Price": "' + Price + '","IsActive": "' + IsActive + '","ProductIdArr": "' + ProductIdArr  + '","QtyArr": "' + QtyArr + '","PriceArr": "' + PriceArr  + '"}',
            url: "managecombo.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == 0) {

                    alert("Insertion Failed.Combo with duplicate name already exists.");
                    return;
                }

                if (Id == "0") {
                    ResetControls();
                    BindGrid();
                    alert("Combo is added successfully.");            
                }
                else {
                    ResetControls();
                    BindGrid();
                    alert("Combo is Updated successfully.");
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }
    function BindProductList(CatId, SubCatId, SubSubCatId) {
        var SubCat = $("#ddlSubCategory").val();
        if (SubCat == "") {
            alert("First Choose SubCategory");
            $("#ddlSubCategory").focus();
            return;
        }
        $("#tbProductList").css({ "display": "block" });
        $("#btnAddProduct").css({ "display": "block" });
        $('#tbProductList tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
        ProductCollection = [];
        $.ajax({
            type: "POST",
            data: '{"CatId":"' + CatId + '", "SubCatId": "' + SubCatId + '","SubSubCatId": "' + SubSubCatId + '"}',
            url: "managecombo.aspx/GetProducts",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                ProductCollection = [];

               
                var obj = jQuery.parseJSON(msg.d);

                var tr = "";
                for (var i = 0; i < obj.DetailData.length; i++) {
                    tr = tr + "<tr><td>" + obj.DetailData[i]["ProductId"] + "</td><td>" + obj.DetailData[i]["Name"] + "</td><td>" + obj.DetailData[i]["Units"] + "</td><td>" + obj.DetailData[i]["Qty"] + "</td><td>" + obj.DetailData[i]["Price"] + "</td><td><input type='checkbox' name='chkProduct'/> </td></tr>";

                    var CO = new clsProduct();
                    CO.Product_Id = obj.DetailData[i]["ProductId"];
                    CO.Product_Name = obj.DetailData[i]["Name"];
                    CO.Price = obj.DetailData[i]["Price"];
                    CO.Qty = obj.DetailData[i]["Qty"];
                    CO.Units = obj.DetailData[i]["Units"];
                    ProductCollection.push(CO);
                  
                }

                $("#tbProductList").append(tr);


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {


                $.uiUnlock();
            }



        });
    }
    $(document).ready(
    function () {
        BindGrid();
        // $("#tbMainProductList").css({ "display": "none" });
        //  $("#tbProductList").css({ "display": "none" });
        //   $("#btnAddProduct").css({ "display": "none" });
        $("#<%=ddlCategory.ClientID%>").change(
           function () {
               BindSubCategories($("#<%=ddlCategory.ClientID%>").val());

           }

       );
        $(document).on("click", "#btnDelete", function (event) {
            var RowIndex = Number($(this).closest('tr').index());
            var tr = $(this).closest("tr");
            tr.remove();

            MainProductCollection.splice(RowIndex, 1);

        });
        $(document).on("change", "input[name='txtQty']", function (event) {


            if ($(this).val().trim() == "0" ) {
                $(this).val("1")
            }

            var RowIndex = Number($(this).closest('tr').index());
            var OriginalQty = MainProductCollection[RowIndex]["LQty"];
            var OriginalPrice = MainProductCollection[RowIndex]["LPrice"];
            var ChangeQty = $(this).val();
            ChangePrice = (OriginalPrice / OriginalQty) * ChangeQty;
            MainProductCollection[RowIndex]["LQty"] = ChangeQty;
            MainProductCollection[RowIndex]["LPrice"] = ChangePrice;

            $('#tbMainProductList tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

            var tr = "";
            for (var i = 0; i < MainProductCollection.length; i++) {
                LSno = i + 1;
                LProduct_Id = MainProductCollection[i]["LProduct_Id"];
                LProduct_Name = MainProductCollection[i]["LProduct_Name"];
                LQty = MainProductCollection[i]["LQty"];
                LPrice = MainProductCollection[i]["LPrice"];
                LUnits = MainProductCollection[i]["LUnits"];


                var tr = "<tr><td>" + LSno + "</td><td>" + LProduct_Id + "</td><td>" + LProduct_Name + "</td><td>" + LUnits + "</td><td><input name='txtQty' id='txtQty"+i+"' style='width:50px' type='text' value='" + LQty + "'/></td><td>" + LPrice + "</td>><td><div id='btnDelete' style='cursor:pointer' ><a >Delete</a></div></td></tr>";
                $("#tbMainProductList").append(tr);

            }
            $("#txtQty"+RowIndex).focus();

        }
            );

        $("#btnAddProduct").click(
            function () {

                $("input[name='chkProduct']").each(

              function (index) {


                  if ($(this).prop("checked")) {



                      var item = $.grep(MainProductCollection, function (item) {
                          return item.LProduct_Id == ProductCollection[index]["Product_Id"];
                      });


                      if (item.length) {

                         // alert("Product Already added in the list");
                          return;

                      } else {

                          $("#tbMainProductList").css({ "display": "block" });
                          LSno = index + 1;

                          LProduct_Id = ProductCollection[index]["Product_Id"];
                          LProduct_Name = ProductCollection[index]["Product_Name"];
                          LQty = ProductCollection[index]["Qty"];
                          LPrice = ProductCollection[index]["Price"];
                          LUnits = ProductCollection[index]["Units"];


                          var tr = "<tr><td>" + LSno + "</td><td>" + LProduct_Id + "</td><td>" + LProduct_Name + "</td><td>" + LUnits + "</td><td><input name='txtQty' id='txtQty"+index+"' style='width:50px' type='text' value='" + LQty + "'/></td><td>" + LPrice + "</td>><td><div id='btnDelete' style='cursor:pointer'><a >Delete</a></div></td></tr>";

                          $("#tbMainProductList").append(tr);



                          var CO = new clsMainProduct();
                          CO.LSno = LSno;
                          CO.LProduct_Id = LProduct_Id;
                          CO.LProduct_Name = LProduct_Name;
                          CO.LQty = LQty;
                          CO.LPrice = LPrice;
                          CO.LUnits = LUnits;
                          MainProductCollection.push(CO);
                      }



                  }
              }
              );


            });
        $("#ddlSubCategory").change(
          function () {

              BindSubSubCategories($("#ddlSubCategory").val());

          }

       );

        $("#ddlSubSubCategory").change(function () {
            var CatId = $("#<%=ddlCategory.ClientID%>").val();
            var SubCatId = $("#ddlSubCategory").val();
            var SubSubCatId = $("#ddlSubSubCategory").val();
            BindProductList(CatId, SubCatId, SubSubCatId);
        });


        $("#btnAdd").click(
        function () {

            m_ComboId = 0;
            InsertUpdate();
        }
        );


        $("#btnUpdate").click(
        function () {
            InsertUpdate();
        }
        );

        $('#txtTitle').focus();
        $('#txtTitle').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );


    }
    );

</script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Combo</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">


                   <table cellpadding="0" cellspacing="0" border="0" id="frmCombo" class="top_table">
                        <tr>
                            <td class="headings">Choose ComboType:</td>
                            <td>

                                <asp:DropDownList ID="ddlComboType" class="validate ddlrequired" runat="server"></asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td class="headings">Title:</td>
                            <td>
                                <input type="text" name="txtTitle" class="inputtxt validate required alphanumeric" data-index="1" id="txtTitle" />
                            </td>
                        </tr>

                        <tr>
                            <td class="headings">Description:</td>
                            <td>

                                <textarea id="txtDescription" rows="3"></textarea>

                            </td>
                        </tr>
                        <tr>
                            <td class="headings">Price:</td>
                            <td>
                                <input type="text" name="txtPrice" class="inputtxt validate required valNumber" data-index="1" id="txtPrice" />
                            </td>
                        </tr>
                        <tr>
                            <td class="headings">Category Level 1:</td>
                            <td>

                                <asp:DropDownList ID="ddlCategory" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="headings">Category Level 2:</td>
                            <td>
                                <select id="ddlSubCategory" data-index="6">
                                </select>
                            </td>
                        </tr>
                        <tr id="myrow" style="display:none">
                            <td class="headings">Category Level 3:</td>
                            <td>
                                <select id="ddlSubSubCategory" data-index="6">
                                </select>

                            </td>
                        </tr>
                        <tr>
                            <td class="headings">IsActive:</td>
                            <td>
                                <input type="checkbox" id="chkIsActive" checked="checked" data-index="2" name="chkIsActive" />
                            </td>
                        </tr>

                    </table>

                                <table id="tbProductList" class="item_table">
                                    <thead>
                                        <tr style="background:#294145;color:white">
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Unit</th>
                                            <th>Qty</th>
                                            <th>Price</th>
                                            <th> </th>

                                        </tr>

                                    </thead>
                                    <tbody class="customTable">

                                    </tbody>

                                </table>

                   <table class="category_table">
                        <tr>
                            <td colspan="100%">
                                <div id="btnAddProduct" class="btn btn-primary btn-small" style="display:block;">Add To List</div>
                            </td>
                        </tr>
                    </table>
                        
                        <table id="tbMainProductList" class="item_table">
                            <thead>
                                <tr style="background:#294145;color:white">
                                    <th>Sno</th>
                                    <th>Product Code</th>
                                    <th>Product Name</th>
                                    <th>Units</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                    <th>Delete</th>

                                </tr>

                            </thead>
                            <tbody class="customTable">

                            </tbody>

                        </table>

                <table cellspacing="0" cellpadding="0" class="category_table">
                    <tr>
                        <td>
                            <div id="btnAdd" class="btn btn-primary btn-small">Save Combo</div>
                        </td>
                        <td>
                            <div id="btnUpdate" class="btn btn-primary btn-small" style="display:none;">Update Combo</div>
                        </td>
                        <td>
                            <div id="btnReset" class="btn btn-primary btn-small" style="display:none;width:60px;">Cancel</div>
                        </td>
                    </tr>
                </table>              
                    			 
       
                    </div>
			  </div>
               


               <div id="rightnow">
                    <h3 class="reallynow">
                                        <span>Manage Combo </span>

                                        <br />
                                    </h3>
                    <div class="youhave">

                        <table id="jQGridDemo">
                        </table>
                        <div id="jQGridDemoPager">
                        </div>

                    </div>
                </div>

            </div>
</form>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/managecombo.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Combo Id', 'ComboTypeId','ComboTypeName', 'Title', 'Description', 'Price', 'IsActive'],
                        colModel: [
                                    { name: 'ComboId', key: true, index: 'ComboId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                    { name: 'ComboTypeId', index: 'ComboTypeId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                     { name: 'ComboTypeName', index: 'ComboTypeName', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Title', index: 'Title', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },

                                    { name: 'Description', index: 'Description', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true }, hidden: true },

                                 { name: 'Price', index: 'Price', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true } },


                                    { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },


                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'ComboId',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Combo List",

                        editurl: 'handlers/managecombo.ashx',



                    });




                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_ComboId = 0;
                    validateForm("detach");
                    var txtTitle = $("#txtTitle");
                    m_ComboId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ComboId');
                       ProductCollection = [];
        MainProductCollection = [];

                    if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                        $('#chkIsActive').prop('checked', true);
                    }
                    else {
                        $('#chkIsActive').prop('checked', false);

                    }
                    $('#tbMainProductList tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                    txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Title'));

                    $("#txtDescription").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Description'));
                    $("#txtPrice").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Price'));

                    var ComboTypeId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ComboTypeId');

                    $("#<%=ddlComboType.ClientID%> option[value='" + ComboTypeId + "']").prop("selected", true);
                    $("#tbMainProductList").css({ "display": "block" });
                    $("#tbProductList").css({ "display": "block" });
                    $("#btnAddProduct").css({ "display": "block" });
                    $.ajax({
                        type: "POST",
                        data: '{ "ComboId": "' + m_ComboId + '"}',
                        url: "managecombo.aspx/GetProductDetail",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {
                          
                            MainProductCollection = [];

               
                            var obj = jQuery.parseJSON(msg.d);

                            var tr = "";
                            for (var i = 0; i < obj.DetailData.length; i++) {
                                LSno = i + 1;
                                LProduct_Id = obj.DetailData[i]["ProductId"];
                                LProduct_Name = obj.DetailData[i]["ProductName"];
                                LQty = obj.DetailData[i]["Qty"];
                                LPrice =obj. DetailData[i]["Price"];
                                LUnits = obj.DetailData[i]["Units"];

                                var tr = "<tr><td>" + LSno + "</td><td>" + LProduct_Id + "</td><td>" + LProduct_Name + "</td><td>" + LUnits + "</td><td><input name='txtQty' style='width:50px' type='text' value='" + LQty + "'/></td><td>" + LPrice + "</td>><td><div id='btnDelete'  style='cursor:pointer'><a> Delete</a></div></td></tr>";

                                $("#tbMainProductList").append(tr);


                                var CO = new clsMainProduct();
                                CO.LSno = LSno;
                                CO.LProduct_Id = LProduct_Id;
                                CO.LProduct_Name = LProduct_Name;
                                CO.LQty = LQty;
                                CO.LPrice = LPrice;
                                CO.LUnits= obj.DetailData[i]["Units"];
                                MainProductCollection.push(CO);
                            }


                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {

                            $.uiUnlock();
                        }



                    });
        txtTitle.focus();
        $("#btnAdd").css({ "display": "none" });
        $("#btnUpdate").css({ "display": "block" });
        $("#btnReset").css({ "display": "block" });
        TakeMeTop();
    }
});

    var DataGrid = jQuery('#jQGridDemo');
    DataGrid.jqGrid('setGridWidth', '500');

    $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                     {
                         refresh: false,
                         edit: false,
                         add: false,
                         del: false,
                         search: false,
                         searchtext: "Search",
                         addtext: "Add",
                     },

                     {//SEARCH
                         closeOnEscape: true

                     }

                       );



}





    </script>

</asp:Content>





