<%@ Page Title="" Language="C#" MasterPageFile="admin.master" AutoEventWireup="true" CodeFile="SubPageCreate.aspx.cs" Inherits="Admin_SubPage" %>
 
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/custom-css/cms.css" rel="stylesheet" />

<form id="frm2" runat="server">

<asp:ScriptManager ID="Scrpt1" runat="server">

</asp:ScriptManager>

<asp:UpdatePanel ID="upd1" runat="server">
    

<ContentTemplate>
 <div id="content" style="padding:6px">
   	<div id="rightnow">
        <h3 class="reallynow">
           <span>Add Sub Pages</span>
        </h3>
	   
           <div class="youhave">

            <table class="top_table" style="width: 100%">
                <tr>
                    <td class="Titles">
                        Parent Page:</td>
                    <td>
                        <asp:DropDownList ID="ddlParentPage" runat="server" AutoPostBack="false">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlParentPage" ErrorMessage="*" ForeColor="Red" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    </td>
                </tr>

                <tr>
                    <td class="Titles">
                        Title:</td>
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTitle" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>

            <table class="contant_editor">
                <tr>
                    <td class="Titles">
                        Content:</td>
                    <td>
                </tr>
                <tr>
                    <td>
                        <FCKeditorV2:FCKeditor ID="EditorDescription" runat="server" BasePath="~/backoffice/fckeditor/" Height="400px">
                        </FCKeditorV2:FCKeditor>
                    </td>
                </tr>
            </table>

            <table class="midle_table">
                <tr>
                    <td colspan="100%">
                        <h3 class="reallynow">
                                                        <span>Meta Tags</span>
                                                    </h3>
                    </td>
                </tr>
                <tr>
                    <td>Keywords:</td>
                    <td>
                        <asp:TextBox ID="txtKeywords" runat="server"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>Page Title:</td>
                    <td>
                        <asp:TextBox ID="txtPageTitle" runat="server"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>Page Description:</td>
                    <td>
                        <asp:TextBox ID="txtPageDescription" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
            </table>

            <table class="top_table">
                <tr>
                    <td class="Titles">IsActive:</td>
                    <td>
                        <asp:CheckBox ID="chkIsActive" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="Titles">
                        Is Full Page:</td>
                    <td>

                        <asp:CheckBox ID="chkFullPage" Checked="true" runat="server" />
                    </td>
                </tr>
            </table>

            <table class="category_table">
                <tr>
                    <td>
                        <asp:Button ID="btnCreatePage" CssClass="CustomButtons" runat="server" Text="Create Page" onclick="btnCreatePage_Click" />
                    </td>
                </tr>
            </table>

        </div>

    </div>
</div>
</ContentTemplate>
</asp:UpdatePanel>

   
   
   
   
    
   
    
    </form>

</asp:Content>

