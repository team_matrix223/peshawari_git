﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="managedeliveredorders.aspx.cs" Inherits="backoffice_managedeliveredorders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
            <link href="css/custom-css/transaction.css" rel="stylesheet" />

    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

    <script type="text/javascript">
        var BillId = 0;
        function Reset() {

            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            $('#tbProductVariation tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            $("#<%=ltBillNo.ClientID%>").val("");
            $("#<%=ltBillDate.ClientID%>").val("");
            $("#<%=ltCustomer.ClientID%>").val("");
            $("#<%=ltAddress.ClientID%>").val("");
            $("#<%=ltBillAmount.ClientID%>").val("");
            $("#<%=ltRecepientname.ClientID%>").val("");
            $("#<%=ltRecepientPhone.ClientID%>").val("");
            $("#<%=ltRecepientMobile.ClientID%>").val("");


            var dialogDiv = $('#dvEdit');
            dialogDiv.dialog("option", "position", [150, 100]);
            dialogDiv.dialog('close');
            BindGrid();


            $("#txtBillNo").val("");
            $("#txtRemarks").val("");
            $("#rdbOrder").prop("checked", false);

        }

        var ProductCollection = [];
        function clsProduct() {
            this.ProductId = 0;
            this.ProductName = 0;
            this.Qty = 0;
            this.Price = 0;
            this.VariationId = 0;
            this.ProductDescription = "";
            this.SubTotal = 0;
        }
    
     function BindTempBills() {


             var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             $("#dvBillno").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillId'));
             $("#dvBillDate").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'strBD'));
             $("#dvCustomer").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'CustomerName'));
             $("#dvBillAmount").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillValue'));
             $("#dvRecepient").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'RecipientFirstName'));
             $("#dvRecepientMobile").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'RecipientMobile'));
             $("#dvRecepientPhone").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'RecipientPhone'));
             $("#dvAddress").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'CompleteAddress'));
             ProductCollection=[];
             $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove(); 
                       
                         $.ajax({
                         type: "POST",
                         data: '{ "Bid": "' +  $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillId') + '"}',
                         url: "managebillcancelation.aspx/GetTemparoryBills",
                         contentType: "application/json",
                         dataType: "json",
                         success: function (msg) {
               
                           ProductCollection = [];

              
                        var obj = jQuery.parseJSON(msg.d);

                        var tr = "";
                        var Total  = 0;
                       
                        for (var i = 0; i < obj.TempBill.length; i++) {
                  
                            var schemeid = obj.TempBill[i]["SchemeId"];
                            if (schemeid == 0) {
                                tr = tr + "<tr><td><img src='../ProductImages/T_" + obj.TempBill[i]["PhotoUrl"] + "' style='width:50px;height:50px'/></td><td>" + obj.TempBill[i]["ProductDescription"] + "</td><td><input type='text' style ='width:35px' id='q_" + obj.TempBill[i]["ProductId"] + "'  value =" + obj.TempBill[i]["Qty"] + " readonly='readonly' /></td> <td>" + obj.TempBill[i]["Price"] + "</td><td>" + obj.TempBill[i]["SubTotal"] + "</td></tr>";
                                Total = Number(Total) + Number(obj.TempBill[i]["SubTotal"]);
                            }
                            else {
                                tr = tr + "<tr><td><img src='../ProductImages/T_" + obj.TempBill[i]["PhotoUrl"] + "' style='width:50px;height:50px'/></td><td>" + obj.TempBill[i]["ProductDescription"] + "</td><td></td><td><input type='text' style ='width:20px' id='q_" + obj.TempBill[i]["ProductId"] + "'  value =" + obj.TempBill[i]["Qty"] + " readonly='readonly' /></td> <td></td><td>" + obj.TempBill[i]["Price"] + "</td><td>" + obj.TempBill[i]["Amount"] + "</td><td><img src='../img/free.gif' /></td></tr>";
                            }
                        // tr = tr + "<tr><td>" + obj.TempBill[i]["ProductDescription"] + "</td><td><div id='btnMinus'  class='btn btn-primary btn-small'>-</div></td>";
                        // tr=tr+"<td><input type='text' style ='width:50px' id='q_"+obj.TempBill[i]["ProductId"]+"'  value =" +obj.TempBill[i]["Qty"] +" /></td>";
                        //tr=tr+"<td><div id='btnPlus'  class='btn btn-primary btn-small'>+</div></td><td>" + obj.TempBill[i]["Price"] + "</td>";
                        //tr=tr+"<td>" + obj.TempBill[i]["SubTotal"] + "</td>";
                        //tr=tr+"<td><div id='"+ obj.TempBill[i]["ProductId"]+"_"+ obj.TempBill[i]["VariationId"] +"'  name='dvClose' ><img src='images/trash.png'/></div> </td>";
                        //tr=tr+"</tr>";
                     
                         var PO = new clsProduct();
                         PO.ProductId = obj.TempBill[i]["ProductId"] ;
                         PO.ProductName = obj.TempBill[i]["ProductName"] ;
                         PO.ProductDescription = obj.TempBill[i]["ProductDescription"] ;
                         PO.Qty = obj.TempBill[i]["Qty"] ; 
                         PO.Price = obj.TempBill[i]["Price"] ; 
                         PO.VariationId = obj.TempBill[i]["VariationId"];

                         ProductCollection.push(PO);
                          
                         //Total = Number(Total) + Number (obj.TempBill[i]["SubTotal"] ); 
                        
                         
                      }
                      
                      
                       $("#tbProductInfo").append(tr);
                       $("div[id='dvsbtotal']").html(" Rs. " + Total);
                      $("div[id='dvdelivery']").html(" Rs. " + 20);
                      $("div[id='dvnetAmount']").html(" Rs. " + Number(Number(Total)+Number(20)));

                 },
                complete: function (msg) {
              
          
            }

        });
      }

     $(document).ready(
        function () {

            $("#txtDateFrom").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });


            $("#txtDateTo").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });

            $("#txtDateTo,#txtDateFrom").val($("#<%=hdnDate.ClientID%>").val());
            BindGrid();




            $("#btnGo").click(
        function () {

            BindGrid();

        }
        );




         $("#btnDetail").click(
        function () {

         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
            if($.trim(SelectedRow) == "")
           {
             alert("No Bill is selected to Edit");
             return;
           }
          BillId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillId');

            $.ajax({
                    type: "POST",
                    data: '{ "BI": "' + BillId + '"}',
                    url: "managedeliveredorders.aspx/InsertTemp",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {
                        


                        var obj = jQuery.parseJSON(msg.d);   

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                    BindTempBills();
                    
                    }

                });
        
           
                   $('#dvEdit').dialog(
            {
            autoOpen: false,

            width:800,
            height:700,
            resizable: false,
            modal: true,
  
            });
            linkObj = $(this);
            var dialogDiv = $('#dvEdit');
            dialogDiv.dialog("option", "position", [150, 100]);
            dialogDiv.dialog('open');
            return false;
        }
        );




        
          


          


       

        });
    


    
    </script>


     <form   runat="server" id="formID" method="post">

     <asp:HiddenField ID="hdnDate" runat="server"/>
  
<div id="content">

             <div id="rightnow">
            <h3 class="reallynow">
                <span>Delivered Orders</span>
                <br />
            </h3>
            <div class="youhave">
              <table class="top_table">
                                <tr><td>Date From:</td><td>
                         
                                <input type="text" readonly="readonly"   class="form-control input-small" style="width:120px;background-color:White"  id="txtDateFrom" /></td><td>Date To:</td><td><input type="text" readonly="readonly"  class="form-control input-small" style="width:120px;background-color:White"    id="txtDateTo" />
                                </td>
                                <td><div id="btnGo"  class="btn btn-primary btn-small"  >Go</div></td>
                                </tr>
                                </table>

                <table id="jQGridDemo">
                </table>
                <div id="jQGridDemoPager">
                </div>

                 <table class="category_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                             <td> <div id="btnDetail"  class="btn btn-primary btn-small" >Show Bill Detail</div></td>
                                            
                                            </tr>
                                            </table>

               
            </div>
        </div>

            </div>







  <div id="dvEdit" style="display:none">
  <table width="100%" cellpadding="0" >
                    

                     <tr>
                     <td valign="top">
                     <table style="width:100%">
                     <tr>
                     <td valign="top">
                     <table  cellpadding="1" style="border:solid 1px silver; width:100%;height:172px">
                         <tr style="background-color:#E6E6E6;font-weight:bold;height:10px">
                         <td colspan="100%">Bill Information</td>
                         </tr>
                         
                    <tr>
                    <td colspan="100%" valign="top">
                     

                    <table cellpadding ="5">
                      <tr>
                     <td align="right">Bill No:</td><td><div id="dvBillno"><asp:Literal ID="ltBillNo" runat ="server"></asp:Literal></div></td>
                     </tr>
                     <tr>
                     <td align="right">Bill Date:</td><td><div id="dvBillDate"><asp:Literal ID="ltBillDate" runat ="server"></asp:Literal></div></td>
       
                     </tr>
                   

                     <tr>
                      <td align="right">Customer Name:</td><td><div id ="dvCustomer"><asp:Literal ID="ltCustomer" runat ="server"></asp:Literal></div></td>
                       
                       </tr>
                         <tr>
                     <td align="right">Bill Amount:</td><td><div id="dvBillAmount"><asp:Literal ID="ltBillAmount" runat ="server"></asp:Literal></div></td>
       
                     </tr>

                      </table>

                      </td>
   
                     </tr>
                    
                     

                    
                    
                    
                    
                    </table>
                    
                    </td>
                    </tr>
                   
                     </table>
                  </td>
                    
                     <td  valign="top">
                     
                     <table  cellpadding="1" style="border:solid 1px silver;width:100%" >
                         <tr style="background-color:#E6E6E6;font-weight:bold">
                         <td colspan="100%">Customer Information</td>
                         </tr>
                    <tr>
                    <td colspan="100%">
                     

                    <table>
                       
                      <tr><td style="text-align:right">Recepient Name:</td><td colspan="100%"><div id ="dvRecepient"><asp:Literal ID="ltRecepientname" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td style="text-align:right">Address:</td><td colspan="100%"><div id ="dvAddress"><asp:Literal ID="ltAddress" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td style="text-align:right">Recepient Mobile:</td><td colspan="100%"><div id ="dvRecepientMobile"><asp:Literal ID="ltRecepientMobile" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td style="text-align:right">Recepient Phone:</td><td colspan="100%"><div id ="dvRecepientPhone"><asp:Literal ID="ltRecepientPhone" runat ="server"></asp:Literal></div></td><td></td></tr>
                 
                    </table>
                    
                    </td>
                    </tr>
                   
                     </table>
                     </td>

                     </tr>
                    
                     </table>
                     <tr>
                     <td colspan="100%">
                  

                     
                      
             <table class="table table-bordered table-striped table-hover" style="width:100%" id="tbProductInfo">
										<thead>
											<tr>
                                                	<th style="width: 150px">
                                            Image
                                        </th>
												<th style="width: 150px">
                                            Description
                                        </th>
                                     
                                        <th style="width: 35px">
                                            Qty
                                        </th>
                                        
                                          <th style="width: 80px">
                                            Price
                                        </th>
                                         <th style="width: 100px">
                                            Amount
                                        </th>
                                       

											</tr>
										</thead>
										 
										
										</table>

                      
                     
                      
                     
                     
                     </td>
                     </tr>

                     <tr>
                     <td  >

                    <table>
                    <tr>
                    <td valign="top" style="width:200px"><%--<table>
                    <tr><td>Remarks:</td><td> <textarea id="txtRemarks" rows="3" style="width:190px"></textarea></td></tr>
                      
                   
                   
                     </table>--%>
                     </td>
                    
                    <td valign="top"><table>
                     <tr>
                     <td valign="top">
                     <table>
                     <tr><td >Gross Amount:</td><td><div id ="dvsbtotal"></div></td></tr>

                     <tr><td>Delivery Charges:</td><td><div id ="dvdelivery"></div></td></tr>
                     <tr><td >Net Amount:</td><td><div id ="dvnetAmount"></div></td></tr>
                    
                     </table>
                     </td>
              
                        
                     </tr>

                     </table></td>
                    </tr>
                    </table> 
                     </td>
                     </tr>
                  
  
  </div>

  

</form>



<script type="text/javascript">
         function BindGrid()
     {
      var DateFrom=$("#txtDateFrom").val();
     var DateTo=$("#txtDateTo").val();
     jQuery("#jQGridDemo").GridUnload();

              jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/DeliverBillsGetByDate.ashx?dateFrom='+DateFrom+'&dateTo='+DateTo,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['BillNo','OrderId','BillDate', 'CustomerName', 'Recipient','Address','Mobile','BillValue','CustomerId','RecipientFirstName','RecipientLastName','RecipientMobile','RecipientPhone','City','Area','Street','Address','Pincode','BillValue','DisPer','DisAmt','ServiceTaxPer','ServiceTaxAmt','ServiceChargePer','ServiceChargeAmt','VatPer','VatAmt','NetAmount','Remarks','ExecutiveId','IPAddress'],
            colModel: [
              { name: 'BillId',key:true, index: 'BillId', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:false },
                        { name: 'OrderId', index: 'OrderId', width: 150, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},
                        { name: 'strBD', index: 'strBD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},
   		                { name: 'CustomerName', index: 'CustomerName', width: 150, stype: 'text', sortable: true,hidden:false, hidden: false, editable: true ,editrules: { required: true }},
                        { name: 'Recipient', index: 'Recipient', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'CompleteAddress', index: 'CompleteAddress', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                        { name: 'RecipientMobile', index: 'RecipientMobile', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
   		                { name: 'BillValue', index: 'BillValue', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                        { name: 'CustomerId',key:true, index: 'CustomerId', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                        { name: 'RecipientFirstName', index: 'RecipientFirstName', width: 150, stype: 'text', sortable: true, hidden: true, editable: true ,editrules: { required: true }},
   		                { name: 'RecipientLastName', index: 'RecipientLastName', width: 150, stype: 'text', sortable: true,hidden:false, hidden: true, editable: true ,editrules: { required: true }},
                        { name: 'RecipientMobile', index: 'RecipientMobile', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'RecipientPhone', index: 'RecipientPhone', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},   
   		                { name: 'City', index: 'City', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'Area', index: 'Area', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'Street',key:true, index: 'Street', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                        { name: 'Address', index: 'Address', width: 150, stype: 'text', sortable: true, hidden: true, editable: true ,editrules: { required: true }},
   		                { name: 'Pincode', index: 'Pincode', width: 150, stype: 'text', sortable: true,hidden:false, hidden: true, editable: true ,editrules: { required: true }},
                        { name: 'BillValue', index: 'BillValue', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'DisPer', index: 'DisPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'DisAmt', index: 'DisAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
   		                { name: 'ServiceTaxPer', index: 'ServiceTaxPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'ServiceTaxAmt', index: 'ServiceTaxAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'ServiceChargePer', index: 'ServiceChargePer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'ServiceChargeAmt', index: 'ServiceChargeAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'VatPer', index: 'VatPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'VatAmt', index: 'VatAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
   		                { name: 'NetAmount', index: 'NetAmount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'Remarks', index: 'Remarks', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'ExecutiveId', index: 'ExecutiveId', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'IPAddress', index: 'IPAddress', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                      
                      
                       ],
            rowNum: 10,
             toolbar: [true, "top"],
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'BillId',
            viewrecords: true,
            height: "100%",
            width:"400px",
            sortorder: 'desc',
            caption: "Bills List",
         
             ignoreCase: true,
                    
             
        });

         var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });


           $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }

                     );
         
    
        var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '600');
        

      }
        
    </script>

</asp:Content>

