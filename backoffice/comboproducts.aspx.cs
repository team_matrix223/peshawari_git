﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_comboproducts : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (!User.IsInRole("ComboProducts"))
            {
               // Response.Redirect("default.aspx");

            }
            BindCategories();
        }
    }



    void BindCategories()
    {

        ddlCategory.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategory.DataValueField = "CategoryId";
        ddlCategory.DataTextField = "Title";
        ddlCategory.DataBind();
        ListItem lii = new ListItem();
        lii.Text = "--Category Level 1--";
        lii.Value = "0";
        ddlCategory.Items.Insert(0, lii);


      

        ddlBrand.DataSource = new BrandBLL().GetAll();
        ddlBrand.DataValueField = "BrandId";
        ddlBrand.DataTextField = "Title";
        ddlBrand.DataBind();
        ListItem l2 = new ListItem();
        l2.Text = "--Choose Brand--";
        l2.Value = "0";
        ddlBrand.Items.Insert(0, l2);

    }



    [WebMethod]
    public static string BindSubCategories(int CatId)
    {

        string SubCat = new CategoryBLL().GetOptions(CatId);


        string ProductHTML = "";// new ProductsBLL().GetProductHTMLByCategoryLevel2(CatId);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {

            SubCatOptions = SubCat,
            ProductHtml = ProductHTML

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindCategoryLevel3(int CatId)
    {

        string SubCat = new CategoryBLL().GetOptions(CatId);


        string ProductHTML = "";// new ProductsBLL().GetProductHTMLByCategoryLevel3(CatId);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {

            SubCatOptions = SubCat,
            ProductHtml = ProductHTML

        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string Insert(int ProductId, Int32 CategoryId, Int32 SubCategoryId, Int32 CategoryLevel3, Int32 Brand, string Name, string ShortName, string Description, string ShortDesc, bool IsActive, string Unit, Int32 Qty, string Type, decimal Price, decimal MRP, string ItemCode, string Descr, string PhotoUrl)
    {


        Products objProducts = new Products()
        {
            ProductId = ProductId,
            CategoryId = CategoryId,
            SubCategoryId = SubCategoryId,
            CategoryLevel3 = CategoryLevel3,
            BrandId = Brand,
            Name = Name,
            ShortName = ShortName,
            Description = Description,
            ShortDescription = ShortDesc,
            IsActive = IsActive,
            Unit = Unit,
            Qty = Qty,
            Type = Type,
            Price = Price,
            MRP = MRP,
            ItemCode = ItemCode,
            descr = Descr,
            PhotoUrl = PhotoUrl,


        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new ProductsBLL().ComboProductsInsertUpdate(objProducts);
        var JsonData = new
        {
            Products = objProducts,
            Status = status
        };
        return ser.Serialize(JsonData);



    }



}