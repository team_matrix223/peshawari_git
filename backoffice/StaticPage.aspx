﻿<%@ Page Language="C#" MasterPageFile="admin.master" AutoEventWireup="true"
    CodeFile="StaticPage.aspx.cs" Inherits="Admin_Default4" Title="Untitled Page" %>
<%@ MasterType VirtualPath="admin.master" %>
 
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" runat="Server">
    <link href="css/custom-css/cms.css" rel="stylesheet" />


<form runat="server" id="frm11"  >
<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Parent Pages</span>
                    </h3>
				   <div class="youhave">

<table align="center"  class="AdminHeader" style="background:url(breadcrumbBG.gif);background-repeat:repeat-y;" width="100%">
 
</table>

                         <table style="width: 100%">
                        
                            <%if (m_bShow == true)
                               { %>
                             
                             <%} %>
                             </table>
                             
                       <table class="top_table">
                           <tr>
                                 <td class="Titles">
                                     PageId:</td>
                                 <td>
                                         <asp:DropDownList ID="ddlPage" runat="server" Height="25px" AutoPostBack="True" onselectedindexchanged="ddlPage_SelectedIndexChanged">
                                </asp:DropDownList>
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                             ControlToValidate="ddlPage" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                   </td>
                             </tr>
                             <tr>
                                 <td class="Titles">
                                      Title:</td>
                                 <td>
                                
                                     <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                         ControlToValidate="txtTitle" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                                 </td>
                             </tr>
                       </table> 
                        
                       <table class="contant_editor">   
                           <tr>
                               <td class="Titles">
                                      Content:</td>
                           </tr> 
                             <tr>                                 
                                 <td>
                                    <FCKeditorV2:FCKeditor ID="EditorDescription" runat="server" BasePath="~/backoffice/fckeditor/"
                                Height="400px">
                            </FCKeditorV2:FCKeditor>
                                   
                                   </td>
                             </tr>
                      </table>                             

                        
                                      <table class="midle_table">
                                          <tr>
                                           <td colspan="100%"> 
                                            <h3 class="reallynow">
                                                <span>Meta Tags</span>
                                            </h3>
                                           </td>
                                           </tr>
                                       <tr>
                                      <td>Url Name:</td>
                                      <td><asp:TextBox ID="txtPageTitle" runat="server"></asp:TextBox>
                                      
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                         ControlToValidate="txtPageTitle" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                                      </td>
                                      </tr>

                                      <tr>
                                      <td>Keywords:</td>
                                      <td> <asp:TextBox ID="txtKeywords" runat="server"></asp:TextBox></td>
                                      </tr>   
                                                                                                                         
                                      <tr>
                                      <td>Page Description:</td>
                                      <td><asp:TextBox ID="txtPageDescription" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                      </tr>
                                      </table>

                       <table class="top_table">
                             <tr>
                                 <td class="Titles">
                                     Show On:</td>
                                 <td>
                                      
                                      <asp:DropDownList ID="ddlShowOn" runat="server">
                                      <asp:ListItem Text="Header"></asp:ListItem>
                                      <asp:ListItem Text="Footer"></asp:ListItem>
                                       
                                      </asp:DropDownList>
                                      </td>
                             </tr>
                             <tr>
                                 <td class="Titles">
                                     IsActive:</td>
                                 <td>
                                      <asp:CheckBox ID="chkIsActive" runat="server" />
                                   
                                   </td>
                             </tr>
                             <tr>
                                 <td class="Titles">
                                   Can't  Deleted:</td>
                                 <td>
                                 
                                 <asp:CheckBox ID="chkFullPage" Checked="true" runat="server" />
                                 </td>
                             </tr>
                       </table>

                       <table class="category_table">
                             <tr>
                                 <td>
                                     <asp:Button ID="btnSubmit" CssClass="CustomButtons" runat="server" 
                                         Text="Create Page" onclick="btnSubmit_Click" />
                                         
                                 </td>
                             </tr>
                         </table>                       
                                        

    </div>
    </div>
   
   
   
   
    
   
    </div>

       </form>
</asp:Content>
