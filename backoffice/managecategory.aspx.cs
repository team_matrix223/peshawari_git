﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;

public partial class backoffice_managecategory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!User.IsInRole("CategoryLevel1"))
        //{
        //    Response.Redirect("default.aspx");

        //}
    }


    [WebMethod]
    public static string BindCategories()
    {
        List<Category>categoryData = new CategoryBLL().GetByParentId(0);
        JavaScriptSerializer ser = new JavaScriptSerializer();
       

        var JsonData = new
        {
            CategoryData = categoryData,

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string BindFeaturedCategories()
    {
        List<Category> categoryData = new CategoryBLL().GetFeaturedCategories();
        JavaScriptSerializer ser = new JavaScriptSerializer();
       

        var JsonData = new
        {
            CategoryData = categoryData,

        };
        return ser.Serialize(JsonData);

    }



    [WebMethod]
    public static string ChangeOrder(string masterArr,Int16 Type)
    {
        StringBuilder strBuilder = new StringBuilder();
        string[] catData = masterArr.Split(',');
        if (Type == 1)
        {
            strBuilder.Append("update dbo.ProductCategories set DisplayOrder=0;");
        }

        else
        {
            strBuilder.Append("update dbo.ProductCategories set FeaturedOrder=0;");
        
        }

        for (int i = 0; i < catData.Length; i++)
        {

            if (Type == 1)
            {
                strBuilder.Append(string.Format("update dbo.ProductCategories set DisplayOrder={1} where CategoryId={0}; ", catData[i], i + 1));
            }
            else
            {
                strBuilder.Append(string.Format("update dbo.ProductCategories set FeaturedOrder={1} where CategoryId={0}; ", catData[i], i + 1));
           
            }

        }
        int status = new CategoryBLL().ChangeOrder(strBuilder.ToString());
        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Status = 1,

        };
        return ser.Serialize(JsonData);

    }

  
    [WebMethod]
    public static string Insert(int id, string title, string Description, bool isActive,bool ShowOnHome,string MetaTitle,string MetaKeyword,string MetaDescription,string PhotoUrl)
    {

        Category  objBookCategory = new Category ()
        {
            CategoryId = id,
            Title = title.Trim(),
            Description = Description.Trim(),
            IsActive = isActive,
            ParentId=0,
            Level=1,
            ShowOnHome=ShowOnHome,
            MetaDescription=MetaDescription,
            MetaKeyword=MetaKeyword,
            MetaTitle=MetaTitle,
            PhotoUrl=PhotoUrl
           
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new CategoryBLL().InsertUpdate(objBookCategory);
        var JsonData = new
        {
            Category = objBookCategory,
            Status = status
        };
        return ser.Serialize(JsonData);



    }
}