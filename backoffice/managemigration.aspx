﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="managemigration.aspx.cs" Inherits="backoffice_managemigration" %>
 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/master.css" rel="stylesheet" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <script type="text/javascript">

        function CheckboxChange(ProductId, ProductName) {

            if ($("#chkactive_"+ProductId+"").prop("checked") == true) 
            {

                var counterId = Number($("#hdnCounter").val()) + 1;
                $("#hdnCounter").val(counterId);

                var tr = "<tr><td><input type='hidden'  id='hdnPid" + counterId + "'  counter='" + counterId + "' value = "+ProductId+"  name='dvPid'/><div name='dvPid' id = 'txtpid " + counterId + "'  counter='" + counterId + "'  >" + ProductId + "</div></td><td><div name='dvPid' id = 'txtpname " + counterId + "'  counter='" + counterId + "'  >" + ProductName + "</div></td><td> <div class='btn btn-primary btn-small' name='btnDelete' id='btn_" + ProductId + "'>Delete</div></td></tr>";

                $("#tbMainList").append(tr);
            }
            else {

                var btnDel = $("#btn_" + ProductId);

                var tr = btnDel.closest("tr");
              
                tr.remove();
               
                
              
            }
             
         

            }

            $(document).on("click", "div[name='btnDelete']", function (event) {
                var rowindex = Number($(this).closest('tr').index());

                var tr = $(this).closest("tr");
                var Id = $(this).attr("id");
                var arrPid = Id.split('_');
                var vid = arrPid[1];
                tr.remove();
              
                $("#chkactive_" + vid + "").prop("checked", false);
            });




            function Reset() {
                $("#<%=ddlnewCategory.ClientID %>").val("0");
                $("#<%=ddlnewSubCategories.ClientID %>").val("0");
                $("#<%=ddlnewCategoryLevel3.ClientID %>").val("0");

                $("#hdnCounter").val("0");
                $('#tbMainList tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            }





            $(document).ready(
        function () {




            $("#<%=ddlnewSubCategories.ClientID %>").change(
            function () {

                $("#<%=ddlnewCategoryLevel3.ClientID %>").html('');

                var sid = $("#<%=ddlnewSubCategories.ClientID %>").val();
                if (sid == 0) {

                    return;
                }

                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemigration.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
 
                        if (obj.SubCatOptions == "") {

                            $("#<%=ddlnewCategoryLevel3.ClientID %>").html("<option value='0'>--Category Level 3--</option>");
                        }
                        else {

                            $("#<%=ddlnewCategoryLevel3.ClientID %>").html(obj.SubCatOptions);
                        }

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                    }

                });


            }
            );



            $("#<%=ddlnewCategory.ClientID %>").change(
            function () {


                $("#<%=ddlnewSubCategories.ClientID %>").html('');

                var sid = $("#<%=ddlnewCategory.ClientID %>").val();
                if (sid == 0) {

                    return;
                }

                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemigration.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlnewSubCategories.ClientID %>").html(obj.SubCatOptions);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                    }

                });



            }
            );




            $("#<%=ddlCategoryLevel3.ClientID %>").change(
            function () {


                $("#dvrepr").html("");


                var sid = $("#<%=ddlCategoryLevel3.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemigration.aspx/BindCategoryLevel3",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);



                        $("#dvrepr").html(obj.ProductHtml);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                    }

                });


            }

            );




            $("#<%=ddlSubCategories.ClientID %>").change(
            function () {


                $("#dvrepr").html("");
                $("#<%=ddlCategoryLevel3.ClientID %>").html('');

                var sid = $("#<%=ddlSubCategories.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemigration.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlCategoryLevel3.ClientID %>").html(obj.SubCatOptions);
                        $("#dvrepr").html(obj.ProductHtml);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                    }

                });


            }

            );




            $("#<%=ddlCategory.ClientID %>").change(
            function () {

                $("#dvrepr").html("");


                $("#<%=ddlSubCategories.ClientID %>").html('');
                $("#<%=ddlCategoryLevel3.ClientID %>").html('');
                var sid = $("#<%=ddlCategory.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemigration.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlSubCategories.ClientID %>").html(obj.SubCatOptions);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                    }

                });


            }

            );



            $("#<%=rdbshiftProduct.ClientID %>").change(
    function () {

        if ($("#<%=rdbshiftProduct.ClientID %>").prop('checked') == true) {
            $("#<%=rdbShiftCategory.ClientID %>").prop('checked', false);

        }


    }
    );
            $("#<%=rdbShiftCategory.ClientID %>").change(
    function () {

        if ($("#<%=rdbShiftCategory.ClientID %>").prop('checked') == true) {
            $("#<%=rdbshiftProduct.ClientID %>").prop('checked', false);

        }


    }
    );




            $("#btnmigrate").click(
                          function () {




                              var oldcategory = $("#<%=ddlCategory.ClientID %>").val();

                              if (oldcategory == "0") {
                                  alert("Please Choose Source Category Level1 ");
                                  $("#<%=ddlCategory.ClientID %>").focus();
                                  return;
                              }

                              var oldsubcategory = $("#<%=ddlSubCategories.ClientID %>").val();
                              if (oldsubcategory == null) {
                                  oldsubcategory = "0";
                              }
                              var oldcategory3 = $("#<%=ddlCategoryLevel3.ClientID %>").val();
                              if (oldcategory3 == null) {
                                  oldcategory3 = "0";
                              }

                              var category = $("#<%=ddlnewCategory.ClientID %>").val();
                              if (category == "0") {
                                  alert("Please Choose Destination Category Level1 ");
                                  $("#<%=ddlnewCategory.ClientID %>").focus();
                                  return;
                              }
                              var subcategory = $("#<%=ddlnewSubCategories.ClientID %>").val();
                              if (subcategory == null) {
                                  subcategory = "0";
                              }
                              var category3 = $("#<%=ddlnewCategoryLevel3.ClientID %>").val();
                              if (category3 == null) {
                                  category3 = "0";
                              }


                              ProductArr = [];
                              var status1 = "";
                              if ($("#<%=rdbShiftCategory.ClientID %>").prop('checked') == true) {
                                  status1 = "SC";
                                  if (oldsubcategory == "0") {
                                      alert("You Cannot directly shift the category Level1 to another Level..Please try again");
                                      return;
                                  }

                                  if (oldsubcategory != "0" && category3 != "0") {
                                      alert("You Cannot  shift the category Level2 to higher Level..Please try again");
                                      return;
                                  }


                              }
                              else {


                                  if (($("#<%=ddlnewCategoryLevel3.ClientID %> option").length) > 1 && category3 == "0") {
                                      alert("Please Choose Level3 ");
                                      $("#<%=ddlnewCategoryLevel3.ClientID %>").focus();
                                      return;
                                  }

                                  status1 = "SP";
                              }



                              $("input[name='dvPid']").each(
            function (x) {

                var counterId = $(this).attr("counter");

                ProductArr[x] = $("#hdnPid" + counterId).val();



            }

            );

                              if (status1 == "SP") {
                                  if (ProductArr == "") {
                                      alert("Please Select Product To Migrate");
                                      return;
                                  }

                              }

                              $.ajax({
                                  type: "POST",
                                  data: '{"oldCategory":"' + oldcategory + '","oldSubCategory":"' + oldsubcategory + '","oldCategory3":"' + oldcategory3 + '","Category": "' + category + '","SubCategory": "' + subcategory + '","Category3": "' + category3 + '","status1": "' + status1 + '","arrProduct": "' + ProductArr + '"}',

                                  url: "managemigration.aspx/Insert",
                                  contentType: "application/json",
                                  dataType: "json",
                                  success: function (msg) {


                                      var obj = jQuery.parseJSON(msg.d);
                                      if (obj.Status == "0") {
                                          alert("Insertion Failed. Please try again later.");
                                          return;
                                      }
                                      jQuery("#jQGridDemo").jqGrid('addRowData', 0, obj.Purchase, "last");

                                      alert("Products Migrated Successfully");
                                      Reset();



                                  },
                                  error: function (xhr, ajaxOptions, thrownError) {

                                      var obj = jQuery.parseJSON(xhr.responseText);
                                      alert(obj.Message);
                                  },
                                  complete: function () {


                                      $.uiUnlock();
                                  }


                              });
                          }
            );


        }
        );
    </script>
  
 <form id="frmProducts" runat="server">
     <asp:HiddenField ID="hdVariationId" Value="-1" runat="server" />
       <input type="hidden" id="hdnCounter" value="0" />
 <div id="content">

     <div id="rightnow">
                    <h3 class="reallynow">
                        <span>Migrate Products</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">
                 <asp:ScriptManager ID="Scrpt1" runat="server"></asp:ScriptManager>
               <asp:UpdatePanel UpdateMode="Conditional" ID="updProducts" runat="server">
                
               <ContentTemplate >
               <table>
                    <tr>
                  <td colspan="100%">
                    <table class="top_table">
                    <tr> 
                        <td class="Titles">Source Level 1:</td>
                        <td><asp:DropDownList ID="ddlCategory" runat="server">
                            <asp:ListItem Text="--Category Level 1--" Value="0"></asp:ListItem>
                            </asp:DropDownList>                           
                        </td>
                    </tr>
                    <tr>
                       <td class="Titles">Source Level 2:</td>
                       <td><asp:DropDownList ID="ddlSubCategories"  Width="330px" runat="server">
                           <asp:ListItem Text="--Category Level 2--" Value="0"></asp:ListItem>
                           </asp:DropDownList>                                     
                       </td>
                    </tr>
                    <tr id="catLevel3" runat="server">
                      <td class="Titles">Source Level 3:</td>
                      <td><asp:DropDownList ID="ddlCategoryLevel3"  Width="330px" runat="server">
                          <asp:ListItem Text="--Category Level 3--" Value="0"></asp:ListItem>
                          </asp:DropDownList>                                           
                          <span style="color:#D9395B;font-weight:normal">   <%=m_sSubMessage %></span>
                      </td>
                   </tr>
                  </table>
                </td>
                  
             </tr>
                              
                            


                             
                   <tr>
                       <td colspan="100%">
                           <table width="100%">
                               <tr>
                                   <td>
                                     <div id="dvrepr" class="migrate_product" style="overflow:scroll;height:450px;width:100%; box-shadow: 3px 3px 5px #00000061;"></div>
                                   </td>
                               </tr>                           
                           </table>                   
                       </td>
                   </tr>

           </table>
                            

</ContentTemplate>
               </asp:UpdatePanel>
                       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                          <ContentTemplate>

                          
                    <table class="top_table">
                        <tr>
                            <td class="Titles">
                                Destination Level 1:</td>
                            <td>
                                <asp:DropDownList ID="ddlnewCategory" runat="server" onselectedindexchanged="ddlnewCategory_SelectedIndexChanged">
                                    <asp:ListItem Text="--Category Level 1--" Value="0"></asp:ListItem>
                                </asp:DropDownList>

                                <asp:RequiredFieldValidator ID="reqddlnewCategory" ControlToValidate="ddlnewCategory" InitialValue="0" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>

                            </td>

                            
                        </tr>
                       <tr>
                            <td class="Titles">
                                Destination Level 2:</td>
                            <td>
                                <asp:DropDownList ID="ddlnewSubCategories" runat="server" onselectedindexchanged="ddlnewSubCategories_SelectedIndexChanged">

                                    <asp:ListItem Text="--Category Level 2--" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqddlnewSubCategories" ControlToValidate="ddlnewSubCategories" InitialValue="0" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>

                            </td>
                            

                        </tr>             
                   
                        <tr id="Tr1" runat="server">
                            <td style="text-align:right" class="Titles">
                                Destination Level 3:</td>
                            <td>
                                <asp:DropDownList ID="ddlnewCategoryLevel3" Width="330px" runat="server" onselectedindexchanged="ddlnewCategoryLevel3_SelectedIndexChanged">

                                    <asp:ListItem Text="--Category Level 3--" Value="0"></asp:ListItem>
                                </asp:DropDownList>

                                <span style="color:#D9395B;font-weight:normal">   <%=m_sSubMessage %></span>
                            </td>
                        </tr>  
                        <tr>
                            <td></td>
                            <td style="font-weight:bold">
                                <asp:RadioButton ID="rdbshiftProduct" GroupName="xyz" Text="Shift Product Only" runat="server" Checked="true" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="font-weight:bold">
                                <asp:RadioButton ID="rdbShiftCategory" GroupName="xyz" Text="Shift Category" runat="server" />
                        </tr>
                 </table>  

          </ContentTemplate>
         </asp:UpdatePanel>  
                                                  
        <table class="item_table">
            <tr>
                <td colspan="100%">

                    <table id="tbMainList">
                        <thead>
                            <tr style="background:#294145;color:white">
                                <th>Product Id</th>
                                <th>Product Name</th>

                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody class="customTable">

                        </tbody>
                    </table>
                </td>
            </tr>
        </table>

        <table class="category_table">
            <tr>
                <td>
                    <%--  <asp:Button runat="server" ID = "btnmigrate" Text ="Migrate" />--%>
                        <div id="btnmigrate" class="btn btn-primary btn-small">
                            Migrate</div>
                </td>
            </tr>

        </table>                            
                             

               
</div>
                   
 
     </div>

       

     </div>
 </form>

</asp:Content>

