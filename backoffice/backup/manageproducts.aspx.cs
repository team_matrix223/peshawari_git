﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class backoffice_manageproducts : System.Web.UI.Page
{
    protected string m_sMessage = "";
    protected string m_sSubMessage = "";
    public static  string PhotoUrl="";
    protected bool m_bShow = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                BindCategories();
                btnUpdate.Visible = false;
                btnCancel.Visible = false;
            }
        }
        catch (Exception ex)
        {
        }
    }


    public void BindCategories()
    {
        try
        {
            ddlCategory.DataSource = new CategoryBLL().GetByLevel(1);
            ddlCategory.DataValueField = "CategoryId";
            ddlCategory.DataTextField = "Title";
            ddlCategory.DataBind();
            ListItem li = new ListItem();
            li.Text = "--Select Category--";
            li.Value = "0";
            ddlCategory.Items.Insert(0, li);

        }
        finally { }

    }
    public void BindSubCategories(int ParentId,DropDownList ddl)
    {
        try
        {
            if (ddlCategory.SelectedIndex != 0)
            {
                lblMessage.Visible = false;
                m_bShow = true;
                ddl.DataSource = new CategoryBLL().GetByParentId(ParentId);
                ddl.DataTextField = "Title";
                ddl.DataValueField = "CategoryId";
                ddl.DataBind();
                ddl.Items.Insert(0, "--Select--");

            }
            else
            {
                m_sMessage = "Please Select  Album";

            }
        }
        finally
        {
        }

    }

    public void BindCategoryLevel3(int ParentId)
    {
        try
        {
            if (ddlCategory.SelectedIndex != 0)
            {
                lblMessage.Visible = false;
                m_bShow = true;
                ddlCategoryLevel3.DataSource = new CategoryBLL().GetByParentId(ParentId);
                ddlCategoryLevel3.DataTextField = "Title";
                ddlCategoryLevel3.DataValueField = "CategoryId";
                ddlCategoryLevel3.DataBind();
                ListItem li = new ListItem();
                li.Text = "--Select Category Level 3--";
                li.Value = "0";
                ddlCategoryLevel3.Items.Insert(0,li);

            }
            else
            {
                m_sMessage = "Please Select  Album";

            }
        }
        finally
        {
        }

    }

    public void Reset()
    {
        PhotoUrl = "";
        hdnProductId.Value = "0";
        txtTitle.Text = string.Empty;
        txtDesc.Text = string.Empty;
 
        chkIsActive.Checked = true ;
        txtShortName.Text = "";
        txtMarketPrice.Text = "";
        hdnProductId.Value = "0";
        lblimg.Text = string.Empty;
        hdnlblimg.Value = string.Empty;
        btnUpdate.Visible = false;
        btnCancel.Visible = false;
        btnAdd.Visible = true;
        txtPrice.Text = "";
        txtQty.Text = "";
        ddlUnits.Text = "";
    }

    public void FillDataListByPageId(int SubCategoryId)
    {
        repProducts.DataSource = null;
        repProducts.DataBind();
        

            repProducts.DataSource = new ProductsBLL().GetBySubCategoryId(SubCategoryId);
            repProducts.DataBind();
         
    }


    public void FillDataListByCategoryLevel3(int SubCategoryId)
    {
        repProducts.DataSource = null;
        repProducts.DataBind();
        repProducts.DataSource = new ProductsBLL().GetByCategoryLevel3(SubCategoryId);
        repProducts.DataBind();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {

        InsertUpdate();

    }

    protected void repProducts_EditCommand(object source, DataListCommandEventArgs e)
    {
       



    }
   
    public void InsertUpdate()
    {

        if (ddlUnits.SelectedValue == "Piece")
        {

            if (txtQty.Text.Contains("."))
            {

                Response.Write("<script>alert('Invalid Quantity.  Quantity should be Numbers Only.')</script>");
                txtQty.Focus();
            }
             
        }
        
          int FinalCatId =0;
        if (ddlSubCategories.SelectedIndex > 0)
        {

          FinalCatId= Convert.ToInt32(ddlSubCategories.SelectedValue);

            if (ddlCategoryLevel3.Items.Count > 1 && ddlCategoryLevel3.SelectedIndex == 0)
            {
                Response.Write("<script>alert('Please select Level 3 Category')</script>");
             
                ddlCategoryLevel3.Focus();
                return;

            }else  if (ddlCategoryLevel3.SelectedIndex > 0)
            {
                FinalCatId = Convert.ToInt32(ddlCategoryLevel3.SelectedValue);
            }
        }
        else
        {
            Response.Write("<script>alert('Please Choose Sub Category')</script>");
        }

        


        string imageName = hdnlblimg.Value;
        imageName = PhotoUrl;
        if (fuImage.HasFile)
        {

            imageName = CommonFunctions.UploadImage(fuImage, "~/backoffice/ProductImages/", true, 160, 145, true, 400, 400);
        }

        if (ddlSubCategories.SelectedValue == "--Select--")
        {
            Response.Write("<script>alert('Please Choose Sub Category')</script>");
           
            ddlSubCategories.Focus();
            return;
        }

       


        Products objProducts = new Products()
        {
            ProductId = Convert.ToInt32(hdnProductId.Value),
            CategoryId = Convert.ToInt32(ddlCategory.SelectedValue),
            SubCategoryId = Convert.ToInt32(ddlSubCategories.SelectedValue),
           CategoryLevel3 = Convert.ToInt32(ddlCategoryLevel3.SelectedValue),

            Name = txtTitle.Text,
            ShortName = txtShortName.Text,
            Price = Convert.ToDecimal(txtPrice.Text),
            MarketPrice = Convert.ToDecimal(txtMarketPrice.Text),
            Qty = Convert.ToDecimal(txtQty.Text),
            Units=ddlUnits.Text ,
            Description = txtDesc.Text,
            PhotoUrl = imageName,
            IsActive = chkIsActive.Checked,


        };

        try
        {

            int Status = new ProductsBLL().InsertUpdate(objProducts);
            if (Status == 0)
            {
                Response.Write("<script>alert('Operation Failed.Please try again.');</script>");
            }
            else
            {
                if (Convert.ToInt32(hdnProductId.Value) == 0)
                {
                    Response.Write("<script>alert('Product  Added  Successfully!')</script>");
                }
                else
                {
                    Response.Write("<script>alert('Product  Updated  Successfully!')</script>");
                
                }
                    
                    if (ddlCategoryLevel3.SelectedIndex > 0)
                    {
                        FillDataListByCategoryLevel3(Convert.ToInt32(ddlCategoryLevel3.SelectedValue));
                    }
                    else
                    {
                        FillDataListByPageId(Convert.ToInt32(ddlSubCategories.SelectedValue));
                    }
                
             
                Reset();
                
            }
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('" + ex.Message + "')</script>");

        }
    
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {

        InsertUpdate();

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindSubCategories(Convert.ToInt32(ddlCategory.SelectedValue),ddlSubCategories);
    }
    protected void ddlSubCategories_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCategoryLevel3(Convert.ToInt32(ddlSubCategories.SelectedValue));

        repProducts.DataSource = null;
        repProducts.DataBind();
        if (ddlCategoryLevel3.Items.Count == 1)
        {
            FillDataListByPageId(Convert.ToInt32(ddlSubCategories.SelectedValue));
        }

    }
    protected void ddlCategoryLevel3_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCategoryLevel3.SelectedIndex > 0)
        {
            FillDataListByCategoryLevel3(Convert.ToInt32(ddlCategoryLevel3.SelectedValue));
        }
    }
    protected void repProducts_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            int ProductId = Convert.ToInt32(((HiddenField)e.Item.FindControl("hdnRepProductId")).Value);
            hdnProductId.Value = ProductId.ToString();
            SqlDataReader dr = new ProductsDAL().GetByProductId(ProductId);
            if (dr.HasRows == true)
            {
                if (dr.Read())
                {
                    txtDesc.Text = dr["Description"].ToString();
                    txtMarketPrice.Text = dr["MarketPrice"].ToString();
                    txtPrice.Text = dr["Price"].ToString();
                    txtQty.Text = dr["Qty"].ToString();
                    txtShortName.Text = dr["ShortName"].ToString();
                    txtTitle.Text = dr["Name"].ToString();
                    chkIsActive.Checked = Convert.ToBoolean(dr["IsActive"]);
                    ddlCategory.SelectedValue = dr["CategoryId"].ToString();
                    ddlSubCategories.SelectedValue = dr["SubCategoryId"].ToString();
                    ddlCategoryLevel3.SelectedValue = dr["CategoryLevel3"].ToString();

                    ddlUnits.Text = dr["Units"].ToString();
                    PhotoUrl = dr["PhotoUrl"].ToString();
                    btnUpdate.Visible = true;
                    btnCancel.Visible = true;
                    btnAdd.Visible = false;
                }
            }
        
        }
    }
}