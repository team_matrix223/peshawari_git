﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="manageproducts.aspx.cs" Inherits="backoffice_manageproducts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <form id="frmVid" runat="server">
<asp:HiddenField ID= "hdnProductId" runat="server" Value ="0" />
 <div id="content" style="padding:6px">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Products</span>
                     
                        <br />
                    </h3>
				   <div class="youhave" style="padding-left:0px">
 
 
  

    <table align="center" border="0"  style="border: 0px solid black;
                margin: 1px" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td>
                    <asp:ScriptManager ID="Scrpt1" runat="server"></asp:ScriptManager>
             
                 <%--   <asp:UpdatePanel ID="up1" runat="server">
                    <ContentTemplate>--%>
                        <table style="width: 100%">
                             <tr>
                                 <td>
                                    </td>
                                 <td>
                                    </td>
                                 <td>
                                    </td>
                             </tr>
                             <tr>
                                 <td style="text-align:right" class="Titles">
                                     Category:</td>
                                 <td>
                                     <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True" 
                                        
                                         Width="192px" onselectedindexchanged="ddlCategory_SelectedIndexChanged">
                                     </asp:DropDownList>
                                    
                                     <asp:Label ID="lblMessage" runat="server" Text="No Sub Page Available" 
                                         Visible="False"></asp:Label>
                                         <span style="color:#D9395B;font-weight:normal">  <%=m_sMessage %></span>
                                     </td>
                                 <td style="color:Red">
                                
                                   </td>
                             </tr>
                              
                             <tr>
                                 <td style="text-align:right" class="Titles">
                                     Sub Category:</td>
                                 <td>
                                       <asp:DropDownList ID="ddlSubCategories"  Width="192px" runat="server" 
                                           AutoPostBack="True" 
                                           onselectedindexchanged="ddlSubCategories_SelectedIndexChanged" >
                                            </asp:DropDownList>
                                       <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                           ControlToValidate="ddlSubCategories" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                   
                                                  <span style="color:#D9395B;font-weight:normal">   <%=m_sSubMessage %></span>
                                                   </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>


                              <tr>
                                 <td style="text-align:right" class="Titles">
                                     Level 3 Category:</td>
                                 <td>
                                       <asp:DropDownList ID="ddlCategoryLevel3"  Width="192px" runat="server" 
                                           AutoPostBack="True" 
                                           onselectedindexchanged="ddlCategoryLevel3_SelectedIndexChanged" >
                                            </asp:DropDownList>
                                                 
                                                  <span style="color:#D9395B;font-weight:normal">   <%=m_sSubMessage %></span>
                                                   </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>

                             <tr>
                                 <td style="text-align:right" class="Titles">
                                    Name:</td>
                                 <td>
                                      <asp:TextBox ID="txtTitle" runat="server" Width ="185px" ></asp:TextBox>
                                      <asp:RequiredFieldValidator runat="server" id="reqName" controltovalidate="txtTitle" ForeColor="Red"  errormessage="*" />
                                 </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>
                                <tr>
                                 <td style="text-align:right" class="Titles">
                                    Short Name:</td>
                                 <td>
                                      <asp:TextBox ID="txtShortName" runat="server" Width ="185px" ></asp:TextBox>
                                      <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator2" controltovalidate="txtTitle" ForeColor="Red"  errormessage="*" />
                                 </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>


                              <tr>
                                 <td style="text-align:right" class="Titles">
                                    Market Price:</td>
                                 <td>
                                      <asp:TextBox ID="txtMarketPrice" runat="server" Width ="185px" ></asp:TextBox>
                                      <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator4" controltovalidate="txtMarketPrice" ForeColor="Red"  errormessage="*" />
                                 </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>

                              <tr>
                                 <td style="text-align:right" class="Titles" >
                                 Our Price:
                                 </td>
                                 <td colspan="100%">                                    <table>
                                    <tr>
                                     <td><asp:DropDownList ID="ddlUnits" runat="server">
                                     <asp:ListItem Text="Units" Value=""></asp:ListItem>
                                     
                                     <asp:ListItem Text="Dozon" Value="Dozon"></asp:ListItem>
                                     <asp:ListItem Text="Kg" Value="Kg"></asp:ListItem>
                                     <asp:ListItem Text="Gram" Value="Gram"></asp:ListItem>
                                     <asp:ListItem Text="Piece" Value="Piece"></asp:ListItem>
                                    
                                    
                                     </asp:DropDownList>

                                     <asp:RequiredFieldValidator ID="reqUnits"
                                     ControlToValidate="ddlUnits" runat="server" ErrorMessage="*"
                                     ForeColor="Red" InitialValue=""></asp:RequiredFieldValidator>
                                     
                                     </td>
                                     <td>
                                     <asp:TextBox ID="txtQty" runat="server" placeholder="Qty" Width="40px"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator5"  Display="Dynamic"
                                     ControlToValidate="txtQty" runat="server" ErrorMessage="*"
                                     ForeColor="Red"  ></asp:RequiredFieldValidator>
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic"
                                     runat="server" ErrorMessage="*" ForeColor="Red"
ControlToValidate="txtQty" ValidationExpression="\d+(\.\d{1,2})?"></asp:RegularExpressionValidator> 

                                                                       

                                     </td>
                                     <td><asp:TextBox ID="txtPrice" placeholder="Price" Width="50px" runat="server"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                   Display="Dynamic"  ControlToValidate="txtPrice" runat="server" ErrorMessage="*"
                                     ForeColor="Red"  ></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="regPrice" Display="Dynamic"
                                     runat="server" ErrorMessage="*" ForeColor="Red"
ControlToValidate="txtPrice" ValidationExpression="\d+(\.\d{1,2})?"></asp:RegularExpressionValidator>   
                                     </td>

                                    </tr>
                                    </table>
                                    
                                    </td>
                                    <td></td>
                                 
                                 
                             </tr>

                              <tr>
                                 <td style="text-align:right" class="Titles">
                                    Description:</td>
                                 <td>
                                      <asp:TextBox ID="txtDesc" runat="server" Width ="185px" TextMode="MultiLine" ></asp:TextBox>
                                      <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator1" controltovalidate="txtDesc" ForeColor="Red"  errormessage="*" />
                                 </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>
                           

                             <tr>
                                 <td style="text-align:right" class="Titles">
                                     Url:</td>
                                 <td>
                                      <asp:FileUpload ID="fuImage" runat="server"  Width ="185px"   />
                                      <asp:Label ID = "lblimg"  runat="server" Width="175px"></asp:Label><asp:HiddenField ID="hdnlblimg" runat="server" />
                                 </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>

                             
                               
                             <tr  >
                                 <td style="text-align:right" class="Titles">
                                     IsActive:</td>
                                 <td>
                                       <input type="checkbox" id="chkIsActive" runat="server" checked="checked" data-index="2"  name="chkIsActive" />
                                 </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>
                             <tr>
                                 <td>
                                     &nbsp;</td>
                                 <td>
                                     <asp:Button ID="btnAdd" CssClass="btn btn-primary btn-small" runat="server" 
                                         Text="Add Product" onclick="btnAdd_Click" />
                                         <asp:Button ID="btnUpdate" CssClass="btn btn-primary btn-small" runat="server" 
                                         Text="Update Product" onclick="btnUpdate_Click"  />
                                          <asp:Button ID="btnCancel" CssClass="btn btn-primary btn-small" runat="server" 
                                         Text="Cancel" onclick="btnCancel_Click"  />
                                 </td>
                                  
                                 <td>
                                     &nbsp;</td>
                             </tr>
                          
                             <tr>
                                 <td>
                                     &nbsp;</td>
                                 <td>
                                     &nbsp;</td>
                                 <td>
                                     &nbsp;</td>
                             </tr>
                         </table>

                         
               


                        
                        
                 <%--   </ContentTemplate>
                    </asp:UpdatePanel>--%>
                   
                    </td>
                </tr>
            </table>


     </div>
     </div>

     <br />
     <div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Products</span>
                     
                        <br />
                    </h3>
				   <div class="youhave" style="padding-left:0px;float:left">
                  
                    <asp:Repeater ID="repProducts" runat="server" onitemcommand="repProducts_ItemCommand">

          

            <ItemTemplate>
<div style="width:150px;float:left;margin-right:5px;border:solid 1px silver">
             <table >
            <tr>
            <td>
              <asp:HiddenField ID="hdnRepProductId" runat="server" Value='<%#Eval("ProductId") %>' />
              <asp:HiddenField ID="hdnalbumID" runat="server" Value ='<%# Eval("SubCategoryId") %>' />
                 <img src="ProductImages/T_<%#Eval("PhotoUrl") %>" height="125px" width="125px" style="border:solid 5px silver" />
            </td>
            </tr>

            <tr>
            <td align="center"  >
            <asp:Label ID="lblCName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                          <asp:HiddenField ID="hdnLangTitle" runat="server" Value ='<%# Eval("ShortName") %>' />
            </td>
            </tr>

            <tr>
           <td align="center"  > <asp:LinkButton runat="server" ID="Lnkedit" CausesValidation="false" ForeColor="Black" Font-Bold="false"
                    CommandName="edit" >
                    Edit
                </asp:LinkButton></td>
            </tr>
            </table>
            
</div>

      

                

             


            </ItemTemplate>
          

</asp:Repeater>
</div>
                   
 
     </div>

       

     </div>
</form>

</asp:Content>
