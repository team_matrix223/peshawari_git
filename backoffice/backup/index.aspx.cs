﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindProductCategories();
            BindFeaturedCategories();
            BindSlider();
        }
    }
    void BindSlider()
    {
        List<Sliders> sliders = new SlidersBLL().GetAll();
        string strSliders = "";
        foreach (var i in sliders)
        {
            if (i.IsActive == true)
            {
                strSliders += "<div><img u='image' src2='SliderImages/" + i.ImageUrl + "' /></div>";
            }
        }
        ltSlider.Text = strSliders;

        



    }

    void BindFeaturedCategories()
    {
        repHomeFeaturedCategories.DataSource = new CategoryBLL().GetFeaturedCategories();
        repHomeFeaturedCategories.DataBind();
    }
    public List<Products> GetProductList(object CategoryLevel1)
    {
        return new ProductsBLL().GetRandomProductsByFeaturedCategory(Convert.ToInt16(CategoryLevel1));
    }

    void BindProductCategories()
    {

        repMenuCategories.DataSource = new CategoryBLL().GetForFrontEnd();
        repMenuCategories.DataBind();

    }
}