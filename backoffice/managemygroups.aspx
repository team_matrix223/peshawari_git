﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="managemygroups.aspx.cs" Inherits="backoffice_managemygroups" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">

    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
        <link href="css/custom-css/master.css" rel="stylesheet" />

    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">
    var m_Id = 0;
    var GroupPhotoUrl1 = "";
    var GroupPhotoUrl2 = "";
    var MobileImageUrl = "";
    var ProductsCollection = [];
    function clsProduct() {
        this.ProductId = 0;
        this.ProductName = "";
       

    }


    function ResetControls() {
        m_Id = 0;
        var txtTitle = $("#<%=txtTitle.ClientID %>").val("");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
        $("#<%=txtTitle.ClientID %>").focus();
        $("#<%=txtTitle.ClientID %>").val("");
        $("#FileUpload1").val("");
        $("#FileUpload2").val("");
        $("#<%=txtTitle.ClientID %>").focus();
        btnAdd.css({ "display": "block" });
        btnAdd.html("Add MyGroup");
        $("#txtDescription").val("");
        btnUpdate.css({ "display": "none" });
        btnUpdate.html("Update MyGroup");
        $("#chkIsActive").prop("checked", "checked");
        $("#btnReset").css({ "display": "none" });
        $("#hdnId").val("0");
        $("#txtUrl").val("");
        $("#<%=ddlSubCategoriesp.ClientID %>").html('');
//        $("#<%=ddlSubCategoriesp.ClientID %>").html('');
//        $("#<%=ddlCategoryLevel3p.ClientID %>").html('');
//        $("#<%=ddlSubCategories.ClientID %>").html('');
//        $("#<%=ddlSubCategoriesp.ClientID %>").html('');
//        $("#<%=ddlCategoryLevel3.ClientID %>").html('');
//        $("#<%=ddlCategoryLevel3p.ClientID %>").html('');
        ProductsCollection = [];

        $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
        $("#tbProducts").append(" <tr><td colspan='100%' align='center'> 	</td></tr>");
        validateForm("detach");


    }

    $(document).on("click", "#dvClose", function (event) {

        var RowIndex = Number($(this).closest('tr').index());
        var tr = $(this).closest("tr");
        tr.remove();

        ProductsCollection.splice(RowIndex, 1);

        if (ProductsCollection.length == 0) {

            $("#tbProducts").append(" <tr><td colspan='100%' align='center'></td></tr>");

        }

    });


    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {
        

        if (!validateForm("frmMyGroups")) {
            return;
        }
        var Id = m_Id;

        var Title = $("#<%=txtTitle.ClientID %>").val();
        if ($.trim(Title) == "") {
            $("#<%=txtTitle.ClientID %>").focus();

            return;
        }

      
        var IsActive = false;

        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
        }


        var HasSubgroup = false;

        if ($('#chkSubGroup').is(":checked")) {
            HasSubgroup = true;
        }

        var PhotoUrl1 = "";
        var fileUpload = $("#<%=FileUpload1.ClientID %>").get(0);
        var files = fileUpload.files;



        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            if (files[i].name.length > 0) {
                PhotoUrl1 = files[i].name;
            }

            data.append(files[i].name, files[i]);
        }

       
        if (m_Id == 0 && PhotoUrl1 == "") {
            alert("Please select Image for Combo");
            return;
        }



        var PhotoUrl2 = "";
        var fileUpload2 = $("#<%=FileUpload2.ClientID %>").get(0);
        var files2 = fileUpload2.files;

      
        for (var i = 0; i < files2.length; i++) {
            if (files2[i].name.length > 0) {
                PhotoUrl2 = files2[i].name;
            }

            data.append(files2[i].name, files2[i]);
        }
        
        if (m_Id == 0 && PhotoUrl2 == "") {
            alert("Please select Image for Combo");
            return;
        }

     

        if (files.length > 0 || files2.length > 0) {

            var options = {};
            options.url = "handlers/GroupFileUploader.ashx";
            options.type = "POST";
            options.async = false;
            options.data = data;
            options.contentType = false;
            options.processData = false;
            options.success = function (msg) {


                var obj = jQuery.parseJSON(msg);
                GroupPhotoUrl1 = obj.File1;
                GroupPhotoUrl2 = obj.File2;


            };

            options.error = function (err) { };
            $.ajax(options);
        }

        var ShowOn = $("#<%=ddlShowon.ClientID%>").val();
        var LinkUrl = $("#txtUrl").val();

        if (LinkUrl == "") {
            alert("Link Group with either categories or products");
            return;
        }

      
        var Mobilephotourl = "";
        var fileUploadmob = $("#FileUpload3").get(0);

        var filesmob = fileUploadmob.files;

        var datamob = new FormData();

        for (var i = 0; i < filesmob.length; i++) {
           
            if (filesmob[i].name.length > 0) {
                Mobilephotourl = filesmob[i].name;
            }

            datamob.append(filesmob[i].name, filesmob[i]);
        }

        if (m_Id == 0 && Mobilephotourl == "") {
            alert("Please select Image for Mobile Category");
            return;
        }
       
        if (filesmob.length > 0) {
            
            var optionsmob = {};
            optionsmob.url = "handlers/CategoryMobileFileUploader.ashx";
            optionsmob.type = "POST";
            optionsmob.async = false;
            optionsmob.data = datamob;
            optionsmob.contentType = false;
            optionsmob.processData = false;
            optionsmob.success = function (msg) {

                Mobilephotourl = msg;

            };


            optionsmob.error = function (err) { };

            $.ajax(optionsmob);



            if (Mobilephotourl.length > 0) {
                MobileImageUrl = Mobilephotourl;
            }
            
        }


        $.ajax({

            type: "POST",
            data: '{"Id":"' + Id + '", "Title": "' + Title + '","PhotoUrl1": "' + GroupPhotoUrl1 + '","PhotoUrl2": "' + GroupPhotoUrl2 + '","ShowOn": "' + ShowOn + '","LinkUrl": "' + LinkUrl + '","IsActive": "' + IsActive + '","HasSubGroup": "' + HasSubgroup + '","MobileImgUrl": "' + MobileImageUrl + '"}',
            url: "managemygroups.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == 0) {

                    alert("Insertion Failed.MyGroup with duplicate name already exists.");
                    return;
                }

                if (Id == "0") {
                    ResetControls();
                    BindGrid();
                    alert("MyGroup is added successfully.");
                }
                else {
                    ResetControls();
                    BindGrid();
                    alert("MyGroup is Updated successfully.");
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }




    function InsertProducts() {

        if (!validateForm("frmMyGroups")) {

            return;
        }

        $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

       
        var Id = m_Id;

        var Title = $("#<%=txtTitle.ClientID %>").val();
        if ($.trim(Title) == "") {
            $("#<%=txtTitle.ClientID %>").focus();

            return;
        }
      
        var IsActive = false;

        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
        }



        var HasSubgroup = false;

        if ($('#chkSubGroup').is(":checked")) {
            HasSubgroup = true;
        }

        var PhotoUrl1 = "";
        var fileUpload = $("#<%=FileUpload1.ClientID %>").get(0);
        var files = fileUpload.files;



        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            if (files[i].name.length > 0) {
                PhotoUrl1 = files[i].name;
            }

            data.append(files[i].name, files[i]);
        }
       

        if (m_Id == 0 && PhotoUrl1 == "") {
            alert("Please select Image for Combo");
            return;
        }



        var PhotoUrl2 = "";
        var fileUpload2 = $("#<%=FileUpload2.ClientID %>").get(0);
        var files2 = fileUpload2.files;


        for (var i = 0; i < files2.length; i++) {
            if (files2[i].name.length > 0) {
                PhotoUrl2 = files2[i].name;
            }

            data.append(files2[i].name, files2[i]);
        }

        if (m_Id == 0 && PhotoUrl2 == "") {
            alert("Please select Image for Combo");
            return;
        }

       
        if (files.length > 0 || files2.length > 0) {
            var options = {};
            options.url = "handlers/GroupFileUploader.ashx";
            options.type = "POST";
            options.async = false;
            options.data = data;
            options.contentType = false;
            options.processData = false;
            options.success = function (msg) {


                var obj = jQuery.parseJSON(msg);
                GroupPhotoUrl1 = obj.File1;
                GroupPhotoUrl2 = obj.File2;


            };

            options.error = function (err) { };
            $.ajax(options);
        }
      
        var ShowOn = $("#<%=ddlShowon.ClientID%>").val();
      
        if (ProductsCollection.length == 0) {
            alert("Please first add a Product to link");
            $.uiUnlock();
            return;

        }

      
        var ProductId = [];
      
        for (var i = 0; i < ProductsCollection.length; i++) {


            ProductId[i] = ProductsCollection[i]["ProductId"];
            
        }
       
        $.ajax({
            type: "POST",
            data: '{"Id":"' + Id + '", "Title": "' + Title + '","PhotoUrl1": "' + GroupPhotoUrl1 + '","PhotoUrl2": "' + GroupPhotoUrl2 + '","ShowOn": "' + ShowOn + '","IsActive": "' + IsActive + '","HasSubGroup": "' + HasSubgroup + '","ProductIdarr": "' + ProductId + '"}',

            url: "managemygroups.aspx/InsertGroupProducts",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);
                if (obj.Status == "0") {
                    alert("Updation Failed. Please try again later.");
                    return;
                }
                else {
                    alert("Products are linked to Group Successfully");
                    ResetControls();
                    BindGrid();

                   
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                $.uiUnlock();
            }

        });
    }





    $(document).ready(
    function () {



        BindGrid();

        $("#btnAdd").click(
        function () {

            m_Id = 0;

            if ($("#<%=ddlLink.ClientID%>").val() == "Categories") {
                InsertUpdate();
            }
            else if ($("#<%=ddlLink.ClientID%>").val() == "Products") {

                InsertProducts();
            }
            else if ($("#<%=ddlLink.ClientID%>").val() == "Brand") {

                InsertUpdate();
            }
        }
        );


        $("#btnUpdate").click(
        function () {

            if ($("#<%=ddlLink.ClientID%>").val() == "Categories") {
                InsertUpdate();
            }
            else if ($("#<%=ddlLink.ClientID%>").val() == "Products") {

                InsertProducts();
            }
            else if ($("#<%=ddlLink.ClientID%>").val() == "Brand") {

                InsertUpdate();
            }
        }
        );


        $("#btnAddAll").click(
        function () {

            $("#<%=ddlProduct.ClientID%> option").each(function () {


                $(this)

                var ProductId = $(this).val();

                var ProductName = $(this).text();
                if (ProductName != "") {
                    var item = $.grep(ProductsCollection, function (item) {
                        return item.ProductId == ProductId;
                    });

                    if (item.length) {

                        alert("This Product is added in the list");
                        return;

                    }


                    var tr = "<tr><td >" + ProductId + "</td><td >" + ProductName + "</td></td> <td><i id='dvClose'><img src='images/trash.png'/></i> </td></tr>";

                    $("#tbProducts").append(tr);


                    TO = new clsProduct();

                    TO.ProductId = ProductId;
                    TO.ProductName = ProductName;


                    ProductsCollection.push(TO);
                }

            });

        });


        $("#<%=txtTitle.ClientID %>").focus();
        $("#<%=txtTitle.ClientID %>").keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );



        $("#chkSubGroup").change(function () {

            if (this.checked) {
                $("#tblCategories").hide();
                $("#tblBrand").hide();
                $("#tblOr").hide();
                $("#tblProductsLink").hide();
                $("#tbProducts").hide();
                $("#dvlink").hide();
                $("#dvlinkPoduct").hide();
                $("#dvUrl").hide();
                $("#dvOr").hide();
            }
            else {

                $("#tblCategories").show();
                $("#tblBrand").show();
                $("#tblOr").show();
                $("#tblProductsLink").show();
                $("#tbProducts").show();
                $("#dvlinkPoduct").show();
                $("#dvlink").show();
                $("#dvUrl").show();
                $("#dvOr").show();
            }
        });


        $("#<%=ddlProduct.ClientID %>").change(
            function () {


                var ProductId = $("#<%=ddlProduct.ClientID %>").val();

                var ProductName = $("#<%=ddlProduct.ClientID %> option:selected").text();

                var item = $.grep(ProductsCollection, function (item) {
                    return item.ProductId == ProductId;
                });

                if (item.length) {

                    alert("This Product is added in the list");
                    return;

                }



                var tr = "<tr><td >" + ProductId + "</td><td >" + ProductName + "</td></td> <td><i id='dvClose'><img src='images/trash.png'/></i> </td></tr>";

                $("#tbProducts").append(tr);


                TO = new clsProduct();

                TO.ProductId = ProductId;
                TO.ProductName = ProductName;


                ProductsCollection.push(TO);



            }

            );



        $("#<%=ddlLink.ClientID %>").change(
                    function () {

                        var LInkTo = $("#<%=ddlLink.ClientID%>").val();
                        if (LInkTo == "Categories") {
                            $("#tblCategories").show();
                            $("#tblBrand").hide();
                            $("#tblOr").hide();
                            $("#tblProductsLink").hide();
                            $("#tbProducts").hide();
                            $("#dvlinkProduct").hide();
                            $("#dvlink").show();
                            $("#dvOr").hide();
                            $("#dvUrl").show();
                        }
                        else if (LInkTo == "Brand") {
                            $("#tblBrand").show();
                            $("#tblCategories").hide();
                            $("#tblOr").hide();
                            $("#tblProductsLink").hide();
                            $("#tbProducts").hide();
                            $("#dvlinkProduct").hide();
                            $("#dvlink").show();
                            $("#dvOr").hide();
                            $("#dvUrl").show();
                        }
                        else if (LInkTo == "Products") {

                            $("#tbProducts").show();
                            $("#tblProductsLink").show();
                            $("#tblCategories").hide();
                            $("#tblBrand").hide();
                            $("#tblOr").hide();
                            $("#dvlink").hide();
                            $("#dvOr").hide();
                            $("#dvlinkProduct").show();
                            $("#dvUrl").hide();
                        }

                    });


        $("#<%=ddlCategoryLevel3.ClientID %>").change(
            function () {


                $("#dvrepr").html("");


                var sid = $("#<%=ddlCategoryLevel3.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');
                $("#txtUrl").val('Peshawarisupermarket.com/list.aspx?sc=' + sid);

                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemygroups.aspx/BindCategoryLevel3",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);



                        $("#dvrepr").html(obj.ProductHtml);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                    }

                });


            }

            );




        $("#<%=ddlSubCategories.ClientID %>").change(
            function () {


                $("#dvrepr").html("");
                $("#<%=ddlCategoryLevel3.ClientID %>").html('');

                var sid = $("#<%=ddlSubCategories.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');
                $("#txtUrl").val('Peshawarisupermarket.com/list.aspx?s=' + sid);

                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemygroups.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlCategoryLevel3.ClientID %>").html(obj.SubCatOptions);
                        $("#dvrepr").html(obj.ProductHtml);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                    }

                });


            }

            );


        $("#<%=ddlBrand.ClientID %>").change(
            function () {

                var sid = $("#<%=ddlBrand.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $("#txtUrl").val('Peshawarisupermarket.com/list.aspx?b=' + sid);


            }

            );







        $("#<%=ddlCategory.ClientID %>").change(
            function () {

                $("#dvrepr").html("");


                $("#<%=ddlSubCategories.ClientID %>").html('');
                $("#<%=ddlCategoryLevel3.ClientID %>").html('');
                var sid = $("#<%=ddlCategory.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');

                $("#txtUrl").val('Peshawarisupermarket.com/list.aspx?c=' + sid);
                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemygroups.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlSubCategories.ClientID %>").html(obj.SubCatOptions);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                    }

                });


            }

            );






        $("#<%=ddlCategoryLevel3p.ClientID %>").change(
            function () {


                $("#dvrepr").html("");


                var sid = $("#<%=ddlCategoryLevel3p.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemygroups.aspx/BindCategoryLevel3",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);



                        $("#dvrepr").html(obj.ProductHtml);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                        BindProductsByCategory3($("#<%=ddlCategoryLevel3p.ClientID %>").val());

                    }

                });


            }

            );




        $("#<%=ddlSubCategoriesp.ClientID %>").change(
            function () {


                $("#dvrepr").html("");
                $("#<%=ddlCategoryLevel3p.ClientID %>").html('');

                var sid = $("#<%=ddlSubCategoriesp.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemygroups.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlCategoryLevel3p.ClientID %>").html(obj.SubCatOptions);
                        $("#dvrepr").html(obj.ProductHtml);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();
                        BindProductsBySubcategoryId($("#<%=ddlSubCategoriesp.ClientID %>").val());
                    }

                });


            }

            );




        $("#<%=ddlCategoryp.ClientID %>").change(
            function () {

                $("#dvrepr").html("");


                $("#<%=ddlSubCategoriesp.ClientID %>").html('');
                $("#<%=ddlCategoryLevel3p.ClientID %>").html('');
                var sid = $("#<%=ddlCategoryp.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemygroups.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlSubCategoriesp.ClientID %>").html(obj.SubCatOptions);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {

                        $.uiUnlock();

                        BindProductsByCategory1($("#<%=ddlCategoryp.ClientID %>").val());
                    }


                });


            }

            );




        function BindProductsByCategory1(cid) {
            $("#<%=ddlProduct.ClientID %>").html('');
            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

            $.ajax({
                type: "POST",
                data: '{ "CategoryId": "' + cid + '"}',
                url: "managemygroups.aspx/FillProductsByCategoryLevel1",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);

                    $("#<%=ddlProduct.ClientID %>").append(obj.ProductOptions);

                },

                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {


                    $.uiUnlock();
                }

            });

        }

        function BindProductsBySubcategoryId(sid) {
            $("#<%=ddlProduct.ClientID %>").html('');
            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

            $.ajax({
                type: "POST",
                data: '{ "SubCategoryId": "' + sid + '"}',
                url: "managemygroups.aspx/FillProductsBySubcategory",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);

                    $("#<%=ddlProduct.ClientID %>").append(obj.ProductOptions);

                },

                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {


                    $.uiUnlock();
                }

            });

        }


        function BindProductsByCategory3(sid) {
            $("#<%=ddlProduct.ClientID %>").html('');
            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

            $.ajax({
                type: "POST",
                data: '{ "SubCategoryId": "' + sid + '"}',
                url: "managemygroups.aspx/FillProductsByCategoryLevel3",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);

                    $("#<%=ddlProduct.ClientID %>").append(obj.ProductOptions);

                },

                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {


                    $.uiUnlock();
                }

            });

        }



    }
    );

</script>
<form runat="server" id="formID" method="post">
    <asp:HiddenField ID="hdnCategory" runat="server" />
    
    <div id="content">
        <div id="rightnow">
            <h3 class="reallynow">
                <span>Add/Update MyGroup</span>
                <br />
            </h3>
            <div class="youhave">
                <table cellpadding="0" cellspacing="0" border="0" class="top_table">
                    <tr>
                        <td class="Titles">
                            Title:
                        </td>
                        <td>
                            <input type="text" name="txtTitle" runat="server" class="inputtxt validate required"
                                data-index="1" id="txtTitle" />
                        </td>
                    </tr>
                 
                    <tr>
                        <td class="Titles">
                            Is Active:
                        </td>
                        <td>
                            <input type="checkbox" id="chkIsActive" checked="checked" data-index="2" name="chkIsActive" />
                        </td>
                    </tr>
                  
                  <tr>
                        <td class="Titles">
                            PhotoUrl 1:
                        </td>
                        <td>
                     
                            <input type="file" id="FileUpload1" runat="server"/>
                        </td>                         
                    </tr>
                    <tr>
                        <td class="Titles">
                            PhotoUrl 2:
                        </td>
                        <td>
                            <input type="file" id="FileUpload2" runat="server"/>
                        </td>
                    </tr>
                     <tr>
                        <td class="Titles">
                            Mobile Image:
                        </td>
                        <td>
                     
                             <input type="file" id="FileUpload3" />
                        </td>
                        </tr>
                   <tr>
                   <td class="Titles">
                            Show On:
                        </td>
                        <td>
                        <asp:DropDownList ID="ddlShowon" runat="server">
                                          <asp:ListItem Text="Home" Value="HomeTop"></asp:ListItem>
                                          <asp:ListItem Text="Quick Shopping Guide" Value="HomeGuide"></asp:ListItem>
                                          <asp:ListItem Text="Side Panel" Value="HomeRight"></asp:ListItem>
                                          <asp:ListItem Text="Inner Pages" Value="InnerPages"></asp:ListItem>
                                           <asp:ListItem Text="Bottom Adds" Value="Bottom"></asp:ListItem>
                                     </asp:DropDownList>
                        </td>
                   </tr>
                    <tr>
                   <td class="Titles">
                            Link To:
                        </td>
                        <td>
                        <asp:DropDownList ID="ddlLink" runat="server">
                                          <asp:ListItem Text="Categories" Value="Categories"></asp:ListItem>
                                          <asp:ListItem Text="Brand" Value="Brand"></asp:ListItem>
                                          <asp:ListItem Text="Products" Value="Products"></asp:ListItem>
                                         
                                     </asp:DropDownList>
                        </td>
                   </tr>
                    <tr>
                        <td class="Titles">
                            HasSubGroup:
                        </td>
                        <td>
                            <input type="checkbox" id="chkSubGroup" checked="checked" data-index="2" name="chkSubGroup" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>

          <div id="rightnow">
                    <h3 class="reallynow">
                        <span> Linking </span>
                      
                        <br />
                    </h3>
				    <div id="dvlink" class="youhave">
                    
                     <table id="tblCategories" class="top_table">
                    <tr> <td class="Titles">
                                  <label id="srclvl1" name="srclvl1">Source Level 1:</label></td>
                                 <td>
                                     <asp:DropDownList ID="ddlCategory" runat="server">
                                          <asp:ListItem Text="--Category Level 1--" Value="0"></asp:ListItem>
                                     </asp:DropDownList>                                                                  
                                </td>
                                    </tr>
                                    <tr>
                                 <td class="Titles">
                                     <label id="srclvl2" name="srclvl2">Source Level 2:</label></td>
                                 <td>
                                       <asp:DropDownList ID="ddlSubCategories" runat="server" 
                                           >
                                           <asp:ListItem Text="--Category Level 2--" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                     
                                                   </td>

                             </tr>
                              <tr id="catLevel3" runat="server">
                                 <td class="Titles">
                                   <label id="srclvl3" name="srclvl3">Source Level 3:</label></td>
                                 <td>
                                       <asp:DropDownList ID="ddlCategoryLevel3" runat="server" 
                                           
                                           >
                                             <asp:ListItem Text="--Category Level 3--" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                                 
                                                 
                                                   </td>
                             </tr>
                          </table>
                           <table id="tblOr">  <tr> <td colspan = "3" align=center><label id="lblor" name="or">OR:</label></td> </tr>

                           </table>
                             <table id = "tblBrand" class="top_table"> 
                                 <tr id="Brand" runat="server">
                                 <td style="text-align:right;" class="Titles">
                                 <label id="Chosbrnd" name="Chosbrnd">Choose Brand:</label></td>
                                 <td>
                                       <asp:DropDownList ID="ddlBrand" runat="server" 
                                           
                                           >
                                             <asp:ListItem Text="--Product Brand--" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                                 
                                                 
                                                   </td>

                             </tr>
                             </table>

                           
                          

                    </div>
			  </div>

               <div id="rightnow">
                    <h3 class="reallynow">
                        <span> URL: </span>
                      
                        <br />
                    </h3>
				    <div id="dvUrl" class="youhave">
                     <table class="top_table"> 
                         <tr id="Tr1" runat="server">
                                 <td class="Titles">
                                 LinkUrl:</td>
                                 <td colspan = "2">
                                      <input type="text" name="txtUrl" class="inputtxt validate required"  data-index="1" id="txtUrl" />
                                                   </td>
                                 
                             </tr>
                          </table>  
                    </div>
                    </div>


                 <div id="dvOr">
                    <h3 class="reallynow">
                        <span> OR </span>
                      
                        <br />
                    </h3>
				
			  </div>

               
            <div id="rightnow">
                    <h3 class="reallynow">
                        <span>Direct Product Linking </span>
                      
                        <br />
                    </h3>
				    <div id="dvlinkProduct" class="youhave">
                    
                     <table id= "tblProductsLink" class="top_table">
                    <tr> <td class="Titles">
                                     Source Level 1:</td>
                                 <td>
                                     <asp:DropDownList ID="ddlCategoryp" runat="server">
                                          <asp:ListItem Text="--Category Level 1--" Value="0"></asp:ListItem>
                                     </asp:DropDownList>
                                                                       
                                     
                                      
                                     </td>
                            </tr>
                                    <tr>
                                 <td class="Titles">
                                      Source Level 2:</td>
                                 <td>
                                       <asp:DropDownList ID="ddlSubCategoriesp" runat="server" 
                                      >
                                           <asp:ListItem Text="--Category Level 2--" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                     
                                                   </td>
                             </tr>
                              <tr id="Tr2" runat="server">
                                 <td class="Titles">
                                    Source Level 3:</td>
                                 <td>
                                       <asp:DropDownList ID="ddlCategoryLevel3p" runat="server" 
                                           
                                       >
                                             <asp:ListItem Text="--Category Level 3--" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                                 
                                                 
                                                   </td>
                             </tr>
                           
                              <tr runat="server">
                                 <td class="Titles">
                                 Choose Product:</td>
                                 <td>
                                       <asp:DropDownList ID="ddlProduct" runat="server" 
                                           
                                           >
                                             <asp:ListItem Text="--Choose Product--" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                                 
                                                 
                                                   </td>
                             </tr>
                             
                    </table>
                        <div id="btnAddAll" class="btn btn-primary btn-small category_table">
                                            Add All Products</div>

                       <table class="tablesorter item_table" id="tbProducts" cellspacing="0" style="    margin-top: 30px;">
                            <thead>
                                <tr>
                                    <th>
                                        ProductId
                                    </th>

                                    <th>
                                        ProductName
                                    </th>
                                    <th> Delete
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="100%" align="center"></td>
                                </tr>

                            </tbody>
                        </table>

                        <table class="category_table">
                            <tr>
                                <td colspan="100%">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <div id="btnAdd" class="btn btn-primary btn-small">
                                                    Add MyGroup</div>
                                            </td>
                                            <td>
                                                <div id="btnUpdate" class="btn btn-primary btn-small" style="display: none;">
                                                    Update MyGroup</div>
                                            </td>
                                            <td>
                                                <div id="btnReset" class="btn btn-primary btn-small" style="display: none; width: 60px;">
                                                    Cancel</div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
</table>

                    </div>
			  </div>

              <div>
              
              
              
              </div>
        <div id="rightnow" style="margin-top: 10px">
            <h3 class="reallynow">
                <span>Manage MyGroups</span>
                <br />
            </h3>
            <div class="youhave" style="padding-left: 30px">
            <table>
            <tr>
            <td valign="top">
              <table id="jQGridDemo">
                </table>
                <div id="jQGridDemoPager">
                </div>
            </td>

           </tr>

            
            </table>
            

              
            </div>
        </div>
    </div>
    </form>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManagMyGroups.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Id', 'Title','ShowOn','LinkUrl','PhotoUrl1','PhotoUrl2', 'IsActive','HasSubGroup','MobileImage'],
                        colModel: [
                                    { name: 'Id', key: true, index: 'Id', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'Title', index: 'Title', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'ShowOn', index: 'ShowOn', width: 200, stype: 'text', sortable: true, editable: true,hidden: true, editrules: { required: true } },
                                     { name: 'LinkUrl', index: 'LinkUrl', width: 200, stype: 'text', sortable: true, editable: true,hidden: true, editrules: { required: true } },
                                     { name: 'PhotoUrl1', index: 'PhotoUrl1', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } ,hidden:true},
                                      { name: 'PhotoUrl2', index: 'PhotoUrl2', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true },hidden:true },
                                    { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                    { name: 'HasSubGroup', index: 'HasSubGroup', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                     { name: 'MobileImgUrl', index: 'MobileImgUrl', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } ,hidden:true},

                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Id',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "MyGroups List",

                        editurl: 'handlers/ManagMyGroups.ashx',



                    });




                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_Id = 0;
                    validateForm("detach");
                    var txtTitle = $("#<%=txtTitle.ClientID %>");
                    m_Id = $('#jQGridDemo').jqGrid('getCell', rowid, 'Id');


                    if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                        $('#chkIsActive').prop('checked', true);
                    }
                    else {
                        $('#chkIsActive').prop('checked', false);

                    }
                     if ($('#jQGridDemo').jqGrid('getCell', rowid, 'HasSubGroup') == "true") {
                        $('#chkSubGroup').prop('checked', true);
                    }
                    else {
                        $('#chkSubGroup').prop('checked', false);

                    }

                    $("#<%=txtTitle.ClientID %>").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Title'));
                    $("#<%=ddlShowon.ClientID%>").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ShowOn'));
                    $("#txtUrl").val($('#jQGridDemo').jqGrid('getCell', rowid, 'LinkUrl'));
                    GroupPhotoUrl1=$('#jQGridDemo').jqGrid('getCell', rowid, 'PhotoUrl1');
                    GroupPhotoUrl2=$('#jQGridDemo').jqGrid('getCell', rowid, 'PhotoUrl2');
                    MobileImageUrl=$('#jQGridDemo').jqGrid('getCell', rowid, 'MobileImgUrl');

                    $("#<%=txtTitle.ClientID %>").focus();
                    $("#btnAdd").css({ "display": "none" });
                    $("#btnUpdate").css({ "display": "block" });
                    $("#btnReset").css({ "display": "block" });
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>
</asp:Content>

