﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="manageemployees.aspx.cs" Inherits="backoffice_manageemployees" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">

        <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>

        <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
        <script src="js/grid.locale-en.js" type="text/javascript"></script>
        <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
        <link href="css/custom-css/master.css" rel="stylesheet" />

        <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
        <script src="js/jquery-ui.js" type="text/javascript"></script>

        <script type="text/javascript" src="js/jquery.uilock.js"></script>

        <style type="text/css">
            .ui-jqgrid .ui-userdata {
                height: auto;
            }
            
            .ui-jqgrid .ui-userdata div {
                margin: .1em .5em .2em;
            }
            
            .ui-jqgrid .ui-userdata div>* {
                vertical-align: middle;
            }
        </style>

        <script language="javascript" type="text/javascript">
            $(document).ready(
                function() {

                    $('#txtEmpName').focus();

                    var DataGrid = jQuery('#jQGridDemo');
                    DataGrid.jqGrid('setGridWidth', '700');
                    //        DataGrid.jqGrid('setGridHeight', '236');

                    $("#jQGridDemo").jqGrid('setGridParam', {
                        onSelectRow: function(rowid, iRow, iCol, e) {

                            validateForm("detach");
                            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

                            $("#hdnEmp_Id").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Emp_Id'));

                            var Prefix = $('#jQGridDemo').jqGrid('getCell', rowid, 'Prefix');
                            $("#ddlPrefix option").removeAttr("selected");
                            $('#ddlPrefix option[value=' + Prefix + ']').prop('selected', 'selected');

                            $("#txtEmpName").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Emp_Name'));

                            $("#txtcontact").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ContactNo'));
                            $("#txtaddress").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Address'));
                            $("#txtUserName").val($('#jQGridDemo').jqGrid('getCell', rowid, 'UserName'));

                            $("#txtEmail").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Email'));
                            $("#txtPassword").val("****************").prop("disabled", true);

                            if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                                $('#chkIsActive').prop('checked', true);
                            } else {
                                $('#chkIsActive').prop('checked', false);

                            }

                            $.uiUnlock();

                        }

                    });

                    function ResetForm() {
                        // $('input:not([id="hdnServiceId"])').val('');
                        // $("#txtServiceId").removeAttr('readonly');
                        $(':input[type=text]').val('');
                        $("#txtPassword").prop("disabled", false).val("");
                        $("#txtUserName").prop("readonly", false);
                        $("#txtaddress").val("");

                        $('#chkIsActive').removeAttr('checked');
                        $("#hdnEmp_Id").val("0");

                        validateForm("detach");

                    }
                    //----------------------btnADD------------------------

                    $("#btnAdd").click(
                        function() {
                            ResetForm();
                            var divP = document.getElementById("view1");

                            document.getElementById("ancV1").style.borderBottom = "solid 1px white";
                            document.getElementById("ancV1").style.backgroundColor = "white";

                            divP.style.display = "block";

                            var IsFirstTime = $("#hdnFirstTime").val();

                            if (IsFirstTime == "1") {

                                // BindContols();

                                $("#hdnFirstTime").val("0");
                            }

                            $('#addDialog').dialog({
                                autoOpen: false,

                                width: 440,
                                resizable: false,
                                modal: false,
                                buttons: {

                                    "Save": function() {

                                        InsertUpdate();

                                    },
                                    "Cancel": function() {
                                        //                

                                        $(this).dialog("close");

                                    }
                                }
                            });

                            //change the title of the dialgo
                            linkObj = $(this);
                            var dialogDiv = $('#addDialog');
                            dialogDiv.dialog("option", "position", [240, 126]);
                            var viewUrl = "customeradd.aspx";
                            dialogDiv.dialog('open');
                            return false;

                        }
                    );

                    function InsertUpdate() {

                        if (!validateForm("addDialog")) {

                            return;
                        }

                        $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

                        var id = $("#hdnEmp_Id").val();
                        var prefix = $("#ddlPrefix").val();
                        var empName = $("#txtEmpName").val();
                        var address = $("#txtaddress").val();
                        var contactno = $("#txtcontact").val();
                        var email = $("#txtEmail").val();

                        var IsActive = false;
                        if ($('#chkIsActive').is(":checked")) {
                            IsActive = true;
                        }

                        var username = $("#txtUserName").val();
                        var Password = $("#txtPassword").val();

                        $.ajax({
                            type: "POST",
                            data: '{ "Id": "' + id + '","Prefix": "' + prefix + '", "EmpName": "' + empName + '","Address": "' + address + '","ContactNo":"' + contactno + '","Email": "' + email + '","IsActive": "' + IsActive + '","UserName": "' + username + '","Password": "' + Password + '"}',

                            url: "manageemployees.aspx/InsertUpdate",
                            contentType: "application/json",
                            dataType: "json",
                            success: function(msg) {

                                var obj = jQuery.parseJSON(msg.d);
                                if (obj.Status == "0") {
                                    alert("Updation Failed. Please try again later.");
                                    return;
                                }

                                if (obj.Status == "-1") {
                                    alert("Updation Failed. User Name already Exists.");
                                    return;
                                }

                                if (id == 0) {
                                    jQuery("#jQGridDemo").jqGrid('addRowData', obj.Users.Emp_Id, obj.Users, "last");
                                    $('#jQGridDemo').trigger('reloadGrid');
                                    alert("Employees Added Successfully");
                                } else {

                                    var myGrid = $("#jQGridDemo");
                                    var selRowId = myGrid.jqGrid('getGridParam', 'selrow');
                                    myGrid.jqGrid('setRowData', selRowId, obj.Users);

                                    alert("Employees Updated Successfully");
                                }

                                ResetForm();

                                var dialogDiv = $('#addDialog');
                                dialogDiv.dialog('close');

                            },
                            error: function(xhr, ajaxOptions, thrownError) {

                                var obj = jQuery.parseJSON(xhr.responseText);
                                alert(obj.Message);
                            },
                            complete: function() {

                                $.uiUnlock();
                            }

                        });
                    }

                    //--------------------------btn Edit------------------------

                    $("#btnEdit").click(
                        function() {

                            $("#hdntabloadId").val("0");
                            document.getElementById('txtUserName').readOnly = true;

                            var divP = document.getElementById("view1");
                            var divD = document.getElementById("view2");
                            var divJ = document.getElementById("view3");

                            document.getElementById("ancV1").style.borderBottom = "solid 1px white";
                            document.getElementById("ancV1").style.backgroundColor = "white";

                            divP.style.display = "block";

                            $("#lblMsg").html("");

                            var mid = $("#hdnEmp_Id").val();

                            if (mid == "0") {
                                alert("No Row Selected");
                                return;
                            }

                            $('#addDialog').dialog({
                                autoOpen: false,

                                width: 600,
                                resizable: false,
                                modal: false,
                                buttons: {

                                    "Update": function() {
                                        InsertUpdate();

                                    },

                                    "Cancel": function() {
                                        $(this).dialog("close");

                                    }
                                }
                            });

                            //change the title of the dialgo
                            linkObj = $(this);
                            var dialogDiv = $('#addDialog');
                            dialogDiv.dialog("option", "position", [240, 50]);
                            var viewUrl = "customeradd.aspx";
                            dialogDiv.dialog('open');
                            return false;
                        }
                    );

                }
            );
        </script>

        <form runat="server" id="formID" method="post">

            <asp:HiddenField ID="hdnDate" runat="server" />

            <input type="hidden" id="hdnEmp_Id" value="0" />
            <input type="hidden" id="hdnFirstTime" value="1" />
            <input type="hidden" id="hdntabloadId" value="0" />

            <asp:ScriptManager ID="scrpt1" runat="server"></asp:ScriptManager>
            <div id="addDialog" style="display:none" title="Add Employees">
                <ul class="tabs" data-persist="true" style="display:none">
                    <li>
                        <a id="ancV1" style="display:none;"></a>
                    </li>

                </ul>

                <div class="tabcontents">
                    <div id="view1">
                        <table cellpadding="3" cellspacing="3">
                            <tr>
                                <td style="width:84px" class="headings">Name:</td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <select id="ddlPrefix" style="width:60px;height:22px">
                                                    <option value="Mr">Mr</option>
                                                    <option value="Mrs">Mrs</option>
                                                    <option value="Miss">Miss</option>
                                                </select>
                                            </td>
                                            <td>
                                                <%--<input type="hidden" id="hdnSID" value="0"/>--%>
                                                    <input type="text" id="txtEmpName" style="width:150px" data-index="1" class="form-control input-small validate required alphanumeric" />

                                            </td>
                                            <td>
                                                <div id="lblMsg" style="color:Red;padding-left:5px"></div>
                                                <input type="hidden" id="hdnStatus" value="0" />

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                            <td class="headings">User Name:</td>
                            <td>
                                <input type="text" id="txtUserName" data-index="3" style="width:221px" class="form-control input-small validate required" />
                            </td>

                            </tr>
                            <tr>
                                <td class="headings">Password:</td>
                                <td>
                                    <input type="password" id="txtPassword" data-index="4" style="width:221px" class="form-control input-small validate required" />
                                </td>

                            </tr>

                            <tr>

                                <td class="headings">Address:</td>
                                <td>
                                    <textarea id="txtaddress" rows="" cols="" style="width:224px" class="form-control input-small validate required"></textarea>
                                </td>
                            </tr>

                            <tr>

                                <td class="headings">ContactNo:</td>
                                <td>
                                    <input type="text" id="txtcontact" style="width:221px" class="form-control input-small validate required" />

                                </td>
                            </tr>

                            <tr>
                                <td class="headings">Email:</td>
                                <td style="width:106px;">
                                    <input type="text" id="txtEmail" style="width:221px" class="form-control input-small validate required" />
                                </td>
                            </tr>

                            <tr>
                                <td class="headings">Is Active:</td>
                                <td style="width:20px;">
                                    <input type="checkbox" id="chkIsActive" />
                                </td>
                            </tr>

                        </table>
                    </div>

                </div>
            </div>

            <div id="content">

                <div id="rightnow">
                    <h3 class="reallynow">
                <span>Employees List</span>
                <br />
            </h3>
                    <div class="youhave">

                        <table id="jQGridDemo">
                        </table>
                        <div id="jQGridDemoPager">
                        </div>

                        <table class="category_table" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <div id="btnAdd" class="btn btn-primary btn-small">New</div>
                                </td>
                                <td>
                                    <div id="btnEdit" class="btn btn-primary btn-small">Edit</div>
                                </td>

                            </tr>
                        </table>
                    </div>
                </div>
                <div id="dvPopup" style="display:none;">

                    <h3 class="reallynow">
                <span>Bill Detail</span>
                <br />
            </h3>
                    <table>
                        <tr>
                            <td>
                                <label>BillNo</label>
                            </td>
                            <td>
                                <input type="text" readonly="readonly" class="form-control input-small" style="width:120px;background-color:White" id="txtBillNo" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Cancelation Remarks *</label>
                            </td>
                            <td>
                                <textarea type="text" class="form-control input-small validate required " id="txtRemarks" style="width:150px"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input type="checkbox" name="CheckBox" value="Order" id="rdbOrder">Cancel Order</label>
                            </td>
                        </tr>
                        <tr>
                            <td>

                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div id="btnCancelConfirm" class="btn btn-primary btn-small">Cancel</div>
                                        </td>

                                    </tr>
                                </table>
                            </td>

                        </tr>

                    </table>
                </div>
            </div>

        </form>

        <script type="text/javascript">
            jQuery("#jQGridDemo").jqGrid({
                url: 'handlers/manageemployees.ashx/',
                ajaxGridOptions: {
                    contentType: "application/json"
                },
                datatype: "json",

                colNames: ['Employee ID', 'Emp Name', 'User Name', 'Prefix', 'Address', 'ContactNo', 'Email', 'Active', 'Password'],
                colModel: [

                    {
                        name: 'Emp_Id',
                        key: true,
                        index: 'Emp_Id',
                        width: 100,
                        stype: 'text',
                        sorttype: 'int',
                        hidden: false
                    }, {
                        name: 'Emp_Name',
                        index: 'Emp_Name',
                        width: 150,
                        stype: 'text',
                        sorttype: 'int',
                        hidden: false
                    }, {
                        name: 'UserName',
                        index: 'UserName',
                        width: 100,
                        stype: 'text',
                        sorttype: 'int',
                        hidden: true
                    }, {
                        name: 'Prefix',
                        index: 'Prefix',
                        width: 100,
                        stype: 'text',
                        sorttype: 'int',
                        hidden: true
                    }, {
                        name: 'Address',
                        index: 'Address',
                        width: 100,
                        stype: 'text',
                        sorttype: 'int',
                        hidden: true
                    }, {
                        name: 'ContactNo',
                        index: 'ContactNo',
                        width: 100,
                        stype: 'text',
                        sorttype: 'int',
                        hidden: true
                    }, {
                        name: 'Email',
                        index: 'Email',
                        width: 100,
                        stype: 'text',
                        sorttype: 'int',
                        hidden: true
                    }, {
                        name: 'IsActive',
                        index: 'IsActive',
                        width: 150,
                        editable: true,
                        hidden: false,
                        edittype: "checkbox",
                        editoptions: {
                            value: "true:false"
                        },
                        formatter: "checkbox",
                        formatoptions: {
                            disabled: true
                        }
                    }, {
                        name: 'Password',
                        index: 'Password',
                        width: 100,
                        stype: 'text',
                        sorttype: 'int',
                        hidden: true
                    },

                ],
                rowNum: 10,
                mtype: 'GET',
                loadonce: true,
                toppager: true,
                rowList: [10, 20, 30],
                pager: '#jQGridDemoPager',
                sortname: 'Emp_Id',
                viewrecords: true,
                height: "100%",
                width: "600px",
                sortorder: 'desc',
                caption: "User List",
                editurl: 'handlers/ManageEmployees.ashx',

                ignoreCase: true,
                toolbar: [true, "top"],

            });
            var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function(e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: {
                    primary: "ui-icon-search"
                },
                text: false
            }).click(function() {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", {
                    search: true
                });
                $grid.trigger("reloadGrid", [{
                    page: 1,
                    current: true
                }]);
                return false;
            });

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager', {
                    refresh: false,
                    edit: false,
                    add: false,
                    del: false,
                    search: true,
                    searchtext: "Search",
                    addtext: "Add",
                },

                { //SEARCH
                    closeOnEscape: true

                }

            );
        </script>
    </asp:Content>