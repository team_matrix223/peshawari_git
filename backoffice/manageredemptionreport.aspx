﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="manageredemptionreport.aspx.cs" Inherits="backoffice_manageredemptionreport" %>
    <%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
        <asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
            <form runat="server" id="frmPurchase">
                <div id="content" style="padding:6px">

                    <div id="rightnow" style="margin-top: 10px">
                        <h3 class="reallynow">
                <span> Redemption Point Used By User on Daily Basis</span>
                <br />
            </h3>
                        <table>
                            <tr style="text-align:center">
                                <td style="font-weight:bold">Choose Month:</td>
                                <td>
                                    <asp:DropDownList ID="ddlmonth" runat="server" AutoPostBack="True">
                                        <asp:ListItem Value="1">January</asp:ListItem>
                                        <asp:ListItem Value="2">February</asp:ListItem>
                                        <asp:ListItem Value="3">March</asp:ListItem>
                                        <asp:ListItem Value="4">April</asp:ListItem>
                                        <asp:ListItem Value="5">May</asp:ListItem>
                                        <asp:ListItem Value="6">June</asp:ListItem>
                                        <asp:ListItem Value="7">July</asp:ListItem>
                                        <asp:ListItem Value="8">August</asp:ListItem>
                                        <asp:ListItem Value="9">September</asp:ListItem>
                                        <asp:ListItem Value="10">October</asp:ListItem>
                                        <asp:ListItem Value="11">November</asp:ListItem>
                                        <asp:ListItem Value="12">December</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="font-weight:bold">Choose User</td>
                                <td>
                                    <asp:DropDownList ID="ddluser" runat="server" AutoPostBack="True"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>

                        <div>

                            <asp:Chart ID="Chart2" runat="server" Height="303px" Width="737px" Palette="SeaGreen" DataSourceID="SqlDataSource1">

                                <series>
                                    <asp:Series Name="Series1" XValueMember="Date" YValueMembers="Data">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Day" Interval="1"></AxisX>
                                        <AxisY Title="NO Of Customer"></AxisY>
                                    </asp:ChartArea>
                                </chartareas>

                            </asp:Chart>

                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="shopping_sp_GraphicalDailyRedemptionPoints" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddlmonth" DefaultValue="1" Name="Month" PropertyName="SelectedValue" Type="String" />
                                    <asp:ControlParameter ControlID="ddluser" DefaultValue="1" Name="UserId" PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                        </div>
                    </div>

                    <div id="rightnow" style="margin-top: 10px">
                        <h3 class="reallynow">
                <span>  Redemption Point Used By User on Weekly Basis</span>
                <br />
            </h3>

                        <div>

                            <asp:Chart ID="Chart3" runat="server" Height="303px" Width="737px" Palette="EarthTones" DataSourceID="SqlDataSource2">

                                <series>
                                    <asp:Series Name="Series1" XValueMember="Series" YValueMembers="Val" ChartType="StackedColumn">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Week" Interval="1"></AxisX>
                                        <AxisY Title="No Of Customers"></AxisY>
                                    </asp:ChartArea>
                                </chartareas>

                            </asp:Chart>

                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="shopping_sp_GraphicalWeeklyRedemptionPoints" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddlmonth" DefaultValue="1" Name="Month" PropertyName="SelectedValue" Type="String" />
                                    <asp:ControlParameter ControlID="ddluser" DefaultValue="1" Name="UserId" PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                        </div>
                    </div>

                    <div id="rightnow" style="margin-top: 10px">
                        <h3 class="reallynow">
                <span>  Redemption Point Used By User on Monthly Basis</span>
                <br />
            </h3>
                        <div>
                            <asp:Chart ID="Chart1" runat="server" Height="303px" Width="737px" DataSourceID="SqlDataSource5">

                                <series>
                                    <asp:Series Name="Series1" XValueMember="mnth" YValueMembers="Data" ChartType="StackedColumn">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Month" Interval="1"></AxisX>
                                        <AxisY Title="No Of Customers"></AxisY>
                                    </asp:ChartArea>
                                </chartareas>

                            </asp:Chart>

                            <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="shopping_sp_GraphicalMonthlyRedemptionPoints" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddluser" DefaultValue="1" Name="UserId" PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="shopping_sp_GraphicalYearlyRedemptionPoints" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddluser" DefaultValue="1" Name="UserId" PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                        </div>
                    </div>

                    <div id="rightnow" style="margin-top: 10px">
                        <h3 class="reallynow">
                <span>  Redemption Point Used By User on Yearly Basis</span>
                <br />
            </h3>
                        <div>
                            <asp:Chart ID="Chart4" runat="server" Height="303px" Width="737px" Palette="Excel" DataSourceID="SqlDataSource4">

                                <series>
                                    <asp:Series Name="Series1" XValueMember="Year" YValueMembers="Value" ChartType="StackedColumn">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Year" Interval="1"></AxisX>
                                        <AxisY Title="No Of Customers"></AxisY>
                                    </asp:ChartArea>
                                </chartareas>

                            </asp:Chart>

                            <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="shopping_sp_GraphicalYearlyRedemptionPoints" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddluser" DefaultValue="1" Name="UserId" PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                        </div>
                    </div>

                </div>
            </form>
        </asp:Content>