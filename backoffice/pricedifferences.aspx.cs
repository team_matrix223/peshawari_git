﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;

public partial class backoffice_pricedifferences : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if (!User.IsInRole("PriceDifferences"))
            {
                Response.Redirect("default.aspx");

            }
        }
    }
    [WebMethod]
    public static string Update(string ProductIds)
    {
        string[] ProductData = ProductIds.Split(',');
        DataTable dt = new DataTable();
        dt.Columns.Add("VariationId");

        for (int i = 0; i < ProductData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["VariationId"] = Convert.ToInt32(ProductData[i]); ;


            dt.Rows.Add(dr);

        }

        int status = new ProductsBLL().UpdatePrice(dt);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            Status = status
        };
        return ser.Serialize(JsonData);
    }
}