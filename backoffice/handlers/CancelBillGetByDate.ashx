﻿<%@ WebHandler Language="C#" Class="CancelBillGetByDate" %>

using System;
using System.Web;

public class CancelBillGetByDate : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {

        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;


        string dateFrom = context.Request.QueryString["dateFrom"];
        string dateTo = context.Request.QueryString["dateTo"];



        //oper = null which means its first load.
        var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        var xss = jsonSerializer.Serialize(
         new BillCancelBLL().CancelBillGetByDate(Convert.ToDateTime(dateFrom), Convert.ToDateTime(dateTo))
           );

        context.Response.Write(xss);





    }




    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}