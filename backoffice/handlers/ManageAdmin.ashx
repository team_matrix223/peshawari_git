﻿<%@ WebHandler Language="C#" Class="ManageAdmin" %>

using System;
using System.Web;


public class ManageAdmin : IHttpHandler
{
    
    public void ProcessRequest (HttpContext context) {

        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
         string strResponse = string.Empty;

        if (strOperation == null)
        {
            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
           
            var xss = jsonSerializer.Serialize(
             new AdminBLL().GetAll() 
               );   

            context.Response.Write(xss);
        }
        
        
                    
        
    }
 
          
     
   
    public bool IsReusable {
        get {
            return false;
        }
    }

}