﻿<%@ WebHandler Language="C#" Class="ManageEmployees" %>

using System;
using System.Web;

public class ManageEmployees : IHttpHandler {
    
    public void ProcessRequest (HttpContext context)
    {
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;

        if (strOperation == null)
        {
            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            jsonSerializer.MaxJsonLength = int.MaxValue;
            var x = jsonSerializer.Serialize(

                new UsersBLL().GetAll());

            context.Response.Write(x);
        }
        else if (strOperation == "del")
        {


        }
        else if (strOperation == "edit")
        {

        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}