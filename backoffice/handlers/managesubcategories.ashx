﻿<%@ WebHandler Language="C#" Class="managesubcategories" %>

using System;
using System.Web;

public class managesubcategories : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;

        if (strOperation == null)
        {
            string CatId = context.Request.QueryString["CatId"];

            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var xss = jsonSerializer.Serialize(
            new CategoryBLL().GetByParentId(Convert.ToInt32 (CatId))
            //  new SubCategoryBLL().GetAll()
               );

            context.Response.Write(xss);
        }

        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}