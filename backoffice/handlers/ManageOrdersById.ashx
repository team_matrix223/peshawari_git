﻿<%@ WebHandler Language="C#" Class="ManageOrdersById" %>

using System;
using System.Web;

public class ManageOrdersById : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;


        string OrderId = context.Request.QueryString["OrderId"];



        //oper = null which means its first load.
        var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        var xss = jsonSerializer.Serialize(
         new OrderBLL().GetOrderbyOrderId(Convert.ToInt32(OrderId))
           );

        context.Response.Write(xss);

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}