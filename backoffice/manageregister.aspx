﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="manageregister.aspx.cs" Inherits="backoffice_manageregister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
      <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnId" runat="server"/>
<div id="content" style="padding:6px">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Registration Form</span>
                     
                        <br />
                    </h3>
				   <div class="youhave" style="padding-left:30px">
                   <table cellpadding="0" cellspacing="0" border="0" id="frmReg">
                     
                   
                     <tr><td class="headings">FirstName:</td><td style="width: 223px">  <input type="text"  name="txtFirstName" class="inputtxt validate required alphanumeric"   data-index="1" id="txtFirstName" style="width: 213px" runat="server"/></td><td>
                         <asp:RequiredFieldValidator ID="req_FirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                         </td></tr>
                          <tr><td class="headings">LastName:</td><td style="width: 223px">  <input type="text"  name="txtLastName" class="inputtxt validate required alphanumeric"   data-index="1" id="txtLastName" style="width: 213px" runat="server"/></td><td></td></tr>
                              <tr><td class="headings">EmailId:</td><td style="width: 223px">  <input type="text"  name="txtEmailId" class="inputtxt validate required alphanumeric"   data-index="1" id="txtEmailId" style="width: 213px" runat="server"/></td><td>
                                  <asp:RequiredFieldValidator ID="req_Email" runat="server" ControlToValidate="txtEmailId" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                  <asp:RegularExpressionValidator ID="RegExp_Email" runat="server" ControlToValidate="txtEmailId" ErrorMessage="Enter Valid EmailID" Font-Bold="False" ForeColor="#CC0000" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                  </td></tr>
                                                    
  <tr><td class="headings" style="height: 28px">Password:</td><td style="height: 28px; width: 223px;">  <input type="password"  name="txtPassword" class="inputtxt validate required alphanumeric"   data-index="1" id="txtPassword" style="width: 213px" runat="server"/></td><td style="height: 28px">
      <asp:RequiredFieldValidator ID="req_Password" runat="server" ControlToValidate="txtPassword" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
      </td></tr>
                                                    
  <tr><td class="headings">Confirm Password:</td><td style="width: 223px">  <input type="password"  name="txtCPassword" class="inputtxt validate required alphanumeric"   data-index="1" id="txtCPassword" style="width: 213px" runat="server"/></td><td>
      <asp:RequiredFieldValidator ID="req_CPassword" runat="server" ControlToValidate="txtCPassword" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
      <asp:CompareValidator ID="Comp_Password" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtCPassword" ErrorMessage="These passwords don't match. Try again?" ForeColor="#CC0000"></asp:CompareValidator>
      </td></tr>
         <tr><td class="headings">IsActive:</td><td>     <input type="checkbox" id="chkIsActive" checked="checked" runat="server" data-index="2"  name="chkIsActive" />
                                            </td></tr>                                            
                    <tr><td colspan="100%">  <span style="font-size:large">Delivery Address Information</span></td></tr>
                       <tr><td class="headings">Recipient FirstName:</td><td style="width: 223px">  <input type="text"  name="txtRFirstName" class="inputtxt validate required alphanumeric"   data-index="1" id="txtRFirstName" style="width: 213px" runat="server"/></td><td>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                         </td></tr>
                          <tr><td class="headings">Recipient LastName:</td><td style="width: 223px">  <input type="text"  name="txtRLastName" class="inputtxt validate required alphanumeric"   data-index="1" id="txtRLastName" style="width: 213px" runat="server"/></td><td></td></tr>
                       <tr><td class="headings" style="height: 28px">MobileNo:</td><td style="height: 28px; width: 223px;">  <input type="text"  name="txtMobileNo" class="inputtxt validate required alphanumeric"   data-index="1" id="txtMobileNo" style="width: 213px" runat="server"/></td><td style="height: 28px"></td></tr>
                                                    
  <tr><td class="headings">Telephone:</td><td style="width: 223px">  <input type="text"  name="txtTelephone" class="inputtxt validate required alphanumeric"   data-index="1" id="txtTelephone" style="width: 213px" runat="server"/></td><td></td></tr>

                        <tr><td class="headings">City:</td><td style="width: 223px"> <select id ="ddlCities" runat="server"></select></td></tr>
                       <tr><td class="headings">Area:</td><td style="width: 223px">  <input type="text"  name="txtArea" class="inputtxt validate required alphanumeric"   data-index="1" id="txtArea" style="width: 213px" runat="server"/></td><td></td></tr>
                       <tr><td class="headings">HNo/Street:</td><td style="width: 223px">  <input type="text"  name="txtStreet" class="inputtxt validate required alphanumeric"   data-index="1" id="txtStreet" style="width: 213px" runat="server"/></td><td></td></tr>                                 
                        
                         <tr><td class="headings">Address:</td><td style="width: 223px">  <textarea   name="txtAddress" class="inputtxt validate required alphanumeric"   data-index="1" id="txtAddress" style="width: 213px; height: 56px;" runat="server"></textarea></td><td></td></tr>                                 
                         <tr><td class="headings">PinCode:</td><td style="width: 223px">  <input type="text"  name="txtPinCode" class="inputtxt validate required alphanumeric"   data-index="1" id="txtPinCode" style="width: 213px" runat="server"/></td><td></td></tr>                                      
                      <tr><td class="headings">Enter Text</td><td style="width: 223px">
                           &nbsp;</td><td>
                                                                   &nbsp;</td></tr>
                        <tr><td></td><td style="width: 223px">
                      <asp:CheckBox ID="chkAgree" Text ="Terms and conditions I agree" runat="server" Font-Bold="True" /></td></tr>
                      
                 
                      
                                            <tr>
                                             
                                            <td colspan="100%" style="padding-left:45px"  >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td></td>
                                            <td>&nbsp;</td><td> <asp:Button ID="btnAdd" runat="server" class="btn btn-primary btn-small" Text="Submit" OnClick="btnAdd_Click" /></td>
                                           <td></td>
                                                <td>

                                                </td>
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>
                    			 
       
                    </div>
			  </div>
               




            </div>
</form>
</asp:Content>

