﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="managegrouplinks.aspx.cs" Inherits="backoffice_managegrouplinks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
        <link href="css/custom-css/master.css" rel="stylesheet" />

 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">
    var CategoryLevel1 = "";
    var CategoryLevel2 = "";
    var CategoryLevel3 = "";
    var LevelNo = "";
    var m_SubgroupId = 0;
    var EventPhotoUrl = "";

     var ProductsCollection = [];
     function clsProduct() {
         this.ProductId = 0;
         this.ProductName = "";
     }


    function Reset() {


        if ($("#<%=ddlGroup.ClientID%>").val() == "0") {
            jQuery("#jQGridDemo").GridUnload();
        }
        m_SubgroupId = 0;
        var txtTitle = $("#txtTitle");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
        $("#FileUpload1").val("");
        txtTitle.focus();
        txtTitle.val("");
        LevelNo = "0";
        txtTitle.focus();
        btnAdd.css({ "display": "block" });
        btnAdd.html("Save Links");
        $("#txtDescription").val("");

        btnUpdate.css({ "display": "none" });
        btnUpdate.html("Update Links");

      
        $("#btnReset").css({ "display": "none" });

        $("#hdnId").val("0");
        validateForm("detach");
        $("#<%=ddlCategory.ClientID %>").val("0");
        $("#<%=ddlSubCategories.ClientID %>").val("0");
        $("#<%=ddlCategoryLevel3.ClientID %>").val("0");
        ProductsCollection = [];

        $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
        $("#tbProducts").append(" <tr><td colspan='100%' align='center'> 	</td></tr>");
        validateForm("detach");
        BindGrid();
    }



    $(document).on("click", "#dvClose", function (event) {

        var RowIndex = Number($(this).closest('tr').index());
        var tr = $(this).closest("tr");
        tr.remove();

        ProductsCollection.splice(RowIndex, 1);

        if (ProductsCollection.length == 0) {

            $("#tbProducts").append(" <tr><td colspan='100%' align='center'></td></tr>");

        }

    });


    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }


    function InsertUpdate() {

//        if (!validateForm("formID")) {
//            return;
//        }
    

        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");

        var Id = m_SubgroupId;

        //alert(Id);
        // return;
        var Title = $("#txtTitle").val();
        if ($.trim(Title) == "") {
            $("#txtTitle").focus();

            return;
        }
        var SubgroupTitle = $("#txtTitle").val();
        var GroupId = $("#<%=ddlGroup.ClientID%>").val();
     

        var photourl = "";
        var fileUpload = $("#FileUpload1").get(0);
        var files = fileUpload.files;
      
        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            if (files[i].name.length > 0) {
                photourl = files[i].name;
            }

            data.append(files[i].name, files[i]);
        }

        if (m_SubgroupId == 0 && photourl == "") {
            alert("Please select Image for SubGroup");
            return;
        }

        if (files.length > 0) {
            var options = {};
            options.url = "handlers/SubGroupFileUploader.ashx";
            options.type = "POST";
            options.async = false;
            options.data = data;
            options.contentType = false;
            options.processData = false;
            options.success = function (msg) {

                photourl = msg;

            };

            options.error = function (err) { };

            $.ajax(options);


            if (photourl.length > 0) {
                EventPhotoUrl = photourl;
            }
        }

        var LinkUrl = $("#txtUrl").val();
      

        $.ajax({
            type: "POST",
            data: '{"SubgroupId":"' + Id + '", "GroupId": "' + GroupId + '","SubGroupTitle": "' + SubgroupTitle + '","PhotoUrl": "' + EventPhotoUrl + '","LinkUrl": "' + LinkUrl + '"}',
            url: "managegrouplinks.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == -1) {

                    alert("Insertion Failed.Links with Same conditions already exists.");
                    return;

                }

                if (Id == "0") {



                    alert("Links Saved added successfully.");
                    Reset();

                }
                else {

                    alert("Links are Updated successfully.");
                    Reset();

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                BindGrid();
                $.uiUnlock();

            }



        });

    }


    function InsertProducts() {

//        if (!validateForm("formID")) {

//            return;
//        }

        $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');


        var Id = m_SubgroupId;

        var Title = $("#txtTitle").val();
        if ($.trim(Title) == "") {
            $("#txtTitle").focus();

            return;
        }
        var SubgroupTitle = $("#txtTitle").val();
        var GroupId = $("#<%=ddlGroup.ClientID%>").val();


        var photourl = "";
        var fileUpload = $("#FileUpload1").get(0);
        var files = fileUpload.files;

        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            if (files[i].name.length > 0) {
                photourl = files[i].name;
            }

            data.append(files[i].name, files[i]);
        }

        if (m_SubgroupId == 0 && photourl == "") {
            alert("Please select Image for Subgroup");
            return;
        }


        var options = {};
        options.url = "handlers/SubGroupFileUploader.ashx";
        options.type = "POST";
        options.async = false;
        options.data = data;
        options.contentType = false;
        options.processData = false;
        options.success = function (msg) {

            photourl = msg;

        };

        options.error = function (err) { };

        $.ajax(options);


        if (photourl.length > 0) {
            EventPhotoUrl = photourl;
        }


      
        if (ProductsCollection.length == 0) {
            alert("Please first add a Product to link");
            $.uiUnlock();
            return;

        }


        var ProductId = [];

        for (var i = 0; i < ProductsCollection.length; i++) {


            ProductId[i] = ProductsCollection[i]["ProductId"];
           
        }

        $.ajax({
            type: "POST",
            data: '{"SubgroupId":"' + Id + '", "GroupId": "' + GroupId + '","SubGroupTitle": "' + SubgroupTitle + '","PhotoUrl": "' + EventPhotoUrl + '","ProductIdarr": "' + ProductId + '"}',

            url: "managegrouplinks.aspx/InsertSubgroupProducts",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);
                if (obj.Status == "0") {
                    alert("Updation Failed. Please try again later.");
                    return;
                }
                else {
                    alert("Products are linked to SubGroup Successfully");
                    Reset();

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                BindGrid();
                $.uiUnlock();
            }

        });
    }



    $(document).ready(
        function () {

            $("#btnAdd").click(
        function () {

            m_SubgroupId = 0;
            if ($("#<%=ddlLink.ClientID%>").val() == "Categories") {
                InsertUpdate();
            }
            else if ($("#<%=ddlLink.ClientID%>").val() == "Products") {

                InsertProducts();
            }
            else if ($("#<%=ddlLink.ClientID%>").val() == "Brand") {

                InsertUpdate();
            }


        }
        );


            $("#btnUpdate").click(
        function () {

            if ($("#<%=ddlLink.ClientID%>").val() == "Categories") {
                InsertUpdate();
            }
            else if ($("#<%=ddlLink.ClientID%>").val() == "Products") {

                InsertProducts();
            }

            else if ($("#<%=ddlLink.ClientID%>").val() == "Brand") {

                InsertUpdate();
            }

        }
        );

            $("#btnReset").click(
        function () {

            ResetControls();

        }
        );



        $("#btnAddAll").click(
        function () {

            $("#<%=ddlProduct.ClientID%> option").each(function () {


                $(this)

                var ProductId = $(this).val();

                var ProductName = $(this).text();
                if (ProductName != "") {
                    var item = $.grep(ProductsCollection, function (item) {
                        return item.ProductId == ProductId;
                    });

                    if (item.length) {

                        alert("This Product is added in the list");
                        return;

                    }


                    var tr = "<tr><td >" + ProductId + "</td><td >" + ProductName + "</td></td> <td><i id='dvClose'><img src='images/trash.png'/></i> </td></tr>";

                    $("#tbProducts").append(tr);


                    TO = new clsProduct();

                    TO.ProductId = ProductId;
                    TO.ProductName = ProductName;


                    ProductsCollection.push(TO);
                }

            });

        });




            $("#<%=ddlGroup.ClientID%>").change(function () {

                if ($(this).val() == "0") {

                    jQuery("#jQGridDemo").GridUnload();
                    return;
                }

               
                BindGrid();
            });

            $("#<%=ddlCategoryLevel3.ClientID %>").change(
            function () {


                $("#dvrepr").html("");


                var sid = $("#<%=ddlCategoryLevel3.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');
                $("#txtUrl").val('Peshawarisupermarket.com/list.aspx?sc=' + sid);

                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managegrouplinks.aspx/BindCategoryLevel3",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);



                        $("#dvrepr").html(obj.ProductHtml);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                    }

                });


            }

            );




            $("#<%=ddlSubCategories.ClientID %>").change(
            function () {


                $("#dvrepr").html("");
                $("#<%=ddlCategoryLevel3.ClientID %>").html('');

                var sid = $("#<%=ddlSubCategories.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');

                $("#txtUrl").val('Peshawarisupermarket.com/list.aspx?s=' + sid);
                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managegrouplinks.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlCategoryLevel3.ClientID %>").html(obj.SubCatOptions);
                        $("#dvrepr").html(obj.ProductHtml);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                    }

                });


            }

            );




            $("#<%=ddlCategory.ClientID %>").change(
            function () {

                $("#dvrepr").html("");


                $("#<%=ddlSubCategories.ClientID %>").html('');
                $("#<%=ddlCategoryLevel3.ClientID %>").html('');
                var sid = $("#<%=ddlCategory.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');
                $("#txtUrl").val('Peshawarisupermarket.com/list.aspx?c=' + sid);

                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managegrouplinks.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlSubCategories.ClientID %>").html(obj.SubCatOptions);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                    }

                });


            }

            );


            $("#<%=ddlProduct.ClientID %>").change(
            function () {


                var ProductId = $("#<%=ddlProduct.ClientID %>").val();

                var ProductName = $("#<%=ddlProduct.ClientID %> option:selected").text();

                var item = $.grep(ProductsCollection, function (item) {
                    return item.ProductId == ProductId;
                });

                if (item.length) {

                    alert("This Product is added in the list");
                    return;

                }



                var tr = "<tr><td >" + ProductId + "</td><td >" + ProductName + "</td></td> <td><i id='dvClose'><img src='images/trash.png'/></i> </td></tr>";

                $("#tbProducts").append(tr);


                TO = new clsProduct();

                TO.ProductId = ProductId;
                TO.ProductName = ProductName;


                ProductsCollection.push(TO);



            }

            );



            $("#<%=ddlLink.ClientID %>").change(
                    function () {

                        var LInkTo = $("#<%=ddlLink.ClientID%>").val();
                        if (LInkTo == "Categories") {
                            $("#tblCategories").show();
                            $("#tblBrand").hide();
                            $("#tblOr").hide();
                            $("#tblProductsLink").hide();
                            $("#tbProducts").hide();
                            $("#dvlinkProduct").hide();
                            $("#dvlink").show();
                            $("#dvOr").hide();
                            $("#dvUrl").show();
                            $
                        }
                        else if (LInkTo == "Brand") {
                            $("#tblBrand").show();
                            $("#tblCategories").hide();
                            $("#tblOr").hide();
                            $("#tblProductsLink").hide();
                            $("#tbProducts").hide();
                            $("#dvlinkProduct").hide();
                            $("#dvlink").show();
                            $("#dvOr").hide();
                            $("#dvUrl").show();
                        }
                        else if (LInkTo == "Products") {

                            $("#tbProducts").show();
                            $("#tblProductsLink").show();
                            $("#tblCategories").hide();
                            $("#tblBrand").hide();
                            $("#tblOr").hide();
                            $("#dvlink").hide();
                            $("#dvOr").hide();
                            $("#dvlinkProduct").show();
                            $("#dvUrl").hide();
                        }

                    });


            $("#<%=ddlBrand.ClientID %>").change(
            function () {

                var sid = $("#<%=ddlBrand.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $("#txtUrl").val('Peshawarisupermarket.com/list.aspx?b=' + sid);


            }

            );


            $("#<%=ddlCategoryLevel3p.ClientID %>").change(
            function () {


                $("#dvrepr").html("");


                var sid = $("#<%=ddlCategoryLevel3p.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemygroups.aspx/BindCategoryLevel3",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);



                        $("#dvrepr").html(obj.ProductHtml);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                        BindProductsByCategory3($("#<%=ddlCategoryLevel3p.ClientID %>").val());

                    }

                });


            }

            );


            function BindProductsByCategory3(sid) {
                $("#<%=ddlProduct.ClientID %>").html('');
                $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

                $.ajax({
                    type: "POST",
                    data: '{ "SubCategoryId": "' + sid + '"}',
                    url: "managemygroups.aspx/FillProductsByCategoryLevel3",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {


                        var obj = jQuery.parseJSON(msg.d);

                        $("#<%=ddlProduct.ClientID %>").append(obj.ProductOptions);

                    },

                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {


                        $.uiUnlock();
                    }

                });

            }



            $("#<%=ddlSubCategoriesp.ClientID %>").change(
            function () {


                $("#dvrepr").html("");
                $("#<%=ddlCategoryLevel3p.ClientID %>").html('');

                var sid = $("#<%=ddlSubCategoriesp.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managegrouplinks.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlCategoryLevel3p.ClientID %>").html(obj.SubCatOptions);
                        $("#dvrepr").html(obj.ProductHtml);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();
                        BindProductsBySubcategoryId($("#<%=ddlSubCategoriesp.ClientID %>").val());
                    }

                });


            }

            );


            function BindProductsBySubcategoryId(sid) {
                $("#<%=ddlProduct.ClientID %>").html('');
                $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

                $.ajax({
                    type: "POST",
                    data: '{ "SubCategoryId": "' + sid + '"}',
                    url: "managemygroups.aspx/FillProductsBySubcategory",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {


                        var obj = jQuery.parseJSON(msg.d);

                        $("#<%=ddlProduct.ClientID %>").append(obj.ProductOptions);

                    },

                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {


                        $.uiUnlock();
                    }

                });

            }




            $("#<%=ddlCategoryp.ClientID %>").change(
            function () {

                $("#dvrepr").html("");


                $("#<%=ddlSubCategoriesp.ClientID %>").html('');
                $("#<%=ddlCategoryLevel3p.ClientID %>").html('');
                var sid = $("#<%=ddlCategoryp.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managegrouplinks.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlSubCategoriesp.ClientID %>").html(obj.SubCatOptions);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {

                        $.uiUnlock();

                        BindProductsByCategory1($("#<%=ddlCategoryp.ClientID %>").val());
                    }


                });


            }

            );


            function BindProductsByCategory1(cid) {
                $("#<%=ddlProduct.ClientID %>").html('');
                $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

                $.ajax({
                    type: "POST",
                    data: '{ "CategoryId": "' + cid + '"}',
                    url: "managegrouplinks.aspx/FillProductsByCategoryLevel1",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {


                        var obj = jQuery.parseJSON(msg.d);

                        $("#<%=ddlProduct.ClientID %>").append(obj.ProductOptions);

                    },

                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {


                        $.uiUnlock();
                    }

                });

            }


        }
        );

</script>



<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Group Links</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">
                   <table cellpadding="0" cellspacing="0" border="0" class="top_table">
                        <tr>
                            <td class="headings">Choose Group:</td>
                            <td>

                                <asp:DropDownList ID="ddlGroup" class=" validate ddlrequired" runat="server"></asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td class="headings">Title:</td>
                            <td>
                                <input type="text" name="txtTitle" class="inputtxt validate required" data-index="1" id="txtTitle" />
                            </td>
                        </tr>

                        <tr>
                            <td class="headings">PhotoUrl:</td>
                            <td>

                                <input type="file" id="FileUpload1" />

                            </td>
                        </tr>

                        <tr>
                            <td class="Titles">
                                Link To:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlLink" runat="server">
                                    <asp:ListItem Text="Categories" Value="Categories"></asp:ListItem>
                                    <asp:ListItem Text="Brand" Value="Brand"></asp:ListItem>
                                    <asp:ListItem Text="Products" Value="Products"></asp:ListItem>

                                </asp:DropDownList>
                            </td>
                        </tr>

                    </table>
                    			 
       
                    </div>
			  </div>
               
                  <div id="rightnow">
                    <h3 class="reallynow">
                        <span>Manage Linking </span>
                      
                        <br />
                    </h3>
				    <div class="youhave">
                    
                     <table id="tblCategories" class="top_table">
                        <tr>
                            <td class="Titles">
                                Source Level 1:</td>
                            <td>
                                <asp:DropDownList ID="ddlCategory" runat="server">
                                    <asp:ListItem Text="--Category Level 1--" Value="0"></asp:ListItem>
                                </asp:DropDownList>

                            </td>
                        </tr>
                        <tr>
                            <td class="Titles">
                                Source Level 2:</td>
                            <td>
                                <asp:DropDownList ID="ddlSubCategories" runat="server">
                                    <asp:ListItem Text="--Category Level 2--" Value="0"></asp:ListItem>
                                </asp:DropDownList>

                            </td>
                        </tr>
                        <tr id="catLevel3" runat="server">
                            <td class="Titles">
                                Source Level 3:</td>
                            <td>
                                <asp:DropDownList ID="ddlCategoryLevel3" runat="server">
                                    <asp:ListItem Text="--Category Level 3--" Value="0"></asp:ListItem>
                                </asp:DropDownList>

                                <span style="color:#D9395B;font-weight:normal">   <%=m_sSubMessage %></span>
                            </td>
                        </tr>
                    </table>

                     <table id="tblOr">  <tr> <td colspan = "3" align=center><label id="lblor" name="or">OR:</label></td> </tr>

                     </table>
                             <table id = "tblBrand" class="top_table"> <tr id="Brand" runat="server">
                                 <td class="Titles">
                                 <label id="Chosbrnd" name="Chosbrnd">Choose Brand:</label></td>
                                 <td>
                                       <asp:DropDownList ID="ddlBrand" runat="server" 
                                           
                                           >
                                             <asp:ListItem Text="--Product Brand--" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                                 
                                                 
                                                   </td>
                             </tr>
                             </table>
      	          
   
      
                    </div>
			  </div>



                  <div id="rightnow">
                    <h3 class="reallynow">
                        <span> URL: </span>
                      
                        <br />
                    </h3>
				    <div id="dvUrl" class="youhave">
                     <table class="top_table"> <tr >
                                 <td class="Titles">
                                 LinkUrl:</td>
                                 <td colspan = "2">
                                      <input type="text" name="txtUrl" class="inputtxt validate required"   data-index="1" id="txtUrl" />
                                                   </td>
                                 
                             </tr>
                          </table>  
                    </div>
                    </div>

 <div id="dvOr" style="margin-top:10px;text-align:center">
                    <h3 class="reallynow">
                        <span> OR </span>
                      
                        <br />
                    </h3>
				
			  </div>

               
            <div id="rightnow">
                    <h3 class="reallynow">
                        <span>Direct Product Linking </span>

                    </h3>
				    <div id="dvlinkProduct" class="youhave">
                    
                     <table id= "tblProductsLink" class="top_table">
                    <tr> <td class="Titles">
                                     Source Level 1:</td>
                                 <td>
                                     <asp:DropDownList ID="ddlCategoryp" runat="server">
                                          <asp:ListItem Text="--Category Level 1--" Value="0"></asp:ListItem>
                                     </asp:DropDownList>
                                                                       
                                     
                                      
                                     </td>
                                 </tr>
                                    <tr>
                                 <td class="Titles">
                                      Source Level 2:</td>
                                 <td>
                                       <asp:DropDownList ID="ddlSubCategoriesp" runat="server">
                                           <asp:ListItem Text="--Category Level 2--" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                     
                                                   </td>
                             </tr>
                              <tr id="Tr2" runat="server">
                                 <td class="Titles">
                                    Source Level 3:</td>
                                 <td>
                                       <asp:DropDownList ID="ddlCategoryLevel3p" runat="server" 
                                           
                                       >
                                             <asp:ListItem Text="--Category Level 3--" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                                 
                                                 
                                                   </td>
                             </tr>
                           
                              <tr id="Tr1" runat="server">
                                 <td class="Titles">
                                 Choose Product:</td>
                                 <td>
                                       <asp:DropDownList ID="ddlProduct" runat="server" 
                                           
                                           >
                                             <asp:ListItem Text="--Choose Product--" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                                 
                                                 
                                                   </td>
                             </tr>
                             
                    </table>

                        <div id="btnAddAll" class="btn btn-primary btn-small category_table">
                                            Add All Products</div>  

                                    <table class="tablesorter item_table" id="tbProducts" cellspacing="0" style=" margin-top: 30px;">
                                        <thead>
                                            <tr>
                                                <th>
                                                    ProductId
                                                </th>

                                                <th>
                                                    ProductName
                                                </th>
                                                <th> Delete
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="100%" align="center"></td>
                                            </tr>

                                        </tbody>
                                    </table>


                                            <table cellspacing="0" cellpadding="0" class="category_table">
                                            <tr>
                                            <td> <div id="btnAdd"  class="btn btn-primary btn-small" >Save Links</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" >Update Links</div></td>
                                            <td><div id="btnReset"  class="btn btn-primary btn-small" style="display:none;width:60px;" >Cancel</div></td>
                                            </tr>
                                            </table>

                                            </div>

                                            </div>

               <div>
               

               </div>

               <div id="rightnow" style="margin-top:10px">
                    <h3 class="reallynow">
                        <span>Manage Sub Groups </span>
                      
                        <br />
                    </h3>
				    <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>
			  </div>

            </div>
</form>




<script type="text/javascript">
                function BindGrid() {
                 var GroupId = $("#<%=ddlGroup.ClientID%>").val();
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/GetGroupLinks.ashx?GroupId=' + GroupId,
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                         colNames: ['SubGroupId', 'Title','GroupId','PhotoUrl','CategoryId','CategoryL1','CategoryL2','CategoryL3','LevelNo'],
                        colModel: [
                        { name: 'SubGroupId', key: true, index: 'SubGroupId', width: 100, stype: 'text', sorttype: 'int', hidden: true },

                        { name: 'SubGroupTitle', index: 'SubGroupTitle', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                { name: 'GroupId', index: 'GroupId', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }, hidden: true},
   		                { name: 'PhotoUrl', index: 'PhotoUrl', width: 150, stype: 'text', sortable: true, editable: true,hidden:true ,editrules: { required: true }},
                        { name: 'CatgoryId', index: 'CatgoryId', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true}, hidden: true },  
                        { name: 'CategoryL1', index: 'CategoryL1', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true}, hidden: true },  
                        { name: 'CategoryL2', index: 'CategoryL2', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true}, hidden: true  },  
                         { name: 'CategoryL3', index: 'CategoryL3', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true}, hidden: true  },  

                          { name: 'LevelNo', index: 'LevelNo', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true}, hidden: true  },  
                       ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Id',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "GroupLInks List",

                        editurl: 'handlers/GetGroupLinks.ashx',



                    });




                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                  
                   
                    m_SubgroupId = 0;
        
        validateForm("detach");
        var txtTitle = $("#txtTitle");
        m_SubgroupId = $('#jQGridDemo').jqGrid('getCell', rowid, 'SubGroupId');
      
        txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'SubGroupTitle'));
         
//        $("#txtUrl").val($('#jQGridDemo').jqGrid('getCell', rowid, 'PhotoUrl'));
         EventPhotoUrl=$('#jQGridDemo').jqGrid('getCell', rowid, 'PhotoUrl');
//        LevelNo = $('#jQGridDemo').jqGrid('getCell', rowid, 'PhotoUrl');
//        var Group = $('#jQGridDemo').jqGrid('getCell', rowid, 'LevelNo');
       $("#FileUpload1").val("");
        $("#<%=ddlGroup.ClientID%> option[value='" + Group + "']").prop("selected", true);
//        $("#<%=ddlGroup.ClientID%>").val($('#jQGridDemo').jqGrid('getCell', rowid, 'GroupId'));
//        


        
        var CategoryId = $('#jQGridDemo').jqGrid('getCell', rowid, 'CategoryId');


                    $("#txtTitle").focus();
                    $("#btnAdd").css({ "display": "none" });
                    $("#btnUpdate").css({ "display": "block" });
                    $("#btnReset").css({ "display": "block" });
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>




   <%--<script type="text/javascript">
                function BindGrid() {
                    var GroupId = $("#<%=ddlGroup.ClientID%>").val();
                    
                    jQuery("#jQGridDemo").GridUnload();
        jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/GetGroupLinks.ashx?GroupId=' + GroupId,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['SubGroupId', 'Title','GroupId','PhotoUrl','CategoryId','CategoryL1','CategoryL2','CategoryL3','LevelNo'],
            colModel: [
                        { name: 'SubGroupId', key: true, index: 'SubGroupId', width: 100, stype: 'text', sorttype: 'int', hidden: true },

                        { name: 'SubGroupTitle', index: 'SubGroupTitle', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                { name: 'GroupId', index: 'GroupId', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }, hidden: true},
   		                { name: 'PhotoUrl', index: 'PhotoUrl', width: 150, stype: 'text', sortable: true, editable: true,hidden:true ,editrules: { required: true }},
                        { name: 'CatgoryId', index: 'CatgoryId', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true}, hidden: true },  
                        { name: 'CategoryL1', index: 'CategoryL1', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true}, hidden: true },  
                        { name: 'CategoryL2', index: 'CategoryL2', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true}, hidden: true  },  
                         { name: 'CategoryL3', index: 'CategoryL3', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true}, hidden: true  },  

                          { name: 'LevelNo', index: 'LevelNo', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true}, hidden: true  },  
                       ],
            rowNum: 10,
          
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'SubGroupId',
            viewrecords: true,
            height: "100%",
            width:"400px",
            sortorder: 'asc',
            caption: "Sub Group List",
         
            editurl: 'handlers/GetGroupLinks.ashx',
         
                    
             
        });




        $("#jQGridDemo").jqGrid('setGridParam',
{
    onSelectRow: function (rowid, iRow, iCol, e) {
        m_SubgroupId = 0;
        
        validateForm("detach");
        var txtTitle = $("#txtTitle");
        m_SubgroupId = $('#jQGridDemo').jqGrid('getCell', rowid, 'SubGroupId');
      
        txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'SubGroupTitle'));

//        $("#txtUrl").val($('#jQGridDemo').jqGrid('getCell', rowid, 'PhotoUrl'));
         EventPhotoUrl=$('#jQGridDemo').jqGrid('getCell', rowid, 'PhotoUrl');
//        LevelNo = $('#jQGridDemo').jqGrid('getCell', rowid, 'PhotoUrl');
//        var Group = $('#jQGridDemo').jqGrid('getCell', rowid, 'LevelNo');
       
        $("#<%=ddlGroup.ClientID%> option[value='" + Group + "']").prop("selected", true);
//        $("#<%=ddlGroup.ClientID%>").val($('#jQGridDemo').jqGrid('getCell', rowid, 'GroupId'));
//        


        
        var CategoryId = $('#jQGridDemo').jqGrid('getCell', rowid, 'CategoryId');

         
               txtTitle.focus();
               $("#btnAdd").css({ "display": "none" });
               $("#btnUpdate").css({ "display": "block" });
               $("#btnReset").css({ "display": "block" });
               TakeMeTop();
             }
         });

       var DataGrid = jQuery('#jQGridDemo');
       DataGrid.jqGrid('setGridWidth', '500');

       $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                        {
                            refresh: false,
                            edit: false,
                            add: false,
                            del: false,
                            search: false,
                            searchtext: "Search",
                            addtext: "Add",
                        },

                        {//SEARCH
                            closeOnEscape: true

                        }

                          );



}
 
      

                   
              
    </script>--%>
</asp:Content>

