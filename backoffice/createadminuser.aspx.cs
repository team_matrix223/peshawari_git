﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class createadminuser :   BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!User.IsInRole("AdminUsers"))
        {
            Response.Redirect("default.aspx");

        }
    }

    //[WebMethod]
    //public static string GetRoles()
    //{
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var RoleData= new RolesBLL().GetAll();
    //    var JsonData = new
    //    {
    //        RoleData = RoleData,
            
    //    };
    //    return ser.Serialize(JsonData);
         

    //}

    [WebMethod]
    public static string Insert(int AdminId,string  Name, string Mobile,string AdminName,string Password,string Roles, bool isActive)
    {
       
        Admin objAdmin= new Admin()
        {
            AdminId=AdminId,
           Name=Name,
           Mobile=Mobile,
           AdminName=AdminName,
           Password=Password,
           Roles=Roles.Substring(0,Roles.Length-1),
           IsActive = isActive

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new AdminBLL().InsertUpdate(objAdmin);
        var JsonData = new
        {
            Admin = objAdmin,
            Status = status
        };
        return ser.Serialize(JsonData);



    }
}