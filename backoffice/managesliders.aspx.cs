﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data;

public partial class backoffice_managesliders : System.Web.UI.Page
{
    protected string m_sMessage = "";
    protected string m_sSubMessage = "";
    HttpWebRequest URLReq;
    HttpWebResponse URLRes;
    protected bool m_bShow = false;

    protected void Page_Load(object sender, EventArgs e)
    {
       try
        {
            if (!IsPostBack)
            {
                if (!User.IsInRole("ManageSliders"))
                {
                    Response.Redirect("default.aspx");

                }
                btnUpdate.Visible = false;
                btnCancel.Visible = false;
                FillDataList();              
            }
        }
        catch (Exception ex)
        {
        }
    } 
  
    public void Reset()
    {
        txtUrlName.Text = "";
        chkIsActive.Checked = true;
        hdnPhoto.Value = "0";
        lblimg.Text = string.Empty;
        hdnlblimg.Value = string.Empty;
        btnUpdate.Visible = false;
        btnCancel.Visible = false;
        btnAdd.Visible = true;
    }

    public void FillDataList()
    {       
       DataList1.DataSource = new SlidersBLL().GetAll();
       DataList1.DataBind();        
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
        if (!fuImage.HasFile)
        {
            Response.Write("<script>alert('Choose Image For Photo Gallery');</script>");
            fuImage.Focus();
            return;
        }
        string imageName = CommonFunctions.UploadImage(fuImage, "~/SliderImages/", false, 980, 300, false, 0, 0);
        Sliders objSliders = new Sliders()
        {
            ImageUrl = imageName,
            IsActive = chkIsActive.Checked,
            UrlName=txtUrlName.Text,
        };     
            int Status = new SlidersBLL().InsertUpdate(objSliders);
            if (Status == 0)
            {
                Response.Write("<script>alert('Insertion Failed.Please try again.');</script>");
            }
            else
            {
                Response.Write("<script>alert('Photo Added  Successfully!')</script>");
                FillDataList();
                Reset();
            }
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('" + ex.Message + "')</script>");
        }
    }

    protected void DataList1_EditCommand(object source, DataListCommandEventArgs e)
    {
        int PhotoId = (int)DataList1.DataKeys[e.Item.ItemIndex];
        hdnPhoto.Value = PhotoId.ToString();
         chkIsActive.Checked = Convert.ToBoolean(((HiddenField)e.Item.FindControl("hdnactive")).Value);

        lblimg.Text = ((HiddenField)e.Item.FindControl("hdnUrl")).Value;
        hdnlblimg.Value = ((HiddenField)e.Item.FindControl("hdnUrl")).Value;
        txtUrlName.Text = ((HiddenField)e.Item.FindControl("hdnUrlName")).Value;
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
    }
 
    protected void ddlSubPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillDataList();
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
        string imageName = hdnlblimg.Value;

        if (fuImage.HasFile)
        {
            imageName = CommonFunctions.UploadImage(fuImage, "~/SliderImages/", false, 980, 300, false, 0, 0);
        }
        Sliders objSliders = new Sliders()
        {
            SliderId = Convert.ToInt32(hdnPhoto.Value),       
            ImageUrl = imageName,   
            IsActive = chkIsActive.Checked,
            UrlName=txtUrlName.Text
        };      
            int Status = new SlidersBLL().InsertUpdate(objSliders);
            if (Status == 0)
            {
                Response.Write("<script>alert('Insertion Failed.Please try again.');</script>");
            }
            else
            {
                Response.Write("<script>alert('Photo Updated  Successfully!')</script>");
                FillDataList ();
                Reset();
            }
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('" + ex.Message + "')</script>");
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

}