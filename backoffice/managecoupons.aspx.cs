﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_managecoupons : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!User.IsInRole("Coupons"))
            {
                Response.Redirect("default.aspx");

            }
        }
    }


    [WebMethod]
    public static string Insert(int CouponId, string CouponNo,string Title, string Description)
    {
        Coupons objCoupons = new Coupons()
        {
            CouponId = CouponId,
            CouponNo = CouponNo.Trim().ToUpper(),
            Title = Title.Trim().ToUpper(),
            Description = Description,
            AdminId = 0,

        };
        int status = new CouponsBLL().InsertUpdate(objCoupons);
        var JsonData = new
        {
            coupons = objCoupons,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
}