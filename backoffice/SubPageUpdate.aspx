﻿<%@ Page Title="" Language="C#" MasterPageFile="admin.master" AutoEventWireup="true" CodeFile="SubPageUpdate.aspx.cs" Inherits="Admin_SubPageUpdate" %>
<%@ MasterType VirtualPath="admin.master" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
        <link href="css/custom-css/cms.css" rel="stylesheet" />


  <div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Update Sub Pages</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">
                  
                  <form id="frmsubpageUpdate" runat="server">
                   
                         <table class="top_table" style="width: 100%">
                             <tr>
                                 <td>
                                    </td>
                                 <td>
                                    </td>
                                 <td>
                                    </td>
                             </tr>
                             <tr>
                                 <td class="Titles">
                                     Parent Page:</td>
                                 <td>
                                     <asp:DropDownList ID="ddlParentPage" runat="server" AutoPostBack="True" 
                                          onselectedindexchanged="ddlParentPage_SelectedIndexChanged" 
                                         Width="122px">
                                     </asp:DropDownList>
                                    
                                     <asp:Label ID="lblMessage" runat="server" Text="No Sub Page Available" 
                                         Visible="False"></asp:Label>
                                         <span style="color:#D9395B;font-weight:normal">  <%=m_sMessage %></span>
                                     </td>
                                 <td style="color:Red">
                                
                                   </td>
                             </tr>
                              <%if (m_bShow == true)
                                { %>
                             <tr>
                                 <td style="text-align:right" class="Titles">
                                     Sub Pages:</td>
                                 <td>
                                       <asp:DropDownList ID="ddlSubPage"  Width="122px" runat="server" AutoPostBack="True" 
                                                onselectedindexchanged="ddlSubPage_SelectedIndexChanged">
                                            </asp:DropDownList>
                                       <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                           ControlToValidate="ddlSubPage" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                   
                                                  <span style="color:#D9395B;font-weight:normal">   <%=m_sSubMessage %></span>
                                                   </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>
                             
                         <tr>
                                 <td style="text-align:right" class="Titles">
                                        Title:</td>
                                 <td>
                                     <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                         ControlToValidate="txtTitle" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                                 </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>
 
                             <tr>
                                 <td style="text-align:right" class="Titles">
                                    Content:</td>
                                 <td>
                                      <FCKeditorV2:FCKeditor ID="EditorDescription" runat="server" BasePath="~/backoffice/fckeditor/"
                                Height="400px">
                            </FCKeditorV2:FCKeditor>
                                     </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>


                                  


                             <tr>
                                 <td style="text-align:right" class="Titles">
                                     Keywords:</td>
                                 <td>
                                     
                                     <fieldset>
                                      <legend>Meta Tags</legend>
                                      <table>
                                      <tr>
                                      <td>Keywords:</td>
                                      <td> <asp:TextBox ID="txtKeywords" runat="server" Width="300px"></asp:TextBox></td>
                                      </tr>
                                      
                                      
                                      
                                      <tr>
                                      <td>Page Title:</td>
                                      <td><asp:TextBox ID="txtPageTitle" runat="server" Width="180px"></asp:TextBox></td>
                                      </tr>
                                      
                                      
                                      
                                      <tr>
                                      <td>Page Description:</td>
                                      <td><asp:TextBox ID="txtPageDescription" runat="server" TextMode="MultiLine" Height="80px"></asp:TextBox></td>
                                      </tr>
                                      </table>
                                      
                                      </fieldset>
                                     </td>
                             </tr>
                             <tr>
                                 <td class="Titles">
                                     IsActive:</td>
                                 <td>
                                     
                                    <asp:CheckBox ID="chkIsActive" runat="server"  />
                                     </td>
                             </tr>
                               <tr >
                                 <td class="Titles">
                                    Is Full Page:</td>
                                 <td>
                                 
                                 <asp:CheckBox ID="chkFullPage" Checked="true" runat="server" />
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     <asp:Button ID="btnUpdatePage" CssClass="CustomButtons" runat="server" 
                                         Text="Update Page" onclick="btnUpdatePage_Click" />
                                 </td>
                             </tr>
                             <%} %>
                         </table>
                  </form>
                  
  </div>
    </div>
   
   
   
   
    
   
    </div>

</asp:Content>

