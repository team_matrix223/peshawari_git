﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;

public partial class backoffice_deallocationorders : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (!User.IsInRole("DeAllocateOrders"))
            {
                Response.Redirect("default.aspx");

            }
            hdnDate.Value = DateTime.Now.ToShortDateString();
            BindCategories();
        }
    }


    void BindCategories()
    {



        ddlemployee.DataSource = new EmployeesBLL().GetAll();
        ddlemployee.DataValueField = "Emp_Id";
        ddlemployee.DataTextField = "Emp_Name";
        ddlemployee.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Employee--";
        li1.Value = "0";
        ddlemployee.Items.Insert(0, li1);



    }


    [WebMethod]

    public static string Insert(string BillIdarr, Int32 EmpId, string arrOrderNo)
    {
        Empassigned objEmp = new Empassigned()
        {

            Emp_Id = Convert.ToInt32(EmpId),

        };

        string[] BillData = BillIdarr.Split(',');
        string[] OrderData = arrOrderNo.Split(',');

        DataTable dt = new DataTable();
        dt.Columns.Add("BillNo");
        dt.Columns.Add("OrderNo");
        for (int i = 0; i < BillData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["BillNo"] = Convert.ToInt32(BillData[i]); ;
            dr["OrderNo"] = Convert.ToInt32(OrderData[i]); ;

            dt.Rows.Add(dr);

        }


        JavaScriptSerializer ser = new JavaScriptSerializer();

        int st = new EmployeesBLL().InsertDeallocation(objEmp, dt);
        var JsonData = new
        {
            Status = st,
            Bill = objEmp

        };
        return ser.Serialize(JsonData);
    }


}



