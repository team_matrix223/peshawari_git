﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string Uname = txtName.Value;
        string Pass = txtPassword.Value;

        if (!string.IsNullOrEmpty(Uname) && !string.IsNullOrEmpty(Pass))
        {
            Admin objLogin = new Admin()
            {
                AdminName = txtName.Value,
                Password = txtPassword.Value
            };

            new AdminBLL().LoginCheck(objLogin);
            if (objLogin.AdminId != 0)
            {


                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, Uname, DateTime.Now, DateTime.Now.AddMinutes(30), false, objLogin.Roles.ToString(), FormsAuthentication.FormsCookiePath);
                string encticket = FormsAuthentication.Encrypt(ticket);
                HttpCookie ckCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encticket);
                Response.Cookies.Add(ckCookie);

                HttpCookie obj = new HttpCookie("Identity");
                obj.Values["UName"] = txtName.Value;
                obj.Values["Pwd"] = txtPassword.Value;
                Response.Cookies.Add(obj);
                Session[Constants.AdminId] = txtName.Value;
                Response.Redirect("default.aspx");

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Error", "showMessage('<strong>Invalid Username / Password.</strong>. Please enter valid Username and Password.');", true);
            }
        }
    }
}