﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shopping Cart Admin Block</title>
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/theme.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" href="css/unicorn.main.css" />
<script src="js/customValidation.js" type="text/javascript"></script>
<script>
    var StyleFile = "theme" + "4" + ".css";
    document.writeln('<link rel="stylesheet" type="text/css" href="css/' + StyleFile + '">')

    $(document).ready(
    function () {
        $(".anc").click(
        function () {

            $("#u" + $(this).attr("id")).toggle(500);
        }

        );


    }
    );
    function ExpandCollapse(li) {
        alert(li);
    }

    function ShowTLinks(val) {


        $("div [name='tLinks']").hide();
        var ul = $("#ulT" + val);

        $(".current").removeClass("current");
        $("#anT" + val).addClass("current");
        ul.show();


    }
</script>

    <style>
        body {overflow-x: hidden; width: 100%; float: left;}
        #header { padding: 30px 30px;}
        .form_login {width: 25%; clear: both; padding: 8% 0;}
        .form_login #content {width: 100%; float: left; overflow: hidden;}
        .form_login #content #rightnow {width: 100%; float: left;}
        div#footer {position: absolute; bottom: 0; left: 0; right: 0; margin: 0px; text-align: center;}
        .form_login #content #rightnow .youhave {padding: 15px 0px;}
        .form_login #content #rightnow .youhave table {width: 100%; margin: 0px;}
        .form_login #content #rightnow .youhave table input {width: 100%;}
        .form_login #content #rightnow .youhave table td {text-align: left; padding: 0px 25px;}
        .form_login #content #rightnow .youhave table td leble {padding: 0 0 10px;width: 100%; float: left; text-align: left; font-size: 15px;font-weight: 400; color: #294145;}
        .form_login #content #rightnow .youhave table input {width: 98%;float: left; height: 25px; border-radius: 0px; margin: 0 0 10px;}
        .form_login #content #rightnow .youhave table #btnLogin {width: 100%; height: 40px; margin: 0px; padding: 0px;}
        .form_login #content #rightnow h3 {padding: 10px 25px;}
    </style>
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="css/ie-sucks.css" />
<![endif]-->
</head>

<body>
    	<div id="header">
        	<h2>SuperMarket<sup style="font-size:10px;font-weight:bold;font-style:italic">PS </sup></h2>
   
      </div>
       

<div id="wrapper">
<form id="frmLogin" runat="server" class="form_login">
            <div id="content">
       			<div id="rightnow"   >
                    <h3 class="reallynow">
                        <span>Admin Login Panel</span>
                        
                        <br />
                    </h3>
				    <div class="youhave">
 
                           <table cellpadding="0" cellspacing="0" border="0">
                             <tr>
                                 <td><leble class="headings">Admin Name:</leble>
                                    <input type="text" runat="server" name="txtAdminName" class="inputtxt validate required alphanumeric"  data-index="1" id="txtName" /></td>
                             </tr>
                             <tr>
                                 <td><leble class="headings">Password:</leble>
                                     <input type="password" runat="server" name="txtPassword" class="inputtxt validate required alphanumeric"  data-index="1" id="txtPassword" /></td>
                             </tr>
                               <tr>
                                   <td><asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="btn btn-primary btn-small" onclick="btnLogin_Click"/></td>
                               </tr>
                            </table>

                    </div>
			  </div>
               
            </div>
</form>
             
      </div>







        <div id="footer">
        <div id="credits">
   		Website Designed and Developed By <a href="#">Matrix Software Solutions Pvt Ltd.</a>
        </div>
        <br />

        </div>
</body>
</html>
