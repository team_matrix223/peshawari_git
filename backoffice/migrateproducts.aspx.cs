﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;

public partial class backoffice_migrateproducts : System.Web.UI.Page
{
    protected string m_sMessage = "";
    protected string m_sSubMessage = "";
    public static string PhotoUrl = "";
    protected bool m_bShow = false;
    protected string ErrorMessage = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (!IsPostBack)
            {

                if (!User.IsInRole("MigrateProducts"))
                {
                    Response.Redirect("default.aspx");

                }
                catLevel3.Visible = false;

                BindCategories();
                BindBrand();
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnAddBrand_Click(object sender, EventArgs e)
    {
        if (txtBrandName.Text.Trim() != "")
        {
            Brand objBrand = new Brand()
            {
                BrandId = 0,
                Title = txtBrandName.Text.Trim().ToUpper(),
                IsActive = true,
                AdminId = 0,

            };

            int status = new BrandBLL().InsertUpdate(objBrand);
            if (status > 0)
            {
                ListItem li = new ListItem();
                li.Text = txtBrandName.Text.Trim().ToUpper();
                li.Selected = true;
                li.Value = status.ToString();
                ddlBrand.SelectedIndex = -1;
                ddlBrand.Items.Add(li);
                txtBrandName.Text = "";
            }
        }
    }

    [WebMethod]
    public static string Search(string Keyword)
    {
        string[]arr=Keyword.Trim().Split(' ');
        var lst = new PackingDAL().Search(arr[0]);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        var JsonData = new
        {

            Data = lst

        };

        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string Insert(int C1, int C2, int C3, int Brand, string Name, string SName, string Desc, string arrItemCode, string arrUnits, string arrQty, string arrMrp, string arrPrice, string arrType, string arrDesc,bool IsSingle,string arrPName)
    {

        string[] ItemCodes = arrItemCode.Split(',');

        string[] Units = arrUnits.Split(',');
        string[] Qty = arrQty.Split(',');
        string[] Price = arrPrice.Split(',');
        string[] Mrp = arrMrp.Split(',');
        string[] Type = arrType.Split(',');
        string[] Description = arrDesc.Split(',');
        string[] ProductName = arrPName.Split(',');

        DataTable dt = new DataTable();
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("Unit");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Mrp");
        dt.Columns.Add("Price");
        dt.Columns.Add("VType");
        dt.Columns.Add("Description");
        dt.Columns.Add("ProductName");

        for (int i = 0; i < ItemCodes.Length; i++)
        {

            DataRow dr = dt.NewRow();
            dr["ItemCode"] = ItemCodes[i];
            dr["Unit"] = Units[i];
            dr["Qty"] = Qty[i];
            dr["Mrp"] = Price[i];
            dr["Price"] = Mrp[i];
            dr["VType"] =Type[i];
            dr["Description"] = Description[i];
            dr["ProductName"] = ProductName[i];
            dt.Rows.Add(dr);
        }


        JavaScriptSerializer ser = new JavaScriptSerializer();
        int status = new PackingDAL().Insert(C1,C2,C3,Brand,Name,SName,Desc,dt,IsSingle);
        var JsonData = new
        {
         
            Status = status
        };
        return ser.Serialize(JsonData);
    }

    public void BindBrand()
    {
        ddlBrand.DataSource = new BrandBLL().GetAll();
        ddlBrand.DataTextField = "Title";
        ddlBrand.DataValueField = "BrandId";
        ddlBrand.DataBind();
        ListItem li = new ListItem();
        li.Text = "--Choose Manufacturer--";
        li.Value = "";
        ddlBrand.Items.Insert(0, li);

    }
    public void BindCategories()
    {
        try
        {
            ddlCategory.DataSource = new CategoryBLL().GetByLevel(1);
            ddlCategory.DataValueField = "CategoryId";
            ddlCategory.DataTextField = "Title";
            ddlCategory.DataBind();
            ListItem li = new ListItem();
            li.Text = "--Category Level 1--";
            li.Value = "0";

            ddlCategory.Items.Insert(0, li);

        }
        finally { }

    }
    public void BindSubCategories(int ParentId, DropDownList ddl)
    {
        try
        {
            if (ddlCategory.SelectedIndex != 0)
            {
                lblMessage.Visible = false;
                m_bShow = true;
                ddl.DataSource = new CategoryBLL().GetByParentId(ParentId);
                ddl.DataTextField = "Title";
                ddl.DataValueField = "CategoryId";
                ddl.DataBind();
                ListItem li = new ListItem();
                li.Text = "--Category Level 2--";
                li.Value = "0";
                ddl.Items.Insert(0, li);

            }
            else
            {
                m_sMessage = "Please Select  Category Level 1";

            }
        }
        finally
        {
        }

    }

    public void BindCategoryLevel3(int ParentId)
    {
        try
        {
            if (ddlCategory.SelectedIndex != 0)
            {
                lblMessage.Visible = false;
                m_bShow = true;
                ddlCategoryLevel3.DataSource = new CategoryBLL().GetByParentId(ParentId);
                ddlCategoryLevel3.DataTextField = "Title";
                ddlCategoryLevel3.DataValueField = "CategoryId";
                ddlCategoryLevel3.DataBind();
                ListItem li = new ListItem();
                li.Text = "--Category Level 3--";
                li.Value = "0";
                ddlCategoryLevel3.Items.Insert(0, li);

            }
            else
            {
                m_sMessage = "Please Select  Category";
                return;
            }
        }
        finally
        {
        }

    }








    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        catLevel3.Visible = false;
        if (ddlCategory.SelectedIndex > 0)
        {
            BindSubCategories(Convert.ToInt32(ddlCategory.SelectedValue), ddlSubCategories);
        }
        else
        {
            catLevel3.Visible = false;

            ddlSubCategories.Items.Clear();
            ListItem li = new ListItem();
            li.Text = "--Category Level 2--";
            li.Value = "0";
            ddlSubCategories.Items.Insert(0, li);

            ddlCategoryLevel3.Items.Clear();
            ListItem li2 = new ListItem();
            li2.Text = "--Category Level 3--";
            li2.Value = "0";
            ddlCategoryLevel3.Items.Insert(0, li2);

        }
    }
    protected void ddlSubCategories_SelectedIndexChanged(object sender, EventArgs e)
    {


        if (ddlSubCategories.SelectedIndex > 0)
        {
            BindCategoryLevel3(Convert.ToInt32(ddlSubCategories.SelectedValue));

            if (ddlCategoryLevel3.Items.Count == 1)
            {
                catLevel3.Visible = false;

            }
            else
            {
                catLevel3.Visible = true;

            }
        }
        else
        {
            ddlCategoryLevel3.Items.Clear();
            ListItem li = new ListItem();
            li.Text = "--Category Level 3--";
            li.Value = "0";
            ddlCategoryLevel3.Items.Insert(0, li);

        }
    }
    protected void ddlCategoryLevel3_SelectedIndexChanged(object sender, EventArgs e)
    {

    }




}