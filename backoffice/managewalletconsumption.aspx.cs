﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_managewalletconsumption : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string Update(int WalletConsumptionId)
    {
        new WalletBLL().WalletUpdateDispatchStatus(WalletConsumptionId);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new {
            Status =0
        };
        return ser.Serialize(JsonData);
    }
}