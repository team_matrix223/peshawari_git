﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_managebrand : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!User.IsInRole("ManageBrands"))
            {
                Response.Redirect("default.aspx");

            }
        }
    }
    [WebMethod]
    public static string Insert(int BrandId, string Title, bool IsActive)
    {
        Brand objBrand = new Brand()
        {
            BrandId = BrandId,
            Title = Title.Trim().ToUpper(),
            IsActive = IsActive,
            AdminId = 0,

        };
        int status = new BrandBLL().InsertUpdate(objBrand);
        var JsonData = new
        {
            City = objBrand,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
}