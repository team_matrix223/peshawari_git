﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_managesettings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!User.IsInRole("Settings"))
            {
               Response.Redirect("default.aspx");

            }
        }
    }
    [WebMethod]
    public static string Insert(decimal DeliveryCharge, decimal MinimumCheckOutAmt, decimal FreeDeliveryAmt, int PointRate, int ReferralPoint, int RefereePoint, int MiniReimbursementPoint, int MiniReimbursementCash)
    {

        Settings objSetting = new Settings()
        {
            DeliveryCharge = DeliveryCharge,
            MinimumCheckOutAmt = MinimumCheckOutAmt,
            FreeDeliveryAmt = FreeDeliveryAmt,
            PointRate = PointRate,
            ReferralPoint=ReferralPoint,
            RefereePoint=RefereePoint,
            MiniReimbursementPoint = MiniReimbursementPoint,
            MiniReimbursementCash = MiniReimbursementCash
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new SettingsDAL ().InsertUpdate(objSetting);
        var JsonData = new
        {
            Setting = objSetting,
            Status = status
        };
        return ser.Serialize(JsonData);



    }
    [WebMethod]
    public static string GetData()
    {

        Settings objSetting = new Settings();
        objSetting = new SettingsDAL().GetSett();
        JavaScriptSerializer ser = new JavaScriptSerializer();

     
        var JsonData = new
        {
            Setting = objSetting
        };
        return ser.Serialize(JsonData);



    }
}