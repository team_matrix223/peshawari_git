﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_checkorderstatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!User.IsInRole("CheckOrderStatus"))
            {
                Response.Redirect("default.aspx");

            }
        }
    }



    [WebMethod]
    public static string GetOrderDetail(int Oid)
    {
        OrderDetail objOrder = new OrderDetail() { OrderId = Oid };


        var OrderDetail = new OrderBLL().GetOrderDetailByOrderId(objOrder);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            DetailData = OrderDetail
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetTemparoryOrders(int Oid)
    {
        OrderDetail objOrder = new OrderDetail() { OrderId = Oid };


        var OrderDetail = new OrderBLL().GetOrdersTemp(objOrder);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            TempOrders = OrderDetail
        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]

    public static string InsertTemp(Int32 OI)
    {
        OrderDetail objOrder = new OrderDetail()
        {
            OrderId = Convert.ToInt32(OI),
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        new OrderBLL().InsertTemp(objOrder);
        var JsonData = new
        {
            Order = objOrder

        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string GetByOrderId(Int32 OrderId)
    {
        
        Order objOrder = new Order() { OrderId = OrderId };
       
        new OrderBLL().GetOrderbyOrderId(objOrder);

        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Orders = objOrder,


        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetByBillId(Int32 BillId)
    {

        Bills objBill = new Bills() { BillId = BillId };

        new BillBLL().GetBillbyBillId(objBill);

        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Bill = objBill,


        };
        return ser.Serialize(JsonData);
    }

}