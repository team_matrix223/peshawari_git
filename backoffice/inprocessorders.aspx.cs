﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;
public partial class backoffice_inprocessorders : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (!User.IsInRole("InProcessOrders"))
            {
                Response.Redirect("default.aspx");

            }


            hdnDate.Value = DateTime.Now.ToShortDateString();
            BindCategories();
        }
    }


    void BindCategories()
    {

        ddlCategories.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategories.DataValueField = "CategoryId";
        ddlCategories.DataTextField = "Title";
        ddlCategories.DataBind();
        ListItem li = new ListItem();
        li.Text = "--Catgory Level 1--";
        li.Value = "0";
        ddlCategories.Items.Insert(0, li);
    }


    [WebMethod]
    public static string GetOrderDetail(int Oid)
    {
        OrderDetail objOrder = new OrderDetail() { OrderId = Oid };


        var OrderDetail = new OrderBLL().GetOrderDetailByOrderId(objOrder);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            DetailData = OrderDetail
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetTemparoryOrders(int Oid)
    {
        OrderDetail objOrder = new OrderDetail() { OrderId = Oid };


        var OrderDetail = new OrderBLL().GetOrdersTemp(objOrder);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            TempOrders = OrderDetail
        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]

    public static string InsertTemp(Int32 OI)
    {
        OrderDetail objOrder = new OrderDetail()
        {
            OrderId = Convert.ToInt32(OI),
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        new OrderBLL().InsertTemp(objOrder);
        var JsonData = new
        {
            Order = objOrder

        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]

    public static string DeleteTempOrders(Int32 OI, Int32 ProductId, Int32 VariationId)
    {
        OrderDetail objOrder = new OrderDetail()
        {
            OrderId = OI,
            ProductId = ProductId,
            VariationId = VariationId,
        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        new OrderBLL().DeleteTempOrders(objOrder.OrderId, objOrder.ProductId, objOrder.VariationId);
        var JsonData = new
        {
            Order = objOrder

        };
        return ser.Serialize(JsonData);
    }


    public static string GetProductOptions(Int32 ProductId)
    {
        Products objProduct = new Products()
        {
            ProductId = ProductId,
        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        new ProductsBLL().GetProductDetailByProductId(objProduct);
        var JsonData = new
        {
            Order = objProduct


        };
        return ser.Serialize(JsonData);
    }





    [WebMethod]

    public static string UpdateOrdersBilled(Int32 OI)
    {


        Order objOrder = new Order()
        {
            OrderId = Convert.ToInt32(OI),


        };


        JavaScriptSerializer ser = new JavaScriptSerializer();

        new OrderBLL().BilledOrders(objOrder.OrderId);
        var JsonData = new
        {
            Order = objOrder

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string UpdateQtyByProductId(int OrderId, int ProductId, Int32 VariationId, int Qty, decimal Price, string Mode)
    {


        int status = new OrderBLL().UpdateOrderDetailQtyByProductId(OrderId, ProductId, VariationId, Qty, Price, Mode);
        var JsonData = new
        {
            Status = status,
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


}