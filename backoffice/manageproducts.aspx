﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master"  EnableEventValidation="false" ViewStateEncryptionMode="Never"  AutoEventWireup="true" CodeFile="manageproducts.aspx.cs" Inherits="backoffice_manageproducts" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
        <asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
            <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>

            <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
            <script src="js/grid.locale-en.js" type="text/javascript"></script>
            <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
            <link href="css/custom-css/master.css" rel="stylesheet" />
            <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
            <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
            <script src="js/jquery-ui.js" type="text/javascript"></script>

            <script type="text/javascript" src="js/jquery.uilock.js"></script>
            <script type="text/javascript">
                $(document).on("click", "div[name='btnEdit']", function(event) {

                    var rowindex = Number($(this).closest('tr').index());

                    //var tr = $(this).closest("tr");
                    //var Id = $(this).attr("id");
                    //var arrPid = Id.split('_');
                    //var vid = arrPid[1];
                    EditTempById(rowindex);
                });

                function EditTempById(rowindex) {
                    $.ajax({
                        type: "POST",
                        data: '{"Id":"' + rowindex + '"}',
                        url: "manageproducts.aspx/EditTempById",
                        contentType: "application/json",
                        dataType: "json",
                        success: function(msg) {
                            var obj = jQuery.parseJSON(msg.d);

                            $("#<%=txtDescr.ClientID %>").val(obj.Desc);
                            //$("#<%=ddlUnits.ClientID %>").val(obj.Unit);
                            $("#<%=ddlUnits.ClientID%> option[value='" + obj.Unit + "']").prop("selected", true);
                            //   $("#<%=ddltype.ClientID %>").val(obj.Type);
                            $("#<%=ddltype.ClientID%> option[value='" + obj.Type + "']").prop("selected", true);
                            $("#<%=txtQty.ClientID %>").val(obj.Qty);
                            $("#<%=txtPrice .ClientID %>").val(obj.Price);
                            $("#<%=txtMRP.ClientID %>").val(obj.Mrp);
                            $("#<%=txtItemCode.ClientID %>").val(obj.ICode);
                            $("#<%=hdVariationId.ClientID %>").val(obj.HdVid);
                            $("#<%=btnAddToTemp.ClientID %>").val('Update');
                            // $("#<%=btnCancelPrice.ClientID %>").show();
                            //  $("#<%=btnCancelPrice.ClientID %>").css('display', 'block');
                            // document.getElementById("<%= btnCancelPrice.ClientID %>").style.display = 'block';
                        },
                        error: function(xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function() {

                            // $.uiUnlock();
                        }

                    });
                }
                $(document).on("click", "div[name='btnDelete']", function(event) {
                    var rowindex = Number($(this).closest('tr').index());

                    var tr = $(this).closest("tr");
                    var Id = $(this).attr("id");
                    var arrPid = Id.split('_');
                    var vid = arrPid[1];
                    DeleteTempById(rowindex);
                    tr.remove();
                });

                function DeleteTempById(rowindex) {
                    $.ajax({
                        type: "POST",
                        data: '{"Id":"' + rowindex + '"}',
                        url: "manageproducts.aspx/DeleteTempById",
                        contentType: "application/json",
                        dataType: "json",
                        success: function(msg) {
                            var obj = jQuery.parseJSON(msg.d);

                            $('#tbMainList tr').not(function() {
                                if ($(this).has('th').length) {
                                    return true
                                }
                            }).remove();

                            //for (var i = 0; i < obj.DetailData.length; i++) {

                            //    var PhotoUrl ="";
                            //    PhotoUrl = obj.DetailData[i]["PhotoUrl"];
                            //    var VariationId = 0;
                            //    VariationId = obj.DetailData[i]["Id"];
                            //    var Unit = "";
                            //    Unit = obj.DetailData[i]["Unit"];
                            //    var type = "";
                            //    type = obj.DetailData[i]["type"];
                            //    var Qty = 0;
                            //    Qty = obj.DetailData[i]["Qty"];
                            //    var Price = 0;
                            //    Price = obj.DetailData[i]["Price"];
                            //    var Mrp = 0;
                            //    Mrp = obj.DetailData[i]["Mrp"];
                            //    var Description = "";
                            //    Description = obj.DetailData[i]["Description"];
                            //    var tr = "<tr><td><img style='height:30px;width:30px' src='../ProductImages/" + PhotoUrl + "' /></td><td>" + Unit + "</td><td>" + type + "</td><td>" + Qty + "</td><td>" + Price + "</td><td>" + Mrp + "</td><td>" + Description + "</td><td> <div class='btn btn-primary btn-small' name='btnDelete' id='btn_" + VariationId + "'>Delete</div></td></tr>";
                            //    $("#tbMainList").append(tr);
                            //}
                            $("#tbMainList").append(obj.DetailData);

                        },
                        error: function(xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function() {

                            $.uiUnlock();
                        }

                    });
                }
                $(document).ready(
                    function() {
                        //    $("#<%=btnCancelPrice.ClientID %>").css('display', 'none');
                    });
            </script>
            <form id="frmProducts" runat="server">
                <asp:HiddenField ID="hdVariationId" Value="-1" runat="server" />
                <div id="content">

                    <div id="rightnow">
                        <h3 class="reallynow">
                        <span>Add/Update Products</span>

                        <br />
                    </h3>
                        <div class="youhave">

                            <table align="center" border="0">
                                <tr>
                                    <td>
                                        <asp:ScriptManager ID="Scrpt1" runat="server"></asp:ScriptManager>

                                        <asp:UpdateProgress AssociatedUpdatePanelID="updCatgories" runat="server">
                                            <ProgressTemplate>
                                                <div id="dvProgress" style="position:absolute;width:750px;height:500px;background-color:Black;color:White;text-align:center;opacity:0.7;vertical-align:middle">
                                                    loading please wait..

                                                </div>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>

                                        <%--   <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="updProducts" runat="server">
            <ProgressTemplate>
           <div id="dvProgress" style="position:absolute;width:750px;height:500px;background-color:Black;color:White;text-align:center;opacity:0.7;vertical-align:middle">
           loading please wait..

           </div>
            </ProgressTemplate>
            </asp:UpdateProgress> --%>

                                            <asp:UpdatePanel ID="updCatgories" runat="server">
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnAdd" />
                                                    <asp:PostBackTrigger ControlID="btnUpdate" />
                                                    <asp:PostBackTrigger ControlID="btnAddToTemp" />
                                                </Triggers>
                                                <ContentTemplate>

                                                    <table style="width: 100%">

                                                        <tr>
                                                            <td>
                                                                <asp:HiddenField ID="hdnProductId" runat="server" Value="0" />
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>

                                                                <table cellspacing="2" cellpadding="0" border="0" class="top_table">

                                                                    <tr>
                                                                        <td colspan="100%" style="color:Red;font-style:italic">

                                                                            <%=ErrorMessage %>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="Titles">
                                                                            Category Level 1:</td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlProductCategory1" runat="server" AutoPostBack="True" onselectedindexchanged="ddlProductCategory1_SelectedIndexChanged">
                                                                                <asp:ListItem Text="--Category Level 1--" Value="0"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="ddlProductCategory1" runat="server" InitialValue="0" ErrorMessage="*" ForeColor="Red" ValidationGroup="b"></asp:RequiredFieldValidator>

                                                                            <asp:Label ID="lblMessage" runat="server" Text="No Sub Page Available" Visible="False"></asp:Label>
                                                                            <span style="color:#D9395B;font-weight:normal">  <%=m_sMessage %></span>
                                                                        </td>

                                                                    </tr>

                                                                    <tr>
                                                                        <td class="Titles">
                                                                            Category Level 2:</td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlProductCategory2" runat="server" AutoPostBack="True" onselectedindexchanged="ddlProductCategory2_SelectedIndexChanged">
                                                                                <asp:ListItem Text="--Category Level 2--" Value="0"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlProductCategory2" ForeColor="Red" InitialValue="0" ErrorMessage="*" ValidationGroup="b"></asp:RequiredFieldValidator>

                                                                            <span style="color:#D9395B;font-weight:normal">   <%=m_sSubMessage %></span>
                                                                        </td>

                                                                    </tr>

                                                                    <tr id="ProductcatLevel3" runat="server">
                                                                        <td class="Titles">
                                                                            Category Level 3:</td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlProductCategory3" runat="server">
                                                                                <asp:ListItem Text="--Category Level 3--" Value="0"></asp:ListItem>
                                                                            </asp:DropDownList>

                                                                            <span style="color:#D9395B;font-weight:normal">   <%=m_sSubMessage %></span>
                                                                        </td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td class="Titles">
                                                                            Brand:</td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlbrand" runat="server">
                                                                                <asp:ListItem Text="--Choose Brand--" Value="0"></asp:ListItem>
                                                                            </asp:DropDownList>

                                                                        </td>

                                                                    </tr>

                                                                    <tr>
                                                                        <td class="Titles">
                                                                            Name:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator runat="server" id="reqName" SetFocusOnError="true" controltovalidate="txtTitle" ForeColor="Red" errormessage="*" ValidationGroup="b" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="Titles">
                                                                            Short Name:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtShortName" runat="server"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator2" controltovalidate="txtTitle" ForeColor="Red" errormessage="*" ValidationGroup="b" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="Titles">
                                                                            Description:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Height="62px"></asp:TextBox>

                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td class="Titles">
                                                                            Short Desc:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtShortDesc" runat="server" TextMode="MultiLine"></asp:TextBox>

                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td class="Titles">
                                                                            IsActive:</td>
                                                                        <td>
                                                                            <input type="checkbox" id="chkIsActive" runat="server" checked="checked" data-index="2" name="chkIsActive" />
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                                            <div id="dvTopBasket" style="display:block">
                                                                                <table class="midle_table">
                                                                                    <tr>
                                                                                        <td colspan="100%">
                                                                                            <h3 class="reallynow">
                                                                                                <span>Price List:</span>
                                                                                                <br />
                                                                                            </h3>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="100%">
                                                                                            <asp:FileUpload ID="FileUpload1" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>

                                                                                        <td>
                                                                                            <asp:DropDownList ID="ddlUnits" runat="server">
                                                                                                <asp:ListItem Text="Units" Value="0"></asp:ListItem>
                                                                                                <asp:ListItem Text="NOS" Value="nos"></asp:ListItem>
                                                                                                <asp:ListItem Text="KG" Value="kg"></asp:ListItem>
                                                                                                <asp:ListItem Text="GRAM" Value="gm"></asp:ListItem>
                                                                                                <asp:ListItem Text="LITER" Value="lt"></asp:ListItem>
                                                                                                <asp:ListItem Text="PIECE" Value="pcs"></asp:ListItem>
                                                                                                <asp:ListItem Text="ML" Value="ml"></asp:ListItem>
                                                                                            </asp:DropDownList>

                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:RequiredFieldValidator ID="reqUnits" ControlToValidate="ddlUnits" runat="server" ErrorMessage="*" ForeColor="Red" InitialValue="0" ValidationGroup="a"></asp:RequiredFieldValidator>

                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtQty" runat="server" placeholder="Qty"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" ValidationGroup="a" ControlToValidate="txtQty" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" ValidationGroup="a" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtQty" ValidationExpression="\d+(\.\d{1,2})?"></asp:RegularExpressionValidator>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:DropDownList ID="ddltype" runat="server">
                                                                                                <asp:ListItem Text="Type" Value=""></asp:ListItem>
                                                                                                <asp:ListItem Text="CARTON" Value="carton"></asp:ListItem>
                                                                                                <asp:ListItem Text="BOTTLE" Value="bottle"></asp:ListItem>
                                                                                                <asp:ListItem Text="POLY BAG" Value="poly bag"></asp:ListItem>
                                                                                                <asp:ListItem Text="POLY PACK" Value="poly pack"></asp:ListItem>
                                                                                                <asp:ListItem Text="PACKET" Value="packet"></asp:ListItem>
                                                                                                <asp:ListItem Text="CUP" Value="cup"></asp:ListItem>
                                                                                                <asp:ListItem Text="BOX" Value="box"></asp:ListItem>
                                                                                                <asp:ListItem Text="TIN" Value="tin"></asp:ListItem>
                                                                                                <asp:ListItem Text="POUCH" Value="pouch"></asp:ListItem>
                                                                                                <asp:ListItem Text="JAR" Value="jar"></asp:ListItem>
                                                                                            </asp:DropDownList>

                                                                                    </tr>
                                                                                    <tr>

                                                                                        <td>
                                                                                            <asp:TextBox ID="txtPrice" placeholder="Price" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" ControlToValidate="txtPrice" runat="server" ErrorMessage="*" ValidationGroup="a" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                                            <asp:RegularExpressionValidator ID="regPrice" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtPrice" ValidationExpression="\d+(\.\d{1,2})?"></asp:RegularExpressionValidator>
                                                                                        </td>

                                                                                        <td>
                                                                                            <asp:TextBox ID="txtMRP" runat="server" placeholder="MRP"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:RequiredFieldValidator ID="ReqMRP" Display="Dynamic" ControlToValidate="txtMRP" runat="server" ErrorMessage="*" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                                                                                            <asp:RegularExpressionValidator ID="regMRP" Display="Dynamic" ValidationGroup="a" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtMRP" ValidationExpression="\d+(\.\d{1,2})?"></asp:RegularExpressionValidator>

                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtItemCode" runat="server" placeholder="Item Code"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="100%">
                                                                                            <asp:TextBox ID="txtDescr" TextMode="SingleLine" placeholder="Description" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </table>

                                                                                            <table class="category_table">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Button ID="btnAddToTemp" CssClass="btn btn-primary btn-small" runat="server" ValidationGroup="a" Text="Add" onclick="btnAddToTemp_Click" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Button ID="btnCancelPrice" CssClass="btn btn-primary btn-small" runat="server" Text="Reset" OnClick="btnCancelPrice_Click" />

                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                            </div>


                                                                            <table id="tbMainList" class="item_table">
                                                                                <thead>
                                                                                    <tr style="background:#294145;color:white">
                                                                                        <th>Image</th>
                                                                                        <th>ItemCode</th>
                                                                                        <th>Units</th>
                                                                                        <th>Type</th>
                                                                                        <th>Qty</th>
                                                                                        <th>Price</th>
                                                                                        <th>MRP</th>
                                                                                        <th>Desc</th>
                                                                                        <th>Edit</th>
                                                                                        <th>Delete</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody class="customTable">
                                                                                    <%=htmldata%>
                                                                                </tbody>
                                                                            </table>

                                                    </table>

                                                    <table cellspacing="2" cellpadding="0" border="0" class="category_table">

                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnAdd" CssClass="btn btn-primary btn-small" runat="server" ValidationGroup="b" Text="Add Product" onclick="btnAdd_Click" />

                                                                <asp:Button ID="btnUpdate" CssClass="btn btn-primary btn-small" runat="server" ValidationGroup="b" Text="Update Product" onclick="btnUpdate_Click" />
                                                                <asp:Button Visible="false" ID="btnCancel" CssClass="btn btn-primary btn-small" runat="server" Text="Cancel" onclick="btnCancel_Click" CausesValidation="false" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                    </table>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                            </td>
                                </tr>
                            </table>

                        </div>

                   <div id="rightnow">
                        <h3 class="reallynow">
                        <span>View Products</span>

                        <br />
                    </h3>
                        <div class="youhave">

                            <asp:UpdatePanel UpdateMode="Conditional" ID="updProducts" runat="server">

                                <ContentTemplate>
                                    <table class="top_table">
                                        <tr>
                                            <td class="Titles">
                                                Category Level 1:</td>
                                            <td>
                                                <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True" onselectedindexchanged="ddlCategory_SelectedIndexChanged">
                                                    <asp:ListItem Text="--Category Level 1--" Value="0"></asp:ListItem>
                                                </asp:DropDownList>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="Titles">
                                                Category Level 2:</td>
                                            <td>
                                                <asp:DropDownList ID="ddlSubCategories" runat="server" AutoPostBack="True" onselectedindexchanged="ddlSubCategories_SelectedIndexChanged">
                                                    <asp:ListItem Text="--Category Level 2--" Value="0"></asp:ListItem>
                                                </asp:DropDownList>

                                            </td>
                                        </tr>

                                        <tr id="catLevel3" runat="server">
                                            <td class="Titles">
                                                Category Level 3:</td>
                                            <td>
                                                <asp:DropDownList ID="ddlCategoryLevel3" runat="server" AutoPostBack="True" onselectedindexchanged="ddlCategoryLevel3_SelectedIndexChanged">
                                                    <asp:ListItem Text="--Category Level 3--" Value="0"></asp:ListItem>
                                                </asp:DropDownList>

                                                <span style="color:#D9395B;font-weight:normal">   <%=m_sSubMessage %></span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="Titles">
                                                IsActive:</td>
                                            <td>
                                                <asp:CheckBox ID="chkActive" Text="IsActive" runat="server" oncheckedchanged="chkActive_CheckedChanged" AutoPostBack="true" />
                                            </td>
                                        </tr>

                                        <%--  <tr>

                                        <td style="text-align:right" class="Titles">
                                     InActive:</td>
                                 <td>
                                       <asp:CheckBox ID="chkInActive" Text="InActive" runat ="server"/>
                                 </td>
                             </tr>--%>
                                            <tr>
                                                <td colspan="100%">
                                                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Enter ProductName to Search" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged"></asp:TextBox>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="100%">

                                                    <table width="100%">
                                                        <tr>
                                                            <td align="center">

                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Button ID="btnPrev2" OnClick="btnPrev_Click" runat="server" CssClass="btn btn-primary btn-small" Text="Prev" Visible="false" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Literal ID="ltMsg2" runat="server"></asp:Literal>

                                                                        </td>

                                                                        <td>

                                                                            <asp:Button ID="btnNext2" OnClick="btnNext_Click" runat="server" Text="Next" CssClass="btn btn-primary btn-small" Visible="false" />

                                                                        </td>

                                                                    </tr>

                                                                </table>

                                                            </td>

                                                        </tr>

                                                        <tr>

                                                            <td>

                                                                <asp:Repeater ID="repProducts" runat="server" onitemcommand="repProducts_ItemCommand" onitemdatabound="repProducts_ItemDataBound">

                                                                    <ItemTemplate>
                                                                        <div class="product_main">
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:HiddenField ID="hdnRepProductId" runat="server" Value='<%#Eval("ProductId") %>' />
                                                                                        <asp:HiddenField ID="hdnalbumID" runat="server" Value='<%# Eval("SubCategoryId") %>' />
                                                                                        <img src="../ProductImages/T_<%#Eval(" PhotoUrl ") %>" height="125px" width="125px" style="border:solid 5px silver" />
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <asp:Label ID="lblCName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                                        <asp:HiddenField ID="hdnLangTitle" runat="server" Value='<%# Eval("ShortName") %>' />
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <asp:LinkButton runat="server" ID="Lnkedit" CausesValidation="false" ForeColor="Black" Font-Bold="false" CommandName="edit">
                                                                                            Edit
                                                                                        </asp:LinkButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>

                                                                        </div>

                                                                    </ItemTemplate>

                                                                </asp:Repeater>

                                                            </td>

                                                        </tr>

                                                        <tr>
                                                            <td align="center">

                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Button ID="btnPrev" OnClick="btnPrev_Click" CssClass="btn btn-primary btn-small" runat="server" Text="Prev" Visible="false" />
                                                                        </td>

                                                                        <td>
                                                                            <asp:Literal ID="ltMsg" runat="server"></asp:Literal>

                                                                        </td>

                                                                        <td>

                                                                            <asp:Button ID="btnNext" OnClick="btnNext_Click" runat="server" Text="Next" CssClass="btn btn-primary btn-small" Visible="false" />

                                                                        </td>

                                                                    </tr>

                                                                </table>
                                                            </td>

                                                        </tr>

                                                    </table>

                                                </td>
                                            </tr>
                                    </table>

                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>

                    </div>

                    </div>                  

                </div>
            </form>

        </asp:Content>