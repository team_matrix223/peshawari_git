﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="categorylevel3.aspx.cs" Inherits="backoffice_categorylevel3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/master.css" rel="stylesheet" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">
    var m_SubCategoryId = 0;




    function BindSubCategories(sid) {
      
      
        $("#<%=ddlSubCategories.ClientID %>").html('');

        if (sid == 0) {

            return;
        }
      
        $.uiLock('');
        var CatId = sid;

        $.ajax({
            type: "POST",
            data: '{ "CatId": "' + CatId + '"}',
            url: "categorylevel3.aspx/BindSubCategories",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#<%=ddlSubCategories.ClientID %>").html(obj.SubCatOptions);

            },
            complete: function (msg) {
                $.uiUnlock();

            }

        });


    }


    function ResetControls() {
        m_SubCategoryId = 0;
        var txtTitle = $("#txtTitle");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
       
        txtTitle.focus();
        txtTitle.val("");
        txtTitle.focus();
        btnAdd.css({ "display": "block" });
        btnAdd.html("Add Sub Category");
        $("#txtDescription").val("");

        $("#txtMetaTitle").val("");
        $("#txtMetaDescription").val("");
        $("#txtMetaKeyword").val("");

        btnUpdate.css({ "display": "none" });
        btnUpdate.html("Update Sub Category");

        $("#chkIsActive").prop("checked", "checked");

        $("#btnReset").css({ "display": "none" });

        $("#hdnId").val("0");
        validateForm("detach");
    }



    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {

        if (!validateForm("formID")) {
            return;
        }

        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");

        var Id = m_SubCategoryId;

        var MetaTitle = $("#txtMetaTitle").val();
        var MetaKeyword = $("#txtMetaKeyword").val();
        var MetaDescription = $("#txtMetaDescription").val();


        //alert(Id);
        // return;
        var Title = $("#txtTitle").val();
        if ($.trim(Title) == "") {
            $("#txtTitle").focus();

            return;
        }
        var Description = $("#txtDescription").val();
        var CategoryId = $("#<%=ddlSubCategories.ClientID%>").val();
        var CatTitle = $("#<%=ddlSubCategories.ClientID%> option:selected").text();

        $.uiLock('');
        if (Id == "0") {
            btnAdd.unbind('click');
        }
        else {
            btnUpdate.unbind('click');
        }

        btnAdd.html("<img src='images/loader.gif' alt='loading...'/>")
        btnUpdate.html("<img src='images/loader.gif' alt='loading...'/>")

        var IsActive = false;


        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
        }


        $.ajax({
            type: "POST",
            data: '{"id":"' + Id + '", "title": "' + Title + '","isActive": "' + IsActive + '","CatId": "' + CategoryId + '","desc": "' + Description + '","CatTitle": "' + CatTitle + '","MetaTitle": "' + MetaTitle + '","MetaKeyword": "' + MetaKeyword + '","MetaDescription": "' + MetaDescription + '"}',
            url: "categorylevel3.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == -1) {
                    ResetControls();


                    if (Id == "0") {
                        btnAdd.bind('click', function () { InsertUpdate(); });
                    }
                    else {

                        btnUpdate.bind('click', function () { InsertUpdate(); });
                    }

                    alert("Insertion Failed.SubCategory with duplicate name already exists.");
                    return;

                }

                if (Id == "0") {
                    btnAdd.bind('click', function () { InsertUpdate(); });
                    jQuery("#jQGridDemo").jqGrid('addRowData', obj.Role.RoleId, obj.Role, "last");

                    ResetControls();

                    alert("SubCategory is added successfully.");

                }
                else {
                    btnUpdate.bind('click', function () { InsertUpdate(); });
                    ResetControls();
                    var myGrid = $("#jQGridDemo");
                    var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


                    myGrid.jqGrid('setRowData', selRowId, obj.Role);

                    alert("SubCategory is Updated successfully.");


                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
                BindGrid();
            }



        });

    }

    $(document).ready(
    function () {



        $("#<%=ddlCategory.ClientID %>").change(
    function () {

        
        BindSubCategories($(this).val());

    }

    );

        $("#<%=ddlSubCategories.ClientID%>").change(function () {
            BindGrid();
        });

        $("#btnAdd").click(
        function () {

            m_SubCategoryId = 0;
            InsertUpdate();
           

        }
        );


        $("#btnUpdate").click(
        function () {
            InsertUpdate();
            

        }
        );







        $('#txtTitle').focus();
        $('#txtTitle').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );


    }
    );

</script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
<div id="content" style="padding:6px">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Category Level 3</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">
                   <table cellpadding="0" cellspacing="0" border="0" class="top_table">
                     <tr><td class="headings">Choose Category:</td><td>  
                     
                     <asp:DropDownList ID="ddlCategory" runat="server" class="inputtxt validate ddlrequired" AutoPostBack="false" 
                             ></asp:DropDownList>
                     </td></tr>

                       <tr><td class="headings">Choose Sub Category:</td><td>  
                     
                     <asp:DropDownList ID="ddlSubCategories" runat="server" class="inputtxt validate ddlrequired"></asp:DropDownList>
                     </td></tr>

                   
                     <tr><td class="headings">Title:</td><td>  <input type="text" name="txtTitle" class="inputtxt validate required"  data-index="1" id="txtTitle" /></td></tr>
                                       
                    <tr><td class="headings">Description:</td><td> 
                    
                    <textarea id="txtDescription" rows="3" cols="26"></textarea>
                      
                     
                     </td></tr>
                     
                    
                     <tr><td class="headings">Is Active:</td><td>     <input type="checkbox" id="chkIsActive" checked=checked data-index="2"  name="chkIsActive" />
                                            </td></tr>


                        </table>

                   <table class="midle_table">
                   <tr>
                   <td colspan="100%"  > 
                    <h3 class="reallynow">
                <span>META INFORMATION FOR SEO</span>
                <br />
            </h3>
                   </td>
                   </tr>

                       <tr>
                        <td class="headings">
                            Meta Title:
                        </td>
                        <td>
                            <input type="text" name="txtMetaTitle" class="inputtxt validate required"
                                data-index="1" id="txtMetaTitle" />
                        </td>
                    </tr>


                      <tr>
                        <td class="headings">
                            Meta Keyword:
                        </td>
                        <td>
                            <input type="text" name="txtMetaKeyword" class="inputtxt validate required"
                                data-index="1" id="txtMetaKeyword" />
                        </td>
                    </tr>


                      <tr>
                        <td class="headings">
                            Meta Description:
                        </td>
                        <td>
                            <input type="text" name="txtMetaDescription" class="inputtxt validate required"
                                data-index="1" id="txtMetaDescription" />
                        </td>
                    </tr>

                   </table>

                                            <table cellspacing="0" cellpadding="0" class="category_table">
                                            <tr>
                                            <td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add Category Level 3</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" >Update Category Level 3</div></td>
                                            <td><div id="btnReset"  class="btn btn-primary btn-small" style="display:none;width:60px;" >Cancel</div></td>
                                            </tr>
                                            </table>

       
                    </div>
			  </div>
               


               <div id="rightnow" style="margin-top:10px">
                    <h3 class="reallynow">
                        <span>Manage  Category Level 3 </span>
                      
                        <br />
                    </h3>
				    <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>
			  </div>

            </div>
</form>

            <script type="text/javascript">
                function BindGrid() {

              
                    var CatId = $("#<%=ddlSubCategories.ClientID%>").val();
                    jQuery("#jQGridDemo").GridUnload();
        jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/ManageSubCategories.ashx?CatId=' + CatId,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['SubCategory Id', 'Category Id', 'Title','Meta Title','Meta Keyword','Meta Description', 'Description', 'Category', 'Is Active'],
            colModel: [
                        { name: 'SubCategoryId', key: true, index: 'SubCategoryId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
   		                { name: 'CategoryId', index: 'CategoryId', width: 100, stype: 'text',sorttype:'int',hidden:true },
   		                
                        { name: 'Title', index: 'Title', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		               { name: 'MetaTitle', index: 'MetaTitle', width: 170, stype: 'text', sortable: true, editable: true ,editrules: { required: true ,hidden:true}},
                        { name: 'MetaKeyword', index: 'MetaKeyword', width: 170, stype: 'text', sortable: true, editable: true ,editrules: { required: true  ,hidden:true}},
                        { name: 'MetaDescription', index: 'MetaDescription', width: 170, stype: 'text', sortable: true, editable: true ,editrules: { required: true ,hidden:true }},
                       
                        { name: 'Description', index: 'Description', width: 150, stype: 'text', sortable: true,hidden:true, editable: true ,editrules: { required: true },hidden:true},
   		               
   		             { name: 'CategoryName', index: 'CategoryName', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		             

                        { name: 'IsActive', index: 'IsActive', width: 150, editable: true,edittype:"checkbox",editoptions: { value:"true:false" } , formatter: "checkbox", formatoptions: { disabled: true} },
 
   		                  
                       ],
            rowNum: 10,
          
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'SubCategoryId',
            viewrecords: true,
            height: "100%",
            width:"400px",
            sortorder: 'asc',
            caption: "SubCategory List",
         
            editurl: 'handlers/ManageSubCategories.ashx',
         
                    
             
        });




        $("#jQGridDemo").jqGrid('setGridParam',
{
    onSelectRow: function (rowid, iRow, iCol, e) {
        m_SubCategoryId = 0;
        validateForm("detach");
        var txtTitle = $("#txtTitle");
        m_SubCategoryId = $('#jQGridDemo').jqGrid('getCell', rowid, 'CategoryId');


        if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
            $('#chkIsActive').prop('checked', true);
        }
        else {
            $('#chkIsActive').prop('checked', false);

        }
          $("#txtMetaTitle").val($('#jQGridDemo').jqGrid('getCell', rowid, 'MetaTitle'))
                 $("#txtMetaDescription").val($('#jQGridDemo').jqGrid('getCell', rowid, 'MetaDescription'))
                 $("#txtMetaKeyword").val($('#jQGridDemo').jqGrid('getCell', rowid, 'MetaKeyword'))


        txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Title'));

        $("#txtDescription").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Description'));

        var CatId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ParentId');

        $("#<%=ddlCategory.ClientID%> option[value='" + CatId + "']").prop("selected", true);


               txtTitle.focus();
               $("#btnAdd").css({ "display": "none" });
               $("#btnUpdate").css({ "display": "block" });
               $("#btnReset").css({ "display": "block" });
               TakeMeTop();
             }
         });

       var DataGrid = jQuery('#jQGridDemo');
       DataGrid.jqGrid('setGridWidth', '500');

       $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                        {
                            refresh: false,
                            edit: false,
                            add: false,
                            del: false,
                            search: false,
                            searchtext: "Search",
                            addtext: "Add",
                        },

                        {//SEARCH
                            closeOnEscape: true

                        }

                          );



}
 
      

                   
              
    </script>

</asp:Content>

