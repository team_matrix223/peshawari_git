﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin : System.Web.UI.MasterPage
{
    public string m_notify { get; set; }
    public string m_notifybirth { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session[Constants.AdminId] != null)

            lblusername.InnerText = "Welcome  " + Session[Constants.AdminId].ToString();

        else

           // Response.Redirect("login.aspx");

        if (Session["Roles"] == null)
        {
            //Response.Redirect("login.aspx");
        }


        Int16 Count = 0;
        Count = new NotificationBLL().CountUnreadNotification();
      //hdnnotify.Value = Convert.ToString(Count);
        m_notify = Convert.ToString(Count);
        Int16 CountBirthday = 0;
        CountBirthday = new NotificationBLL().CountUnreadNotificationBirthDay();
       // hdnnotifybirthday.Value = Convert.ToString(CountBirthday);
        m_notifybirth = Convert.ToString(CountBirthday);

        if (!IsPostBack)
        {
            rep.DataSource = new NotificationBLL().GetNotificationOrders();
            rep.DataBind();
            repbirth.DataSource = new NotificationBLL().GetNotificationBirthdayAnniversary();
            repbirth.DataBind();




        }



    }
}
