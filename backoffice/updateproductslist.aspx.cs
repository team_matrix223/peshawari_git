﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_updateproductslist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnDeleteProduct_Click(object sender, EventArgs e)
    {
        if (ddlMode.Text.Trim() == "Product")
        {
            if (txtSearch.Text.Trim() == "")
            {
                Response.Write("<script>alert('Enter ProductId');</script>");
                txtSearch.Focus();
                return;
            }
            int status = new ProductsDAL().DeleteProductById(Convert.ToInt64(txtSearch.Text));
            if (status == 0)
            {
                Response.Write("<script>alert('Product Deleteion failed,Plz try again');</script>");
            }
            else
            {
                Response.Write("<script>alert('Product is deleted');</script>");
                txtSearch.Text = "";

            }
        }
    }
    protected void btnClearImage_Click(object sender, EventArgs e)
    {
        string mode = ddlMode.Text.Trim();
        if (txtSearch.Text.Trim() == "" )
        {
            if (mode == "Product")
            {
                Response.Write("<script>alert('Enter ProductId');</script>");
            }
            else
            {
                Response.Write("<script>alert('Enter VariationId');</script>");
                
            }
            txtSearch.Focus();
            return;
        }

        int status=0;
        if (mode == "Product")
        {
            status = new ProductsDAL().DeletePhotoById(Convert.ToInt64(txtSearch.Text),mode );
        }
        else
        {
            status = new ProductsDAL().DeletePhotoById(Convert.ToInt64(txtSearch.Text), mode);
        }
        
        if (status == 0)
        {
            Response.Write("<script>alert('Image Deleteion failed,Plz try again');</script>");
        }
        else
        {
            Response.Write("<script>alert('Image is deleted');</script>");
            txtSearch.Text = "";

        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtSearch.Text = "";
    }
}