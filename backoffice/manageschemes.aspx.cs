﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_manageschemes : System.Web.UI.Page
{
   public static  DataTable dtMaster = new DataTable();
     public static DataTable dtFree = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (!User.IsInRole("Schemes"))
            {
                Response.Redirect("default.aspx");

            }
            dtMaster.Columns.Clear();
            dtMaster.Columns.Add("VariationId",typeof(Int32));
            dtFree.Columns.Clear();
            dtFree.Columns.Add("VariationId", typeof(Int32));
        }
    }
    [WebMethod]
    public static string CheckAllReadyExistId(int VariationId)
    {
       ;

       Int16  status = new SchemeBLL().CheckAllReadyExistId(VariationId );
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
         Status=status
        };
        return ser.Serialize(JsonData);

    }
    [WebMethod]
    public static string GetProductDetail(int SchemeId)
    {
        Schemes objScheme = new Schemes() { SchemeId = SchemeId };

        var MasterProductDetail = new SchemeBLL().GetMasterProductDetailByID(objScheme);
        var FreeProductDetail = new SchemeBLL().GetFreeProductDetailByID(objScheme);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            MasterData = MasterProductDetail,
            FreeData = FreeProductDetail
        };
        return ser.Serialize(JsonData);

    }
    [WebMethod]
    public static string Insert(Int32 SchemeId, string Title, string Description, string PhotoUrl, int Qty, bool PackScheme, DateTime StartDate, DateTime EndDate, bool IsActive, string VariationIdArrM, string VariationIdArrF)
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();
        Schemes objScheme = new Schemes()
        {
            SchemeId =SchemeId,
            Title=Title,
            Description=Description,
            PhotoUrl=PhotoUrl,
            Qty=Qty,
            PackScheme=PackScheme,
            StartDate=StartDate,
            EndDate=EndDate,
            IsActive=IsActive
        };
        string[] masterArr = VariationIdArrM.ToString().Split(',');
        dtMaster.Rows.Clear();
        dtFree.Rows.Clear();
        for (int i = 0; i < masterArr.Length; i++)
        {
            dtMaster.Rows.Add();
            dtMaster.Rows[dtMaster.Rows.Count - 1]["VariationId"] = masterArr[i].ToString();
        }
        string[] freeArr = VariationIdArrF.ToString().Split(',');
        for (int i = 0; i < freeArr.Length; i++)
        {
            dtFree.Rows.Add();
            dtFree.Rows[dtFree.Rows.Count - 1]["VariationId"] = freeArr[i].ToString();
        }
        Int32 status = new SchemeBLL().InsertUpdate(objScheme,dtMaster,dtFree);
        var Json = new
        {
            Status=status
        };
        return ser.Serialize(Json);
    }
}