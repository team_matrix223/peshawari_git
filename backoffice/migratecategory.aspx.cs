﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class backoffice_migratecategory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (!User.IsInRole("MigrateCategory"))
            {
                Response.Redirect("default.aspx");

            }
            BindDepartments();
        }
    }
    void BindDepartments()
    {
        ddlDepartments.DataSource = new MigrationDAL().GetAllDepartments();
        ddlDepartments.DataTextField = "PROP_NAME";
        ddlDepartments.DataValueField = "PROP_NAME";
        ddlDepartments.DataBind();
        ddlDepartments.Items.Insert(0, "");
    }
    protected void ddlDepartments_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlList.Items.Clear();
        DataSet ds = new MigrationDAL().GetGroupsByDepartment(ddlDepartments.SelectedItem.Value);

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            
            ListItem li = new ListItem();
            li.Text = ds.Tables[0].Rows[i]["Group_Name"].ToString();
            li.Value = ds.Tables[0].Rows[i]["Group_ID"].ToString();
            if (ds.Tables[0].Rows[i]["IsSelected"].ToString() == "1")
            {
                li.Selected = true;
            }
            ddlList.Items.Add(li);


        }

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        DataTable dt = new DataTable();
        dt.Columns.Add("Code");
        dt.Columns.Add("Name");


        int[] selectedIndices = ddlList.GetSelectedIndices();

        for (int i = 0; i < selectedIndices.Length; i++)
        {

            DataRow dr = dt.NewRow();
            ListItem li = ddlList.Items[selectedIndices[i]];
            dr["Code"] = li.Value;
            dr["Name"] = li.Text;
            dt.Rows.Add(dr);

        }

       int status= new MigrationDAL().InsertUpdateDepartmentGroups(ddlDepartments.SelectedValue, dt);
       if (status == 1)
       {

           ddlList.Items.Clear();
           ddlDepartments.SelectedIndex = -1;
           Response.Write("<script>alert('Records Updated Successfully')</script>");
       }
       else

       {
           Response.Write("<script>alert('An Error occured during transaction. Please try again later')</script>");
       
       }
    }
    protected void btnAddDepartment_Click(object sender, EventArgs e)
    {
        Common objCommon = new Common()
        {

            Title = txtDepartmentTitle.Value.Trim().ToUpper(),
            Status = "1",
        };
        int status = new CommonBLL().InsertUpdate(objCommon);
        if (status != 0)
        {
            lblMessage.Text = "Department Added Successfully";
            BindDepartments();
            ddlList.Items.Clear();
        }
        else
        {
            lblMessage.Text = "Department Will duplicate name alreay Exists";
        
        }
    }
    protected void btnAddGroup_Click(object sender, EventArgs e)
    {
        Common objCommon = new Common()
        {

            Title =txtGroupTitle.Value.Trim().ToUpper(),
            Status = "2",
        };
        int status = new CommonBLL().InsertUpdate(objCommon);
        if (status != 0)
        {
            BindDepartments();
            ddlList.Items.Clear();
            lblMessage.Text = "Group Added Successfully";
        }
        else
        {
            lblMessage.Text = "Group Will duplicate name alreay Exists";

        }
    }
}