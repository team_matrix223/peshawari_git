﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
 

public partial class Admin_Default4 : System.Web.UI.Page
{
    Cms objCMS = new Cms();
    
    public string Status { get { return Request.QueryString["status"] != null ? Request.QueryString["status"].ToString() : "I"; } }
    protected bool m_bShow = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
                try
                {


                    if (Status == "U")
                    {
                        if (!User.IsInRole("EditParentPages"))
                        {
                            Response.Redirect("default.aspx");

                        }

                        BindControls();

                    }
                    else
                    {
                        if (!User.IsInRole("AddParentPages"))
                        {
                            Response.Redirect("default.aspx");

                        }
                    }
                }
                catch (Exception ex)
                {

                   
                }

            }
        }
        catch (Exception ex)
        {

            
        }
    }
 

    //public void BindBreadCrumbs()
    //{
    //    try
    //    {
    //        if (Status == "U")
    //        {
    //           // Master.PageName = "Update Static Pages";
                
    //        }
    //        else
    //        {
    //           // Master.PageName = "Create Static Pages";
             

    //        }
    //    }
    //    finally
    //    { 
    //    }
    //}


    public void BindFields()
    {
        try
        {
            if (Status == "U")
            {
                objCMS.PageId = Convert.ToInt16(ddlPage.SelectedItem.Value);
                objCMS.GetById();
                objCMS.Title = txtTitle.Text;
                objCMS.IsActive = chkIsActive.Checked;
                objCMS.Description = EditorDescription.Value;

               
                    objCMS.IsFullPage = chkFullPage.Checked;
                
                objCMS.Keywords = txtKeywords.Text;
                objCMS.MetaTitle = txtPageTitle.Text;
                objCMS.PageDescription = txtPageDescription.Text;
                objCMS.ShowOn = Convert.ToInt16(ddlShowOn.SelectedIndex);
                objCMS.PunjabiTitle = "";
                objCMS.PunjabiDescription = "";
                objCMS.InsertUpdate();
                BindControls();
           

                m_bShow = true;
                string script = @"alert('Page Updated Successfully !!');";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "filesDeleted", script, true);
            }
            else
            {

                objCMS.PageId = 0;
                objCMS.IsActive = chkIsActive.Checked;
                objCMS.Description = EditorDescription.Value;
                objCMS.ParentPage = 0;
                objCMS.Title = txtTitle.Text;
                objCMS.IsFullPage = chkFullPage.Checked;
                objCMS.Keywords = txtKeywords.Text;
                objCMS.MetaTitle = txtPageTitle.Text;
                objCMS.PageDescription = txtPageDescription.Text;
                objCMS.ShowOn = Convert.ToInt16(ddlShowOn.SelectedIndex);
                objCMS.PunjabiTitle = "";
                objCMS.PunjabiDescription = "";
                objCMS.InsertUpdate();
                string script = @"alert('Page Created Successfully !!');";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "filesDeleted", script, true);

            }

            ResetControls();
           


        }
        catch (Exception ex)
        { 
       
        }
        finally
        {
        }

    }

    public void BindControls()
    {
        m_bShow = true;
        btnSubmit.Text = "Update Page";
        BindDDL();
    }
    public void BindDDL()
    {
        try
        {
            objCMS.ParentPage = 0;
            ddlPage.DataSource = objCMS.GetByParentId();
            ddlPage.DataTextField = "Title";
            ddlPage.DataValueField = "PageId";
            ddlPage.DataBind();
            ddlPage.Items.Insert(0, "--Select--");

        }
        finally { }

    }


    protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Int16 Id = Convert.ToInt16(ddlPage.SelectedValue);
            
            
            objCMS.PageId = Id;
            objCMS.GetById();
            chkIsActive.Checked = objCMS.IsActive;

            chkFullPage.Checked = objCMS.IsFullPage;
            if (objCMS.IsFullPage == true)
            {
                chkFullPage.Enabled = false;
            }
            
            txtTitle.Text = objCMS.Title;
            txtKeywords.Text = objCMS.Keywords;
            txtPageTitle.Text = objCMS.MetaTitle;
            txtPageDescription.Text = objCMS.PageDescription;
            EditorDescription.Value = objCMS.Description;
            ddlShowOn.SelectedIndex = objCMS.ShowOn;

           // PunjabiDescription.Value = objCMS.PunjabiDescription;
            //txtPunjabiTitle.Text = objCMS.PunjabiTitle;
            m_bShow = true;
        }
        catch (Exception ex)
        {
       
            Response.Redirect("message.aspx");
        }
    }

   
     protected void btnSubmit_Click(object sender, EventArgs e)
     {

         try
         {
             BindFields();

         }
         catch (Exception ex)
         {

             Response.Redirect("message.aspx");
         }
    }

 
     public void ResetControls()
     {
         EditorDescription.Value = "";
         txtTitle.Text = "";
         chkFullPage.Checked = false;
         chkIsActive.Checked = false;
         txtKeywords.Text = "";

         txtPageDescription.Text = "";
         txtPageTitle.Text = "";
  
         btnSubmit.Text = "Add Page";

     }
     
}
