﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="manageAssignEmp.aspx.cs" Inherits="backoffice_manageAssignEmp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">

 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
        <link href="css/custom-css/transaction.css" rel="stylesheet" />

    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

    <script type="text/javascript">
      var BillId = 0;
      var Category = 0;

      var m_BillNo = 0;
      var m_OrderNo = 0;
      var m_LocalBillNo = "";
     


      var OrderCollection = [];
      function clsorder() {
          this.BillNo = 0;
          this.OrderNo = 0;
          this.LocalBillNo = "";
          

      }


        function Reset()
        {

            OrderCollection = [];

            $('#tbOrderInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            $("#tbOrderInfo").append(" <tr><td colspan='100%' align='center'> 	 Orders TO be Dispatched With BillNo will be shown here</td></tr>");
          
            m_BillNo = 0;
            m_OrderNo = 0;
            m_LocalBillNo = "";

           BindGrid();
        }







        $(document).ready(
        function () {

            $("#txtDateFrom").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });


            $("#txtDateTo").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });

            $("#txtDateTo,#txtDateFrom").val($("#<%=hdnDate.ClientID%>").val());
            BindGrid();




            $("#btnGo").click(
        function () {

            BindGrid();

        }
        );





            $("#btnSave").click(
        function () {



            InsertUpdate();


        }
        );

            $(document).on("click", "#dvClose", function (event) {

                var RowIndex = Number($(this).closest('tr').index());
                var tr = $(this).closest("tr");
                tr.remove();

                OrderCollection.splice(RowIndex, 1);

                if (OrderCollection.length == 0) {

                    $("#tbOrderInfo").append(" <tr><td colspan='100%' align='center'> 	 Order To be Dispatched will be shown here</td></tr>");

                }

            });


            function InsertUpdate() {

                if (!validateForm("addDialog")) {

                    return;
                }

                $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

                //                var selRow = jQuery("#jQGridDemo").jqGrid('getGridParam', 'selarrrow');



                //                var Orders = [];
                //                for (i = 0; i < selRow.length; i++) {
                //                    alert(selRow[i]);
                //                    var OrderNo = $('#jQGridDemo').jqGrid('getCell', selRow[i], 'OrderId');
                //                    Orders = Orders + OrderNo + ',';
                //                }
                //                Orders = Orders.slice(0, -1);
                //                alert(Orders);
                //               
                //                BillNo = [];
                //                BillNo = selRow;
                //                if (BillNo.toString().trim() == "") {
                //                    alert("Choose BillNo from BillList");
                //                    $.uiUnlock();
                //                    return;
                //                }

                if (OrderCollection.length == 0) {
                    alert("Please first add a Order");
                    $.uiUnlock();
                    return;

                }

                var LocalBillNo = [];
                var BillNo = [];
                var Orders = [];

                for (var i = 0; i < OrderCollection.length; i++) {


                    BillNo[i] = OrderCollection[i]["BillNo"];
                    Orders[i] = OrderCollection[i]["OrderNo"];
                    if ($("#q_" + OrderCollection[i].BillNo).val() == "")
                     {
                     alert("Enter Local BillNo");
                     $.uiUnlock();
                     return;
                    }
                   else
                    {
                    LocalBillNo[i] = ($("#q_" + OrderCollection[i].BillNo).val());
                    }



                }


                var Emp = $("#<%=ddlemployee.ClientID %>").val();
                if (Emp == "0") {
                    alert("Choose Employee");
                    $("#<%=ddlemployee.ClientID %>").focus();
                    $.uiUnlock();
                    return;
                }

                $.ajax({
                    type: "POST",
                    data: '{"BillIdarr": "' + BillNo + '","EmpId": "' + Emp + '","arrOrderNo": "' + Orders + '","arrLocalBillNo": "' + LocalBillNo + '"}',

                    url: "manageAssignEmp.aspx/Insert",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {


                        var obj = jQuery.parseJSON(msg.d);
                        if (obj.Status == "0") {
                            alert("Updation Failed. Please try again later.");
                            return;
                        }
                        else {
                            alert("Order Assigned to Employee Successfully");
                            Reset();
                            $("#<%=ddlemployee.ClientID %>").val("0");
                        }









                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                        $.uiUnlock();
                    }

                });
            }




        });
    
    </script>



        <form   runat="server" id="formID" method="post">

     <asp:HiddenField ID="hdnDate" runat="server"/>
  
<div id="content">

             <div id="rightnow">
            <h3 class="reallynow">
                <span>Orders To Be Dispatched</span>
                <br />
            </h3>
            <div class="youhave">
              <table class="top_table">
                                <tr><td>Date From:</td><td>
                         
                           <input type="text" readonly="readonly"   class="form-control input-small"  id="txtDateFrom" /></td><td>Date To:</td>
                                    <td><input type="text" readonly="readonly"  class="form-control input-small"    id="txtDateTo" />
                           </td>
                           <td><div id="btnGo"  class="btn btn-primary btn-small"  >Go</div></td>
                           </tr>
                        
                           </table>

                <table id="jQGridDemo">
                </table>
                <div id="jQGridDemoPager">
                </div>

                        <table class="tablesorter item_table" style="margin-top:20px;"  id="tbOrderInfo"  cellspacing="0"> 
			<thead> 
				<tr style="background:#294145;color:white"> 
   					  <th>
                                            BillNo
                                        </th>
                                        <th>
                                           OrderNo
                                        </th>
                                        <th>
                                            LocalBillNo
                                        </th>
                                        <th>Delete</th>
				</tr> 
			</thead> 
			<tbody>
       
       
			</tbody> 
			</table>
               
                
                <table class="top_table">
                
                   <tr><td>Employee:</td><td><asp:DropDownList ID ="ddlemployee" runat="server"></asp:DropDownList></td><td></td><td></td><td></td></tr>
                </table>
           
                

                                            <table class="category_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                             <td> <div id="btnSave"  class="btn btn-primary btn-small" >Save</div></td>
                                            
                                           <%-- <td>&nbsp;</td><td> <div id="btnPrint"  class="btn btn-primary btn-small" >Print</div></td>--%>
                                            </tr>
                                            </table>
            </div>
        </div>
   
            </div>




  
</form>



<script type="text/javascript">
         function BindGrid()
     {
      var DateFrom=$("#txtDateFrom").val();
     var DateTo=$("#txtDateTo").val();
     jQuery("#jQGridDemo").GridUnload();

              jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/manageempassign.ashx?dateFrom='+DateFrom+'&dateTo='+DateTo,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['BillNo','OrderId','Date', 'Name', 'Recipient','Address','Mobile','BillValue','CustomerId','RecipientFirstName','RecipientLastName','RecipientMobile','RecipientPhone','City','Area','Street','Address','Pincode','BillValue','DisPer','DisAmt','ServiceTaxPer','ServiceTaxAmt','ServiceChargePer','ServiceChargeAmt','VatPer','VatAmt','NetAmount','Remarks','ExecutiveId','IPAddress'],
            colModel: [
              { name: 'BillId',key:true, index: 'BillId', width: 100, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:false },
                        { name: 'OrderId', index: 'OrderId', width: 150, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},
                        { name: 'strBD', index: 'strBD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},
   		                { name: 'CustomerName', index: 'CustomerName', width: 150, stype: 'text', sortable: true,hidden:false, hidden: false, editable: true ,editrules: { required: true }},
                        { name: 'Recipient', index: 'Recipient', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'CompleteAddress', index: 'CompleteAddress', width: 200, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                        { name: 'RecipientMobile', index: 'RecipientMobile', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
   		                { name: 'BillValue', index: 'BillValue', width: 100, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                        { name: 'CustomerId',key:true, index: 'CustomerId', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                        { name: 'RecipientFirstName', index: 'RecipientFirstName', width: 150, stype: 'text', sortable: true, hidden: true, editable: true ,editrules: { required: true }},
   		                { name: 'RecipientLastName', index: 'RecipientLastName', width: 150, stype: 'text', sortable: true,hidden:false, hidden: true, editable: true ,editrules: { required: true }},
                        { name: 'RecipientMobile', index: 'RecipientMobile', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'RecipientPhone', index: 'RecipientPhone', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},   
   		                { name: 'City', index: 'City', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'Area', index: 'Area', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'Street',key:true, index: 'Street', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                        { name: 'Address', index: 'Address', width: 150, stype: 'text', sortable: true, hidden: true, editable: true ,editrules: { required: true }},
   		                { name: 'Pincode', index: 'Pincode', width: 150, stype: 'text', sortable: true,hidden:false, hidden: true, editable: true ,editrules: { required: true }},
                        { name: 'BillValue', index: 'BillValue', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'DisPer', index: 'DisPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'DisAmt', index: 'DisAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
   		                { name: 'ServiceTaxPer', index: 'ServiceTaxPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'ServiceTaxAmt', index: 'ServiceTaxAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'ServiceChargePer', index: 'ServiceChargePer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'ServiceChargeAmt', index: 'ServiceChargeAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'VatPer', index: 'VatPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'VatAmt', index: 'VatAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
   		                { name: 'NetAmount', index: 'NetAmount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'Remarks', index: 'Remarks', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'ExecutiveId', index: 'ExecutiveId', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'IPAddress', index: 'IPAddress', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                      
                      
                       ],
            rowNum: 10,
           toolbar: [true, "top"],
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'BillId',
            viewrecords: true,
            height: "100%",
            width:"400px",
            sortorder: 'desc',
            caption: "Orders For Dispatch",
         
           ignoreCase: true,
                    
             
        });

        var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });


        $("#jQGridDemo").jqGrid('setGridParam',
         {

             onSelectRow: function (rowid, iRow, iCol, e) {
          
               m_BillNo = $('#jQGridDemo').jqGrid('getCell', rowid, 'BillId');
             
                m_OrderNo =$('#jQGridDemo').jqGrid('getCell',rowid, 'OrderId');
                var item = $.grep(OrderCollection, function (item) {
                    return item.BillNo == m_BillNo;
                });

                if (item.length) {

                    alert("This Order added in the list");
                    return;

                }
          
               

                var tr = "<tr><td >" + m_BillNo + "</td><td >" + m_OrderNo + "</td><td ><input type='text' id='q_" + m_BillNo + "' /></td> <td><i id='dvClose'><img src='images/trash.png'/></i> </td></tr>";

                $("#tbOrderInfo").append(tr);


                TO = new clsorder();
               
                TO.BillNo = m_BillNo;
                TO.OrderNo = m_OrderNo;
                

                OrderCollection.push(TO);
              

             }
         });






           $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }

                     );
         
    
        var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '686');





    



        

      }
        
    </script>
</asp:Content>

