﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="managequicklist.aspx.cs" Inherits="backoffice_managequicklist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">


 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
            <link href="css/custom-css/transaction.css" rel="stylesheet" />

    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
     <script type="text/javascript">
         var ListId = 0;
          var Total  = 0;
           var ProductCollection = [];
        function clsProduct() {
            this.ProductId = 0;
            this.ProductName = 0;
            this.Qty = 0;
            this.Price = 0;
            this.Url = 0;
            this.VariationId = 0;
            this.ProductDescription="";
            this.SubTotal =0;
        }


            function addToList() {

                 var Price = 0;

            var SelectedRow = jQuery('#jQGridDemoProduct').jqGrid('getGridParam', 'selrow');
            if ($.trim(SelectedRow) == "") {
                alert("No Product is selected to add");
                return;
            }

            var ProductId =   $('#jQGridDemoProduct').jqGrid('getCell', SelectedRow, 'ProductId');
            var VariationId = $('#jQGridDemoProduct').jqGrid('getCell', SelectedRow, 'VariationId');

            var ProductName = $('#jQGridDemoProduct').jqGrid('getCell', SelectedRow, 'Name');
            var Url = $('#jQGridDemoProduct').jqGrid('getCell', SelectedRow, 'PhotoUrl');
            var Qty = $("#txtQty").val();
            Price = $('#jQGridDemoProduct').jqGrid('getCell', SelectedRow, 'Price');

          
       

        var TO = new clsProduct();

         TO.ProductId =   ProductId;
         TO.ProductName = ProductName
         TO.VariationId = VariationId;
         TO.Qty = Qty;
         TO.Price = Price;
         TO.Url = Url;
        
        
     
         ProductCollection.push(TO);
         Bindtr();

        }
        



        function Bindtr()  {
        Total = 0;
         $('#tborderProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove(); 
            var DeliveryCharges=0;
                        var DisAmt=0;
                        var tr = "";
                       

                       

            for (var i = 0; i < ProductCollection.length; i++) {
                               var Amount = Number(ProductCollection[i]["Qty"] * ProductCollection[i]["Price"]);
                             
                              var Amount = Number(ProductCollection[i]["Qty"] * ProductCollection[i]["Price"]) ; 
                           var  tr = tr + "<tr><td><img src='../ProductImages/T_" + ProductCollection[i]["Url"] + "' style='width:50px;height:50px'/></td><td>" + ProductCollection[i]["ProductName"] + "</td><td><div id='btnMinus'  class='btn btn-primary btn-small' style='height:6px'>-</div></td><td><input type='text' style ='width:35px' id='q_" + ProductCollection[i]["ProductId"] + "'  value =" + ProductCollection[i]["Qty"] + " readonly='readonly' /></td> <td><div id='btnPlus'  class='btn btn-primary btn-small' style='height:6px'>+</div></td><td>" + ProductCollection[i]["Price"] + "</td><td>" + Amount + "</td><td><i id='dvClose'><img src='images/trash.png'/></i> </td></tr>";
                                Total = Number(Total) + Number(Amount);
                             
                         
                                        
                      }
                       $("div[id='dvsbtotal']").html(" Rs. " + Total);
                    
                     $("#tborderProducts").append(tr);


                     var dialogDiv = $('#dvProductOptions');
                    dialogDiv.dialog("option", "position", [500, 200]);
                    dialogDiv.dialog('close');
        }

         

 
  



       $(document).on("click", "#btnPlus", function (event) {
    

         var RowIndex = Number($(this).closest('tr').index());
         var PId = ProductCollection[RowIndex]["ItemId"];

         var Mode = "Plus";

         var Qty = ProductCollection[RowIndex]["Qty"];
         var Price = ProductCollection[RowIndex]["Price"];
         var fQty = 0;
         if(Qty < 0)
         {
           fQty = Number(Qty) + (-1);
         }
         else
         {
           fQty = Number(Qty) + 1;
         }




         ProductCollection[RowIndex]["Qty"] = fQty;
         Bindtr();
      
    });


     $(document).on("click", "#btnMinus", function (event) {

         var RowIndex = Number($(this).closest('tr').index());
         var PId = ProductCollection[RowIndex]["ItemId"];
         var Mode = "Minus";

         var Qty = ProductCollection[RowIndex]["Qty"];
         var Price = ProductCollection[RowIndex]["Price"];

        var fQty = 0;
         if(Qty < 0)
         {
          fQty =Number(Qty) - (-1);
         }
         else
         {
           fQty =Number(Qty) - 1;
         }

         ProductCollection[RowIndex]["Qty"] = fQty;
         if (fQty == "0") {
             ProductCollection.splice(RowIndex, 1);
         }

         Bindtr();


     });
     function BindProducts(Category)
     {
           
            jQuery("#jQGridDemoProduct").GridUnload();

              jQuery("#jQGridDemoProduct").jqGrid({
            url: 'handlers/ProductsByCategory.ashx?Category='+Category,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
          colNames: ['VariationId','CategoryId','ProductId','ProductName','Description','ShortName','Price','Photo'],
            colModel: [

              { name: 'VariationId', key: true, index: 'VariationId', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
              { name: 'CategoryId',key:true, index: 'CategoryId', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
              { name: 'ProductId',key:true, index: 'ProductId', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
              { name: 'Name', index: 'Name', width: 100, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},
              { name: 'Description',key:true, index: 'Description', width: 100, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
              { name: 'ShortName', index: 'ShortName', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
              { name: 'Price', index: 'Price', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true },hidden:true },
              { name: 'PhotoUrl', index: 'PhotoUrl', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true },hidden:true },
                         ],

            rowNum: 10,
          
            mtype: 'GET',
               toolbar: [true, "top"],
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPagerProduct',
            sortname: 'ProductId',
            viewrecords: true,
            height: "100%",
            width:"300px",
            sortorder: 'desc',
         ignoreCase: true,
            caption: "Products List",
            
        });


          var $grid = $("#jQGridDemoProduct");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });

         
            $('#jQGridDemoProduct').jqGrid('navGrid', '#jQGridDemoPagerProduct',
                       {
                           refresh: false,
                           edit: false,
                           add: false,
                           del: false,
                           search: true,
                           searchtext: "Search",
                           addtext: "Add",
                       },

                       {//SEARCH
                           closeOnEscape: true

                       }

                         );



         $("#jQGridDemoProduct").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {
             var ProductId = $('#jQGridDemoProduct').jqGrid('getCell', rowid, 'ProductId');                  
             var VariationId = $('#jQGridDemoProduct').jqGrid('getCell', rowid, 'VariationId');
             var item = $.grep(ProductCollection, function (item) {
             return item.VariationId == VariationId;
             });
             if (item.length) {
                 $("#txtQty").val("1");
              
                 return;
             }
             else {
                 $('#dvProductOptions').dialog(
               {
                   autoOpen: false,
                   width: 300,
                   height: 150,
                   resizable: false,
                   modal: true,
               });
                 linkObj = $(this);
                 var dialogDiv = $('#dvProductOptions');
                 dialogDiv.dialog("option", "position", [500, 200]);
                 dialogDiv.dialog('open');
                 return false;
             }
            }
         });

   
  
        var DataGrid = jQuery('#jQGridDemoProduct');
        DataGrid.jqGrid('setGridWidth', '400');
      }


        function Reset()
        {
        
          $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove(); 
          $('#tborderProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove(); 
         
          $("#dvListno").html('');
          $("#dvListDate").html('');
           $("#dvDeliveryDate").html('');
          $("#dvCustomer").html('');
          $("#dvAddress").html('');
          
          $("#dvRecepient").html('');
        
          $("#dvDeliverySlot").html('');
         $("div[id='dvsbtotal']").html('');
          var dialogDiv = $('#dvPopup');
          dialogDiv.dialog("option", "position", [500, 200]);
          dialogDiv.dialog('close');
          BindGrid();
        }


         function BindListProducts() {


             var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             $("#dvListno").html($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'ListId'));
             $("#dvListDate").html($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'strOD'));
             $("#dvCustomer").html($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'UserName'));            
             $("#dvDeliveryDate").html($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'strDD'));

             $("#dvRecepient").html($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Recipient'));

             $("#dvAddress").html($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'CompleteAddress'));
             $("#dvDeliverySlot").html($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Slot'));


             $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
             ListId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'ListId');
            
             $.ajax({
                 type: "POST",
                 data: '{ "Lid": "' + ListId + '"}',
                 url: "managequicklist.aspx/GetListProducts",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {



                     var obj = jQuery.parseJSON(msg.d);

                     var tr = "";
                    
                     for (var i = 0; i < obj.ListProducts.length; i++) {

                         

                         tr = tr + "<tr><td style='text-align:center' >" + obj.ListProducts[i]["ProductName"] + "</td><td style='text-align:center'>" + obj.ListProducts[i]["Qty"] + "</td><td style='text-align:center'>" + obj.ListProducts[i]["Unit"] + "</td></tr>";

                     }


                     $("#tbProductInfo").append(tr);

                 },
                 complete: function (msg) {


                 }

             });
         }



         function InsertUpdate() 
         {

           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');  
            var CustomerId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'UserId');
            var OrderDate =  $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'strOD');
            var BillValue = Total;
            var DeliveryAddressId =  $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'DeliveryAddressId');
            var DeliveryDate = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'strDD');
            var DeliverySlot = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'DeliverySlot');
  


             var ProductId = [];
                var ProductName = [];
                var Qty = [];
                var Price = [];
                var VariationId = [];
               


                if (ProductCollection.length == 0) {
                    alert("Please first Select Products For Order");
                 
                    return;
                }

                for (var i = 0; i < ProductCollection.length; i++) {

                    ProductId[i] = ProductCollection[i]["ProductId"];
                    ProductName[i] = ProductCollection[i]["ProductName"];
                    Qty[i] = ProductCollection[i]["Qty"];
                    Price[i] = ProductCollection[i]["Price"];
                    VariationId[i] = ProductCollection[i]["VariationId"];
                   
                
                }

              

                  $.ajax({
                    type: "POST",
                    data: '{ "ListId": "' + ListId + '", "CustomerId": "' + CustomerId + '","OrderDate": "' + OrderDate + '","BillValue": "' + BillValue + '","DeliveryAddressId": "' + DeliveryAddressId + '","DeliveryDate": "' + DeliveryDate + '","DeliverySlot": "' + DeliverySlot + '","productidArr": "' + ProductId + '","productnameArr": "' + ProductName + '","qtyArr": "' + Qty + '","priceArr": "' + Price + '","variationArr": "' + VariationId + '"}',
                    url: "managequicklist.aspx/InsertUpdate",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
          
                        alert("Order Placed Successfully");
                        Reset();

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                        $.uiUnlock();
                    }

                });

           
         }




         $(document).ready(
        function () {

            $("#txtDateFrom").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });


            $("#txtDateTo").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });

            $("#txtDateTo,#txtDateFrom").val($("#<%=hdnDate.ClientID%>").val());
            BindGrid();


             $(document).on("click", "#dvClose", function (event) {

                var RowIndex = Number($(this).closest('tr').index());
                var tr = $(this).closest("tr");
                tr.remove();

                ProductCollection.splice(RowIndex, 1);

                if (ProductCollection.length == 0) {



                }
                Bindtr();

            });




             $("#btnAdd").click(
        function () {

     
           
            if ($.trim($("#txtQty").val()) == "") {
                alert("Enter Product Qty");
                return;
            }
           
            addToList();

        }
        );

        
            $("#btnCancel").click(
        function () {

          Reset();
        }
        );


            $("#btnGo").click(
        function () {

            BindGrid();

        }
        );


        $("#btnSave").click(
        function()  {
       
         InsertUpdate();

        }

        );




        $("#<%=ddlCategories.ClientID%>").change(function () {

            if ($(this).val() == "0") {

              
                Category = 0;
                return;
            }

            Category = $("#<%=ddlCategories.ClientID %>").val();

            BindProducts(Category);
        });




        });
     
     </script>



         <form   runat="server" id="formID" method="post">

     <asp:HiddenField ID="hdnDate" runat="server"/>
  
<div id="content">

     <div id="rightnow">
            <h3 class="reallynow">
                <span>Quick Lists</span>
                <br />
            </h3>
            <div class="youhave">
           <div id="dvMessage" style="width:100%;text-align:center">
          <b> Quick List Order Information Will be Displayed Here.</b>
            </div>
            <div id="dvPopup" style="display:none">
  <table width="100%" cellpadding="3" >
                    

                     <tr>
                     <td valign="top">
                     <table style="width:100%">
                     <tr>
                     <td valign="top">
                     <table  cellpadding="0" style="border:solid 1px silver; width:100%;height:172px">
                         <tr style="background-color:#E6E6E6;font-weight:bold;height:10px">
                         <td colspan="100%">List Information</td>
                         </tr>
                         
                    <tr>
                    <td colspan="100%" valign="top">
                     

                    <table cellpadding ="5">
                      <tr>
                     <td align="right">List No:</td><td><div id="dvListno"><asp:Literal ID="ltListNo" runat ="server"></asp:Literal></div></td>
                     </tr>
                     <tr>
                     <td align="right">List Date:</td><td><div id="dvListDate"><asp:Literal ID="ltListDate" runat ="server"></asp:Literal></div></td>
       
                     </tr>
                   

                     <tr>
                      <td align="right">Customer Name:</td><td><div id ="dvCustomer"><asp:Literal ID="ltCustomer" runat ="server"></asp:Literal></div></td>
                       
                       </tr>
                      <tr>
                     <td align="right">Delivery Date:</td><td><div id="dvDeliveryDate"><asp:Literal ID="ltDeliveryDate" runat ="server"></asp:Literal></div></td>
       
                     </tr>
                      </table>

                      </td>
   
                     </tr>
                    
                     

                    
                    
                    
                    
                    </table>
                    
                    </td>
                    </tr>
                   
                     </table>
                  </td>
                    
                     <td  valign="top">
                     
                     <table  cellpadding="1" style="border:solid 1px silver;width:100%" >
                         <tr style="background-color:#E6E6E6;font-weight:bold">
                         <td colspan="100%">Customer Information</td>
                         </tr>
                    <tr>
                    <td colspan="100%">
                     

                    <table>
                       
                      <tr><td style="text-align:right">Recepient Name:</td><td colspan="100%"><div id ="dvRecepient"><asp:Literal ID="ltRecepientname" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td style="text-align:right">Address:</td><td colspan="100%"><div id ="dvAddress"><asp:Literal ID="ltAddress" runat ="server"></asp:Literal></div></td><td></td></tr>
                   <%-- <tr><td style="text-align:right">Recepient Mobile:</td><td colspan="100%"><div id ="dvRecepientMobile"><asp:Literal ID="ltRecepientMobile" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td style="text-align:right">Recepient Phone:</td><td colspan="100%"><div id ="dvRecepientPhone"><asp:Literal ID="ltRecepientPhone" runat ="server"></asp:Literal></div></td><td></td></tr>--%>
                     <tr><td style="text-align:right">Delivery Slot:</td><td colspan="100%"><div id ="dvDeliverySlot"><asp:Literal ID="ltDeliverySlot" runat ="server"></asp:Literal></div></td><td></td></tr>
                    </table>
                    
                    </td>
                    </tr>
                   
                     </table>
                     </td>

                     </tr>
                    
                    
                     <tr>
                     <td colspan="100%">
                  

                     
                      
                     <table class="table table-bordered table-striped table-hover" style="width:100%" id="tbProductInfo">
										<thead>
											<tr>
                                                	<th style="width: 150px">
                                            ProductName
                                        </th>
											
                                    
                                        <th style="width: 35px">
                                            Qty
                                        </th>
                                       
                                         <th style="width: 100px">
                                            Unit
                                        </th>
                                        <th style="width: 50px"></th>
                                           
											</tr>
										</thead>
										 
										
										</table>

                      
                     
                      
                     
                     
                     </td>
                     </tr>

                     <tr>
                     <td colspan = "3">
                     
                     </td>
                     
                     </tr>

                      </table>


<table  width="100%" cellpadding="3">
 <tr style="background-color:#E6E6E6;font-weight:bold;height:10px">
            <td colspan="100%">Place Order Against List</td>
                       
           </tr>
                         
<tr>
<td>

  <table class="table table-bordered table-striped table-hover" style="width:100%" id="tborderProducts">
										<thead>
											<tr>
                                                	<th style="width: 150px">
                                            Image
                                        </th>
												<th style="width: 150px">
                                            Description
                                        </th>
                                       <th style="width: 20px">
                                        </th>
                                        <th style="width: 35px">
                                            Qty
                                        </th>
                                        <th style="width: 20px">
                                        </th>
                                          <th style="width: 80px">
                                            Price
                                        </th>
                                         <th style="width: 100px">
                                            Amount
                                        </th>
                                        <th style="width: 50px"></th>
                                           

											</tr>
										</thead>
										 
										
										</table>

                                            <table>
                    <tr>
                    <td valign="top" style="width:50px"><%--<table>
                    <tr><td>Remarks:</td><td> <textarea id="txtRemarks" rows="3" style="width:190px"></textarea></td></tr>
                      
                   
                   
                     </table>--%>
                     </td>
                    
                    <td valign="top"><table>
                     <tr>
                     <td valign="top">
                     <table style="width:100%">
                     <tr><td style="width:60%">Gross Amount:</td><td style="width:100px"><div id ="dvsbtotal"></div></td></tr>

                   <%--  <tr ><td colspan="100%"><div id="trDeliveryCharges"><table><tr>
                     <td  style="width:60%">Delivery Charges:</td><td style="width:100px"><div id ="dvdelivery"></div></td>
                     </tr></table></div></td></tr>
                        <tr ><td colspan="100%"><div id="trDisAmt"><table><tr>
                        <td  style="width:60%">Dis Amount:</td><td style="width:100px"><div id ="dvDisAmt"></div></td>
                        </tr></table></div></td></tr>
                     <tr><td  style="width:60%">Net Amount:</td><td style="width:100px"><div id ="dvnetAmount"></div></td></tr>--%>
                    
                     </table>
                     </td>
                     <td valign="top">
                     <table>
                     <tr><td>  <div id="btnSave"  class="btn btn-primary btn-small" style="width:120px" >Ready For Process</div></td></tr>
                <tr><td>  <div id="btnCancel" style="background-color:Maroon;width:100px" class="btn btn-primary btn-small"  >Cancel</div></td></tr>
                   
                   
                     </table>
                     
                     </td>
                        
                     </tr>

                     </table></td>
                    </tr>
                    </table> 

                      
</td>
<td>
 
<div id="dvProducts" >

<h3 class="reallynow">
                <span>Products</span>
                <br />
            </h3>
  <table>
  <tr>
  <td style="width:150px" >
                <asp:DropDownList ID="ddlCategories" runat="server" style="width:150px" >
               
                </asp:DropDownList>
            </td>
  </tr>
 </table>        

<table>
<tr>
<td style ="width:300px">
<table id="jQGridDemoProduct">
                </table>
                <div id="jQGridDemoPagerProduct">
                </div>

</td>
</tr>

</table>

</div>

<div id="dvProductOptions"  style="display:none;" >

<h3 class="reallynow">
                <span>Product Qty</span>
                <br />
            </h3>
        
<table id="tbProductVariation">
 
</table>
<table>
<tr>
<td>
 <table>
            <tr>
                <td>Qty:</td>
            <td style="width:150px" >
                <input id="txtQty" type="text"  class="form-control input-small" style="width:120px;background-color:White"/>
             
            </td>
            </tr>
            </table>

</td>

<td>
 <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add</div></td>
                                            
                                            </tr>
                                            </table>

</td>
</tr>

</table>

</div>

</td>

</tr>
</table>
                    
  
  </div>
            </div>
            </div>




             <div id="rightnow">
            <h3 class="reallynow">
                <span>Quick Lists</span>
                <br />
            </h3>
            <div class="youhave">
                                <table class="top_table">
                                <tr><td>Date From:</td>
                                    <td><input type="text" readonly="readonly" class="form-control input-small" id="txtDateFrom" /></td>
                                    <td>Date To:</td>
                                    <td><input type="text" readonly="readonly"  class="form-control input-small" id="txtDateTo" /></td>
                                    <td><div id="btnGo"  class="btn btn-primary btn-small"  >Go</div></td>
                                </tr>
                                </table>

                <table id="jQGridDemo">
                </table>
                <div id="jQGridDemoPager">
                </div>



       
            </div>
        </div>

            </div>


            






  

</form>



<script type="text/javascript">
         function BindGrid()
     {
      var DateFrom=$("#txtDateFrom").val();
     var DateTo=$("#txtDateTo").val();
     jQuery("#jQGridDemo").GridUnload();

              jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/ManageQuickLists.ashx?dateFrom='+DateFrom+'&dateTo='+DateTo,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['ListId','UserId','UserName','DeliveryAddressId','OrderDate','Recipient','Address','DeliverySlotId','Slot','DeliveryDay','status'],
            colModel: [
              { name: 'ListId',key:true, index: 'ListId', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                         { name: 'UserId',key:true, index: 'UserId', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:false },
                         { name: 'UserName', index: 'UserName', width: 150, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},                      
                         { name: 'DeliveryAddressId',key:true, index: 'DeliveryAddressId', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                        { name: 'strOD', index: 'strOD', width: 150, stype: 'text', sortable: true, editable: true, hidden: false  ,editrules: { required: true }},
                        { name: 'Recipient', index: 'Recipient', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
   		                   { name: 'CompleteAddress', index: 'CompleteAddress', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                          { name: 'DeliverySlot',key:true, index: 'DeliverySlot', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                      
                        { name: 'Slot', index: 'Slot', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                         { name: 'strDD', index: 'strDD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},
   		                { name: 'Status', index: 'Status', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                       ],
            rowNum: 10,
          
            mtype: 'GET',
              toolbar: [true, "top"],
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'ListId',
            viewrecords: true,
            height: "100%",
            width:"700px",
            sortorder: 'desc',
             ignoreCase: true,
            caption: "Quick Lists",
         
           
                    
             
        });

        var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });




       $("#jQGridDemo").jqGrid('setGridParam',
         {

             onSelectRow: function (rowid, iRow, iCol, e) {
            



                 LIstId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ListId');
                 BindListProducts();
             
//                     $.ajax({
//                    type: "POST",
//                    data: '{ "OI": "' + OrderId + '"}',
//                    url: "managebilling.aspx/InsertTemp",
//                    contentType: "application/json",
//                    dataType: "json",
//                    success: function (msg) {
//                        


//                        var obj = jQuery.parseJSON(msg.d);   

//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {

//                        var obj = jQuery.parseJSON(xhr.responseText);
//                        alert(obj.Message);
//                    },
//                    complete: function () {

//                    BindTempOrders();
//                    }

//                });
              $('#dvPopup').css("display","block");
              $('#dvMessage').css("display","none");


//                   $('#dvPopup').dialog(
//            {
//            autoOpen: false,

//            width:800,
//            height:800,
//            resizable: false,
//            modal: true,
//  
//            });
//            linkObj = $(this);
//            var dialogDiv = $('#dvPopup');
//            dialogDiv.dialog("option", "position", [150, 100]);
//            dialogDiv.dialog('open');
//            return false;
                
             }
         });
      


        


           $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }

                     );
         
    
        var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '700');
        

      }
        
    </script>

</asp:Content>

