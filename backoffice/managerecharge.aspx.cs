﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_managerecharge : System.Web.UI.Page
{

    int totalRecharge = 0;
    int UsedAmt = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            if (!User.IsInRole("Recharge"))
            {
                Response.Redirect("default.aspx");

            }
            BindGrid();
        }
    }



    [WebMethod]
    public static string Insert(decimal Amount)
    {

        int UserId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        RechargeWallet objRechargeWallet = new RechargeWallet()
        { 
            Amount = Amount,
           
            UserId = UserId,
   
        };
        int status = new RechargeWalletBLL().Insert(objRechargeWallet);
        var JsonData = new
        {
            Recharge = objRechargeWallet,
            Status = status
          
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
        
    }



    [WebMethod]
    public static string GetRechargeByUserId()
    {
        int UserId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        RechargeWallet objRecharge = new RechargeWallet()
        {
            UserId = UserId,
        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        new RechargeWalletBLL().GetByUserId(UserId);
        var JsonData = new
        {
            User = objRecharge,

        };
        return ser.Serialize(JsonData);
    }



    public void BindGrid()
    {


        int UserId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        if (UserId != null)
        {
            GridView2.DataSource = new RechargeWalletBLL().GetByUserId(UserId);
            GridView2.DataBind();
        }
    }
    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       
        totalRecharge = totalRecharge+ Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Amount"));
        UsedAmt  = UsedAmt+ Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "UsedAmount"));
        
        lbltotal.Text = totalRecharge.ToString();
        lblusedamt.Text = UsedAmt.ToString();

        lblleft.Text = (totalRecharge - UsedAmt).ToString();
        
    }
}