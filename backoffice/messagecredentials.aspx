﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="messagecredentials.aspx.cs" Inherits="backoffice_messagecredentials" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/custom-css/settings.css" rel="stylesheet" />

    <form   runat="server" id="formID" method="post">

     <asp:HiddenField ID="hdnDate" runat="server"/>
  
<div id="content">

             <div id="rightnow">
            <h3 class="reallynow">
                <span>Message Credentials</span>
                <br />
            </h3>
            <div class="youhave">
              <table class="top_table">
                    <tr>
                        <td>Admin MobileNo:</td>
                        <td>
                            <asp:TextBox ID="txtAdminMobileNo" runat="server" MaxLength="10" ValidationGroup="a"></asp:TextBox>
                        </td>
                        <td>
                            <%--<asp:RegularExpressionValidator ID="rqExpMobile" runat="server" ErrorMessage="*" ForeColor="Green" ControlToValidate="txtAdminMobileNo" ValidationGroup="a" ValidationExpression="\d{5}([- ]*)\d{6}" ></asp:RegularExpressionValidator>--%>
                                <asp:RequiredFieldValidator ID="reqMobileNo" runat="server" ErrorMessage="*" ForeColor="Maroon" ControlToValidate="txtAdminMobileNo" ValidationGroup="a"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="100%">

                            <h3> <asp:Label ID="Label1" runat="server" Text="Write @OrderNo at the Place Of OrderNo " ForeColor="Maroon"></asp:Label></h3>
                            <h3> <asp:Label ID="Label2" runat="server" Text="Write @CName at the Place Of CustomerName" ForeColor="Maroon"></asp:Label></h3>
                        </td>
                    </tr>
                    <tr>
                        <td>Admin Message Text:</td>
                        <td>
                            <asp:TextBox ID="txtAdminMsg" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="reqAdminMsg" runat="server" ErrorMessage="*" ForeColor="Maroon" ControlToValidate="txtAdminMsg" ValidationGroup="a"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Customer Message Text:</td>
                        <td>
                            <asp:TextBox ID="txtCustomerMsg" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="reqCustomerMsg" runat="server" ErrorMessage="*" ForeColor="Maroon" ControlToValidate="txtCustomerMsg" ValidationGroup="a"></asp:RequiredFieldValidator>
                        </td>

                    </tr>
                    <tr>
                        <td>Order On Call:</td>
                        <td>
                            <asp:TextBox ID="txtOrderOnCall" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="reqOrderOnCall" runat="server" ErrorMessage="*" ForeColor="Maroon" ControlToValidate="txtOrderOnCall" ValidationGroup="a"></asp:RequiredFieldValidator>
                        </td>

                    </tr>
                    <tr>
                        <td>Call Us:</td>
                        <td>
                            <asp:TextBox ID="txtCallUs" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="reqCallUs" runat="server" ErrorMessage="*" ForeColor="Maroon" ControlToValidate="txtCallUs" ValidationGroup="a"></asp:RequiredFieldValidator>
                        </td>

                    </tr>
                </table>
                             <table class="category_table">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary btn-small" Text="Save" ValidationGroup="a" OnClick="btnSave_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnReset" runat="server" CssClass="btn btn-primary btn-small" Text="Reset" OnClick="btnReset_Click" />
                                    </td>
                                </tr>
                            </table>
                </div>
                 </div>
    </div>
        </form>
</asp:Content>

