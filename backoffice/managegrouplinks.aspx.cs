﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;

public partial class backoffice_managegrouplinks : System.Web.UI.Page
{


    protected string m_sMessage = "";
    protected string m_sSubMessage = "";
    public DataTable dtFinal = new DataTable();
    protected bool m_bShow = false;
    protected string ErrorMessage = "";
    protected string htmldata = "";
    public int m_CurrentPage = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!User.IsInRole("ManageSubGroups"))
            {
                Response.Redirect("default.aspx");

            }
            BindCategories();
        }
    }



    void BindCategories()
    {

        ddlGroup.DataSource = new MyGroupsBLL().GetAllHasSubgroups();
        ddlGroup.DataValueField = "Id";
        ddlGroup.DataTextField = "Title";
        ddlGroup.DataBind();
        ListItem li = new ListItem();
        li.Text = "--Choose Groups--";
        li.Value = "0";
        ddlGroup.Items.Insert(0, li);


        ddlCategory.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategory.DataValueField = "CategoryId";
        ddlCategory.DataTextField = "Title";
        ddlCategory.DataBind();
        ListItem lii = new ListItem();
        lii.Text = "--Category Level 1--";
        lii.Value = "0";
        ddlCategory.Items.Insert(0, li);


        ddlCategoryp.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategoryp.DataValueField = "CategoryId";
        ddlCategoryp.DataTextField = "Title";
        ddlCategoryp.DataBind();
        ListItem l3 = new ListItem();
        l3.Text = "--Category Level 1--";
        l3.Value = "0";
        ddlCategoryp.Items.Insert(0, l3);


        ddlBrand.DataSource = new BrandBLL().GetAll();
        ddlBrand.DataValueField = "BrandId";
        ddlBrand.DataTextField = "Title";
        ddlBrand.DataBind();
        ListItem l2 = new ListItem();
        l2.Text = "--Choose Brand--";
        l2.Value = "0";
        ddlBrand.Items.Insert(0, l2);


    }


    [WebMethod]
    public static string BindSubCategories(int CatId)
    {

        string SubCat = new CategoryBLL().GetOptions(CatId);


        //string ProductHTML = new ProductsBLL().GetProductHTMLByCategoryLevel2(CatId);


        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            SubCatOptions = SubCat,
            ProductHtml = "",

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindCategoryLevel3(int CatId)
    {

        string SubCat = new CategoryBLL().GetOptions(CatId);


        //string ProductHTML = new ProductsBLL().GetProductHTMLByCategoryLevel3(CatId);


        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            SubCatOptions = SubCat,
            ProductHtml = ""

        };
        return ser.Serialize(JsonData);
    }



    public void BindSubCategories(int ParentId, DropDownList ddlParent, DropDownList ddl)
    {
        try
        {
            if (ddlParent.SelectedIndex != 0)
            {

                m_bShow = true;
                ddl.DataSource = new CategoryBLL().GetByParentId(ParentId);
                ddl.DataTextField = "Title";
                ddl.DataValueField = "CategoryId";
                ddl.DataBind();
                ListItem li = new ListItem();
                li.Text = "--Category Level 2--";
                li.Value = "0";
                ddl.Items.Insert(0, li);
            }
            else
            {
                m_sMessage = "Please Select  Category Level 1";
            }
        }
        finally
        {
        }

    }

    public void BindCategoryLevel3(int ParentId, DropDownList ddl)
    {
        try
        {
            if (ParentId != 0)
            {

                m_bShow = true;
                ddl.DataSource = new CategoryBLL().GetByParentId(ParentId);
                ddl.DataTextField = "Title";
                ddl.DataValueField = "CategoryId";
                ddl.DataBind();
                ListItem li = new ListItem();
                li.Text = "--Category Level 3--";
                li.Value = "0";
                ddl.Items.Insert(0, li);
            }
            else
            {
                m_sMessage = "Please Select  Category";
                return;
            }
        }
        finally
        {
        }

    }

    [WebMethod]
    public static string Insert(int SubgroupId, Int32 GroupId, string SubGroupTitle, string PhotoUrl,string LinkUrl)
    {


        GroupLinks objGroupLinks = new GroupLinks()
        {
            SubGroupId = SubgroupId,
            GroupId = GroupId,
            SubGroupTitle = SubGroupTitle.Trim().ToUpper(),
            PhotoUrl = PhotoUrl,
            LinkUrl = LinkUrl,
            CatgoryId = 0,
            LevelNo = 0,
            AdminId = 0,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new GroupLinksBLL().InsertUpdate(objGroupLinks);
        var JsonData = new
        {
            Categries = objGroupLinks,
            Status = status
        };
        return ser.Serialize(JsonData);



    }



    [WebMethod]
    public static string getbySubGroupId(int SubGroupId)
    {


        GroupLinks objGroupLinks = new GroupLinks()
        {

            SubGroupId = SubGroupId,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        new GroupLinksBLL().GetbySubGroupId(objGroupLinks);
        var JsonData = new
        {
            CategoryLevels = objGroupLinks,
          
        };
        return ser.Serialize(JsonData);



    }



    [WebMethod]
    public static string FillProductsByCategoryLevel1(int CategoryId)
    {


        string Products = new ProductsBLL().GetByCategoryLevel1forgroups(CategoryId);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            ProductOptions = Products

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string FillProductsBySubcategory(int SubCategoryId)
    {


        string Products = new ProductsBLL().GetBySubCategoryforgroups(SubCategoryId);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            ProductOptions = Products

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string FillProductsByCategoryLevel3(int SubCategoryId)
    {


        string Products = new ProductsBLL().GetByCategory3forgroups(SubCategoryId);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            ProductOptions = Products

        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]

    public static string InsertSubgroupProducts(int SubgroupId, Int32 GroupId, string SubGroupTitle, string PhotoUrl, string ProductIdarr)
    {
        GroupLinks objGroupLinks = new GroupLinks()
        {

            SubGroupId = SubgroupId,
            GroupId = GroupId,
            SubGroupTitle = SubGroupTitle,
            PhotoUrl = PhotoUrl,
         
            AdminId = 0,

        };

        string[] ProductData = ProductIdarr.Split(',');


        DataTable dt = new DataTable();
        dt.Columns.Add("ProductId");

        for (int i = 0; i < ProductData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ProductId"] = Convert.ToInt32(ProductData[i]); ;

            dt.Rows.Add(dr);

        }


        JavaScriptSerializer ser = new JavaScriptSerializer();

        int st = new GroupLinksBLL().InsertSubGroupProducts(objGroupLinks, dt);
        var JsonData = new
        {
            Status = st,
            SubGroupProduct = objGroupLinks

        };
        return ser.Serialize(JsonData);
    }
}