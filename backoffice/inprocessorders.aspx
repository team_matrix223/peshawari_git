﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="inprocessorders.aspx.cs" Inherits="backoffice_inprocessorders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">


  <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/transaction.css" rel="stylesheet" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

    <script type="text/javascript">
        var m_BillNo = 0;
        var OrderId = 0;
        var Category = 0;
        var Unit1 = "";
        var Unit2 = "";
        var Unit3 = "";
        var DeliveryChrg =0;
        var FreeAmount =0;

        var ProductCollection = [];
        function clsProduct() {
         this.BrandName = "";
            this.ProductId = 0;
            this.ProductName = 0;
            this.Qty = 0;
            this.Price = 0;
            this.VariationId = 0;
            this.ProductDescription = "";
            this.SubTotal = 0;
        }

    
        function Reset()
        {
        
          $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove(); 
          $('#tbProductVariation tr').not(function () { if ($(this).has('th').length) { return true } }).remove(); 
          $("#<%=ltOrderNo.ClientID%>").val("");
          $("#<%=ltOrderDate.ClientID%>").val("");
          $("#<%=ltItems.ClientID%>").val("");
          $("#<%=ltCustomer.ClientID%>").val("");
          $("#<%=ltAddress.ClientID%>").val("");
          $("#<%=ltOrderAmount.ClientID%>").val("");
          $("#<%=ltRecepientname.ClientID%>").val("");
          $("#<%=ltRecepientPhone.ClientID%>").val("");
          $("#<%=ltRecepientMobile.ClientID%>").val("");
          $("#<%=ltDeliverySlot.ClientID%>").val("");
          $("#<%=ltStatus.ClientID%>").val("");
            $("#<%=ltMessage.ClientID%>").val("");
          $("#txtRemarks").val("");
          var dialogDiv = $('#dvPopup');
          dialogDiv.dialog("option", "position", [500, 200]);
          dialogDiv.dialog('close');
          BindGrid();
        }

        function AddProductToList() {
            var Price = 0;

            var SelectedRow = jQuery('#jQGridDemoProduct').jqGrid('getGridParam', 'selrow');
            if ($.trim(SelectedRow) == "") {
                alert("No Product is selected to add");
                return;
            }

            var ProductId = $('#jQGridDemoProduct').jqGrid('getCell', SelectedRow, 'ProductId');
            var VariationId = $('#jQGridDemoProduct').jqGrid('getCell', SelectedRow, 'VariationId');

            var ProductName = $('#jQGridDemoProduct').jqGrid('getCell', SelectedRow, 'Name');

            var Qty = $("#txtQty").val();
            Price = $('#jQGridDemoProduct').jqGrid('getCell', SelectedRow, 'Price');

            var Mode = "Plus";


            $.ajax({
                type: "POST",
                data: '{ "OrderId": "' + OrderId + '","ProductId": "' + ProductId + '","VariationId": "' + VariationId + '","Qty": "' + Qty + '","Price": "' + Price + '","Mode": "' + Mode + '"}',
                url: "inprocessorders.aspx/UpdateQtyByProductId",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {



                    var obj = jQuery.parseJSON(msg.d);
                    alert("Product is added successfully.");
                    $("#txtQty").val("");
                    var dialogDiv = $('#dvProductOptions');
                    dialogDiv.dialog("option", "position", [500, 200]);
                    dialogDiv.dialog('close');


                    var dialogDiv1 = $('#dvProducts');
                    dialogDiv1.dialog("option", "position", [500, 200]);
                    dialogDiv1.dialog('close');

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    BindTempOrders();
                }

            });

        }
          function BindProducts(Category)
     {
           
             jQuery("#jQGridDemoProduct").GridUnload();

              jQuery("#jQGridDemoProduct").jqGrid({
            url: 'handlers/ProductsByCategory.ashx?Category='+Category,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
          colNames: ['VariationId','CategoryId','ProductId','ProductName','Description','ShortName','Price'],
            colModel: [

              { name: 'VariationId', key: true, index: 'VariationId', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
              { name: 'CategoryId',key:true, index: 'CategoryId', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
              { name: 'ProductId',key:true, index: 'ProductId', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
              { name: 'Name', index: 'Name', width: 100, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},
              { name: 'Description',key:true, index: 'Description', width: 100, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
              { name: 'ShortName', index: 'ShortName', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                { name: 'Price', index: 'Price', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true },hidden:true },
                         ],

            rowNum: 10,
          
            mtype: 'GET',
              toolbar: [true, "top"],
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPagerProduct',
            sortname: 'ProductId',
            viewrecords: true,
            height: "100%",
            width:"300px",
            sortorder: 'desc',
             ignoreCase: true,
            caption: "Products List",
            
        });


         var $grid = $("#jQGridDemoProduct");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText1\">Global search for:&nbsp;</label><input id=\"globalSearchText1\" type=\"text\"></input>&nbsp;<button id=\"globalSearch1\" type=\"button\">Search</button></div>"));
            $("#globalSearchText1").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch1").click();
                }
            });
            $("#globalSearch1").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText1").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });



         $("#jQGridDemoProduct").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {
             var ProductId = $('#jQGridDemoProduct').jqGrid('getCell', rowid, 'ProductId');                  
             var VariationId = $('#jQGridDemoProduct').jqGrid('getCell', rowid, 'VariationId');
             var item = $.grep(ProductCollection, function (item) {
             return item.VariationId == VariationId;
             });
             if (item.length) {
                 $("#txtQty").val("1");
                 AddProductToList();
                 return;
             }
             else {
                 $('#dvProductOptions').dialog(
               {
                   autoOpen: false,
                   width: 300,
                   height: 150,
                   resizable: false,
                   modal: true,
               });
                 linkObj = $(this);
                 var dialogDiv = $('#dvProductOptions');
                 dialogDiv.dialog("option", "position", [500, 200]);
                 dialogDiv.dialog('open');
                 return false;
             }
             }
         });

        $('#jQGridDemoProduct').jqGrid('navGrid', '#jQGridDemoPagerProduct',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,                  
                   {//SEARCH
                       closeOnEscape: true
                   }
                     );
  
        var DataGrid = jQuery('#jQGridDemoProduct');
        DataGrid.jqGrid('setGridWidth', '400');
      }
    
     function UpdateBilledOrders() {
        
         
         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
        if($.trim(SelectedRow) == "")
           {
             alert("No Order is selected to send for billing");
             return;
           }
        else
        {
        var OrderId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'OrderId');
       
        
         $.ajax({
            type: "POST",
            data: '{"OI":"' + OrderId + '"}',
            url: "inprocessorders.aspx/UpdateOrdersBilled",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                 
                alert("Order Send for billing successfully.");
           
                Reset();
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });


        BindGrid();
        }

    }


      function BindTempOrders() {


             var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             DeliveryChrg = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'DeliveryCharges');
             FreeAmount = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'FreeDeliveryAmt');
             $("#dvOrderno").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'OrderId'));
             $("#dvOrderDate").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'strOD'));
              $("#dvItems").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Comment'));
             $("#dvCustomer").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'CustomerName'));
             $("#dvOrderAmount").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillValue'));
             $("#dvRecepient").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'RecipientFirstName'));
             $("#dvRecepientMobile").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'RecipientMobile'));
             $("#dvRecepientPhone").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'RecipientPhone'));
             $("#dvAddress").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'CompleteAddress'));
             $("#dvDeliverySlot").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'DeliverySlot'));
             $("#dvCouponNo").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'CouponNo'));
              $("#dvStatus").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'PaymentStatus'));
              $("#dvMessage").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'ResponseMessage'));
              ProductCollection=[];
              if($.trim($("#dvCouponNo").html())=="")
             {
             $("#trCouponNo").css({"display":"none"});
             }
             else{
              $("#trCouponNo").css({"display":"block"});
             }
                         $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove(); 
                       
                         $.ajax({
                         type: "POST",
                         data: '{ "Oid": "' + OrderId + '"}',
                         url: "inprocessorders.aspx/GetTemparoryOrders",
                         contentType: "application/json",
                         dataType: "json",
                         success: function (msg) {
               
                           ProductCollection = [];

              
                        var obj = jQuery.parseJSON(msg.d);

                          var DeliveryCharges=0;
                        var DisAmt=0;
                        var tr = "";
                        var Total  = 0;
                        for (var i = 0; i < obj.TempOrders.length; i++) {
                   
                            var schemeid = obj.TempOrders[i]["SchemeId"];
                            if (schemeid == 0) {
                                tr = tr + "<tr><td><img src='../ProductImages/T_" + obj.TempOrders[i]["PhotoUrl"] + "' style='width:50px;height:50px'/></td><td>" + obj.TempOrders[i]["BrandName"] + "</td><td>" + obj.TempOrders[i]["ProductDescription"] + "</td><td><div id='btnMinus'  class='btn btn-primary btn-small' style='height:6px'>-</div></td><td><input type='text' style ='width:35px' id='q_" + obj.TempOrders[i]["ProductId"] + "'  value =" + obj.TempOrders[i]["Qty"] + " readonly='readonly' /></td> <td><div id='btnPlus'  class='btn btn-primary btn-small' style='height:6px'>+</div></td><td>" + obj.TempOrders[i]["Price"] + "</td><td>" + obj.TempOrders[i]["Amount"] + "</td><td><div id='" + obj.TempOrders[i]["ProductId"] + "_" + obj.TempOrders[i]["VariationId"] + "'  name='dvClose' ><img src='images/trash.png'/></div> </td></tr>";
                                Total = Number(Total) + Number(obj.TempOrders[i]["Amount"]);
                            }
                            else {
                                tr = tr + "<tr><td><img src='../ProductImages/T_" + obj.TempOrders[i]["PhotoUrl"] + "' style='width:50px;height:50px'/></td><td>" + obj.TempOrders[i]["BrandName"] + "</td><td>" + obj.TempOrders[i]["ProductDescription"] + "</td><td></td><td><input type='text' style ='width:20px' id='q_" + obj.TempOrders[i]["ProductId"] + "'  value =" + obj.TempOrders[i]["Qty"] + " readonly='readonly' /></td> <td></td><td>" + obj.TempOrders[i]["Price"] + "</td><td>" + obj.TempOrders[i]["Amount"] + "</td><td><img src='../img/free.gif' /></td></tr>";
                            }
                         
                         var PO = new clsProduct();
                         PO.ProductId = obj.TempOrders[i]["ProductId"] ;
                         PO.ProductName = obj.TempOrders[i]["ProductName"] ;
                         PO.ProductDescription = obj.TempOrders[i]["ProductDescription"] ;
                         PO.Qty = obj.TempOrders[i]["Qty"] ; 
                         PO.Price = obj.TempOrders[i]["Price"] ; 
                         PO.VariationId = obj.TempOrders[i]["VariationId"];
                         ProductCollection.push(PO);     
                         
                         
                         DeliveryCharges=obj.TempOrders[i]["DeliveryCharges"];  
                         DisAmt=obj.TempOrders[i]["DisAmt"];                                             
                      }
                      
                      $("div[id='dvsbtotal']").html(" Rs. " + Total);
                      if(Number(Total < Number(FreeAmount)))
                      {
                         $("div[id='dvdelivery']").html(" Rs. " + DeliveryChrg);
                         $("div[id='dvnetAmount']").html(" Rs. " + Number(Number(Total)+Number(DeliveryChrg)));
                      }
                      else
                      {
                         $("div[id='dvdelivery']").html(" Rs. " + 0);
                         $("div[id='dvnetAmount']").html(" Rs. " + Number(Number(Total)+0));
                      }

                      if(Number( DeliveryCharges)==0){
                      $("#trDeliveryCharges").css({"display":"none"});
                    }
                      else{
                      $("#trDeliveryCharges").css({"display":"block"});
                      }
                      if(Number(DisAmt)==0){
                      $("#trDisAmt").css({"display":"none"});
                    }
                      else{
                      $("#trDisAmt").css({"display":"block"});
                      }
                      $("div[id='dvdelivery']").html(" Rs. " + DeliveryCharges);
                       $("div[id='dvDisAmt']").html(" Rs. " + DisAmt);
                      $("div[id='dvnetAmount']").html(" Rs. " + Number(Number(Total)+Number(DeliveryCharges)-Number(DisAmt)));
                     $("#tbProductInfo").append(tr);

                 },
                complete: function (msg) {
              
             BindProducts(Category);
            }

        });
      }

        $(document).on("click", "div[name='dvClose']", function (event) {
         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');

           if($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'PaymentStatus') == "Success")
           {
           alert("This order can not be Edit ..... Payment has been done for this order");
           return;
           }
           else
           {
         
          var id=$(this).attr("id");
          var mm=[];
          mm=id.split('_');
          var ProductId = mm[0];
          var VariationId= mm[1];
         var RowIndex = Number($(this).closest('tr').index());
      
        
                $.ajax({
                    type: "POST",
                    data: '{ "OI": "' + OrderId + '","ProductId": "' + ProductId  + '","VariationId": "' + VariationId  + '"}',
                    url: "inprocessorders.aspx/DeleteTempOrders",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {
                        


                        var obj = jQuery.parseJSON(msg.d);   
                       
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                   
                    BindTempOrders();
                    }
                 });
                 }
    });




     $(document).on("click", "#btnPlus", function (event) {
    
     var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');

           if($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'PaymentStatus') == "Success")
           {
           alert("This order can not be Edit ..... Payment has been done for this order");
           return;
           }
           else
           {
        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ProductId"];

        var Mode="Plus";
        var VariationId = ProductCollection[RowIndex]["VariationId"];
        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];

//        $("#cq_" + PId + VariationId).html("<img src='images/progressbar.gif' style='margin-top:4px' alt='...'/>");
        UpdateQty(OrderId,PId,VariationId,Qty,Price,Mode);
        }
      
    });
    $(document).on("click", "#btnMinus", function (event) {

     var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');

           if($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'PaymentStatus') == "Success")
           {
           alert("This order can not be Edit ..... Payment has been done for this order");
           return;
           }
           else
           {

        var RowIndex = Number($(this).closest('tr').index());

        var PId = ProductCollection[RowIndex]["ProductId"];
        var Mode = "Minus";
        var VariationId = ProductCollection[RowIndex]["VariationId"];
         var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"]; 
         UpdateQty(OrderId,PId,VariationId,Qty,Price,Mode);
         }
      
    });
  
    function UpdateQty(OrderId,PId,VariationId,Qty,Price,Mode) {
     
        $.ajax({
            type: "POST",
            data: '{"OrderId":"' + OrderId + '","ProductId":"' + PId + '","VariationId":"' + VariationId + '","Qty":"' + Qty + '","Price":"' + Price + '","Mode":"' + Mode + '"}',
            url: "inprocessorders.aspx/UpdateQtyByProductId",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
//                 $("#cq_" + PId+ VariationId).html(Qty);

//                $("#cq_" + PId + VariationId).html(Qty);
//                $("#span" +PId + VariationId).html(obj.Calc.ProductSubTotal);

//                 if (Qty == 0) {
//                    $("#tp_" +PId + VariationId).remove();
//                }
                    BindTempOrders();
              
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
           
                $.uiUnlock();

            }
        });
    }

        $(document).ready(
        function () {


          function Printt(celValue) {

      $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
         var iframe = document.getElementById('reportout');
         iframe = document.createElement("iframe");
         iframe.setAttribute("id", "reportout");
         iframe.style.width = 0 + "px";
         iframe.style.height = 0 + "px";
         document.body.appendChild(iframe);
         document.getElementById('reportout').contentWindow.location = "Reports/RptBilling.aspx?BillNowPrefix=" + celValue;
       $.uiUnlock();

     }


     $("#btnprint").click(
     function(){
     var orderno = $("#dvOrderno").html();
    
     Printt(orderno);
     }
     );

            $("#txtDateFrom").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });


            $("#txtDateTo").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });

            $("#txtDateTo,#txtDateFrom").val($("#<%=hdnDate.ClientID%>").val());
            BindGrid();



            $("#btnGo").click(
        function () {
          
           BindGrid();

        }
        );


         
         $("#btnCancel").click(
        function() {

         $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove(); 
          $('#tbProductVariation tr').not(function () { if ($(this).has('th').length) { return true } }).remove(); 
          $("#<%=ltOrderNo.ClientID%>").val("");
          $("#<%=ltOrderDate.ClientID%>").val("");
          $("#<%=ltItems.ClientID%>").val("");
          $("#<%=ltCustomer.ClientID%>").val("");
          $("#<%=ltAddress.ClientID%>").val("");
          $("#<%=ltOrderAmount.ClientID%>").val("");
          $("#<%=ltRecepientname.ClientID%>").val("");
          $("#<%=ltRecepientPhone.ClientID%>").val("");
          $("#<%=ltRecepientMobile.ClientID%>").val("");
          $("#<%=ltDeliverySlot.ClientID%>").val("");
           $("#<%=ltStatus.ClientID%>").val("");
             $("#<%=ltMessage.ClientID%>").val("");
          $("#txtRemarks").val("")
           var dialogDiv = $('#dvPopup');
                dialogDiv.dialog("option", "position", [170, 150]);
                dialogDiv.dialog('close');
                

        }
        );


         
        
            $("#btnSave").click(
        function () {
           if(ProductCollection.length==0)
        {
         alert("Add atleast one Item");
          return;
         }
         UpdateBilledOrders();

        }
        );


       
       
        $("#<%=ddlCategories.ClientID%>").change(function () {

            if ($(this).val() == "0") {

                jQuery("#jQGridDemo").GridUnload();
                Category = 0;
                return;
            }

            Category = $("#<%=ddlCategories.ClientID %>").val();
           
            BindProducts(Category);
        });

       
            $("#btnAddProduct").click(
        function () {

         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');

           if($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'PaymentStatus') == "Success")
           {
           alert("Product can not be Added now ..... Payment has been done for this order");
           return;
           }
           else
           {

         
         $('#dvProducts').dialog(
            {
            autoOpen: false,

            width:450,
            height:440,
            resizable: false,
            modal: true,
  
            });
            linkObj = $(this);
            var dialogDiv = $('#dvProducts');
            dialogDiv.dialog("option", "position", [170, 120]);
            dialogDiv.dialog('open');
            return false;

            }
                
        

        }
        );
       $("#btnAdd").click(
        function () {
            if ($.trim($("#txtQty").val()) == "") {
                alert("Enter Product Qty");
                return;
            }
            AddProductToList();

        }
        );



        });

    </script>
      <iframe id="reportout" width="0" height="0"   onload="processingComplete()"></iframe>
     <form   runat="server" id="formID" method="post">

     <asp:HiddenField ID="hdnDate" runat="server"/>
  
<div id="content">

             <div id="rightnow">
            <h3 class="reallynow">
                <span>InProcess Orders</span>
                <br />
            </h3>
            <div class="youhave">
              <table class="top_table">
                                <tr><td>Date From:</td><td>
                         
                                <input type="text" readonly="readonly"   class="form-control input-small"  id="txtDateFrom" /></td>
                                    <td>Date To:</td><td><input type="text" readonly="readonly"  class="form-control input-small"    id="txtDateTo" />
                                </td>
                                <td><div id="btnGo"  class="btn btn-primary btn-small"  >Go</div></td>
                                </tr>
                                </table>

                <table id="jQGridDemo">
                </table>
                <div id="jQGridDemoPager">
                </div>

 
            </div>
        </div>

            </div>


<div id="dvPopup" style="display:none">
  <table width="100%" cellpadding="3" >
                    

                     <tr>
                     <td valign="top">
                     <table style="width:100%">
                     <tr>
                     <td valign="top">
                     <table  cellpadding="0" style="border:solid 1px silver; width:100%;height:172px">
                         <tr style="background-color:#E6E6E6;font-weight:bold;height:10px">
                         <td colspan="100%">Order Information</td>
                         </tr>
                         
                    <tr>
                    <td colspan="100%" valign="top">
                     

                    <table cellpadding ="5">
                      <tr>
                     <td align="right">Order No:</td><td><div id="dvOrderno"><asp:Literal ID="ltOrderNo" runat ="server"></asp:Literal></div></td>
                     </tr>
                     <tr>
                     <td align="right">Order Date:</td><td><div id="dvOrderDate"><asp:Literal ID="ltOrderDate" runat ="server"></asp:Literal></div></td>
       
                     </tr>
                   

                     <tr>
                      <td align="right">Customer Name:</td><td><div id ="dvCustomer"><asp:Literal ID="ltCustomer" runat ="server"></asp:Literal></div></td>
                       
                       </tr>
                         <tr>
                     <td align="right">Order Amount:</td><td><div id="dvOrderAmount"><asp:Literal ID="ltOrderAmount" runat ="server"></asp:Literal></div></td>
       
                     </tr>
                             <tr >
                      <td colspan="100%"><div id="trCouponNo">
                         <table><tr>
                   <td align="right">CouponNo:</td><td><div id="dvCouponNo"></div></td>
                       </tr></table>
                         </div></td>
                     
       
                     </tr>
                      <tr>
                     <td align="right">Items Not Found:</td><td><div id="dvItems"><asp:Literal ID="ltItems" runat ="server"></asp:Literal></div></td>
       
                     </tr>
                       <tr><td style="text-align:right">Payment Status:</td><td colspan="100%"><div id ="dvStatus"><asp:Literal ID="ltStatus" runat ="server"></asp:Literal></div></td><td></td></tr>
                      <tr><td style="text-align:right">Error Message:</td><td colspan="100%"><div id ="dvMessage"><asp:Literal ID="ltMessage" runat ="server"></asp:Literal></div></td><td></td></tr>

                      </table>

                      </td>
   
                     </tr>
                    
                     

                    
                    
                    
                    
                    </table>
                    
                    </td>
                    </tr>
                   
                     </table>
                  </td>
                    
                     <td  valign="top">
                     
                     <table  cellpadding="1" style="border:solid 1px silver;width:100%" >
                         <tr style="background-color:#E6E6E6;font-weight:bold">
                         <td colspan="100%">Customer Information</td>
                         </tr>
                    <tr>
                    <td colspan="100%">
                     

                    <table>
                       
                      <tr><td style="text-align:right">Recepient Name:</td><td colspan="100%"><div id ="dvRecepient"><asp:Literal ID="ltRecepientname" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td style="text-align:right">Address:</td><td colspan="100%"><div id ="dvAddress"><asp:Literal ID="ltAddress" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td style="text-align:right">Recepient Mobile:</td><td colspan="100%"><div id ="dvRecepientMobile"><asp:Literal ID="ltRecepientMobile" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td style="text-align:right">Recepient Phone:</td><td colspan="100%"><div id ="dvRecepientPhone"><asp:Literal ID="ltRecepientPhone" runat ="server"></asp:Literal></div></td><td></td></tr>
                     <tr><td style="text-align:right">Delivery Slot:</td><td colspan="100%"><div id ="dvDeliverySlot"><asp:Literal ID="ltDeliverySlot" runat ="server"></asp:Literal></div></td><td></td></tr>
                    </table>
                    
                    </td>
                    </tr>
                   
                     </table>
                     </td>

                     </tr>
                    
                     </table>
                     <tr>
                     <td colspan="100%">
                  

                     
                      
                     <table class="table table-bordered table-striped table-hover" style="width:100%" id="tbProductInfo">
										<thead>
											<tr>
                                                	<th style="width: 150px">
                                            Image
                                        </th>
                                        <th style="width: 150px">
                                            BrandName
                                        </th>
												<th style="width: 150px">
                                            Description
                                        </th>
                                       <th style="width: 20px">
                                        </th>
                                        <th style="width: 35px">
                                            Qty
                                        </th>
                                        <th style="width: 20px">
                                        </th>
                                          <th style="width: 80px">
                                            Price
                                        </th>
                                         <th style="width: 100px">
                                            Amount
                                        </th>
                                        <th style="width: 50px"></th>
                                           

											</tr>
										</thead>
										 
										
										</table>

                      
                     
                      
                     
                     
                     </td>
                     </tr>

                     <tr>
                     <td  >

                    <table>
                    <tr>
                    <td valign="top" style="width:90px"><%--<table>
                    <tr><td>Remarks:</td><td> <textarea id="txtRemarks" rows="3" style="width:190px"></textarea></td></tr>
                      
                   
                   
                     </table>--%>
                     </td>
                    
                    <td valign="top"><table>
                     <tr>
                     <td valign="top">
                     <table>
                     <tr><td >Gross Amount:</td><td><div id ="dvsbtotal"></div></td></tr>

                     <tr ><td colspan="100%"><div id="trDeliveryCharges"><table><tr>
                     <td  style="width:60%">Delivery Charges:</td><td style="width:100px"><div id ="dvdelivery"></div></td>
                     </tr></table></div></td></tr>
                        <tr ><td colspan="100%"><div id="trDisAmt"><table><tr>
                        <td  style="width:60%">Dis Amount:</td><td style="width:100px"><div id ="dvDisAmt"></div></td>
                        </tr></table></div></td></tr>
                     <tr><td >Net Amount:</td><td><div id ="dvnetAmount"></div></td></tr>
                    
                     </table>
                     </td>
                     <td valign="top">
                     <table>
                     <tr><td>  <div id="btnSave"  class="btn btn-primary btn-small" style="width:120px" >Update Bill</div></td></tr>
                     <tr><td>  <div id="btnprint"  class="btn btn-primary btn-small" style="width:120px" >Print</div></td></tr>
                      <tr><td>  <div id="btnCancel" style="background-color:Maroon;width:100px" class="btn btn-primary btn-small"  >Cancel</div></td></tr>
                   
                   
                     </table>
                     
                     </td>
                         <td>
    <div id="btnAddProduct" class="btn btn-primary btn-small" style="width:150px;height:30px;background:green;vertical-align:middle;padding-top:25px">Add New Product</div>
  </td>
                     </tr>

                     </table></td>
                    </tr>
                    </table> 
                     </td>
                     </tr>
                     <tr>
                     <td colspan = "3">
                     
                     </td>
                     
                     </tr>
                    
  
  </div>

  
<div id="dvProducts"  style="display:none;">

<h3 class="reallynow">
                <span>Products</span>
                <br />
            </h3>
  <table>
  <tr>
  <td style="width:150px" >
                <asp:DropDownList ID="ddlCategories" runat="server" style="width:150px" >
               
                </asp:DropDownList>
            </td>
  </tr>
 </table>        

<table>
<tr>
<td style ="width:300px">
<table id="jQGridDemoProduct">
                </table>
                <div id="jQGridDemoPagerProduct">
                </div>

</td>
</tr>

</table>

</div>

<div id="dvProductOptions"  style="display:none;">

<h3 class="reallynow">
                <span>Product Qty</span>
                <br />
            </h3>
        
<table id="tbProductVariation">
 
</table>
<table>
<tr>
<td>
 <table>
            <tr>
                <td>Qty:</td>
            <td style="width:150px" >
                <input id="txtQty" type="text"  class="form-control input-small" style="width:120px;background-color:White"/>
               <%-- <asp:DropDownList ID="ddlQty" runat="server" style="width:150px" >
                <asp:ListItem Text="1" Value ="1"></asp:ListItem>
                <asp:ListItem Text="2" Value ="2"></asp:ListItem>
                <asp:ListItem Text="3" Value ="3"></asp:ListItem>
                <asp:ListItem Text="4" Value ="4"></asp:ListItem>
                <asp:ListItem Text="5" Value ="5"></asp:ListItem>
                </asp:DropDownList>--%>
            </td>
            </tr>
            </table>

</td>

<td>
 <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add</div></td>
                                            
                                            </tr>
                                            </table>

</td>
</tr>

</table>

</div>
</form>



<script type="text/javascript">
         function BindGrid()
     {
      var DateFrom=$("#txtDateFrom").val();
     var DateTo=$("#txtDateTo").val();
     jQuery("#jQGridDemo").GridUnload();

              jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/InProcessOrdersGetByDate.ashx?dateFrom='+DateFrom+'&dateTo='+DateTo,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['OrderNo','OrderDate', 'CustomerName', 'Recipient','Address','Mobile','BillValue','Status','CustomerId','RecipientFirstName','RecipientLastName','RecipientMobile','RecipientPhone','City','Area','Street','Address','Pincode','BillValue','DisPer','DisAmt','ServiceTaxPer','ServiceTaxAmt','ServiceChargePer','ServiceChargeAmt','VatPer','VatAmt','NetAmount','Remarks','ExecutiveId','IPAddress','Bill Mode','DeliverySlot','DeliveryCharges','FreeDeliveryAmt','Comment','CouponNo','PaymentStatus','Message','Type'],
            colModel: [
              { name: 'OrderId',key:true, index: 'OrderId', width: '50px', stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:false },
                         { name: 'strOD', index: 'strOD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},
   		                 
                         { name: 'CustomerName', index: 'CustomerName', width: 150, stype: 'text', sortable: true,hidden:false, hidden: false, editable: true ,editrules: { required: true }},
                         { name: 'Recipient', index: 'Recipient', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'CompleteAddress', index: 'CompleteAddress', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                        { name: 'RecipientMobile', index: 'RecipientMobile', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
   		                   { name: 'BillValue', index: 'BillValue', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'Status', index: 'Status', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                      
                      { name: 'CustomerId',key:true, index: 'CustomerId', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                         { name: 'RecipientFirstName', index: 'RecipientFirstName', width: 150, stype: 'text', sortable: true, hidden: true, editable: true ,editrules: { required: true }},
   		                 { name: 'RecipientLastName', index: 'RecipientLastName', width: 150, stype: 'text', sortable: true,hidden:false, hidden: true, editable: true ,editrules: { required: true }},
                         { name: 'RecipientMobile', index: 'RecipientMobile', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'RecipientPhone', index: 'RecipientPhone', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},   
   		                   { name: 'City', index: 'City', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'Area', index: 'Area', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                         { name: 'Street',key:true, index: 'Street', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                         { name: 'Address', index: 'Address', width: 150, stype: 'text', sortable: true, hidden: true, editable: true ,editrules: { required: true }},
   		                 { name: 'Pincode', index: 'Pincode', width: 150, stype: 'text', sortable: true,hidden:false, hidden: true, editable: true ,editrules: { required: true }},
                         { name: 'BillValue', index: 'BillValue', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'DisPer', index: 'DisPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'DisAmt', index: 'DisAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
   		                   { name: 'ServiceTaxPer', index: 'ServiceTaxPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'ServiceTaxAmt', index: 'ServiceTaxAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                       { name: 'ServiceChargePer', index: 'ServiceChargePer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                       { name: 'ServiceChargeAmt', index: 'ServiceChargeAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'VatPer', index: 'VatPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'VatAmt', index: 'VatAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
   		                   { name: 'NetAmount', index: 'NetAmount', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                        { name: 'Remarks', index: 'Remarks', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                       { name: 'ExecutiveId', index: 'ExecutiveId', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                      { name: 'IPAddress', index: 'IPAddress', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                      { name: 'PaymentMode', index: 'PaymentMode', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                       { name: 'DeliverySlot', index: 'DeliverySlot', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                       { name: 'DeliveryCharges', index: 'DeliveryCharges', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                       { name: 'FreeDeliveryAmt', index: 'FreeDeliveryAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                      
                       { name: 'Comment', index: 'Comment', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                     { name: 'CouponNo', index: 'CouponNo', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                      { name: 'PaymentStatus', index: 'PaymentStatus', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                           { name: 'ResponseMessage', index: 'ResponseMessage', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},

                           { name: 'OrderType', index: 'OrderType', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                       ],
            rowNum: 10,
          
            mtype: 'GET',
             toolbar: [true, "top"],
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'OrderId',
            viewrecords: true,
            height: "100%",
            width:"700px",
            sortorder: 'desc',
            caption: "InProcess Orders List",
         
           ignoreCase: true,
                    
             
        });

      
      var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });
      


        $("#jQGridDemo").jqGrid('setGridParam',
         {

             onSelectRow: function (rowid, iRow, iCol, e) {
            

            $("#<%=ltOrderNo.ClientID%>").val( $('#jQGridDemo').jqGrid('getCell', rowid, 'OrderId'));
             $("#<%=ltOrderDate.ClientID%>").val( $('#jQGridDemo').jqGrid('getCell', rowid, 'strOD'));
            
            $("#<%=ltCustomer.ClientID%>").val( $('#jQGridDemo').jqGrid('getCell', rowid, 'CustomerName'));
             $("#<%=ltRecepientname.ClientID%>").val( $('#jQGridDemo').jqGrid('getCell', rowid, 'RecipientFirstName'));
            
            $("#<%=ltRecepientMobile.ClientID%>").val( $('#jQGridDemo').jqGrid('getCell', rowid, 'RecipientMobile'));
             $("#<%=ltRecepientPhone.ClientID%>").val( $('#jQGridDemo').jqGrid('getCell', rowid, 'RecipientPhone'));
            
            $("#<%=ltAddress.ClientID%>").val( $('#jQGridDemo').jqGrid('getCell', rowid, 'CompleteAddress'));
             $("#<%=ltOrderAmount.ClientID%>").val( $('#jQGridDemo').jqGrid('getCell', rowid, 'BillValue'));
             $("#<%=ltDeliverySlot.ClientID%>").val( $('#jQGridDemo').jqGrid('getCell', rowid, 'DeliverySlot'));
           
           $("#<%=ltItems.ClientID%>").val( $('#jQGridDemo').jqGrid('getCell', rowid, 'Comment'));
             OrderId = $('#jQGridDemo').jqGrid('getCell', rowid, 'OrderId');
              
//                var ProductId = [];
//                var ProductName = [];
//                var ProductVariation = [];
//                var Qty = [];
//                var Units = [];
//                var Price = [];
//                var DisPer = [];
//                var DisAmt = [];
//                var VatPer = [];
//                var VatAmt = [];
//                var ServiceTaxPer = [];
//                var ServiceTaxAmt = [];
//                var ServiceChargePer = [];
//                var ServiceChargeAmt = [];
//                var Amount = [];
//                var VariationId = [];

//               
//            $.ajax({
//            type: "POST",
//            data: '{ "Oid": "' + OrderId + '"}',
//            url: "managebilling.aspx/GetOrderDetail",
//            contentType: "application/json",
//            dataType: "json",
//            success: function (msg) {
//               var obj = jQuery.parseJSON(msg.d);
//               
//                for (var i = 0; i < obj.DetailData.length; i++) {

//                    ProductId[i] = obj.DetailData[i]["ProductId"];
//                    ProductName[i] = obj.DetailData[i]["ProductName"];
//                    ProductVariation[i] = obj.DetailData[i]["ProductVariation"];

//                    Qty[i] = obj.DetailData[i]["Qty"];

//                    Units[i] = obj.DetailData[i]["Units"];
//                    Price[i] = obj.DetailData[i]["Price"];
//                    DisPer[i] = obj.DetailData[i]["DisPer"];
//                    DisAmt[i] = obj.DetailData[i]["DisAmt"];

//                    VatPer[i] = obj.DetailData[i]["VatPer"];

//                    VatAmt[i] = obj.DetailData[i]["VatAmt"];
//                    ServiceTaxPer[i] = obj.DetailData[i]["ServiceTaxPer"];
//                    ServiceTaxAmt[i] = obj.DetailData[i]["ServiceTaxAmt"];
//                    ServiceChargePer[i] = obj.DetailData[i]["ServiceChargePer"];

//                    ServiceChargeAmt[i] = obj.DetailData[i]["ServiceChargeAmt"];

//                    Amount[i] = obj.DetailData[i]["Amount"];
//                    VariationId[i] = obj.DetailData[i]["VariationId"];
//                }

//                

//                
//                 },
//            complete: function (msg) {
//              
                     $.ajax({
                    type: "POST",
                    data: '{ "OI": "' + OrderId + '"}',
                    url: "inprocessorders.aspx/InsertTemp",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {
                        


                        var obj = jQuery.parseJSON(msg.d);   

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                    BindTempOrders();
                    }

                });
             
//            }


//            });



  
  

                   $('#dvPopup').dialog(
            {
            autoOpen: false,

            width:800,
            height:800,
            resizable: false,
            modal: true,
  
            });
            linkObj = $(this);
            var dialogDiv = $('#dvPopup');
            dialogDiv.dialog("option", "position", [150, 100]);
            dialogDiv.dialog('open');
            return false;
                
             }
         });
      


        


           $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }

                     );
         
    
        var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '700');
        

      }
        
    </script>

</asp:Content>

