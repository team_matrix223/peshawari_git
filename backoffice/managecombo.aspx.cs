﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_managecombo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {

                if (!User.IsInRole("ManageCombo"))
                {
                    Response.Redirect("default.aspx");

                }
                BindCategories();
                BindCategoriesType();
                ddlCategory.SelectedValue = "0";
            }
        }
        catch (Exception ex)
        {
        }
    }

    void BindCategoriesType()
    {

        ddlComboType.DataSource = new ComboTypeBLL ().GetAll();
        ddlComboType.DataValueField = "ComboTypeId";
        ddlComboType.DataTextField = "Title";
        ddlComboType.DataBind();
        ListItem li = new ListItem();
        li.Text = "--Select ComboType--";
        li.Value = "0";
        ddlComboType.Items.Insert(0, li);
    }
    [WebMethod]
    public static string Insert(int ComboId, int ComboTypeId, string Title, string Description,decimal Price,bool IsActive, string ProductIdArr,string QtyArr,string PriceArr)
    {
        Combo objCombo = new Combo()
        {
            ComboId  = ComboId,
            ComboTypeId = ComboTypeId,
            Title = Title.Trim(),
            Description = Description,
            Price=Price,
            IsActive = IsActive,          
            AdminId = 0,
        };
        string[] ProductIdA = ProductIdArr.Split(',');
        string[] QtyA = QtyArr.Split(',');
        string[] PriceA = PriceArr.Split(',');


        DataTable dt = new DataTable();
        dt.Columns.Add("ComboDetailId");
        dt.Columns.Add("ComboId");
        dt.Columns.Add("ProductId");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Price");


        for (int i = 0; i < ProductIdA.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ComboDetailId"] = 0;
            dr["ComboId"] = ComboId;
            dr["ProductId"] = ProductIdA[i];
            dr["Qty"] = QtyA[i];
            dr["Price"] =PriceA[i];

            dt.Rows.Add(dr);
        }
        JavaScriptSerializer ser = new JavaScriptSerializer();
        int status = new ComboBLL().InsertUpdate(objCombo, dt);
        var JsonData = new
        {
            Role = objCombo,
            Status = status
        };
        return ser.Serialize(JsonData);

    }
    public void reset()
    {
        ddlCategory.SelectedValue = "0";
    }
     
    public void BindCategories()
    {
        try
        {
            ddlCategory.DataSource = new CategoryBLL().GetByLevel(1);
            ddlCategory.DataValueField = "CategoryId";
            ddlCategory.DataTextField = "Title";
            ddlCategory.DataBind();
            ListItem li = new ListItem();
            li.Text = "--Select Category--";
            li.Value = "0";
            ddlCategory.Items.Insert(0, li);

        }
        finally { }

    }
    [WebMethod]
    public static string BindSubCategories(int CatId)
    {

        string SubCat = new CategoryBLL ().GetOptions(CatId);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            SubCatOptions = SubCat

        };
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string BindSubSubCategories(int SubCatId)
    {

        string SubSubCat = new CategoryBLL().GetOptions(SubCatId);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        int showrow = 0;
        if (SubSubCat.Length > 0)
        {
            showrow = 1;
        }
        else
        {
            showrow=0;
        }
        var JsonData = new
        {
            result=showrow,
            SubSubCatOptions = SubSubCat

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string GetProducts(int CatId,int SubCatId,int SubSubCatId)
    {
        Products  objProduct = new Products() { CategoryId  = CatId,SubCategoryId=SubCatId ,CategoryLevel3=SubSubCatId  };


        var ProductDetail = new ProductsBLL().ProductGetByCatIdSubCatId(objProduct);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            MasterData = objProduct,
            DetailData = ProductDetail
        };
        return ser.Serialize(JsonData);

    }
    [WebMethod]
    public static string GetProductDetail(int ComboId)
    {
        Combo objCombo = new Combo() { ComboId = ComboId };

        var ProductDetail = new ComboBLL ().GetProductDetailByID(objCombo);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            MasterData = objCombo,
            DetailData = ProductDetail
        };
        return ser.Serialize(JsonData);

    }
   
    
}