﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_managebillcancelation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!User.IsInRole("EditBill"))
            {
                Response.Redirect("default.aspx");

            }
            hdnDate.Value = DateTime.Now.ToShortDateString();
            BindCategories();
        }
    }


    void BindCategories()
    {

        ddlCategories.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategories.DataValueField = "CategoryId";
        ddlCategories.DataTextField = "Title";
        ddlCategories.DataBind();
        ListItem li = new ListItem();
        li.Text = "--Catgory Level 1--";
        li.Value = "0";
        ddlCategories.Items.Insert(0, li);
    }

    [WebMethod]

    public static string InsertCancelBill(Int32 BillId, Int32 OrderId, string CancelRemarks,string Status)
    {
        BillCancel objBillCancel = new BillCancel()
        {
            BillId = Convert.ToInt32(BillId),
            OrderId = Convert.ToInt32(OrderId),
            CancelRemarks = CancelRemarks,
            CancelDate = Convert.ToDateTime(DateTime.Now.ToShortDateString()),
            Status = Status,
          
        };


        JavaScriptSerializer ser = new JavaScriptSerializer();

        new BillCancelBLL().InsertUpdate(objBillCancel);
        var JsonData = new
        {
            Bill = objBillCancel

        };
        return ser.Serialize(JsonData);
    }




    [WebMethod]

    public static string InsertTemp(Int32 BI)
    {


        Bills objBill = new Bills()
        {
            BillId = Convert.ToInt32(BI),


        };

       
        JavaScriptSerializer ser = new JavaScriptSerializer();

        new BillBLL().InsertTemp(objBill);
        var JsonData = new
        {
            Bill = objBill

        };
        return ser.Serialize(JsonData);
    }



    //[WebMethod]

    //public static string UpdateStatusDeliver(Int32 OI)
    //{


    //    Order objOrder = new Order()
    //    {
    //        OrderId = Convert.ToInt32(OI),


    //    };


    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    new OrderBLL().UpdateStatusDeliver(objOrder.OrderId);
    //    var JsonData = new
    //    {
    //        Order = objOrder

    //    };
    //    return ser.Serialize(JsonData);
      
    //}




    [WebMethod]
    public static string GetTemparoryBills(int Bid)
    {
        BillDetail objBill = new BillDetail() { BillId = Bid };


        var BillDetail = new BillBLL().GetBillsTemp(objBill);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            TempBill = BillDetail
        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]

    public static string DeleteTempBills(Int32 BI, Int32 ProductId, Int64  VariationId)
    {
        BillDetail objBill = new BillDetail()
        {
            BillId = BI,
            ProductId = ProductId,
            VariationId = VariationId,
        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        new BillBLL().DeleteTempBills(objBill.BillId, objBill.ProductId, objBill.VariationId);
        var JsonData = new
        {
            Bill = objBill

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string UpdateQtyByProductId(int BillId, int ProductId, Int64 VariationId, int Qty, decimal Price, string Mode)
    {


        int status = new BillBLL().UpdateBillDetailQtyByProductId(BillId, ProductId, VariationId, Qty, Price, Mode);
        var JsonData = new
        {
            Status = status,
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    [WebMethod]

    public static string BillUpdate(Int32 BillId)
    {
        Bills objBill = new Bills()
        {
            BillId = Convert.ToInt32(BillId),

        };


        JavaScriptSerializer ser = new JavaScriptSerializer();

        new BillBLL().BillUpdate(objBill);
        var JsonData = new
        {
            Bill = objBill

        };
        return ser.Serialize(JsonData);
    }
}