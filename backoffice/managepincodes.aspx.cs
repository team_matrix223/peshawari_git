﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_managepincodes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!User.IsInRole("ManagePinCodes"))
            {
                Response.Redirect("default.aspx");

            }

            BindCities();
        }
    }



    void BindCities()
    {

        ddlCity.DataSource = new CitiesBLL().GetAll();
        ddlCity.DataValueField = "CityId";
        ddlCity.DataTextField = "Title";
        ddlCity.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose City--";
        li1.Value = "0";
        ddlCity.Items.Insert(0, li1);

    }


    [WebMethod]
    public static string Insert(int PinCodeId, string PinCode, string Sector,int City, bool IsActive)
    {




        PinCodes  objPCode = new PinCodes()
        {PinCodeId=PinCodeId,
         PinCode = PinCode.ToUpper(),
         Sector=Sector.ToUpper(),
         CityId = City,
            IsActive = IsActive
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new PinCodeDAL().InsertUpdate(objPCode);
        var JsonData = new
        {
            Role = objPCode,
            Status = status
        };
        return ser.Serialize(JsonData);



    }
}