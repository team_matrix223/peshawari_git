﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_userlogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Constants.AdminId] == null)

            Response.Redirect("login.aspx");
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        Users  objuser = new Users()
        {
            EmailId = txtName.Value,
            Password = txtPassword.Value

        };

        string  status = new UsersBLL().UserLoginCheck(objuser);
        if (status.ToString() == "-1")
        {
            Response.Write("<script>alert('Invalid User Name');</script>");
        }
        else if (status.ToString() == "-2")
        {
            Response.Write("<script>alert('Invalid Password');</script>");
        }
        else if (status.ToString() == "0")
        {   Response.Redirect("manageregister.aspx");
            Response.Write("<script>alert('First Register,Then SignIn');</script>");
         
        }
        else
        {

            Session["Roles"] = status;
            Session[Constants.UserId] = status;
            Session[Constants.Email] = txtName.Value.Trim();
            Response.Redirect("dashboard.aspx");
        }
    }

    protected void btnForgetpswd_Click(object sender, EventArgs e)
    {
        string UserName = txtName.Value;
        string Password = CommonFunctions.GenerateRandomPassword(8);
        new UsersBLL().ResetPassword(UserName, Password);

        try
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress("support@matrixposs.com");
            message.To.Add(new MailAddress(UserName));
            message.Subject = "Password Recovery";
            message.Body = "Dear User. Your new password is:" + Password;
            SmtpClient client = new SmtpClient();
            client.Send(message);
            Response.Write("<script>alert('Please check your inbox for new Password.');</script>");
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('Mailing Server Down. Please try again Later');</script>");
        }

    }
}