﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;

public partial class backoffice_managemygroups : System.Web.UI.Page
{


    protected string m_sMessage = "";
    protected string m_sSubMessage = "";
    public DataTable dtFinal = new DataTable();
    protected bool m_bShow = false;
    protected string ErrorMessage = "";
    protected string htmldata = "";
    public int m_CurrentPage = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!User.IsInRole("ManageGroups"))
            {
                Response.Redirect("default.aspx");

            }
                 
            BindCategories();
        }
    }


    [WebMethod]
    public static string Insert(int Id, string Title,string PhotoUrl1,string PhotoUrl2,string ShowOn,string LinkUrl, bool IsActive,bool HasSubGroup,string MobileImgUrl)
    {
        MyGroups objMyGroups = new MyGroups()
        {
            Id = Id,
            Title = Title.Trim().ToUpper(),
            PhotoUrl1 = PhotoUrl1,
            PhotoUrl2 = PhotoUrl2,
            ShowOn = ShowOn,
            LinkUrl = LinkUrl,
            IsActive = IsActive,
            HasSubGroup = HasSubGroup,
            AdminId = 0,
            MobileImgUrl = MobileImgUrl,

        };
        int status = new MyGroupsBLL().InsertUpdate(objMyGroups);
        var JsonData = new
        {
            MyGroup = objMyGroups,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    void BindCategories()
    {

        ddlCategory.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategory.DataValueField = "CategoryId";
        ddlCategory.DataTextField = "Title";
        ddlCategory.DataBind();
        ListItem lii = new ListItem();
        lii.Text = "--Category Level 1--";
        lii.Value = "0";
        ddlCategory.Items.Insert(0, lii);


        ddlCategoryp.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategoryp.DataValueField = "CategoryId";
        ddlCategoryp.DataTextField = "Title";
        ddlCategoryp.DataBind();
        ListItem li = new ListItem();
        li.Text = "--Category Level 1--";
        li.Value = "0";
        ddlCategoryp.Items.Insert(0, li);


        ddlBrand.DataSource = new BrandBLL().GetAll();
        ddlBrand.DataValueField = "BrandId";
        ddlBrand.DataTextField = "Title";
        ddlBrand.DataBind();
        ListItem l2 = new ListItem();
        l2.Text = "--Choose Brand--";
        l2.Value = "0";
        ddlBrand.Items.Insert(0, l2);

    }


    [WebMethod]
    public static string BindSubCategories(int CatId)
    {

        string SubCat = new CategoryBLL().GetOptions(CatId);


        string ProductHTML = "";// new ProductsBLL().GetProductHTMLByCategoryLevel2(CatId);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {

            SubCatOptions = SubCat,
           ProductHtml = ProductHTML

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindCategoryLevel3(int CatId)
    {

        string SubCat = new CategoryBLL().GetOptions(CatId);


        string ProductHTML = "";// new ProductsBLL().GetProductHTMLByCategoryLevel3(CatId);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {

            SubCatOptions = SubCat,
            ProductHtml = ProductHTML

        };
        return ser.Serialize(JsonData);
    }



    public void BindSubCategories(int ParentId, DropDownList ddlParent, DropDownList ddl)
    {
        try
        {
            if (ddlParent.SelectedIndex != 0)
            {

                m_bShow = true;
                ddl.DataSource = new CategoryBLL().GetByParentId(ParentId);
                ddl.DataTextField = "Title";
                ddl.DataValueField = "CategoryId";
                ddl.DataBind();
                ListItem li = new ListItem();
                li.Text = "--Category Level 2--";
                li.Value = "0";
                ddl.Items.Insert(0, li);
            }
            else
            {
                m_sMessage = "Please Select  Category Level 1";
            }
        }
        finally
        {
        }

    }

    public void BindCategoryLevel3(int ParentId, DropDownList ddl)
    {
        try
        {
            if (ParentId != 0)
            {

                m_bShow = true;
                ddl.DataSource = new CategoryBLL().GetByParentId(ParentId);
                ddl.DataTextField = "Title";
                ddl.DataValueField = "CategoryId";
                ddl.DataBind();
                ListItem li = new ListItem();
                li.Text = "--Category Level 3--";
                li.Value = "0";
                ddl.Items.Insert(0, li);
            }
            else
            {
                m_sMessage = "Please Select  Category";
                return;
            }
        }
        finally
        {
        }

    }

     

    [WebMethod]
    public static string FillProductsByCategoryLevel1(int CategoryId)
    {
       

        string Products = new ProductsBLL().GetByCategoryLevel1forgroups(CategoryId);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {

            ProductOptions = Products

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string FillProductsBySubcategory(int SubCategoryId)
    {


        string Products = new ProductsBLL().GetBySubCategoryforgroups(SubCategoryId);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {

            ProductOptions = Products

        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string FillProductsByCategoryLevel3(int SubCategoryId)
    {


        string Products = new ProductsBLL().GetByCategory3forgroups(SubCategoryId);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {

            ProductOptions = Products

        };
        return ser.Serialize(JsonData);
    }




   





    [WebMethod]

    public static string InsertGroupProducts(int Id, string Title,string PhotoUrl1,string PhotoUrl2,string ShowOn, bool IsActive,bool HasSubGroup,string ProductIdarr)
    {
        MyGroups objMyGroups = new MyGroups()
        {

            Id = Id,
            Title = Title.Trim().ToUpper(),
            PhotoUrl1 = PhotoUrl1,
            PhotoUrl2 = PhotoUrl2,
            ShowOn = ShowOn,
            IsActive = IsActive,
            HasSubGroup = HasSubGroup,
            AdminId = 0,

        };

        string[] ProductData = ProductIdarr.Split(',');
       

        DataTable dt = new DataTable();
        dt.Columns.Add("ProductId");
      
        for (int i = 0; i < ProductData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ProductId"] = Convert.ToInt32(ProductData[i]); ;
            
            dt.Rows.Add(dr);

        }


        JavaScriptSerializer ser = new JavaScriptSerializer();

        int st = new MyGroupsBLL().InsertGroupProducts(objMyGroups, dt);
        var JsonData = new
        {
            Status = st,
            GroupProduct = objMyGroups

        };
        return ser.Serialize(JsonData);
    }

}