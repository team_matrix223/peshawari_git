﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using DevExpress.XtraPrinting;

public partial class backoffice_Reports_rptproducts : System.Web.UI.Page
{
    public string PageNumber { get { return Request.QueryString["PageNumber"] != null ? Convert.ToString(Request.QueryString["PageNumber"]) : "0"; } }
    public string PageSize { get { return Request.QueryString["PageSize"] != null ? Convert.ToString(Request.QueryString["PageSize"]) : "0"; } }
    public string Cat2 { get { return Request.QueryString["Cat2"] != null ? Convert.ToString(Request.QueryString["Cat2"]) : "0"; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            rptproducts r = new rptproducts(Convert.ToInt32(PageNumber), Convert.ToInt32(PageSize), Convert.ToInt32(Cat2));
            r.CreateDocument();
            PdfExportOptions opts = new PdfExportOptions();
            opts.ShowPrintDialogOnOpen = true;
            r.ExportToPdf(ms, opts);
            ms.Seek(0, SeekOrigin.Begin);
            byte[] report = ms.ToArray();
            Page.Response.ContentType = "application/Pdf";
            Page.Response.Clear();
            Page.Response.OutputStream.Write(report, 0, report.Length);
            Page.Response.End();
        }
    }
}