﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class Admin_SubPageUpdate : System.Web.UI.Page
{
    Cms objCMS = new Cms();
    protected string m_sMessage = "";
    protected string m_sSubMessage = "";


    protected bool m_bShow = false;
    protected bool m_bShowMake = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (!User.IsInRole("EditSubPages"))
                {
                    Response.Redirect("default.aspx");

                }
           
             
                BindDDL();
            }

        }
        catch (Exception ex)
        {
            
         
        }

    }

 
    public void BindDDL()
    {
        try
        {
            objCMS.ParentPage = 0;
            ddlParentPage.DataSource = objCMS.GetByParentId();
            ddlParentPage.DataTextField = "Title";
            ddlParentPage.DataValueField = "PageId";
            ddlParentPage.DataBind();
            ddlParentPage.Items.Insert(0, "--Select--");

        }
        finally { }

    }
    public void BindDDLChild()
    {

        try
        {
            if (ddlParentPage.SelectedIndex != 0)
            {
                objCMS.ParentPage = Convert.ToInt16(ddlParentPage.SelectedItem.Value);
                DataSet ds = objCMS.GetByParentId();
                
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblMessage.Visible = false;
                    m_bShow = true;
                    ddlSubPage.DataSource = ds;
                    ddlSubPage.DataTextField = "Title";
                    ddlSubPage.DataValueField = "PageId";
                    ddlSubPage.DataBind();
                    ddlSubPage.Items.Insert(0, "--Select--");
                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "No Sub Pages Exist";

                }
            }
            else
            {
                m_sMessage = "Please Select a  Page";
                txtTitle.Text = "";
                EditorDescription.Value = "";
            }
        }
        catch (Exception ex)
        {
       
        }

    }


    public void GetDataByPageId()
    {
        try
        {
            m_bShow = true;
            if (ddlSubPage.SelectedIndex != 0)
            {
                objCMS.PageId = Convert.ToInt16(ddlSubPage.SelectedItem.Value);
                objCMS.GetById();
                txtTitle.Text = objCMS.Title;
                chkIsActive.Checked = objCMS.IsActive;
                txtKeywords.Text = objCMS.Keywords;
                txtPageDescription.Text = objCMS.PageDescription;
                txtPageTitle.Text = objCMS.MetaTitle;
                EditorDescription.Value = objCMS.Description;
            
            }
            else
            {
                m_sSubMessage = "Please Select Sub Page";
                txtTitle.Text = "";
                EditorDescription.Value = "";
                txtKeywords.Text = "";
                txtPageTitle.Text = "";
                txtPageDescription.Text = "";
              }
        }
        catch (Exception ex)
        {
            
           
        }

    }

    protected void ddlParentPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindDDLChild();
        }
        catch (Exception ex)
        {
           
           
        }
        finally
        {
        }
    }


    protected void ddlSubPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetDataByPageId();
        m_bShow = true;
    }
    protected void btnUpdatePage_Click(object sender, EventArgs e)
    {
        try
        {
            m_bShow = true;
           
            objCMS.PageId = Convert.ToInt16(ddlSubPage.SelectedItem.Value);
            objCMS.GetById();
            
            objCMS.Title = txtTitle.Text;
            objCMS.IsActive = chkIsActive.Checked;
            objCMS.Description = EditorDescription.Value;
            objCMS.Keywords = txtKeywords.Text;
            objCMS.PageDescription = txtPageDescription.Text;
            objCMS.MetaTitle = txtPageTitle.Text;
            objCMS.PunjabiDescription = "";
            objCMS.PunjabiTitle = "";
            objCMS.InsertUpdate();


            EditorDescription.Value = "";
            txtTitle.Text = "";
            chkIsActive.Checked = false;
            txtKeywords.Text = "";
            ddlSubPage.SelectedIndex = 0;
            txtPageTitle.Text = "";
            txtPageDescription.Text = "";
         
            string script = @"alert('Page Updated Successfully !!');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "filesDeleted", script, true);
        }
        catch (Exception ex)
        {
            
        }
    }
}
