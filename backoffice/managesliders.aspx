﻿<%@ Page Title="" Language="C#" MasterPageFile="admin.master" AutoEventWireup="true" CodeFile="managesliders.aspx.cs" Inherits="backoffice_managesliders" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/custom-css/master.css" rel="stylesheet" />
<form id="frmVid" runat="server">
<asp:HiddenField ID= "hdnPhoto" runat="server" Value ="0" />
 <div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Sliders</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">  
                        <table class="top_table">
                             <tr>
                                 <td class="Titles">
                                     Image:</td>
                                 <td>
                                      <asp:FileUpload ID="fuImage" runat="server" />
                                      <asp:Label ID = "lblimg"  runat="server"></asp:Label><asp:HiddenField ID="hdnlblimg" runat="server" />
                                 </td>
                             </tr>

                                              <tr>
                                 <td class="Titles">
                                     Url:</td>
                                 <td>
                                     <asp:TextBox ID="txtUrlName" runat="server" TextMode="MultiLine"></asp:TextBox>
                                 </td>
                             </tr>
                             <tr>
                                 <td class="Titles">
                                     IsActive:</td>
                                 <td>
                                       <input type="checkbox" id="chkIsActive" runat="server" checked="checked" data-index="2"  name="chkIsActive" />
                                 </td>
                             </tr>                             
                         </table>

                       <table class="category_table">
                           <tr>
                                 <td>
                                     <asp:Button ID="btnAdd" CssClass="btn btn-primary btn-small" runat="server" 
                                         Text="Add Photo" onclick="btnAdd_Click" />
                                         <asp:Button ID="btnUpdate" CssClass="btn btn-primary btn-small" runat="server" 
                                         Text="Update Photo" onclick="btnUpdate_Click"  />
                                          <asp:Button ID="btnCancel" CssClass="btn btn-primary btn-small" runat="server" 
                                         Text="Cancel" onclick="btnCancel_Click"  />
                                 </td>
                             </tr>
                       </table>

                         </div>

                         </div>

                         
               <div id="rightnow" style="margin-top:10px">
                    <h3 class="reallynow">
                        <span>Manage Photo Sliders</span>
                      
                        <br />
                    </h3>


                            <div class="youhave" style="padding-left:0px">

        <asp:DataList ID="DataList1" DataKeyField="SliderId" runat="server"  BorderColor="#666666"

            BorderStyle="None" BorderWidth="2px"    CellPadding="5" CellSpacing="2"

            Font-Names="Verdana" Font-Size="Small"  GridLines="Both" RepeatColumns="5" RepeatDirection="Horizontal"

           oneditcommand="DataList1_EditCommand">

            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />

            <HeaderStyle   Font-Bold="True" Font-Size="Large" ForeColor="White"

                HorizontalAlign="Center" VerticalAlign="Middle" />

             

            <ItemStyle BackColor="White" ForeColor="Black" BorderWidth="2px" />

            <ItemTemplate>
        <div style="margin:1px;float:left">
        
        
                 <img src="../SliderImages/<%#Eval("ImageUrl") %>" height="125px" width="125px" />

                <br />

                    
                  
                 <asp:HiddenField ID="hdnSliderId" runat="server" Value ='<%# Eval("SliderId") %>' />
                            <asp:HiddenField ID="hdnactive" runat="server" Value ='<%# Eval("IsActive") %>' />
                             <asp:HiddenField ID="hdnurl" runat="server" Value ='<%# Eval("ImageUrl") %>' />
                        <asp:HiddenField ID="hdnUrlName" runat="server" Value ='<%# Eval("UrlName") %>' />
                
                <br />
                <asp:LinkButton runat="server" ID="Lnkedit" CausesValidation="false" ForeColor="Blue" Font-Bold="false"
                    CommandName="edit" >
                    Edit
                </asp:LinkButton>
             

        </div>
       

            </ItemTemplate>
          


        </asp:DataList>

    </div>

			  </div>
              </div>
 

                        
                 
                   
                

 
    
</form>

</asp:Content>


