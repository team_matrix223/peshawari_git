﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="productlist.aspx.cs" Inherits="backoffice_productlist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
        <link href="css/custom-css/master.css" rel="stylesheet" />
        <link href="css/custom-css/master.css" rel="stylesheet" />

    <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

    <style>
#sortable { list-style-type: none;   padding: 0; width: 100%; }
#sortable li { margin: 0 3px 3px 3px; padding-left: 5px; font-size: 12px; height: 13px; }
 #sortableFeatured { list-style-type: none;   padding: 0; width: 100%; }
#sortableFeatured li { margin: 0 3px 3px 3px; padding-left: 5px; font-size: 12px; height: 13px; }

  .ui-state-default2 
{
background:#e6e6e6;
border:1px solid #d3d3d3;
color:#555555;
font-weight:normal;
text-indent:19px;
padding:5px;

}



.ui-state-default1  
{
background:#e6e6e6;
border:1px solid #d3d3d3;
color:#555555;
font-weight:normal;
text-indent:19px;
padding:5px;

}
</style>


 <script type ="text/javascript">
     function BindCat1() {
         $.ajax({
             type: "POST",
             data: '',
             url: "productlist.aspx/BindCat1",
             contentType: "application/json",
             dataType: "json",
             success: function (msg) {

                 var obj = jQuery.parseJSON(msg.d);


                 $("#<%=ddlCat1.ClientID%>").html(obj.Category1);
                

             }, error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function (msg) {
                 $.uiUnlock();

             }

         });
     }
  
     $(document).ready(
     function () {
         BindCat1();
         $("#<%=ddlCat2.ClientID %>").change(
    function () {
        var sid = $("#<%=ddlCat2.ClientID%>").val();
        BindGrid(sid);
    });
         $("#<%=ddlCat1.ClientID %>").change(
     function () {

       
        // var sid = $("#<%=ddlCat2.ClientID%>").val();
       //  if (sid == "0") {

       //      return;
       //  }
      
         var cid = $("#<%=ddlCat1.ClientID%>").val();
       
         $.ajax({
             type: "POST",
             data: '{ "CatId": "' + cid + '"}',
             url: "productlist.aspx/BindCat2",
             contentType: "application/json",
             dataType: "json",
             success: function (msg) {

                 var obj = jQuery.parseJSON(msg.d);
                 

                 $("#<%=ddlCat2.ClientID%>").html(obj.Category2);


             }, error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function (msg) {
               //  $.uiUnlock();

             }

         });
     });
     
        

         $("#btnRefresh").click(
         function () {
             var sid = $("#<%=ddlCat2.ClientID%>").val();
             BindGrid(sid);
         }
         );
     }
     );
 </script>



        <form runat="server" id="frm1">


<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Product List</span>
                      
                        <br />
                    </h3>
                       <div class="youhave">
                   <table cellpadding="0" cellspacing="0" border="0" class="top_table">
                     <tr><td class="headings">Choose CategoryLevel1:</td><td>  
                         <asp:DropDownList ID="ddlCat1" runat="server"></asp:DropDownList>
                  
                     </td></tr>
                    <tr><td class="headings">Choose CategoryLevel2:</td><td>  
                     
               
                         <asp:DropDownList ID="ddlCat2" runat="server"></asp:DropDownList>
                     </td></tr>
                       </table>
                           </div>
				   <div class="youhave" style="overflow:scroll;width:730px">
                    <table id="jQGridDemo" class="midle_table">
                </table>

                       <div id="btnRefresh" class="btn btn-primary btn-small category_table">Refresh</div>
                <div id="jQGridDemoPager">
                </div>		 
       
                    </div>
			  </div>
               
            
                
         
               


                

            </div>
</form>

 
  	
  	
  

                   <script type="text/javascript">

                   function BindGrid(CatLevel2)
                   {
          var lastsel;
    jQuery("#jQGridDemo").GridUnload();
        jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/ProductList.ashx?CatLevel2=' + CatLevel2,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: [ 'VariationId', 'ProductId', 'Name', 'ShortName', 'Description', 'DOC', 'Code', 'Brand', 'Unit', 'Qty', 'Price', 'Mrp', 'Type', 'Units', 'Active', ],
            colModel: [
                        { name: 'VariationId', key: true, width: 40, index: 'VariationId', stype: 'text', sortable: true, editable: false, editrules: { required: true }, hidden: false },
                        { name: 'ProductId', index: 'ProductId', width: 20, stype: 'text', sorttype: 'int', hidden: true },
   		                { name: 'Name', index: 'Name',  stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
                        { name: 'ShortName', index: 'ShortName', stype: 'text', sortable: true, editable: true, editrules: { required: false }, hidden: true },
                        { name: 'Description', index: 'Description', stype: 'text', sortable: true, editable: true, hidden: true },
                        { name: 'DOC', index: 'DOC',   stype: 'text', sortable: true, editable: true ,editrules: { required: false },hidden:true},
                        { name: 'ItemCode', index: 'ItemCode', width: 60, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                        { name: 'BrandName', index: 'BrandName',width:60,  stype: 'text', sortable: true, editable: false ,editrules: { required: true }},
                        //{ name: 'Unit', index: 'Unit',width:50,    stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:false},
                        {
                            name: 'Unit', index: 'Unit', width: 60,
                            sortable: true,
                            align: 'center',
                            editable: true,
                            cellEdit: true,
                            edittype: 'select',
                            formatter: 'select',

                            editoptions: { value: getAllSelectOptions() }
                        },
                        { name: 'Qty', index: 'Qty', width:50,    stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:false},
                        { name: 'Price', index: 'Price', width:50,  stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
                        { name: 'Mrp', index: 'Mrp', width:50,   stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
                        //{ name: 'Type', index: 'Type', width: 80, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                        
                         {
                             name: 'Type', index: 'Type', width: 70,
                             sortable: true,
                             align: 'center',
                             editable: true,
                             cellEdit: true,
                             edittype: 'select',
                             formatter: 'select',

                             editoptions: { value: getAllTypeOptions() }
                         },
                        { name: 'Units', index: 'Units',width:60,    stype: 'text', sortable: true, editable: false ,editrules: { required: true },hidden:true},
                        
                       
                       { name: 'IsActive', index: 'IsActive', width: 50, editable: true,edittype:"checkbox",editoptions: { value:"true:false" } , formatter: "checkbox", formatoptions: { disabled: true} },
                           
                       ],
            rowNum: 10,
            toolbar: [true, "top"],
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'VariationId',
            viewrecords: true,
            height: "100%",
            width:"400px",
            sortorder: 'asc',
            ignoreCase: true,
            caption: "Product List", onSelectRow: function (id) {
             if (id && id !== lastsel) {
                 jQuery('#jQGridDemo').jqGrid('restoreRow', lastsel);
                 jQuery('#jQGridDemo').jqGrid('editRow', id, true); 
                 lastsel = id;
             }
         },
         
            editurl: 'handlers/ProductList.ashx?CatLevel2=0',
         
                    
             
        });
        function getAllSelectOptions() {
            var units = {
                '': 'Units', 'nos': 'NOS', 'kg': 'KG',
                'gm': 'GRAM', 'lt': 'LITER', 'pcs': 'PIECE','ml':'ML'
            };

            return units;

        }
        function getAllTypeOptions() {
            var types = {
                '': 'Type', 'carton': 'CARTON', 'bottle': 'BOTTLE',
                'poly bag': 'POLY BAG', 'poly pack': 'POLY PACK', 'packet': 'PACKET', 'cup': 'CUP', 'box': 'BOX', 'tin': 'TIN', 'pouch': 'POUCH', 'jar': 'JAR'
            };

            return types;

        }
  
        var $grid = $("#jQGridDemo");
                       // fill top toolbar
        $('#t_' + $.jgrid.jqID($grid[0].id))
            .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
        $("#globalSearchText").keypress(function (e) {
            var key = e.charCode || e.keyCode || 0;
            if (key === $.ui.keyCode.ENTER) { // 13
                $("#globalSearch").click();
            }
        });
        $("#globalSearch").button({
            icons: { primary: "ui-icon-search" },
            text: false
        }).click(function () {
            var postData = $grid.jqGrid("getGridParam", "postData"),
                colModel = $grid.jqGrid("getGridParam", "colModel"),
                rules = [],
                searchText = $("#globalSearchText").val(),
                l = colModel.length,
                i,
                cm;
            for (i = 0; i < l; i++) {
                cm = colModel[i];
                if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                    rules.push({
                        field: cm.name,
                        op: "cn",
                        data: searchText
                    });
                }
            }
            postData.filters = JSON.stringify({
                groupOp: "OR",
                rules: rules
            });
            $grid.jqGrid("setGridParam", { search: true });
            $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
            return false;
        });

        $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh: false,
                       edit: false,
                       add: false,
                       del: false,
                       search: true,
                       searchtext: "Search",
                       addtext: "Add",
                   },

                   {//SEARCH
                       closeOnEscape: true

                   }

                     );
    
     $("#jQGridDemo").jqGrid('inlineNav', '#jQGridDemoPager',
                    {
                        edit: true,
                        editicon: "ui-icon-pencil",
                        add: true,
                        addicon: "ui-icon-plus",
                        save: true,
                        saveicon: "ui-icon-disk",
                        cancel: true,
                        cancelicon: "ui-icon-cancel",

                        editParams: {
                            keys: false,
                            oneditfunc: null,
                            successfunc: function (val) {
                           
                                if (val.responseText != "") {
                                    alert(val.responseText);
                                    $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                                }
                            },
                            url: null,
                            extraparam: {
                                VariationId: function () {
                                    var sel_id = $('#jQGridDemo').jqGrid('getGridParam', 'selrow');
                                    var value = $('#jQGridDemo').jqGrid('getCell', sel_id, 'VariationId');
                                  
                                    return value;
                                }
                            },
                            aftersavefunc: null,
                            errorfunc: null,
                            afterrestorefunc: null,
                            restoreAfterError: true,
                            mtype: "POST"
                        },
                        addParams: {
                            useDefValues: true,
                            addRowParams: {
                                keys: true,
                                extraparam: {},
                                // oneditfunc: function () { alert(); },
                                successfunc: function (val) {
                                    if (val.responseText != "") {
                                        alert(val.responseText);
                                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                                    }
                                }
                            }
                        }
                    }
        );           

              }
                   
              
    </script>
</asp:Content>

