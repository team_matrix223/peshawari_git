﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
 
public partial class Admin_SubPage : System.Web.UI.Page
{
    Cms objCMS = new Cms();
    protected bool m_bShow = false;
 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (!User.IsInRole("AddSubPages"))
            {
                Response.Redirect("default.aspx");

            }

            try
            {
      
                BindDDLParentPage();
            }
            catch (Exception ex)
            {
               
                 
            }
        }
     
    }

 








    public void BindDDLParentPage()
    {
        try
        {
            objCMS.ParentPage = 0;
            ddlParentPage.DataSource = objCMS.GetByParentId();
            ddlParentPage.DataTextField = "Title";
            ddlParentPage.DataValueField = "PageId";
            ddlParentPage.DataBind();
            ddlParentPage.Items.Insert(0, "--Select--");

        }
        finally { }

    }
    protected void btnCreatePage_Click(object sender, EventArgs e)
    {

        try
        {
             
           
            objCMS.PageId = 0;

            objCMS.Description = EditorDescription.Value;
            objCMS.ParentPage = Convert.ToInt16(ddlParentPage.SelectedItem.Value);
            objCMS.Title = txtTitle.Text;
            objCMS.IsActive = chkIsActive.Checked;
            objCMS.Keywords = txtKeywords.Text;
            objCMS.PageDescription = txtPageDescription.Text;
            objCMS.MetaTitle = txtPageTitle.Text;
            objCMS.IsFullPage = chkFullPage.Checked;
            objCMS.PunjabiDescription ="";
            objCMS.PunjabiTitle ="";



            objCMS.InsertUpdate();
            EditorDescription.Value = "";
            txtTitle.Text = "";
            txtKeywords.Text = "";
            chkFullPage.Checked = false;
            chkIsActive.Checked = false;
            txtPageTitle.Text = "";
            txtPageDescription.Text = "";
            

            string script = @"alert('Page Created Successfully !!');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "filesDeleted", script, true);
           
        }
        catch (Exception ex)
        {
             
            
        }
    }
}