﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_managealbum : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!User.IsInRole("CategoryLevel2"))
            {
                Response.Redirect("default.aspx");

            }
            BindCategories();
        }
    }

    void BindCategories()
    {

        ddlCategory.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategory.DataValueField = "CategoryId";
        ddlCategory.DataTextField = "Title";
        ddlCategory.DataBind();
        ListItem li = new ListItem();
        li.Text = "--Catgory Level 1--";
        li.Value = "0";
        ddlCategory.Items.Insert(0, li);
    }
    [WebMethod]
    public static string Insert(int id, string title, bool isActive, int CatId, string desc, string CatTitle, string MetaTitle, string MetaKeyword, string MetaDescription)
    {




        Category objCategory = new Category()
        {
            Title = title.Trim().ToUpper(),
            IsActive = isActive,
            CategoryId = id,
            Description = desc,
            AdminId = 0,
            ParentId = CatId,
            Level = 2,
            MetaDescription = MetaDescription,
            MetaKeyword = MetaKeyword,
            MetaTitle = MetaTitle

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new CategoryBLL().InsertUpdate(objCategory);
        var JsonData = new
        {
            Role = objCategory,
            Status = status
        };
        return ser.Serialize(JsonData);



    }
}