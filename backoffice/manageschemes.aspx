﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="manageschemes.aspx.cs" Inherits="backoffice_manageschemes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
       <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/master.css" rel="stylesheet" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">
    var SchemePhotoUrl = "";
    var SchemeId = 0;
    var StatusProducts = "";
    var VariationId = 0;
    var ProductCollection = [];
    var FreeProductCollection = [];
    function clsProduct() {
        this.VariationId = 0;
    }
    $(function () {
        $("#txtStartDate").datepicker({
            yearRange: '1900:2030',
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
    });
    $(function () {
        $("#txtEndDate").datepicker({
            yearRange: '1900:2030',
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
    });

    function ResetControls() {
        $("#txtStartDate").val("");
        $("#txtEndDate").val("");
        $("#chkPackScheme").prop("checked",false);
        SchemePhotoUrl = "";
        ProductCollection = [];
        FreeProductCollection = [];
        VariationId=0;
        SchemeId = 0;
        $("#txtQty").val("0");
        var txtTitle = $("#txtTitle");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
        $('#tbMainProductList tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
        $('#tbFreeProductList tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
        txtTitle.focus();
        txtTitle.val("");
        txtTitle.focus();
        btnAdd.css({ "display": "block" });
        btnAdd.html("Add Scheme");
        $("#txtDescription").val("");
        btnUpdate.css({ "display": "none" });
        btnUpdate.html("Update Scheme");
        $("#chkIsActive").prop("checked", "checked");
        $("#btnReset").css({ "display": "none" });
        $("#hdnId").val("0");
        validateForm("detach");
        $("#FileUpload1").val("");
    }
    function InsertUpdate() {
        if (!validateForm("frmScheme")) {
            return;
        }
        var Id = SchemeId;
        var Title = $("#txtTitle").val();
        if ($.trim(Title) == "") {
            $("#txtTitle").focus();

            return;
        }
       var Qty= $("#txtQty").val();
        var Description = $("#txtDescription").val();
        var StartDate = $("#txtStartDate").val();
        var EndDate = $("#txtEndDate").val();
        var IsActive = false;
        var IsPackScheme = false;
        if ($("#chkPackScheme").is(":checked")) {
            IsPackScheme = true;
        }
        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
        }
        var VariationIdArrM = [];
        var VariationIdArrF = [];
        if (ProductCollection.length == 0) {
            alert("Please first add a in Master Products");
            return;
        }
        if (FreeProductCollection.length == 0) {
            alert("Please first add a in Free Products");
            return;
        }
        var Qty = $("#txtQty").val();
        if ($.trim(Qty) == "0") {
            $("#txtQty").focus();

            return;
        }
        for (var i = 0; i < ProductCollection.length; i++) {
            VariationIdArrM[i] = ProductCollection[i]["VariationId"];
        }
        for (var j = 0; j < FreeProductCollection.length; j++) {
            VariationIdArrF[j] = FreeProductCollection[j]["VariationId"];
        }

        var photourl = "";
        var fileUpload = $("#FileUpload1").get(0);
        var files = fileUpload.files;

        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            if (files[i].name.length > 0) {
                photourl = files[i].name;
            }

            data.append(files[i].name, files[i]);
        }

        if (Id == 0 && photourl == "") {
            alert("Please select Image for Scheme");
            return;
        }
     
        var options = {};
        options.url = "handlers/FileUploader.ashx?foldername=SchemeImages";
        options.type = "POST";
        options.async = false;
        options.data = data;
        options.contentType = false;
        options.processData = false;
        options.success = function (msg) {

            photourl = msg;

        };
        options.error = function (err) { };

        $.ajax(options);


        $.uiLock('');
        if (photourl.length > 0) {
            SchemePhotoUrl = photourl;
        }
        $.ajax({
            type: "POST",
            data: '{"SchemeId":"' + Id + '", "Title": "' + Title + '", "Description": "' + Description + '","PhotoUrl":"' + SchemePhotoUrl + '","Qty": "' + Qty + '","PackScheme":"'+ IsPackScheme+'","StartDate":"' + StartDate + '","EndDate":"' + EndDate + '","IsActive": "' + IsActive + '","VariationIdArrM": "' + VariationIdArrM + '","VariationIdArrF":"' + VariationIdArrF + '"}',
            url: "manageschemes.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == 0) {

                    alert("Insertion Failed.Combo with duplicate name already exists.");
                    return;
                }

                if (Id == "0") {
                    jQuery("#jQGridProductList").GridUnload();
                    ResetControls();
                    BindGrid();
                    alert("Scheme is added successfully.");
                }
                else {
                    jQuery("#jQGridProductList").GridUnload();
                    ResetControls();
                    BindGrid();
                    alert("Scheme is Updated successfully.");
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });
    }
    $(document).ready(function () {
        BindGrid();
        $("#btnReset").click(function () {
            ResetControls();
        });
        $("#chkPackScheme").change(function () {
            if ($('#chkPackScheme').is(":checked")) {
                $("#txtQty").attr("readonly", true);
                $("#txtQty").val(ProductCollection.length);
            }
            else {
                $("#txtQty").attr("readonly", false);
                $("#txtQty").val(0);
            }
         
        });
        
        $("#btnAdd").click(function () {
            SchemeId = 0;
            InsertUpdate();
        });
        $("#btnUpdate").click(function () {
            InsertUpdate();
        });
        $("#btnAddMasterProducts").click(function () {


            $('#dvPopupProduct').dialog(
          {
              autoOpen: false,

              width: 400,
              height: 400,
              resizable: false,
              modal: true,

          });
            linkObj = $(this);
            var dialogDiv = $('#dvPopupProduct');
            dialogDiv.dialog("option", "position", [420, 200]);
            dialogDiv.dialog('open');
            StatusProducts = "Master";
            BindProductList();
        });

        $("#btnAddFreeProducts").click(function () {
            $('#dvPopupProduct').dialog(
          {
              autoOpen: false,

              width: 400,
              height: 400,
              resizable: false,
              modal: true,
          });
            linkObj = $(this);
            var dialogDiv = $('#dvPopupProduct');
            dialogDiv.dialog("option", "position", [420, 200]);
            dialogDiv.dialog('open');
            StatusProducts = "Free";
            BindProductList();
        });
        $(document).on("click", "#btnDeleteFree", function (event) {
            var RowIndex = Number($(this).closest('tr').index());
            var tr = $(this).closest("tr");
            tr.remove();
            FreeProductCollection.splice(RowIndex, 1);
        });
        $(document).on("click", "#btnDelete", function (event) {
            var RowIndex = Number($(this).closest('tr').index());
            var tr = $(this).closest("tr");
            tr.remove();
           ProductCollection.splice(RowIndex, 1);
            $("#txtQty").val(ProductCollection.length);
        });
    });
    function BindProductList() {
        jQuery("#jQGridProductList").GridUnload();
        jQuery("#jQGridProductList").jqGrid({
            url: 'handlers/ManageProducts.ashx?CatId=' + 0,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
            colNames: ['VariationId','ProductId', 'Image', 'Name','Price'],
            colModel: [
                 { name: 'VariationId', key: true, index: 'VariationId', width: '100px', stype: 'text', sorttype: 'int', hidden: true },
                       { name: 'ProductId', key: true, index: 'ProductId', width: '100px', stype: 'text', sorttype: 'int', hidden: true },
                         { name: 'PhotoUrl', index: 'PhotoUrl', width: '60px', align: "center", editable: true, formatter: imageFormat, unformat: imageUnFormat },
                        { name: 'Name', index: 'Name', width: '150px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                           { name: 'Price', index: 'Price', width: '150px', stype: 'text', sortable: true, editable: true, editrules: { required: true },hidden:true  },
            ],
            rowNum: 10,

            mtype: 'GET',
            toolbar: [true, "top"],
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridProductListPager',
            sortname: 'VariationId',
            viewrecords: true,
            height: "100%",
            width: "500px",
            sortorder: 'asc',
            caption: "Product List",
            ignoreCase: true,
            editurl: 'handlers/ManageProducts.ashx',

        });

        function imageFormat(cellvalue, options, rowObject) {
            return '<img src="../ProductImages/T_' + cellvalue + '" style="width:20px;height:20px"/>';
        }
        function imageUnFormat(cellvalue, options, cell) {
            return $('img', cell).attr('src');
        }


        $("#jQGridProductList").jqGrid('setGridParam',
{
    onSelectRow: function (rowid, iRow, iCol, e) {
   
        var Vid = $('#jQGridProductList').jqGrid('getCell', rowid, 'VariationId');
       var PName = $('#jQGridProductList').jqGrid('getCell', rowid, 'Name');
       var Url= $('#jQGridProductList').jqGrid('getCell', rowid, 'PhotoUrl');
       var pr = $('#jQGridProductList').jqGrid('getCell', rowid, 'Price');

      
       if (StatusProducts == "Master") {
           var item = $.grep(ProductCollection, function (item) {
               return item.VariationId == Vid;
           });
           if (item.length) {
               alert("Product is already added");
               return;
           }
           $.ajax({
               type: "POST",
               data: '{ "VariationId": "' + Vid + '"}',
               url: "manageschemes.aspx/CheckAllReadyExistId",
               contentType: "application/json",
               dataType: "json",
               success: function (msg) {                
                   var obj = jQuery.parseJSON(msg.d);
                   if (obj.Status != 0) {
                       alert("Choosed Product is already alloted to another Scheme");
                       return;
                   }
                   else {
                       $("#tbMainProductList").css({ "display": "block" });
                       var tr = "<tr><td><img style='height:30px;width:30px' src='../ProductImages/" + Url + "' /></td><td>" + PName + "</td><td>" + pr + "</td><td><div id='btnDelete' style='cursor:pointer'><a >Delete</a></div></td></tr>";
                       $("#tbMainProductList").append(tr);
                       var CO = new clsProduct();
                       CO.VariationId = Vid;
                       CO.ProductName = PName;
                       CO.Price = pr;
                       CO.PhotoUrl = Url;
                       ProductCollection.push(CO);
                       if ($("#chkPackScheme").is(":checked")) {

                           $("#txtQty").val(ProductCollection.length);
                       }
                   }

               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {

                   $.uiUnlock();
               }



           });
         
        }
       else {
           var item = $.grep(FreeProductCollection, function (item) {
               return item.VariationId == Vid;
           });
           if (item.length) {
               alert("Product is already added");
               return;
           }
            $("#tbFreeProductList").css({ "display": "block" });
            var tr = "<tr><td><img style='height:30px;width:30px' src='../ProductImages/" + Url + "' /></td><td>" + PName + "</td><td>" + pr + "</td><td><div id='btnDeleteFree' style='cursor:pointer'><a >Delete</a></div></td></tr>";
            $("#tbFreeProductList").append(tr);

            var CO = new clsProduct();
            CO.VariationId = Vid;
            CO.ProductName = PName;
            CO.Price = pr;
            CO.PhotoUrl = Url;
            FreeProductCollection.push(CO);
        }
       
        //tbFreeProductList
       
    }
});


        var DataGrid = jQuery('#jQGridProductList');
        DataGrid.jqGrid('setGridWidth', '350');

        var $grid = $("#jQGridProductList");
        // fill top toolbar
        $('#t_' + $.jgrid.jqID($grid[0].id))
            .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
        $("#globalSearchText").keypress(function (e) {
            var key = e.charCode || e.keyCode || 0;
            if (key === $.ui.keyCode.ENTER) { // 13
                $("#globalSearch").click();
            }
        });
        $("#globalSearch").button({
            icons: { primary: "ui-icon-search" },
            text: false
        }).click(function () {
            var postData = $grid.jqGrid("getGridParam", "postData"),
                colModel = $grid.jqGrid("getGridParam", "colModel"),
                rules = [],
                searchText = $("#globalSearchText").val(),
                l = colModel.length,
                i,
                cm;
            for (i = 0; i < l; i++) {
                cm = colModel[i];
                if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                    rules.push({
                        field: cm.name,
                        op: "cn",
                        data: searchText
                    });
                }
            }
            postData.filters = JSON.stringify({
                groupOp: "OR",
                rules: rules
            });
            $grid.jqGrid("setGridParam", { search: true });
            $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
            return false;
        });

        $('#jQGridProductList').jqGrid('navGrid', '#jQGridProductListPager',
                   {
                       refresh: false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                   },

                   {//SEARCH
                       closeOnEscape: true

                   }

                     );


    }
    </script>
    <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Schemes</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">

                   <table id="frmScheme">
                   <tr>
                   <td>
                   <table cellpadding="0" cellspacing="0" border="0" class="top_table">
                                    
                     <tr><td class="headings">Title:</td><td>  <input type="text"  name="txtTitle" class="inputtxt validate required alphanumeric"   data-index="1" id="txtTitle" /></td></tr>
                                       
                    <tr><td class="headings">Description:</td><td> 
                    
                    <textarea id="txtDescription" rows="3"></textarea>
                      
                     
                     </td></tr>
                     <tr><td class="headings">IsActive:</td><td>     <input type="checkbox" id="chkIsActive" checked="checked" data-index="2"  name="chkIsActive" />
                                            </td></tr>
     
                                       
                     <tr><td class="headings">StartDate:</td><td>  <input type="text"  name="txtStartDate" class="form-control input-small validate required"   data-index="1" id="txtStartDate" /></td></tr>

                                       
                     <tr><td class="headings">EndDate:</td><td>  <input type="text"  name="txtEndDate" class="form-control input-small validate required"   data-index="1" id="txtEndDate" /></td></tr>
                       <tr><td class="headings">Image:</td><td><input type="file" id="FileUpload1" />
    <button id="btnUpload" style="display:none">Upload</button></td></tr>
                     </table>
                   
                   </td>
                   <td valign="top">
                       <table>
                           <tr>
                               <td>
                                   <div id="dvPopupProduct"  style="display:none;">
                    	          <table id="jQGridProductList">
    </table>
    <div id="jQGridProductListPager">
    </div>
                                       </div>
                               </td>
                           </tr>
                           <tr>
                               <td>

                                                                 </td>
                           </tr>
                       </table>
            
  
                  </td>
                   </tr>

                   <tr>
                   <td colspan="100%">
                   
                   <table>
                       <tr><td ><table><tr><td><h4><b>Master Products </b></h4></td><td><div id="btnAddMasterProducts"  class="btn btn-primary btn-small">Add New</div></td>

                       </tr>
                           </table>
                           </td>
                           </tr>
                        <tr>
                        <td>
                        
                        <table id="tbMainProductList" style="display:block" >
                        <thead>
                        <tr style="background:#294145;color:white">
                          
                              <th  style = "width:40px">Image</th>
                        <th   style = "width:150px">Product Name</th>
                            <th  style = "width:40px">Price</th>
                          <th  style = "width:30px">Delete</th>
                         
                        </tr>


                        </thead>
                        <tbody class="customTable">
                        
                        </tbody>


                        </table>
                        </td>
                        
                        </tr>
                       <tr><td colspan="100%"><table><tr>
                           <td class="headings">Qty</td><td><input type="text" id="txtQty"   placeholder="Qty" value="0"  class="inputtxt validate required valNumber" style="width:30px;height:10px"/></td>
                           <td><input  type="checkbox" id="chkPackScheme"/><label for="chkPackScheme" >Pack Scheme</label></td>
                           </tr></table>
                           </td>
                       </tr>
                  <tr><td ><table><tr><td><h4><b>Free Products </b></h4></td><td><div id="btnAddFreeProducts"  class="btn btn-primary btn-small">Add New</div></td>

                       </tr>
                           </table>
                           </td>
                           </tr>
                        <tr>
                        <td>
                        
                        <table id="tbFreeProductList" style="display:block" >
                        <thead>
                        <tr style="background:#294145;color:white">
                          
                              <th  style = "width:40px">Image</th>
                        <th   style = "width:150px">Product Name</th>
                            <th  style = "width:40px">Price</th>
                          <th  style = "width:30px">Delete</th>
                         
                        </tr>


                        </thead>
                        <tbody class="customTable">
                        
                        </tbody>


                        </table>
                        </td>
                        
                        </tr>
                       <tr>
                                            
                                            <td colspan="100%"  >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd"  class="btn btn-primary btn-small" >Save Scheme</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" >Update Scheme</div></td>
                                            <td><div id="btnReset"  class="btn btn-primary btn-small" style="display:none;width:60px;" >Cancel</div></td>
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>
                        </table>
                   
                   </td>
                   </tr>
               
                   </table>


                   
                    			 
       
                    </div>
			  </div>
               


               <div id="Div1">
                    <h3 class="reallynow">
                        <span>Manage Schemes </span>
                      
                        <br />
                    </h3>
				    <div class="youhave">
                    
      	          <table id="
                        
                        
                        
                        ">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>
			  </div>

            </div>
</form>

              <script type="text/javascript">
                  function BindGrid() {
                      jQuery("#jQGridDemo").GridUnload();
                      jQuery("#jQGridDemo").jqGrid({
                          url: 'handlers/manageschemes.ashx',
                          ajaxGridOptions: { contentType: "application/json" },
                          datatype: "json",

                          colNames: ['SchemeId', 'Image',  'Title', 'Description', 'Qty','PackScheme', 'StartDate','EndDate','IsActive'],
                          colModel: [
                                   { name: 'SchemeId', key: true, index: 'SchemeId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                   { name: 'PhotoUrl', index: 'PhotoUrl', width: '80px', align: "center", editable: true, formatter: imageFormat, unformat: imageUnFormat },
                                   { name: 'Title', index: 'Title', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                   { name: 'Description', index: 'Description', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true }, hidden: true },
                                   { name: 'Qty', index: 'Qty', width: '70px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                     { name: 'PackScheme', index: 'PackScheme', width: '120px', editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                       { name: 'StrStartDate', index: 'StrStartDate', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                   { name: 'StrEndDate', index: 'StrEndDate', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                      { name: 'IsActive', index: 'IsActive', width: '100px', editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                          ],
                          rowNum: 10,
                          mtype: 'GET',
                          toolbar: [true, "top"],
                          loadonce: true,
                          rowList: [10, 20, 30],
                          pager: '#jQGridDemoPager',
                          sortname: 'SchemeId',
                          viewrecords: true,
                          height: "100%",
                          width: "500px",
                          sortorder: 'asc',
                          caption: "Scheme List",
                          ignoreCase: true,
                          editurl: 'handlers/manageschemes.ashx',
                          rowattr: function (rd) {
                              if (rd.IsActive == false) {
                                  return { "class": "myAltRowClass" };
                              }
                          }
                      });

                      function imageFormat(cellvalue, options, rowObject) {
                          return '<img alt="N/A" src="../SchemeImages/T_' + cellvalue + '" style="width:20px;height:20px"/>';
                      }
                      function imageUnFormat(cellvalue, options, cell) {
                          return $('img', cell).attr('src');
                      }

                      $("#jQGridDemo").jqGrid('setGridParam',
                 {
                  onSelectRow: function (rowid, iRow, iCol, e) {
                      SchemeId = 0;
                      validateForm("detach");
                      var txtTitle = $("#txtTitle");
                      SchemeId = $('#jQGridDemo').jqGrid('getCell', rowid, 'SchemeId');
                      SchemePhotoUrl = $('#jQGridDemo').jqGrid('getCell', rowid, 'PhotoUrl');
                      SchemePhotoUrl = SchemePhotoUrl.toString().substring(18, SchemePhotoUrl.toString().length);
                      ProductCollection = [];
                      FreeProductCollection = [];
                      if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                          $('#chkIsActive').prop('checked', true);
                      }
                      else {
                          $('#chkIsActive').prop('checked', false);

                      }
                      if ($('#jQGridDemo').jqGrid('getCell', rowid, 'PackScheme') == "true") {
                          $('#chkPackScheme').prop('checked', true);
                      }
                      else {
                          $('#chkPackScheme').prop('checked', false);

                      }
                      $('#tbFreeProductList tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                      $('#tbMainProductList tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                      txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Title'));
                      $("#txtDescription").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Description'));
                      $("#txtStartDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'StrStartDate'));
                      $("#txtEndDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'StrEndDate'));
                      $("#txtQty").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Qty'));

                      $("#tbFreeProductList").css({ "display": "block" });
                      $("#tbMainProductList").css({ "display": "block" });
                     $.ajax({
                        type: "POST",
                        data: '{ "SchemeId": "' + SchemeId + '"}',
                        url: "manageschemes.aspx/GetProductDetail",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            ProductCollection = [];
                            FreeProductCollection = [];

                            var obj = jQuery.parseJSON(msg.d);

                            var tr = "";
                            for (var i = 0; i < obj.MasterData.length; i++) {
                                var pname1 = "";
                                pname1 = obj.MasterData[i]["Name"];
                                var price1 = 0;
                                price1 = obj.MasterData[i]["Price"];
                                var vid1 = 0;
                                vid1 = obj.MasterData[i]["VariationId"];
                                var PhotoUrl1 = "";
                                PhotoUrl1 = obj.MasterData[i]["PhotoUrl"];
                                var tr = "<tr><td><img style='height:30px;width:30px' src='../ProductImages/" + PhotoUrl1 + "' /></td><td>" + pname1 + "</td><td>" + price1 + "</td><td><div id='btnDelete' style='cursor:pointer'><a >Delete</a></div></td></tr>";

                                $("#tbMainProductList").append(tr);
                                var CO = new clsProduct();
                                CO.ProductName = pname1;
                                CO.Price = price1;
                                CO.VariationId = vid1;
                                ProductCollection.push(CO);
                            }
                            var tr = "";
                            for (var i = 0; i < obj.FreeData.length; i++) {
                                var pname2 = "";
                                pname2 = obj.FreeData[i]["Name"];
                                 var price2 = 0;
                                price2 = obj.FreeData[i]["Price"];
                                var vid2 = "";
                                vid2 = obj.FreeData[i]["VariationId"];
                                var PhotoUrl2 = "";
                                PhotoUrl2 = obj.FreeData[i]["PhotoUrl"];
                                var tr = "<tr><td><img style='height:30px;width:30px' src='../ProductImages/" + PhotoUrl2 + "' /></td><td>" + pname2 + "</td><td>" + price2 + "</td><td><div id='btnDeleteFree' style='cursor:pointer'><a >Delete</a></div></td></tr>";
                                $("#tbFreeProductList").append(tr);
                                var CO = new clsProduct();
                                CO.ProductName = pname2;
                                CO.Price = price2;
                                CO.VariationId = vid2;
                               FreeProductCollection.push(CO);
                            }

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {

                            $.uiUnlock();
                        }



                    });
                    txtTitle.focus();
                    $("#btnAdd").css({ "display": "none" });
                    $("#btnUpdate").css({ "display": "block" });
                    $("#btnReset").css({ "display": "block" });
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

        

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                       {
                           refresh: false,
                           edit: false,
                           add: false,
                           del: false,
                           search: true,
                           searchtext: "Search",
                           addtext: "Add",
                       },

                       {//SEARCH
                           closeOnEscape: true

                       }

                         );



        }





            </script>
</asp:Content>

