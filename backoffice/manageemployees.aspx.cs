﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_manageemployees : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!User.IsInRole("ManageEmployees"))
            {
                Response.Redirect("default.aspx");

            }
        }
    }


    [WebMethod]
    public static string InsertUpdate(int Id, string Prefix, string EmpName, string Address,string ContactNo, string Email, bool IsActive,  string UserName, string Password)
    {

        Employees objUsers = new Employees()
        {
            Emp_Id = Id,
            Prefix = Prefix,
            Emp_Name = EmpName.ToUpper(),
            Address = Address,
            Email = Email,
            Password = Password.Trim(),
            ContactNo = ContactNo,
            UserName = UserName.Trim(),
            IsActive = Convert.ToBoolean(IsActive)
        };


        int st = new EmployeesBLL().InsertUpdate(objUsers);
        var JsonData = new
        {

            Status = st,
            Users = objUsers
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        return ser.Serialize(JsonData);

    }
}