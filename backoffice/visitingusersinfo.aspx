﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="visitingusersinfo.aspx.cs" Inherits="backoffice_vistingusersinfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
       <form id="Action" runat ="server"> 
<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Users</span>
                     
                        <br />
                    </h3>
				   <div class="youhave" style="padding-left:10px">
                       <table>
                           <tr>
                               <td>
                                     <div style="margin: 4px 4px 4px 4px;border:thin;background-color:lightskyblue;width:200px;padding-left:10px">
                                   <table>
                                        <tr>
                               <td>   <h2  style="color:maroon">Todays's Users</h2></td>
                                            </tr>
                                       <tr>
                               <td> <h3 ><asp:Literal ID="ltTodayUsers" runat="server"></asp:Literal></h3></td>
                           </tr>
                                   </table>
                                         </div>
                               </td>
                              </tr>
                          <tr><td>
                               <div style="margin: 4px 4px 4px 4px;border:thin;background-color:lightskyblue;width:200px;padding-left:10px">
                             <table>
                                 <tr>             
                           <td><h2>MonthWise Records</h2></td>
                                   </tr>
                                 <tr>
                                     <asp:Repeater ID="repMonthWiseUsers" runat="server">
                                         <HeaderTemplate>
                                             <table>
                                                 <thead>
                                                     <tr>
                                                         <th style="width:80px">Month</th>
                                                           <th style="width:80px">Total Users</th>
                                                     </tr>
                                                 </thead>
                                           

                                         </HeaderTemplate>
                                         <ItemTemplate>
                                            <tr><td><%#Eval("Month") %></td><td><%#Eval("MonthlyUsers") %></td></tr>
                                         </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                     </asp:Repeater>
                                 </tr>
                                 </table>
                                   </div>
                              </td>
                          </tr>
                             <tr><td>
                                 <div style="margin: 4px 4px 4px 4px;border:thin;background-color:lightgreen;width:200px;padding-left:10px">
                             <table >
                                 <tr>             
                           <td><h3>YearWise Records</h3></td>
                                   </tr>
                                 <tr>
                                     <asp:Repeater ID="repYearWiseUsers" runat="server">
                                         <HeaderTemplate>
                                             <table>
                                                 <thead>
                                                     <tr>
                                                         <th style="width:80px">Year</th>
                                                           <th style="width:80px">Total Users</th>
                                                     </tr>
                                                 </thead>
                                           

                                         </HeaderTemplate>
                                         <ItemTemplate>
                                            <tr><td><%#Eval("Year") %></td><td><%#Eval("YearlyUsers") %></td></tr>
                                         </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                     </asp:Repeater>
                                 </tr>
                                 </table>
                                     </div>
                              </td>
                          </tr>
                       </table>
                       </div>
                       </div>
    </div>
           </form>
</asp:Content>

