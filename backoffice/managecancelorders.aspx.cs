﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_managecancelorders : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (!User.IsInRole("CanceledOrders"))
            {
                Response.Redirect("default.aspx");

            }
            hdnDate.Value = DateTime.Now.ToShortDateString();
            
        }
    }


    [WebMethod]

    public static string InsertTemp(Int32 OI)
    {
        OrderDetail objOrder = new OrderDetail()
        {
            OrderId = Convert.ToInt32(OI),
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        new CancelOrderBLL().TempInsert(objOrder);
        var JsonData = new
        {
            Order = objOrder

        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string GetTemparoryOrders(int Oid)
    {
        OrderDetail objOrder = new OrderDetail() { OrderId = Oid };


        var OrderDetail = new OrderBLL().GetOrdersTemp(objOrder);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            TempOrders = OrderDetail
        };
        return ser.Serialize(JsonData);
    }

}