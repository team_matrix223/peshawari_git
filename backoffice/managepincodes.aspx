﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="managepincodes.aspx.cs" Inherits="backoffice_managepincodes" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
        <link href="css/custom-css/master.css" rel="stylesheet" />

    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">
    var m_PinCodeId = 0;


    function ResetControls() {
        m_PinCodeId = 0;
        var txtTitle = $("#txtPinCode");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");

        txtTitle.focus();
        txtTitle.val("");
        txtTitle.focus();
        btnAdd.css({ "display": "block" });
        btnAdd.html("Add PinCode");
        $("#txtSector").val("");
        $("#<%=ddlCity.ClientID %>").val("0");
        btnUpdate.css({ "display": "none" });
        btnUpdate.html("Update PinCode");

        $("#chkIsActive").prop("checked", "checked");

        $("#btnReset").css({ "display": "none" });

        $("#hdnId").val("0");
        validateForm("detach");
    }



    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {

        if (!validateForm("formID")) {
            return;
        }

        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");

        var Id = m_PinCodeId;

        //alert(Id);
        // return;
        var PinCode = $("#txtPinCode").val();
        if ($.trim(PinCode) == "") {
            $("#txtPinCode").focus();

            return;
        }
        var Sector = $("#txtSector").val();
        var City = $("#<%=ddlCity.ClientID %>").val();

        if (City == 0 || City == "") {

            $("#<%=ddlCity.ClientID %>").focus();

            return;
        }
        $.uiLock('');
        if (Id == "0") {
            btnAdd.unbind('click');
        }
        else {
            btnUpdate.unbind('click');
        }

        btnAdd.html("<img src='images/loader.gif' alt='loading...'/>")
        btnUpdate.html("<img src='images/loader.gif' alt='loading...'/>")

        var IsActive = false;


        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
        }
     
        $.ajax({
            type: "POST",
            data: '{"PinCodeId":"' + Id + '", "PinCode": "' + PinCode + '","Sector":"' + Sector + '","City":"' + City + '","IsActive": "' + IsActive + '"}',
            url: "managepincodes.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == -1) {
                    ResetControls();


                    if (Id == "0") {
                        btnAdd.bind('click', function () { InsertUpdate(); });
                    }
                    else {

                        btnUpdate.bind('click', function () { InsertUpdate(); });
                    }

                    alert("Insertion Failed.PinCode with duplicate name already exists.");
                    return;

                }

                if (Id == "0") {
                    btnAdd.bind('click', function () { InsertUpdate(); });
                   

                    alert("PinCode is added successfully.");
                    ResetControls();
                }
                else {
                    btnUpdate.bind('click', function () { InsertUpdate(); });
                   
                   alert("PinCode is Updated successfully.");
                   ResetControls();
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
                BindGrid();
            }



        });

    }

    $(document).ready(
    function () {




 
        $("#btnAdd").click(
        function () {

            m_PinCodeId = 0;
            InsertUpdate();


        }
        );


        $("#btnUpdate").click(
        function () {
            InsertUpdate();


        }
        );







        $('#txtPinCode').focus();
        $('#txtPinCode').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );

        BindGrid();
    }
    );

</script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update PinCodes</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">
                   <table cellpadding="0" cellspacing="0" border="0" class="top_table">
                 

                  
                     <tr><td class="headings">City:</td><td><asp:DropDownList ID ="ddlCity" class="inputtxt validate required" runat="server"></asp:DropDownList></td></tr>
                   
                     <tr><td class="headings">PinCode:</td><td>  <input type="text" name="txtPinCode" class="inputtxt validate required"  data-index="1" id="txtPinCode" /></td></tr>
                                       
                    <tr><td class="headings">Sector:</td><td> 
                    
                    <input id="txtSector"  type="text" class="inputtxt validate required"/>
                      
                     
                     </td></tr>
                     
                    
                     <tr><td class="headings">Is Active:</td><td>     <input type="checkbox" id="chkIsActive" checked=checked data-index="2"  name="chkIsActive" />
                                            </td></tr>


                     </table>

                                            <table cellspacing="0" cellpadding="0" class="category_table">
                                            <tr>
                                            <td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add PinCode</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" >Update PinCode</div></td>
                                            <td><div id="btnReset"  class="btn btn-primary btn-small" style="display:none;width:60px;" >Cancel</div></td>
                                            </tr>
                                            </table>
                    			 
       
                    </div>
			  </div>
               

        <div id="rightnow">
               <div id="Div1">
                     <h3 class="reallynow">
                        <span >Manage  PinCodes</span>
                      
                        <br />
                    </h3>
				    <div class="youhave">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>
			  </div>
        </div>

            </div>
</form>

            <script type="text/javascript">
                function BindGrid() {


                  
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/managepincodes.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['PinCodeId', 'PinCode', 'Sector','City','CityId', 'IsActive'],
                        colModel: [
                                    { name: 'PinCodeId', key: true, index: 'PinCodeId', width: 100, stype: 'text', sorttype: 'int', hidden: true },

                                    { name: 'PinCode', index: 'PinCode', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true } },

                                    { name: 'Sector', index: 'Sector', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true }, hidden: false },

                                     { name: 'CityName', index: 'CityName', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true }, hidden: false },
                                      { name: 'CityId', index: 'CityId', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true }, hidden: true },

                                    { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },


                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'PinCodeId',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "PinCodes List",

                        editurl: 'handlers/managepincodes.ashx',



                    });




                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_PinCodeId = 0;
                    validateForm("detach");
                    var txtTitle = $("#txtPinCode");
                    m_PinCodeId = $('#jQGridDemo').jqGrid('getCell', rowid, 'PinCodeId');


                    if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                        $('#chkIsActive').prop('checked', true);
                    }
                    else {
                        $('#chkIsActive').prop('checked', false);

                    }
                    txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'PinCode'));

                    $("#txtSector").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Sector'));
                     $("#<%=ddlCity.ClientID %>").val($('#jQGridDemo').jqGrid('getCell', rowid, 'CityId'));
               


        txtTitle.focus();
        $("#btnAdd").css({ "display": "none" });
        $("#btnUpdate").css({ "display": "block" });
        $("#btnReset").css({ "display": "block" });
        TakeMeTop();
    }
});

    var DataGrid = jQuery('#jQGridDemo');
    DataGrid.jqGrid('setGridWidth', '500');

    $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                     {
                         refresh: false,
                         edit: false,
                         add: false,
                         del: false,
                         search: false,
                         searchtext: "Search",
                         addtext: "Add",
                     },

                     {//SEARCH
                         closeOnEscape: true

                     }

                       );



}





    </script>

</asp:Content>

