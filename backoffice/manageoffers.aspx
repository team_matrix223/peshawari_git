﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="manageoffers.aspx.cs" Inherits="backoffice_manageoffers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/master.css" rel="stylesheet" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <script language="javascript" type="text/javascript">
        var m_OfferId = 0;

        var GiftPhotoUrl1 = "";
        var GiftPhotoUrl2 = "";
        var GiftPhotoUrl3 = "";
        var GiftPhotoUrl4 = "";
        var GiftPhotoUrl5 = "";
        function isSpaceKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode == 32 ) {
                alert("Please don't enter space");
                return false;
            }

            return true;
        }
        function ResetControls() {
            m_OfferId = 0;
            $("#ddlOfferType").val("");
            $("#txtCouponNo").val("");
            $("#txtTitle").val("");
            $("#txtTitle").focus();
            var btnAdd = $("#btnAdd");
            var btnUpdate = $("#btnUpdate");
            btnAdd.css({ "display": "block" });
            btnAdd.html("Add");
            $("#txtDesc").val("");
            $("#txtOrderAmount").val("");
            $("#ddlDiscountType").val("");
            $("#txtPoints").val("");
            $("#ddlDiscountMode").val("");
            $("#txtDiscount").val("");
            $("#txtOrdersPerCustomer").val("");
            $("#txtMaxLimit").val("");
            $("#txtFromDate,#txtToDate").val($("#<%=hdnDate.ClientID%>").val());
            $("#ddlPaymentOptions").val("");
            btnUpdate.css({ "display": "none" });
            btnUpdate.html("Update");
            $("#chkIsActive").prop("checked", "checked");
            $("#btnReset").css({ "display": "none" });
            $("#dvPoint").css({ "display": "none" });
            $("#dvDiscount").css({ "display": "none" });
            $("#dvGifts").css({ "display": "none" });
            $("#txtGiftName1").val("");
            $("#txtGiftDesc1").val("");

            $("#txtGiftName2").val("");
            $("#txtGiftDesc2").val("");

            $("#txtGiftName3").val("");
            $("#txtGiftDesc3").val("");

            $("#txtGiftName4").val("");
            $("#txtGiftDesc4").val("");

            $("#txtGiftName5").val("");
            $("#txtGiftDesc5").val("");

            $("#<%=fuGiftPhotoUrl1.ClientID %>").val("");
            $("#<%=fuGiftPhotoUrl2.ClientID %>").val("");
            $("#<%=fuGiftPhotoUrl3.ClientID %>").val("");
            $("#<%=fuGiftPhotoUrl4.ClientID %>").val("");
            $("#<%=fuGiftPhotoUrl5.ClientID %>").val("");
            validateForm("detach");
        }
        function TakeMeTop() {
            $("html, body").animate({ scrollTop: 0 }, 500);
        }

        function RefreshGrid() {
            $('#jQGridDemo').trigger('reloadGrid');

        }

        function InsertUpdate() {
            if (!validateForm("frmOffers")) {
                return;
            }
            var Id = m_OfferId ;
            var Title = $("#txtTitle").val();
           
            var IsActive = false;

            if ($('#chkIsActive').is(":checked")) {
                IsActive = true;
            }
            var OfferType = $("#ddlOfferType option:selected").text();
            if ($.trim(OfferType) == "") {
                alert("Choose Offer Type");
                $("#ddlOfferType").focus();
                return;
            }
            var Description = $("#txtDesc").val();
            var CouponNo = $("#txtCouponNo").val();


            var OrderAmount = $("#txtOrderAmount").val();
            var DiscountType = $("#ddlDiscountType option:selected").text();
            var Points = $("#txtPoints").val();
            var DiscountMode = $("#ddlDiscountMode option:selected").text();
            var Discount = $("#txtDiscount").val();

            var GiftName1 = $("#txtGiftName1").val();
            var GiftDesc1 = $("#txtGiftDesc1").val();

            var GiftName2 = $("#txtGiftName2").val();
            var GiftDesc2 = $("#txtGiftDesc2").val();

            var GiftName3 = $("#txtGiftName3").val();
            var GiftDesc3 = $("#txtGiftDesc3").val();

            var GiftName4 = $("#txtGiftName4").val();
            var GiftDesc4 = $("#txtGiftDesc4").val();

            var GiftName5 = $("#txtGiftName5").val();
            var GiftDesc5 = $("#txtGiftDesc5").val();

            var PhotoUrl1 = "";
            var fileUpload1 = $("#<%=fuGiftPhotoUrl1.ClientID %>").get(0);
            var files1 = fileUpload1.files;

            var data = new FormData();
            for (var i = 0; i < files1.length; i++) {
                if (files1[i].name.length > 0) {
                    PhotoUrl1 = files1[i].name;
                }

                data.append(files1[i].name, files1[i]);
            }

            var PhotoUrl2 = "";
            var fileUpload2 = $("#<%=fuGiftPhotoUrl2.ClientID %>").get(0);
            var files2 = fileUpload2.files;


            for (var i = 0; i < files2.length; i++) {
                if (files2[i].name.length > 0) {
                    PhotoUrl2 = files2[i].name;
                }

                data.append(files2[i].name, files2[i]);
            }

            var PhotoUrl3 = "";
            var fileUpload3 = $("#<%=fuGiftPhotoUrl3.ClientID %>").get(0);
            var files3 = fileUpload3.files;

            
            for (var i = 0; i < files3.length; i++) {
                if (files3[i].name.length > 0) {
                    PhotoUrl3 = files3[i].name;
                }

                data.append(files3[i].name, files3[i]);
            }
            var PhotoUrl4 = "";
            var fileUpload4 = $("#<%=fuGiftPhotoUrl4.ClientID %>").get(0);
            var files4 = fileUpload4.files;

          
            for (var i = 0; i < files4.length; i++) {
                if (files4[i].name.length > 0) {
                    PhotoUrl4 = files4[i].name;
                }

                data.append(files4[i].name, files4[i]);
            }
            var PhotoUrl5 = "";
            var fileUpload5 = $("#<%=fuGiftPhotoUrl5.ClientID %>").get(0);
            var files5 = fileUpload5.files;

           
            for (var i = 0; i < files5.length; i++) {
                if (files5[i].name.length > 0) {
                    PhotoUrl5 = files5[i].name;
                }

                data.append(files5[i].name, files5[i]);
            }
            if ($.trim(DiscountType) == "") {
                alert("Choose Discount Type");
                $("#ddlDiscountType").focus();
                return;
            }
            else if ($.trim(DiscountType) == "Point") {
                if ($.trim(Points) == "") {
                    alert("Enter Points");
                    $("#txtPoints").focus();
                    return;
                }
            }
            else if ($.trim(DiscountType) == "Discount") {
                if ($.trim(DiscountMode) == "") {
                    alert("Choose DiscountMode");
                    $("#ddlDiscountMode").focus();
                    return;
                }
                if ($.trim(Discount) == "" || $.trim(Discount) == "0.00") {
                    alert("Enter Discount");
                    $("#txtDiscount").focus();
                    return;
                }
            }
            else if ($.trim(DiscountType) == "Gifts") {
               
                    if ($.trim(GiftName1) == "") {
                        alert("Enter GiftName1");
                        $("#txtGiftName1").focus()
                        return;
                    }
                    if (m_OfferId == 0 && PhotoUrl1 == "") {
                        alert("Please choose Image for Gift1");
                        return;
                    }
                    if ($.trim(GiftName2) != "") {
                        if (m_OfferId == 0 && PhotoUrl2 == "") {
                            alert("Please choose Image for Gift2");
                            return;
                        }
                    }
                    if ($.trim(GiftName3) != "") {
                        if (m_OfferId == 0 && PhotoUrl3 == "") {
                            alert("Please choose Image for Gift3");
                            return;
                        }
                    }
                    if ($.trim(GiftName4) != "") {
                        if (m_OfferId == 0 && PhotoUrl4 == "") {
                            alert("Please choose Image for Gift4");
                            return;
                        }
                    }
                    if ($.trim(GiftName5) != "") {
                        if (m_OfferId == 0 && PhotoUrl5 == "") {
                            alert("Please choose Image for Gift5");
                            return;
                        }
                    }
              
           
            }
                var PaymentOption = $("#ddlPaymentOptions option:selected").text();
                if ($.trim(DiscountType) != "Gifts") {
                    if ($.trim(PaymentOption) == "") {
                        alert("Choose PaymentOption");
                        $("#ddlPaymentOptions").focus();
                        return;
                    } 
                }
             var offertype = $("#ddlOfferType option:selected").text();
             if (offertype == "SpecialDay") {
                 $("#txtToDate").val($("#txtFromDate").val());
             }
            var DateFrom = $("#txtFromDate").val();
            var DateTo = $("#txtToDate").val();
            if ($.trim(Points) == "") {
                Points = "0";
            }
            if ($.trim(Discount) == "") {
                Discount = "0";
            }
            var PhotoUrl01 = "";
            var PhotoUrl02 = "";
            var PhotoUrl03 = "";
            var PhotoUrl04 = "";
            var PhotoUrl05 = "";
           
            if ($.trim(DiscountType) == "Gifts") {
                var options = {};
                options.url = "handlers/OffersFileUploader.ashx";
                options.type = "POST";
                options.async = false;
                options.data = data;
                options.contentType = false;
                options.processData = false;
                options.success = function (msg) {


                    var obj = jQuery.parseJSON(msg);
                    PhotoUrl01 = obj.File1;
                    PhotoUrl02 = obj.File2;
                    PhotoUrl03 = obj.File3;
                    PhotoUrl04 = obj.File4;
                    PhotoUrl05 = obj.File5;
                };

                options.error = function (err) { };
                $.ajax(options);
            }
            if ($.trim(PhotoUrl01) != "") {
                GiftPhotoUrl1 = PhotoUrl01;
            }
            if ($.trim(PhotoUrl02) != "") {
                GiftPhotoUrl2 = PhotoUrl02;
            }
            if ($.trim(PhotoUrl03) != "") {
                GiftPhotoUrl3 = PhotoUrl03;
            }
            if ($.trim(PhotoUrl04) != "") {
                GiftPhotoUrl4 = PhotoUrl04;
            }
            if ($.trim(PhotoUrl05) != "") {
                GiftPhotoUrl5 = PhotoUrl05;
            }
            var OrdersPerCustomer = $("#txtOrdersPerCustomer").val();
            var MaxLimit = $("#txtMaxLimit").val();
            var ChkNews = 0;
            var Newsletter = "";
            var SendTo = 0;
            if($("#chknewsletter").prop('checked'))
            {
            ChkNews = "1";
            Newsletter = $("#txtnewsletter").val();
            SendTo = $("#ddlsendto").val();
            }
            else
            {
            ChkNews = "0";
            }

            
         
           
     
            $.ajax({
                type: "POST",
                data: '{"OfferId":"' + Id + '", "OfferType": "' + OfferType + '","Title":"' + Title + '","Description":"' + Description + '","CouponNo":"' + CouponNo + '","OrderAmount":"' + OrderAmount + '","DiscountType":"' + DiscountType + '","Points":"' + Points + '","DiscountMode":"' + DiscountMode + '","Discount":"' + Discount + '","DateFrom":"' + DateFrom + '","DateTo":"' + DateTo + '","PaymentOption":"' + PaymentOption + '","IsActive": "' + IsActive + '","OrdersPerCustomer":"' + OrdersPerCustomer + '","MaxLimit":"' + MaxLimit + '","GiftName1":"' + GiftName1 + '","GiftDesc1":"' + GiftDesc1 + '","GiftPhotoUrl1":"' + GiftPhotoUrl1 + '","GiftName2":"' + GiftName2 + '","GiftDesc2":"' + GiftDesc2 + '","GiftPhotoUrl2":"' + GiftPhotoUrl2 + '","GiftName3":"' + GiftName3 + '","GiftDesc3":"' + GiftDesc3 + '","GiftPhotoUrl3":"' + GiftPhotoUrl3 + '","GiftName4":"' + GiftName4 + '","GiftDesc4":"' + GiftDesc4 + '","GiftPhotoUrl4":"' + GiftPhotoUrl4 + '","GiftName5":"' + GiftName5 + '","GiftDesc5":"' + GiftDesc5 + '","GiftPhotoUrl5":"' + GiftPhotoUrl5 + '","Chknews":"'+ChkNews+'","Newsletter":"'+Newsletter+'","SendTo":"'+SendTo+'"}',
                url: "manageoffers.aspx/Insert",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == 0) {

                        alert("Insertion Failed.Offer with duplicate name already exists.");
                        return;
                    }
                    if (obj.Status == -1) {

                        alert("Insertion Failed.Offer with duplicate CouponNo already exists.");
                        return;
                    }
                    if (Id == "0") {
                        ResetControls();
                        BindGrid();
                        alert("Offer is added successfully.");
                    }
                    else {
                        ResetControls();
                        BindGrid();
                        alert("Offer is Updated successfully.");
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });

        }


        $(document).ready(
        function () {

            $("#chknewsletter").change(
            function () {
                if ($("#chknewsletter").prop('checked')) {
                    $("#tbl").css('display', 'block');
                }
                else {
                    $("#tbl").css('display', 'none');
                }
            }
            );

            $("#ddlOfferType").change(function () {
                var offertype = $("#ddlOfferType option:selected").text();
                if (offertype == "SpecialDay") {
                    $("#dvFromDate").css({ "display": "block" });
                    $("#dvToDate").css({ "display": "none" });
                    $("#sp_FromDate").html("Choose Date:");
                    $("#txtToDate").val("1900-01-01");
                    $("#txtFromDate").val($("#<%=hdnDate.ClientID%>").val());
                }
                else if (offertype == "NormalDay" || offertype == "FirstOrder") {
                    $("#dvFromDate").css({ "display": "block" });
                    $("#dvToDate").css({ "display": "block" });
                    $("#sp_FromDate").html("From:");
                    $("#txtFromDate,#txtToDate").val($("#<%=hdnDate.ClientID%>").val());
                }

            });

            $("#ddlDiscountType").change(function () {
                var distype = $("#ddlDiscountType option:selected").text();
                if (distype == "Point") {
                    $("#dvPoint").css({ "display": "block" });
                    $("#dvDiscount").css({ "display": "none" });
                    $("#txtDiscount").val("");
                    $("#ddlDiscountMode").val("");
                    $("#dvGifts").css({ "display": "none" });
                    $("#ddlPaymentOptions").attr('disabled', true);
                    $("#ddlPaymentOptions").val("Wallet");

                }

                else if (distype == "Discount") {
                    $("#dvPoint").css({ "display": "none" });
                    $("#dvDiscount").css({ "display": "block" });
                    $("#txtPoints").val("");
                    $("#dvGifts").css({ "display": "none" });
                    $("#ddlPaymentOptions").prop('disabled', true);
                    $("#ddlPaymentOptions").val("Instant");
                }
                else if (distype == "Gifts") {
                    $("#dvGifts").css({ "display": "block" });
                    $("#dvPoint").css({ "display": "none" });
                    $("#dvDiscount").css({ "display": "none" });
                    $("#ddlPaymentOptions").prop('disabled', true);
                    $("#ddlPaymentOptions").val("Instant");
                }
                else {
                    $("#dvPoint").css({ "display": "none" });
                    $("#dvDiscount").css({ "display": "none" });
                    $("#dvGifts").css({ "display": "none" });
                    $("#ddlPaymentOptions").removeAttr('disabled');
                }


            });

            $("#txtFromDate").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });


            $("#txtToDate").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });

            $("#txtFromDate,#txtToDate").val($("#<%=hdnDate.ClientID%>").val());
            $("#txtDiscount").val("");
            BindGrid();

            $("#btnReset").click(
        function () {
            ResetControls();
        });
            $("#btnAdd").click(
        function () {

            m_OfferId = 0;
            InsertUpdate();
        }
        );


            $("#btnUpdate").click(
        function () {
            InsertUpdate();
        }
        );
        });
    </script>
        <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnDate" runat="server"/>
<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Offers</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">

                   <table id="frmOffers">
                   <tr>
                   <td>
                   <table cellpadding="0" cellspacing="0" border="0" class="top_table">
                   <tr>
                   <td>OfferType:</td>
                   <td><select id="ddlOfferType">
                   <option value=""></option>
                   <option value="SpecialDay">SpecialDay</option>
                      <option value="NormalDay">NormalDay</option>
                    
                       <option value="FirstOrder">FirstOrder</option>
                                 
                       <option value="Birthday">Birthday</option>          
                       <option value="Anniversary">Anniversary</option>
                       </select>
                   </td></tr>
                 
                   <tr>
                   <td>CouponNo:</td><td><input type="text" id="txtCouponNo" class="inputtxt validate required" onkeypress="return isSpaceKey(event)" />
                   </td></tr>
               
                      <tr>
                   <td>Title:</td>
                   <td><input  type="text" id="txtTitle" class="inputtxt validate required" />
                   </td></tr> 
                      <tr>
                   <td>Description:</td>
                   <td><textarea id="txtDesc" class="inputtxt validate required"></textarea>
                   </td></tr>
                      <tr>
                   <td>Order Amount:<b style="padding-left:10px">>=</b></td>
                   <td><input  type="text" id="txtOrderAmount" class="inputtxt validate required valNumber" />
                   </td></tr>
                    <tr>
                   <td>DiscountType:</td>
                   <td><select id="ddlDiscountType">
                    <option value=""></option>
                   <option value="Point">Point</option>
                     <option value="Discount">Discount</option> 
                      <option value="Gifts">Gifts</option>
                   </select>
                   </td></tr>
                   <tr ><td colspan="100%"><div id="dvPoint" style="display:none;border:2px solid" >
                  <table width="100%"><tr>
                   <td>Points:</td><td><input type="text" id="txtPoints"/></td>
                   </tr>
                   </table>
                    </div></td>
                   </tr>
                  
                     <tr >
                     <td colspan="100%">
                     <div id="dvDiscount" style="display:none;border:2px solid">
                     <table width="100%">
                     <tr>
                     <td>
                     DiscountMode:
                     </td>
                         <td><select id="ddlDiscountMode">
                      <option value=""></option>
                   <option value="Cash">Cash</option>
                     <option value="Percent">Percent</option>
                   </select></td>

            
                     </tr>
                     <tr>
                       <td>Discount:
                   </td><td><input type="text" id="txtDiscount"/></td>
                     </tr>
                     </table>
                     </div>
                     
                     </td>
               </tr>
                    <tr><td colspan="100%" >
                <div id="dvGifts" style="display:none;border:2px solid"><table width="100%">
                <tr><td colspan="100%" align="center"><b>Gift1 </b></td></tr>
                <tr>
                <td>Name</td>
                <td><input type="text" id="txtGiftName1" style="width:75%" /></td>
                </tr>
                   <tr>
                <td>Description</td>
                <td><textarea  id="txtGiftDesc1" style="width:75%" ></textarea> </td>
                </tr>
                 <tr>
                <td>PhotoUrl</td>
                <td><input type="file" id="fuGiftPhotoUrl1" style="width:75%"   runat="server"/></td>
                </tr>
               
                <tr><td colspan="100%" align="center"><b>Gift2 </b></td></tr>
                <tr>
                <td style="width:96px">Name</td>
                <td  style="padding-left: 5px"><input type="text" id="txtGiftName2" style="width:75%" /></td>
                </tr>
                   <tr>
                <td style="width:96px">Description</td>
                <td  style="padding-left: 5px"><textarea  id="txtGiftDesc2" style="width:75%" ></textarea></td>
                </tr>
                 <tr>
                <td style="width:96px">PhotoUrl</td>
                <td  style="padding-left: 5px"><input type="file" id="fuGiftPhotoUrl2" style="width:75%"  runat="server" /></td>
                </tr>
                <tr><td colspan="100%" align="center"><span><b>Gift3 </b></span></td></tr>
                <tr>
                <td style="width:96px">Name</td>
                <td  style="padding-left: 5px"><input type="text" id="txtGiftName3" style="width:75%" /></td>
                </tr>
                   <tr>
                <td style="width:96px">Description</td>
                <td  style="padding-left: 5px"><textarea id="txtGiftDesc3" style="width:75%" ></textarea></td>
                </tr>
                 <tr>
                <td style="width:96px">PhotoUrl</td>
                <td  style="padding-left: 5px"><input type="file" id="fuGiftPhotoUrl3" style="width:75%"  runat="server"/></td>
                </tr>
                <tr><td colspan="100%" align="center"><span><b>Gift4 </b></span></td></tr>
                <tr>
                <td style="width:96px">Name</td>
                <td  style="padding-left: 5px"><input type="text" id="txtGiftName4" style="width:75%" /></td>
                </tr>
                   <tr>
                <td style="width:96px">Description</td>
                <td  style="padding-left: 5px"><textarea id="txtGiftDesc4" style="width:75%" ></textarea></td>
                </tr>
                 <tr>
                <td style="width:96px">PhotoUrl</td>
                <td  style="padding-left: 5px"><input type="file" id="fuGiftPhotoUrl4" style="width:75%"  runat="server" /></td>
                </tr>
                <tr><td colspan="100%" align="center"><span><b>Gift5 </b></span></td></tr>
                <tr>
                <td style="width:96px">Name</td>
                <td  style="padding-left: 5px"><input type="text" id="txtGiftName5" style="width:75%" /></td>
                </tr>
                   <tr>
                <td style="width:96px">Description</td>
                <td  style="padding-left: 5px"><textarea id="txtGiftDesc5" style="width:75%" ></textarea></td>
                </tr>
                 <tr>
                <td style="width:96px">PhotoUrl</td>
                <td  style="padding-left: 5px"><input type="file" id="fuGiftPhotoUrl5" style="width:75%"  runat="server" /></td>
                </tr>
              
                </table>
                </div></td></tr>      
                <tr><td colspan="100%" >
                <div id="dvFromDate" style="display:none"><table width="100%">
                <tr>
                <td style="width:96px"><span id="sp_FromDate"> From:</span></td>
                <td  style="padding-left: 15px"><input type="text" id="txtFromDate" style="width:56%" class="inputtxt validate required"/></td>
                </tr>
                </table>
                </div></td></tr>
                  <tr><td  colspan="100%">
                  <div id="dvToDate" style="display:none">
                  <table width="100%">
                  <tr>
                  <td style="width:96px" >To:
                  </td>                <td style="padding-left: 15px"><input type="text" id="txtToDate"  class="inputtxt validate required" style="width:56%"/></td>
                  </tr>
                  </table>
                  </div></td></tr>
                  <tr>
                   <td style="padding-left:10px">PaymentOptions:</td>
                   <td><select id="ddlPaymentOptions">
                   <option value=""></option>
                     <option value="Instant">Instant</option>
                       <option value="Wallet">Wallet</option></select>
                   </td></tr>
                          <tr  >
                                 <td  class="Titles">
                                     OrdersPerCustomer:</td>
                                 <td>
                                     <input type="text" id="txtOrdersPerCustomer" class="inputtxt validate required valNumber"/>
                                 </td>
                             </tr> 
                                    <tr  >
                                 <td  class="Titles">
                                     MaxLimit:</td>
                                 <td>
                                       <input type="text" id="txtMaxLimit"  class="inputtxt validate required valNumber"/>
                                 </td>
                             </tr> 
                      <tr>
                                 <td  class="Titles">
                                     IsActive:</td>
                                 <td>
                                       <input type="checkbox" id="chkIsActive"  checked="checked" data-index="2"  name="chkIsActive" />
                                 </td>
                             </tr> 


                                            <tr><td  class="Titles">Send NewsLetter</td>
                                                <td><input type="checkbox" id="chknewsletter"   data-index="2"  name="chkIsActive" /></td></tr>

                             <tr><td></td><td colspan="100%"><table id="tbl"  style="border:solid 1px silver;display:none">
                       
                             <tr><td>NewsLetter:</td><td><textarea  id ="txtnewsletter" style="width:250px;height:150px"></textarea></td></tr>
                             <tr><td>Send To:</td><td><select style="width:250px" id="ddlsendto" style="width: 100px">
                    <option value=""></option>
                   <option value="1">All</option>
                     <option value="2">Birthday</option> 
                      <option value="3">Anniversary</option>
                       <option value="4">Birthday/Anniversary</option>
                   </select></td></tr>
                                       </table>
                   </td>
                   </tr>
                
                   </table>

                                            <table cellspacing="0" cellpadding="0" class="category_table">
                                            <tr>
                                            <td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" >Update</div></td>
                                            <td><div id="btnReset"  class="btn btn-primary btn-small" style="display:none;width:60px;" >Cancel</div></td>
                                            </tr>
                                            </table>
                   </td>
                   </tr>
                   </table>
                   </div>
                   </div>
                       <div id="rightnow" style="margin-top:10px">
                    <h3 class="reallynow">
                        <span>Manage Offers </span>
                      
                        <br />
                    </h3>
				    <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>
			  </div>
                   </div>
                   </form>
                        <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageOffers.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",
                        colNames: ['OfferId','OfferType', 'Title','Description','CouponNo','OrderAmount','DiscountType','Points','DiscountMode','Discount','FromDate','ToDate','PaymentOption','OrdersPerCustomer','MaxLimit', 'IsActive'],
                        colModel: [
                                    { name: 'OfferId', key: true, index: 'OfferId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                   { name: 'OfferType', index: 'OfferType', width: '150px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Title', index: 'Title', width: '150px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                      { name: 'Description', index: 'Description', width: '200px', stype: 'text', sortable: true, editable: true, editrules: { required: true },hidden:true },
                                        { name: 'CouponNo', index: 'CouponNo', width: '100px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                 { name: 'OrderAmount', index: 'OrderAmount', width: '100px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                  { name: 'DiscountType', index: 'DiscountType', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                            { name: 'Points', index: 'Points', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } ,hidden:true },
                            { name: 'DiscountMode', index: 'DiscountMode', width: '100px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                           
                            { name: 'Discount', index: 'Discount', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true },hidden:true },
                            { name: 'strFromDate', index: 'strFromDate', width: '100px', stype: 'text', sortable: true, editable: true, editrules: { required: true },hidden:true },
                        { name: 'strToDate', index: 'strToDate', width: '100px', stype: 'text', sortable: true, editable: true, editrules: { required: true } ,hidden:true},
                           { name: 'PaymentOption', index: 'PaymentOption', width: '100px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                           { name: 'OrdersPerCustomer', index: 'OrdersPerCustomer', width: '100px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                          { name: 'MaxLimit', index: 'MaxLimit', width: '100px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                            { name: 'IsActive', index: 'IsActive', width: '70px', editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },


                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'OfferId',
                        viewrecords: true,
                        height: "100%",
                        width: "800px",
                        sortorder: 'asc',
                        caption: "Offer List",

                        editurl: 'handlers/ManageOffers.ashx',



                    });




                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_OfferId = 0;
                    validateForm("detach");
                    var txtTitle = $("#txtTitle");
                    m_OfferId = $('#jQGridDemo').jqGrid('getCell', rowid, 'OfferId');


                    if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                        $('#chkIsActive').prop('checked', true);
                    }
                    else {
                        $('#chkIsActive').prop('checked', false);

                    }
                    txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Title'));
                       $("#txtDesc").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Description'));
                      $("#txtCouponNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'CouponNo'));
                     
                     $("#ddlOfferType").val($('#jQGridDemo').jqGrid('getCell', rowid, 'OfferType'));
                     var offertype = $('#jQGridDemo').jqGrid('getCell', rowid, 'OfferType');
                if (offertype == "SpecialDay") {
                    $("#dvFromDate").css({ "display": "block" });
                    $("#dvToDate").css({ "display": "none" });
                    $("#sp_FromDate").html("Choose Date:");
                    $("#txtToDate").val("1900-01-01");
                    $("#txtFromDate").val($("#<%=hdnDate.ClientID%>").val());
                }
                else if (offertype == "NormalDay") {
                    $("#dvFromDate").css({ "display": "block" });
                    $("#dvToDate").css({ "display": "block" });
                    $("#sp_FromDate").html("From:");
                    $("#txtFromDate,#txtToDate").val($("#<%=hdnDate.ClientID%>").val());
                }
                else if (offertype == "FirstOrder") {
                    $("#dvFromDate").css({ "display": "none" });
                    $("#dvToDate").css({ "display": "none" });
                    $("#txtFromDate,#txtToDate").val("1900-01-01");
                }
                     $("#txtOrderAmount").val($('#jQGridDemo').jqGrid('getCell', rowid, 'OrderAmount'));
                     $("#ddlDiscountType").val($('#jQGridDemo').jqGrid('getCell', rowid, 'DiscountType'));
                     $("#txtFromDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'strFromDate'));
                     $("#txtToDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'strToDate'));
                     $("#ddlPaymentOptions").val($('#jQGridDemo').jqGrid('getCell', rowid, 'PaymentOption'));
                        $("#txtPoints").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Points'));
                       $("#ddlDiscountMode").val($('#jQGridDemo').jqGrid('getCell', rowid, 'DiscountMode'));

                  $("#txtDiscount").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Discount'));
                  $("#txtOrdersPerCustomer").val($('#jQGridDemo').jqGrid('getCell', rowid, 'OrdersPerCustomer'));
                    $("#txtMaxLimit").val($('#jQGridDemo').jqGrid('getCell', rowid, 'MaxLimit'));
                var distype = $("#ddlDiscountType option:selected").text();
                if (distype == "Point") {
                    $("#dvPoint").css({ "display": "block" });
                    $("#dvDiscount").css({ "display": "none" });
                    $("#txtDiscount").val("");
                    $("#ddlDiscountMode").val("");
                    $("#dvGifts").css({ "display": "none" });
                }
                else if (distype == "Discount") {
                    $("#dvPoint").css({ "display": "none" });
                    $("#dvDiscount").css({ "display": "block" });
                    $("#txtPoints").val("");
                    $("#dvGifts").css({ "display": "none" });
                }
                  else if (distype == "Gifts") {
                   $("#dvPoint").css({ "display": "none" });
                     $("#dvDiscount").css({ "display": "none" });
                      $("#dvGifts").css({ "display": "block" });


             GiftPhotoUrl1 = "";
             GiftPhotoUrl2 = "";
             GiftPhotoUrl3 = "";
             GiftPhotoUrl4 = "";
             GiftPhotoUrl5 = "";

                   $.ajax({
            type: "POST",
            data: '{"OfferId":"'+ m_OfferId+'"}',
            url: "manageoffers.aspx/GetGiftsDetail",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);
                for (var i = 0; i < obj.DetailData.length; i++) {
                                if(i==0){
                                
                                $("#txtGiftName1") .val( obj.DetailData[i]["GiftName"]);
                                $("#txtGiftDesc1") .val( obj.DetailData[i]["GiftDesc"]);
                              GiftPhotoUrl1=obj. DetailData[i]["GiftPhotoUrl"];
                              
                                }
                               if(i==1){
                                
                                $("#txtGiftName2") .val( obj.DetailData[i]["GiftName"]);
                                $("#txtGiftDesc2") .val( obj.DetailData[i]["GiftDesc"]);
                              GiftPhotoUrl2=obj. DetailData[i]["GiftPhotoUrl"];
                              
                                }
                                if(i==2){
                                
                                $("#txtGiftName3") .val( obj.DetailData[i]["GiftName"]);
                                $("#txtGiftDesc3") .val( obj.DetailData[i]["GiftDesc"]);
                              GiftPhotoUrl3=obj. DetailData[i]["GiftPhotoUrl"];
                              
                                }
                                if(i==3){
                                
                                $("#txtGiftName4") .val( obj.DetailData[i]["GiftName"]);
                                $("#txtGiftDesc4") .val( obj.DetailData[i]["GiftDesc"]);
                              GiftPhotoUrl4=obj. DetailData[i]["GiftPhotoUrl"];
                              
                                }
                                if(i==4){
                                
                                $("#txtGiftName5") .val( obj.DetailData[i]["GiftName"]);
                                $("#txtGiftDesc5") .val( obj.DetailData[i]["GiftDesc"]);
                              GiftPhotoUrl5=obj. DetailData[i]["GiftPhotoUrl"];
                              
                                }
                                }
   
             
                

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {
               
            }

        });
                   
                  }
                    txtTitle.focus();
                    $("#btnAdd").css({ "display": "none" });
                    $("#btnUpdate").css({ "display": "block" });
                    $("#btnReset").css({ "display": "block" });
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>
</asp:Content>

