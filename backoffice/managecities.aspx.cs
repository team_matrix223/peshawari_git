﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_managecities : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (!User.IsInRole("ManageCities"))
            {
                Response.Redirect("default.aspx");

            }
        }
   
    }


   
     
    [WebMethod]
    public static string Insert(int CityId, string Title, bool IsActive)
    {
        Cities objCities = new Cities()
        {
            CityId = CityId,
            Title = Title.Trim().ToUpper(),
            IsActive = IsActive,
            AdminId=0,

        };
        int status = new CitiesBLL().InsertUpdate(objCities);
        var JsonData = new
        {
            City = objCities,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

}