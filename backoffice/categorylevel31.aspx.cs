﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_categorylevel3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCategories();
        }
    }
    [WebMethod]
    public static string BindSubCategories(int CatId)
    {

        string SubCat = new CategoryBLL().GetOptions(CatId);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            SubCatOptions = SubCat

        };
        return ser.Serialize(JsonData);
    }
    void BindCategories()
    {

        ddlCategory.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategory.DataValueField = "CategoryId";
        ddlCategory.DataTextField = "Title";
        ddlCategory.DataBind();
        ListItem li = new ListItem();
        li.Text = "--Select Catgory--";
        li.Value = "0";
        ddlCategory.Items.Insert(0, li);
    }



    [WebMethod]
    public static string Insert(int id, string title, bool isActive, int CatId, string desc, string CatTitle)
    {




        Category objCategory = new Category()
        {
            Title = title.Trim(),
            IsActive = isActive,
            CategoryId = id,
            Description = desc,
            AdminId = 0,
            ParentId = CatId,
            Level =3,
           

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new CategoryBLL().InsertUpdate(objCategory);
        var JsonData = new
        {
            Role = objCategory,
            Status = status
        };
        return ser.Serialize(JsonData);



    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSubCategories.DataSource = new CategoryBLL().GetByParentId(Convert.ToInt32(ddlCategory.SelectedValue));
        ddlSubCategories.DataValueField = "CategoryId";
        ddlSubCategories.DataTextField = "Title";
        ddlSubCategories.DataBind();
        ListItem li = new ListItem();
        li.Text = "--Select Catgory--";
        li.Value = "0";
        ddlSubCategories.Items.Insert(0, li);

    }
}