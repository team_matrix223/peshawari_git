﻿<%@ Page Title="" Language="C#" MasterPageFile="admin.master" AutoEventWireup="true" CodeFile="managebrand.aspx.cs" Inherits="backoffice_managebrand" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
        <link href="css/custom-css/master.css" rel="stylesheet" />

    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">
    var m_CityId = 0;

    function ResetControls() {
        m_CityId = 0;
        var txtTitle = $("#txtTitle");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
        txtTitle.focus();
        txtTitle.val("");
        txtTitle.focus();
        btnAdd.css({ "display": "block" });
        btnAdd.html("Add Brand");
        $("#txtDescription").val("");
        btnUpdate.css({ "display": "none" });
        btnUpdate.html("Update Brand");
        $("#chkIsActive").prop("checked", "checked");
        $("#btnReset").css({ "display": "none" });
        $("#hdnId").val("0");
        validateForm("detach");
    }



    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {

        if (!validateForm("frmCity")) {
            return;
        }
        var Id = m_CityId;
        var Title = $("#txtTitle").val();
        if ($.trim(Title) == "") {
            $("#txtTitle").focus();

            return;
        }
        var IsActive = false;

        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
        }

        $.ajax({
            type: "POST",
            data: '{"BrandId":"' + Id + '", "Title": "' + Title + '","IsActive": "' + IsActive + '"}',
            url: "managebrand.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == 0) {

                    alert("Insertion Failed.Brand with duplicate name already exists.");
                    return;
                }

                if (Id == "0") {
                    ResetControls();
                    BindGrid();
                    alert("Brand is added successfully.");
                }
                else {
                    ResetControls();
                    BindGrid();
                    alert("Brand is Updated successfully.");
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }

    $(document).ready(
    function () {
        BindGrid();

        $("#btnAdd").click(
        function () {

            m_CityId = 0;
            InsertUpdate();
        }
        );


        $("#btnUpdate").click(
        function () {
            InsertUpdate();
        }
        );

        $('#txtTitle').focus();
        $('#txtTitle').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );


    }
    );

</script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Brand</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">
                   <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="top_table">
                     
                   
                     <tr><td class="headings">Title:</td><td>  <input type="text"  name="txtTitle" class="inputtxt"   data-index="1" id="txtTitle"/></td></tr>
                                                    
                       
                      
                     <tr><td class="headings">IsActive:</td><td>     <input type="checkbox" id="chkIsActive" checked="checked" data-index="2"  name="chkIsActive" />
                                            </td></tr>

                     </table>
                    		
                                            <table cellspacing="0" cellpadding="0" class="category_table">
                                            <tr>
                                            <td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add Brand</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" >Update Brand</div></td>
                                            <td><div id="btnReset"  class="btn btn-primary btn-small" style="display:none;">Cancel</div></td>
                                            </tr>
                                            </table>	 
       
                    </div>
			  </div>
               


               <div id="rightnow">
                    <h3 class="reallynow">
                        <span>Manage Brand </span>
                      
                        <br />
                    </h3>
				    <div class="youhave">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>
			  </div>

            </div>
</form>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/managebrand.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Brand Id', 'Title', 'IsActive'],
                        colModel: [
                                    { name: 'BrandId', key: true, index: 'BrandId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'Title', index: 'Title', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },

                                    { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },


                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'BrandId',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Brand List",

                        editurl: 'handlers/managebrand.ashx',



                    });




                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_CityId = 0;
                    validateForm("detach");
                    var txtTitle = $("#txtTitle");
                    m_CityId = $('#jQGridDemo').jqGrid('getCell', rowid, 'BrandId');


                    if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                        $('#chkIsActive').prop('checked', true);
                    }
                    else {
                        $('#chkIsActive').prop('checked', false);

                    }
                    txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Title'));

                    txtTitle.focus();
                    $("#btnAdd").css({ "display": "none" });
                    $("#btnUpdate").css({ "display": "block" });
                    $("#btnReset").css({ "display": "block" });
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>

</asp:Content>

