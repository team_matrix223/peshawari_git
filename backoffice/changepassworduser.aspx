﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="changepassworduser.aspx.cs" Inherits="backoffice_changepassworduser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
 


<form runat="server" id="frmHeading">
  

			<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Change Password</span>
                      
                        <br />
                    </h3>
			
             
								
							
							<div class="widget-content nopadding">
								<div class="form-horizontal">
                                <asp:ValidationSummary ID="smry" runat="server" ShowMessageBox="true" ShowSummary="false" />
						<div class="form-group">
										<label class="control-label">Old Password</label>
										<div class="controls">
							<asp:TextBox ID="txtOldPassword" runat="server"  class="form-control input-small" 
                                                TextMode="Password" ></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator1"   runat="server" ControlToValidate="txtOldPassword"  ErrorMessage="Please enter old password" SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
              
                                         </div>
									</div>
                        
                        
                        <div class="form-group">
										<label class="control-label">New Password</label>
										<div class="controls">
									
                                          		 			<asp:TextBox ID="txtNewPassword" runat="server"  
                                                                class="form-control input-small" TextMode="Password" ></asp:TextBox>
              <asp:RequiredFieldValidator ID="reqPassword"   runat="server" ControlToValidate="txtNewPassword" ErrorMessage="Please enter new password" SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
										</div>
									</div>
                        
                                      
                        <div class="form-group">
										<label class="control-label">Confirm Password</label>
										<div class="controls">
											 	 			<asp:TextBox ID="txtConfirmPassword" runat="server"  
                                                                class="form-control input-small" TextMode="Password" ></asp:TextBox>
              
                                                   <asp:CompareValidator SetFocusOnError="true" ID="RequiredFieldValidator2" runat="server" ControlToCompare="txtNewPassword" ErrorMessage="Password and confirm password do not match." Display="None" Operator="Equal" Type="String" ControlToValidate="txtConfirmPassword"></asp:CompareValidator>
									      
										</div>
									</div>


                                    <div class="form-actions">
									
                  <asp:Button ID="btnAdd" Text="Change Password" class="btn btn-primary btn-small" runat="server" 
                                            onclick="btnAdd_Click" />                   
  
									</div>
								</div>
							</div>
						
					</div>
                </div>
		 
 
		 
   
</form>




</asp:Content>

