﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_migratesubcategory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDept();
        }

    }
    public void BindDept()
    {
        ddlDepartments.DataSource = new migratesubcategoryDAL().GetAllDepartment();
        ddlDepartments.DataValueField = "CategoryId";
        ddlDepartments.DataTextField = "Title";
        ddlDepartments.DataBind();
        ddlDepartments.Items.Insert(0,"--Select--");
    }
    public void BindGroups()
    {
        if (ddlDepartments.Text.ToString() != "" && ddlDepartments.Text.Trim().ToString() != "--Select--")
        {
            ddlGroups.DataSource = new migratesubcategoryDAL().GetGroupsByDeptId(Convert.ToInt32( ddlDepartments.SelectedValue.ToString()));
            ddlGroups.DataValueField = "CategoryId";
            ddlGroups.DataTextField = "Title";
            ddlGroups.DataBind();
            ddlGroups.Items.Insert(0, "--Select--");
        }
    }
    public void BindSGroups()
    {
        if (ddlGroups.Text.ToString() != "" && ddlGroups.Text.Trim().ToString() != "--Select--")
        {
            //ddlList.DataSource = new migratesubcategoryDAL().GetSGroupsByGroupId(Convert.ToString( ddlGroups.SelectedValue.ToString()));
            //ddlList.DataValueField = "SGroup_Id";
          //  ddlList.DataTextField = "SGroup_Name";
           // ddlList.DataBind();

            ddlList.Items.Clear();
            DataSet ds = new migratesubcategoryDAL().GetSGroupsByGroupId(Convert.ToString(ddlGroups.SelectedValue.ToString()));

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ListItem li = new ListItem();
                li.Text = ds.Tables[0].Rows[i]["SGroup_Name"].ToString();
                li.Value = ds.Tables[0].Rows[i]["SGroup_Id"].ToString();
                if (ds.Tables[0].Rows[i]["IsSelected"].ToString() == "1")
                {
                    li.Selected = true;
                }
                ddlList.Items.Add(li);
            }
        }
    }
    protected void ddlDepartments_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGroups();
       
    }
    protected void ddlGroups_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindSGroups();

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Code");
        dt.Columns.Add("Name");


        int[] selectedIndices = ddlList.GetSelectedIndices();

        for (int i = 0; i < selectedIndices.Length; i++)
        {

            DataRow dr = dt.NewRow();
            ListItem li = ddlList.Items[selectedIndices[i]];
            dr["Code"] = li.Value;
            dr["Name"] = li.Text;
            dt.Rows.Add(dr);

        }

        int status = new migratesubcategoryDAL().InsertUpdateSGroups(Convert.ToInt32( ddlGroups.SelectedValue), dt);
        if (status == 1)
        {
            Response.Write("<script>alert('Records Updated Successfully')</script>");
              ddlList.Items.Clear();
            //ddlDepartments.Text = "";
              ddlGroups.SelectedIndex = -1;

        }
        else
        {
            Response.Write("<script>alert('An Error occured during transaction. Please try again later')</script>");

        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        ddlList.Items.Clear();
        //ddlDepartments.Text = "";
        ddlGroups.DataSource = null;
        ddlList.DataSource = null;

    }
    protected void btnAddSubGroup_Click(object sender, EventArgs e)
    {
        Common objCommon = new Common()
        {

            Title = txtDepartmentTitle.Value.Trim().ToUpper(),
            Status = "3",
        };
        int status = new CommonBLL().InsertUpdate(objCommon);
        if (status != 0)
        {
            lblMessage.Text = "Sub Group Added Successfully";
            BindSGroups();
            ddlList.Items.Clear();
        }
        else
        {
            lblMessage.Text = "Sub Group With duplicate name already Exists";

        }

    }
}