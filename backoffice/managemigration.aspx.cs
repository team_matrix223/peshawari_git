﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Data;

public partial class backoffice_managemigration : System.Web.UI.Page
{

    protected string m_sMessage = "";
    protected string m_sSubMessage = "";
    public DataTable dtFinal = new DataTable();
    protected bool m_bShow = false;
    protected string ErrorMessage = "";
    protected string htmldata = "";
    public int m_CurrentPage = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (!User.IsInRole("MigrateProducts"))
                {
                    Response.Redirect("default.aspx");

                }
                 
               // catLevel3.Visible = false;
            
                BindCategories();
                

            }
        }
        catch (Exception ex)
        {
        }
    }


    public void BindCategories()
    {
        try
        {
            ddlCategory.DataSource = new CategoryBLL().GetByLevel(1);
            ddlCategory.DataValueField = "CategoryId";
            ddlCategory.DataTextField = "Title";
            ddlCategory.DataBind();
            ListItem li = new ListItem();
            li.Text = "--Category Level 1--";
            li.Value = "0";
            ddlCategory.Items.Insert(0, li);

            ddlnewCategory.DataSource = new CategoryBLL().GetByLevel(1);
            ddlnewCategory.DataValueField = "CategoryId";
            ddlnewCategory.DataTextField = "Title";
            ddlnewCategory.DataBind();
            ListItem li1 = new ListItem();
            li1.Text = "--Category Level 1--";
            li1.Value = "0";
            ddlnewCategory.Items.Insert(0, li1);

          


        }
        finally { }

    }

    [WebMethod]
    public static string BindSubCategories(int CatId)
    {

        string SubCat = new CategoryBLL().GetOptions(CatId);


        string ProductHTML = new ProductsBLL().GetProductHTMLByCategoryLevel2(CatId);


        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            SubCatOptions =  SubCat,
            ProductHtml = ProductHTML

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindCategoryLevel3(int CatId)
    {

        string SubCat = new CategoryBLL().GetOptions(CatId);


        string ProductHTML = new ProductsBLL().GetProductHTMLByCategoryLevel3(CatId);


        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            SubCatOptions =  SubCat,
            ProductHtml = ProductHTML

        };
        return ser.Serialize(JsonData);
    }



    public void BindSubCategories(int ParentId, DropDownList ddlParent, DropDownList ddl)
    {
        try
        {
            if (ddlParent.SelectedIndex != 0)
            {
                
                m_bShow = true;
                ddl.DataSource = new CategoryBLL().GetByParentId(ParentId);
                ddl.DataTextField = "Title";
                ddl.DataValueField = "CategoryId";
                ddl.DataBind();
                ListItem li = new ListItem();
                li.Text = "--Category Level 2--";
                li.Value = "0";
                ddl.Items.Insert(0, li);
            }
            else
            {
                m_sMessage = "Please Select  Category Level 1";
            }
        }
        finally
        {
        }

    }

    public void BindCategoryLevel3(int ParentId, DropDownList ddl)
    {
        try
        {
            if (ParentId != 0)
            {
              
                m_bShow = true;
                ddl.DataSource = new CategoryBLL().GetByParentId(ParentId);
                ddl.DataTextField = "Title";
                ddl.DataValueField = "CategoryId";
                ddl.DataBind();
                ListItem li = new ListItem();
                li.Text = "--Category Level 3--";
                li.Value = "0";
                ddl.Items.Insert(0, li);
            }
            else
            {
                m_sMessage = "Please Select  Category";
                return;
            }
        }
        finally
        {
        }

    }

   




    public void FillDataListByPageId(int SubCategoryId)
    {



        //repProducts.DataSource = new ProductsBLL().GetBySubCategoryId(SubCategoryId);
        //repProducts.DataBind();

        updProducts.Update();


    }

    public void FillDataListByCategoryLevel3(int SubCategoryId)
    {



        //repProducts.DataSource = new ProductsBLL().GetByCategoryLevel3(SubCategoryId);
        ///repProducts.DataBind();

        updProducts.Update();


    }
    public void FillDataListByCategoryLevel1(int Categoryid)
    {

       // repProducts.DataSource = new ProductsBLL().GetByCategoryLevel1(Categoryid);
       // repProducts.DataBind();
       updProducts.Update();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
         
    }
    


    protected void btnAddToTemp_Click(object sender, EventArgs e)
    {
       
    }

    
    protected void btnUpdate_Click(object sender, EventArgs e)
    {

    

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
      
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        
       
        ddlSubCategories.Items.Clear();
        ListItem li = new ListItem();
        li.Text = "--Category Level 2--";
        li.Value = "0";
        ddlSubCategories.Items.Insert(0, li);

        ddlCategoryLevel3.Items.Clear();
        ListItem li2 = new ListItem();
        li2.Text = "--Category Level 3--";
        li2.Value = "0";
        ddlCategoryLevel3.Items.Insert(0, li2);
        if (ddlCategory.SelectedIndex > 0)
        {
            BindSubCategories(Convert.ToInt32(ddlCategory.SelectedValue), ddlCategory, ddlSubCategories);
            FillDataListByCategoryLevel1(Convert.ToInt32(ddlCategory.SelectedValue));
        }
        else
        {
            catLevel3.Visible = false;
           // repProducts.DataSource = null;
           // repProducts.DataBind();

           
        }
    }
    protected void ddlSubCategories_SelectedIndexChanged(object sender, EventArgs e)
    {
        
       
        updProducts.Update();
        if (ddlSubCategories.SelectedIndex > 0)
        {
            BindCategoryLevel3(Convert.ToInt32(ddlSubCategories.SelectedValue), ddlCategoryLevel3);
           // repProducts.DataSource = null;
           // repProducts.DataBind();
            if (ddlCategoryLevel3.Items.Count == 1)
            {
                catLevel3.Visible = false;
            }
            else
            {
                catLevel3.Visible = true;
            }
            FillDataListByPageId(Convert.ToInt32(ddlSubCategories.SelectedValue));
        }
        else
        {
            ddlCategoryLevel3.Items.Clear();
            ListItem li = new ListItem();
            li.Text = "--Category Level 3--";
            li.Value = "0";
            ddlCategoryLevel3.Items.Insert(0, li);
           // repProducts.DataSource = null;
          //  repProducts.DataBind();
            FillDataListByCategoryLevel1(Convert.ToInt32(ddlCategory.SelectedValue));
        }
    }
    protected void ddlCategoryLevel3_SelectedIndexChanged(object sender, EventArgs e)
    {
         

        if (ddlCategoryLevel3.SelectedIndex > 0)
        {
            FillDataListByCategoryLevel3(Convert.ToInt32(ddlCategoryLevel3.SelectedValue));
        }
        else
        {
            FillDataListByPageId(Convert.ToInt32(ddlSubCategories.SelectedValue));

        }
    }
    protected void repProducts_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
       if (e.CommandName == "edit")
      {
    
     }
   
    }
    protected void repProducts_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
    
    }
     

    protected void btnCancelPrice_Click(object sender, EventArgs e)
    {
       
       
    }
    protected void ddlnewCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        

        ddlnewSubCategories.Items.Clear();
        ListItem li = new ListItem();
        li.Text = "--Category Level 2--";
        li.Value = "0";
        ddlnewSubCategories.Items.Insert(0, li);

        ddlnewCategoryLevel3.Items.Clear();
        ListItem li2 = new ListItem();
        li2.Text = "--Category Level 3--";
        li2.Value = "0";
        ddlnewCategoryLevel3.Items.Insert(0, li2);
        if (ddlnewCategory.SelectedIndex > 0)
        {
            BindSubCategories(Convert.ToInt32(ddlnewCategory.SelectedValue), ddlnewCategory, ddlnewSubCategories);
     
        }
        else
        {
            catLevel3.Visible = false;
          //  repProducts.DataSource = null;
           // repProducts.DataBind();

        }



    }
    protected void ddlnewSubCategories_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        updProducts.Update();
        if (ddlnewSubCategories.SelectedIndex > 0)
        {
            BindCategoryLevel3(Convert.ToInt32(ddlnewSubCategories.SelectedValue), ddlnewCategoryLevel3);
           
            if (ddlnewCategoryLevel3.Items.Count == 1)
            {
                Tr1.Visible = false;
            }
            else
            {
                Tr1.Visible = true;
            }
           // FillDataListByPageId(Convert.ToInt32(ddlnewSubCategories.SelectedValue));
        }
        else
        {
            ddlnewCategoryLevel3.Items.Clear();
            ListItem li = new ListItem();
            li.Text = "--Category Level 3--";
            li.Value = "0";
            ddlnewCategoryLevel3.Items.Insert(0, li);
           
            //FillDataListByCategoryLevel1(Convert.ToInt32(ddlCategory.SelectedValue));
        }


    }
    protected void ddlnewCategoryLevel3_SelectedIndexChanged(object sender, EventArgs e)
    {
     
    }




    [WebMethod]
    public static string Insert(int oldCategory, int oldSubCategory, int oldCategory3, int Category, int SubCategory, int Category3, string status1, string arrProduct)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("ProductId");
        if (arrProduct != "")
        {
            string[] ProductData = arrProduct.Split(',');



            

            for (int i = 0; i < ProductData.Length; i++)
            {
                DataRow dr = dt.NewRow();
                dr["ProductId"] = Convert.ToInt16(ProductData[i]);
                dt.Rows.Add(dr);
            }
        }

        int Status = new MigrationDAL().UpdateMigrateProducts(oldCategory, oldSubCategory, oldCategory3, Category, SubCategory, Category3, status1, dt);

        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Status = Status,
       

        };
        return ser.Serialize(JsonData);
    }





 
}