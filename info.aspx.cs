﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class info : System.Web.UI.Page
{
    public int PageId { get { return Request.QueryString["pid"] != null ? Convert.ToInt16(Request.QueryString["pid"]) : 0; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindPageInformation();
        }
    }
    void BindPageInformation()
    {

        Cms objCms = new Cms();
        objCms.PageId = PageId;
        objCms.GetById();
        ltPageTitle.Text = objCms.Title;
        ltPageDescription.Text = objCms.Description;

        int ParentId = 0;
        if (objCms.ParentPage != 0)
        {
            ParentId = objCms.ParentPage;
            ltParentPageTitle.Text = objCms.ParentPageName;
        }
        else
        {
            ParentId = objCms.PageId;
            ltParentPageTitle.Text = objCms.Title;

        }

        repMenu.DataSource = new Cms().GetByParentIdFrontEnd(PageId);
        repMenu.DataBind();
    }
}