﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="basket.aspx.cs" Inherits="basket" %>

<%@ Register Src="~/usercontrols/ucMenu.ascx" TagPrefix="uc1" TagName="ucMenu" %>
 <%@ MasterType VirtualPath="main.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cnMaster" Runat="Server">
      
     <%--<script src="jquery-1.4.4.min.js" type="text/javascript"></script>--%>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="smartpaginator.js" type="text/javascript"></script>
    <link href="smartpaginator.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.uilocksearch.js" type="text/javascript"></script>



     <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <%--<script src="//code.jquery.com/jquery-1.10.2.js"></script>--%>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="css/jquery.notifyBar.css">

   <script src="jquery.notifyBar.js"></script>


    
         <link rel="stylesheet" href="styles.css">
   <%--<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>--%>
   <script src="script.js"></script>
<script src="js/jquery.uilock.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">

    function goBack() {
        window.history.back();
    }
    $(document).on("click", "button[name='btnAddToCart']", function (event) {

        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = $("#q_" + arrData[1]).val();
        var st = "p";
        var type = "Product";

        $("#a_" + vid).css("width", "55px");
        $("#a_" + vid).html("<img src='images/loaderadd.gif' style='margin-top:2px'   alt='.....'/>");



        ATC(vid, qty, st, type);
        $.notifyBar({ cssClass: "success", html: "Product Added Succcessfully!" });

    });
    function ATC(vid, qty, st,type) {

        $.ajax({
            type: "POST",
            data: '{"vid":"' + vid + '","qty":"' + qty + '","st":"' + st + '","type":"' + type + '"}',
            url: "basket.aspx/ATC",
            contentType: "application/json",
            dataType: "json",
            async:false,
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);

                if (obj.error == 1) {

                    alert("An Error occured during transaction. Please refresh the page and try again Later.");
                    return;
                }
                BindCart();
               

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

                $.uiUnlock();

            }

        });

    }


    $(document).on("click", "div[name='cartminus']", function (event) {

        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = 1;
        var st = "m";
        var type = "Product";
        $("#cq_" + vid).css("height", "20px");

//        $("#cq_" + vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");
        var tempQty = $("#cq_" + vid).html();

        var tempQ = Number(tempQty) - 1;
        if (tempQ <= 1) {
            tempQ = 1;
        }
        $("#cq_" + vid).html(tempQ);


        ATC(vid, qty, st, type);

    });

    $(document).on("click", "div[name='cartadd']", function (event) {



        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = 1;
        var st = "p";
        var type = "Product";
        $("#cq_" + vid).css("height", "20px");

        var tempQty = $("#cq_" + vid).html();

        var tempQ = Number(tempQty) + 1;

        $("#cq_" + vid).html(tempQ);

//        $("#cq_" + vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");

        ATC(vid, qty, st, type);


    });




    $(document).on("click", "div[name='dvClose']", function (event) {

        var vid = $(this).attr("id");
        var st = "m";
        var type = "Combo";
        DFC(vid, st, type);



    });
    function DFC(vid, st, type) {
        $.ajax({
            type: "POST",
            data: '{"vid":"' + vid + '","st":"' + st + '","type":"' + type + '"}',
            url: "index.aspx/RemoveFromCart",
            async: false,
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);

                if (obj.error == 1) {

                    alert("An Error occured during transaction. Please refresh the page and try again Later.");
                    return;
                }


                if (type == "Combo") {

                    $("#cartcontainer").html(obj.cartHTML);

                    $("#cbox_" + obj.pid).html(obj.cartHTML);


                    if (obj.qty <= 0) {

                        $("#cbox_" + obj.pid).css("background", "white")

                    }
                    else {

                        $("#cbox_" + obj.pid).css("background", "#D8EDC0")

                    }



                }
                else {



                    $("#variation_" + vid).html(obj.productHTML);



                }

                BindCart();
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });

    }

    function BindCart() {

       
        $.ajax({
            type: "POST",
            data: '{}',
            url: "basket.aspx/GetCartHTML",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);

                $("#cartcontainer").html(obj.html);

                $("#sp2").html(" Delivery Charges: ");
                $("#subtotal").html(" Rs. " + obj.ST);
                $("#delivery").html(" Rs. " + obj.DC);
                $("#netamount").html(" Rs. " + obj.NA);

                $("#sp_TotalItems").html(obj.TotalItems);
                $("#sp_TotalItems1").html("MY CART(" + obj.TotalItems + ")");

                $("#sp_msg").html("");
                $("#hdTotalAmt").val(obj.ST);
                $("#txtComments").html(obj.Comments); 


                $("#hdMinimumCheckOutAmt").val(obj.MCOA);
                
                if (Number(obj.ST) >= Number(obj.FDA)) {
                   
                    $("#sp2").html("");
                   
                    $("#delivery").html("");
                    $("#netamount").html(" Rs. " + obj.ST);
                }
                if (Number(obj.TotalItems) < 1) {

                    var url = "index.aspx";
                    $(location).attr('href', url);
                }
               

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
    }


    function InsertComments() {
        var Comments = $("#txtComments").val();
        $.ajax({
            type: "POST",
            data: '{"Comments":"' + Comments + '"}',
            url: "basket.aspx/InsertComments",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                if (msg.d == "1") {
                }
                else {
                    $("#sp_msg").html("* Insufficient Order Amount for Checkout");
                }



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {
                  var url = "delivery.aspx";
                   $(location).attr('href', url);
            }

        });


    }
    $(document).ready(
    function () {

        BindCart();
        $("#btnCheckOut").click(
function () {
    var totalAmt = Number($("#hdTotalAmt").val());
    var minAmt = Number($("#hdMinimumCheckOutAmt").val());
    if (totalAmt < minAmt) {
        $("#sp_msg").html("* Order Amount should be greater than " + minAmt);
        return false;
    }
    else {



        $.ajax({
            type: "POST",
            data: '{}',
            url: "basket.aspx/ValidateCheckOut",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                if (msg.d == "1") {
                    InsertComments();
                   
                }
                else {
                    $("#sp_msg").html("* Insufficient Order Amount for Checkout");
                }



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });






    }
});
    }
    );
</script>
<input type="hidden" id="hdMinimumCheckOutAmt" />
        <input type="hidden" id="hdTotalAmt" />
<div class="row" style="margin-top:15px;margin-bottom:15px">

<div class="col-md-12" >
<div class="row">
<div class="col-xs-12"  >
 
</div>

</div>

<div  class="col-md-12" style=" border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
 
 <div class="row">
 <div class="col-md-12">
<span style="color:#58595b;font-size:16px;font-weight:bold;">Your Basket (<span id="sp_TotalItems"></span> Items)</span>
 <hr  style="margin:2px;"/>

 <table style="width:100%">
 <thead>
 <tr style="background:black;color:White;font-weight:bold;"><td></td><td style="padding:10px;width:400px">ITEM</td><td>UNIT PRICE</td><td>SUB TOTAL</td><td></td></tr>
 </thead>
 <tbody id="cartcontainer">
 <tr>
 <td colspan="100%">
 <img src='loading.gif' style='margin-left:470px'/>
 
 </td></tr>
 </tbody>
 </table>

 
 

 <table width="100%">
 <tr>
 <td>
 <div class="row">
 <div class="col-md-6">
 <textarea style="border-radius:10px;border: 2px solid silver;font-size: 15px;width:100%;margin-top:5px" placeholder="Enter Items which you can not find on our site." rows="5" id="txtComments"></textarea>

 </div>
  <div class="col-md-2"></div>
 <div class="col-md-3">
 <div style="margin:5px;border:solid 1px silver;width:230px;float:Right;padding:5px;box-shadow:1px 1px 2px black">
<table style="float:right;margin-right:0px">
 <tr><td style="font-weight:bold;text-align:right;padding-right:10px">Sub Total: </td><td> <span id="subtotal"></span></td></tr>
 <tr><td style="font-weight:bold;text-align:right;padding-right:10px"><span id="sp2"> Delivery Charges: </span></td><td> <span id="delivery"></span></td></tr>
 <tr ><td style="font-weight:bold;text-align:right;padding-right:10px">Net Amount: </td><td> <span id="netamount"></span></td></tr>
 <tr><td colspan="100%">
    <%-- <a href="delivery.aspx">--%>
         <button id="btnCheckOut" class="btn btn-success" type="button" style="width:100%;background:#D50002;border-color:#9e0a0a">Checkout</button>

     <%--</a>--%>

     </td></tr>
 <tr><td colspan="100%" style="padding-top:5px"> <a href="javascript:goBack();"><button class="btn btn-success" type="button" style="width:100%;background:#5cb85c;border-color:#398439">Continue Shopping</button></a></td></tr>
 <tr>
     <td colspan="100%">
          <span id="sp_msg" style="color:red;font-size:11px;padding-left:5px"></span>

     </td>
 </tr>
 </table>
  </div>

 </div>
 
 </div>


 
 </td>
 

 

 
 </tr> 
 </table>
 
  

 </div>
 
 </div>

 


</div>
</div>

 

 


</div>
    <div id="dvMenuCategories" style="position:absolute;top:153px;display:none">

<uc1:ucMenu ID="ucMenu1" runat="server" />

</div>
</asp:Content>

