﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class combos : System.Web.UI.Page
{protected string strProduct="";
    public string ProductId { get { return Request.QueryString["p"] != null ? Request.QueryString["p"] : ""; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        BindSelectedCombo();

        Int32 totitems = new CartBLL().GetTotalItems(HttpContext.Current.Session.SessionID);
        Master.TotalItems = totitems;
        Master.PageTitle = "COMBO OFFERS";

    }
    [WebMethod]
    public static string GetAllComboProducts()
    {
        string categories = new ProductsBLL().GetAllComboProductsHtml();
        return categories;
    }
    [WebMethod]
    public static string FirstTimeATC(string vid, string qty, string st, string type)
    {


        string m_Status = "Plus";

        if (st == "m")
        {
            m_Status = "Minus";

        }



        Int16 IsError = 0;




        int m_Qty = CommonFunctions.ValidateQuantity(qty);
        if (!CommonFunctions.IsValidProductId(vid))
        {
            IsError = 1;
        }

        Calc objCalc = new Calc();
        UserCart objUserCart = new UserCart();
        string html = "";
        string ProductHTML = "";
        if (IsError == 0)
        {
            objUserCart.SessionId = HttpContext.Current.Session.SessionID;
            objUserCart.ProductId = 0;
            objUserCart.CustomerId = 0;
            objUserCart.VariationId = Convert.ToInt64(vid);
            objUserCart.ProductDesc = "";
            objUserCart.Qty = m_Qty;
            objUserCart.Price = 0;
            objUserCart.type = type;

            html = new CartBLL().UserCartInsertUpdate(objUserCart, m_Status, objCalc, out  ProductHTML);

        }
        Int32 totitems = new CartBLL().GetTotalItems(HttpContext.Current.Session.SessionID);
        var JsonData = new
        {

            qty = objUserCart.Qty,
            error = IsError,
            cartHTML = html,
            Calc = objCalc,
            productHTML = ProductHTML,
            TotalItems = totitems
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

    }



    void BindSelectedCombo()
    {
        Int64 PId = CommonFunctions.IsNumeric(ProductId );
        strProduct = new ProductsBLL().GetComboProductsHtmlByProductId(Convert.ToInt32(PId));

    }
}