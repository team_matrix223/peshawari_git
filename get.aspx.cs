﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class get : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

       

        SqlConnection con = new SqlConnection();
        con.ConnectionString = ParamsClass.sqlDataString;
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "insert into cbse(Code,DOC)values(@Code,@DOC)";
        cmd.Parameters.Add("@Code", Request.Form["data"]);
        cmd.Parameters.Add("@DOC", DateTime.Now);
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();
    }
}