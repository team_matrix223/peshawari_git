﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="quickorder.aspx.cs" Inherits="quickorder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cnMaster" Runat="Server">
  <script src="jquery-1.8.3.min"></script>

    <script language="javascript" type="text/javascript">
      var ListStatus="";
     

     
     var m_ProductName = "";
     var m_Qty = 0;
     var m_Unit = 0;
     

       var QuickProductsList = [];
          function clsQuickProduct() {
           
            this.ProductName = 0;
            this.Qty = 0;
            this.Unit = 0;


        }



        function BindDeliveryAddress() {

            $.ajax({
                type: "POST",
                data: '{}',
                url: "quickorder.aspx/BindDeliveryAddress",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    $("#dvDeliveryAddresses").html(msg.d);




                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {


                }

            });

            $.ajax({
                type: "POST",
                data: '{}',
                url: "quickorder.aspx/BindDays",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);
                      $("#<%=ddlDays.ClientID %>").append(obj.ddd);




                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {


                }

            });





        }




         function AddtoArray(m_ProductName,m_Qty,m_Unit)  { 

        
       
     
         var item = $.grep(QuickProductsList, function (item) {
             return item.ProductName == m_ProductName;
         });

         if (item.length) {

             alert("Product Alredy Added in shop list");

             return;

         }


         TO = new clsQuickProduct();

         TO.ProductName = m_ProductName;
         TO.Qty = m_Qty;
         TO.Unit = m_Unit;
        
         QuickProductsList.push(TO);
         $("#QLresult").css("display", "none");
         $("#txtQuickListSearch").val("");
         $("#txtQQty").val("");
         $("#ddlQuickUnit").val("");
         Bindtr();
        
    }

    function Bindtr()  {
      var count = 1;
      $('#tbQuickList tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
         for (var i = 0; i < QuickProductsList.length; i++) {
        
       
         count = count +i;
        

        var tr = "<tr style='border-bottom:dashed 1px silver' ><td style='width:15px;padding:2px 0px 2px 0px'>"+ count +" </td><td style='width:120px'>" + QuickProductsList[i]["ProductName"] + "</td><td style='width:80px'>" + QuickProductsList[i]["Qty"] + " " + QuickProductsList[i]["Unit"] + " </td><td><i id='dvClose'><img src='backoffice/images/trash.png'></i></td></tr>";

        // var tr = "<tr><td></td><td style='width:80px;text-align:center'>" + QuickProductsList[i]["ProductName"] + "</td><td style='width:180px;text-align:center'>" + ProductCollection[i]["Qty"] + "+" + ProductCollection[i]["Unit"] + " </td><td><i id='dvClose'><img src='backoffice/images/trash.png'/></i></td></tr>";
 
              $("#tbQuickList").append(tr);
              }


            
    }


    function SaveQuickProducts() {
              
              var   delval=$("input[name='delivery']:checked").val();
             var DeliveryDay = $("#<%=ddlDays.ClientID%> option:selected").text();
            
             var DeliverySlot = $("#<%=ddlTimeSlot.ClientID%>").val();
             var ProductName = [];
                var Qty = [];
                var Unit = [];

                 if (QuickProductsList.length == 0) {
                    alert("Please first Select Products");
                 
                    return;
                }
               
           
                for (var i = 0; i < QuickProductsList.length; i++) {
                 


                      
                    ProductName[i] = QuickProductsList[i]["ProductName"];
                    Qty[i] = QuickProductsList[i]["Qty"];
                    Unit[i] = QuickProductsList[i]["Unit"];  
                  

                }
          
                 $.ajax({
                    type: "POST",
                    data: '{ "DeliveryAddress": "' + delval + '", "DeliveryDay": "' + DeliveryDay + '", "DeliverySlot": "' + DeliverySlot + '", "ProductNameArr": "' + ProductName + '","qtyArr": "' + Qty + '","unitArr": "' + Unit + '"}',
                    url: "quickorder.aspx/InsertQuickProducts",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);

                        if(obj.list == "-2")
                        {
                           alert("Delivery Address does not belong to this user");
                           return;
                        }
                        else
                        {
                        alert("Order Placed Successfully");
                        $('#tbQuickList tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                        $("#inner").hide();
                        $("#QLPanel").show();
                        $("#dvQlDeliveryAddress").hide();

                        }
                        
                      

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                      
                        $.uiUnlock();
                    }

                });


    }

    function QLAdd(ProductName,Qty,Unit,Vid)
    {

        if (Vid > 1) {

        var m_Qt=Number($("#Qty" + Vid).val());
            if (m_Qt <= 0) {

                alert("Quantity should be greater than 0");

                return;

            }
            if (!$.isNumeric(m_Qt)) {
                alert("Quantity can only be Numeric");
                return;
            }



            Qty = Qty * m_Qt;
        }
   
          AddtoArray(ProductName,Qty,Unit);
    }


    $(document).ready(
    function () {



        $("#<%=ddlDays.ClientID%>").change(function () {

            //     var slctday =  $("#<%=ddlDays.ClientID%> option:selected").text();
            var slctday = $("#<%=ddlDays.ClientID%>").val();

            $.ajax({
                type: "POST",
                data: '{"SelectedDay": "' + slctday + '"}',
                url: "quickorder.aspx/BindSlots",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);

                    $("#<%=ddlTimeSlot.ClientID %>").html(obj.Slots);




                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {


                }

            });


        });


        $("#btnlogin").click(
    function () {

        var MobileNo = $("#txtName").val();
        if (MobileNo == "") {
            alert("Enter Username");
            $("#txtName").focus();
            return;
        }
        var Password = $("#txtPassword").val();
        if (Password == "") {
            alert("Enter Password");
            $("#txtPassword").focus();
            return;
        }

        $.ajax({
            type: "POST",
            data: '{"MobileNo": "' + MobileNo + '","Password": "' + Password + '"}',

            url: "index.aspx/Login",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == "-1") {
                    alert("Invalid User Name.");
                    $("#txtName").focus();
                    return;
                }
                else if (obj.Status == "-2") {
                    alert("Invalid Password");
                    $("#txtPassword").focus();
                    return;
                }
                else if (obj.Status == "0") {
                    alert("First Register,Then SignIn");
                    return;
                }
                else {


                    BindDeliveryAddress();
                    $("#inner").hide();
                    $("#QLPanel").hide();
                    $("#dvQlDeliveryAddress").show();
                    $(".ui-dialog-titlebar").hide();

                }




            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {


                $.uiUnlock();
            }


        });



    }
    );



        $("#btnOk").click(
    function () {
        SaveQuickProducts();
    }
    );

        $(document).on("click", "#dvClose", function (event) {

            var RowIndex = Number($(this).closest('tr').index());
            var tr = $(this).closest("tr");
            tr.remove();

            QuickProductsList.splice(RowIndex, 1);

            if (QuickProductsList.length == 0) {

                $("#tbQuickList").append(" <tr><td colspan='100%' align='center'></td></tr>");

            }
            Bindtr();

        });




        $("#txtQQty").focus(
    function () {

        $("#QLresult").css("display", "none");
    }
    );


        $("#ddlQuickUnit").change(
            function () {

                if ($(this).val() == "") { return; }

                var m_ProductName = $("#txtQuickListSearch").val();

                if (m_ProductName == "") {
                    $("#ddlQuickUnit option").removeAttr("selected");
                    alert("Please enter Item Name");
         
                    return;

                }



                var m_Qty = $("#txtQQty").val();
                if (m_Qty == "") {
                    $("#ddlQuickUnit option").removeAttr("selected");
                    alert("Please enter Quantity");

                    return;

                }

                if (m_Qty <= 0) {
                    $("#ddlQuickUnit option").removeAttr("selected");
                    alert("Quantity should be greater than 0");

                    return;

                }
                if (!$.isNumeric(m_Qty)) {
                    $("#ddlQuickUnit option").removeAttr("selected");
                    alert("Quantity can only be Numeric");
                    return;
                }


                var m_Unit = $("#ddlQuickUnit").val();
                AddtoArray(m_ProductName, m_Qty, m_Unit);

            }


         );

        $("#btnclose").click(
        function () {

            $("#inner").hide();
            $("#QLPanel").show();
            $("#dvQlDeliveryAddress").hide();

        }

        );

        $("#btnCloseDelivery").click(
        function () {

            $("#inner").hide();
            $("#QLPanel").show();
            $("#dvQlDeliveryAddress").hide();

        }

        );

        $("#btnQLPlaceOrder").click(
         function () {

             if (QuickProductsList.length == "0") {
                 alert("First add products to place order");
                 return;
             }
             else {
                 ListStatus = "Quick";
                 $.ajax({
                     type: "POST",
                     data: '{}',
                     url: "index.aspx/CheckUser",
                     contentType: "application/json",
                     dataType: "json",
                     success: function (msg) {


                         if (msg.d == "0") {
                             $("#inner").show();
                             $("#QLPanel").hide();
                             $("#dvQlDeliveryAddress").hide();


                             $(".ui-dialog-titlebar").hide();
                         }
                         else {
                             BindDeliveryAddress();
                             $(".ui-dialog-titlebar").hide();

                             $("#inner").hide();
                             $("#QLPanel").hide();
                             $("#dvQlDeliveryAddress").show();




                         }

                     }, error: function (xhr, ajaxOptions, thrownError) {

                         var obj = jQuery.parseJSON(xhr.responseText);
                         alert(obj.Message);
                     },
                     complete: function (msg) {


                     }

                 });

             }


         });



    });


        </script>


        


          

<div class="row">
            
            
            <div class="col-md-12">
  					
              <h2 style="border-bottom: 1px dashed silver; border-top: 1px dashed silver; 
                  color: gray; padding: 7px; font-size: 20px; margin-top: 10px;
                   background: none repeat scroll 0% 0% rgb(238, 238, 238); 
                   font-family: serif;">PESHAWARI QUICK LIST - FASTEST WAY TO PLACE AN ORDER </span></h2> 
  					</div>
            
        </div>

      <div style="margin-top:15px;margin-bottom:15px" class="row">

<div class="col-md-12">
<div class="row">
<div class="col-xs-12">
 
</div>

</div>

<div style=" border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px" class="col-md-12">
 
   <div id ="inner" class="formy well" style="z-index: 1;display:none;border:solid 2px silver;background-color:#f5f5f5">
                     <h3  style="font-weight:bold;padding-bottom:20px;padding-left:20px" class="title">Login to Your Account</h3>
                                  <div class="form">
                                

                                      <!-- Login  form (not working)-->
                                                                       
                                          <!-- Username -->
                                         <table><tr><td>
                                           <label for="txtName" class="control-label col-md-3">Username</label>
                                         </td>
                                         <td>
                                                <input id="txtName" type ="text" style="margin-bottom:10px" class = "form-control" onkeypress="return isNumberKey(event)" />
                                         </td></tr><tr>
                                         <td>
                                            <label for="txtPassword" class="control-label col-md-3">Password</label>
                                         </td>
                                         <td>   <input type="password" id = "txtPassword" style="margin-bottom:10px" class="form-control" />
                                         </td></tr>
                                         <tr><td></td><td colspan="100%"><table><tr>
                                         <td style="padding-right:15px">
                                           <button id="btnlogin" class="btn btn-success" style ="background-color:#5cb85c;border-color: #398439;"  title="Check"   name="Login" type="button"><span>Login</span></button>
</td><td >
	<button id = "btnclose" class="btn btn-danger"  type="reset">Close</button>
</td></tr></table></td></tr>
<tr><td>    <a href="user/account.aspx" style="color:Blue;">New User Register Here</a>
</td><td></td></tr></table>

                                       
                                      

  
                                    </div> 
                                  </div>




 

<div class="col-sm-9" id = "dvQlDeliveryAddress" style="display:none">
<div class="row" style="margin-top:15px;margin-bottom:15px">

<div class="col-md-12" >
<div class="row">
<div class="col-xs-12"  >
 
</div>

</div>

<div  class="col-md-12" style=" border-radius: 1px; border:solid 1px silver ;padding:5px 5px 5px">
 
 <div class="row">
 <div class="col-md-12">
<span style="color:#58595b;font-size:16px;font-weight:bold;">DELIVERY INFORMATION</span>
 <hr  style="margin:2px;"/>

 <div id="dvDeliveryAddresses" >
 
  <asp:Literal ID="ltDeliveryAddress" runat="server"></asp:Literal>
 </div>

 </div>
 
 </div>

 


</div>
</div>

 

 


</div>
   

<div class="row" style="margin-top:15px;margin-bottom:15px">

<div class="col-md-12" >
<div class="row">
<div class="col-xs-12"  >
 
</div>

</div>

<div  class="col-md-12" style=" border-radius: 1px; border:solid 1px silver ; padding:5px 5px 5px">
 
 <div class="row">
 <div class="col-md-12">
 <asp:TextBox ID="txtDeliveryAddressId" runat="server" Width="0" Height="0" style="border:0px"></asp:TextBox>
<span style="color:#58595b;font-size:16px;font-weight:bold;">CHOOSE DELIVERY SLOT</span>
 <hr  style="margin:2px;"/><br />
 <asp:UpdatePanel ID="upd1" runat="server">
      
                   
 <ContentTemplate>

 <div class="row">
 <div class="col-md-3"><b>Delivery Day:</b></div>
 <div class="col-md-3"> <asp:DropDownList ID="ddlDays" runat="server" CssClass="form-control"  Width="100%" 
           ></asp:DropDownList></td><td style="padding-left:10px">
          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ForeColor="Red" 
 ControlToValidate="ddlDays"></asp:RequiredFieldValidator></div>
 <div class="col-md-2"><b>Time Slot:</b></div>
 <div class="col-md-3">   <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="upd1" runat="server">
                        <ProgressTemplate>
                            <div id="dvProgress" style="padding-top:5px;position: absolute; width: 200px; height: 35px; background-color: Black;
                                color: White; text-align: center; opacity: 0.7; vertical-align: middle">
                                loading please wait..
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
       

 <asp:DropDownList ID="ddlTimeSlot" runat="server"  CssClass="form-control" Width="100%"></asp:DropDownList>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ForeColor="Red" 
 ControlToValidate="ddlTimeSlot"></asp:RequiredFieldValidator>
 </div>
 <div class="col-md-3"><b>Payment Mode:</b></div>
 <div class="col-md-3">
 <asp:DropDownList ID="ddlPaymentMode" runat="server" CssClass="form-control"  Width="100%">
 <asp:ListItem Text=""></asp:ListItem>
 <asp:ListItem Text="Cash On Delivery" Value="Cash On Delivery"></asp:ListItem>
 </asp:DropDownList>

  <asp:RequiredFieldValidator ID="reqPaymentMode" runat="server" ErrorMessage="*" 
 ForeColor="Red" ControlToValidate="ddlPaymentMode"></asp:RequiredFieldValidator>
 </div>
 
 </div>
 <table>
 <tr>
   <td colspan="100%" style="padding-left:45px" >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td>
                                            <td> <div id="btnOk"  class="btn btn-primary btn-small" >OK</div></td>
                                            <td style="padding-left:5px"> <div id="btnCloseDelivery"  class="btn btn-primary btn-small" >Cancel</div></td>
                                          
                                           
                                            </tr>
                                            </table>
                                            </td>
 </tr>
 
 </table>

  
 </ContentTemplate>
 </asp:UpdatePanel>
 
 
 

  

 </div>
 
 </div>

 


</div>
</div>

 

 


</div>



</div>
 



 

 <div class="row" id="QLPanel">
 <div class="col-md-12">
<span style="color:#58595b;font-size:16px;font-weight:bold;">Quick List</span>
 <hr style="margin:2px;">

 <span style="font-style:italic">Type your items in the list shown below.</span>
 
 <table id= "tbQuickList" style="font-size:12px;color:Silver">
 

 </table>

 	<table style="margin-top:5px">
    <tbody><tr><td>       <input type="text" class="inputCommon" placeholder="Enter Product Name" id="txtQuickListSearch" style="font-size:8pt;font-style:italic">
    
        <div id="QLresult" style="position:absolute;z-index:99999;background:#FFF5EE;max-height:290px;overflow-y:scroll;display:none;left:15px;">
   <div id="tbQLKeywordSearch" style="border:solid 1px silver;border-top:0px;text-align:left;font-size: 11px " >
  
 
 
 
   </div>
        
    </div>
    
    </td>
    
    <td>    <input type="text" id = "txtQQty" class="inputCommon" placeholder="Qty" style="font-size:8pt;font-style:italic;width:30px">
    
     
    
    </td>
   <td style="padding:2px 0px 2px 0px">
   
     <select id= "ddlQuickUnit" style="width:46px;height:29px;border:solid 1px silver;margin-left:10px">
  <option> </option>  
  <option>Gram</option>  
    <option>KG</option>  
      <option>LTR</option>  
  <option>ML</option>  
  <option>MG</option>  
    </select></td>
  
    </tr>
    
     
    <tr   ><td colspan="100%">
<div class="btn btn-success" style="width:100%" id="btnQLPlaceOrder">Place Order</div>
</td></tr>
    </tbody></table>
 

  
 
 

   
 
  

 </div>
 
 </div>

 


</div>
</div>

 

 


</div>

<script>


    $('#txtQuickListSearch').donetyping(function () {


        var Keyword = $(this).val();

        if (Keyword.length >= 3) {

            $("#QLresult").css("display", "block");
            $("#tbQLKeywordSearch").html("<img src='images/searchloader.gif' style='margin:auto;float:right'/>");

            $.ajax({
                type: "POST",
                data: '{"Keyword":"' + Keyword + '"}',
                url: "quickorder.aspx/KeywordSearch",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    // var obj = jQuery.parseJSON(msg.d);

                    
                    var Content = msg.d;
                    $("#tbQLKeywordSearch").html(Content);
                    if (Content.length < 10) {
                        $("#QLresult").css("display", "none");
                        $("#tbQLKeywordSearch").html('No record found for the Search');
                    }







                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {


                }



            });

        }
        else {


            $("#QLresult").css("display", "none");


        }


    });


 

</script>
    <link href="jquery-ui.min.css" rel="stylesheet" />
    <script src="jquery.notifyBar.js"></script>
    <script src="jquery-ui.min.js"></script>
</asp:Content>

