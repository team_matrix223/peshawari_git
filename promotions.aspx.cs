﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class promotions : System.Web.UI.Page
{
    public string SchemeId { get { return Request.QueryString["sc"] != null ? Request.QueryString["sc"] : ""; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        BindSchemeInformation();

        lstOffers.DataSource = new SchemeBLL().GetAllActiveSchemes();
        lstOffers.DataBind();

    }

    [WebMethod]
    public static string FirstTimeATC(string vid, string qty, string st, string type)
    {


        string m_Status = "Plus";

        if (st == "m")
        {
            m_Status = "Minus";

        }



        Int16 IsError = 0;




        int m_Qty = CommonFunctions.ValidateQuantity(qty);
        if (!CommonFunctions.IsValidProductId(vid))
        {
            IsError = 1;
        }

        Calc objCalc = new Calc();
        UserCart objUserCart = new UserCart();
        string html = "";
        string ProductHTML = "";
        int schemeQty = 0;
        if (IsError == 0)
        {
            objUserCart.SessionId = HttpContext.Current.Session.SessionID;
            objUserCart.ProductId = 0;
            objUserCart.CustomerId = 0;
            objUserCart.VariationId = Convert.ToInt64(vid);
            objUserCart.ProductDesc = "";
            objUserCart.Qty = m_Qty;
            objUserCart.Price = 0;
            objUserCart.type = type;

            html = new CartBLL().SchemeProductsInsertUpdate(objUserCart, m_Status, objCalc, out  ProductHTML, out schemeQty);

        }
        var JsonData = new
        {

            qty = objUserCart.Qty,
            error = IsError,
            cartHTML = html,
            Calc = objCalc,
            productHTML = ProductHTML,
            SchemeQty = schemeQty
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

    }



    void BindSchemeInformation()
    {
        Int64 SId = CommonFunctions.IsNumeric(SchemeId);
        string SchemeTitle = "";
        string SchemeDesc = "";
        string SchemeImage = "";
        int SchemeCount = 0;
        string EndDate = "";
        ltSchemeProducts.Text = new ProductsBLL().GetSchemeProducts(Session.SessionID, SId, out SchemeTitle, out SchemeDesc, out SchemeImage, out SchemeCount, out EndDate);
        ltSchemeTitle.Text = SchemeTitle;
        ltSchemeDesc.Text = SchemeDesc;
        ltTotal.Text = SchemeCount.ToString();
        ltValidUpTo.Text = EndDate;

    }
}