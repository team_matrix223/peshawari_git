﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="System" %>
<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup

        RegisterRoutes(RouteTable.Routes);
        // Code that runs on application startup

    }

    public static void RegisterRoutes(RouteCollection routeCollection)
    {

        routeCollection.MapPageRoute("RouteForCustomer", "Customer/{Id}/{*Cid}", "~/Customer.aspx");
        routeCollection.MapPageRoute("test", "test/{Id}/{*Cid}", "~/test.aspx");
        routeCollection.MapPageRoute("groupsRoute", "groups/{id}", "~/groups.aspx");

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }
   
    void Session_Start(object sender, EventArgs e)
    {
        //string ipaddress;
        //ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        //if (ipaddress == "" || ipaddress == null)
        //    ipaddress = Request.ServerVariables["REMOTE_ADDR"];

        //HttpRequest request = base.Request;
        //string address = request.UserHostAddress;
        
        string VisitorsIPAddr = string.Empty;        
        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
        {
            VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
        }

        VisitingUsers objUser = new VisitingUsers()
        {
            IPAddress = VisitorsIPAddr,
            SDate=DateTime.Now,
        };
        int status = new VisitingUsersBLL().InsertUpdate(objUser );
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }


    protected void Application_AuthenticateRequest(Object sender, EventArgs e)
    {
        string cookieName = FormsAuthentication.FormsCookieName;
        HttpCookie authCookie = Context.Request.Cookies[cookieName];

        if (authCookie == null)
        {
            return;
        }
        FormsAuthenticationTicket authTicket = null;
        try
        {
            authTicket = FormsAuthentication.Decrypt(authCookie.Value);
        }
        catch
        {
            return;
        }
        if (authTicket == null)
        {
            return;
        }
        string[] roles = authTicket.UserData.Split(new char[] { '|' });
        FormsIdentity id = new FormsIdentity(authTicket);
        System.Security.Principal.GenericPrincipal principal = new System.Security.Principal.GenericPrincipal(id, roles);

        Context.User = principal;
    }
       
</script>
