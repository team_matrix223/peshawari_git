﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="teste.aspx.cs" Inherits="teste" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	Peshawari Super Market
</title>

   <meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1" />
   <link rel="shortcut icon" type="image/png" href="img/pslogo.png" />
 

  
     


</head>

<body style="background:#E0E0E0">
    <form method="post" action="index.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="frmUser">
 
 

    <div class="container" style="background:white">
        <div class="row" style="background: black;">
            <div class="col-xs-12">
                <div class="menu">
                    <ul>
                     <li><span id="sp_SelectedCity" style="cursor:pointer;color:White;font-family:Myriad Pro;font-size:14px"></span></li>
                        <li><a href="user/dashboard.aspx">My Account</a></li>
                      
                        <li><a href="basket.aspx">Checkout</a></li>
                       
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
               <a href="index.aspx"><img src="images/logo.png" alt="" style="width:200px" /></a> 
            </div>

                        <div class="col-md-6">
        <div id="search" style="margin-top:50px">

    <div id="text">
    <input type="text" id="txtSearch" placeholder="Search from more than 5,000 products.."  style="width:84%;border:solid 1px silver;border-radius:2px;height:32px;padding-left:20px;border-top-right-radius:5px;border-bottom-right-radius:5px"/>
    <div id="btnSearch" style="background:#6BA52E;height:32px;width:70px;color:White;float:left;vertical-align:middle;text-align:center;padding:5px;border-top-left-radius: 5px; border-bottom-left-radius: 5px;">
      Search
    </div>
    </div>
    
    <div id="result" style="position:absolute;z-index:99999;background:white;max-height:400px;overflow-y:scroll;display:none;width:452px">
   <div id="tbKeywordSearch" style="border:solid 1px silver;border-top:0px;text-align:center " >
  
 
 
 
   </div>
        
    </div>

    </div>
            </div>



            <div class="col-md-3">
                <div class="phone" style="width:350px">
                <ul>
                        <h3>
                          <b style="font-size:22px ">  Order on call:</b></h3>
                        <li><a href="tel:8699099035" style="color:red "><b style="font-size:22px ">869-9099-035</b> </a></li>
                    </ul>
                </div>
                <div class="item">
                    <table>
                        <tr>
                            <td>
                                <img src="images/shopping-cart.png" alt="" />
                            </td>
                            <td>
                                <h5>
                                    <a href="basket.aspx" style="color: Black">MY CART</a></h5>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="100%">
                                <p style="font-size: 12px; color: #808182;">
                                    </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        
        </div>



     

<ul id="main-menu"   class="sm sm-mint" style="display:none;z-index:99">
  <li style="background:#6BA52E;color:White"><a href="index.aspx" style="color:White;border-radius:0px;padding:12px">Home</a></li>
 
  
     
     
    <li>
    <a href="list.aspx?c=1">Fruits & Vegetables </a><ul class="mega-menu">
    <li><div style="width:100%;"><div style="padding:5px 8px;">

    
      <div style='width:100%;'>
        <div style='padding:5px 8px;'>


           	 
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=12" style="color:Green;font-size:11px">
        FRUITS ( 21)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=521" style="font-size:11px;margin:3px">
        SARDA( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=103" style="font-size:11px;margin:3px">
        APPLE( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=104" style="font-size:11px;margin:3px">
        BANANA( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=105" style="font-size:11px;margin:3px">
        GRAPES( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=106" style="font-size:11px;margin:3px">
        MANGOES( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=107" style="font-size:11px;margin:3px">
        POMEGRANATE/ANAR( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=108" style="font-size:11px;margin:3px">
        ORANGES & SWEET LIME( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=109" style="font-size:11px;margin:3px">
        KIWI( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=465" style="font-size:11px;margin:3px">
        APRICOTS( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=466" style="font-size:11px;margin:3px">
        CHERRY( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=467" style="font-size:11px;margin:3px">
        COCONUT( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=468" style="font-size:11px;margin:3px">
        PAPAYA( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=469" style="font-size:11px;margin:3px">
        PEARS( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=470" style="font-size:11px;margin:3px">
        PEACH( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=472" style="font-size:11px;margin:3px">
        PLUM( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=473" style="font-size:11px;margin:3px">
        SWEET TAMARIND( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=542" style="font-size:11px;margin:3px">
        KHARBOOJA( 1)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=13" style="color:Green;font-size:11px">
        EXOTIC VEGETABLES ( 12)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=506" style="font-size:11px;margin:3px">
        AVOCADO( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=540" style="font-size:11px;margin:3px">
        YELLOW CAPSICUM( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=514" style="font-size:11px;margin:3px">
        CELERY( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=516" style="font-size:11px;margin:3px">
        PARSHLEY( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=474" style="font-size:11px;margin:3px">
        BABYCORN( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=475" style="font-size:11px;margin:3px">
        BROCCOLI( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=476" style="font-size:11px;margin:3px">
        CHERRY TOMATO( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=477" style="font-size:11px;margin:3px">
        ICEBURG LETTUCE( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=478" style="font-size:11px;margin:3px">
        GREEN LETTUCE( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=479" style="font-size:11px;margin:3px">
        RED CABBAGE( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=480" style="font-size:11px;margin:3px">
        ZUCCINI( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=482" style="font-size:11px;margin:3px">
        RED CAPSICUM( 1)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=15" style="color:Green;font-size:11px">
        VEGETABLES ( 25)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=503" style="font-size:11px;margin:3px">
        LOKI/GHIYA( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=505" style="font-size:11px;margin:3px">
        CABBAGE( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=507" style="font-size:11px;margin:3px">
        GINGER( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=484" style="font-size:11px;margin:3px">
        CARROTS( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=110" style="font-size:11px;margin:3px">
        FRANSBEAN( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=111" style="font-size:11px;margin:3px">
        BRINJALS( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=112" style="font-size:11px;margin:3px">
        CHILLI GREEN( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=113" style="font-size:11px;margin:3px">
        LEMON( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=114" style="font-size:11px;margin:3px">
        CUCUMBER( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=115" style="font-size:11px;margin:3px">
        POTATO( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=116" style="font-size:11px;margin:3px">
        ONION( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=485" style="font-size:11px;margin:3px">
        GARLIC( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=117" style="font-size:11px;margin:3px">
        TOMATO( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=118" style="font-size:11px;margin:3px">
        LADY FINGER( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=488" style="font-size:11px;margin:3px">
        GREEN PEAS( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=489" style="font-size:11px;margin:3px">
        CAULI FLOWER( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=491" style="font-size:11px;margin:3px">
        GREEN CAPSICUM( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=492" style="font-size:11px;margin:3px">
        MUSHROOMS( 1)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=522" style="color:Green;font-size:11px">
        OTHER VEGETABLES ( 1)</a> </b>  
        
 
         
       
         </div>


    
       </div>

    
             
             
              </div></div>

    


   </li></ul></li>

     
     
    <li>
    <a href="list.aspx?c=2">Grocery & Staples </a><ul class="mega-menu">
    <li><div style="width:100%;"><div style="padding:5px 8px;">

    
      <div style='width:100%;'>
        <div style='padding:5px 8px;'>


           	 
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=18" style="color:Green;font-size:11px">
        DALS & PULSES ( 44)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=153" style="font-size:11px;margin:3px">
        ORGANIC DALS & PULSES( 20)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=519" style="font-size:11px;margin:3px">
        REGULAR DALS & PULSES( 24)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=19" style="color:Green;font-size:11px">
        DRY FRUIT ( 49)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=126" style="font-size:11px;margin:3px">
        ALMONDS( 10)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=127" style="font-size:11px;margin:3px">
        CASHEWS( 6)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=129" style="font-size:11px;margin:3px">
        PISTA( 6)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=130" style="font-size:11px;margin:3px">
        RAISINS( 6)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=131" style="font-size:11px;margin:3px">
        OTHER DRY FRUITS( 7)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=526" style="font-size:11px;margin:3px">
        AKHROT( 8)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=527" style="font-size:11px;margin:3px">
        ANJEER( 3)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=535" style="font-size:11px;margin:3px">
        KHURMANI( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=536" style="font-size:11px;margin:3px">
        CHUARA( 1)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=20" style="color:Green;font-size:11px">
        EDIBLE OILS ( 26)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=134" style="font-size:11px;margin:3px">
        SOYABEAN OIL( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=136" style="font-size:11px;margin:3px">
        MUSTARD OILS( 3)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=137" style="font-size:11px;margin:3px">
        OLIVE OILS( 7)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=138" style="font-size:11px;margin:3px">
        RICE BRAN( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=139" style="font-size:11px;margin:3px">
        SUNFLOWER OILS( 7)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=438" style="font-size:11px;margin:3px">
        OTHER OILS( 6)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=21" style="color:Green;font-size:11px">
        FLOURS & SOOJI ( 31)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=141" style="font-size:11px;margin:3px">
        BESAN( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=142" style="font-size:11px;margin:3px">
        MAIDA( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=144" style="font-size:11px;margin:3px">
        SOOJI & RAVA( 3)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=145" style="font-size:11px;margin:3px">
        WHEAT ATTA( 7)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=146" style="font-size:11px;margin:3px">
        OTHER FLOURS( 16)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=156" style="font-size:11px;margin:3px">
        ORGANIC FLOURS( 1)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=22" style="color:Green;font-size:11px">
        MASALAS & SPICES ( 131)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=147" style="font-size:11px;margin:3px">
        COOKING PASTE( 15)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=148" style="font-size:11px;margin:3px">
        ESSENCES & FOOD FLAVOUR( 0)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=149" style="font-size:11px;margin:3px">
        HERBS & SEASONINGS( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=150" style="font-size:11px;margin:3px">
        POWDERED SPICES( 12)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=151" style="font-size:11px;margin:3px">
        READY MASALAS( 68)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=152" style="font-size:11px;margin:3px">
        WHOLE SPICES( 30)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=25" style="color:Green;font-size:11px">
        RICE & RICE PRODUCTS ( 27)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=162" style="font-size:11px;margin:3px">
        RICE-LOOSE( 8)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=164" style="font-size:11px;margin:3px">
        RICE-BRANDED( 13)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=166" style="font-size:11px;margin:3px">
        RICE PRODUCTS( 6)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=26" style="color:Green;font-size:11px">
        SALT, SUGAR & JAGGERY ( 31)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=501" style="font-size:11px;margin:3px">
        HONEY( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=167" style="font-size:11px;margin:3px">
        JAGGERY( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=168" style="font-size:11px;margin:3px">
        SALT( 4)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=169" style="font-size:11px;margin:3px">
        SUGAR( 14)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=170" style="font-size:11px;margin:3px">
        SUGAR CUBES( 3)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=171" style="font-size:11px;margin:3px">
        SUGAR FREE( 6)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=497" style="color:Green;font-size:11px">
        PAPAD & WARIYAN ( 17)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=496" style="font-size:11px;margin:3px">
        SOYA & SOYA PRODUCTS( 7)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=498" style="font-size:11px;margin:3px">
        PAPAD( 6)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=499" style="font-size:11px;margin:3px">
        WARIYAN( 4)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=520" style="color:Green;font-size:11px">
        DESI GHEE & VANASPATI ( 8)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=133" style="font-size:11px;margin:3px">
        DESI GHEE( 8)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=140" style="font-size:11px;margin:3px">
        VANASPATI( 0)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
             
             
              </div></div>

    


   </li></ul></li>

     
     
    <li>
    <a href="list.aspx?c=3">Bread Dairy & Eggs </a><ul class="mega-menu">
    <li><div style="width:100%;"><div style="padding:5px 8px;">

    
      <div style='width:100%;'>
        <div style='padding:5px 8px;'>


           	 
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=27" style="color:Green;font-size:11px">
        BREAD & BAKERY ( 34)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=173" style="font-size:11px;margin:3px">
        BREADS( 12)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=174" style="font-size:11px;margin:3px">
        BISCUITS & COOKIES( 10)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=175" style="font-size:11px;margin:3px">
        CAKES & MUFFINS( 8)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=176" style="font-size:11px;margin:3px">
        RUSK( 4)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=28" style="color:Green;font-size:11px">
        DAIRY PRODUCTS ( 63)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=178" style="font-size:11px;margin:3px">
        BUTTER( 3)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=180" style="font-size:11px;margin:3px">
        CHEESE-SPREAD-SLICES( 21)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=182" style="font-size:11px;margin:3px">
        CURD( 11)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=184" style="font-size:11px;margin:3px">
        MILK & MILK PRODUCTS( 25)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=186" style="font-size:11px;margin:3px">
        OTHER MILK PRODUCTS( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=187" style="font-size:11px;margin:3px">
        SOYA PANEER & TOFU( 2)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
             
             
              </div></div>

    


   </li></ul></li>

     
     
    <li>
    <a href="list.aspx?c=4">Beverages </a><ul class="mega-menu">
    <li><div style="width:100%;"><div style="padding:5px 8px;">

    
      <div style='width:100%;'>
        <div style='padding:5px 8px;'>


           	 
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=32" style="color:Green;font-size:11px">
        HEALTH DRINKS & CONCENTRATES ( 39)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=191" style="font-size:11px;margin:3px">
        MILK MIXES( 31)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=192" style="font-size:11px;margin:3px">
        CONCENTRATES( 4)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=185" style="font-size:11px;margin:3px">
        MILK DRINKS( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=190" style="font-size:11px;margin:3px">
        ENERGY DRINKS( 2)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=33" style="color:Green;font-size:11px">
        JUICES & DRINKS ( 71)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=193" style="font-size:11px;margin:3px">
        JUICES( 46)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=195" style="font-size:11px;margin:3px">
        SYRUPS & SQUASHES( 16)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=188" style="font-size:11px;margin:3px">
        SOYA DRINKS( 8)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=189" style="font-size:11px;margin:3px">
        YOGURT & LASSI( 1)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=34" style="color:Green;font-size:11px">
        WATER & SODA ( 12)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=197" style="font-size:11px;margin:3px">
        MINERAL WATER( 6)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=202" style="font-size:11px;margin:3px">
        SODA & FLAVOURS( 6)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=37" style="color:Green;font-size:11px">
        SOFT DRINKS ( 32)</a> </b>  
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=38" style="color:Green;font-size:11px">
        TEA & COFFEE ( 96)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=203" style="font-size:11px;margin:3px">
        DAIRY WHITENERS & CREAMERS( 3)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=206" style="font-size:11px;margin:3px">
        COFFEE( 9)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=208" style="font-size:11px;margin:3px">
        TEA( 44)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=209" style="font-size:11px;margin:3px">
        TEA BAGS( 40)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
             
             
              </div></div>

    


   </li></ul></li>

     
     
    <li>
    <a href="list.aspx?c=5">Branded Foods </a><ul class="mega-menu">
    <li><div style="width:100%;"><div style="padding:5px 8px;">

    
      <div style='width:100%;'>
        <div style='padding:5px 8px;'>


           	 
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=39" style="color:Green;font-size:11px">
        AYURVEDIC FOOD ( 23)</a> </b>  
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=40" style="color:Green;font-size:11px">
        BABY FOOD ( 16)</a> </b>  
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=41" style="color:Green;font-size:11px">
        BAKING & DESSERT ITEMS ( 7)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=213" style="font-size:11px;margin:3px">
        BAKING INGREDIENTS( 7)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=42" style="color:Green;font-size:11px">
        BISCUITS ( 124)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=529" style="font-size:11px;margin:3px">
        REGULAR BISCUITS( 16)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=530" style="font-size:11px;margin:3px">
        HEALTHY & DIGESTIVE BISCUITS( 10)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=537" style="font-size:11px;margin:3px">
        IMPORTED BISCUITS( 3)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=538" style="font-size:11px;margin:3px">
        COOKIES( 7)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=216" style="font-size:11px;margin:3px">
        SUGARLESS COOKIES( 8)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=217" style="font-size:11px;margin:3px">
        CREAM BISCUITS( 77)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=531" style="font-size:11px;margin:3px">
        SALTY BISCUITS( 3)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=43" style="color:Green;font-size:11px">
        BREAKFAST CEREALS ( 50)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=223" style="font-size:11px;margin:3px">
        FLAKES( 22)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=224" style="font-size:11px;margin:3px">
        MUESLI( 9)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=225" style="font-size:11px;margin:3px">
        OATS( 12)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=226" style="font-size:11px;margin:3px">
        BRAN AND PORRIDGE( 6)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=44" style="color:Green;font-size:11px">
        CANNED FOOD ( 8)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=227" style="font-size:11px;margin:3px">
        VEG & FRUIT( 8)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=45" style="color:Green;font-size:11px">
        CHOCOLATES & SWEETS ( 51)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=228" style="font-size:11px;margin:3px">
        CHEWING GUM( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=229" style="font-size:11px;margin:3px">
        CHOCOLATE( 45)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=230" style="font-size:11px;margin:3px">
        FESTIVE GIFT PACKS( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=231" style="font-size:11px;margin:3px">
        TOFFEE & CANDY( 3)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=46" style="color:Green;font-size:11px">
        HEALTH & NUTRITION ( 11)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=543" style="font-size:11px;margin:3px">
        GLUTEN FREE( 7)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=48" style="color:Green;font-size:11px">
        JAMS & SPREADS ( 34)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=236" style="font-size:11px;margin:3px">
        MAYONNAISE( 10)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=238" style="font-size:11px;margin:3px">
        OTHER SPREADS( 11)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=239" style="font-size:11px;margin:3px">
        JAMS & PRESERVES( 13)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=49" style="color:Green;font-size:11px">
        NOODLES ( 5)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=240" style="font-size:11px;margin:3px">
        INSTANT NOODLES( 5)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=51" style="color:Green;font-size:11px">
        PASTA & VERMICELLI ( 13)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=243" style="font-size:11px;margin:3px">
        INSTANT PASTAS( 6)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=244" style="font-size:11px;margin:3px">
        REGULAR PASTA( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=245" style="font-size:11px;margin:3px">
        VERMICELLI( 6)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=52" style="color:Green;font-size:11px">
        PICKLES ( 29)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=249" style="font-size:11px;margin:3px">
        VEG PICKLE( 29)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=53" style="color:Green;font-size:11px">
        SAUCES & KETCHUP ( 66)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=252" style="font-size:11px;margin:3px">
        TOMATO KETCHUP( 3)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=253" style="font-size:11px;margin:3px">
        TOMATO SAUCE( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=254" style="font-size:11px;margin:3px">
        VINEGAR( 8)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=255" style="font-size:11px;margin:3px">
        OTHER SAUCES( 51)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=54" style="color:Green;font-size:11px">
        VEG SNACKS ( 62)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=256" style="font-size:11px;margin:3px">
        CHIPS( 16)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=260" style="font-size:11px;margin:3px">
        NAMKEEN( 44)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=261" style="font-size:11px;margin:3px">
        POPCORN( 2)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=263" style="color:Green;font-size:11px">
        READY TO EAT & COOK ( 87)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=264" style="font-size:11px;margin:3px">
        COOKING PASTES & POWDERS( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=265" style="font-size:11px;margin:3px">
        FROZEN NON VEG FOOD( 14)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=266" style="font-size:11px;margin:3px">
        FROZEN VEG FOOD( 23)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=267" style="font-size:11px;margin:3px">
        FRYUMS & PAPAD( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=268" style="font-size:11px;margin:3px">
        HEAT & EAT READY MEALS( 19)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=269" style="font-size:11px;margin:3px">
        READY MIX( 19)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=271" style="font-size:11px;margin:3px">
        SOUP( 9)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
             
             
              </div></div>

    


   </li></ul></li>

     
     
    <li>
    <a href="list.aspx?c=6">Personal Care </a><ul class="mega-menu">
    <li><div style="width:100%;"><div style="padding:5px 8px;">

    
      <div style='width:100%;'>
        <div style='padding:5px 8px;'>


           	 
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=56" style="color:Green;font-size:11px">
        BABY CARE ( 40)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=272" style="font-size:11px;margin:3px">
        BABY CARE ACCESSORIES( 3)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=273" style="font-size:11px;margin:3px">
        BABY CREAMS & LOTION( 11)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=274" style="font-size:11px;margin:3px">
        BABY OIL & SHAMPOOS( 3)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=275" style="font-size:11px;margin:3px">
        BABY POWDER( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=276" style="font-size:11px;margin:3px">
        BABY SOAP( 3)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=277" style="font-size:11px;margin:3px">
        DIAPERS & WIPES( 19)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=57" style="color:Green;font-size:11px">
        COSMETICS ( 1)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=279" style="font-size:11px;margin:3px">
        EYE CARE & KAJAL( 1)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=58" style="color:Green;font-size:11px">
        DEOS & PERFUMES ( 9)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=439" style="font-size:11px;margin:3px">
        WOMENS DEO( 5)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=281" style="font-size:11px;margin:3px">
        MENS DEO( 4)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=62" style="color:Green;font-size:11px">
        HAIR CARE ( 36)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=293" style="font-size:11px;margin:3px">
        HAIR CONDITIONER( 7)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=294" style="font-size:11px;margin:3px">
        HAIR OIL( 4)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=295" style="font-size:11px;margin:3px">
        SHAMPOO( 23)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=296" style="font-size:11px;margin:3px">
        STYLING PRODUCTS( 2)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=65" style="color:Green;font-size:11px">
        ORAL CARE ( 67)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=304" style="font-size:11px;margin:3px">
        MOUTHWASH( 10)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=305" style="font-size:11px;margin:3px">
        TOOTH POWDER( 3)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=306" style="font-size:11px;margin:3px">
        TOOTHBRUSH( 21)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=307" style="font-size:11px;margin:3px">
        TOOTHPASTE( 33)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=66" style="color:Green;font-size:11px">
        SANITARY NEEDS ( 5)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=315" style="font-size:11px;margin:3px">
        SANITARY PADS( 5)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=67" style="color:Green;font-size:11px">
        PERSONAL HYGIENE ( 31)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=308" style="font-size:11px;margin:3px">
        BODY WASH( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=309" style="font-size:11px;margin:3px">
        FACE WASH( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=310" style="font-size:11px;margin:3px">
        HAND SANITIZER( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=311" style="font-size:11px;margin:3px">
        HAND WASH( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=312" style="font-size:11px;margin:3px">
        LIQUID SOAPS & BARS( 26)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=69" style="color:Green;font-size:11px">
        SHAVING NEEDS ( 34)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=316" style="font-size:11px;margin:3px">
        AFTER SHAVE( 3)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=317" style="font-size:11px;margin:3px">
        SHAVING BLADE & RAZORS( 25)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=319" style="font-size:11px;margin:3px">
        SHAVING CREAM, FOAM & GELS( 6)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=70" style="color:Green;font-size:11px">
        SKIN CARE ( 47)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=320" style="font-size:11px;margin:3px">
        BODY LOTION( 3)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=323" style="font-size:11px;margin:3px">
        FACE CREAM( 22)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=324" style="font-size:11px;margin:3px">
        FACE PACKS & SCRUBS( 14)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=327" style="font-size:11px;margin:3px">
        GROOMING( 5)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=328" style="font-size:11px;margin:3px">
        LIP CARE( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=329" style="font-size:11px;margin:3px">
        TALC( 2)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
             
             
              </div></div>

    


   </li></ul></li>

     
     
    <li>
    <a href="list.aspx?c=7">Household </a><ul class="mega-menu">
    <li><div style="width:100%;"><div style="padding:5px 8px;">

    
      <div style='width:100%;'>
        <div style='padding:5px 8px;'>


           	 
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=75" style="color:Green;font-size:11px">
        CLEANING ACCESSORIES ( 6)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=337" style="font-size:11px;margin:3px">
        BROOMS & DUST PANS( 2)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=342" style="font-size:11px;margin:3px">
        WIPER( 4)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=78" style="color:Green;font-size:11px">
        DETERGENTS ( 34)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=349" style="font-size:11px;margin:3px">
        LIQUID DETERGENT( 4)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=350" style="font-size:11px;margin:3px">
        PRE & POST WASH CARE( 7)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=351" style="font-size:11px;margin:3px">
        WASHING BARS( 8)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=352" style="font-size:11px;margin:3px">
        WASHING POWDERS( 15)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=79" style="color:Green;font-size:11px">
        ELECTRICALS ( 1)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=353" style="font-size:11px;margin:3px">
        BATTERY( 1)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=85" style="color:Green;font-size:11px">
        PAPER & DISPOSABLE ( 11)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=381" style="font-size:11px;margin:3px">
        ALUMINIUM FOIL & CLING WRAP( 5)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=386" style="font-size:11px;margin:3px">
        TISSUE PAPER & NAPKINS( 4)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=387" style="font-size:11px;margin:3px">
        TOILET PAPER( 2)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=88" style="color:Green;font-size:11px">
        POOJA NEEDS ( 2)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=397" style="font-size:11px;margin:3px">
        AGARBATTI( 2)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=89" style="color:Green;font-size:11px">
        REPELLENTS & FRESHENERS ( 3)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=400" style="font-size:11px;margin:3px">
        HOME & AIR FRESHENER( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=401" style="font-size:11px;margin:3px">
        MOSQUITO REPELLENT( 2)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=92" style="color:Green;font-size:11px">
        SHOE CARE ( 9)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=407" style="font-size:11px;margin:3px">
        SHOE POLISH( 9)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=94" style="color:Green;font-size:11px">
        TOILET, FLOOR & OTHER CLEANERS ( 13)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=453" style="font-size:11px;margin:3px">
        DRAIN CLEANERS( 1)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=454" style="font-size:11px;margin:3px">
        FLOOR CLEANERS( 7)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=458" style="font-size:11px;margin:3px">
        TOILET CLEANERS( 5)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=95" style="color:Green;font-size:11px">
        UTENSIL CLEANERS ( 28)</a> </b>  
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=460" style="font-size:11px;margin:3px">
        DISHWASH BARS( 9)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=461" style="font-size:11px;margin:3px">
        DISHWASH LIQUIDS & PASTES( 7)</a>
       </span> 
      
        
        
        
       
       <span style="width:100%">
        <a href="list.aspx?sc=463" style="font-size:11px;margin:3px">
        UTENSIL SCRUB PADS( 12)</a>
       </span> 
      
        
        
        
 
         
       
         </div>


    
       </div>

    
             
             
              </div></div>

    


   </li></ul></li>

     
     
    <li>
    <a href="list.aspx?c=9">Meat </a><ul class="mega-menu">
    <li><div style="width:100%;"><div style="padding:5px 8px;">

    
      <div style='width:100%;'>
        <div style='padding:5px 8px;'>


           	 
      <div class='row'>
      
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=523" style="color:Green;font-size:11px">
        MUTTON ( 7)</a> </b>  
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=524" style="color:Green;font-size:11px">
        CHICKEN ( 7)</a> </b>  
        
 
         
       
         </div>


    
    
     <div class='col-md-4'> <br />
        <b>
       
        <a href="list.aspx?s=533" style="color:Green;font-size:11px">
        NON-VEG FROZEN ( 6)</a> </b>  
        
 
         
       
         </div>


    
       </div>

    
             
             
              </div></div>

    


   </li></ul></li>

     

     <li style="background:#6BA52E;color:White"><a href="user/account.aspx" style="color:White;border-radius:0px;padding:12px">Login</a></li>
 
</ul>



<div class="list-group" style="margin:10px 0 0 0;position:relative;width:100%;z-index:99999">
 






   <div id='cssmenu' style="width:94%">
<ul>

<!--LITERAL MENU-->
</ul>
</div>




    
          </div>
        
        <div class="row">
            <div class="col-sm-3 border ">
                <div class="freedelivery">
                    <table>
                        <tr>
                            <td>
                                <img src="images/free-delivery.png" alt="" />
                            </td>
                            <td>
                                <h2>
                                    FREE DELIVERY</h2>
                                <p style="font-size: 12px;font-family:cursive">
                                    On order Value Above Rs.1000</p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-3 border ">
                <div class="freedelivery">
                    <table>
                        <tr>
                            <td>
                                <img src="images/calender.png" alt="" />
                            </td>
                            <td>
                               <div id="dvShowSlots" style="cursor:pointer">
                                  <h2> DELIVERY SLOTS</h2>
                                     <p style="font-size: 12px;font-family:cursive">
                                    Click here to view Delivery Slots</p>
                                  </div> 
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-3 ">
                <div class="freedelivery">
                    <table>
                        <tr>
                            <td>
                                <img src="images/money.png" alt="" />
                            </td>
                            <td>
                                <h2>
                                     ORDER AMOUNT</h2>
                                <p style="font-size: 12px;font-family:cursive">
                                    Minimum Amount for Order is Rs.500</p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="callus">
                    <table>
                        <tr>
                            <td>
                                <img src="images/phonepic.png" alt="" />
                            </td>
                            <td>
                                <h2>
                                    CALL US :  0172-5005544 </h2>
                                <p style="font-size: 12px; color: #FFFFFF;">
                                    Need Help Call Us</p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
            <div class="row" style="margin-top:15px;">
     
            <div class="col-md-4">
            </div>
                 
            <div class="col-md-4">
            <div id="dvPopupDSlots" style="display:none;border:solid 2px silver" >
  <table width="100%" cellpadding="3" >
                    

                     <tr style="background-color:#386D3F;color:White;height:25px">
                     <td valign="top" style="text-align:center " colspan="100%" >
                    <b> DELIVERY SLOTS</b>
                  
                    </td>
                    <td valign="top">  <div id="dvclose" style="cursor:pointer"><b>Close</b></div></td>
                    </tr>
                    <tr><td colspan="100%" align="center">Time Slots From Monday to Saturday</td></tr>
                    <tr>
                    <td style="padding-bottom:10px" align="center">
                   <span id="sp_TimeSlots"></span>
                  
                    </td></tr>
            
                    </table>
                    </div>
            </div>
            <div class="col-md-4">
            </div>


</div>
       
        
        
    
        <link rel="shortcut icon" type="image/png" href="img/pslogo.png">
 

     

      
     

    

 

<!--Row End-->


<!--Row Start-->
 
<div class="row">




 <div class="col-md-12" style="padding-right:0px" >
<div class="slider">

<div id="slider2_container" style="display: none; position: relative; margin: 0 auto; width: 980px;
        height: 350px; overflow: hidden;">

            <!-- Loading Screen -- previous height :522px>
            <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;

                background-color: #000; top: 0px; left: 0px;width: 100%; height:100%;">
                </div>
                <div style="position: absolute; display: block; background: url(img/loading.gif) no-repeat center center;

                top: 0px; left: 0px;width: 100%;height:100%;">
                </div>
            </div>

            <!-- Slides Container -->
            <div    u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 980px; height: 350px;
            overflow: hidden;">
                  <div><img u='image' src2='SliderImages/701402015081061540AM562p0pic01.jpg' /></div><div><img u='image' src2='SliderImages/7014020150101561480AM765p0pic02.jpg' /></div><div><img u='image' src2='SliderImages/7018020150121271270AM801.jpg' /></div><div><img u='image' src2='SliderImages/7018020150121281090AM333.jpg' /></div><div><img u='image' src2='SliderImages/7018020150121381400AM5962.jpg' /></div>


            </div>
            <!-- Bullet Navigator Skin Begin -->
            
            <!-- bullet navigator container -->
            <div u="navigator" class="jssorb05" style="position: absolute; bottom: 16px; right: 6px;">
                <!-- bullet navigator item prototype -->
                <div u="prototype" style="POSITION: absolute; WIDTH: 16px; HEIGHT: 16px;"></div>
            </div>
            <!-- Bullet Navigator Skin End -->
            <!-- Arrow Navigator Skin Begin -->

       
            <!-- Arrow Left -->
            <span u="arrowleft" class="jssora11l" style="width: 37px; height: 37px; top: 123px; left: 8px;">
            </span>
            <!-- Arrow Right -->
            <span u="arrowright" class="jssora11r" style="width: 37px; height: 37px; top: 123px; right: 8px">
            </span>
            <!-- Arrow Navigator Skin End -->
            <a style="display: none" href="http://www.jssor.com">jQuery Slider</a>
        </div>


 </div>

</div>


 
</div>
<!--Row End-->


<!--GROUP CATEGORIES  -->

 <div class="row">
            
            
            <div class="col-md-12">
  					
              <h2 style="border-bottom: 1px dashed silver; border-top: 1px dashed silver; color: gray; padding: 7px; font-size: 20px; margin-top: 10px; background: none repeat scroll 0% 0% rgb(238, 238, 238); font-family: serif;">PESHAWARI QUICK SHOPPING GUIDE</h2>
  					</div>
            
        </div>

<div class="row">
 
 

 
<div id="myGroups">

</div>

 
</div>

<!--GROUP END  -->

    <!--Row Start-->
<div class="row" style="margin-top:15px;">
     
            <div class="col-md-4">
            </div>
                 
            <div class="col-md-4">
            <div id="dvPopupCheck" style="display:none;border:solid 2px silver" >
  <table width="100%" cellpadding="3" >
                    

                     <tr>
                     <td valign="top" style="background-color:green ;text-align:center;color:white;text-transform:uppercase;padding:3px;font-family:sans-serif" colspan="100%" >
                    <b> Check Delivery Availability</b>
                    </td>
                    </tr>
                    <tr>
                    <td style="padding-bottom:10px;padding-top:20px">Choose City</td>
                    <td style="padding-bottom:10px;padding-top:20px"">
                    <select name="ctl00$cnMaster$ddlCity" id="cnMaster_ddlCity" class="form-control" style="width:200px">
</select>
                    </td></tr>
                    
                    <tr>
                    <td style="padding-bottom:10px">
                    Choose Sector</td>
                    <td style="padding-bottom:10px">
                    <select name="ctl00$cnMaster$ddlPincode" id="cnMaster_ddlPincode" class="form-control" style="width:200px">
</select>
                    </td></tr>
                    <tr>
                    <td></td>
                    <td>
                    <button id="zipcheck" class="btn" title="Check" style="color:white;background:green;  " name="zip-check" type="button"><span>Proceed</span></button>
                    </td>
                    </tr>
                    </table>
                    </div>
            </div>
            <div class="col-md-4">
            </div>


</div>
<!--Row End-->

<!--Row Start-->
<div class="row" style="margin-top:15px;">





</div>
<!--Row End-->


<!--Row Start-->
<div class="row">

<div class="col-sm-9">
 
    <div    id="featuredCategories">


    </div>


    <div  id="featuredComboTypes">


</div>

</div>



<div class="col-md-3"   >
<div class="yourbasket">




<div id="dvTopBasket" style="border-bottom:solid 2px #d4d4d4;;padding:6px">

 
<img src="images/basket.png" alt="" > <h6 style="float:right;margin-right:0px;font-size:16px">Your Basket (<span id="sp_TotalItems"></span> items)</h6>
 </div>
 
<div class="basketitem" >

 <div id="cartcontainer" style="max-height:200px;overflow-y:scroll">
 </div>
   
  
 
 <table>
 <tr>
 <td style="padding:10px 0px 0px 70px;">
 <table>
    <tr ><td><h3 style="color:red"><b><span id="msg"></span> </b></h3> </td></tr>
 <tr><td><h4><b><span id="sp1">Sub Total: Rs.</span> <span id="subtotal"></span></b></h4></td></tr>
 <tr>
 <td><h5><span id="sp2">Delivery Charges: Rs.</span>  <span id="delivery"></span></h5></td>
 </tr>
 <tr>
 <td><h5 style="color:#f40f00;margin-left:10px"><span id="sp3">Total: Rs.</span><span id="netamount"></span></h5></td>
 </tr>
 <tr>
 <td style="padding:10px 0 0 50px;">

<button id="btnCheckOut" type="button" class="btn btn-success">Checkout</button>
     
 
 </td>
 </tr>
 </table>
 </td>
 </tr>
 </table>
 
 
 
 
 
<br />

 
 
 
 </div>
 <span id="sp_msg" style="color:red;font-size:10px;padding-left:5px"></span>
</div>

 <input type="hidden" id="hdDeliveryCharge" />
      <input type="hidden" id="hdMinimumCheckOutAmt" />
      <input type="hidden" id="hdFreedeliveryAmt" />
      <input type="hidden" id="hdTotalAmt" />
<div class="yourbasket"   >


 <div class="enqury"><h2>HELP US IMPROVE</h2></div>

<div id="contact_form"  style="margin:15px 0 0 30px";  >
	
    
    <div class="row"  style="margin-bottom:10px">
       
        <input name="ctl00$cnMaster$ucEnquiryForm1$txtPersonName" type="text" id="cnMaster_ucEnquiryForm1_txtPersonName" class="inputCommon" PlaceHolder="Enter Your Name" style="font-size:8pt;font-style:italic" />

         <span id="cnMaster_ucEnquiryForm1_reqPersonName" style="color:Red;visibility:hidden;">*</span>
	</div>

    <div class="row" style="margin-bottom:10px">
	 <input name="ctl00$cnMaster$ucEnquiryForm1$txtEmail" type="text" id="cnMaster_ucEnquiryForm1_txtEmail" class="inputCommon" PlaceHolder="Enter Your Email" style="font-size:8pt;font-style:italic" />
	  <span id="cnMaster_ucEnquiryForm1_reqEmailID" style="color:Red;display:none;">*</span>

	  <span id="cnMaster_ucEnquiryForm1_RequiredFieldValidator1" style="color:Red;display:none;">*</span>

    <br />
    </div>

    <div class="row" style="margin-bottom:10px">
    <textarea name="ctl00$cnMaster$ucEnquiryForm1$txtMessage" rows="2" cols="20" id="cnMaster_ucEnquiryForm1_txtMessage" class="input inputEnquiry bigtext" PlaceHolder="Help Us Improve Our Services by reporting Issue or Suggesting new Ideas. If we found it Valuable, you will get a Gift Voucher. Thank you." Multiline="true" style="height:85px;width:190px;">
</textarea>
              <span id="cnMaster_ucEnquiryForm1_reqEnquiryQuestion" style="color:Red;visibility:hidden;">*</span>
	</div>
    
    
	<div class="row" style="margin-bottom:10px" >
	 <input name="ctl00$cnMaster$ucEnquiryForm1$txtMobile" type="text" id="cnMaster_ucEnquiryForm1_txtMobile" class="inputCommon" PlaceHolder="Enter Your MobileNo" style="font-size:8pt;font-style:italic" />
	
    </div>
 <input type="submit" name="ctl00$cnMaster$ucEnquiryForm1$btnAsk" value="Send" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$cnMaster$ucEnquiryForm1$btnAsk&quot;, &quot;&quot;, true, &quot;enquiry&quot;, &quot;&quot;, false, false))" id="cnMaster_ucEnquiryForm1_btnAsk" class="btn btn-success" />


  
    </div>

  
    </div>
  
 
<div class="yourbasket" style="border:0px" id="dvRightAdd">



    </div>
   	





    <br />
 <br />
    <br />
 
 
</div>


</div>





<div class="row">
<div class="col-md-12" id="dvBottomAdd">




</div>
 
</div>




        <!--Row Start-->
        <!-footer start--->
        <!--Row Fluid Start-->
      <div class="row" style="background: #1F1F1F no-repeat; margin-top:5px; padding-bottom: 10px">
            <div class="col-md-4">
                <div class="link">
                    <h2>
                        Categories</h2>
                    <ul>
                        
                                <li>
                                   <a href="list.aspx?c=1" style='color:white' > Fruits & Vegetables</a> </li>
                            
                                <li>
                                   <a href="list.aspx?c=2" style='color:white' > Grocery & Staples</a> </li>
                            
                                <li>
                                   <a href="list.aspx?c=4" style='color:white' > Beverages</a> </li>
                            
                                <li>
                                   <a href="list.aspx?c=6" style='color:white' > Personal Care</a> </li>
                            
                                <li>
                                   <a href="list.aspx?c=5" style='color:white' > Branded Foods</a> </li>
                            
                                <li>
                                   <a href="list.aspx?c=7" style='color:white' > Household</a> </li>
                            
                                <li>
                                   <a href="list.aspx?c=3" style='color:white' > Bread Dairy & Eggs</a> </li>
                            
                                <li>
                                   <a href="list.aspx?c=9" style='color:white' > Meat</a> </li>
                            
                    </ul>
                </div>
            </div>
            
                    <div class="col-md-4">
                        <div class="link">
                            <h2>
                                Information</h2>
                            <ul>
                                
                                        <li>
                                         <a style="color:White" href='info.aspx?pid=5'>Terms & Conditions</a>   </li>
                                    
                                        <li>
                                         <a style="color:White" href='info.aspx?pid=6'>Privacy Policy</a>   </li>
                                    
                                        <li>
                                         <a style="color:White" href='info.aspx?pid=7'>Cancellation & Return Policy</a>   </li>
                                    
                            </ul>
                        </div>
                    </div>
                
            <div class="col-md-4">
                <div class="link">
                    <h2>
                        Contact</h2>
                    <ul>
                        <li><a href="01725005544" style="color:white "> 0172-5005544</a></li>
                        <li>Subscribe to our mailing list</li>
                        
<div id="contact_form"  style="margin:15px 0 0 30px";  >
	
    
    <li>Name:</li>
            <li>
                <input name="ctl00$ucNewsLetter$txtPersonName" type="text" id="ucNewsLetter_txtPersonName" PlaceHolder="Enter Your Name" style="color:Gray;" />
        
         <span id="ucNewsLetter_reqPersonName" style="color:Red;visibility:hidden;">*</span>
	
            
             </li>
            <li>Email:</li>
            
            <li>
             <input name="ctl00$ucNewsLetter$txtEmail" type="text" id="ucNewsLetter_txtEmail" PlaceHolder="Enter Your Email" style="color:Gray;" />
	  <span id="ucNewsLetter_reqEmailID" style="color:Red;visibility:hidden;">*</span>
  
            </li>
           
            <li>
             <input type="submit" name="ctl00$ucNewsLetter$btnSubscribe" value="Subscribe" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ucNewsLetter$btnSubscribe&quot;, &quot;&quot;, true, &quot;newsletter&quot;, &quot;&quot;, false, false))" id="ucNewsLetter_btnSubscribe" class="btn btn-success" />
            </li>
            

   
   



  
    </div>    
                    </ul>
                </div>
            </div>
        </div>
        <!--Row Fluid End-->
        <!-footer End--->
    </div>
 
</form>
</body>
<link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style.css" rel="stylesheet" />

<script language="javascript" type="text/javascript">

    function BindRightAdds()
    {
         var Location = "HomeRight";
         var Top = "1";


       $.ajax({
            type: "POST",
            data: '{ "Location":  "' + Location + '","Top":  "' + Top + '"}',
            url: "index.aspx/BindSideAdds",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
              
              $("#dvRightAdd").html(msg.d);

               


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
    
    
    
    }



function BindBottomAdds()
    {
         var Location = "Bottom";
         var Top = "2";


       $.ajax({
            type: "POST",
            data: '{ "Location":  "' + Location + '","Top":  "' + Top + '"}',
            url: "index.aspx/BindBottomAdds",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
              
              $("#dvBottomAdd").html(msg.d);

               


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
    
    
    }


    $(document).on("change", "select[name='variation']", function (event)
     {
       var pid = $(this).attr("id");
       var oldSelVal=$(this).attr("selvar");
          var arrPid = pid.split('_');
        var fPid=  arrPid[1];
       var vid = $(this).val();
 
 
     $(this).find("option[value='"+oldSelVal+"']").prop("selected",true);
        
       
       $("div[name='product_"+fPid+"']").css("display","none");
 

       $("#variation_"+vid).css("display","block");
 
//        var totalQ = $(this).find("option:selected").attr("v");
//        var htmlData="";
//        if (totalQ <= 0) {
//        htmlData="<button name='btnAddToCart' type='button' id='a_" +fPid+ "' style='padding:2px; margin-left:10px; margin-top:10px;' class='btn btn-success'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</button>" +
//                "<div class='qty'> <div align='center'><div style='margin-left:10px;background-color:#f4f2f2;width:68px;border:solid 1px silver;border-radius:4px'>" +
//                "Qty<select name='qty'  id='q_" +fPid + "' style='margin:1px 0px 1px 0px; padding:3px;border:0px'>" +
//                "<option value='1' style='font-size:12px;'>1</option>" +
//                "<option value='2' style='font-size:12px;'>2</option>" +
//                "<option value='3' style='font-size:12px;'>3</option><option value='4' style='font-size:12px;'>4</option><option value='5' style='font-size:12px;'>5</option></select></div></div></div><br/><br/>";


//        $("#dvChangable" + fPid).html(htmlData);


//            $("#box_" + fPid).css("background", "white")

//        }
//        else {

//        htmlData="<div style='margin:15px 0px 0px 20px'><div style='width:115px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;'>" +
//        "<div name='decr' style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' id='decr_" + fPid + "' >-</div><div style='width:80px;text-align:center;float:left;background:white;padding:0px 0 0 3px;' id='cqty_" + fPid + "'>" + totalQ + "</div>" +
//        "<div name='incr' style='width:10px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' id='incr_" + fPid + "' >+</div></div></div><br/><br/>"
 
//            $("#dvChangable" + arrPid[1]).html(htmlData);

//            $("#cqty_" + fPid).html(totalQ);
//            $("#box_" + fPid).css("background", "#D8EDC0");
 
//        }

       });


    $(document).on("click", "div[name='decr']", function (event) {
            var data = $(this).attr("id");
            

        var arrData = data.split('_');
        var vid = arrData[1];
      $("#cqty_"+vid).css("height","20px");
        
//           $("#cqty_"+vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");
//         
  var tempQty=$("#cqty_"+vid).html();

        var tempQ=Number(tempQty)-1;
         if (tempQ <= 1) {
            tempQ = 1;
        }
           $("#cqty_"+vid).html(tempQ);
        var qty = 1; 
        var st = "m";
        var type = "Product";
          ATC(vid, qty, st, type);
    });


    $(document).on("click", "div[name='incr']", function (event) {


        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = 1; 
        var st = "p";
        var type = "Product";
           $("#cqty_"+vid).css("height","20px");
        
        var tempQty=$("#cqty_"+vid).html();

        var tempQ=Number(tempQty)+1;
         
           $("#cqty_"+vid).html(tempQ);
         
          ATC(vid, qty, st, type);

//        var pid = $(this).attr("id");
//        var arrPid = pid.split('_');
//        var vid = $("#d_" + arrPid[1]).val();
//        var qty = $("#q_" + arrPid[1]).val();
//        var st = "p";
//        var type = "Product";
//        ATC(pid, vid, qty, st,type);


    });


    $(document).on("click", "div[name='Combodecr']", function (event) {
        var pid = $(this).attr("id");
        var arrPid = pid.split('_');
        var vid = $("#d_" + arrPid[1]).val();
        var qty = $("#qc_" + arrPid[1]).val();
        var st = "m";
        var type = "Combo";
        ATC(pid, vid, qty, st, type);
    });


    $(document).on("click", "div[name='Comboincr']", function (event) {

        var pid = $(this).attr("id");
        var arrPid = pid.split('_');
        var vid = $("#d_" + arrPid[1]).val();
        var qty = $("#qc_" + arrPid[1]).val();
        var st = "p";
        var type = "Combo";
        ATC(pid, vid, qty, st, type);


    });

    function ATC(vid, qty, st, type) {
        $.ajax({
            type: "POST",
            data: '{"vid":"' + vid + '","qty":"' + qty + '","st":"' + st + '","type":"'+ type +'"}',
            url: "index.aspx/FirstTimeATC",
            async:false,
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);

                if (obj.error == 1) {

                    alert("An Error occured during transaction. Please refresh the page and try again Later.");
                    return;
                }


                if (type == "Combo") {

                 $("#cartcontainer").html(obj.cartHTML);

                    $("#cbox_" + obj.pid).html(obj.cartHTML);


                    if (obj.qty <= 0) {

                        $("#cbox_" + obj.pid).css("background", "white")

                    }
                    else {

                        $("#cbox_" + obj.pid).css("background", "#D8EDC0")

                    }



                }
                else {


                 //$("#cartcontainer").html(obj.cartHTML);
               
           
                   $("#variation_" + vid).html(obj.productHTML);


//                    if (obj.qty <= 0) {

//                        $("#box_" + obj.pid).css("background", "white")

//                    }
//                    else {

//                        $("#box_" + obj.pid).css("background", "#D8EDC0")

//                    }

                }

             
                //$("#cqty_" + obj.pid).html(totalQ);
             

                  BindCart();
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
    
    }

    $(document).on("click", "button[name='btnAddToCart']", function (event) {

        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = $("#q_" + arrData[1]).val();
        var st = "p";
        var type = "Product";
        
      $("#a_"+vid).css("width","55px");
          $("#a_"+vid).html("<img src='images/loaderadd.gif' style='margin-top:2px'   alt='.....'/>");
         


        ATC(vid, qty, st, type);
           $.notifyBar({ cssClass: "success", html: "Product Added Succcessfully!" });

    });
    $(document).on("click", "div[name='btnView']", function (event) {
        var pid = $(this).attr("id");
        $.ajax({
            type: "POST",
            data: '{"PId":"'+pid +'"}',
            url: "index.aspx/GetComboDetail",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);
               
                
                for (var i = 0; i < obj.Cart.length; i++) {

                    var pname = "";
                    pname = obj.Cart[i]["ProductName"];
                    var PhotoUrl = "";
                    PhotoUrl = obj.Cart[i]["PhotoUrl"];
                    var Qty = "";
                    PhotoUrl = obj.Cart[i]["Qty"];
                    var Price = "";
                    PhotoUrl = obj.Cart[i]["Price"];
                    var Desc = "";
                    PhotoUrl = obj.Cart[i]["Desc"];
                    var Unit = "";
                    PhotoUrl = obj.Cart[i]["Unit"];

               

                    var tr = "<tr><td><img style='height:30px;width:30px' src='../ProductImages/" + PhotoUrl + "' /></td><td>" + pname + "</td><td>" + Desc + "</td><td>" + Unit + "</td><td>" + Qty + "/></td><td>" + Price + "</td></tr>";

                    $("#tbComboDetail").append(tr);
                }
                alert("ddd");




                


                $('#dvPopupCombo').dialog(
              {
                  autoOpen: false,

                  width: 300,
                  height: 170,
                  resizable: false,
                  modal: false,

              });
                alert("ddd1");
                linkObj = $(this);
                var dialogDiv = $('#dvPopupCombo');
                dialogDiv.dialog("option", "position", [500, 200]);
                dialogDiv.dialog('open');
            
                

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });


    });
    $(document).on("click", "button[name='btnAddToCombo']", function (event) {

        var pid = $(this).attr("id");
        var arrPid = pid.split('_');
        var vid = $("#d_" + arrPid[1]).val();
        var qty = $("#cq_" + arrPid[1]).val();
        var st = "p";
        var type = "Combo";
        ATC(pid, vid, qty, st, type);
    });

    $(document).on("click", "div[name='cartminus']", function (event) {

       var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = 1; 
        var st = "m";
        var type = "Product";
           $("#cq_"+vid).css("height","20px");
        
           $("#cq_"+vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");
         
          ATC(vid, qty, st, type);

    });

    $(document).on("click", "div[name='cartadd']", function (event) {

 

    var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = 1; 
        var st = "p";
        var type = "Product";
           $("#cq_"+vid).css("height","20px");
        
           $("#cq_"+vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");
         
          ATC(vid, qty, st, type);


    });
    $(document).on("click", "div[name='cartcombominus']", function (event) {

        var pid = $(this).attr("id");
        var arrPid = pid.split('_');
        var vid = $(this).attr("v");
        var qty = 1;
        var st = "m";
        var type = "Combo";
        ATC(pid, vid, qty, st,type);

    });

    $(document).on("click", "div[name='cartcomboadd']", function (event) {

        var pid = $(this).attr("id");
        var arrPid = pid.split('_');
        var vid = $(this).attr("v");
        var qty = 1;
        var st = "p";
        var type = "Combo";
        ATC(pid, vid, qty, st,type);




    });
    function BindCart() {
    
        $.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/GetCartHTML",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);

                $("#cartcontainer").html(obj.html);
                 
                $("#sp_msg").html("");
               
                $("#hdFreedeliveryAmt").val(obj.FDA);

               
                $("#hdMinimumCheckOutAmt").val(obj.MCOA);
                $("#subtotal").html(obj.ST);
                $("#hdTotalAmt").val(obj.ST);
                $("#delivery").html(obj.DC);
                $("#hdDeliveryCharge").val(obj.DC);
                $("#netamount").html(obj.NA);
                $("#sp_TotalItems").html(obj.TotalItems);

               
               
               
                
                if (obj.ST == 0) {

                    $("#msg").html("Cart is Empty");
                    $("#subtotal").html("");
                    $("#delivery").html("");
                    $("#netamount").html("");
                    $("#sp1").html("");
                    $("#sp2").html("");
                    $("#sp3").html("");
                    $("#btnCheckOut").hide();
                }
                else {
                    $("#msg").html("");
                    $("#sp1").html("Sub Total: Rs.");
                    $("#sp2").html("Delivery Charges: Rs.");
                    $("#sp3").html("Total: Rs.");
                        $("#btnCheckOut").show();
                      
                        if (Number($("#hdTotalAmt").val()) >= Number($("#hdFreedeliveryAmt").val())) {
                  
                    $("#sp2").html("");
                    $("#delivery").html("");
                    $("#netamount").html($("#hdTotalAmt").val());
                }
                }
                

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {
               
            }

        });
    }



    function BindCustomGroups()
    {
       $.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/BindGroups",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
              
              $("#myGroups").html(msg.d);

               


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
    
    
    }
 
    function BindCities() {
        $.ajax({
            type: "POST",
            data: '',
            url: "index.aspx/BindCities",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#cnMaster_ddlCity").html(obj.CityList);


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {
              

                $.uiUnlock();
              
            }

        });
    }
    function BindPincodes(CityId) {

    $("#cnMaster_ddlPincode").html("<option>Please wait while loading..</option>");
        $.ajax({
            type: "POST",
            data: '{"CityId":' + CityId + '}',
            url: "index.aspx/BindPincodes",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#cnMaster_ddlPincode").html(obj.PinCodeList);


             }, error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function (msg) {
         
                 $.uiUnlock();
                  
             }

         });
     }
     function CheckAddress()
     {$.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/CheckAddress",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
              

              if(msg.d=="0")
              {
                 $("#dvPopupCheck").dialog({ modal: true, closeOnEscape: false, width: 400 });
                $(".ui-dialog-titlebar").hide();
              }

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
     }

    $(document).ready(
    function () {

    BindCustomGroups();
    

    BindBottomAdds();
    BindRightAdds();

    CheckAddress(); 


//    $("#zipcheck").click(function () {
//        var city = $("#cnMaster_ddlCity option:selected").text();
//        var pincode = $("#cnMaster_ddlPincode option:selected").text();
//      
//        if (city == "") {
//            $("#cnMaster_ddlCity").focus();
//            return;
//        }
//        if (pincode == "" || pincode == "Please wait while loading..") {
//            $("#cnMaster_ddlPincode").focus();
//            return;
//        }
//        var dialogDiv = $('#dvPopupCheck');
//        dialogDiv.dialog("option", "position", [500, 200]);
//        dialogDiv.dialog('close');
//    });
    
    $("#zipcheck").click(function () {
        var city = $("#cnMaster_ddlCity option:selected").text();
        var pincode = $("#cnMaster_ddlPincode option:selected").text();
      
        if (city == "") {
            $("#cnMaster_ddlCity").focus();
            return;
        }
        if (pincode == "" || pincode == "Please wait while loading..") {
            $("#cnMaster_ddlPincode").focus();
            return;
        }
         var Id = $("#cnMaster_ddlPincode").val();
        $.ajax({
            type: "POST",
            data: '{"Id":"'+ Id +'"}',
            url: "index.aspx/RestoreAddress",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
             $("#sp_SelectedCity").html(msg.d);

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {
              var dialogDiv = $('#dvPopupCheck');
        dialogDiv.dialog("option", "position", [500, 200]);
        dialogDiv.dialog('close');

            }

        });
      
    });
   

    BindCities();
    
    $("#cnMaster_ddlCity").change(function () {
  

        var CityId = $("#cnMaster_ddlCity").val();
        if(CityId=="")
        {
         $("#cnMaster_ddlPincode").html("");
        return;
        }
        BindPincodes(CityId);
    });

       
       
       
        $("#dvMenu").css("display", "block");
            
       
       
        $("#featuredCategories").html("<img id='imgloading' src='images/ajax-loader.gif' style='margin-top:200px;margin-left:300px' alt='loading please wait..'/>");

 


        BindCart();
       


        $.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/GetFeaturedCategoriesAndProductsList",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                $("#imgloading").remove();

                $("#featuredCategories").html(msg.d);



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });


        $.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/GetFeaturedComboTypesAndCombos",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                $("#imgloading").remove();

                $("#featuredComboTypes").append(msg.d);



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
        $("#closepopup").click(
            function () {
                var dialogDiv = $('#dvPopupProduct');
                dialogDiv.dialog("option", "position", [500, 200]);
                dialogDiv.dialog('close');
                return false;
            });
        $("#btnCheckOut").click(
function () {
    var totalAmt = Number($("#hdTotalAmt").val());
           var minAmt = Number($("#hdMinimumCheckOutAmt").val());
           if (totalAmt < minAmt) {
               $("#sp_msg").html("* Order Amount should be greater than " + minAmt);
               return false;
           }
           else {



              $.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/ValidateCheckOut",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
              

              if(msg.d=="1")
              {
                  var url = "basket.aspx";
               $(location).attr('href', url);
              }
              else
              {
                $("#sp_msg").html("* Insufficient Order Amount for Checkout");
              }



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });





           
           }
       });
      }
    );


 
</script>




     <style>
                /* jssor slider bullet navigator skin 05 css */
                /*
                .jssorb05 div           (normal)
                .jssorb05 div:hover     (normal mouseover)
                .jssorb05 .av           (active)
                .jssorb05 .av:hover     (active mouseover)
                .jssorb05 .dn           (mousedown)
                */
                .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
                    background: url(img/b05.png) no-repeat;
                    overflow: hidden;
                    cursor: pointer;
                }

                .jssorb05 div {
                    background-position: -7px -7px;
                }

                    .jssorb05 div:hover, .jssorb05 .av:hover {
                        background-position: -37px -7px;
                    }

                .jssorb05 .av {
                    background-position: -67px -7px;
                }

                .jssorb05 .dn, .jssorb05 .dn:hover {
                    background-position: -97px -7px;
                }
            </style>
            <style>
                /* jssor slider arrow navigator skin 11 css */
                /*
                .jssora11l              (normal)
                .jssora11r              (normal)
                .jssora11l:hover        (normal mouseover)
                .jssora11r:hover        (normal mouseover)
                .jssora11ldn            (mousedown)
                .jssora11rdn            (mousedown)
                */
                .jssora11l, .jssora11r, .jssora11ldn, .jssora11rdn {
                    position: absolute;
                    cursor: pointer;
                    display: block;
                    background: url(img/a11.png) no-repeat;
                    overflow: hidden;
                }

                .jssora11l {
                    background-position: -11px -41px;
                }

                .jssora11r {
                    background-position: -71px -41px;
                }

                .jssora11l:hover {
                    background-position: -131px -41px;
                }

                .jssora11r:hover {
                    background-position: -191px -41px;
                }

                .jssora11ldn {
                    background-position: -251px -41px;
                }

                .jssora11rdn {
                    background-position: -311px -41px;
                }
            </style>

 <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
<script>


        jQuery(document).ready(function ($) {
         





               
            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 6000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseOutQuint,          //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration: 800,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Scale: false,                                  //Scales bullets navigator or not while slider scale
                },

                $BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 12,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 4,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1,                                //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                    $Scale: false,                                  //Scales bullets navigator or not while slider scale
                }
            };

            //Make the element 'slider1_container' visible before initialize jssor slider.
            $("#slider2_container").css("display", "block");
            var jssor_slider1 = new $JssorSlider$("slider2_container", options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth) {
                    jssor_slider1.$ScaleWidth(parentWidth - 30);
                }
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });

</script>

<script src="js/bootstrap.min.js" type="text/javascript"></script>
<link href="css/sm-core-css.css" rel="stylesheet" type="text/css" />

<link href="css/sm-mint/sm-mint.css" rel="stylesheet" type="text/css" />


 
<script type="text/javascript" src="jquery.smartmenus.js"></script>
 
<script type="text/javascript">
    $(function () {
        $('#main-menu').smartmenus({
            mainMenuSubOffsetX: -3,
            mainMenuSubOffsetY: -8,
            subMenusSubOffsetX: 1,
            subMenusSubOffsetY: -8,
            subMenusMinWidth: 700

        });

    });
</script>


 <script type="text/javascript">


     $("#main-menu").css("display", "block");
     //
     // $('#element').donetyping(callback[, timeout=1000])
     // Fires callback when a user has finished typing. This is determined by the time elapsed
     // since the last keystroke and timeout parameter or the blur event--whichever comes first.
     //   @callback: function to be called when even triggers
     //   @timeout:  (default=1000) timeout, in ms, to to wait before triggering event if not
     //              caused by blur.
     // Requires jQuery 1.7+
     //
     ; (function ($) {
         $.fn.extend({
             donetyping: function (callback, timeout) {
                 timeout = timeout || 1e3; // 1 second default timeout
                 var timeoutReference,
                doneTyping = function (el) {
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
                 return this.each(function (i, el) {
                     var $el = $(el);
                     // Chrome Fix (Use keyup over keypress to detect backspace)
                     // thank you @palerdot
                     $el.is(':input') && $el.on('keyup keypress', function (e) {
                         // This catches the backspace button in chrome, but also prevents
                         // the event from triggering too premptively. Without this line,
                         // using tab/shift+tab will make the focused element fire the callback.
                         if (e.type == 'keyup' && e.keyCode != 8) return;

                         // Check if timeout has been set. If it has, "reset" the clock and
                         // start over again.
                         if (timeoutReference) clearTimeout(timeoutReference);
                         timeoutReference = setTimeout(function () {
                             // if we made it here, our timeout has elapsed. Fire the
                             // callback
                             doneTyping(el);
                         }, timeout);
                     }).on('blur', function () {
                         // If we can, fire the event since we're leaving the field
                         doneTyping(el);
                     });
                 });
             }
         });
     })(jQuery);

     $('#txtSearch').donetyping(function () {
         // $('#example-output').text('Event last fired @ ' + (new Date().toUTCString()));


         var Keyword = $(this).val();

         if (Keyword.length >= 3) {

             $("#result").css("display", "block");
             $("#tbKeywordSearch").html("<img src='images/searchloader.gif'>");

             $.ajax({
                 type: "POST",
                 data: '{"Keyword":"' + Keyword + '"}',
                 url: "productsearch.aspx/KeywordSearch",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {
                     // var obj = jQuery.parseJSON(msg.d);


                     var Content = msg.d;
                     $("#tbKeywordSearch").html(Content);
                     if (Content.length < 10) {

                         $("#tbKeywordSearch").html('No record found for the Search');
                     }







                 }, error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function (msg) {


                 }

             });














         }
         else {


             $("#result").css("display", "none");


         }



     });
    </script>

 

 
<style type="text/css">
	#main-menu {
		position:relative;
		z-index:9999;
		width:100%;
		margin-top:5px;
		background:black;
	}
	#main-menu ul {
		width:12em;  
	}
</style>

 <script language="javascript" type="text/javascript">
     $(document).ready(
    function () {

        $.ajax({
            type: "POST",
            data: '{}',
            url: "index.aspx/GetAddress",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                if (obj.Status == 0) {
                    $("#dvPopupCheck").dialog({ modal: true, closeOnEscape: false, width: 400 });
                    $(".ui-dialog-titlebar").hide();
                    return;
                }

                $("#sp_SelectedCity").html(obj.SAddress);



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });

        $("#sp_SelectedCity").click(function () {
            $("#dvPopupCheck").dialog({ modal: true, closeOnEscape: false, width: 400 });
            $(".ui-dialog-titlebar").hide();
        });

        $(document).keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '27') {
                var dialogDiv1 = $('#dvPopupDSlots');
                dialogDiv1.dialog("option", "position", [500, 200]);
                dialogDiv1.dialog('close');
            }

        });
        $("#dvclose").click(function () {

            var dialogDiv1 = $('#dvPopupDSlots');
            dialogDiv1.dialog("option", "position", [500, 200]);
            dialogDiv1.dialog('close');
        });
        $("#dvShowSlots").click(function () {

            $("#sp_TimeSlots").html("Please wait while loading..");

            $.ajax({
                type: "POST",
                data: '{}',
                url: "index.aspx/GetTimeSlots",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    $("#sp_TimeSlots").html(obj.TSlots);



                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {


                }

            });
            $("#dvPopupDSlots").dialog({ modal: true, closeOnEscape: false, width: 400 });
            $(".ui-dialog-titlebar").hide();
        });

    });
</script>       

<script language="javascript" type="text/javascript">

    $(document).ready(

    function () {


        $("#lnkHome").mouseover(
        function () {

            $('#main-menu').smartmenus('enable');

            $("#dvMenuCategories").show();
        }
        );

        $("#cssmenu").css("width", "100%");

        $("#lnkHome").mouseleave(
                function () {
                    //  $('#main-menu').smartmenus('disable');
                    $("#dvMenuCategories").hide();
                }
                );

        $("#dvMenuCategories").mouseover(
                function () {


                    $('#main-menu').smartmenus('enable');
                    $("#dvMenuCategories").show();
                }
                );

        $("#dvMenuCategories").mouseleave(
                function () {

                    $('#main-menu').smartmenus('disable');
                    $(this).hide();
                }
                );

    }
    );


</script>

</html>
