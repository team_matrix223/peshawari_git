﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class contactus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.PageTitle = "Contact Us";
        Int32 totitems = new CartBLL().GetTotalItems(HttpContext.Current.Session.SessionID);
        Master.TotalItems = totitems;
    }
    public void ResetControls()
    {
        txtEmail.Value = "";
        txtEnquiry.Value = "";
        txtFirstName.Value = "";

        txtPhoneNo.Value = "";
        txtSubject.Value = "";
     

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        ContactUs objcon = new ContactUs()
        {
            ContactId = 0,
            FirstName = txtFirstName.Value,
            LastName = "",
            Email = txtEmail.Value,
            Phoneno = txtPhoneNo.Value,
            Subject = txtSubject.Value,
            Enquiry = txtEnquiry.Value,
            AdminId = 0,
            Sdate = DateTime.Now,
            ParentId = 0
        };
        Int16 retval = new ContactUsBLL().Insert(objcon);
        ResetControls();
       // Response.Write("<script>alert('Information saved successfully');</script>");
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Information saved successfully');", true);
    }
}