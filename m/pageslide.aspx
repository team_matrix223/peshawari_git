﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pageslide.aspx.cs" Inherits="pageslide" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>jQuery PageSlide Demo</title>
<!--  The jquery.pageslide.css stylesheet has the class that you'll need on your page -->
<link rel="stylesheet" type="text/css" href="jquery.pageslide.css">
<style>
body {
background: #f4f4f4;
font: 14px/18px "HelveticaNeue", "Helvetica Neue", Helvetica, Arial, sans-serif;
color: #666;
-webkit-font-smoothing: antialiased; /* Fix for webkit rendering */
-webkit-text-size-adjust: none;
}
a, a:visited {
color: #C30;
text-decoration: none;
border-bottom: 1px dotted #C30;
}
a:hover {
color: #900;
border-bottom-style: solid;
}
p {
margin-bottom: 20px;
}
#content {
width: 940px;
padding: 10px;
margin: 0 auto;
}
#content h1 {
color: #333;
line-height: 1em;
}
#content ul {
padding-left: 0;
list-style: none;
}
#content ul li {
margin-bottom: 20px;
}
#modal {
display: none;
}
#modal a {
background: #CCC;
color: #333;
font-weight: bold;
padding: 5px 10px;
border: none;
}
#modal a:hover {
background: #aaa;
}
pre {
border: 1px solid #CCC;
background-color: #EEE;
color: #333;
padding: 10px 20px;
}
</style>
 </head>
<body style="margin-left: 8px;">
<div id="content">
<h1>PageSlide Basic Demo</h1>
<p>Below are a couple examples of how the plugin works:</p>
<ul>
<li> <a href="_secondary.html" class="first">Slide to the right, and load content from a secondary page.</a>
<pre>&lt;a href="_secondary.html" class="first"&gt;Link text&lt;/a&gt;
&lt;script&gt;
    $("a.first").pageslide();
&lt;/script&gt;
                </pre>
</li>
<li> <a href="#modal" class="second">Slide to the left, and display hidden content from this page in a modal pane.</a>
<pre>&lt;a href="#modal" class="second"&gt;Link text&lt;/a&gt;
&lt;div id="modal" style="display:none"&gt;
    &lt;h2&gt;Modal&lt;/h2&gt;
    &lt;a href="javascript:$.pageslide.close()"&gt;Close&lt;/a&gt;
&lt;/div&gt;
&lt;script&gt;
    $(".second").pageslide({ direction: "left", modal: true });
&lt;/script&gt;
                </pre>
</li>
<li> <a href="javascript:$.pageslide({ direction: 'left', href: '_secondary.html' })">Open the page programatically.</a>
<pre>&lt;a href="javascript:$.pageslide({ direction: 'left', href='_secondary.html' })"&gt;Link text&lt;/a&gt;
                </pre>
</li>
</ul>
<div id="modal">
<h2>Modal</h2>
<p>This slide uses "modal" option set to "true". When using a modal pageslide, clicking on the main window will not close the window. You must explicitly call <code>$.pageslide.close()</code>.</p>
<p><a href="javascript:$.pageslide.close()">Close</a></p>
</div>
</div>
<script src="http://www.google-analytics.com/ga.js" async="" type="text/javascript"></script><script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="jquery.pageslide.min.js"></script><div style="left: -300px; right: auto; display: none;" id="pageslide"><iframe style="width: 100%; height: 100%;" hspace="0" src="_secondary.html" frameborder="0"></iframe></div> 
<script>
    /* Default pageslide, moves to the right */
    $(".first").pageslide();

    /* Slide to the left, and make it model (you'll have to call $.pageslide.close() to close) */
    $(".second").pageslide({ direction: "left", modal: true });
    </script>
 


</body>
</html>