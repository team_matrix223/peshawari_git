﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class productdetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnProductId .Value = Request.QueryString["p"] != null ? Request.QueryString["p"] : "0";
            hdnVariationId.Value = Request.QueryString["v"] != null ? Request.QueryString["v"] : "0";
        }
    }
    [WebMethod]
    public static string GetData(int ProductId,int VariationId)
    {
        string Url="";
        string name = "";
        int CartCnt = 0;
        string  SessionId = HttpContext.Current.Session.SessionID;
        new ProductsBLL().GetProductDetailByProductIdZoom(SessionId, ProductId, VariationId, out Url, out name, out CartCnt);

        var JsonData = new
        {
            PhotoUrl = Url,
            Name = name ,
            CartCount = CartCnt
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
 
}