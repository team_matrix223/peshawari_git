﻿using System;
using System.Collections.Generic;
using System.Web;

public class Category
{
    public int CategoryId { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public int ParentId { get; set; }
    public int Level { get; set; }
    public bool IsActive { get; set; }
    public int AdminId { get; set; }
    public DateTime DOC { get; set; }
    public bool ShowOnHome { get; set; }
    public int DisplayOrder { get; set; }
    public int FeaturedOrder { get; set; }
	public Category()
	{
        CategoryId = 0;
        Title = string.Empty;
        IsActive = false;
        AdminId = 0;
        DOC = DateTime.Now;
        Description = string.Empty;
        ParentId = 0;
        Level = 0;
        ShowOnHome = false;
        DisplayOrder = 0;
        FeaturedOrder = 0;
      
	}
}