﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="groups.aspx.cs" Inherits="groups" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cnMaster" Runat="Server">
    <script src="backoffice/js/jquery-1.9.0.min.js"></script>

    <div class="row">
  <div style="" class="col-md-12">
<div style="margin:auto;padding:auto;width:100%" id="Outer">
  <div id="dvImage">
  <asp:Image ID="imgCategory" runat="server" Width="100%" />
   
  </div>
  <div style="margin-left:5px">
  
 <asp:Literal ID="ltCategoriesHTML" runat="server"></asp:Literal>
 </div>

</div>
  
</div>
</div>
 <div class="row">
<asp:Literal ID="ltSubGroups" runat="server"></asp:Literal>
 </div>
</asp:Content>

