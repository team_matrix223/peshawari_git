﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="pictureorder.aspx.cs" Inherits="pictureorder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cnMaster" Runat="Server">

        <link rel="shortcut icon" type="image/png" href="img/pslogo.png">
    <script src="jquery-1.8.3.min.js"></script>
      <link rel="stylesheet" href="css/jquery.notifyBar.css">
    
    <link href="jquery-ui.min.css" rel="stylesheet" />
    <script src="jquery.notifyBar.js"></script>
    <script src="jquery-ui.min.js"></script>
     <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
<script type="text/javascript">

    function BindDeliveryAddress() {

       
        $.ajax({
            type: "POST",
            data: '{}',
            url: "pictureorder.aspx/BindDeliveryAddress",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                $("#dvDeliveryAddresses").html(msg.d);




            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });

        $.ajax({
            type: "POST",
            data: '{}',
            url: "pictureorder.aspx/BindDays",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);
               
                $("#<%=ddlDays.ClientID %>").html(obj.ddd);




            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });





    }


    function SavePictureOrder() {

        var fd = new FormData();

                var count = document.getElementById('fileToUpload').files.length;

                for (var index = 0; index < count; index++) {

                    var file = document.getElementById('fileToUpload').files[index];

                    fd.append(file.name, file);


                }

             

                var xhr = new XMLHttpRequest();

                xhr.upload.addEventListener("progress", uploadProgress, false);

                xhr.addEventListener("load", uploadComplete, false);

                xhr.addEventListener("error", uploadFailed, false);

                xhr.addEventListener("abort", uploadCanceled, false);

                xhr.open("POST", "savetofile.aspx");

                xhr.send(fd);
       
       
       
       


    }





    $(document).ready(

      function () {



          $("#btnOk").click(
    function () {

        SavePictureOrder();
    }
    );

          $("#dvCancelOrder").click(
          function () {

              $("#dvQlDeliveryAddress").dialog("close");

          }

          );

          $("#btnlogin").click(
    function () {

        var MobileNo = $("#txtName").val();
        if (MobileNo == "") {
            alert("Enter Username");
            $("#txtName").focus();
            return;
        }
        var Password = $("#txtPassword").val();
        if (Password == "") {
            alert("Enter Password");
            $("#txtPassword").focus();
            return;
        }

        $.ajax({
            type: "POST",
            data: '{"MobileNo": "' + MobileNo + '","Password": "' + Password + '"}',

            url: "pictureorder.aspx/Login",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == "-1") {
                    alert("Invalid User Name.");
                    $("#txtName").focus();
                    return;
                }
                else if (obj.Status == "-2") {
                    alert("Invalid Password");
                    $("#txtPassword").focus();
                    return;
                }
                else if (obj.Status == "0") {
                    alert("First Register,Then SignIn");
                    return;
                }
                else {
                    var dialogDiv = $('#inner');
                    dialogDiv.dialog("option", "position", [500, 200]);
                    dialogDiv.dialog('close');


                    BindDeliveryAddress();
                    $("#dvQlDeliveryAddress").dialog({ modal: true, closeOnEscape: true, width: 350 });
                    $(".ui-dialog-titlebar").hide();

                }




            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {


                $.uiUnlock();
            }


        });



    }
    );



          $("#<%=ddlDays.ClientID%>").change(function () {

              //     var slctday =  $("#<%=ddlDays.ClientID%> option:selected").text();
              var slctday = $("#<%=ddlDays.ClientID%>").val();

              $.ajax({
                  type: "POST",
                  data: '{"SelectedDay": "' + slctday + '"}',
                  url: "pictureorder.aspx/BindSlots",
                  contentType: "application/json",
                  dataType: "json",
                  success: function (msg) {
                      var obj = jQuery.parseJSON(msg.d);

                      $("#<%=ddlTimeSlot.ClientID %>").html(obj.Slots);




                  }, error: function (xhr, ajaxOptions, thrownError) {

                      var obj = jQuery.parseJSON(xhr.responseText);
                      alert(obj.Message);
                  },
                  complete: function (msg) {


                  }

              });


          });


          $("#btnclose").click(
    function () {
        var dialogDiv = $('#inner');
        dialogDiv.dialog("option", "position", [500, 200]);
        dialogDiv.dialog('close');
    }
    );




      }

      );


    function fileSelected() {

        var count = document.getElementById('fileToUpload').files.length;

        document.getElementById('details').innerHTML = "";

        for (var index = 0; index < count; index++) {

            var file = document.getElementById('fileToUpload').files[index];

            var fileSize = 0;

            if (file.size > 1024 * 1024)

                fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';

            else

                fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';

            document.getElementById('details').innerHTML += 'Name: ' + file.name + '<br>Size: ' + fileSize + '<br>Type: ' + file.type;

            document.getElementById('details').innerHTML += '<p>';

        }

    }

    function uploadFile() {


        var count = document.getElementById('fileToUpload').files.length;
        if (count == 0) {
            alert("Please first upload a file");
            return;
        }
        $.ajax({
            type: "POST",
            data: '{}',
            url: "pictureorder.aspx/CheckUser",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                if (msg.d == "0") {

                    $("#inner").dialog({ modal: true, closeOnEscape: false, width: 399 });
                    $(".ui-dialog-titlebar").hide();
                }
                else {


                    BindDeliveryAddress();
                    $("#dvQlDeliveryAddress").dialog({ modal: true, closeOnEscape: false, width: 399 });
                    $(".ui-dialog-titlebar").hide();

                }

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });
  

     

    }

    function uploadProgress(evt) {

        if (evt.lengthComputable) {

            var percentComplete = Math.round(evt.loaded * 100 / evt.total);

            document.getElementById('progress').innerHTML ="Uploading Image: "+ percentComplete.toString() + '%';

        }

        else {

            document.getElementById('progress').innerHTML = 'unable to compute';

        }

    }

    function uploadComplete(evt) {

        /* This event is raised when the server send back a response */
        var FileName = evt.target.responseText;
        if (FileName == "") {
            alert("First Choose Picture");
            return;
        }
        var delval = $("input[name='delivery']:checked").val();
        if (delval == "") {
            alert("Choose Delivery Address");
            return;
        }
        var DeliveryDay = $("#<%=ddlDays.ClientID%> option:selected").text();

        if (DeliveryDay == "") {
            alert("Choose Delivery Day");
            $("#<%=ddlDays.ClientID%>").focus();
            return;
        }
        var DeliverySlot = $("#<%=ddlTimeSlot.ClientID%>").val();
        if (DeliverySlot == "") {
            alert("Choose Delivery Slot");
            $("#<%=ddlTimeSlot.ClientID%>").focus();
            return;
        }


 

        var PhotoUrl = FileName;

        $.ajax({
            type: "POST",
            data: '{"PhotoUrl": "' + PhotoUrl + '", "DeliveryAddress": "' + delval + '", "DeliveryDay": "' + DeliveryDay + '", "DeliverySlot": "' + DeliverySlot + '"}',
            url: "pictureorder.aspx/InsertPictureOrder",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
              
                if (obj.Picture == "-2") {
                    alert("Delivery Address does not belong to this user");
                    return;
                }
                else {
                    alert("Order Placed Successfully. Our Technical team will consult you soon");
                    $("#dvQlDeliveryAddress").dialog("close");
                }



            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                $.uiUnlock();
            }

        });

         
    }

    function uploadFailed(evt) {

        alert("There was an error attempting to upload the file.");

    }

    function uploadCanceled(evt) {

        alert("The upload has been canceled by the user or the browser dropped the connection.");

    }
 
    </script>

<div class="row">
            
            
            <div class="col-md-12">
  					
              <h2 style="border-bottom: 1px dashed silver; border-top: 1px dashed silver; 
                  color: gray; padding: 7px; font-size: 20px; margin-top: 10px;
                   background: none repeat scroll 0% 0% rgb(238, 238, 238); 
                   font-family: serif;">PESHAWARI PICTURE ORDER </span></h2> 
  					</div>
            
        </div>

        <div class="row">
        <div class="col-md-12">

        <form id="form1" enctype="multipart/form-data" method="get" action="savetofile.aspx">
        <table width="100%">
        <tr>
        <td style="font-weight:bold;font-style:italic;padding:10px" colspan="100%">Click Photo from Your Mobile or Camera and Send photo of Hand written Order</td>
        </tr>
        <tr>
        <td style="padding:10px">
   <table>
   <tr>
       <td><b>Choose Photo:</b></td> <td>
      <input type="file" name="fileToUpload" id="fileToUpload" onchange="fileSelected();" accept="image/*" capture="camera" />
 </td>
 
 <td></td>   
   </tr>
   <tr>
   <td></td>
   <td style="padding-top:5px">  <input type="button" onclick="uploadFile()" value="Upload Photo" /></td>
   </tr>
   <tr>
   <td colspan="100%"></td>
   </tr>

   <tr>
   <td></td>
   <td style="padding-top:5px">
    <table>
        <tr><td> </td><td>    
            <div id="details" style="display:none"></div>
 
    
        </td></tr>
        </table>
   
   </td>
   </tr>
   
   </table>     
        
        </td>



    
        </tr>

        <tr>
        <td>
        
        <input type="hidden" id="hdnDeliveryId" name="hdnDeliveryId" value="43" />
        
        </td>
        <td> 
       
        
       </td>
       
        </tr>
        </table>
 
 

 
    
 
 

 
  </form>
 
        
        </div>
        
        </div>



         <div class="row">


    <div class="col-md-12">
                  <div id ="inner" class="formy well" style="z-index: 1;display:none;border:solid 2px silver;background-color:#f5f5f5">
                     <h3  style="font-weight:bold;padding-bottom:20px;padding-left:20px" class="title">Login to Your Account</h3>
                                  <div class="form">
                                

                                      <!-- Login  form (not working)-->
                                                                       
                                          <!-- Username -->
                                         <table><tr><td>
                                           <label for="txtName" class="control-label col-md-3">Username</label>
                                         </td>
                                         <td>
                                                <input id="txtName" type ="text" style="margin-bottom:10px" class = "form-control" onkeypress="return isNumberKey(event)" />
                                         </td></tr><tr>
                                         <td>
                                            <label for="txtPassword" class="control-label col-md-3">Password</label>
                                         </td>
                                         <td>   <input type="password" id = "txtPassword" style="margin-bottom:10px" class="form-control" />
                                         </td></tr>
                                         <tr><td></td><td colspan="100%"><table><tr>
                                         <td style="padding-right:15px">
                                           <button id="btnlogin" class="btn btn-success" style ="background-color:#5cb85c;border-color: #398439;"  title="Check"   name="Login" type="button"><span>Login</span></button>
</td><td >
	<button id = "btnclose" class="btn btn-danger"  type="reset">Close</button>
</td></tr></table></td></tr>
<tr><td>    <a href="user/account.aspx" style="color:Blue;">New User Register Here</a>
</td><td></td></tr></table>

                                       
                                      

  
                                    </div> 
                                  </div>

                </div>

                

                </div>



                

<div class="row" >


<div class="col-sm-9" id = "dvQlDeliveryAddress" style="display:none">
<div class="row" style="margin-top:15px;margin-bottom:15px">

<div class="col-md-12" >
<div class="row">
<div class="col-xs-12"  >
 
</div>

</div>

<div  class="col-md-12" style=" border-radius: 1px; border:solid 1px silver ;padding:5px 5px 5px">
 
 <div class="row">
 <div class="col-md-12">
<span style="color:#58595b;font-size:16px;font-weight:bold;">DELIVERY INFORMATION</span>
 <hr  style="margin:2px;"/>

 <div id="dvDeliveryAddresses" >
 
  <asp:Literal ID="ltDeliveryAddress" runat="server"></asp:Literal>
 </div>

 </div>
 
 </div>

 


</div>
</div>

 

 


</div>
   

<div class="row" style="margin-top:15px;margin-bottom:15px">

<div class="col-md-12" >
<div class="row">
<div class="col-xs-12"  >
 
</div>

</div>

<div  class="col-md-12" style=" border-radius: 1px; border:solid 1px silver ; padding:5px 5px 5px">
 
 <div class="row">
 <div class="col-md-12">
 <asp:TextBox ID="txtDeliveryAddressId" runat="server" Width="0" Height="0" style="border:0px"></asp:TextBox>
<span style="color:#58595b;font-size:16px;font-weight:bold;">CHOOSE DELIVERY SLOT</span>
 <hr  style="margin:2px;"/><br />
 <asp:UpdatePanel ID="upd1" runat="server">
      
                   
 <ContentTemplate>

 <div class="row">
 <div class="col-md-6"><b>Delivery Day:</b></div>
 <div class="col-md-6"> <asp:DropDownList ID="ddlDays" runat="server" CssClass="form-control"  Width="100%" 
           ></asp:DropDownList></td><td style="padding-left:10px">
          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ForeColor="Red" 
 ControlToValidate="ddlDays"></asp:RequiredFieldValidator></div>
 <div class="col-md-6"><b>Time Slot:</b></div>
 <div class="col-md-6">   <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="upd1" runat="server">
                        <ProgressTemplate>
                            <div id="dvProgress" style="padding-top:5px;position: absolute; width: 200px; height: 35px; background-color: Black;
                                color: White; text-align: center; opacity: 0.7; vertical-align: middle">
                                loading please wait..
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
       

 <asp:DropDownList ID="ddlTimeSlot" runat="server"  CssClass="form-control" Width="100%"></asp:DropDownList>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ForeColor="Red" 
 ControlToValidate="ddlTimeSlot"></asp:RequiredFieldValidator>
 </div>
 <div class="col-md-6"><b>Payment Mode:</b></div>
 <div class="col-md-6">
 CASH ON DELIVERY
<%-- <asp:DropDownList ID="ddlPaymentMode" runat="server" CssClass="form-control"  Width="100%">
 <asp:ListItem Text=""></asp:ListItem>
 <asp:ListItem Text="Cash On Delivery" Value="Cash On Delivery"></asp:ListItem>
 </asp:DropDownList>--%>
<%--
  <asp:RequiredFieldValidator ID="reqPaymentMode" runat="server" ErrorMessage="*" 
 ForeColor="Red" ControlToValidate="ddlPaymentMode"></asp:RequiredFieldValidator>--%>
 </div>
 
 </div>
 <table>
 <tr>
   <td colspan="100%" style="padding-left:45px;padding-top:10px" >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnOk"  class="btn btn-primary btn-small" >OK</div></td>
                                           <td style="padding-left:5px"><div id="dvCancelOrder"  class="btn btn-primary btn-small" >Cancel</div></td>
                                           <td><div id="progress"  ></div></td>
                                            </tr>
                                            </table>
                                            </td>
 </tr>
 
 </table>

  
 </ContentTemplate>
 </asp:UpdatePanel>
 
 
 

  

 </div>
 
 </div>

 


</div>
</div>

 

 


</div>



</div>
 



</div>
</asp:Content>

