﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="OrderSuccess.aspx.cs" Inherits="OrderSuccess" %>

<%@ Register Src="~/usercontrols/ucMenu.ascx" TagPrefix="uc1" TagName="ucMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnMaster" Runat="Server">
      <%--<script src="jquery-1.4.4.min.js" type="text/javascript"></script>--%>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="smartpaginator.js" type="text/javascript"></script>
    <link href="smartpaginator.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.uilocksearch.js" type="text/javascript"></script>
    <script src="backoffice/js/customValidation.js" type="text/javascript"></script>


     <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <%--<script src="//code.jquery.com/jquery-1.10.2.js"></script>--%>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="css/jquery.notifyBar.css">
   <script src="jquery.notifyBar.js"></script>

    
         <link rel="stylesheet" href="styles.css">
   <%--<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>--%>
   <script src="script.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#txtSearch").remove();
            $("#btnSearch").remove();
        });
        </script>


         <script language="javascript" type="text/javascript">



             $(document).ready(
       function () {

           var m_ReviewId = 0;

           function ResetControls() {

               $("#txtTitle").val("");
               $("#txtTitle").focus();
               $("#txtDescription").val("");

               $("#<%=ddlRate.ClientID %>").val("");
           }

           $("#btnCancel").click(
           function () {
               ResetControls();
           });


           $("#btnAdd").click(
           function () {


               InsertUpdate();


           });




           function InsertUpdate() {


               if (!validateForm("frmMain")) {
                   return;
               }

               var Title = $("#txtTitle").val();
               if ($.trim(Title) == "") {
                   $("#txtTitle").focus();

                   return;
               }

               var Description = $("#txtDescription").val();
               if ($.trim(Description) == "") {
                   $("#Description").focus();

                   return;
               }


               var Rating = $("#<%=ddlRate.ClientID %>").val();

               $.ajax({
                   type: "POST",
                   data: '{ "Title": "' + Title + '","Description": "' + Description + '","Rating": "' + Rating + '"}',
                   url: "OrderSuccess.aspx/Insert",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);

                       if (obj.Status == 0) {

                           alert("Error Occured during Transaction. Try again Later.");
                           return;
                       }

                       if (m_ReviewId == "0") {

                           alert("Thanks for your Valuable Reviews with Peshawari.");
                           ResetControls();
                       }


                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {

                   }
               });

           }

       });
     
      

 </script>


    <div class="row" style="margin-top:15px;margin-bottom:15px">

<div class="col-md-12" >
<div class="row">
<div class="col-xs-12"  >
 
</div>

</div>
    <table>
        <tr><td colspan="100%">

        <div style="color:darkgreen;padding-left:20px;padding-bottom:30px"><h1>Your Order Has Been Processed!</h1></div>     
<div  class="col-md-12" style=" border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px;padding-left:20px;width:650px;">
     <table>
   <tr>
       <td style="padding-top:10px;border-right:1px solid;border-color:lightgray;padding-left:10px;padding-right:10px;vertical-align:top">
           <table><tr><td>
   <h3 style="padding-left:10px;color:darkgray "> <b>Your Order Number Is: <asp:Literal ID="ltOrderNumber" runat="server"></asp:Literal></b></h3>
       <br />
           <span style="padding-left:10px">Thanks for Visiting Peshawari Online Store.<br />
              Your Order is In Process. Our Staff will contact you soon.
              <br />
              For any Enquery please contact us at 9041781535
              or mail us at peshawarisupermarket1957@gmail.com

              </span>

          </td></tr>
          
          <tr>
          <td>
          
          
<form id="frmMain"  >


<div class="row">
<div class="col-md-12">
		
              <h2 style="border-bottom: 1px dashed silver; border-top: 1px dashed silver; 
                  color: gray; padding: 4px; font-size: 20px; margin-top: 10px;
                   background: none repeat scroll 0% 0% rgb(238, 238, 238); 
                   font-family: serif;width:100%;margin-bottom:10px">Review Your Experience with Peshawari </span></h2> 

</div>



<div class="col-md-6">
<div class="row">


<div class="col-md-6">
Title:
</div>

<div class="col-md-6">
 <input  type="text" id="txtTitle" name="txtTitle" style="width:215px;margin:3px" class="form-control input-small validate required"/>
</div>


<div class="col-md-6">
Review:
</div>


<div class="col-md-6">
   <textarea id="txtDescription" name="txtDescription" style="width:215px;margin:3px" class="form-control input-small validate required"></textarea>
</div>


<div class="col-md-6">
Rating:
</div>


<div class="col-md-6">

<asp:DropDownList ID="ddlRate"     runat="server"  style="width:217px;height:35px;margin:3px">

 <asp:ListItem Value ="None" Text ="None"></asp:ListItem>
 <asp:ListItem Value ="Poor" Text ="Poor"></asp:ListItem>
 <asp:ListItem Value ="Average" Text ="Average"></asp:ListItem>
 <asp:ListItem Value ="Good" Text ="Good"></asp:ListItem>
 <asp:ListItem Value ="Excellent" Text ="Excellent"></asp:ListItem>
</asp:DropDownList>
 
 
</div>

<div class="col-md-6"></div>
<div class="col-md-6">
<div id="btnAdd" class="btn btn-success">Submit</div>
</div>




</div>


</div>
</div>
 
 
           
       </form>
          
          </td>
          </tr>
          
          </table>
         </td> <td style="padding-top:10px;padding-left:10px;width:200px;vertical-align:top">

     <h4 style="padding-left:10px;color:black"><b>Shipping Address</b></h4>
    <span style="padding-left:10px;color:black" id="spAddress"><%=ShippingAddress %></span>

         </td></tr></table>
    </div>
             </td></tr>
    </table>





    </div>
       </div>
         <div id="dvMenuCategories" style="position:absolute;top:153px;display:none">

<uc1:ucMenu ID="ucMenu1" runat="server" />

</div>
</asp:Content>

