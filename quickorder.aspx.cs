﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;
using System.Text;

public partial class quickorder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }



    [WebMethod]
    public static string BindDeliveryAddress()
    {
        Int32 Uid = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId] == null ? "0" : HttpContext.Current.Session[Constants.UserId].ToString());
        Int64 DeliveryAddressId = 0;
        string groupHTML = new DeliveryAddressBLL().GetHtmlByUserId(Uid, out DeliveryAddressId);

        return groupHTML;
    }


    [WebMethod]
    public static string BindDays()
    {

        ListItem li = new ListItem();
        li.Text = "";
        li.Value = "";


        DateTime dd;
        DateTime totaldd;
        dd = Convert.ToDateTime(DateTime.Now.ToString("d"));
        totaldd = dd.AddDays(7);
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.Append(string.Format("<option value={0}>{1}</option>", "", ""));


        while (dd != totaldd)
        {
            li = new ListItem();
            li.Text = dd.ToLongDateString();
            li.Value = dd.DayOfWeek.ToString();
            if (li.Value != "Sunday")
            {

                strBuilder.Append(string.Format("<option value={0}>{1}</option>", dd.DayOfWeek.ToString(), dd.ToLongDateString()));



            }
            dd = dd.AddDays(1);

        }
        var JsonData = new
        {
            ddd = strBuilder.ToString()

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }




    [WebMethod]
    public static string BindSlots(string SelectedDay)
    {
        string ggg = "";
        // string Date = Convert.ToDateTime(SelectedDay).ToShortDateString() + " ";
        TimeSlots objTimeSlots = new TimeSlots();


        bool IsAvailable = false;
        DateTime dd;
        dd = Convert.ToDateTime(DateTime.Now.ToString("d"));

        string curday = dd.DayOfWeek.ToString();
        // string[] slctday = SelectedDay.Split(','); 
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.Append(string.Format("<option value={0}>{1}</option>", "", ""));

        if (curday != SelectedDay)
        {

            ggg = new TimeSlotsBLL().GetAllSlotsOptions();


        }
        else
        {

            ggg = new TimeSlotsBLL().GetSlotsoption();
        }

        var JsonData = new
        {
            Slots = ggg,
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);


    }
   
    


    [WebMethod]
    public static string KeywordSearch(string Keyword)
    {
        string Data = new ProductsBLL().QuickListKeywordSearch(Keyword.Trim());

        return Data;
    }

    [WebMethod]
    public static string InsertQuickProducts(Int32 DeliveryAddress, string DeliveryDay, Int32 DeliverySlot, string ProductNameArr, string qtyArr, string unitArr)
    {
        Int32 uId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        QuickList objQuickList = new QuickList()
        {
            UserId = uId,
            DeliveryAddressId = Convert.ToInt32(DeliveryAddress),
            DeliveryDate = Convert.ToDateTime(DeliveryDay),
            DeliverySlot = DeliverySlot,


        };


        string[] ProductName = ProductNameArr.Split(',');
        string[] Qty = qtyArr.Split(',');
        string[] Unit = unitArr.Split(',');



        DataTable dt = new DataTable();
        dt.Columns.Add("ProductName");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Unit");






        for (int i = 0; i < ProductName.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ProductName"] = ProductName[i];
            dr["Qty"] = Qty[i];
            dr["Unit"] = Unit[i];

            dt.Rows.Add(dr);
        }



        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new QuickListBLL().InsertQuickList(objQuickList, dt);
        var JsonData = new
        {
            list = status,

        };
        return ser.Serialize(JsonData);
    }

}