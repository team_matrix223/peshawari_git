﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="System" %>
<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup

        RegisterRoutes(RouteTable.Routes);
        // Code that runs on application startup

    }

    public static void RegisterRoutes(RouteCollection routeCollection)
    {

        routeCollection.MapPageRoute("RouteForCustomer", "Customer/{Id}/{*Cid}", "~/Customer.aspx");
        routeCollection.MapPageRoute("test", "test/{Id}/{*Cid}", "~/test.aspx");

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
