﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="productdetail.aspx.cs" Inherits="productdetail" %>

<%@ Register Src="~/usercontrols/ucMenu.ascx" TagPrefix="uc1" TagName="ucMenu" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cnMaster" Runat="Server">
   <%--<script src="js/jquery-1.6.js"></script>--%>
    
<script src='jquery-1.8.3.min.js'></script>
	<%--<script src='jquery.elevatezoom.js'></script>--%>
   <%-- <script src="js/jquery.min.js" type="text/javascript"></script>--%>
    <script src="smartpaginator.js" type="text/javascript"></script>
    <link href="smartpaginator.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.uilocksearch.js" type="text/javascript"></script>
     <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="css/jquery.notifyBar.css">
   <script src="jquery.notifyBar.js"></script>    
         <link rel="stylesheet" href="styles.css">
   <script src="script.js"></script>

       <script src="js/jquery.jqzoom-core.js"></script>
        <link href="css/jquery.jqzoom.css" rel="stylesheet" />


   
       <style type="text/css">
  
#facet-loading {
    background: none repeat scroll 0 0 #fff;
    border: 4px double #ccc;
    border-radius: 12px;
    float: left;
    margin-left: 280px;
    margin-top: 200px;
    opacity: 0.9;
    padding: 10px;
    position: absolute;
    z-index: 999;
   
}
#facet-loading span {
    font: 400 17px/23px Lato,Arial,Helvetica,sans-serif;
    padding: 10px;
    vertical-align: middle;
}
#facet-loading img {
    vertical-align: middle;
}
#facet-loading-mask {
    background-color: rgba(61, 61, 61, 0.3);
    height: 100% !important;
    left: 0;
    margin: 0 auto;
    position: absolute;
    top: 0;
    width: 100% !important;
    z-index: 998;
}
#facet-products-wrapper {
    position: relative;
}


</style>
    <script type="text/javascript" language="javascript" >
        
 
        $(document).on("change", "input[name='radioVariation']", function (event) {
            var pid = $(this).attr("id");
            var arrPid = pid.split('_');
            var ProductId = arrPid[1];
            var VariationId = arrPid[2];
            $("#<%=hdnProductId.ClientID%>").val(ProductId);
            $("#<%=hdnVariationId.ClientID%>").val(VariationId);
            GetData(ProductId, VariationId);
        });
        function GetData(ProductId, VariationId) {
            $.ajax({
                type: "POST",
                data: '{"ProductId":"' + ProductId + '","VariationId":"' + VariationId + '"}',
                url: "productdetail.aspx/GetData",
                contentType:"application/json",
                datatype: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.error == 1) {

                        alert("An Error occured during transaction. Please refresh the page and try again Later.");
                        return;
                    }

                    $("#dvPhoto").html(obj.PhotoUrl);
                    if (obj.CartCount == 0) {
                        $("#tdQty0").css("display", "block");
                        $("#tdQty1").css("display", "none");
                    }
                    else {
                        $("#tdQty1").css("display", "block");
                        $("#tdQty0").css("display", "none");
                        $("#cqty").html(obj.CartCount);
                    }
                   // $("#dvName").html("");
                   // $('#zoom_01').attr('src', '');
                    $("#dvName").html(obj.Name);

                    $('.minipic').jqzoom({
                        zoomType: 'standard',
                        lens: true,
                        preloadImages: false,
                        alwaysOn: false,
                        zoomHeight: 250,
                        zoomWidth: 250
                    });





//                    $('.zoomContainer').remove();
//                    $('#zoom_01').removeData('elevateZoom');
//                    $('#zoom_01').elevateZoom({
//                        zoomType: "inner",
//                       cursor: "crosshair",
//                        zoomWindowFadeIn: 500,
//                        zoomWindowFadeOut:500
//                    });
                   
                  
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {


                }
            });
        }
        function ATC(vid, qty, st, type) {
            $.ajax({
                type: "POST",
                data: '{"vid":"' + vid + '","qty":"' + qty + '","st":"' + st + '","type":"' + type + '"}',
                url: "index.aspx/FirstTimeATC",
                async: false,
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.error == 1) {

                        alert("An Error occured during transaction. Please refresh the page and try again Later.");
                        return;
                    }


                    if (type == "Combo") {

                        $("#cartcontainer").html(obj.cartHTML);

                        $("#cbox_" + obj.pid).html(obj.cartHTML);


                        if (obj.qty <= 0) {

                            $("#cbox_" + obj.pid).css("background", "white")

                        }
                        else {

                            $("#cbox_" + obj.pid).css("background", "#D8EDC0")

                        }



                    }
                    else {

                        $("#variation_" + vid).html(obj.productHTML);
                    }
                    var ProductId = $("#<%=hdnProductId.ClientID%>").val();
                    var VariationId = $("#<%=hdnVariationId.ClientID%>").val();
                    GetData(ProductId, VariationId);
                    //javascript: goBack();

                   // BindCart();
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {


                }

            });

        }
        $(document).ready(
            function () {
           
                var ProductId = $("#<%=hdnProductId.ClientID%>").val();
                var VariationId = $("#<%=hdnVariationId.ClientID%>").val();
                GetData(ProductId, VariationId);
            
                $("#btnAddToCart").click(
                    function () {                                           
                        var vid = $("#<%=hdnVariationId.ClientID%>").val();
                        var qty = $("#ddlQty").val();
                        var st = "p";
                        var type = "Product";                     
                        ATC(vid, qty, st, type);
                        $.notifyBar({ cssClass: "success", html: "Product Added Succcessfully!" });                      
                    });
                $("#decr").click(
                    function()
                    {
                    var vid = $("#<%=hdnVariationId.ClientID%>").val();
                    var qty = 1;
                    var st = "m";
                    var type = "Product";
                    ATC(vid, qty, st, type);
                });


                $("#incr").click(function() {
                    var vid = $("#<%=hdnVariationId.ClientID%>").val();
                    var qty = 1;
                    var st = "p";
                    var type = "Product";
                    ATC(vid, qty, st, type);
                });
               
            });

</script>
    <div class="row">
<div class="col-md-3">
 
    <asp:HiddenField ID="hdnProductId" Value="0" runat="server" />
    <asp:HiddenField ID="hdnVariationId" Value="0"  runat="server" />
    <div>
        

    </div>
 <table style="width:600px">
  <tr>
<td >
  <div style="position:relative;top:200px;left:300px;width:200px;">
        
      
        </div>
    <div id="dvPhoto" style="margin: 40px 4px 4px 50px;border:solid;width:100px;height:300px;border-color:white;border-width:1px">    
    
 </div>
</td>
    
      <td valign="top" style="padding-top:15px">
          <table><tr><td><div id="dvName"></div></td></tr>
              
         <tr><td colspan='100%' style='padding-top:0px;padding-left:0px;display:none' id="tdQty0"><table><tr><td><div id='btnAddToCart'  class='btn btn-success atc' style='width:55px;height:28px'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</div>
                   </td><td style='padding:10px 0px 0px 10px'>Qty<select name='ddlQty' id='ddlQty'  style='margin:1px 1px 1px 1px; padding:3px;border:thin 1px'>
                   <option value='1' style='font-size:12px;'>1</option><option value='2' style='font-size:12px;'>2</option>
                   <option value='3' style='font-size:12px;'>3</option><option value='4' style='font-size:12px;'>4</option><option value='5' style='font-size:12px;'>5</option></select></td></tr></table></td>
             <td colspan='100%' style='padding-top:0px;display:none' id="tdQty1">
                 <div style='margin:14px  0px 0px 20px'><div class='priceDiv'>
    <div class='decr' id='decr' >-</div><div style='width:80px;text-align:center;float:left;background:white;padding:0px 0 0 3px;' id='cqty'></div>
     <div  class='incr' id='incr' >+</div></div></div>
             </td>
         </tr>


          </table>
          </td>
      </tr>

  </table>
   </div>
   
      </div>
    
 
<div id="dvMenuCategories" style="position:absolute;top:153px;display:none">

<uc1:ucMenu ID="ucMenu1" runat="server" />

</div>
</asp:Content>

