﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindProductCategories();
            BindFeaturedCategories();
            BindSlider();
            BindCart();
        }
    }
    void BindCart()
    {
        repCart.DataSource = new CartBLL().GetCartBySessionId(Session.SessionID);
        repCart.DataBind();
    }
    void BindSlider()
    {
        List<Sliders> sliders = new SlidersBLL().GetAll();
        string strSliders = "";
        foreach (var i in sliders)
        {
            if (i.IsActive == true)
            {
                strSliders += "<div><img u='image' src2='SliderImages/" + i.ImageUrl + "' /></div>";
            }
        }
        ltSlider.Text = strSliders;





    }

    void BindFeaturedCategories()
    {
        repHomeFeaturedCategories.DataSource = new CategoryBLL().GetFeaturedCategories();
        repHomeFeaturedCategories.DataBind();
    }
    public List<Products> GetProductList(object CategoryLevel1)
    {
        return new ProductsBLL().GetRandomProductsByFeaturedCategory(Convert.ToInt16(CategoryLevel1), Session.SessionID);
    }

    void BindProductCategories()
    {

        repMenuCategories.DataSource = new CategoryBLL().GetForFrontEnd();
        repMenuCategories.DataBind();

    }
    protected void repHomeFeaturedCategories_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }

    protected void ddlVariation_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Get the drop down instance as follows
        DropDownList ddlVariation = sender as DropDownList;
        // Get the current item index
        RepeaterItem repProducts = (RepeaterItem)ddlVariation.NamingContainer;

        ((HtmlGenericControl)(repProducts.FindControl("dvBox"))).Attributes["style"] = ("background:white" + ";)");

        int currentItemIndex = ((RepeaterItem)ddlVariation.NamingContainer).ItemIndex;

        HiddenField hdnQA = (HiddenField)repProducts.FindControl("hdnQA");
        HiddenField hdnQB = (HiddenField)repProducts.FindControl("hdnQB");
        HiddenField hdnQC = (HiddenField)repProducts.FindControl("hdnQC");
        Panel pnlFirst = (Panel)repProducts.FindControl("pnlFirst");
        Panel pnlSecond = (Panel)repProducts.FindControl("pnlSecond");
        pnlFirst.Visible = true;
        pnlSecond.Visible = false;

        if (ddlVariation.SelectedItem.Value == "a")
        {
            Label lblMrpPrice = (Label)repProducts.FindControl("lblMrpPrice");
            Label lblPrice = (Label)repProducts.FindControl("lblPrice");
            HiddenField hdnPrice = (HiddenField)repProducts.FindControl("hdnp1");
            HiddenField hdnMRP = (HiddenField)repProducts.FindControl("hdnmrp1");
            lblMrpPrice.Text = hdnMRP.Value + "Rs";
            lblPrice.Text = hdnPrice.Value + "Rs";


            if (hdnQA.Value == "1")
            {
                pnlFirst.Visible = false;
                pnlSecond.Visible = true;
                ((HtmlGenericControl)(repProducts.FindControl("dvBox"))).Attributes["style"] = ("background:#EFE1B0" + ";)");

            }

        }
        else if (ddlVariation.SelectedItem.Value == "b")
        {
            Label lblMrpPrice = (Label)repProducts.FindControl("lblMrpPrice");
            Label lblPrice = (Label)repProducts.FindControl("lblPrice");
            HiddenField hdnPrice = (HiddenField)repProducts.FindControl("hdnp2");
            HiddenField hdnMRP = (HiddenField)repProducts.FindControl("hdnmrp2");
            lblMrpPrice.Text = hdnMRP.Value + "Rs";
            lblPrice.Text = hdnPrice.Value + "Rs";

            if (hdnQB.Value == "1")
            {
                pnlFirst.Visible = false;
                pnlSecond.Visible = true;
                ((HtmlGenericControl)(repProducts.FindControl("dvBox"))).Attributes["style"] = ("background:#EFE1B0" + ";)");

            }

        }

        else if (ddlVariation.SelectedItem.Value == "c")
        {
            Label lblMrpPrice = (Label)repProducts.FindControl("lblMrpPrice");
            Label lblPrice = (Label)repProducts.FindControl("lblPrice");
            HiddenField hdnPrice = (HiddenField)repProducts.FindControl("hdnp3");
            HiddenField hdnMRP = (HiddenField)repProducts.FindControl("hdnmrp3");
            lblMrpPrice.Text = hdnMRP.Value + "Rs";
            lblPrice.Text = hdnPrice.Value + "Rs";
            if (hdnQC.Value == "1")
            {
                pnlFirst.Visible = false;
                pnlSecond.Visible = true;

                ((HtmlGenericControl)(repProducts.FindControl("dvBox"))).Attributes["style"] = ("background:#EFE1B0" + ";)");

            }

        }


    }



    protected void repCart_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        
        int ProductId = Convert.ToInt32(((HiddenField)e.Item.FindControl("hdnCartProductId")).Value);
        TextBox txtProdQty = (TextBox)e.Item.FindControl("txtCartQty");
        decimal Price = Convert.ToDecimal(((Label)e.Item.FindControl("lblCartPrice")).Text);
        HiddenField hdnVariation = (HiddenField)e.Item.FindControl("hdnVariation");
        Label lblProductDesc =  (Label)e.Item.FindControl("lblProductDesc");
        if (e.CommandName == "CartPlus")
        {
           UserCart objUserCart = new UserCart()
            {
                SessionId = Session.SessionID,
                ProductId = Convert.ToInt32(ProductId),
                CustomerId = 0,
                VariationId = hdnVariation.Value,
                ProductDesc = lblProductDesc.Text,
                Qty = 0,
                Price = Price

            };

            new CartBLL().UserCartInsertUpdate(objUserCart, "Plus");
            txtProdQty.Text = objUserCart.Qty.ToString();
        }
        else if (e.CommandName == "CartMinus")
        {

            UserCart objUserCart = new UserCart()
            {
                SessionId = Session.SessionID,
                ProductId = Convert.ToInt32(ProductId),
                CustomerId = 0,
                VariationId = hdnVariation.Value,
                ProductDesc = lblProductDesc.Text,
                Qty = 0,
                Price = Price

            };

            new CartBLL().UserCartInsertUpdate(objUserCart, "Minus");
             
        }
        BindCart();
        BindFeaturedCategories();
        updOuterCart.Update();
     

    }


    protected void repProductList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int ProductId = Convert.ToInt32(((HiddenField)e.Item.FindControl("hdnProductId")).Value);
        DropDownList ddlVariation = (DropDownList)e.Item.FindControl("ddlVariation");
        TextBox txtProdQty = (TextBox)e.Item.FindControl("txtProdQty");
        Panel pnlFirst = (Panel)e.Item.FindControl("pnlFirst");
        Panel pnlSecond = (Panel)e.Item.FindControl("pnlSecond");

        //System.Threading.Thread.Sleep(5000);
        if (e.CommandName == "AddToCart")
        {
            HiddenField hdnQA = (HiddenField)e.Item.FindControl("hdnQA");
            HiddenField hdnQB = (HiddenField)e.Item.FindControl("hdnQB");
            HiddenField hdnQC = (HiddenField)e.Item.FindControl("hdnQC");


            DropDownList ddlQty = (DropDownList)e.Item.FindControl("ddlQty");


            pnlFirst.Visible = true;
            pnlSecond.Visible = false;



            decimal Price = 0;
            if (ddlVariation.SelectedItem.Value == "a")
            {
                Price = Convert.ToDecimal(((HiddenField)e.Item.FindControl("hdnp1")).Value);

                hdnQA.Value = "1";
            }
            else if (ddlVariation.SelectedItem.Value == "b")
            {
                Price = Convert.ToDecimal(((HiddenField)e.Item.FindControl("hdnp2")).Value);

                hdnQB.Value = "1";
            }
            else if (ddlVariation.SelectedItem.Value == "c")
            {
                Price = Convert.ToDecimal(((HiddenField)e.Item.FindControl("hdnp3")).Value);

                hdnQC.Value = "1";
            }

            UserCart objUserCart = new UserCart()
            {
                SessionId = Session.SessionID,
                ProductId = Convert.ToInt32(ProductId),
                CustomerId = 0,
                VariationId = ddlVariation.SelectedItem.Value,
                ProductDesc = "",
                Qty = Convert.ToInt16(ddlQty.SelectedValue),
                Price = Price

            };

            new CartBLL().UserCartInsertUpdate(objUserCart, "Add");

            pnlFirst.Visible = false;
            pnlSecond.Visible = true;
            txtProdQty.Text = ddlQty.SelectedValue;


            if (ddlVariation.SelectedItem.Value == "a")
            {
                hdnQA.Value = "1";
            }
            else if (ddlVariation.SelectedItem.Value == "b")
            {
                hdnQB.Value = "1";
            }
            else if (ddlVariation.SelectedItem.Value == "c")
            {
                hdnQC.Value = "1";
            }

            ((HtmlGenericControl)(e.Item.FindControl("dvBox"))).Attributes["style"] = ("background:#EFE1B0" + ";)");
        }
        else if (e.CommandName == "Plus")
        {
            decimal Price = 0;
            if (ddlVariation.SelectedItem.Value == "a")
            {
                Price = Convert.ToDecimal(((HiddenField)e.Item.FindControl("hdnp1")).Value);

            }
            else if (ddlVariation.SelectedItem.Value == "b")
            {
                Price = Convert.ToDecimal(((HiddenField)e.Item.FindControl("hdnp2")).Value);

            }
            else if (ddlVariation.SelectedItem.Value == "c")
            {
                Price = Convert.ToDecimal(((HiddenField)e.Item.FindControl("hdnp3")).Value);

            }

            UserCart objUserCart = new UserCart()
            {
                SessionId = Session.SessionID,
                ProductId = Convert.ToInt32(ProductId),
                CustomerId = 0,
                VariationId = ddlVariation.SelectedItem.Value,
                ProductDesc = "",
                Qty = 0,
                Price = Price

            };

            new CartBLL().UserCartInsertUpdate(objUserCart, "Plus");
            txtProdQty.Text = objUserCart.Qty.ToString();
        }
        else if (e.CommandName == "Minus")
        {
            decimal Price = 0;
            if (ddlVariation.SelectedItem.Value == "a")
            {
                Price = Convert.ToDecimal(((HiddenField)e.Item.FindControl("hdnp1")).Value);

            }
            else if (ddlVariation.SelectedItem.Value == "b")
            {
                Price = Convert.ToDecimal(((HiddenField)e.Item.FindControl("hdnp2")).Value);

            }
            else if (ddlVariation.SelectedItem.Value == "c")
            {
                Price = Convert.ToDecimal(((HiddenField)e.Item.FindControl("hdnp3")).Value);

            }

            UserCart objUserCart = new UserCart()
            {
                SessionId = Session.SessionID,
                ProductId = Convert.ToInt32(ProductId),
                CustomerId = 0,
                VariationId = ddlVariation.SelectedItem.Value,
                ProductDesc = "",
                Qty = 0,
                Price = Price

            };

            new CartBLL().UserCartInsertUpdate(objUserCart, "Minus");
            txtProdQty.Text = objUserCart.Qty.ToString();

            if (objUserCart.Qty == 0)
            {
                pnlSecond.Visible = false;
                pnlFirst.Visible = true;
                ((HtmlGenericControl)(e.Item.FindControl("dvBox"))).Attributes["style"] = ("background:white" + ";)");
            }

        }
        BindCart();
        updOuterCart.Update();


    }

    protected void repProductList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        DropDownList ddlVariation = (DropDownList)e.Item.FindControl("ddlVariation");
        string Unit1 = DataBinder.Eval(e.Item.DataItem, "Unit1").ToString();
        string Unit2 = DataBinder.Eval(e.Item.DataItem, "Unit2").ToString();
        string Unit3 = DataBinder.Eval(e.Item.DataItem, "Unit3").ToString();

        HiddenField hdnQA = (HiddenField)e.Item.FindControl("hdnQA");
        HiddenField hdnQB = (HiddenField)e.Item.FindControl("hdnQB");
        HiddenField hdnQC = (HiddenField)e.Item.FindControl("hdnQC");

        TextBox txtProdQty = (TextBox)e.Item.FindControl("txtProdQty");
        Int16 Variation1 = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "Variation1"));
        Int16 Variation2 = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "Variation2"));

        Int16 Variation3 = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "Variation3"));
        Panel pnlFirst = (Panel)e.Item.FindControl("pnlFirst");
        Panel pnlSecond = (Panel)e.Item.FindControl("pnlSecond");
        pnlFirst.Visible = true;
        pnlSecond.Visible = false;

        hdnQA.Value = "0";
        hdnQB.Value = "0";
        hdnQC.Value = "0";

        if (Variation1 > 0)
        {
            txtProdQty.Text = Variation1.ToString();
            hdnQA.Value = "1";
            ((HtmlGenericControl)(e.Item.FindControl("dvBox"))).Attributes["style"] += ("background:" + "#EFE1B0" + ";)");
            pnlFirst.Visible = false;
            pnlSecond.Visible = true;
        }


        if (Variation2 > 0)
        {
            hdnQB.Value = "1";

        }



        if (Variation3 > 0)
        {
            hdnQC.Value = "1";

        }


        hdnQA.Value = Variation1 > 0 ? "1" : "0";
        hdnQB.Value = Variation2 > 0 ? "1" : "0";
        hdnQC.Value = Variation3 > 0 ? "1" : "0";




        if (Unit1 != "")
        {
            ListItem li = new ListItem();
            li.Text = DataBinder.Eval(e.Item.DataItem, "Qty1").ToString() + " " + Unit1 + " (" + DataBinder.Eval(e.Item.DataItem, "Desc1").ToString() + ") - Rs." + DataBinder.Eval(e.Item.DataItem, "Price1").ToString();
            li.Value = "a";
            ddlVariation.Items.Add(li);

        }


        if (Unit2 != "")
        {
            ListItem li = new ListItem();
            li.Text = DataBinder.Eval(e.Item.DataItem, "Qty2").ToString() + " " + Unit2 + " (" + DataBinder.Eval(e.Item.DataItem, "Desc2").ToString() + ") - Rs." + DataBinder.Eval(e.Item.DataItem, "Price2").ToString();
            li.Value = "b";
            ddlVariation.Items.Add(li);

        }


        if (Unit3 != "")
        {
            ListItem li = new ListItem();
            li.Text = DataBinder.Eval(e.Item.DataItem, "Qty3").ToString() + " " + Unit2 + " (" + DataBinder.Eval(e.Item.DataItem, "Desc3").ToString() + ") - Rs." + DataBinder.Eval(e.Item.DataItem, "Price3").ToString();
            li.Value = "c";
            ddlVariation.Items.Add(li);

        }

        //   ((HtmlGenericControl)(e.Item.FindControl("dvBox"))).Attributes["style"] += ("background:" + "yellow" + ";)");
    }
    protected void repHomeFeaturedCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
    }
}