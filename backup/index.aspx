﻿<%@ Page Title="" Language="C#" MasterPageFile="main.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>
<%@ Register src="usercontrols/ucEnquiryForm.ascx" tagname="ucEnquiryForm" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cnMaster" Runat="Server">

    <script type="text/javascript" src="js/jssor.js"></script>
    <script type="text/javascript" src="js/jssor.slider.js"></script>
<div class="row">
<div class="col-sm-3 border ">
<div class="freedelivery">
 <table >
 <tr>
 <td><img src="images/free-delivery.png" alt="" /></td>
 <td><h2>FREE DELIVERY</h2>
  <p style="font-size:12px">On order Value Above $250</p></td>
 </tr>
 </table>
 
 
 </div>
</div>

<div class="col-sm-3 border ">
<div class="freedelivery">
 <table >
 <tr>
 <td><img src="images/calender.png" alt="" /></td>
 <td><h2>FREE DELIVERY</h2>
  <p style="font-size:12px">On order Value Above $250</p></td>
 </tr>
 </table>
 
 </div>
</div>



<div class="col-sm-3 ">
<div class="freedelivery">
 <table >
 <tr>
 <td><img src="images/money.png" alt="" /></td>
 <td><h2>FREE DELIVERY</h2>
  <p style="font-size:12px">On order Value Above $250</p></td>
 </tr>
 </table>
 
 
 </div>
</div>


<div class="col-sm-3">
<div class="callus">
 <table>
 <tr>
 <td><img src="images/phonepic.png" alt="" /></td>
 <td><h2>CALL US : + 91-808-227</h2>
  <p style="font-size:12px; color:#FFFFFF;">Need Help Call Us</p></td>
 </tr>
 </table>
 
 
 </div>
</div>
 
</div>
<!--Row End-->


<!--Row Start-->
<div class="row">

<div class="col-sm-3" style="padding-right:0px" >

<div class="list-group" style="margin:10px 0 0 0">
   <a href="#" class="list-group-item active">OUR CATEGORIES</a>
<asp:Repeater ID="repMenuCategories" runat="server">
<ItemTemplate>
         
   <a href="#" class="list-group-item" style="text-transform:uppercase"><%#Eval("Title") %></a>
</ItemTemplate>

</asp:Repeater>

    
          </div>
          <img src="images/shado.png" style="margin-top:-12px"/>
         
</div>

<div class="col-md-9" padding:0px>
<div class="slider">

<div id="slider2_container" style="display: none; position: relative; margin: 0 auto; width: 980px;
        height: 470px; overflow: hidden;">

            <!-- Loading Screen -->
            <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;

                background-color: #000; top: 0px; left: 0px;width: 100%; height:100%;">
                </div>
                <div style="position: absolute; display: block; background: url(img/loading.gif) no-repeat center center;

                top: 0px; left: 0px;width: 100%;height:100%;">
                </div>
            </div>

            <!-- Slides Container -->
            <div    u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 980px; height: 470px;
            overflow: hidden;">
                  <asp:Literal ID="ltSlider" runat="server"></asp:Literal>


            </div>
            <!-- Bullet Navigator Skin Begin -->
            <style>
                /* jssor slider bullet navigator skin 05 css */
                /*
                .jssorb05 div           (normal)
                .jssorb05 div:hover     (normal mouseover)
                .jssorb05 .av           (active)
                .jssorb05 .av:hover     (active mouseover)
                .jssorb05 .dn           (mousedown)
                */
                .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
                    background: url(img/b05.png) no-repeat;
                    overflow: hidden;
                    cursor: pointer;
                }

                .jssorb05 div {
                    background-position: -7px -7px;
                }

                    .jssorb05 div:hover, .jssorb05 .av:hover {
                        background-position: -37px -7px;
                    }

                .jssorb05 .av {
                    background-position: -67px -7px;
                }

                .jssorb05 .dn, .jssorb05 .dn:hover {
                    background-position: -97px -7px;
                }
            </style>
            <!-- bullet navigator container -->
            <div u="navigator" class="jssorb05" style="position: absolute; bottom: 16px; right: 6px;">
                <!-- bullet navigator item prototype -->
                <div u="prototype" style="POSITION: absolute; WIDTH: 16px; HEIGHT: 16px;"></div>
            </div>
            <!-- Bullet Navigator Skin End -->
            <!-- Arrow Navigator Skin Begin -->
            <style>
                /* jssor slider arrow navigator skin 11 css */
                /*
                .jssora11l              (normal)
                .jssora11r              (normal)
                .jssora11l:hover        (normal mouseover)
                .jssora11r:hover        (normal mouseover)
                .jssora11ldn            (mousedown)
                .jssora11rdn            (mousedown)
                */
                .jssora11l, .jssora11r, .jssora11ldn, .jssora11rdn {
                    position: absolute;
                    cursor: pointer;
                    display: block;
                    background: url(img/a11.png) no-repeat;
                    overflow: hidden;
                }

                .jssora11l {
                    background-position: -11px -41px;
                }

                .jssora11r {
                    background-position: -71px -41px;
                }

                .jssora11l:hover {
                    background-position: -131px -41px;
                }

                .jssora11r:hover {
                    background-position: -191px -41px;
                }

                .jssora11ldn {
                    background-position: -251px -41px;
                }

                .jssora11rdn {
                    background-position: -311px -41px;
                }
            </style>
            <!-- Arrow Left -->
            <span u="arrowleft" class="jssora11l" style="width: 37px; height: 37px; top: 123px; left: 8px;">
            </span>
            <!-- Arrow Right -->
            <span u="arrowright" class="jssora11r" style="width: 37px; height: 37px; top: 123px; right: 8px">
            </span>
            <!-- Arrow Navigator Skin End -->
            <a style="display: none" href="http://www.jssor.com">jQuery Slider</a>
        </div>


<%--<img src="images/slider-image.png" class="img-responsive" alt="">--%>
</div>

</div>

</div>
<!--Row End-->



<!--Row Start-->
<div class="row" style="margin-top:15px;">

<div class="col-md-4">
<img src="images/1img.png" class="img-responsive" alt="" />

</div>

<div class="col-md-4">

<img src="images/2img.png" class="img-responsive" alt="" />
</div>

<div class="col-md-4">

<img src="images/3img.png" class="img-responsive" alt="" />
</div>

</div>
<!--Row End-->


<!--Row Start-->
<div class="row">

<div class="col-sm-9">


<asp:Repeater ID="repHomeFeaturedCategories" runat="server" 
        onitemcommand="repHomeFeaturedCategories_ItemCommand" 
        onitemdatabound="repHomeFeaturedCategories_ItemDataBound">
<ItemTemplate>

<div class="vegetables">
<div class="line" style="height: 2px; background-color: #abaaaa; text-align: left;margin-top:20px">
<span style="text-transform:uppercase;background-color: white; position: relative; top:-0.6em;font-size:25px; font-family:Verdana, Geneva, sans-serif; color:#3c3b3b; ">
      <%#Eval("Title")%> 
</span>
</div>
</div>


<div class="row boxbordre">

<asp:UpdatePanel ID="upOuterProductList" UpdateMode="Conditional" runat="server">
<ContentTemplate>

<asp:Repeater ID="repProductList" OnItemDataBound="repProductList_ItemDataBound" OnItemCommand="repProductList_ItemCommand" runat="server" DataSource='<%#GetProductList(Eval("CategoryId")) %>'>
<ItemTemplate>
<asp:UpdatePanel ID="upd1" runat="server">
<ContentTemplate>
<asp:UpdateProgress ID="updprogress1" AssociatedUpdatePanelID="upd1" runat="server">
<ProgressTemplate>
 Please wait while loading
 
</ProgressTemplate>
</asp:UpdateProgress>
<div class="col-md-3">

<div class="box1" id="dvBox" runat="server">
<img src='ProductImages/T_<%#Eval("PhotoUrl") %>' style="border:solid 1px #d6d3d3; margin-left:22px; margin-top:20px;" alt="">
<span><b>(<%#Eval("Name") %>)</b></span>

<div name="myform" >
<div align="center">
<asp:DropDownList ID="ddlVariation" ClientIDMode="AutoID"  OnSelectedIndexChanged="ddlVariation_SelectedIndexChanged" AutoPostBack="true" runat="server" Width="128px">
 


</asp:DropDownList>

<asp:HiddenField  id="hdnProductId" runat="server" Value='<%#Eval("ProductId") %>'/>

<asp:HiddenField  id="hdnQA" runat="server" value="0" />
<asp:HiddenField   id="hdnQB" runat="server" value="0" />
<asp:HiddenField   id="hdnQC" runat="server" value="0" />

<asp:HiddenField  id="hdnp1" runat="server" Value='<%#Eval("Price1") %>' />
<asp:HiddenField   id="hdnp2" runat="server" Value='<%#Eval("Price2") %>' />
<asp:HiddenField   id="hdnp3" runat="server" Value='<%#Eval("Price3") %>'  />
 
<asp:HiddenField  id="hdnmrp1" runat="server" Value='<%#Eval("Mrp1") %>' />
<asp:HiddenField   id="hdnmrp2" runat="server" Value='<%#Eval("Mrp2") %>'/>
 <asp:HiddenField   id="hdnmrp3" runat="server" Value='<%#Eval("Mrp3") %>'/>
 
</div>
</div>
<table>
<tr><td><h2 style="text-decoration:line-through"><asp:Label ID="lblMrpPrice" runat="server" Text="20 Rs"></asp:Label></h2></td><td><h2><asp:Label ID="lblPrice" Text="50 Rs" runat="server"></asp:Label></h2></td></tr>
</table>


<asp:Panel ID="pnlFirst" runat="server" Visible="true">
<div class="qty">
<div name="myform" >
<div align="center">

<div style="background-color:#f4f2f2;width:68px;border:solid 1px silver;border-radius:4px">
  Qty  
<asp:DropDownList ID="ddlQty" runat="server" Width="40px" >
<asp:ListItem Text="1" Value="1"></asp:ListItem>
<asp:ListItem Text="2" Value="2"></asp:ListItem>
<asp:ListItem Text="3" Value="3"></asp:ListItem>
<asp:ListItem Text="4" Value="4"></asp:ListItem>
<asp:ListItem Text="5" Value="5"></asp:ListItem>
 
</asp:DropDownList>
 
</div>
</div>
</div>

</div>


 <asp:ImageButton ID="imgAdd" runat="server" style="padding:2px; margin-left:10px; margin-top:6px;"  ImageUrl="images/addtocart.png" Width="65px" Height="30px"  CommandName="AddToCart" />

</asp:Panel>


<asp:Panel ID="pnlSecond" runat="server" Visible="false">
<table style="margin-left:10px;margin-top:10px">
<tr><td>
<asp:Button runat="server" ID="btnPlus" Text="+" CommandName="Plus"/>
</td><td>
<asp:TextBox ID="txtProdQty" ReadOnly="true" runat="server" Width="80px"></asp:TextBox>
</td>

<td>
<asp:Button runat="server" ID="btnMinus" Text="-" CommandName="Minus"/>

</td></tr>
</table>

</asp:Panel>

 





<br>
<br>
</div>
</div>

</ContentTemplate>
</asp:UpdatePanel>

</ItemTemplate>
</asp:Repeater>

</ContentTemplate>
</asp:UpdatePanel>

 



</div>


</ItemTemplate>
</asp:Repeater>



 
 
</div>



<div class="col-md-3">
<div class="yourbasket">

<div id="dvTopBasket" style="border-bottom:solid 2px #d4d4d4;;padding:6px">
<img src="images/basket.png" alt="" > <h6 style="float:right;margin-right:20px;font-size:16px">Your Basket (3 items)</h6>
 </div>
 
<div class="basketitem">

<asp:UpdatePanel ID="updOuterCart" UpdateMode="Conditional" runat="server">
<ContentTemplate>

<asp:Repeater OnItemCommand="repCart_ItemCommand"  ID="repCart" runat="server">
<ItemTemplate>
 
 <asp:UpdatePanel ID="updInnerCart" UpdateMode="Conditional" runat="server">
<ContentTemplate>
 <table style="border-bottom:solid 2px #d4d4d4;" >
 <tr>
 <td style="vertical-align:top;padding-top:8px"><img src='ProductImages/T_<%#Eval("PhotoUrl") %>' style="width:45px;height:46px;margin:5px" alt="" /></td>




 <td style="padding-top:10px">
 
  

   <table>
 <tr><td><h4><b><%#Eval("Name") %>
 
 <asp:HiddenField ID="hdnCartProductId" runat="server" Value='<%#Eval("ProductId") %>'/>
 </b></h4></td></tr>


 <tr>
 <td><h5><%#Eval("ShortDescription") %>
 <asp:Label ID="lblProductDesc" runat="server" Text='<%#Eval("ProductDesc") %>'></asp:Label>
 </h5></td></tr>


 <tr>
 

  <td style="padding-top:10px;padding-bottom:10px">
 
 <div class="button">

 <div style="width:57px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;" />
  <asp:Button ID="btnCartMinus" CommandName="CartMinus" runat="server" Text="-"  style="width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;"/>
  <asp:TextBox ID="txtCartQty" Text='<%#Eval("Qty") %>' runat="server"  style="width:25px;float:left;background:white;padding:0px 0 0 3px;"></asp:TextBox>
 
 <asp:Button ID="btnCartPlus"  CommandName="CartPlus" runat="server" Text="+"  style="width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;"/>
 
 </div>
 
 <span style="color:#f40f00;margin-left:10px">Rs 
 <asp:Label ID="lblCartPrice" runat="server" Text='<%#Eval("Price") %>'></asp:Label>
 <asp:HiddenField ID="hdnVariation" runat="server"  Value='<%#Eval("VariationId") %>' />
</span>
 
 
 </td>


 </tr>

 
 
 
 </table>




 </td>
 </tr>
 </table>
 
 </ContentTemplate>
 </asp:UpdatePanel>
</ItemTemplate>
</asp:Repeater>
 

</ContentTemplate>
</asp:UpdatePanel>

 
 
 
 
 <table>
 <tr>
 <td style="padding:10px 0px 0px 70px;">
 <table>
 <tr><td><h4><b>Sub Total: Rs. 267.8</b></h4></td></tr>
 <tr>
 <td><h5>Delivery Charges: Rs. 20</h5></td>
 <tr>
 <td><h5 style="color:#f40f00;margin-left:10px">Total: Rs. 287.8</h5></td>
 <tr>
 <td style="padding:10px 0 0 50px;">
 <button type="button" class="btn btn-success">Checkout</button>
 
 </td>
 </tr>
 </table>
 </td>
 </tr>
 </table>
 
 
 
 
 
<br>

 
 
 
 </div>
 
</div>



 
<div class="yourbasket">
<div class="enqury"><h2>ENQUIRY FORM</h2></div>
<uc1:ucEnquiryForm ID="ucEnquiryForm1" runat="server" />

<%--<form id="contact_form"  style="margin:15px 0 0 30px"; action="#" method="POST" enctype="multipart/form-data">
	<div class="row" >
	<input id="Text1" class="input inputCommon" name="email" type="text" value="" placeholder="Enter Your Name" size="30" style="margin:10px 0 0 0px"  />
	</div>

    <div class="row" >
	<input id="Text2" class="input inputCommon" name="email" type="text" value="" placeholder="Enter Your Email" size="30" style="margin:10px 0 0 0px"  />
	
    <br />
    </div>

    <div class="row">
    <textarea id="message" class="input inputEnquiry bigtext" name="message" rows="4" cols="26" style="margin-top:10px"></textarea><br />
	</div>
    
    
	<div class="row" >
	<input id="txtEmail" class="input inputCommon" name="email" type="text" value="" placeholder="Enter Mobile No" size="30" style="margin:10px 0 0 0px"  />
	
    </div>
 
    <div class="ask">       
    <h4>Ask Price</h4>
    </div>  
    </form>--%>
    </div>
   	
    <br />
 <br />
    <br />
 
 
</div>


</div>





<div class="row">
<div class="col-md-4">
<div class="paypal">
<img src="images/footer-img.png" alt="" />
</div>
</div>
<div class="col-md-4">
<div class="paypal">
<table>
<tr><td><h4 style="font-size:18px; font-weight:700;">Are you Hooked on Gadgets?</h4></td>
<tr><td><h4 style="font-size:14px; padding:5px 0 0 0; font-weight:300;">Then, you will love our Mobile Apps
Download the HungerQuenchers Apps on</h4></td></tr>
<td style="vertical-align:top;padding-top:8px"><img src="images/googleply.png" style="margin:5px" alt=""> <img src="images/appstore.png" style="margin:5px" alt=""></td>
<tr><td><h4 style="font-size:14px; font-weight:300;">and Enjoy our Services on the Go</h4></td>

</table>
</div>
</div>
<div class="col-md-4">
<div class="paypal">
<img src="images/paypalimg.png" alt="" class="img-responsive"/>

</div>
</div>

</div>


 <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
<script>


        jQuery(document).ready(function ($) {
         





               
            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 6000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseOutQuint,          //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration: 800,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Scale: false,                                  //Scales bullets navigator or not while slider scale
                },

                $BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 12,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 4,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1,                                //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                    $Scale: false,                                  //Scales bullets navigator or not while slider scale
                }
            };

            //Make the element 'slider1_container' visible before initialize jssor slider.
            $("#slider2_container").css("display", "block");
            var jssor_slider1 = new $JssorSlider$("slider2_container", options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth) {
                    jssor_slider1.$ScaleWidth(parentWidth - 30);
                }
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });

</script>
</asp:Content>

