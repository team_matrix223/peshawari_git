﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using SFA;
public partial class orderreview : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (new CartBLL().ValidateCheckout(Session.SessionID) == 0)
        {
            Response.Redirect("index.aspx");
        } 
        if (!IsPostBack)
        {
            BindOrderInformation();
        }
        Int32 totitems = new CartBLL().GetTotalItems(HttpContext.Current.Session.SessionID);
        Master.TotalItems = totitems;
    }

    void BindOrderInformation()
    {
        Calc objCalc = new Calc();
        ltCartContainer.Text = new CartBLL().GetBasketHTML(Session.SessionID, objCalc,2);
       // ltNetAmount.Text = objCalc.NetAmount.ToString();
       // ltDeliveryCharges.Text = objCalc.DeliveryCharges.ToString();
        ltSubTotal.Text = objCalc.SubTotal.ToString();
        ltItems.Text = objCalc.TotalItems.ToString();

        UserCartMst objUserCartMaster = new UserCartMst()
        {
            SessionId = Session.SessionID  
        };

        new UserCartMstBLL().GetBySessionId(objUserCartMaster);
        ltAddress.Text = objUserCartMaster.Address;
        ltRecipientName.Text = objUserCartMaster.RecipientName;
        ltStreet.Text = objUserCartMaster.Street;
        ltArea.Text = objUserCartMaster.Area;
        ltCity.Text = objUserCartMaster.City;
        ltPinCode.Text = objUserCartMaster.PinCode;
        ltMobile.Text = objUserCartMaster.MobileNo;
        if (objUserCartMaster.Telephone != "")
        {
            ltMobile.Text += ", " + objUserCartMaster.Telephone;

        }
        ltDeliveryDate.Text = objUserCartMaster.DeliveryDate.ToLongDateString();
        ddlPaymentMode.Text = objUserCartMaster.PaymentMode;
        ltDeliverySlot.Text = objUserCartMaster.DeliverySlot;
        if (Convert.ToDecimal(ltSubTotal.Text) >= Convert.ToDecimal(objCalc.FreeDeliveryAmt))
        {
            trDel.Visible = false;
            ltDeliveryCharges.Text = "0";
            ltNetAmount.Text = objCalc.SubTotal.ToString();
        }
        else
        {
            trDel.Visible = true;
            ltDeliveryCharges.Text = objCalc.DeliveryCharges.ToString();
            ltNetAmount.Text = objCalc.NetAmount.ToString();
        }
        ltDisAmt.Text = objCalc.DisAmt.ToString();
        if (Convert.ToDecimal(ltDisAmt.Text) == 0)
        {
            trDisAmt.Visible = false;
          
           
        }
        else
        {
            trDisAmt.Visible = true;  
            ltNetAmount.Text = Convert.ToDecimal(Convert.ToDecimal(ltNetAmount.Text)-Convert.ToDecimal(ltDisAmt.Text)).ToString();
        }
    }

    [WebMethod]
    public static string InsertUpdate()
    {
        try
        {
            UserCartMst objUserCartMst = new UserCartMst();
            new UserCartMstBLL().InsertUpdate(objUserCartMst);

            return "success";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    public void sendMsg(string MobileNo,string MsgText)
    {
        try
        {
            WebClient Client = new WebClient();
            string baseurl = "http://alerts.sinfini.com/api/web2sms.php?username=peshawari&password=Rohit@123&to=" + MobileNo.ToString() + "&sender=Peshaw&message=" + MsgText.ToString() + "";
         //  string baseurl = "http://india.timessms.com/http-api/receiverall.aspx?username=harjit&password=9617583&sender=Demo&cdmasender=&to=" + MobileNo.ToString() + "&message= "  + MsgText.ToString() + "";
            Stream data = Client.OpenRead(baseurl);
            StreamReader reader = new StreamReader(data);
            string s1 = reader.ReadToEnd();
            data.Close();
            reader.Close();
        }
        catch
        {
        }
    }
    protected void btnPlaceOrder_Click(object sender, EventArgs e)
    {
        //string Remarks = txtRemarks.Text;
        string Remarks = "";
       
            int status = new OrderBLL().InsertOrder(Session.SessionID, Convert.ToInt32(Session[Constants.UserId]), Remarks,ddlPaymentMode.Text);
            if (status > 0)
            {
             
                MessageCredentials objMsg = new MessageCredentials();
                objMsg = new OrderBLL().GetMobileNos(Convert.ToInt64(status.ToString()));
                //string cusMsg="Your Order Has Been Processed! Your Order Number Is: " + status .ToString() + "" +
                //    ".Thanks for Visiting Peshawari Online Store.";

                objMsg.CustomerMessageText = objMsg.CustomerMessageText.Replace("@OrderNo", status.ToString());
                string cusMsg = objMsg.CustomerMessageText.Replace("@CName", objMsg.CustomerName);
                sendMsg(objMsg.CustomerMobileNo, cusMsg);
                //string AdminSms = "New Order of " + CustomerName + " Has Been Processed! Order Number Is: " + status.ToString() + "";
                objMsg.AdminMessageText = objMsg.AdminMessageText.Replace("@OrderNo", status.ToString());
                string AdminSms = objMsg.AdminMessageText.Replace("@CName", objMsg.CustomerName);
                sendMsg(objMsg.AdminMobileNo, AdminSms);
                Session[Constants.OrderId] = status;
                if (ddlPaymentMode.Text == "Cash On Delivery")
                {
                    new CartBLL().UserCartDelete(Session.SessionID);
                    Response.Redirect("OrderSuccess.aspx");
                    Response.Write("<script>alert('Your order is placed successfully...Will contact you soon');</script>");
                }
                else if (ddlPaymentMode.Text == "Online Payment")
                {
                    if (status > 0)
                    {
                        OnlinePayment(status);
                    }
                }
            }
            else
            {
                Response.Write("<script>alert('Your order is not placed due to some issues... Try again');</script>");
            }
        
    }
    public string getRemoteAddr()
    {
        string UserIPAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (UserIPAddress == null)
        {
            UserIPAddress = Request.ServerVariables["REMOTE_ADDR"];
        }
        return UserIPAddress;
    }
    public string getSecureCookie(HttpRequest Request)
    {

        HttpCookie secureCookie = Request.Cookies["vsc"];
        if (secureCookie != null)
        {
            return secureCookie.ToString();
        }
        else
        {
            return "";
        }


    }

    void OnlinePayment(int OrderId)
    {
        Order objOrder=new Order();
        objOrder.OrderId=OrderId;
        new OrderBLL().GetOrderbyOrderId(objOrder);
        string NetAmount = objOrder.NetAmount.ToString();

        PGResponse objPGResponse = new PGResponse();
        CustomerDetails oCustomer = new CustomerDetails();
        SessionDetail oSession = new SessionDetail();
        AirLineTransaction oAirLine = new AirLineTransaction();
        MerchanDise oMerchanDise = new MerchanDise();

        SFA.CardInfo objCardInfo = new SFA.CardInfo();

        SFA.Merchant objMerchant = new SFA.Merchant();

        ShipToAddress objShipToAddress = new ShipToAddress();
        BillToAddress oBillToAddress = new BillToAddress();
        ShipToAddress oShipToAddress = new ShipToAddress();
        MPIData objMPI = new MPIData();
        PGReserveData oPGreservData = new PGReserveData();
        Address oHomeAddress = new Address();
        Address oOfficeAddress = new Address();
        // For getting unique MerchantTxnID 
        // Only for testing purpose. 
        // In actual scenario the merchant has to pass his transactionID
        DateTime oldTime = new DateTime(1970, 01, 01, 00, 00, 00);
        DateTime currentTime = DateTime.Now;
        TimeSpan structTimespan = currentTime - oldTime;
        string lMrtTxnID = ((long)structTimespan.TotalMilliseconds).ToString();

        //Setting Merchant Details
        objMerchant.setMerchantDetails("00215442", "00215442", "00215442", "", OrderId.ToString(), "Ord123", "http://www.peshawarisupermarket.com/SFAResponse.aspx", "POST", "INR", "INV123", "req.Sale", NetAmount, "GMT+05:30", "ASP.NET64", "true", "ASP.NET64", "ASP.NET64", "ASP.NET64");

        // Setting BillToAddress Details
        oBillToAddress.setAddressDetails("CID", "Maha Lakshmi", "Aline 1", "Aline2", "Aline3", "Pune", "MH", "48927489", "IND", "tester@opussoft.com");

        // Setting ShipToAddress Details
        oShipToAddress.setAddressDetails("$23@#|", "<script>", "Add 3", "City", "State", "443543", "IND", "tester@opussoft.com");

        //Setting MPI datails.
        //objMPI.setMPIRequestDetails ("1000","INR10.00","356","2","2 shirts","","","","0","","image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-powerpoint, application/vnd.ms-excel, application/msword, application/x-shockwave-flash, */*","Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.0)");

        // Setting Name home/office Address Details 
        // Order of Parameters =>        AddLine1, AddLine2,      AddLine3,   City,   State ,  Zip,          Country, Email id
        oHomeAddress.setAddressDetails("2Sandeep", "Uttam Corner", "Chinchwad", "Pune", "state", "4385435873", "IND", "test@test.com");

        // Order of Parameters =>        AddLine1, AddLine2,      AddLine3,   City,   State ,  Zip,          Country, Email id
        oOfficeAddress.setAddressDetails("2Opus", "MayFairTowers", "Wakdewadi", "Pune", "state", "4385435873", "IND", "test@test.com");

        // Stting  Customer Details 
        // Order of Parameters =>  First Name,LastName ,Office Address Object,Home Address Object,Mobile No,RegistrationDate, flag for matching bill to address and ship to address 
        oCustomer.setCustomerDetails("Sandeep", "patil", oOfficeAddress, oHomeAddress, "9423203297", "13-06-2007", "Y");

        //Setting Merchant Dise Details 
        // Order of Parameters =>       Item Purchased,Quantity,Brand,ModelNumber,Buyers Name,flag value for matching CardName and BuyerName
        oMerchanDise.setMerchanDiseDetails("Computer", "2", "Intel", "P4", "Sandeep Patil", "Y");

        //Setting  Session Details        
        // Order of Parameters =>     Remote Address, Cookies Value            Browser Country,Browser Local Language,Browser Local Lang Variant,Browser User Agent'
        oSession.setSessionDetails(getRemoteAddr(), getSecureCookie(Request), "", Request.ServerVariables["HTTP_ACCEPT_LANGUAGE"], "", Request.ServerVariables["HTTP_USER_AGENT"]);

        //Settingr AirLine Transaction Details  
        //Order of Parameters =>               Booking Date,FlightDate,Flight   Time,Flight Number,Passenger Name,Number Of Tickets,flag for matching card name and customer name,PNR,sector from,sector to'
        oAirLine.setAirLineTransactionDetails("10-06-2007", "22-06-2007", "13:20", "119", "Sandeep", "1", "Y", "25c", "Pune", "Mumbai");

        SFAClient objSFAClient = new SFAClient("c:\\inetpub\\wwwroot\\SFAClient\\Config\\");
        objPGResponse = objSFAClient.postSSL(objMPI, objMerchant, oBillToAddress, oShipToAddress, oPGreservData, oCustomer, oSession, oAirLine, oMerchanDise);

        if (objPGResponse.RedirectionUrl != "" & objPGResponse.RedirectionUrl != null)
        {
            string strResponseURL = objPGResponse.RedirectionUrl;
            Response.Redirect(strResponseURL);
        }
        else
        {
            Response.Write("Response Code:" + objPGResponse.RespCode);
            Response.Write("Response message:" + objPGResponse.RespMessage);
        }

    }

}