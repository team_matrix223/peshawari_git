﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

public partial class performance : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string AddUpdate()
    {
        return InsertUpdate().ToString();
    }

    public static int InsertUpdate()
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[1];
          objParam[0] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[0].Direction = ParameterDirection.ReturnValue;
 
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Performance_Insert", objParam);
            retValue = Convert.ToInt32(objParam[0].Value);
           
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }

}