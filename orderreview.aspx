﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="orderreview.aspx.cs" Inherits="orderreview" %>

<%@ Register Src="~/usercontrols/ucMenu.ascx" TagPrefix="uc1" TagName="ucMenu" %>
<%@ MasterType VirtualPath="main.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cnMaster" Runat="Server">

   <%--<script src="jquery-1.4.4.min.js" type="text/javascript"></script>--%>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="smartpaginator.js" type="text/javascript"></script>
    <link href="smartpaginator.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.uilocksearch.js" type="text/javascript"></script>



     <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <%--<script src="//code.jquery.com/jquery-1.10.2.js"></script>--%>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="css/jquery.notifyBar.css">
   <script src="jquery.notifyBar.js"></script>

    
         <link rel="stylesheet" href="styles.css">
   <%--<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>--%>
   <script src="script.js"></script>

<style type="text/css">
.cartheading
{
font-weight:bold;
text-align:left;    
width:110px
}
</style>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#txtSearch").remove();
            $("#btnSearch").remove();
        });
        </script>


 

     <asp:HiddenField ID="hdnId" runat="server" Value="0"/>
<div class="row" style="margin-top:15px;margin-bottom:15px">

<div class="col-md-12" >
 

<div  class="col-md-12" style=" border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
 
 <div class="row">
 <div class="col-md-12">
<span style="color:#58595b;font-size:16px;font-weight:bold;">DELIVERY INFORMATION</span>
 <hr  style="margin:2px;"/>
 <br />

 <div id="dvDeliveryAddresses" >
 <table width="100%">
 <tr>
 <td>
 <table>
 <tr><td class="cartheading">Name:</td><td><asp:Literal ID="ltRecipientName" runat="server"></asp:Literal></td></tr>
 <tr><td class="cartheading">Street:</td><td><asp:Literal ID="ltStreet" runat="server"></asp:Literal></td></tr>
 <tr><td class="cartheading">Area:</td><td><asp:Literal ID="ltArea" runat="server"></asp:Literal></td></tr>
 <tr><td class="cartheading">City</td><td><asp:Literal ID="ltCity" runat="server"></asp:Literal></td></tr>
 <tr><td class="cartheading">Address:</td><td><asp:Literal ID="ltAddress" runat="server"></asp:Literal></td></tr>
 <tr><td class="cartheading">PinCode:</td><td><asp:Literal ID="ltPinCode" runat="server"></asp:Literal></td></tr>
 <tr><td class="cartheading">Mobile</td><td><asp:Literal ID="ltMobile" runat="server"></asp:Literal></td></tr>
 
 </table>
 
 </td>
 
 <td valign="top">
 <table>
 <tr><td class="cartheading">Delivery Date:</td><td><asp:Literal ID="ltDeliveryDate" runat="server"></asp:Literal></td></tr>
 <tr><td class="cartheading">Delivery Slot:</td><td><asp:Literal ID="ltDeliverySlot" runat="server"></asp:Literal></td></tr>
<%-- <tr><td class="cartheading">Payment Mode:</td><td><asp:DropDownList ID="ddlPaymentMode" Height="30px" runat="server" CssClass="form-control"  Width="100%">
 <asp:ListItem Text=""></asp:ListItem>
 <asp:ListItem Text="Cash On Delivery" Value="Cash On Delivery"></asp:ListItem>
  <asp:ListItem Text="Online Payment" Value="Online Payment"></asp:ListItem>
 </asp:DropDownList></td></tr>--%>
 
 </table>
 
 </td>
 </tr>
 </table>
 <br />
 </div>
 
 

  

 </div>
 
 </div>

 


</div>
</div>

 

 


</div>
  

 <div class="row" style="margin-top:15px;margin-bottom:15px">

<div class="col-md-12" >
 

<div  class="col-md-12" style=" border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
 
 <div class="row">
 <div class="col-md-12">
<span style="color:#58595b;font-size:16px;font-weight:bold;">DELIVERY INFORMATION (<asp:Literal ID="ltItems" runat="server"></asp:Literal> Items)</span>
 <hr  style="margin:2px;"/>

 <div class="row">
 <div class="col-md-12">
 

  <table style="width:100%">
 <thead>
 <tr style="background:black;color:White;font-weight:bold;"><td></td><td style="padding:10px;width:400px">ITEM</td><td>UNIT PRICE</td><td>SUB TOTAL</td></tr>
 </thead>
 <tbody >
 <asp:Literal ID="ltCartContainer" runat="server"></asp:Literal>
 </tbody>
 </table>

 
 

 <table width="100%">

 <tr>
 <td><div style="margin:5px;border:solid 1px silver;float:right;width:350px;padding:5px;box-shadow:1px 1px 2px black">

<table style="float:right;margin-right:0px">
 <tr><td style="font-weight:bold;text-align:right;padding-right:10px;color:Green">Choose Payment Mode: </td><td><asp:DropDownList ID="ddlPaymentMode" Height="30px" runat="server" CssClass="form-control"  Width="100%">
 <asp:ListItem Text=""></asp:ListItem>
 <asp:ListItem Text="Cash On Delivery" Value="Cash On Delivery"></asp:ListItem>
  <asp:ListItem Text="Online Payment" Value="Online Payment"></asp:ListItem>
 </asp:DropDownList></td></tr>

 </table>
  </div></td>

<td>
 <div style="margin:5px;border:solid 1px silver;float:right;width:200px;padding:5px;box-shadow:1px 1px 2px black">

<table style="float:right;margin-right:0px">
 <tr><td style="font-weight:bold;text-align:right;padding-right:10px">Sub Total: </td><td> <asp:Literal ID="ltSubTotal" runat="server"></asp:Literal></td></tr>
 <tr id="trDel" runat="server"><td style="font-weight:bold;text-align:right;padding-right:10px">Delivery Charges: </td><td> <asp:Literal ID="ltDeliveryCharges" runat="server"></asp:Literal></td></tr>
  <tr id="trDisAmt" runat="server"><td style="font-weight:bold;text-align:right;padding-right:10px">Dis Amt: </td><td> <asp:Literal ID="ltDisAmt" runat="server"></asp:Literal></td></tr>
 <tr><td style="font-weight:bold;text-align:right;padding-right:10px">Net Amount: </td><td> <asp:Literal ID="ltNetAmount" runat="server"></asp:Literal></td></tr>
<%-- <tr><td colspan="100%"><a href="delivery.aspx"><button class="btn btn-success" type="button" style="width:100%;background:#D50002;border-color:#9e0a0a">Add To My Combo List</button></a></td></tr>--%>
<%-- <tr><td colspan="100%" style="padding-top:5px"> <a href="index.aspx"><button class="btn btn-success" type="button" style="width:100%;background:#5cb85c;border-color:#398439">Place Order</button></a></td></tr>--%>
  <tr><td colspan="100%" style="padding-top:5px"> 
 <asp:Button ID="btnPlaceOrder" runat="server" CssClass="btn btn-success"
         style="width:100%;background:#5cb85c;border-color:#398439" Text="Place Order"  Width="100%"
         onclick="btnPlaceOrder_Click" />
  
 
 </td></tr>
 </table>
  </div>
</td>
 </tr> 
 </table>
 
  

 </div>
 
 </div>
 

  

 </div>
 
 </div>

 


</div>
</div>

 

 


</div>
    <div id="dvMenuCategories" style="position:absolute;top:153px;display:none">

<uc1:ucMenu ID="ucMenu1" runat="server" />

</div>

</asp:Content>


 