﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="combos.aspx.cs" Inherits="combos" %>
 <%@ MasterType VirtualPath="main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnMaster" Runat="Server">
 <%--<script src="jquery-1.4.4.min.js" type="text/javascript"></script>--%>
    <script src="js/jquery.min.js" type="text/javascript"></script>
     <script src="js/jquery.uilocksearch.js" type="text/javascript"></script>



 
  <link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="css/jquery.notifyBar.css">
   <script src="jquery.notifyBar.js"></script>



   <script language="javascript" type="text/javascript">
       var m_MinPrice = -1;
       var m_MaxPrice = -1;
       var selValues = "";







       $(document).on("click", "div[name='btnAdd']", function (event) {

           var qty = 1;
           var data = $(this).attr("id");
           var arrData = data.split('_');
           var vid = arrData[1];

           var st = "p";
           var type = "Product";
           ATC(vid, qty, st, type);
           $.notifyBar({ cssClass: "success", html: "Product Added Succcessfully!" });




       });



       $(document).on("click", "div[name='dvAddToCart']", function (event) {

           var data = $(this).attr("id");
           var arrData = data.split('_');
           var vid = arrData[1];
           var qty = $("#q_" + arrData[1]).val();
           var st = "p";
           var type = "Product";
           ATC(vid, qty, st, type);
           $.notifyBar({ cssClass: "success", html: "Product Added Succcessfully!" });

       });




       function ATC(vid, qty, st, type) {
           $.ajax({
               type: "POST",
               data: '{"vid":"' + vid + '","qty":"' + qty + '","st":"' + st + '","type":"' + type + '"}',
               url: "combos.aspx/FirstTimeATC",
               async: false,
               contentType: "application/json",
               dataType: "json",
               success: function (msg) {


                   var obj = jQuery.parseJSON(msg.d);

                   if (obj.error == 1) {

                       alert("An Error occured during transaction. Please refresh the page and try again Later.");
                       return;
                   }


                   $("#sp_TotalItems1").html("MY CART(" + obj.TotalItems + ")");


      
               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function (msg) {


               }

           });

       }
     
       $(document).ready(function () {


        
           $.ajax({
               type: "POST",
               data: '{}',
               url: "combos.aspx/GetAllComboProducts",
               contentType: "application/json",
               dataType: "json",
               success: function (msg) {
                   $("#imgloading").remove();

                   $("#ComboProducts").append(msg.d);



               }, error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function (msg) {


               }

           });

       });

 

  


 


</script>
 <div class="row">
            
            
            <div class="col-md-12">
<div  id="dvSelectedCombo">
 <div class="row">
<%=strProduct %>
</div>
</div>
</div>
</div>
 <div class="row">
            
            <hr />
            <div class="col-md-12">
            </div>
            </div>
            <div class="row">
            
            
            <div class="col-md-12">
  					
              <h2 style="border: 1px solid silver; color: Black ; padding: 7px; font-size: 20px; margin-top: 10px; background: none repeat scroll 0% 0% rgb(238, 238, 238); font-family: serif;background-color:lightgray;">ALL COMBO OFFERS</h2>
</div>
</div>
 <div class="row">
            
            
            <div class="col-md-12">
<div  id="ComboProducts">


</div>
</div>
</div>





 


 
</asp:Content>
